<?php

/**
 * Created by PhpStorm.
 * User: Ankur Jha
 * Date: 26/09/18
 * Time: 11:21 AM
 */
class Database extends PDO
{

    public $dbtype = "mysql" ;
    public $host = "localhost" ;
    public $dbname = "dbagenpoint" ;
    public $dbuser = "root" ;
    public $dbpassword = "" ;

    /*public $dbtype = "mysql" ;
    public $host = "localhost" ;
    public $dbname = "dbagenpoint" ;
    public $dbuser = "dbagenpoint" ;
    public $dbpassword = "dbagenpoint@2019" ;*/

    /**
     * Database constructor.
     * @param string $dbtype
     * @param string $host
     * @param string $dbname
     * @param string $dbuser
     * @param string $dbpassword
     */
    public function __construct()
    {
        // e.g $dsn = "mysql:host=localhost;dbname=db1;" ;
        $dsn = $this->dbtype . ":" . "host=" . $this->host . ";" . "dbname=" . $this->dbname . ";" ;
        parent::__construct($dsn, $this->dbuser, $this->dbpassword);
    }

    /**
     * @return string
     */
    public function getDbtype()
    {
        return $this->dbtype;
    }

    /**
     * @param string $dbtype
     */
    public function setDbtype(string $dbtype)
    {
        $this->dbtype = $dbtype;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @param string $dbname
     */
    public function setDbname(string $dbname)
    {
        $this->dbname = $dbname;
    }

    /**
     * @return string
     */
    public function getDbuser()
    {
        return $this->dbuser;
    }

    /**
     * @param string $dbuser
     */
    public function setDbuser(string $dbuser)
    {
        $this->dbuser = $dbuser;
    }

    /**
     * @return string
     */
    public function getDbpassword()
    {
        return $this->dbpassword;
    }

    /**
     * @param string $dbpassword
     */
    public function setDbpassword(string $dbpassword)
    {
        $this->dbpassword = $dbpassword;
    }



}