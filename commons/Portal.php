<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 3:16 PM
 */

class Portal
{

// 1. check for URL
// 2. if BLANK URL -> redirect to login OR Home
// 3. if URL
//      3.1 then redirect to required controller
//      3.2 then controller loads the required model
//      3.3 then controller invokes the method which renders the view [usually the index method]

    public function __construct() {

        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);

//        In case URL is empty then redirect to Initial/Landing/Home page
        if (empty($url[0])) {
            $this->showInitialPage("login");
//            $this->showInitialPage("dashboard");
        }
        else {

            $filename = 'modules/' . $url[0] . '/controller/' . $url[0].'.php';
            
            if (file_exists($filename)) {
                require_once $filename;
            } else {
                $this->error();
            }

            $controller = new $url[0];

            $modelPath = 'modules/' . $url[0] . "/model/" . $url[0] ."_model.php";
            $name = $url[0] . "_model";

            $model = $controller->loadModel($modelPath,$name);

            if (isset($url[1])) {


                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}();
                } else {
                    $this->error();
                }
            } else {
                $controller->index();
            }

            if (isset($url[2])) {
                if (method_exists($controller, $url[1])) {
                    $controller->{$url[1]}($url[2]);
                } else {
                    $this->error();
                }
            }
        }
    }

    public function showInitialPage( $initialPage = "login" ){

//        $initialPage = 'modules/dashboard';
//        $initialPage = 'login';

        $arrModules = explode('/', rtrim($initialPage, '/'));
        $initialModule = explode('/', rtrim($initialPage, '/'))[count($arrModules)-1];

//        print_r($initialModule);
//        exit(0);
        require_once "modules/" . $initialPage . "/controller/" . $initialModule . ".php" ;
        $controller = new $initialModule;

        $model = $controller->loadModel( "modules/" . $initialPage . "/model/" . $initialModule . "_model.php", $initialModule ."_model");
        $controller->index();
        return false;
    }

    public function error() {
        // require_once 'controller/error.php';
        // $err = new Error();
        // $err->index();
        
            ini_set('display_errors', 0);
            require_once "./404.php";

       
        // /return true;
    }
}