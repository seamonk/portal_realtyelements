<?php

/**
 * Created by PhpStorm.
 * User: maxwellchristian
 * Date: 26/09/18
 * Time: 6:45 PM
 */
class SessionHandling
{
        /**
     * SessionHandling constructor.
     */
    public static function init()
    {
        session_start();
    }

    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function get($key)
    {
        if (isset($_SESSION[$key])){
            return $_SESSION[$key];
        }
        return false;
    }

    public static function unset_key($key) {
        unset($_SESSION[$key]);
    }

    public static function destroy()
    {
        session_destroy();
    }
}