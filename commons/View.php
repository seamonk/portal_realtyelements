<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 1:22 PM
 */

class View
{
    public function __construct() {
        //echo 'This is a view page...<br/>';
    }

    public function render($name,$title = "Pubsta", $header = null,$footer = null) {

//        echo $name;
//        exit();

        $initialContents = file_get_contents(  Urls::$COMMONS . "initialHeader.php");
        $contentsWithTitle = str_replace("@title", $title, $initialContents);

        echo $contentsWithTitle ;

        if($header != null){
            require $header;
        }

        require $name;

        if($footer != null) {
            require $footer;
        }

    }
}