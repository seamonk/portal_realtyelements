<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 1:13 PM
 */

class Controller
{
    public $view ;
    public $model ;

    function __construct()
    {
        //echo 'parent class call....<br/>';
        $this->view = new View();
    }

    public function loadModel($path, $name) {
//        $path = "models/".$name."_model.php";

        if (file_exists($path)) {

            require $path;
            $modelName = $name;
            $this->model = new $modelName();

            return $this->model;
        }
        else {
            echo "Model file does not exist : " . $path . " : " . $name;
            exit(0);
        }
    }
}