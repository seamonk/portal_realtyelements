<?php
/**
 * Created by PhpStorm.
 * Date: 26/09/18
 * Time: 11:24 AM
 */

// BASE_URL : defines the URL to the START of the APPLICATION
//define("BASE_URL","http://localhost:8888/ClientWorkspace/gurjarvani_portal/");

define("SITE_URL", dirname(__FILE__));

define("BASE_URL","http://localhost/portals/agenpoint/");

define("WEBSITE_BASE_URL","http://localhost/websites/agenpoint/");

/*define("BASE_URL","http://www.agenpoint.com/portals/");

define("WEBSITE_BASE_URL","http://www.agenpoint.com/");*/

/*-----START : SMS Gateway Details---------*/

define ( 'SMS_API', 'uqRDm7M08kO1D75YdT9U9Q' ) ;

define ( 'SMS_API_URL', 'https://www.smsgatewayhub.com/api/mt/SendSms' ) ;

define ( 'SMS_API_USER_NAME', 'vikas.agenpoint' ) ;

define ( 'SMS_API_PASSWORD', '742215' ) ;

define ( 'SMS_API_SENDERID', 'SMSTST' ) ;

define ( 'MESSAGE_FOR_OTP_OVER_SMS', "is OTP to verify your mobile number. Don't share it with anyone." ) ;

define ( 'MESSAGE_FOR_OTP_OVER_EMAIL', 'is OTP to verify your email address at AgenPoint. Donot share it with anyone' ) ;

define ( 'RESULT_FORMAT_CSV', 'csv' ) ;
define ( 'RESULT_FORMAT_JSON', 'json' ) ;

define ('APPOINTMENT_RESPONSE_MESSAGE',"We are awaiting the confirmation from<BR><b style='color:#01b2e7'>#1</b><BR><BR>You can track the progress of this booking in your Appointment history.<BR><BR>Your appointment ID is<BR><b style='color:#01b2e7'>#2</b>");

/*-----END : SMS Gateway Details---------*/

define("DUMMY_DATA",0);

define("ADMIN","Superadmin");
define("AGENT","Agents");
define("DEVELOPER","Developer");


class Urls {

    // BASE_URL : defines the URL to the START of the APPLICATION
    static $BASE = BASE_URL ;
    static $WEBSITE_BASE_URL = WEBSITE_BASE_URL;

    static $COMMONS = BASE_URL."commons/";

    static $MODULES = BASE_URL."modules/";

    static $ASSETS = BASE_URL."commons/assets/";

    static $MEDIA = BASE_URL."media/";

    // LOGO_URL : defines the URL to the LOGO for the APPLICATION
    static $LOGOS = BASE_URL."media/logos/" ;

    static $LOADER = BASE_URL."media/" ;

    static $IMG_UPLOADS = "public/uploads/";

    static $IMG_UPLOADS_LISTING = "public/uploads/listings/";
    
    static $CITY_IMAGE_UPLAODS = "public/uploads/city_Image/";
    

    static $IMG_UPLOADS_USER = "public/uploads/users/";

    static $ASSOCIATIONS = "public/uploads/associations/";
    
    static $ABOUTUS = "public/uploads/amenities/";
    static $TEAM = "public/uploads/team/";

     static $BLOGS = "public/uploads/blogs/";

    static $BLOG_UPLAODS = "public/uploads/blogs/";
    static $CV_UPLOAD = "public/uploads/carearDocuments/";
    
    
    static $TESTIMONIALS_UPLAODS = "public/uploads/testimonial/";


}



