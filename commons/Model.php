<?php

/**
 * Created by PhpStorm.
 * User: maxwellchristian
 * Date: 26/09/18
 * Time: 12:35 PM
 */
class Model
{
    public $db ;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->db = new Database();
    }

    /**
     * @return Database
     */
    public function getDb()
    {
        return $this->db;
    }

    public function update($data, $table,$conditions)
    {
        $colvalSet = '';
        $whereSql = '';

        $i = 0;

        foreach ($data as $key => $val) {
            $pre = ($i > 0) ? ', ' : '';
            $colvalSet .= $pre . $key . " = :" . $key ;
            $i++;
        }

        if (!empty($conditions) && is_array($conditions)) {

            $whereSql .= ' WHERE ';

            $i = 0;
            foreach ($conditions as $key => $value) 
            {
                $pre = ($i > 0) ? ' AND ' : '';
                $whereSql .= $pre . $key . " = :" . $key;
                $i++;
            }
        }

        $sql = "UPDATE " . $table . " SET " . $colvalSet . $whereSql;

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {
            $query->bindValue(':' . $key, $val);
        }

        foreach ($conditions as $key => $val) {
            $query->bindValue(':' . $key, $val);
        }
        $update = $query->execute();

        if ($update) {
            return $update ? $query->rowCount() : false;
        }
    }




}