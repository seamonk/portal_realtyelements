<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
?>
<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow SideTopSpace">
        <li class="m-menu__item  <?php if (strpos($url,'/dashboard') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true">
            <a href="<?php echo Urls::$BASE; ?>dashboard" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-home"></i>
                <span class="m-menu__link-title">
					<span class="m-menu__link-wrap">
						<span class="m-menu__link-text">Dashboard</span>
					</span>
				</span>
            </a>
        </li>
        <li class="m-menu__item <?php if (strpos($url,'/agent') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="<?php echo Urls::$BASE; ?>agent" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fa fa-user-tie"></i>
                <span class="m-menu__link-text">Agents / Developer</span>
            </a>
        </li>
        <li class="m-menu__item  <?php if (strpos($url,'/properties/listing') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="<?php echo Urls::$BASE; ?>properties/listing" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fa fa-home"></i>
                <span class="m-menu__link-text">Listing</span>
            </a>
        </li>


        <li class="m-menu__item <?php if (strpos($url,'/properties/types') !== false || strpos($url,'properties/subtypes') !== false || strpos($url,'properties/amenity') !== false || strpos($url,'properties/facing') !== false || strpos($url,'properties/units') !== false || strpos($url,'location/') !== false || strpos($url,'associations/') !== false || strpos($url,'designations/') !== false || strpos($url,'companies/') !== false || strpos($url,'services/') !== false || strpos($url,'properties/propertyBHK') !== false || strpos($url,'/specializations/') !== false || strpos($url,'/languages/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">

            <a href="<?php echo Urls::$BASE; ?>agent" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fas fa-cog"></i>
                <span class="m-menu__link-text">Tools</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">

                    <li class="m-menu__item <?php if (strpos($url,'properties/types') !== false || strpos($url,'properties/subtypes') !== false  || strpos($url,'properties/amenity') !== false || strpos($url,'properties/facing') !== false || strpos($url,'properties/units') !== false  || strpos($url,'properties/propertyBHK') !== false){echo 'm-menu__item--active m-menu__item--open' ;} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">

                        <a href="javascript:;" class="m-menu__link m-menu__toggle">

                            <i style="<?php if (strpos($url,'properties/types') !== false || strpos($url,'properties/subtypes') !== false || strpos($url,'properties/derivedtype') !== false || strpos($url,'properties/amenity') !== false || strpos($url,'properties/facing') !== false || strpos($url,'properties/units') !== false || strpos($url,'properties/propertyBHK') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; } ?>" class="m-menu__link-bullet fa fa-list">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">Listings</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>properties/types" class="m-menu__link">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Types</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>properties/subtypes" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Sub Types</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>properties/amenity" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Amenities</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>properties/facing" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Facing</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>properties/units" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Units</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>properties/propertyBHK" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">BHK's</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                   
                    <li class="m-menu__item  <?php if (strpos($url,'location/') !== false || strpos($url,'location/state') !== false || strpos($url,'location/city') !== false || strpos($url,'location/area') !== false || strpos($url, 'location/locality' != false)){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i style="<?php if (strpos($url,'location/') !== false || strpos($url,'location/state') !== false || strpos($url,'location/city') !== false || strpos($url,'location/area') !== false || strpos($url, 'location/locality' != false)){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; } ?>" class="m-menu__link-bullet fa fa-location-arrow">
                                <span>&nbsp;&nbsp;</span>
                            </i>
                           <span class="m-menu__link-text">Location</span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>location/" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Countries</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>location/state" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">States</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>location/city" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Cities</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>location/area" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Areas</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>location/locality" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Locality</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>


                    <li class="m-menu__item <?php if (strpos($url,'/associations/') !== false || strpos($url,'/designations/') !== false || strpos($url,'/companies/') !== false || strpos($url,'/services/') !== false || strpos($url,'/imagelabels/') !== false || strpos($url,'/specializations/') !== false || strpos($url,'/languages/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i style="<?php if (strpos($url,'/associations/') !== false  || strpos($url,'/designations/') !== false || strpos($url,'/companies/') !== false || strpos($url,'/services/') !== false || strpos($url,'/imagelabels/') !== false || strpos($url,'/specializations/') !== false || strpos($url,'/languages/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; } ?>" class="m-menu__link-bullet fa fa-user-tie">
                                <span>&nbsp;&nbsp;</span>
                            </i>
                           <span class="m-menu__link-text">Agents</span>
                        <i class="m-menu__ver-arrow la la-angle-right" ></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                     <a href="<?php echo Urls::$BASE; ?>associations/" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon far fa-building" style="font-size: 10px; color: #8c8ea4;"></i>
                                        <span class="m-menu__link-text">Associations</span>                
                                    </a> 
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>designations/" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon far fa-building" style="font-size: 10px; color: #8c8ea4;"></i>
                                        <span class="m-menu__link-text">Designations</span>                
                                    </a> 
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>companies/" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon far fa-building" style="font-size: 10px; color: #8c8ea4;"></i>
                                        <span class="m-menu__link-text">Companies</span>                
                                    </a> 
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>services/" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon far fa-building" style="font-size: 10px; color: #8c8ea4;"></i>
                                        <span class="m-menu__link-text">Services</span>                
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>specializations/" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon far fa-building" style="font-size: 10px; color: #8c8ea4;"></i>
                                        <span class="m-menu__link-text">Specializations</span>                
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>languages/" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon fas fa-language" style="font-size: 10px; color: #8c8ea4;"></i>
                                        <span class="m-menu__link-text">Languages</span>                
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    
                    
                </ul>
            </div>
        </li>


        <li class="m-menu__item <?php if (strpos($url,'/aboutus/') !== false  || strpos($url,'/career/') !== false || strpos($url,'/testimonials/') !== false || strpos($url,'/newsletters/') !== false || strpos($url,'/team/') !== false || strpos($url,'/contactus/') !== false || strpos($url,'/news/') !== false || strpos($url,'/users/') !== false || strpos($url,'/reports/') !== false || strpos($url,'/contactus') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="#" class="m-menu__link m-menu__toggle" >
                <i class="m-menu__link-icon fa fa-globe"></i>
                    <span class="m-menu__link-text">Website</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                 <ul class="m-menu__subnav">
                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-info" style=" <?php if (strpos($url,'/aboutus/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>" ></i>
                            <span class="m-menu__link-text">About Us</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>aboutus/" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Add New</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>aboutus/listing" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Listing</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-user-circle"  style=" <?php if (strpos($url,'/team/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">Team</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>team/designations" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Team Designations</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>team/" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Team Member</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-users"  style=" <?php if (strpos($url,'/career/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">Career</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                     <a href="<?php echo Urls::$BASE; ?>career/listing" class="m-menu__link">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Career</span>
                                    </a>
                                </li>
                                
                                <li class="m-menu__item " aria-haspopup="true">
                                   <a href="<?php echo Urls::$BASE; ?>career/" class="m-menu__link">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Designations</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                   <a href="<?php echo Urls::$BASE; ?>career/positionrequest" class="m-menu__link">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Position Request</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                   <!--  <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="<?php echo Urls::$BASE; ?>career/" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-newspaper"  style=" <?php if (strpos($url,'/career/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">Career</span>                
                        </a> 
                    </li>  -->

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-users"  style=" <?php if (strpos($url,'/users/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">Users</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>users/user_type" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">User Type</span>
                                    </a>
                                </li>
                              <!--   <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>users/add_user" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Add New User</span>
                                    </a>
                                </li> -->
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>users/" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">User Listing</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="<?php echo Urls::$BASE ?>contactus" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-users-1"  style=" <?php if (strpos($url,'/contactus/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">Contact Us</span>
                            <!-- <i class="m-menu__ver-arrow la la-angle-right"></i> -->
                        </a>
                    </li>

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-user-circle"  style=" <?php if (strpos($url,'/testimonials/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">Testimonials</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>testimonials/add_testimonial" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Add Testimonials</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>testimonials/" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Testimonials Listing</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>  

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="<?php echo Urls::$BASE; ?>newsletters/" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon fa fa-newspaper"  style=" <?php if (strpos($url,'/newsletters/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">News Letters</span>                
                        </a>  
                    </li>  

                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">

                            <i class="m-menu__link-icon fa fa-newspaper"  style=" <?php if (strpos($url,'/news/') !== false){ echo "font-size: 10px;color: #ef424a ;"; }else{ echo "font-size: 10px;color: #8c8ea4;"; }?>"></i>
                            <span class="m-menu__link-text">News</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">

                            <span class="m-menu__arrow"></span>

                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">

                                    <a href="<?php echo Urls::$BASE; ?>news/add_news" class="m-menu__link ">

                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">

                                            <span></span>

                                        </i>

                                        <span class="m-menu__link-text">Add News</span>

                                    </a>

                                </li>
                                <li class="m-menu__item " aria-haspopup="true">

                                    <a href="<?php echo Urls::$BASE; ?>news/" class="m-menu__link ">

                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">

                                            <span></span>

                                        </i>

                                        <span class="m-menu__link-text">News Listing</span>

                                    </a>

                                </li>
                            </ul>

                        </div>
                    </li>
                    
                    <li class="m-menu__item  <?php if (strpos($url,'/reports/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <!-- <i class=""></i> -->
                            <i class="m-menu__link-icon  far fa-chart-bar"></i>
                            <span class="m-menu__link-text">Reports</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                        <div class="m-menu__submenu ">
                            <span class="m-menu__arrow"></span>
                            <ul class="m-menu__subnav">
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?php echo Urls::$BASE; ?>reports/user_reports" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Customers</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="#" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Listings</span>
                                    </a>
                                </li>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="#" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                                        <span class="m-menu__link-text">Finance</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>

                     <li class="m-menu__item  <?php if (strpos($url,'/reports/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover" style="margin-bottom: 200px !important;">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <!-- <i class=""></i> -->
                            <i class="m-menu__link-icon flaticon-settings"></i>
                            <span class="m-menu__link-text">Support</span>
                            <!-- <i class="m-menu__ver-arrow la la-angle-right"></i> -->
                        </a>
                       
                    </li>
                </ul>
            </div>
        </li>
        <li class="m-menu__item  <?php if (strpos($url,'/plans/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-coins"></i>
                    <span class="m-menu__link-text">subcription</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>plans/" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Subscription Plans </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>plans/user_subcription" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Purchase History</span>
                            </a>
                        </li>                    
                    </ul>
                </div>
            </li>
       <!--  <li class="m-menu__item  <?php if (strpos($url,'/subcription/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="<?php echo Urls::$BASE; ?>subcription" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon fa fa-chart-bar"></i>
                <span class="m-menu__link-text">Subcription</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            
        </li> -->
        
    </ul>
</div>