

<style type="text/css">
    .hover:hover{
        background-color: #f4516a !important;
    }
</style>

<?php
$url = 'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
?>
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-light m-aside-menu--submenu-skin-light" m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500">

        <ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow  m-menu__item--active m-menu__item--open">
            <li class="m-menu__item" aria-haspopup="true" style="text-align: center;">
                <span class="m-menu__link" style="cursor: default; ">
                    
                    <span class="m-menu__link-title" style="width: 60px;">
                        <a style="text-decoration: none;" href="<?php echo Urls::$WEBSITE_BASE_URL; ?>" target="_blank" >
                            <span class="m-menu__link-wrap" style="cursor: pointer;">
                                <span class="m-menu__link-text" style="text-align: center; background-color: #f4516c;color: #fff !important; padding: 10px;width: 128px !important;">
                                    View Website
                                </span>
                            </span>
                        </a>
                    </span>
                    
                </span>
               
            </li>
            <br>
            <li class="m-menu__item  <?php if (strpos($url,'/dashboard') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true">
                <a href="<?php echo Urls::$BASE; ?>dashboard" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">Dashboard</span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  <?php if (strpos($url,'/agent/profile') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true">
                <a href="<?php echo Urls::$BASE; ?>agent/profile" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-profile-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">Profile</span>
                        </span>
                    </span>
                </a>
            </li>  
            <li class="m-menu__item  <?php if (strpos($url,'/plans/userSubcription') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true">
                <a href="<?php echo Urls::$BASE; ?>plans/user_subcription" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-profile-1"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">Subcription</span>
                        </span>
                    </span>
                </a>
            </li>  
                     
            <li class="m-menu__item  <?php if (strpos($url,'/properties/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-home"></i>
                    <span class="m-menu__link-text">Listings</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>properties/add_properties" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Add New </span>
                            </a>
                        </li>

                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>properties/" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text"> Listings</span>
                            </a>
                        </li>
                       
                    </ul>
                </div>
            </li>

            <li class="m-menu__item  <?php if (strpos($url,'/leads/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa-users"></i>
                    <span class="m-menu__link-text">Leads <span class="m-menu__link-badge">
                            <span class="m-badge m-badge--danger" id="leadCountInMenu">0</span>
                        </span></span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                       <!--  <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>leads" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">New Leads</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>leads" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Pending Leads</span>
                            </a>
                        </li>  -->
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>leads" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">All Leads</span>
                            </a>
                        </li>                    
                    </ul>
                </div>
            </li>

            
           
           <!--  <li class="m-menu__item  <?php if (strpos($url,'/reports/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="#" class="m-menu__link m-menu__toggle">
                   
                    <i class="m-menu__link-icon  far fa-chart-bar"></i>
                    <span class="m-menu__link-text">Reports</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="<?php echo Urls::$BASE; ?>reports/user_reports" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Customers</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="#" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Listings</span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="#" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">Finance</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> -->

             <li class="m-menu__item  <?php if (strpos($url,'/reports/') !== false){echo 'm-menu__item--active m-menu__item--open';} ?>" aria-haspopup="true" m-menu-submenu-toggle="hover" style="margin-bottom: 200px !important;">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <!-- <i class=""></i> -->
                    <i class="m-menu__link-icon flaticon-settings"></i>
                    <span class="m-menu__link-text">Support</span>
                    <!-- <i class="m-menu__ver-arrow la la-angle-right"></i> -->
                </a>
               
            </li>
           
          
        </ul>
    </div>
    <!-- END: Aside Menu -->

