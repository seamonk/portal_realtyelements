</div>
</div>

<!-- end:: Body -->

<!-- begin::Footer -->
<footer class="m-grid__item		m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
				<span class="m-footer__copyright">
					<?php echo date('Y'); ?> &copy; Seamonk Solution
					<a href="#" class="m-link">AgenPoint</a>
				</span>
            </div>
        </div>
    </div>
</footer>

<!-- end::Footer -->
</div>

<!-- end:: Page -->


<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>



<!--begin::Base Scripts -->
<!--<script src="--><?php //echo Urls::$COMMONS ?><!--assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>-->
<!--<script src="--><?php //echo Urls::$COMMONS ?><!--assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>-->

<!--end::Base Scripts -->

<script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>

<!--begin::Page Vendors Scripts -->
<script src="<?php echo Urls::$COMMONS ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors Scripts -->

<!--begin::Page Vendors Scripts -->
<script src="<?php echo Urls::$COMMONS ?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors Scripts -->

<!--begin::Page Resources -->
<script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/crud/datatables/basic/paginations.js" type="text/javascript"></script>

<script src="<?php echo Urls::$COMMONS ?>assets/custom/datatable_pdf_maker.js"></script>


<!--end::Page Resources -->

<!-- <script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/crud/forms/validation/form-controls.js" type="text/javascript"></script>
 -->
<!--begin::Page Snippets -->
<script src="<?php echo Urls::$COMMONS ?>assets/app/js/dashboard.js" type="text/javascript"></script>

<!--end::Page Snippets -->

<script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/crud/forms/widgets/summernote.js" type="text/javascript"></script>

<script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js" type="text/javascript"></script>

<script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/components/base/sweetalert2.js" type="text/javascript"></script>


<!-- <script src="<?php echo Urls::$COMMONS ?>assets/demo/default/custom/crud/wizard/wizard.js" type="text/javascript"></script> -->
<script>
    $(window).on('load', function() {
        $('body').removeClass('m-page--loading');
    });
</script>

<?php 
if (SessionHandling::get("err_msg"))
{
    echo ' <script> swal({
            type:"error",
            title:"'.SessionHandling::get('err_msg').'",
            showConfirmButton:!1,timer:1500
            });</script>';
    SessionHandling::unset_key("err_msg");
}
if (SessionHandling::get('suc_msg'))
{
    echo ' <script> swal({
            type:"success",
            title:"'.SessionHandling::get('suc_msg').'",
            showConfirmButton:!1,timer:1500
            });</script>';
    SessionHandling::unset_key('suc_msg');
}
?>
</body>

<!-- end::Body -->
</html>