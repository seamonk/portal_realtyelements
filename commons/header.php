
 <?php
        if (empty(SessionHandling::get('userName')))
        {   
            header("Location:".Urls::$BASE);

        }
       
        
 ?>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->

    <!--begin::Page Vendors Styles -->
    <link href="<?php echo Urls::$COMMONS ?>assets\vendors\custom\jquery-ui\jquery-ui.bundle.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo Urls::$COMMONS ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

    <!-- <link href="<?php echo Urls::$COMMONS ?>assets/vendors/custom/fullcalendar/fullcalendar.bundle.rtl.css" rel="stylesheet" type="text/css" /> -->

    <!--end::Page Vendors Styles -->
    <link href="<?php echo Urls::$COMMONS ?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <!--begin::Base Styles -->
    <link href="<?php echo Urls::$COMMONS ?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />

    <!--RTL version:<link href="<?php echo Urls::$COMMONS ?>assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo Urls::$COMMONS ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />


    <link href="<?php echo Urls::$COMMONS ?>assets/custom/custom.css" rel="stylesheet" type="text/css" />
    


    <link rel="stylesheet" type="text/css" href="<?php echo Urls::$COMMONS ?>assets/custom/agenpoint_custom.css">

    

    <!--end::Base Styles -->
    <link rel="shortcut icon" href="<?php echo Urls::$MEDIA; ?>ico/logo.ico" />
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-37564768-1', 'auto');
        ga('send', 'pageview');
    </script>

    <!--begin::Base Scripts -->
    <script src="<?php echo Urls::$COMMONS ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="<?php echo Urls::$COMMONS ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

</head>

<!-- end::Head -->

<!-- begin::Body -->
<!-- <body class="m-page--fluid m--skin- m-page--loading-enabled m-page--loading m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"> -->

    <body class="m-page--fluid m--skin- m-page--loading m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<div class="m-page-loader m-page-loader--base">
    <img src="<?php echo Urls::$LOADER; ?>loader.gif" alt="">
</div>

<body class="m-content--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside--offcanvas-default">
<!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
        <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">

                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand  m-brand--skin-light">
                        <div class="m-stack m-stack--ver m-stack--general m-stack--fluid">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="<?php echo WEBSITE_BASE_URL;?>home" class="m-brand__logo-wrapper">
                                    <img src="<?php echo Urls::$MEDIA; ?>logo.png" width="150px;">
                                </a>
                            </div>
                            <div class="m-stack__item m-stack__item--middle m-brand__tools">

                                <!-- BEGIN: Left Aside Minimize Toggle -->
                                <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                                    <span></span>
                                </a>

                                <!-- END -->

                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                    
                                    <span></span>
                                </a>

                                <!-- END -->

                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                    <i class="flaticon-more"></i>
                                </a>

                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>

                    <!-- END: Brand -->
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                        <!-- BEGIN: Topbar -->
                        <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                <ul class="m-topbar__nav m-nav m-nav--inline">
                                   <!--  <li class="m-nav__item m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center m-dropdown--mobile-full-width m-dropdown--skin-light m-list-search m-list-search--skin-light" m-dropdown-toggle="click" id="m_quicksearch" m-quicksearch-mode="dropdown"
                                     m-dropdown-persistent="1">
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                            <div class="m-dropdown__inner ">
                                                <div class="m-dropdown__header">
                                                    <form class="m-list-search__form">
                                                        <div class="m-list-search__form-wrapper">
                                                            <span class="m-list-search__form-input-wrapper">
                                                                <input id="m_quicksearch_input" autocomplete="off" type="text" name="q" class="m-list-search__form-input" value="" placeholder="Search...">
                                                            </span>
                                                            <span class="m-list-search__form-icon-close" id="m_quicksearch_close">
                                                                <i class="la la-remove"></i>
                                                            </span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__scrollable m-scrollable" data-scrollable="true" data-height="300" data-mobile-height="200">
                                                        <div class="m-dropdown__content">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="m-nav__item m-topbar__notifications m-dropdown m-dropdown--large m-dropdown--arrow m-dropdown--align-center  m-dropdown--mobile-full-width" m-dropdown-toggle="click" m-dropdown-persistent="1">
                                        <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                                            <span class="m-nav__link-icon">
                                                <span class="m-nav__link-icon-wrapper">
                                                    <i class="flaticon-alarm"></i>
                                                </span>
                                                <span class="m-nav__link-badge m-badge m-badge--success">3</span>
                                            </span>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center">
                                                    <span class="m-dropdown__header-title">9 New</span>
                                                    <span class="m-dropdown__header-subtitle">User Notifications</span>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                                                            <li class="nav-item m-tabs__item">
                                                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                                                                    Alerts
                                                                </a>
                                                            </li>
                                                            <li class="nav-item m-tabs__item">
                                                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">Events</a>
                                                            </li>
                                                            <li class="nav-item m-tabs__item">
                                                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">Logs</a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                                                                <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                    <div class="m-list-timeline m-list-timeline--skin-light">
                                                                        <div class="m-list-timeline__items">
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                                                                <span class="m-list-timeline__text">12 new users registered</span>
                                                                                <span class="m-list-timeline__time">Just now</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span class="m-list-timeline__text">System shutdown
                                                                                    <span class="m-badge m-badge--success m-badge--wide">pending</span>
                                                                                </span>
                                                                                <span class="m-list-timeline__time">14 mins</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span class="m-list-timeline__text">New invoice received</span>
                                                                                <span class="m-list-timeline__time">20 mins</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span class="m-list-timeline__text">DB overloaded 80%
                                                                                    <span class="m-badge m-badge--info m-badge--wide">settled</span>
                                                                                </span>
                                                                                <span class="m-list-timeline__time">1 hr</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span class="m-list-timeline__text">System error -
                                                                                    <a href="#" class="m-link">Check</a>
                                                                                </span>
                                                                                <span class="m-list-timeline__time">2 hrs</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span href="" class="m-list-timeline__text">New order received
                                                                                    <span class="m-badge m-badge--danger m-badge--wide">urgent</span>
                                                                                </span>
                                                                                <span class="m-list-timeline__time">7 hrs</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item m-list-timeline__item--read">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span class="m-list-timeline__text">Production server down</span>
                                                                                <span class="m-list-timeline__time">3 hrs</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge"></span>
                                                                                <span class="m-list-timeline__text">Production server up</span>
                                                                                <span class="m-list-timeline__time">5 hrs</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                                                                <div class="m-scrollable" data-scrollable="true" data-height="250" data-mobile-height="200">
                                                                    <div class="m-list-timeline m-list-timeline--skin-light">
                                                                        <div class="m-list-timeline__items">
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                <a href="" class="m-list-timeline__text">New order received</a>
                                                                                <span class="m-list-timeline__time">Just now</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                                                                <a href="" class="m-list-timeline__text">New invoice received</a>
                                                                                <span class="m-list-timeline__time">20 mins</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                                                                <a href="" class="m-list-timeline__text">Production server up</a>
                                                                                <span class="m-list-timeline__time">5 hrs</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                <a href="" class="m-list-timeline__text">New order received</a>
                                                                                <span class="m-list-timeline__time">7 hrs</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                <a href="" class="m-list-timeline__text">System shutdown</a>
                                                                                <span class="m-list-timeline__time">11 mins</span>
                                                                            </div>
                                                                            <div class="m-list-timeline__item">
                                                                                <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                                                                <a href="" class="m-list-timeline__text">Production server down</a>
                                                                                <span class="m-list-timeline__time">3 hrs</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                                                                <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                                                                    <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                                                        <span class="">All caught up!
                                                                            <br>No new logs.</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="m-nav__item m-topbar__quick-actions m-dropdown m-dropdown--skin-light m-dropdown--large m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-nav__link-icon">
                                                <span class="m-nav__link-icon-wrapper">
                                                    <i class="flaticon-share"></i>
                                                </span>
                                                <span class="m-nav__link-badge m-badge m-badge--brand">5</span>
                                            </span>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center">
                                                    <span class="m-dropdown__header-title">Quick Actions</span>
                                                    <span class="m-dropdown__header-subtitle">Shortcuts</span>
                                                </div>
                                                <div class="m-dropdown__body m-dropdown__body--paddingless">
                                                    <div class="m-dropdown__content">
                                                        <div class="m-scrollable" data-scrollable="false" data-height="380" data-mobile-height="200">
                                                            <div class="m-nav-grid m-nav-grid--skin-light">
                                                                <div class="m-nav-grid__row">
                                                                    <a href="#" class="m-nav-grid__item">
                                                                        <i class="m-nav-grid__icon flaticon-file"></i>
                                                                        <span class="m-nav-grid__text">Generate Report</span>
                                                                    </a>
                                                                    <a href="#" class="m-nav-grid__item">
                                                                        <i class="m-nav-grid__icon flaticon-time"></i>
                                                                        <span class="m-nav-grid__text">Add New Event</span>
                                                                    </a>
                                                                </div>
                                                                <div class="m-nav-grid__row">
                                                                    <a href="#" class="m-nav-grid__item">
                                                                        <i class="m-nav-grid__icon flaticon-folder"></i>
                                                                        <span class="m-nav-grid__text">Create New Task</span>
                                                                    </a>
                                                                    <a href="#" class="m-nav-grid__item">
                                                                        <i class="m-nav-grid__icon flaticon-clipboard"></i>
                                                                        <span class="m-nav-grid__text">Completed Tasks</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>    -->                                
                                    
                                    <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                        <?php
                                            if (SessionHandling::get('userProfile')) 
                                            { 
                                                $profileImg = SessionHandling::get('userProfile');
                                            }
                                            else
                                            {
                                                $profileImg = 'user.jpg';
                                            }

                                            // $loggedInName = SessionHandling::get('vUserFirstName') . " " . SessionHandling::get('vUserLastName');
                                        ?>
                                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__username" style="color: #ef3f44;"><h6>Hi, <?php echo SessionHandling::get('userName') ." ".SessionHandling::get('lastName'); ?></h6><center style="color: #cacaca;font-weight: bold;"><?php echo SessionHandling::get('userType'); ?></center></span>&nbsp;&nbsp;&nbsp;
                                                    <span class="m-topbar__userpic">
                                                        <img id="HeaderProfilePic" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$profileImg; ?>" class="m--img-rounded m--marginless" alt="" />
                                                    </span>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center" style="background: url(<?php echo Urls::$COMMONS ?>assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover; filter: grayscale(100%) !important;">
                                                    <div class="m-card-user m-card-user--skin-dark">
                                                        <div class="m-card-user__pic">
                                                            <img src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$profileImg; ?>" class="m--img-rounded m--marginless" alt="" />
                                                        </div>
                                                        <div class="m-card-user__details">
                                                            <span class="m-card-user__name m--font-weight-500"><?php if (SessionHandling::get('userName')) { echo SessionHandling::get('userName') ." ".SessionHandling::get('lastName');} else { echo "";} ?></span>
                                                            <a href="#" id="emailID_Header"  class="m-card-user__email m--font-weight-300 m-link"><?php if (SessionHandling::get('emailID')) { echo SessionHandling::get('emailID');} else { echo "";} ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav m-nav--skin-light">
                                                            <li class="m-nav__section m--hide">
                                                                <span class="m-nav__section-text">Section</span>
                                                            </li>
                                                            <?php 

                                                                $URL = '';

                                                                if(SessionHandling::get('userType'))
                                                                {
                                                                    if(SessionHandling::get('userType') == ADMIN)
                                                                    {
                                                                        
                                                                    }
                                                                    if(SessionHandling::get('userType') == DEVELOPER)
                                                                    {
                                                                        ?>
                                                                        <li class="m-nav__item">
                                                                            <a href="<?php echo Urls::$BASE; ?>agent/profile" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                                <span class="m-nav__link-title">
                                                                                    <span class="m-nav__link-wrap">
                                                                                        <span class="m-nav__link-text">My Profile</span>
                                                                                    </span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    elseif (SessionHandling::get('userType') == AGENT)
                                                                    {
                                                                        ?>
                                                                        <li class="m-nav__item">
                                                                            <a href="<?php echo Urls::$BASE; ?>agent/profile" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                                <span class="m-nav__link-title">
                                                                                    <span class="m-nav__link-wrap">
                                                                                        <span class="m-nav__link-text">My Profile</span>
                                                                                    </span>
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }                                   
                                                                }

                                                             ?>
                                                            

                                                            <li class="m-nav__separator m-nav__separator--fit">
                                                            </li>
                                                            <?php 

                                                                $URL = '';

                                                                if(SessionHandling::get('userType'))
                                                                {
                                                                    if(SessionHandling::get('userType') == ADMIN)
                                                                    {
                                                                        $URL = Urls::$BASE."dashboard/logout";
                                                                    }
                                                                    elseif (SessionHandling::get('userType') == AGENT)
                                                                    {
                                                                        $URL = WEBSITE_BASE_URL."home/logout";
                                                                         // $URL = Urls::$BASE."dashboard/logout";
                                                                    } 
                                                                    elseif (SessionHandling::get('userType') == DEVELOPER )
                                                                    {
                                                                        $URL = WEBSITE_BASE_URL."home/logout";
                                                                         // $URL = Urls::$BASE."dashboard/logout";
                                                                    }                                   
                                                                }

                                                             ?>
                                                            <li class="m-nav__item">
                                                                <a href="<?php echo $URL; ?>" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- END: Topbar -->
                    </div>
                </div>
            </div>
        </header>

    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

            <?php
                if(SessionHandling::get('userType'))
                {
                    if(SessionHandling::get('userType') == ADMIN)
                    {
                        include "menus/superadmin_menu.php";
                    }
                    elseif (SessionHandling::get('userType') == AGENT)
                    {
                        include "menus/agent_menu.php";
                    }
                    elseif (SessionHandling::get('userType') == DEVELOPER)
                    {
                        include "menus/agent_menu.php";
                    }
                }
           ?>
            <!-- BEGIN: Aside Menu -->



            <!-- END: Aside Menu -->
        </div>

        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">