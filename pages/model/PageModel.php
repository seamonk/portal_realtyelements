<?php

/**
 * Created by PhpStorm.
 * User: maxwellchristian
 * Date: 26/09/18
 * Time: 12:40 PM
 */
class PageModel extends Model
{

    public $pageName ;
    public $pageTitle ;

    /**
     * Page_Model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getPageName()
    {
        return $this->pageName;
    }

    /**
     * @param mixed $pageName
     */
    public function setPageName($pageName)
    {
        $this->pageName = $pageName;
    }

    /**
     * @return mixed
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param mixed $pageTitle
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }


}