<?php

// URLS
require "./commons/Urls.php" ;

// Database
require "./commons/" . "Database.php" ;

// Master Class
require "./commons/" . "Portal.php" ;

// Common MVC
require "./commons/" . "Controller.php" ;
require "./commons/" . "Model.php" ;
require "./commons/" . "View.php" ;

// Session Handling
require "./commons/" . "session_handling/SessionHandling.php";

// Master Object
$portal = new Portal();
//$portal->show("dashboard/controller/dashboard.php");
//$portal->show("login/controller/login.php");

?>