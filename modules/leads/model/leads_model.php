<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class leads_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();         
        }
        return 0;
    }

   

    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE ".$whereId." = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // public function getleadsDetails()
    // {

    //   $loggedId = SessionHandling::get('loggedId');
    //   // print_r($loggedId);
    //   // exit();
    //     $sth = $this->db->prepare("SELECT
    //                                     lead.nLeadIDPK,
    //                                     lead.nUserIDFK,
    //                                     lead.tCustomerName,
    //                                     lead.vCustomerrMobileNo,
    //                                     lead.tCustomerEmailId,
    //                                     lead.dtCreatedOn,
    //                                     lead.bIsRemoved,
    //                                     prop.tPropertyName,
    //                                     prop.nPropertyIDPK
    //                                 FROM
    //                                     tblleads as lead
    //                                     LEFT JOIN tblproperties as prop
    //                                     ON (lead.nPropertyIdFK = prop.nPropertyIDPK AND lead.nUserIDFK = prop.nCreatedBy)
    //                                 WHERE
    //                                     lead.bIsRemoved = 0 AND lead.nUserIDFK = 2
    //                                 ORDER BY
    //                                     lead.dtCreatedOn DESC
    //                                   ");

    //         // print_r($sth);
    //         // exit();
    //     $sth->execute();
    //     $row = $sth->fetchAll();
    // // print_r($row);
    // // exit(); 
    //     if (!empty($row)) {
    //         return $row;
    //     } else {
    //         return NULL;
    //     }
    // }

    public function getleadsDetails()
    {
        $loggedId = SessionHandling::get('loggedId');
        $requestData= $_REQUEST;
        $whereSql = "";
        // print_r($_GET['selectLeadType']);
        // exit();

        if ($requestData['selectLeadType'] == 1) 
        {
            $columns = array( 

            0=> '',
            1=> 'lead.tCustomerName',
            2=> 'lead.vCustomerrMobileNo',
            3=> 'lead.tCustomerEmailId',
            4=> 'prop.tPropertyName',
            5=> 'lead.dtCreatedOn',
            );

            $whereSql = "AND lead.nPropertyIdFK = prop.nPropertyIDPK ";
        }
        else if($requestData['selectLeadType'] == 0)
        {
            $columns = array( 

            0=> '',
            1=> 'lead.tCustomerName',
            2=> 'lead.vCustomerrMobileNo',
            3=> 'lead.tCustomerEmailId',
            4=> 'lead.dtCreatedOn',

            );
            $whereSql = "AND lead.nPropertyIdFK IS NULL";
        }
        
       $sql = "SELECT
                lead.nLeadIDPK,
                lead.nUserIDFK,
                lead.tCustomerName,
                lead.vCustomerrMobileNo,
                lead.tCustomerEmailId,
                lead.dtCreatedOn,
                lead.bIsRemoved,
                prop.tPropertyName,
                prop.nPropertyIDPK
            FROM
                tblleads as lead
                LEFT JOIN tblproperties as prop
                ON (lead.nPropertyIdFK = prop.nPropertyIDPK AND lead.nUserIDFK = prop.nCreatedBy)
            WHERE
                lead.bIsRemoved = 0 AND lead.nUserIDFK =  $loggedId ".$whereSql."";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (lead.dtCreatedOn LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR lead.tCustomerName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR lead.vCustomerrMobileNo LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR lead.tCustomerEmailId LIKE '%".$requestData['search']['value']."%'"; 
            $sql.=" OR prop.tPropertyName LIKE '%".$requestData['search']['value']."%' )"; 
        }
        $sth = $this->db->prepare($sql);
        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        // print_r($sql);
        // exit();

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {       
            $nestedData=array();

            $nestedData[] = $i;
            $nestedData[] = $row['tCustomerName'];
            $nestedData[] = $row['vCustomerrMobileNo'];
            $nestedData[] = $row['tCustomerEmailId'];
            $nestedData[] = $row['dtCreatedOn'];

            if ($requestData['selectLeadType'] == 1) 
            {
                $nestedData[] = "<td>
                                <form target='_blank' action='".Urls::$WEBSITE_BASE_URL."property/details' method='POST' class='m-form'>
                                    <input type='hidden' name='propertyId' id='propertyId' value='".$row['nPropertyIDPK']."'>
                                    <input type='hidden' name='userId' id='userId' value='".$row['nUserIDFK']."'>
                                   <button type='submit' style='background-color: white !important;'  data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Show Property' class='btn_view btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i style='color: #575962;' class='fa fa-eye'></i></button>
                                </form>
                            </td>   ";
            }
            else{
                $nestedData[] = "<td>
                                <form target='_blank' action='".Urls::$WEBSITE_BASE_URL."property/details' method='POST' class='m-form'>
                                    <input type='hidden' name='propertyId' id='propertyId' value='".$row['nPropertyIDPK']."'>
                                    <input type='hidden' name='userId' id='userId' value='".$row['nUserIDFK']."'>
                                   <button type='submit' disabled style='cursor: not-allowed;background-color: white !important;'  data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Show Property' class='btn_view btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i style='color: #575962;' class='fa fa-eye'></i></button>
                                </form>
                            </td>   ";
            }            
            // $nestedData[] = "
            //                 ".$Controll."
            //                 <form method='post' action='".Urls::$BASE."agent/view_Agent' style='display: inline-table;'>
            //                     <input type='hidden' name='agentID' value='".$row['nUserIDPK']."'>
            //                     <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tUserName']." Detail' data-original-title='View ".$row['tUserName']." Detail' type='button' class='btn_view btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i class='fa fa-eye'></i></button>
            //                 </form>
            //                 <form style='display: inline-table;'>
            //                     <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nAgentIDPK'].",".$row['nUserIDPK'].")'><i class='fa fa-trash-alt'></i></button>
            //                 </form>
            //                 ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }

}