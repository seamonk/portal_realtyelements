<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Leads extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
//        Session::init();
    }

    public function index()
    {
        $name = 'modules/leads/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        // $id = $_POST['userID'];
        // print_r($id);
        // exit();
        // $data = $this->model->getleadsDetails();
        // print_r($data);
        // exit();
        // $this->view->data = $data;

        $this->view->render($name, "Leads Listing", $header, $footer);
    }

    public function getAllleadsDetails()
    {
        // print_r($_GET['selectLeadType']);
        // exit();
        $data = $this->model->getleadsDetails();
        $this->view->data = $data;
    }

   

}