

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                               All Leads Details
                            </h3>
                        </div>
                    </div>
                    
                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Contact No</th>
                                <th>Email ID</th>
                                <th>Property Name</th>
                                <!-- <th>Actions</th> -->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            // print_r($this->data);
                            if (!empty($this->data)) {

                                for ($i = 0; $i < count($this->data); $i++) {
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        <td><?php echo $this->data[$i]['tCustomerName']; ?></td>
                                        <td><?php echo $this->data[$i]['vCustomerrMobileNo']; ?></td>
                                        <td><?php echo $this->data[$i]['tCustomerEmailId']; ?></td>
                                        <td><?php echo $this->data[$i]['tPropertyName']; ?></td>
                                        
                                       <!-- <td><?php echo $this->data[$i]['dtCreatedOn']; ?></td> -->
                                        
                                    </tr>
                                    <?php
                                }
                            }

                            ?>
                        </tbody>
                       </table></div></div></div></div></div></div>

                       <script type="text/javascript">
                               $(document).ready(function () {

        $('#s1').DataTable({
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "columnDefs": [
                {"width": "5%", "targets": 0}
            ]
        });

    });
                       </script>