

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                               All Leads Details
                            </h3>
                        </div>
                    </div>
                    
                </div>
                <div class="m-portlet__body">
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Select Leads Type</label>
                            <select style="width: 100%;" id="selectLeadType" name="selectLeadType" class="form-group m-select2">
                               <!--  <option value="0">Agent Leads</option> -->
                               <?php
                                    if (SessionHandling::get('userType') == AGENT)
                                    {
                                        echo '<option value="0">Agent Leads</option>';
                                    }
                               ?>
                                <option value="1">Property Leads</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead>
                            <tr id="AddRow">
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Contact No</th>
                                <th>Email ID</th>
                                <th>Inquiry On</th>
                                <th id="RemoveRow">Agent Leads</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                       </table>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#selectLeadType').select2();
       
        loadDatatable();

    });
    $(document).on('change', '#selectLeadType', function() 
    {
        loadDatatable();
    });

    // $('#selectLeadType').on('change',function()
    // {   
    //     var data = $('#selectLeadType :selected').val();
    //     $('#AddRow').html('');
    //     ale
    //     if (data == 0) 
    //     {

    //         $('#AddRow').html('<th>#</th>\
    //                             <th>Full Name</th>\
    //                             <th>Contact No</th>\
    //                             <th>Email ID</th>\
    //                             <th>Inquiry On</th>');
    //     }
    //     else
    //     {
    //         $('#AddRow').html('<th>#</th>\
    //                             <th>Full Name</th>\
    //                             <th>Contact No</th>\
    //                             <th>Email ID</th>\
    //                             <th>Inquiry On</th>\
    //                             <th>Property Name</th>');
    //     }
    //     loadDatatable();
    // });

    function loadDatatable() 
    {   
        var data = $('#selectLeadType :selected').val();
        if (data == 0) 
        {
            $('#RemoveRow').html("Agent Leads");
            // $('#AddRow').html('<th>#</th>\
            //                     <th>Full Name</th>\
            //                     <th>Contact No</th>\
            //                     <th>Email ID</th>\
            //                     <th>Inquiry On</th>');
        }
        else if(data == 1)
        {
           $('#RemoveRow').html("Property Leads");
            // $('#AddRow').html('<th>#</th>\
            //                     <th>Full Name</th>\
            //                     <th>Contact No</th>\
            //                     <th>Email ID</th>\
            //                     <th>Inquiry On</th>\
            //                     <th>Property Name</th>');
        }
        console.log(data);

        $('#s1').DataTable(
        {
            "ajax": "<?php echo Urls::$BASE ?>leads/getAllleadsDetails?selectLeadType="+data,
            "type": "GET",
            "processing": true,
            "serverSide": true,
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]} 
                // {"orderable": false,"targets": [0,8]}                   
            ],
            "order":[[1,"asc"]],
            "destroy": true
        });
        
    }

</script>