<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Reports extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
//        Session::init();
    }

    public function user_reports()
    {
        $name = 'modules/reports/view/user_reports.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getUsers();
        $this->view->data = $data;

        $this->view->render($name,"User Report", $header, $footer);
    }
}