<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class reports_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();         
        }
        return 0;
    }

    
    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE ".$whereId." = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getUsers()
    {
        $sth = $this->db->prepare("SELECT u.nUserIDPK, u.tUserName, u.vUserEmailID, u.nUserPhoneNumber, u.tUserProfileImgPath, u.bIsActive, ut.nUserTypeName, u.vUserFirstName, u.vUserLastName FROM tblusers as u,tblusertype as ut WHERE  u.bIsUserRemoved = 0 AND u.nIsUserDummyData = 0 AND u.nUserTypeIDFK = ut.nUserTypeIDPK AND u.nUserTypeIDFK !=1");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }    
    }
}