<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    table.dataTable td {
      white-space: nowrap;
    }
    .buttons-pdf{background-color: transparent !important; border-color: none !important;}
    .btn.btn-default, .btn.btn-secondary {
        background: white;
        border-color: white !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="fa fa-user"></i> Users Reports</h3>
                            </div>
                            
                            <!-- <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Download User Report">
                                <i class="fa fa-download"></i>
                                
                            </a> -->
                            <div id="buttons"></div>

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <!-- <th>Image</th> -->
                                        <th>User Type</th>
                                        <th>Name </th>
                                        <th>Phone Number</th>
                                        <th>Email</th>
                                       
                                    </tr>   
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($this->data)) 
                                    {
                                        for ($i = 0; $i < count($this->data); $i++) 
                                        {
                                            ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td> 
                                            
                                            <!-- <td style="text-align: center;">
                                                <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$this->data[$i]['tUserProfileImgPath']; ?>"
                                                    style="height: 4em;width: 4em; box-shadow: 1px 0px 9px #060606;">  -->
                                            </td>
                                            <td><?php echo $this->data[$i]['nUserTypeName']; ?></td>
                                            <td><?php echo $this->data[$i]['vUserFirstName'].' '.$this->data[$i]['vUserLastName']; ?></td>
                                            <!-- <td><?php echo $this->data[$i]['tUserName']; ?></td> -->
                                            <td><?php echo $this->data[$i]['nUserPhoneNumber']; ?></td>
                                            <td><?php echo $this->data[$i]['vUserEmailID']; ?></td>
                                            
                                        </tr>
                                        <?php
                                        }
                                    }
                                    ?>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() {
        
         var table = $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [{ "width": "5%", "targets": 0 }],
           
        } ); 



        var buttons = new $.fn.dataTable.Buttons(table, 
        {
            "buttons": [
            {
                text: '<a href="#" class="btn m-btn m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Download User Report"><i class="fa fa-download"></i></a>',
                extend: 'pdf',
                title: 'User_Report',
                //titleAttr: 'Download User Report',
                extension: '.pdf'
            }]
        }).container().appendTo($('#buttons'));
    });
</script>
