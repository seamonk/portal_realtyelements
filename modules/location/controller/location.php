<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Location extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set("Asia/Kolkata");
        }
//        Session::init();
    }

    public function index() // fetch all countries
    {
        $name = 'modules/location/view/country.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        // $data = $this->model->getCountries();
        // $this->view->data = $data;
        $this->view->render($name, "Countries", $header, $footer);
    }
    public function getCountries()
    {
        $data = $this->model->getCountries();
        $this->view->data = $data;
    }
    public function getStates()
    {
       $data = $this->model->getStates();  // listing for a datatable
       $this->view->data = $data;
    }
    public function ShowInHeaderCityCount()
    {
        $data = $this->model->getShowInHeaderCityCount();  // listing for a datatable
       echo json_encode($data);
    }
    public function createCountry() // insert country
    {
        $loggedId = SessionHandling::get("loggedId"); 

        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                $field = array('tCountryCode', 'tCountryName','nCountryCreatedBy');
                $value = array($countryCode, $countryName,$loggedId);

                $countryData = array_combine($field, $value);

                $countryID = $this->model->insert($countryData, 'tblcountries');

                if (isset($countryID) && $countryID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Country");
                    header("Location:../location/");
                } else {
                    SessionHandling::set("suc_msg", "Country added successully");
                    header("Location:../location/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update country
            extract($_POST);

            $id = array('nCountryIDPK' => $countryID);

            $field = array('tCountryCode', 'tCountryName');
            $value = array($countryCode, $countryName);

            $countryData = array_combine($field, $value);

            $countryID = $this->model->update($countryData, 'tblcountries', $id);

            if (isset($countryID) && $countryID > 0) {
                SessionHandling::set("suc_msg", "Country updated successully");
                header("Location:../location/");
            } else if (isset($countryID) && $countryID == 0) {
                SessionHandling::set("suc_msg", "Country updated successully");
                header("Location:../location/");
            } else {
                SessionHandling::set("err_msg", "Error while updating country");
                header("Location:../location/");
            }
        }
    }

    public function editCountry()   // edit country
    {
        $name = 'modules/location/view/country.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $editCountryID = intval($editCountryID);

        $singleCountryData = $this->model->singleCountry($editCountryID);

        $this->view->singleCountry = $singleCountryData;

        $this->view->render($name, "Country Update", $header, $footer);
    }

    public function deleteCountry() // delete country
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        $countryID = $_POST['id'];

        $id = array('nCountryIDPK' => $countryID);

        $field = array('bIsCountryRemoved','nCountryRemovedBy','dtCountryRemovedOn');
        $value = array(1,$loggedId,$currentDate);
        $countryData = array_combine($field, $value);

        $res = $this->model->update($countryData, 'tblcountries', $id);

        if ($res > 0) {
            
            echo "true";
        } else {
            
            echo "false";
        }
    }

    public function state() // load the state view
    {
        $name = 'modules/location/view/state.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        // $data = $this->model->getStates();  // listing for a datatable
        // $this->view->data = $data;
        $this->view->render($name, "States", $header, $footer);
    }

    public function getCountry()
    {
        $data = $this->model->fetchAllCountry();
        echo json_encode($data);
    }

    public function createState()   // insert state
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                $field = array('tStateName', 'nCountryIDFK','nStateCreatedBy');
                $value = array($stateName, $country, $loggedId);

                $stateData = array_combine($field, $value);

                $stateID = $this->model->insert($stateData, 'tblstates');

                if (isset($stateID) && $stateID == 0) {
                    SessionHandling::set("err_msg", "Error while adding State");
                    header("Location:../location/state");
                } else {
                    SessionHandling::set("suc_msg", "State added successully");
                    header("Location:../location/state");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update country
            extract($_POST);

            $id = array('nStateIDPK' => $stateID);

            $field = array('tStateName', 'nCountryIDFK');
            $value = array($stateName, $country);

            $stateData = array_combine($field, $value);

            $stateID = $this->model->update($stateData, 'tblstates', $id);

            if (isset($stateID) && $stateID > 0) {
                SessionHandling::set("suc_msg", "State updated successully");
                header("Location:../location/state");
            } else if (isset($stateID) && $stateID == 0) {
                SessionHandling::set("suc_msg", "State updated successully");
                header("Location:../location/state");
            } else {
                SessionHandling::set("err_msg", "Error while updating State");
                header("Location:../location/state");
            }
        }
    }

    public function editState()   // edit state
    {
        $name = 'modules/location/view/state.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $editStateID = intval($editStateID);

        $singleStateData = $this->model->singleState($editStateID);
        $this->view->singleState = $singleStateData;

     
        $this->view->render($name, "State Update", $header, $footer);
    }

    public function deleteState()   // delete state
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        $stateID = $_POST['id'];

        $id = array('nStateIDPK' => $stateID);

        $field = array('bIsStateRemoved','nStateRemovedBy','dtStateRemovedOn');
        $value = array(1,$loggedId,$currentDate);
        $stateData = array_combine($field, $value);

        $res = $this->model->update($stateData, 'tblstates', $id);

        if ($res > 0) {
           
            echo "true";
        } else {
            
            echo "false";
        }
    }

    public function city() // load the city view
    {
        $name = 'modules/location/view/city.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';


        $this->view->render($name, "City", $header, $footer);
    }
    public function getCities()
    {
       
        $data = $this->model->getCities();  // listing for a datatable
        $this->view->data = $data;
    }

    public function getState()  // append in city form
    {
        $id = $_POST['countryID'];

        $stateList = $this->model->fetchState($id);
        echo json_encode($stateList);

    }
    public function getlocality()
    {
        $data = $this->model->getlocality();  // listing for a datatable
        $this->view->data = $data;
    }
    public function getAllLocalities()
    {
        $id = $_POST['areaID'];
        $data = $this->model->getAllLocalities($id);
        echo json_encode($data);  // listing for a datatable
       
    }

    public function createCity()   // insert city
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        if ($_POST['submit'] == "submit") 
        {
            if (isset($_POST) && !empty($_POST)) 
            {
                $preImgText = date('ymdhsi');

                extract($_POST);
                $files = $_FILES['city_image']['name'];

                $extension = pathinfo($files);


                if($_FILES['city_image']['name'] != "")
                {
                    $img = $preImgText.".".$extension['extension'];
                }
                else
                {
                    $img = "";
                }

                $targetPath = Urls::$CITY_IMAGE_UPLAODS.$img;

                $field = array('tCityName','tCityImagePath','nStateIDFK','nCityCreatedBy');
                $value = array($cityName,$img,$state, $loggedId);

                move_uploaded_file($_FILES["city_image"]["tmp_name"], $targetPath);

                $cityData = array_combine($field, $value);

                // print_r($cityData);
                // exit();

                $cityID = $this->model->insert($cityData, 'tblcities');

                if (isset($cityID) && $cityID == 0) {
                    SessionHandling::set("err_msg", "Error while adding City");
                    header("Location:../location/city");
                } else {
                    SessionHandling::set("suc_msg", "City added successully");
                    header("Location:../location/city");
                }
            }
        } else if ($_POST['submit'] == "update") 
        {  // update country
            extract($_POST);

            $id = array('nCityIDPK' => $cityID);
            $preImgText = date('ymdhsi');
           
            if($_FILES['city_image']['name'] != "")
            {
                $files = $_FILES['city_image']['name'];
                $extension = pathinfo($files);
                $img = $preImgText.".".$extension['extension'];

                $field = array('tCityName','tCityImagePath', 'nStateIDFK');
                $value = array($cityName,$img, $state);
            }
            else
            {
                $img = "";
                $field = array('tCityName', 'nStateIDFK');
                $value = array($cityName, $state);
            }

            $targetPath = Urls::$CITY_IMAGE_UPLAODS.$img;

            move_uploaded_file($_FILES["city_image"]["tmp_name"], $targetPath);

            $cityData = array_combine($field, $value);
            // print_r($cityData);
            // exit();

            $cityID = $this->model->update($cityData, 'tblcities', $id);

            if (isset($cityID) && $cityID > 0) {
                SessionHandling::set("suc_msg", "City updated successully");
                header("Location:../location/city");
            } else if (isset($cityID) && $cityID == 0) {
                SessionHandling::set("suc_msg", "City updated successully");
                header("Location:../location/city");
            } else {
                SessionHandling::set("err_msg", "Error while updating City");
                header("Location:../location/city");
            }
        }
    }
     public function ShowInHeader()
    {
        extract($_POST);
        // print_r($_POST);
        // exit();
        $Cityid = array('nCityIDPK' => $_POST['id']);

        $field = array('bShowInHeader');

        $value = array($status);

        $dataUpdate = array_combine($field, $value);


       $res =  $this->model->update($dataUpdate, 'tblcities',$Cityid);
        if($res > 0)
        {
            echo "true";     
        }
        else if($res == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        } 
    }

    public function editCity()   
    {
        $name = 'modules/location/view/city.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $editCityID = intval($editCityID);



        $singleCityData = $this->model->singleCity($editCityID);

        $this->view->singleCity = $singleCityData;

        // $data = $this->model->getCities();  // listing for a datatable
        // $this->view->data = $data;

        $this->view->render($name, "City Update", $header, $footer);
    }
/*
    public function getStateAll()
    {
        $allStates = $this->model->fetchAllStates();
        echo json_encode($allStates);
    }*/

    public function deleteCity()
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        $cityID = $_POST['id'];

        $id = array('nCityIDPK' => $cityID);

        $field = array('bIsCityRemoved','nCityRemovedBy','dtCityRemovedOn');
        $value = array(1, $loggedId,$currentDate);
        $cityData = array_combine($field, $value);

        $res = $this->model->update($cityData, 'tblcities', $id);

        if ($res > 0) {
            
            echo "true";
        } else {
           
            echo "false";
        }
    }

    public function area() // load the area view
    {
        $name = 'modules/location/view/area.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $this->view->render($name, "Area", $header, $footer);
    }
    public function getAreas()
    {
        $data = $this->model->getAreas();  // listing for a datatable
        $this->view->data = $data;

    }
    public function locality() // load the area view
    {
        $name = 'modules/location/view/locality.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

       
        
        $this->view->render($name, "Locality", $header, $footer);
    }

    public function getCity()  // append in area form
    {
        $id = $_POST['stateID'];

        $cityList = $this->model->fetchCity($id);
        echo json_encode($cityList);
    }
    public function fetchCarearCity()  // append in area form
    {
        

        $cityList = $this->model->fetchCarearCity();
        echo json_encode($cityList);
    }
    public function getArea()  // append in area form
    {
        $id = $_POST['cityID'];
        // print_r($_POST);
        // exit();
        $cityList = $this->model->fetchArea($id);
        echo json_encode($cityList);
    }

    public function createArea()   
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        if ($_POST['submit'] == "submit") 
        {
            if (isset($_POST) && !empty($_POST)) 
            {
                extract($_POST);

                $field = array('nCityIDFK', 'tAreaName','nAreaPincode','dAreaLatitude','dAreaLongitude','nAreaCreatedBy');
                $value = array($city, $areaName, $areaPincode, $areaLatitude, $areaLongitude, $loggedId);

                $areaData = array_combine($field, $value);

                $areaID = $this->model->insert($areaData, 'tblareas');

                if (isset($areaID) && $areaID == 0) 
                {
                    SessionHandling::set("err_msg", "Error while adding Areas");
                    header("Location:../location/area");
                } 
                else 
                {
                    SessionHandling::set("suc_msg", "Area added successully");
                    header("Location:../location/area");
                }
            }
        } 
        else if ($_POST['submit'] == "update") 
        {
            extract($_POST);

            $id = array('nAreaIDPK' => $areaID);

            $field = array('nCityIDFK', 'tAreaName','nAreaPincode','dAreaLatitude','dAreaLongitude');
            $value = array($city, $areaName, $areaPincode, $areaLatitude, $areaLongitude);

            $areaData = array_combine($field, $value);

            $areaID = $this->model->update($areaData, 'tblareas', $id);

            if (isset($areaID) && $areaID > 0) 
            {
                SessionHandling::set("suc_msg", "Area updated successully");
                header("Location:../location/area");
            } 
            else if (isset($areaID) && $areaID == 0) 
            {
                SessionHandling::set("suc_msg", "Area updated successully");
                header("Location:../location/area");
            } 
            else 
            {
                SessionHandling::set("err_msg", "Error while updating Area");
                header("Location:../location/area");
            }
        }
    }
   

    public function createNewArea()
    { 
        // print_r($_POST);
        // exit();
        $loggedId = SessionHandling::get("loggedId"); 

        if (isset($_POST) && !empty($_POST)) 
        {
            extract($_POST);

            $field = array('nCityIDFK', 'tAreaName','nAreaPincode','dAreaLatitude','dAreaLongitude','nAreaCreatedBy');
            $value = array($city, $areaName, $areaPincode, $areaLatitude, $areaLongitude, $loggedId);

            $areaData = array_combine($field, $value);
            // print_r($areaData);
            // exit();
            $areaID = $this->model->insert($areaData, 'tblareas');

            if($areaID > 0)
            {
                echo $areaID;   
            }
            else
            {
                echo "Error";     
            } 

            
        }
    }

    public function createLocality()   
    {
        // print_r($_POST);
        // exit();
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        if ($_POST['submit'] == "submit") 
        {
            if (isset($_POST) && !empty($_POST)) 
            {
                extract($_POST);

                $field = array('nAreaIDFK', 'nCityIDFK','nStateIDFK','tLocalityName','dLocalityLatitude','dLocalityLongitude','nLocalityCreatedBy');
                $value = array($area,$city,$state, $localityName, $localityLatitude, $localityLongitude, $loggedId);

                $areaData = array_combine($field, $value);
                // print_r($areaData);
                // exit();

                $areaID = $this->model->insert($areaData, 'tbllocalities');

                if (isset($areaID) && $areaID == 0) 
                {
                    SessionHandling::set("err_msg", "Error while adding Locality");
                    header("Location:../location/locality");
                } 
                else 
                {
                    SessionHandling::set("suc_msg", "Locality added successully");
                    header("Location:../location/locality");
                }
            }
        } 
        else if ($_POST['submit'] == "update") 
        {
            extract($_POST);

            $id = array('nLocalityIDPK' => $localityID);

            $field = array('nAreaIDFK', 'nCityIDFK','nStateIDFK','tLocalityName','dLocalityLatitude','dLocalityLongitude');
                $value = array($area,$city,$state, $localityName, $localityLatitude, $localityLongitude);

            $areaData = array_combine($field, $value);
            // print_r($areaData);
            // exit();
            $areaID = $this->model->update($areaData, 'tbllocalities', $id);

            if (isset($areaID) && $areaID > 0) 
            {
                SessionHandling::set("suc_msg", "Locality updated successully");
                header("Location:../location/locality");
            } 
            else if (isset($areaID) && $areaID == 0) 
            {
                SessionHandling::set("suc_msg", "Locality updated successully");
                header("Location:../location/locality");
            } 
            else 
            {
                SessionHandling::set("err_msg", "Error while updating Locality");
                header("Location:../location/locality");
            }
        }
    }
    public function createNewLocality()   
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        if (isset($_POST) && !empty($_POST)) 
        {
            extract($_POST);

            $field = array('nAreaIDFK', 'nCityIDFK','nStateIDFK','tLocalityName','dLocalityLatitude','dLocalityLongitude','nLocalityCreatedBy');
            $value = array($areaID,$cityID,$stateID, $LocalityName, $areaLatitude, $areaLongitude, $loggedId);

            $areaData = array_combine($field, $value);
           
            $areaID = $this->model->insert($areaData, 'tbllocalities');

            if($areaID > 0)
            {
                echo $areaID;   
            }
            else
            {
                echo "Error";     
            } 
        }   
    }

    public function deleteArea()
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        $areaID = $_POST['id'];

        $id = array('nAreaIDPK' => $areaID);

        $field = array('bIsAreaRemoved','nAreaRemovedBy','dtAreaRemovedOn');
        $value = array(1, $loggedId,$currentDate);

        $areaData = array_combine($field, $value);

        $res = $this->model->update($areaData, 'tblareas', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Area deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Area");
            echo "false";
        }
    }
    public function deletelocality()
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $currentDate = date('Y-m-d H:i:s');

        $localityID = $_POST['id'];
        // print_r($localityID);
        // exit();
        $id = array('nLocalityIDPK' => $localityID);

        $field = array('bIsLocalityRemoved');
        $value = array(1);

        $areaData = array_combine($field, $value);
        
        $res = $this->model->update($areaData, 'tbllocalities', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Locality deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Locality");
            echo "false";
        }
    }

    public function editlocalityArea()   
    {
        $name = 'modules/location/view/area.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $editAreaID = intval($editAreaID);

        $singleAreaData = $this->model->singleArea($editAreaID);
        $this->view->singleArea = $singleAreaData;

        $this->view->render($name, "Area Update", $header, $footer);
    }
    public function editlocality()   
    {
        $name = 'modules/location/view/locality.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $editlocalityID = intval($editlocalityID);

        $singleLocality = $this->model->singleLocality($editlocalityID);
        $this->view->singlelocality = $singleLocality;

        $this->view->render($name, "Area Update", $header, $footer);
    }

    public function checkForLocation()
    {
        extract($_POST);

        $locationData = $this->model->checkForLocation($country,$state,$city,$area,$route,$pincode,$latitude,$longitude);

        echo json_encode($locationData);

        // $this->view->singlelocality = $singleLocality;

    }
}