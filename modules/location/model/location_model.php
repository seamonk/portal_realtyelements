<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class location_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    
    public function delete($id, $table, $whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE " . $whereId . " = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }


    public function getCountries()  // fetch all countries for datatable
    {   
        $requestData= $_REQUEST;
       
        $columns = array( 

            0=> 'nCountryIDPK',
            1=> 'tCountryName',
            2=> 'tCountryCode',
            // 7=> 'agent.bIsAgentFeatured',
            
        );
       $sql = "SELECT 
                    `nCountryIDPK`, 
                    `tCountryName`, 
                    `tCountryCode`
                     FROM 
                        `tblcountries`
                     WHERE 
                        `bIsCountryRemoved` = 0 
                    AND 
                        bIsCountryDummyData = 0 
                                        ";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (tCountryName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR tCountryCode LIKE '%".$requestData['search']['value']."%' "; 
        }
        $sth = $this->db->prepare($sql);

        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {   
            $nestedData=array();

            $nestedData[] = $i; 
           
            $nestedData[] = $row['tCountryName'];
            $nestedData[] = $row['tCountryCode'];
            $nestedData[] = "
                            <form method='post' action='".Urls::$BASE."location/editCountry' style='display: inline-table;'>
                                <input type='hidden' name='editCountryID' value='".$row['nCountryIDPK']."'>
                                <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tCountryName']." Detail' data-original-title='View ".$row['tCountryName']." Detail' type='submit' style='background-color: white !important;border-color: #f4516c !important;' class='btn_edit btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i style='color: #f4516c !important;' class='fa fa-pencil-alt'></i></button>
                            </form>
                            <form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nCountryIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                            </form>
                            ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }
    public function getStates()  // fetch all States for datatable
    {   
        $requestData= $_REQUEST;
       
        $columns = array( 

            0=> 's.nStateIDPK',
            1=> 's.tStateName',
            2=> 'c.tCountryName',
            // 7=> 'agent.bIsAgentFeatured',
            
        );
       $sql = "SELECT 
                s.nStateIDPK,
                s.tStateName,
                c.tCountryName 
                FROM tblstates as s,
                    tblcountries as c
                WHERE 
                    s.bIsStateRemoved = 0 
                AND 
                    s.bIsStateDummyData = 0
                 AND 
                    s.nCountryIDFK = c.nCountryIDPK
                                        ";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (s.tStateName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR c.tCountryName LIKE '%".$requestData['search']['value']."%' "; 
        }
        $sth = $this->db->prepare($sql);

        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {   
            $nestedData=array();

            $nestedData[] = $i; 
           
            $nestedData[] = $row['tStateName'];
            $nestedData[] = $row['tCountryName'];
            $nestedData[] = "
                            <form method='post' action='".Urls::$BASE."location/editState' style='display: inline-table;'>
                                <input type='hidden' name='editStateID' value='".$row['nStateIDPK']."'>
                                <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tStateName']." Detail' data-original-title='View ".$row['tStateName']." Detail' type='submit' style='background-color: white !important;border-color: #f4516c !important;' class='btn_edit btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i style='color: #f4516c !important;' class='fa fa-pencil-alt'></i></button>
                            </form>
                            <form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nStateIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                            </form>
                            ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }
    public function getCities()  // fetch all Cities for datatable
    {   
        $requestData= $_REQUEST;
       
        $columns = array( 

            0=> '',
            1=> 'ct.tCityName',
            2=> 's.tStateName',
            3=> 'c.tCountryName',
            4=> 'ct.tCityImagePath',
            5=> 'ct.bShowInHeader',
            
        );
       $sql = "SELECT 
                    ct.nCityIDPK, 
                    ct.tCityName,
                    s.tStateName,
                    c.tCountryName,
                    ct.tCityImagePath,
                    ct.bShowInHeader
                    FROM 
                        tblcities as ct,
                        tblstates as s,
                        tblcountries as c 
                    WHERE
                        ct.nStateIDFK = s.nStateIDPK 
                        AND 
                        s.nCountryIDFK = c.nCountryIDPK 
                        AND 
                        ct.bIsCityRemoved = 0 
                        AND 
                        s.bIsStateRemoved = 0 
                        AND 
                        c.bIsCountryRemoved = 0 
                        AND 
                        ct.bIsCityDummyData = 0
                                        ";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (ct.tCityName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR s.tStateName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" or c.tCountryName LIKE '%".$requestData['search']['value']."%')"; 
        }
        $sth = $this->db->prepare($sql);

        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {   
            $nestedData=array();

            if (empty($row['tCityImagePath'])) 
            {
               $row['tCityImagePath'] = "no-image.png";
            }
            $nestedData[] = $i; 
            $nestedData[] = "<img width='32px;' src='".Urls::$BASE.Urls::$CITY_IMAGE_UPLAODS.$row['tCityImagePath']."'/>";
            $nestedData[] = $row['tCityName'];
            $nestedData[] = $row['tStateName'];
            $nestedData[] = $row['tCountryName'];
            if ($row['bShowInHeader'] == 1) 
            {
                $nestedData[] = "<span class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                    <label>
                                        <input type='checkbox' name='verified' checked  id='".$row['nCityIDPK']."' onchange='ShowInHeader(".$row['nCityIDPK'].",0);'>
                                        <span ></span>
                                    </label>
                                </span>";
            }
            else{
                 $nestedData[] = "<span class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                    <label>
                                        <input type='checkbox' name='verified' id='".$row['nCityIDPK']."' onchange=ShowInHeader(".$row['nCityIDPK'].",1);>
                                        <span ></span>
                                    </label>
                                </span>";
            }
            $nestedData[] = "
                            <form method='post' action='".Urls::$BASE."location/editCity' style='display: inline-table;'>
                                <input type='hidden' name='editCityID' value='".$row['nCityIDPK']."'>
                                <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tCityName']." Detail' data-original-title='View ".$row['tCityName']." Detail' type='submit' style='background-color: white !important;border-color: #f4516c !important;' class='btn_edit btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i style='color: #f4516c !important;' class='fa fa-pencil-alt'></i></button>
                            </form>
                            <form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nCityIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                            </form>
                            ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }
    public function getAreas()  // fetch all Area for datatable
    {   
        $requestData= $_REQUEST;
       
        $columns = array( 

            0=> '',
            1=> 'a.tAreaName',
            2=> 'a.nAreaPincode',
            3=> 'ct.tCityName',
            4=> 's.tStateName',
            5=> 'c.tCountryName',
            
        );
       $sql = "SELECT
                    a.nAreaIDPK,
                    a.tAreaName,
                    a.nAreaPincode,
                    ct.tCityName,
                    s.tStateName,
                    c.tCountryName
                FROM
                    tblareas AS a,
                    tblcities AS ct,
                    tblstates AS s,
                    tblcountries AS c
                WHERE
                    a.nCityIDFK = ct.nCityIDPK 
                    AND 
                    ct.nStateIDFK = s.nStateIDPK 
                    AND 
                    s.nCountryIDFK = c.nCountryIDPK 
                    AND 
                    ct.bIsCityRemoved = 0 
                    AND 
                    s.bIsStateRemoved = 0 
                    AND 
                    c.bIsCountryRemoved = 0 
                    AND
                    a.bIsAreaRemoved = 0 
                    AND
                    a.bIsAreaDummyData = 0
                                        ";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (a.tAreaName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR a.nAreaPincode LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR ct.tCityName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR s.tStateName LIKE '%".$requestData['search']['value']."%' ";
            $sql.=" or c.tCountryName LIKE '%".$requestData['search']['value']."%')"; 
        }
        $sth = $this->db->prepare($sql);

        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {   
            $nestedData=array();

            if (empty($row['tCityImagePath'])) 
            {
               $row['tCityImagePath'] = "no-image.png";
            }
            $nestedData[] = $i; 
            $nestedData[] = $row['tAreaName'];;
            $nestedData[] = $row['nAreaPincode'];
            $nestedData[] = $row['tCityName'];
            $nestedData[] = $row['tStateName'];
            $nestedData[] = $row['tCountryName'];
            $nestedData[] = "
                            <form method='post' action='".Urls::$BASE."location/editlocalityArea' style='display: inline-table;'>
                                <input type='hidden' name='editAreaID' value='".$row['nAreaIDPK']."'>
                                <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tAreaName']." Detail' data-original-title='View ".$row['tAreaName']." Detail' type='submit' style='background-color: white !important;border-color: #f4516c !important;' class='btn_edit btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i style='color: #f4516c !important;' class='fa fa-pencil-alt'></i></button>
                            </form>
                            <form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nAreaIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                            </form>
                            ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }
    public function getLocality()  // fetch all Area for datatable
    {   
        $requestData= $_REQUEST;
       
        $columns = array( 

            0=> '',
            1=> 'loc.tLocalityName',
            2=> 'a.tAreaName',
            3=> 'ct.tCityName',
            4=> 's.tStateName'
            
        );
       $sql = "SELECT
                                    loc.nLocalityIDPK,
                                    loc.nAreaIDFK,
                                    a.tAreaName,
                                    ct.tCityName,
                                    loc.nCityIDFK,
                                    s.tStateName,
                                    loc.nStateIDFK,
                                    loc.tLocalityName,
                                    loc.dLocalityLatitude,
                                    loc.dLocalityLongitude,
                                    loc.dLocalityCreatedOn,
                                    loc.nLocalityCreatedBy,
                                    loc.bIsLocalityRemoved
                                FROM
                                    tbllocalities AS loc,
                                    tblareas AS a,
                                    tblcities AS ct,
                                    tblstates AS s,
                                    tblcountries AS c
                                WHERE
                                        loc.nAreaIDFK = a.nAreaIDPK 
                                    AND 
                                        loc.nCityIDFK = ct.nCityIDPK 
                                    AND 
                                        ct.nStateIDFK = s.nStateIDPK 
                                    AND 
                                        s.nCountryIDFK = c.nCountryIDPK 
                                    AND 
                                        ct.bIsCityRemoved = 0 
                                    AND 
                                        s.bIsStateRemoved = 0 
                                    AND 
                                        c.bIsCountryRemoved = 0
                                        AND 
                                        loc.bIsLocalityRemoved = 0
                                        and
                                        a.bIsAreaRemoved = 0
                                        ";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (loc.tLocalityName  LIKE '%".$requestData['search']['value']."%' ";
            $sql.=" OR a.tAreaName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR ct.tCityName  LIKE '%".$requestData['search']['value']."%' ";
            $sql.=" or s.tStateName LIKE '%".$requestData['search']['value']."%')"; 
        }
        $sth = $this->db->prepare($sql);

        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {   
            $nestedData=array();

            
            $nestedData[] = $i; 
            $nestedData[] = $row['tLocalityName'];;
            $nestedData[] = $row['tAreaName'];
            $nestedData[] = $row['tCityName'];
            $nestedData[] = $row['tStateName'];
            $nestedData[] = "
                            <form method='post' action='".Urls::$BASE."location/editlocality' style='display: inline-table;'>
                                <input type='hidden' name='editlocalityID' value='".$row['nLocalityIDPK']."'>
                                <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tLocalityName']." Detail' data-original-title='View ".$row['tLocalityName']." Detail' type='submit' style='background-color: white !important;border-color: #f4516c !important;' class='btn_edit btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i style='color: #f4516c !important;' class='fa fa-pencil-alt'></i></button>
                            </form>
                            <form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nLocalityIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                            </form>
                            ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }


    public function singleCountry($editCountryID)   // fetch single country
    {
        $variableType = gettype($editCountryID);
     
        if($variableType == "integer")
        {
             $sth = $this->db->prepare("SELECT `nCountryIDPK`, `tCountryName`, `tCountryCode` FROM `tblcountries` WHERE `bIsCountryRemoved` = 0 AND `nCountryIDPK` = $editCountryID");
        }
        else
        {
             $sth = $this->db->prepare("SELECT `nCountryIDPK`, `tCountryName`, `tCountryCode` FROM `tblcountries` WHERE `bIsCountryRemoved` = 0 AND `tCountryName` = '$editCountryID'"); 
        }       

       
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function fetchAllCountry()   // fetch country for state form and append it to the dropdown
    {
        // SELECT `nCountryIDPK`, `tCountryName`, `tCountryCode`, `bIsRemoved` FROM `tblcountries` WHERE `bIsRemoved` = 0
        $sth = $this->db->prepare("SELECT `nCountryIDPK`, `tCountryName`, `tCountryCode`, `bIsCountryRemoved` FROM `tblcountries` WHERE `bIsCountryRemoved` = 0 AND bIsCountryDummyData = 0");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAllLocalities($id)   // fetch country for state form and append it to the dropdown
    {
        // SELECT `nCountryIDPK`, `tCountryName`, `tCountryCode`, `bIsRemoved` FROM `tblcountries` WHERE `bIsRemoved` = 0
        $sth = $this->db->prepare("SELECT
                                    loc.nLocalityIDPK,
                                    loc.nAreaIDFK,
                                    a.tAreaName,
                                    ct.tCityName,
                                    loc.nCityIDFK,
                                    s.tStateName,
                                    loc.nStateIDFK,
                                    loc.tLocalityName,
                                    loc.dLocalityLatitude,
                                    loc.dLocalityLongitude,
                                    loc.dLocalityCreatedOn,
                                    loc.nLocalityCreatedBy,
                                    loc.bIsLocalityRemoved
                                FROM
                                    tbllocalities AS loc,
                                    tblareas AS a,
                                    tblcities AS ct,
                                    tblstates AS s,
                                    tblcountries AS c
                                WHERE
                                        loc.nAreaIDFK = a.nAreaIDPK 
                                    AND 
                                        loc.nCityIDFK = ct.nCityIDPK 
                                    AND 
                                        ct.nStateIDFK = s.nStateIDPK 
                                    AND 
                                        s.nCountryIDFK = c.nCountryIDPK 
                                    AND 
                                        ct.bIsCityRemoved = 0 
                                    AND 
                                        s.bIsStateRemoved = 0 
                                    AND 
                                        c.bIsCountryRemoved = 0
                                        AND 
                                        loc.bIsLocalityRemoved = 0
                                        and
                                        a.bIsAreaRemoved = 0
                                        AND
                                        loc.nAreaIDFK = $id");
        // echo "<pre>";
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function singleState($editStateID)   // fetch single state
    {
        $variableType = gettype($editStateID);
     
        if($variableType == "integer")
        {
            $sth = $this->db->prepare("SELECT `nStateIDPK`, `tStateName`, `nCountryIDFK`, `bIsStateRemoved` FROM `tblstates` WHERE `nStateIDPK` = $editStateID AND `bIsStateRemoved` = 0");
        }
        else
        {
              $sth = $this->db->prepare("SELECT `nStateIDPK`, `tStateName`, `nCountryIDFK`, `bIsStateRemoved` FROM `tblstates` WHERE `tStateName` = '$editStateID' AND `bIsStateRemoved` = 0"); 
        }       
      
       
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function fetchState($id) // fetch state for city form and append it to the dropdown dependent
    {
        $sth = $this->db->prepare("SELECT `nStateIDPK`, `tStateName`, `nCountryIDFK`, `bIsStateRemoved` FROM `tblstates` WHERE `bIsStateRemoved` = 0 AND `nCountryIDFK` = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    // fetchArea
    public function fetchArea($id) // fetch state for city form and append it to the dropdown dependent
    {
        $sth = $this->db->prepare("SELECT
                                      `nAreaIDPK`,
                                      `nCityIDFK`,
                                      city.tCityName,
                                      `tAreaName`
                                     
                                    FROM
                                      tblareas as area,
                                      tblcities as city
                                    WHERE
                                    area.nCityIDFK = city.nCityIDPK
                                    AND
                                    `bIsAreaRemoved` = 0 AND `nCityIDFK` = $id ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function singleCity($id)
    {
        $variableType = gettype($id);
     
        if($variableType == "integer")
        {
            $sth = $this->db->prepare("SELECT ct.nCityIDPK, ct.tCityName,ct.nStateIDFK,s.tStateName,s.nCountryIDFK,ct.tCityImagePath,c.tCountryName FROM tblcities as ct,tblstates as s,tblcountries as c WHERE ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.nCityIDPK = $id AND ct.bIsCityRemoved = 0 AND c.bIsCountryRemoved = 0 AND s.bIsStateRemoved = 0");
        }
        else
        {
            $sth = $this->db->prepare("SELECT ct.nCityIDPK, ct.tCityName,ct.nStateIDFK,s.tStateName,s.nCountryIDFK,ct.tCityImagePath,c.tCountryName FROM tblcities as ct,tblstates as s,tblcountries as c WHERE ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.tCityName = '$id' AND ct.bIsCityRemoved = 0 AND c.bIsCountryRemoved = 0 AND s.bIsStateRemoved = 0");
        } 

     
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function fetchAllStates()    // fetch all states
    {
        $sth = $this->db->prepare("SELECT s.nStateIDPK,s.tStateName,c.tCountryName FROM tblstates as s,tblcountries as c WHERE s.bIsStateRemoved = 0 AND s.nCountryIDFK = c.nCountryIDPK ORDER BY s.tStateName ASC");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    
    // singleLocality
    public function singleLocality($id)
    {
        $variableType = gettype($id);
     
        if($variableType == "integer")
        {
             $sth = $this->db->prepare("SELECT
                                  loc.nLocalityIDPK,
                                  loc.nAreaIDFK,
                                  a.tAreaName,
                                  c.nCountryIDPK,
                                  ct.tCityName,
                                  loc.nCityIDFK,
                                  s.tStateName,
                                  loc.nStateIDFK,
                                  loc.tLocalityName,
                                  loc.dLocalityLatitude,
                                  loc.dLocalityLongitude,
                                  loc.dLocalityCreatedOn,
                                  loc.nLocalityCreatedBy
                                 
                                FROM
                                  tbllocalities as loc,
                                  tblareas AS a,
                                  tblcities AS ct,
                                  tblstates AS s,
                                  tblcountries AS c
                                WHERE
                                  loc.nAreaIDFK  = a.nAreaIDPK AND loc.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.bIsCityRemoved = 0 AND s.bIsStateRemoved = 0 AND c.bIsCountryRemoved = 0 AND a.bIsAreaRemoved = 0 AND a.bIsAreaDummyData = 0
                                  AND
                                  loc.nLocalityIDPK = $id
                                ORDER by
                                  a.tAreaName ASC");
        }
        else
        {
             $sth = $this->db->prepare("SELECT
                                  loc.nLocalityIDPK,
                                  loc.nAreaIDFK,
                                  a.tAreaName,
                                  c.nCountryIDPK,
                                  ct.tCityName,
                                  loc.nCityIDFK,
                                  s.tStateName,
                                  loc.nStateIDFK,
                                  loc.tLocalityName,
                                  loc.dLocalityLatitude,
                                  loc.dLocalityLongitude,
                                  loc.dLocalityCreatedOn,
                                  loc.nLocalityCreatedBy
                                 
                                FROM
                                  tbllocalities as loc,
                                  tblareas AS a,
                                  tblcities AS ct,
                                  tblstates AS s,
                                  tblcountries AS c
                                WHERE
                                  loc.nAreaIDFK  = a.nAreaIDPK AND loc.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.bIsCityRemoved = 0 AND s.bIsStateRemoved = 0 AND c.bIsCountryRemoved = 0 AND a.bIsAreaRemoved = 0 AND a.bIsAreaDummyData = 0
                                  AND
                                  loc.tLocalityName = '$id'
                                ORDER by
                                  a.tAreaName ASC");
        } 
       
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetch();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function fetchCity($id) // fetch state for city form and append it to the dropdown dependent
    {
        $sth = $this->db->prepare("SELECT `nCityIDPK`, `tCityName`, `nStateIDFK`, `bIsCityRemoved` FROM `tblcities` WHERE `bIsCityRemoved` = 0 AND `nStateIDFK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function fetchCarearCity() // fetch state for city form and append it to the dropdown dependent
    {
        $sth = $this->db->prepare("SELECT `nCityIDPK`, `tCityName`, `nStateIDFK`, `bIsCityRemoved` FROM `tblcities` WHERE `bIsCityRemoved` = 0");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function singleArea($id)
    {
        $variableType = gettype($id);
     
        if($variableType == "integer")
        {
            $sth = $this->db->prepare("SELECT a.nAreaIDPK, a.tAreaName, a.nCityIDFK, a.nAreaPincode, a.dAreaLatitude, a.dAreaLongitude, ct.tCityName, ct.nStateIDFK, s.tStateName,s.nCountryIDFK,c.tCountryName FROM tblareas as a ,tblcities as ct,tblstates as s,tblcountries as c WHERE a.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND a.nAreaIDPK = $id AND a.bIsAreaRemoved = 0 AND c.bIsCountryRemoved = 0 AND s.bIsStateRemoved = 0 AND ct.bIsCityRemoved = 0");
        }
        else
        {
             $sth = $this->db->prepare("SELECT a.nAreaIDPK, a.tAreaName, a.nCityIDFK, a.nAreaPincode, a.dAreaLatitude, a.dAreaLongitude, ct.tCityName, ct.nStateIDFK, s.tStateName,s.nCountryIDFK,c.tCountryName FROM tblareas as a ,tblcities as ct,tblstates as s,tblcountries as c WHERE a.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND a.tAreaName = '$id' AND a.bIsAreaRemoved = 0 AND c.bIsCountryRemoved = 0 AND s.bIsStateRemoved = 0 AND ct.bIsCityRemoved = 0");
        } 
       

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getShowInHeaderCityCount()
    {
         $sth = $this->db->prepare("SELECT `nCityIDPK`, `tCityName`, `nStateIDFK`, `bIsCityRemoved`,bShowInHeader FROM `tblcities` WHERE `bIsCityRemoved` = 0 AND bShowInHeader = 1");
        $sth->execute();
        $row = $sth->fetchAll();
        $count = count($row);
        if (!empty($row)) {
            return $count;
        } else {
            return NULL;
        }
    }

    public function checkForLocation($country,$state,$city,$area,$route,$pincode,$latitude,$longitude)
    {
         $loggedId = SessionHandling::get("loggedId");

        $countryId = '';
        $stateId = '';
        $cityId = '';
        $areaId = '';
        $localityId = '';

        if(!empty($country))
        {           
            $result = $this->singleCountry($country);

           if(is_null($result))
           {
                $field = array('tCountryCode', 'tCountryName','nCountryCreatedBy');
                $value = array('', $country,$loggedId);

                $countryData = array_combine($field, $value);

                $countryId = $this->insert($countryData, 'tblcountries');
           }
           else
           {
                $countryId = $result[0]['nCountryIDPK'];
           }
        }

        if(!empty($state))
        {           
            $result = $this->singleState($state);

           if(is_null($result))
           {
                $field = array('tStateName', 'nCountryIDFK','nStateCreatedBy');
                $value = array($state, $countryId, $loggedId);

                $stateData = array_combine($field, $value);

               $stateId = $this->insert($stateData, 'tblstates');

                // echo $stateID;
           }
           else
           {
                $stateId = $result[0]['nStateIDPK'];
           }
        }

        if(!empty($city))
        {           
            $result = $this->singleCity($city);

           if(is_null($result))
           {
                $field = array('tCityName','tCityImagePath','nStateIDFK','nCityCreatedBy');
                $value = array($city,'',$stateId, $loggedId);
                $cityData = array_combine($field, $value);
                $cityId = $this->insert($cityData, 'tblcities');

                // echo $countryID;
           }
           else
           {
                $cityId = $result[0]['nCityIDPK'];
           }
        }

        if(!empty($area))
        {           
            $result = $this->singleArea($area);

            if(is_null($result))
            {
                $field = array('nCityIDFK', 'tAreaName','nAreaPincode','dAreaLatitude','dAreaLongitude','nAreaCreatedBy');
                $value = array($cityId, $area, $pincode, $latitude, $longitude, $loggedId);

                $areaData = array_combine($field, $value);

                $areaId = $this->insert($areaData, 'tblareas');
            }
            else
            {
                $areaId = $result[0]['nAreaIDPK'];
            }
        }
        
        if(!empty($route))
        {           
            $result = $this->singleLocality($route);

           if(is_null($result))
           {
                $field = array('nAreaIDFK', 'nCityIDFK','nStateIDFK','tLocalityName','dLocalityLatitude','dLocalityLongitude','nLocalityCreatedBy');
                $value = array($areaId, $cityId, $stateId, $route,$latitude, $longitude, $loggedId);

                $localityData = array_combine($field, $value);

                $localityId = $this->insert($localityData, 'tbllocalities');
           }
           else
           {
                $localityId = $result['nLocalityIDPK'];
           }
        }

        $data = Array();
        
        $data['countryId'] = $countryId;
        $data['stateId'] = $stateId;
        $data['cityId'] = $cityId;
        $data['areaId'] = $areaId;
        $data['localityId'] = $localityId;

        return $data;
    }
}