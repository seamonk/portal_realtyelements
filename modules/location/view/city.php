<style type="text/css">
    .paddingLeft {
        padding-left: 0px !important;
    }

    .paddinTop {
        padding-top: 50px !important;
    }
    img {
    transition: -webkit-transform 0.55s ease;
    }

    img:active {
        -webkit-transform: scale(3);
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head"
                             data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-map-location"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleCity)) {
                                    echo "Update City";
                                } else {
                                    echo "Add City";
                                } ?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" "
                             role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                                <!--                                <div class="m-portlet">-->
                                <!--begin::Form-->
                                <?php
                                if (!empty($this->singleCity)) 
                                {
                                    ?>
                                    <form id="m_form" class="m-form" enctype="multipart/form-data" action="<?php echo Urls::$BASE; ?>location/createCity"
                                          method="POST">
                                        <div class="m-portlet__body">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="col-lg-12">
                                                                <label>Select City Image </label>
                                                                <span class="" onclick="document.getElementById('city_image').click();">
                                                                    <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px; height: 100%;cursor: pointer;">
                                                                        <input style="display:none;" type="file" name="city_image" id="city_image" onchange="city_image_preview(this)" />
                                                                        <div align="center" id="city_display_preview" style="height: 100%;">
                                                                             <img width="150px" src="<?php echo Urls::$BASE.Urls::$CITY_IMAGE_UPLAODS.$this->singleCity[0]['tCityImagePath']; ?>" class="vpb_image_style" class="img-thumbnail">
                                                                        </div>
                                                                    </div>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <input type="hidden" name="cityID"
                                                               value="<?php echo $this->singleCity[0]['nCityIDPK']; ?>">
                                                            <div class="col-lg-12">
                                                                <label>Select Country Name</label>
                                                                <select class="form-control m-select2" name="country" id="select2Country" placeholder="Country Name"></select>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <label>Select State Name</label>
                                                                <select class="form-control m-select2" name="state" id="select2State" placeholder="State Name"></select>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <label>City Name</label>
                                                                <input type="text" class="form-control m-input" name="cityName" value="<?php echo $this->singleCity[0]['tCityName']; ?>"
                                                                     placeholder="City Name" id="cityName">
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4">   
                                                    <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update City
                                                    </button>
                                                </div>
                                                <div class="col-lg-4">
                                                    <button type="reset" class="btn default btn-block" name="submit" id="submit" onclick="window.location = '<?php echo Urls::$BASE; ?>location/city';">
                                                        Cancel
                                                    </button>
                                                </div>
                                                <div class="col-lg-2"></div>
                                            </div>
                                        </div>
                                    </form>
                                    <?php
                                } else {
                                    ?>
                                    <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createCity"
                                          method="POST"  enctype = "multipart/form-data">
                                        <!--                                            <div class="m-portlet__body">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="col-lg-12">
                                                            <label>Select city Image</label>
                                                            <span class="" onclick="document.getElementById('city_image').click();">
                                                                <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;cursor: pointer;">
                                                                    <input style="display:none;" type="file" name="city_image" id="city_image" onchange="city_image_preview(this)" />


                                                                    <div align="center" id="city_display_preview">
                                                                        <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="col-lg-12">
                                                            <label>Select Country Name</label>
                                                            <select class="form-control m-select2" name="country"
                                                                    id="select2Country" placeholder="Country Name"></select>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <label>Select State Name</label>
                                                            <select class="form-control m-select2" name="state"
                                                                    id="select2State" placeholder="State Name"></select>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <label>City Name</label>
                                                            <input type="text" class="form-control m-input"
                                                                   name="cityName" placeholder="City Name" id="cityName">
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--                                            </div>-->
                                        <!--                                            <div class="m-portlet__foot m-portlet__foot--fit">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit"  id="submit">
                                                    Add City
                                                </button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block" name="reset" id="submit">Cancel
                                                </button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <!--                                            </div>-->
                                    </form>
                                    <?php
                                } ?>
                                <!--end::Form-->
                                <!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>City Name</th>
                                    <th>State Name</th>
                                    <th>Country Name</th>
                                    <th>Show In Header?</th>
                                    <th width="15%">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                               
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function () 
    {
         toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $("#select2Country").select2({
            placeholder: "-- Select --"
        });

        $("#select2State").select2({
            placeholder: "-- Select --"
        });
        $("#m_form").validate({
            rules: {
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                cityName: {
                    required: true
                }
            }
        });

        loadDatatable();

        getCountryList();   // fetch all country and append it to the dropdown

        $('#select2Country').on('change', function () {    // on country dropdown change state
            getStateListOfOne();
        });

        


    }); // DOM end
   
    function loadDatatable() 
    {
        
        $('#s1').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "savestate":true,
            "ajax": "<?php echo Urls::$BASE ?>location/getCities",
            "type": "POST",
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]},
                {"orderable": false,"targets": [0,6]} 
            ],
            "order":[[1,"asc"]],
            "destroy": true
        });
    }
    $(document).on('click', ".btn_edit", function (e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });

    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this City ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>location/deleteCity",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            toastr.success("City Deleted Sucessfully", "City");
                            loadDatatable();
                        }
                        else{
                            toastr.success("error wile Deleting City", "City");
                            loadDatatable();
                        }
                    }
                });
            }
        });
    }

    function ShowInHeader(id,status) 
    {   
        // alert(status);
        if(status == 1)
        {
            swal({
            title:"Are you sure?",
            text:"You want to show this city in header ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {   

                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>location/ShowInHeaderCityCount",
                        data:{id:id,status:status},
                        success: function(data)
                        {   

                            if (data.trim() < 8) 
                            {
                                $.ajax({
                                type: "POST",
                                url: "<?php echo Urls::$BASE ?>location/ShowInHeader",
                                data:{id:id,status:status},
                                success: function(data)
                                {        
                                    if (data.trim() == "true") 
                                    {   
                                        // loadDatatable();
                                        toastr.success("City Added in header Sucessfully", "City");
                                        loadDatatable();

                                    }
                                    else
                                    {
                                        toastr.error("Error While Adding city in Header ", "City"); 
                                        loadDatatable();

                                    }
                                    // mUtil.scrollTo("subscriptionForm", -200);    
                                }
                                });
                            }
                            else
                            {   
                                toastr.error("Maximum Limit Reached on header ", "City"); 
                                loadDatatable();
                            }
                        }
                    });

                    
                }
                else
                {
                    loadDatatable();
                }    
           });
            
        }
        else if (status == 0)
        {
            
            swal({
            title:"Are you sure?",
            text:"You want to show this city in header ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>location/ShowInHeader",
                        data:{id:id,status:status},
                        success: function(data)
                        {        
                            if (data.trim() == "true") 
                            {   
                                // loadDatatable();
                                toastr.success("City Removed from header Sucessfully", "city");
                                loadDatatable();

                            }
                            else
                            {
                                toastr.error("Error While Removing city From Header ", "city"); 
                                 loadDatatable();
                            }
                            // mUtil.scrollTo("subscriptionForm", -200);    
                        }
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
        }
       
        
    }
    function getCountryList()   // fetch all country list insert / update
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function (data) {
                $('#select2Country').empty();
                $('#select2Country').append('<option value="">-- Select Country --</option>');
                var country = JSON.parse(data);
                for (var i = 0; i < country.length; i++) {
                    var data1 = "<?php if (isset($this->singleCity)) {
                        echo $this->singleCity[0]['nCountryIDFK'];
                    } else {
                        echo NULL;
                    } ?>";
                    if (data1 == country[i].nCountryIDPK)
                        $('#select2Country').append('<option value=' + country[i].nCountryIDPK + ' selected>' + country[i].tCountryName + '</option>');
                    else
                        $('#select2Country').append('<option value=' + country[i].nCountryIDPK + '>' + country[i].tCountryName + '</option>');
                }
                $("#select2Country").trigger("change");
            }
        }).done(function () {
            getStateListOfOne();
        });
    }
     function city_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0)
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                    $('#city_display_preview').html('');

                    var reader = new FileReader();

                    reader.onload = function(e)
                    {
                        $('#city_display_preview').append(
                            ' \
                            <img style="height:70%;width:70%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                    }
                    reader.readAsDataURL(file);
                }
            }
            else {  return false; }
        });
    }

    function getStateListOfOne() // fetch state list on the basis of the country id
    {
        var countryID = $('#select2Country').val();

        //alert(countryID);

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getState',
            type: 'POST',
            data: {countryID: countryID},
            success: function (data) {
                console.log(data);
                $('#select2State').empty();
                $('#select2State').append('<option value="">-- Select State --</option>');
                var state = JSON.parse(data);
                for (var i = 0; i < state.length; i++) {

                    var data2 = "<?php if (isset($this->singleCity)) {
                        echo $this->singleCity[0]['nStateIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == state[i].nStateIDPK)
                        $('#select2State').append('<option value=' + state[i].nStateIDPK + ' selected>' + state[i].tStateName + '</option>');
                    else
                        $('#select2State').append('<option value=' + state[i].nStateIDPK + '>' + state[i].tStateName + '</option>');
                }
            }

        }).done(function () {

        });
    }

   /* function getStateList() // fetch all state list
    {
        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getStateAll',
            success: function (data) {
                $('#select2State').empty();
                $('#select2State').append('<option value="">-- Select State --</option>');
                var state = JSON.parse(data);
                for (var i = 0; i < state.length; i++) {
                    var data2 = "<?php if (isset($this->singleCity)) {
                        echo $this->singleCity[0]['nStateIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == state[i].nStateIDPK)
                        $('#select2State').append('<option value=' + state[i].nStateIDPK + ' selected>' + state[i].tStateName + '</option>');
                    else
                        $('#select2State').append('<option value=' + state[i].nStateIDPK + '>' + state[i].tStateName + '</option>');
                }
                //$("#select2State").trigger("change");
            }

        }).done(function () {
            getStateListOfOne();
        });
    }*/

</script>
