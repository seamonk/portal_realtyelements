<style type="text/css">
    .paddingLeft {
        padding-left: 0px !important;
    }

    .paddinTop {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head"
                             data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-map-location"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleCategories)) {
                                    echo "Update Country";
                                } else {
                                    echo "Add Country";
                                } ?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" "
                             role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                                <!--                                <div class="m-portlet">-->
                                <!--begin::Form-->
                                <?php
                                if (!empty($this->singleCountry)) {
                                    for ($i = 0; $i < count($this->singleCountry); $i++) {
                                        ?>
                                        <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createCountry" method="POST" id="update_form" name="update_form">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <input type="hidden" name="countryID"
                                                                   value="<?php echo $this->singleCountry[$i]['nCountryIDPK']; ?>">
                                                            <div class="col-lg-6">
                                                                <label>Country Code</label>
                                                                <input type="text" onkeypress="return isNumberKey(event);" class="form-control m-input" value="<?php echo $this->singleCountry[$i]['tCountryCode']; ?>" name="countryCode" placeholder="Country Code" id="countryCode">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label>Country Name</label>
                                                                <input type="text" class="form-control m-input" value="<?php echo $this->singleCountry[$i]['tCountryName']; ?>" name="countryName" placeholder="Country Name" id="countryName">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update Country
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="reset" class="btn default btn-block"  onclick="window.location = '<?php echo Urls::$BASE; ?>location/';">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-2"></div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createCountry"
                                          method="POST" id="add_form" name="add_form">
                                        <!--                                            <div class="m-portlet__body">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Country Code</label>
                                                        <input type="text" onkeypress="return isNumberKey(event);" class="form-control m-input" name="countryCode" id="countryCode" placeholder="Country Code">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Country Name</label>
                                                        <input type="text" class="form-control m-input" name="countryName" id="countryName" placeholder="Country Name">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--                                            </div>-->
                                        <!--                                            <div class="m-portlet__foot m-portlet__foot--fit">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">
                                                    Add Country
                                                </button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block" name="reset">Cancel
                                                </button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <!--                                            </div>-->
                                    </form>
                                    <?php
                                } ?>
                                <!--end::Form-->
                                <!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Country Code</th>
                                    <th>Country Name</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function () 
    {
         toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        loadDatatable()

        $("#m_form").validate({
            rules: {
                countryCode: {
                    required: true
                },
                countryName: {
                    required: true
                }
            }
        });

    });

    function loadDatatable() 
    {
        
        $('#s1').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo Urls::$BASE ?>location/getCountries",
            "type": "POST",
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]},
                {"orderable": false,"targets": [0,3]}
                // {"orderable": false,"targets": [0,8]}                   
            ],
            "order":[[1,"asc"]],
            "destroy": true
        });
    }

    $(document).on('click', ".btn_edit", function (e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        form.submit();
    });

    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this Country ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>location/deleteCountry",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                           toastr.success("Country Deleted Sucessfully", "Country");
                           loadDatatable();
                        }
                        else{
                            toastr.success("error wile Deleting Country", "Country");
                            loadDatatable();
                        }
                    }
                });
            }
        });
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
</script>
