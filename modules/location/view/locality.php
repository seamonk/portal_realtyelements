<style type="text/css">
    .paddingLeft {
        padding-left: 0px !important;
    }

    .paddinTop {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
   
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head"
                             data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-map-location"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singlelocality)) {
                                    echo "Update locality";
                                } else {
                                    echo "Add locality";
                                } ?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" "
                             role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                                <!--                                <div class="m-portlet">-->
                                <!--begin::Form-->
                                <?php
                                if (!empty($this->singlelocality)) 
                                {
                                    ?>
                                        <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createLocality" method="POST">
                                            <input type="hidden" name="localityID" value="<?php echo $this->singlelocality['nLocalityIDPK']; ?>">
                                            <div class="form-group row">
                                                <div class="col-lg-3">
                                                    <label>Select Country Name</label>
                                                    <select class="form-control m-select2" name="country" id="select2Country" placeholder="Country Name"></select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Select State Name</label>
                                                    <select class="form-control m-select2" name="state" id="select2State" placeholder="State Name"></select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Select City Name</label>
                                                    <select class="form-control m-select2" name="city" id="select2City" placeholder="City Name"></select>
                                                </div>
                                                 <div class="col-lg-3">
                                                    <label>Select Area Name</label>
                                                    <select class="form-control m-select2" name="area" id="select2SArea" placeholder="City Name"></select>
                                                </div>
                                            </div>
                                            <div class="form-group  row">
                                                <div class="col-lg-3">
                                                    <label>locality Name</label>
                                                    <input type="text" class="form-control m-input" name="localityName" placeholder="locality Name" id="localityName" value="<?php echo $this->singlelocality['tLocalityName']; ?>">
                                                </div>
                                                
                                                <div class="col-lg-3">
                                                    <label>Latitude</label>
                                                    <input type="text" class="form-control m-input" id="localityLatitude" name="localityLatitude" placeholder="Latitude" value="<?php echo $this->singlelocality['dLocalityLatitude']; ?>">
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Longitude</label>
                                                    <input type="text" class="form-control m-input" name="localityLongitude"  placeholder="Longitude" id="localityLongitude" value="<?php echo $this->singlelocality['dLocalityLongitude']; ?>">
                                                </div>
                                            </div>
                                          
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4">
                                                    <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">
                                                        Update locality
                                                    </button>
                                                </div>
                                                <div class="col-lg-4">
                                                    <button type="reset" class="btn default btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>location/locality';">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-lg-2"></div>
                                            </div>
                                        </form>
                                    <?php
                                } 
                                else 
                                {
                                    ?>
                                    <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createlocality"  method="POST">
                                        <!--                                            <div class="m-portlet__body">-->
                                        <div class="form-group  row">
                                            <div class="col-lg-3">
                                                <label>Select Country Name</label>
                                                <select class="form-control m-select2" name="country" id="select2Country" placeholder="Country Name"></select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Select State Name</label>
                                                <select class="form-control m-select2" name="state" id="select2State" placeholder="State Name"></select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Select City Name</label>
                                                <select class="form-control m-select2" name="city" id="select2City" placeholder="City Name"></select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Select Area Name</label>
                                                <select class="form-control m-select2" name="area" id="select2SArea" placeholder="City Name"></select>
                                            </div>

                                            
                                        </div>
                                        <div class="form-group  row">
                                            <div class="col-lg-3">
                                                <label>locality Name</label>
                                                <input type="text" class="form-control m-input" name="localityName" placeholder="locality Name" id="localityName">
                                            </div>
                                           <!--  <div class="col-lg-3">
                                                <label>locality Pincode</label>
                                                <input type="text" onkeypress="return isNumberKey(event);" class="form-control m-input" name="localityPincode" id="localityPincode" placeholder="locality Pincode">
                                            </div> -->
                                            <div class="col-lg-3">
                                                <label>Latitude</label>
                                                <input type="text" class="form-control m-input" id="localityLatitude" name="localityLatitude" placeholder="Latitude">
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Longitude</label>
                                                <input type="text" class="form-control m-input" name="localityLongitude"  placeholder="Longitude" id="localityLongitude">
                                            </div>
                                        </div>
                                      
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">
                                                    Add locality
                                                </button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block">Cancel
                                                </button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <!--                                            </div>-->
                                    </form>
                                    <?php
                                } ?>
                                <!--end::Form-->
                                <!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>locality Name</th>
                                    <th>Area Name</th>
                                    <th>City Name</th>
                                    <th>State Name</th>
                                    <!-- <th>Country Name</th> -->
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function () 
    {

        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $("#select2Country").select2({
            placeholder: "-- Select --"
        });

        $("#select2State").select2({
            placeholder: "-- Select --"
        });

        $("#select2City").select2({
            placeholder: "-- Select --"
        });
        $("#select2SArea").select2({
            placeholder: "-- Select --"
        })

        //Update Time
        
        loadDatatable();

        $("#m_form").validate({
            rules: {
                country: {
                    required: true
                },
                state: {
                    required: true
                },
                city: {
                    required: true
                },
                area: {
                    required: true
                },
                localityName: {
                    required: true
                }
            }
        });
        
        getCountryList();   // fetch all country and append it to the dropdown

        $('#select2Country').on('change', function () {    // on country dropdown change state
            getStateList();
        });

        $('#select2State').on('change', function () {    // on country dropdown change state
            getCityList();
        });
        $('#select2City').on('change', function () {    // on country dropdown change state
            getAreaList();
        });

        $("#localityLatitude").focusout(function()
        {
            var latitude = document.getElementById('localityLatitude').value;
           
            var reg = new RegExp("^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$");

            if(latitude != '')
            {
                if(!reg.exec(latitude)) 
                {
                    document.getElementById('localityLatitude').value='';
                    toastr.error("Please enter valid latitude value.", "latitude");
                } 
            }
        });

        $("#localityLongitude").focusout(function()
        {
            var longitude = document.getElementById('localityLongitude').value;

            var reg = new RegExp("^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$");

            if(longitude != '')
            {
                if(!reg.exec(longitude)) 
                {
                    document.getElementById('localityLongitude').value='';
                    toastr.error("Please enter valid longitude value.", "longitude");
                }  
            }
        });


    }); // DOM end

    function loadDatatable() 
    {
        
        $('#s1').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo Urls::$BASE ?>location/getLocality",
            "type": "POST",
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]},
                {"orderable": false,"targets": [0,5]}
                
                // {"orderable": false,"targets": [0,8]}                   
            ],
            "order":[[1,"asc"]],
            "destroy": true
        });
    }
    $(document).on('click', ".btn_edit", function (e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });

    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this locality ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>location/deletelocality",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>location/locality";
                        }
                    }
                });
            }
        });
    }

    function getCountryList()   // fetch all country list insert / update
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function (data) {
                $('#select2Country').empty();
                $('#select2Country').append('<option value="">-- Select Country --</option>');
                var country = JSON.parse(data);
                for (var i = 0; i < country.length; i++) 
                {
                    var data1 = "<?php if (isset($this->singlelocality)) {
                        echo $this->singlelocality['nCountryIDPK'];
                    } else {
                        echo NULL;
                    } ?>";
                    if (data1 == country[i].nCountryIDPK)
                        $('#select2Country').append('<option value=' + country[i].nCountryIDPK + ' selected>' + country[i].tCountryName + '</option>');
                    else
                        $('#select2Country').append('<option value=' + country[i].nCountryIDPK + '>' + country[i].tCountryName + '</option>');
                }
                $("#select2Country").trigger("change");
            }
        }).done(function () {
            getStateList();
        });
    }
    
    function getStateList() // fetch state list on the basis of the country id
    {
        var countryID = $('#select2Country').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getState',
            type: 'POST',
            data: {countryID: countryID},
            success: function (data) {
                console.log(data);
                $('#select2State').empty();
                $('#select2State').append('<option value="">-- Select State --</option>');
                var state = JSON.parse(data);
                for (var i = 0; i < state.length; i++) {

                    var data2 = "<?php if (isset($this->singlelocality)) {
                        echo $this->singlelocality['nStateIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == state[i].nStateIDPK)
                        $('#select2State').append('<option value=' + state[i].nStateIDPK + ' selected>' + state[i].tStateName + '</option>');
                    else
                        $('#select2State').append('<option value=' + state[i].nStateIDPK + '>' + state[i].tStateName + '</option>');
                }
                $("#select2State").trigger("change");
            }

        }).done(function () {
            getCityList();
        });
    }


    function getCityList() // fetch state list on the basis of the country id
    {
        var stateID = $('#select2State').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getCity',
            type: 'POST',
            data: {stateID: stateID},
            success: function (data) {
                console.log(data);
                $('#select2City').empty();
                $('#select2City').append('<option value="">-- Select City --</option>');
                var city = JSON.parse(data);
                for (var i = 0; i < city.length; i++) {

                    var data2 = "<?php if (isset($this->singlelocality)) {
                        echo $this->singlelocality['nCityIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == city[i].nCityIDPK)
                        $('#select2City').append('<option value=' + city[i].nCityIDPK + ' selected>' + city[i].tCityName + '</option>');
                    else
                        $('#select2City').append('<option value=' + city[i].nCityIDPK + '>' + city[i].tCityName + '</option>');
                }
                 $("#select2City").trigger("change");
            }           


        }).done(function () {

            getAreaListOfOne()
        });
    }
  
    function getAreaList() // fetch state list on the basis of the country id
    {
        var cityID = $('#select2City').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getArea',
            type: 'POST',
            data: {cityID: cityID},
            success: function (data) {
                console.log(data);
                $('#select2SArea').empty();
                $('#select2SArea').append('<option value="">-- Select City --</option>');
                var city = JSON.parse(data);
                for (var i = 0; i < city.length; i++) {

                    var data2 = "<?php if (isset($this->singlelocality)) {
                        echo $this->singlelocality['nAreaIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == city[i].nAreaIDPK)
                        $('#select2SArea').append('<option value=' + city[i].nAreaIDPK + ' selected>' + city[i].tAreaName + '</option>');
                    else
                        $('#select2SArea').append('<option value=' + city[i].nAreaIDPK + '>' + city[i].tAreaName + '</option>');
                }
            }

        }).done(function () {

        });
    }
   
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
</script>
