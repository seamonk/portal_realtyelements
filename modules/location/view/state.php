<style type="text/css">
    .paddingLeft {
        padding-left: 0px !important;
    }

    .paddinTop {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
   
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head"
                             data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-map-location"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleState)) {
                                    echo "Update State";
                                } else {
                                    echo "Add State";
                                } ?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" "
                             role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                                <!--                                <div class="m-portlet">-->
                                <!--begin::Form-->
                                <?php
                                if (!empty($this->singleState)) {
                                    for ($i = 0; $i < count($this->singleState); $i++) {
                                        ?>
                                        <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createState" method="POST">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <input type="hidden" name="stateID"
                                                                   value="<?php echo $this->singleState[$i]['nStateIDPK']; ?>">
                                                            <div class="col-lg-6">
                                                                <label>Select Country Name</label>
                                                                <select class="form-control m-select2" name="country" id="select2Country" placeholder="Country Name"></select>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label>State Name</label>
                                                                <input type="text" class="form-control m-input" name="stateName" value="<?php echo $this->singleState[$i]['tStateName'] ?>" placeholder="State Name" id="stateName">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update State
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="reset" class="btn default btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>location/state';">
                                                           
                                                            Cancel
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-2"></div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <form id="m_form" class="m-form" action="<?php echo Urls::$BASE; ?>location/createState"
                                          method="POST">
                                        <!--                                            <div class="m-portlet__body">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Select Country Name</label>
                                                        <select class="form-control m-select2" name="country" id="select2Country"></select placeholder="Country Name">
                                                      
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>State Name</label>
                                                        <input type="text" class="form-control m-input" name="stateName" placeholder="State Name" id="stateName">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--                                            </div>-->
                                        <!--                                            <div class="m-portlet__foot m-portlet__foot--fit">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" id="submit" value="submit">
                                                    Add State
                                                </button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block" >Cancel
                                                </button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <!--                                            </div>-->
                                    </form>
                                    <?php
                                } ?>
                                <!--end::Form-->
                                <!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                        <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>State Name</th>
                                    <th>Country Name</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function () 
    {
         toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $("#select2Country").select2({
            placeholder: "-- Select --"
        }); 
        loadDatatable();

       
        $("#m_form").validate({
            rules: {
                select2Country: {
                    required: true
                },
                stateName: {
                    required: true
                }
            }
        });

       
    });

    getCountryList();   // fetch all country and append it to the dropdown

    $(document).on('click', ".btn_edit", function (e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });
    function loadDatatable() 
    {
        
        $('#s1').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo Urls::$BASE ?>location/getStates",
            "type": "POST",
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]},
                {"orderable": false,"targets": [0,3]}                   
                
            ],
            "order":[[1,"asc"]],
            "destroy": true
        });
    }
    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this State ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>location/deleteState",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") 
                        {
                           toastr.success("State Deleted Sucessfully", "State");
                           loadDatatable();
                        }
                        else{
                            toastr.success("error wile Deleting State", "State");
                            loadDatatable();
                        }
                    }
                });
            }
        });
    }

    function getCountryList()   // fetch all country list
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function (data) {
                $('#select2Country').empty();
                $('#select2Country').append('<option value="">-- Select Country --</option>');
                var country = JSON.parse(data);
                for (var i = 0; i < country.length; i++) {
                    var data1 = "<?php if (isset($this->singleState)) {
                        echo $this->singleState[0]['nCountryIDFK'];
                    } else {
                        echo NULL;
                    } ?>";
                    if (data1 == country[i].nCountryIDPK)
                        $('#select2Country').append('<option value=' + country[i].nCountryIDPK + ' selected>' + country[i].tCountryName + '</option>');
                    else
                        $('#select2Country').append('<option value=' + country[i].nCountryIDPK + '>' + country[i].tCountryName + '</option>');
                }
                $("#select2Country").trigger("change");
            }
        });
    }
</script>
