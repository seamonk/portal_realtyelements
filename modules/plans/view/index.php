<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    
    th{text-align: center;}
    td{text-align: center;}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                          <span class="m-accordion__item-icon">
                              <i class="fa flaticon-edit-1"></i>
                          </span>
                          <span class="m-accordion__item-title"> 
                            
                                  <?php if (!empty($this->singlePlans)) {
                                      echo "Update Subscriptions";
                                  } else {
                                      echo "Add Subscriptions";
                                  } ?>
                             
                            </span>
                          <span class="m-accordion__item-mode"></span>
                        </div>

                         <?php if (!empty($this->singlePlans)) {
                             ?>
                                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php

                          } else {
                              ?>
                                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php
                          } ?>
                      
                            <div class="m-accordion__item-content">
                                <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>plans/createPlans" enctype = "multipart/form-data" id="add_form" name="add_form">
                                    <?php  
                                    if (!empty($this->singlePlans)) 
                                    { ?>
                                        <input type="hidden" name="plansID" value="<?php echo $this->singlePlans[0]['nSubscriptionIDPK']; ?>">
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <label>Subscriptions Name</label>
                                                <input class="form-control m-input" id="subName" name="subName" placeholder="Subscriptions Name" type="text" value="<?php echo $this->singlePlans[0]['tSubscriptionName']; ?>"/>
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Subscriptions Price</label>
                                                <!-- text area -->
                                                <input class="form-control m-input date-time-picker" id="subIndianPrice" name="subIndianPrice"  placeholder="Indian Price" type="text" value="<?php echo $this->singlePlans[0]['fSubscriptionPrice']; ?>" onkeypress="return isNumberKey(event);" />
                                            </div>
                                           <!--  <div class="col-lg-4">
                                                <label>Subscriptions Other Country Price</label>
                                                
                                                <input class="form-control m-input date-time-picker" id="subOtherCountryPrice" name="subOtherCountryPrice"  placeholder="Other Country Price" value="<?php echo $this->singlePlans[0]['fOtherCountrySubscriptionPrice']; ?>" type="text" onkeypress="return isNumberKey(event);" />
                                            </div> -->
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <label>No of Listings</label>
                                                <!-- text area -->
                                                <input class="form-control m-input date-time-picker" id="noListings" name="noListings"  placeholder="No of Listings" type="text" value="<?php echo $this->singlePlans[0]['nListingLimit']; ?>" onkeypress="return isNumberKey(event);" />
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Subscriptions Validity [In Days]</label>
                                                <input class="form-control m-input" id="subValidity" value="<?php echo $this->singlePlans[0]['dtSubscriptionExpireOn']; ?>" name="subValidity" placeholder="Subscriptions Validity" type="text" />
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Listing Validity [In Days]</label>
                                                <!-- text area -->
                                                <input class="form-control m-input date-time-picker" id="ListingValidity" name="ListingValidity"  placeholder="Listing Validity" value="<?php echo $this->singlePlans[0]['dtListingExpireOn']; ?>" type="text" onkeypress="return isNumberKey(event);" />
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                             <div class="col-lg-2"></div>
                                             <div class="col-4">
                                                <button type="submit" class="btn btn-block" name="submit" value="Update"  id="submit" style="background-color: #a1842f; color: white;">Update Subscription Plans</button>
                                            </div>
                                            <div class="col-4">
                                                <button type="reset" class="btn btn-secondary btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>plans';">Cancel</button>
                                            </div> <div class="col-lg-2"></div>

                                        </div>
                                    <?php
                                    }
                                    else
                                    {?>    
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <label>Subscriptions Name</label>
                                                <input class="form-control m-input" id="subName" name="subName" placeholder="Subscriptions Name" type="text" />
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Subscriptions Price</label>
                                                <!-- text area -->
                                                <input class="form-control m-input date-time-picker" id="subIndianPrice" name="subIndianPrice"  placeholder="Indian Price" type="text" onkeypress="return isNumberKey(event);" />
                                            </div>
                                           <!--  <div class="col-lg-4">
                                                <label>Subscriptions Other Country Price</label>
                                                
                                                <input class="form-control m-input date-time-picker" id="subOtherCountryPrice" name="subOtherCountryPrice"  placeholder="Other Country Price" type="text" onkeypress="return isNumberKey(event);" />
                                            </div> -->
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <label>No of Listings</label>
                                                <!-- text area -->
                                                <input class="form-control m-input date-time-picker" id="noListings" name="noListings"  placeholder="No of Listings" type="text" onkeypress="return isNumberKey(event);" />
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Subscriptions Validity [In Days]</label>
                                                <input class="form-control m-input" id="subValidity" name="subValidity" placeholder="Subscriptions Validity" type="text" />
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Listing Validity [In Days]</label>
                                                <!-- text area -->
                                                <input class="form-control m-input date-time-picker" id="ListingValidity" name="ListingValidity"  placeholder="Listing Validity" type="text" onkeypress="return isNumberKey(event);" />
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                             <div class="col-lg-2"></div>
                                             <div class="col-4">
                                                <button type="submit" class="btn btn-block" name="submit" value="submit" id="submit" style="background-color: #a1842f; color: white;">Add Subscription Plans</button>
                                            </div>
                                            <div class="col-4">
                                                <button type="reset" class="btn btn-secondary btn-block">Cancel</button>
                                            </div> <div class="col-lg-2"></div>

                                        </div>
                                    <?php
                                    }?>
                                </form>
                            <!--end::Form-->
                            </div>
                        </div>
                </div>
            </div>
        </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="fa fa-list"></i>&nbsp;&nbsp;&nbsp;&nbsp;Subscriptions Listing</h3>
                            </div>
                             <!-- <a href="<?php echo Urls::$BASE; ?>plans/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Add Subscriptions ">
                                <i class="fa fa-plus"></i>
                               
                            </a> -->
                            
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                       
                                        <th>No of Listings</th>
                                        <th>Validity <br> [Days]</th>
                                        <th>Listing Validity <br> [Days]</th>
                                        <th width="15%" style="white-space: nowrap;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($this->data)) 
                                    {
                                        for ($i = 0; $i < count($this->data); $i++) 
                                        {
                                        ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td> 

                                            <td><?php echo $this->data[$i]['tSubscriptionName']; ?></td>
                                            <td><?php echo $this->data[$i]['fSubscriptionPrice']; ?></td>
                                           
                                             <td><?php echo $this->data[$i]['nListingLimit']; ?></td>
                                            <td><?php echo $this->data[$i]['dtSubscriptionExpireOn']; ?></td>
                                            <td><?php echo $this->data[$i]['dtListingExpireOn']; ?></td>

                                            <td style="white-space: nowrap;">
                                                <!-- <form method="post" action="<?php echo Urls::$BASE; ?>plans/view_plans" style="display: inline-table;">
                                                    <input type="hidden" name="ID" value="<?php echo $this->data[$i]['nSubscriptionIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="View Detail" type="button" class="btn_view btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa  fa-eye"></i></button>
                                                </form> -->

                                                <form method="post" action="<?php echo Urls::$BASE; ?>plans/edit_plans" style="display: inline-table;">
                                                    <input type="hidden" name="ID" value="<?php echo $this->data[$i]['nSubscriptionIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Detail" type="button" class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                                </form>

                                                <form style="display: inline-table;">
                                                    <button  data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete" type="button" class="btn btn-outline-danger  m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick="setId(<?php echo $this->data[$i]['nSubscriptionIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                    }
                                    ?>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() 
    {

        $('#s1').DataTable({
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 

        $("#add_form").validate({
            rules: {
                subName: {
                    required: true
                },
                subIndianPrice: {
                    required: true
                },
                noListings: {
                    required: true
                },
                subValidity: {
                    required: true
                },
                ListingValidity: {
                    required: true
                }
            }
        });
    });
    
    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });  

    $(document).on('click',".btn_view", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });    

    function setId (del_id)
    {
        swal({
        title:"Are you sure?",
        text:"You want to delete this Plan ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>plans/delete_plans",
                    data:{'id':del_id},
                    success: function(data)
                    {     
                      if(data == "true")
                      {
                        location.href="<?php echo Urls::$BASE ?>plans/";
                      }               
                    }
                });
            }   
       });
    } 
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    } 
</script>
