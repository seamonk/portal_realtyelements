<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    td{ white-space: nowrap; }
    th{text-align: center;}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title "><i class="flaticon-event-calendar-symbol"></i>
                   Plans Detail
             </h3>
            </div>
            <a href="<?php echo Urls::$BASE; ?>plans/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Plans Listing">
                <i class="fa fa-list"></i>
               
            </a>
            
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__body">
                        <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>plans/createPlans" enctype = "multipart/form-data" id="add_form" name="add_form">
                            <?php  
                            if (!empty($this->data)) 
                            { ?>
                                <div class="form-group row">
                                    <div class="col-lg-4">
                                        <label>Plans Name</label>
                                        <input class="form-control m-input" id="planName" name="planName" placeholder="Plans Name" type="text" value="<?php echo $this->data[0]['tPlanName']; ?>" disabled />
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Indian Price</label>
                                        <!-- text area -->
                                        <input class="form-control m-input date-time-picker" id="planIndianPrice" name="planIndianPrice"  placeholder="Indian Price" type="text" onkeypress="return isNumberKey(event);" value="<?php echo $this->data[0]['fIndianPlanPrice']; ?>" disabled />
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Other Country Price</label>
                                        <!-- text area -->
                                        <input class="form-control m-input date-time-picker" id="planIndianPrice" name="planOtherCountryPrice"  placeholder="Other Country Price" type="text" onkeypress="return isNumberKey(event);" value="<?php echo $this->data[0]['fOtherCountryPlanPrice']; ?>"  disabled />
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label><b>Features Name</b></label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><b>Yes/No</b></label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label><b>Features Value</b></label>
                                    </div>
                                </div>
                                <?php
                                if (!empty($this->totalFeatures)) 
                                {
                                    for ($i = 0; $i < count($this->totalFeatures); $i++) 
                                    {
                                        $featuresName = $this->totalFeatures[$i]['tFeatureName'];

                                        $featuresTextValue = '';

                                        $featuresID = $this->totalFeatures[$i]['nFeatureIDPK'];

                                        for ($j = 0; $j < count($this->singleSelectedFeatures); $j++) 
                                        {
                                            if($this->singleSelectedFeatures[$j]['nFeatureIDFK'] == $this->totalFeatures[$i]['nFeatureIDPK'])
                                            {
                                                if($this->singleSelectedFeatures[$j]['bIsPlanFeatureText'] == 1)
                                                {
                                                    $featuresTextValue = $this->singleSelectedFeatures[$j]['tPlanFeatureValue'];

                                                    $feturesYesNo = 1;
                                                }
                                                else
                                                {
                                                    $featuresTextValue = '';

                                                    $feturesYesNo = $this->singleSelectedFeatures[$j]['tPlanFeatureValue'];
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="form-group row">
                                            <div class="col-lg-4">
                                                <label><?php echo $this->totalFeatures[$i]['tFeatureName']; ?></label>
                                            </div>
                                            <div class="col-lg-2">
                                                <select class="form-control m-select2" id="planYNID<?php echo $this->totalFeatures[$i]['nFeatureIDPK']; ?>" name="planYN[]" style="width: 100% !important" disabled>
                                                    <option value="0" <?php if($feturesYesNo == 0){
                                                        echo ' selected';
                                                    } ?>>NO</option>
                                                    <option value="1" <?php if($feturesYesNo == 1){
                                                        echo ' selected';
                                                    } ?>>YES</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <input class="form-control m-input" id="planTextValue" name="planTextValue[]" placeholder="Features Value" type="text" value="<?php echo $featuresTextValue;  ?>" disabled />
                                            </div>
                                        </div>
                                    <?php
                                    }
                                }
                                ?>
                            <?php
                            }?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() 
    {
        // dynamically load select2
        <?php
            if (!empty($this->totalFeatures)) 
            {
                for ($i = 0; $i < count($this->totalFeatures); $i++) 
                {?>
                    $("#planYNID<?php echo $this->totalFeatures[$i]['nFeatureIDPK']; ?>").select2();
                <?php
                }
            }
        ?>

        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
    });
</script>
