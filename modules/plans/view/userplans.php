<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="fa fa-list"></i>&nbsp;&nbsp;&nbsp;&nbsp;Subscriptions Listing</h3>
                            </div>                            
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Full Name</th>
                                        <th>Plan Name</th>
                                        <th>Price</th>
                                        <th>No of Listings</th>
                                        <th>Validity Expire On</th>
                                        <th>Validity <br> [In Days]</th>
                                        <th>Listing Validity <br> [In Days]</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($this->data)) 
                                    {
                                        $date = date('Y-m-d');
                                        $k = 1;

                                        for ($i = 0; $i < count($this->data); $i++) 
                                        {
                                            $expireOnDate = date('Y-m-d',strtotime($this->data[$i]['dtSubscriptionExpireOn']));
                                           
                                            if ($date >= $expireOnDate) 
                                            {   
                                               
                                                ?>
                                                <tr>
                                                    <td><?php echo $k; $k++; ?></td> 
                                                    <td ><?php echo $this->data[$i]['vUserFirstName']." ".$this->data[$i]['vUserMiddleName']." ".$this->data[$i]['vUserLastName']; ?></td>
                                                    <td style="color: red;"><?php echo $this->data[$i]['tSubscriptionName']; ?></td>

                                                    <td ><?php echo $this->data[$i]['fSubscriptionPrice']; ?></td>
                                                    <td ><?php echo $this->data[$i]['nListingLimit']; ?></td>
                                                    
                                                    <td style="color: red;"><?php echo $this->data[$i]['dtSubscriptionExpireOn']."  " ." [Expired]"; ?></td>
                                                    <td ><?php echo $this->data[$i]['SubscriptionExpirecount']; ?></td>
                                                    <td ><?php echo $this->data[$i]['dtListingExpireOn']; ?></td>
                                                </tr>
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <tr >
                                                    <td><?php echo $k; $k++; ?></td>
                                                    <td ><?php echo $this->data[$i]['vUserFirstName']." ".$this->data[$i]['vUserMiddleName']." ".$this->data[$i]['vUserLastName']; ?></td>
                                                    <td  style="color: green;"><?php echo $this->data[$i]['tSubscriptionName']; ?></td>
                                                    <td><?php echo $this->data[$i]['fSubscriptionPrice']; ?></td>
                                                    <td><?php echo $this->data[$i]['nListingLimit']; ?></td>
                                                    
                                                    <td style="color: green;"><?php echo $this->data[$i]['dtSubscriptionExpireOn']; ?></td>
                                                    <td><?php echo $this->data[$i]['SubscriptionExpirecount']; ?></td>
                                                    <td><?php echo $this->data[$i]['dtListingExpireOn']; ?></td>
                                                </tr>
                                                <?php
                                            }   
                                        }
                                    }
                                    ?>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
     $(document).ready(function() 
    {
        $('#s1').DataTable({
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 
    });
</script>