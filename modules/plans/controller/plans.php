<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Plans extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index() // fetch all countries
    {
        $name = 'modules/plans/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getPlansList();
        $this->view->data = $data;

        $this->view->render($name, "Subscription Listing", $header, $footer);
    }
      /*---------------------------------------------Add Plans --------------------------------*/

    public function user_subcription() // fetch all countries
    {

        $loggedId = SessionHandling::get('loggedId');

        $name = 'modules/plans/view/userplans.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getUserPlansList($loggedId);
        $this->view->data = $data;

        $this->view->render($name, "Subscription Listing", $header, $footer);
    }
    public function add_plans()  
    {
        $name = 'modules/plans/view/add_plan.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $totalFeatures = $this->model->getFetures();
        $this->view->totalFeatures = $totalFeatures;

        $this->view->render($name, "Add Subscription", $header, $footer);
    }

    public function delete_plans() 
    {
        $ID = $_POST['id'];

        $id = array('nSubscriptionIDPK' => $ID);

        $field = array('bIsActive','bIsRemoved');
        $value = array(0,1);
        $Data = array_combine($field, $value);

        $res = $this->model->update($Data, 'tblsubcription', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Subscription deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Subscription");
            echo "false";
        }
    }

    public function createPlans() 
    {
        if ($_POST['submit'] == "submit") 
        {
            if (isset($_POST) && !empty($_POST)) 
            {
                extract($_POST);


                $planField = array('tSubscriptionName','fSubscriptionPrice','nListingLimit','dtSubscriptionExpireOn','dtListingExpireOn');
                $planValue = array($subName,$subIndianPrice,$noListings,$subValidity,$ListingValidity);

                $planData = array_combine($planField, $planValue);

                $planID = $this->model->insert($planData, 'tblsubcription');
                
                if (isset($planID) && $planID > 0) 
                {
                    SessionHandling::set("suc_msg", "Subscription Added successfully");
                    header("Location:../plans/");
                } 
                else if (isset($planID) && $planID == 0) 
                {
                    SessionHandling::set("suc_msg", "Subscription Added successfully");
                    header("Location:../plans/");
                } 
                else 
                {
                    SessionHandling::set("err_msg", "Error while Adding Subscription");
                    header("Location:../plans/");
                }

            }
        } 
        else if ($_POST['submit'] == "Update") 
        {  
            extract($_POST);

            $id = array('nSubscriptionIDPK' => $plansID);

            $planField = array('tSubscriptionName','fSubscriptionPrice','fOtherCountrySubscriptionPrice','nListingLimit','dtSubscriptionExpireOn','dtListingExpireOn');

            $planValue = array($subName,$subIndianPrice,$subOtherCountryPrice,$noListings,$subValidity,$ListingValidity);

            $planData = array_combine($planField, $planValue);

            $planID = $this->model->update($planData, 'tblsubcription', $id);
            
            if (isset($planID) && $planID > 0) 
            {
                SessionHandling::set("suc_msg", "Subscription updated successfully");
                header("Location:../plans/");
            } 
            else if (isset($planID) && $planID == 0) 
            {
                SessionHandling::set("suc_msg", "Subscription updated successfully");
                header("Location:../plans/");
            } 
            else 
            {
                SessionHandling::set("err_msg", "Error while updating Subscription");
                header("Location:../plans/");
            }
        }
    }

    public function edit_plans()   
    {
        $name = 'modules/plans/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $totalFeatures = $this->model->getFetures();
        $this->view->totalFeatures = $totalFeatures;

        $singlePlans = $this->model->singlePlans($ID);
        $this->view->singlePlans = $singlePlans;

        $data = $this->model->getPlansList();
        $this->view->data = $data;

        $this->view->render($name, "Update Subscription", $header, $footer);
    }

    public function view_plans()   
    {
        $name = 'modules/plans/view/plans_detail.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $totalFeatures = $this->model->getFetures();
        $this->view->totalFeatures = $totalFeatures;

        $singlePlans = $this->model->singlePlans($ID);
        $this->view->data = $singlePlans;

        $singleSelectedFeatures = $this->model->singleSelectedFeatures($ID);
        $this->view->singleSelectedFeatures = $singleSelectedFeatures;

        $this->view->render($name, "Subscription Detail", $header, $footer);
    }

   
}