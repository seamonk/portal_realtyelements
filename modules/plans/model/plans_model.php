<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class plans_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);
            
        foreach ($data as $key => $val) {

            //$val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
            
        }
        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    

    public function delete($id, $table, $whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE " . $whereId . " = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    /*---------------------------Plans Features--------------------------------------------*/

    public function getFetures() // fetch all faqs' category
    {
        $sth = $this->db->prepare("SELECT nFeatureIDPK, tFeatureName FROM tblfeatures WHERE bIsFeatureRemoved = 0");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function singleFeatures($id)
    {
        $sth = $this->db->prepare("SELECT nFeatureIDPK, tFeatureName FROM tblfeatures WHERE nFeatureIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

   /*---------------------------Plans Module ----------------------------------*/

    public function getPlansList() // fetch all faqs' category
    {
        $dummyID = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT
                                        nSubscriptionIDPK,
                                        tSubscriptionName,
                                        fSubscriptionPrice,
                                        fOtherCountrySubscriptionPrice,
                                        nListingLimit,
                                        dtSubscriptionExpireOn,
                                        dtListingExpireOn,
                                        bIsActive,
                                        dCreatedOn,
                                        dtRemovedOn,
                                        bIsRemoved,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblsubcription
                                    WHERE
                                        bIsRemoved= 0
                                        AND
                                        bIsActive = 1
                                        AND
                                        bIsDummyData = $dummyID");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getUserPlansList($id) // fetch all faqs' category
    {
        $dummyID = DUMMY_DATA;

        $loggedId = SessionHandling::get('loggedId');

        $wheresql = '';


        if(SessionHandling::get('userType') == ADMIN)
        {
            $wheresql .= '';
        }
        elseif(SessionHandling::get('userType') == DEVELOPER)
        {
            $wheresql .= 'AND  agent.nUserIDFK = '.$id;
        }
        elseif (SessionHandling::get('userType') == AGENT)
        {
            $wheresql .= 'AND  agent.nUserIDFK = '.$id;
        } 

        $sth = $this->db->prepare("SELECT
                                    sub.nSubscriptionIDPK,
                                    agent.nUserIDFK,
                                    users.vUserFirstName,
                                    users.vUserMiddleName,
                                    users.vUserLastName,
                                    agent.nSubscriptionIDFK,
                                    DATE_FORMAT(agent.dtSubscriptionExpireOn, '%d-%m-%Y') as dtSubscriptionExpireOn,
                                    sub.tSubscriptionName,
                                    sub.fSubscriptionPrice,
                                    sub.fOtherCountrySubscriptionPrice,
                                    sub.nListingLimit,
                                    sub.dtSubscriptionExpireOn as SubscriptionExpirecount ,
                                    sub.dtListingExpireOn,
                                    sub.bIsActive,
                                    sub.dCreatedOn,
                                    sub.dtRemovedOn,
                                    sub.bIsRemoved,
                                    sub.nRemovedBy,
                                    sub.bIsDummyData
                                FROM
                                    tblsubcription AS sub,
                                    tblagentsubscription AS agent,
                                    tblusers AS users
                                WHERE
                                    sub.bIsRemoved = 0 
                                    AND 
                                    sub.bIsActive = 1 
                                    AND 
                                    sub.bIsDummyData = $dummyID
                                    AND 
                                    agent.nUserIDFK = users.nUserIDPK
                                    AND
                                    agent.nSubscriptionIDFK = sub.nSubscriptionIDPK
                                    ".$wheresql."
                                    ORDER BY agent.dtSubscriptionExpireOn ASC");
        // echo "<pre>";
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function singlePlans($id)
    {
        $dummyID = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT
                                        nSubscriptionIDPK,
                                        tSubscriptionName,
                                        fSubscriptionPrice,
                                        fOtherCountrySubscriptionPrice,
                                        nListingLimit,
                                        dtSubscriptionExpireOn,
                                        dtListingExpireOn,
                                        bIsActive,
                                        dCreatedOn,
                                        dtRemovedOn,
                                        bIsRemoved,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblsubcription
                                    WHERE
                                        bIsRemoved= 0
                                        AND
                                        bIsActive = 1
                                        AND
                                        bIsDummyData = $dummyID AND nSubscriptionIDPK = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function singleSelectedFeatures($id)
    {
        $sth = $this->db->prepare("SELECT nPlanFeatureIDPK, nFeatureIDFK, bIsPlanFeatureText, tPlanFeatureValue FROM tblplanfeatures WHERE nPlanIDFK = $id AND bIsPlanFeatureRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function checkFeaturesExist($planID,$featureID)
    {
        $sth = $this->db->prepare("SELECT nPlanFeatureIDPK FROM tblplanfeatures WHERE nPlanIDFK = $planID AND nFeatureIDFK = $featureID");

        $sth->execute();
        return $total = $sth->rowCount();
        $row = $sth->fetch();   
    }

    /*-------------------------   Plans Papers ----------------------*/

    public function getAges()
    {
        $sth = $this->db->prepare("SELECT nAgeLevelIDPK, nStartAge, nEndAge, vAgeLevelName, tAgeStandardTitle FROM tblagelevel WHERE bIsRemoved = 0 ORDER BY nAgeLevelIDPK ASC");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getPapersBaseOnCategory($id)
    {
        $sth = $this->db->prepare("SELECT nPaperIDPK, tPaperName FROM tblpapers WHERE nAgeLevelIDFK = $id AND bIsPaperRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getSelectedPlansPapers($selectedPlans,$selectedCategory)
    {   
        $requestData= $_REQUEST;

        $columns = array( 
            0=> '',
            1=> 'pn.tPlanName',
            2 => 'a.nAgeLevelIDPK', 
            3 => 'p.tPaperName'
        );

        $whereSql = '';

        if($selectedPlans != 'all')
        {
            $whereSql .= ' AND pn.nPlanIDPK = '.$selectedPlans;
        }

        if($selectedCategory != 'all')
        {
            $whereSql .= ' AND a.nAgeLevelIDPK = '.$selectedCategory;
        }
        
        $sql = "SELECT pp.nPlanPaperIDPK, pn.tPlanName, p.tPaperName, a.nStartAge, a.nEndAge, a.vAgeLevelName, a.tAgeStandardTitle FROM tblplanpapers as pp LEFT JOIN tblplans as pn ON pp.nPlanIDFK = pn.nPlanIDPK LEFT JOIN tblpapers as p ON pp.nPaperIDFK = p.nPaperIDPK LEFT JOIN tblagelevel as a ON pp.nAgeLevelIDFK = a.nAgeLevelIDPK WHERE pp.bIsPlanPaperRemoved = 0 $whereSql";
 
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $totalData = $sth->rowCount();
        
        if( !empty($requestData['search']['value']) ) 
        {   
            $sql.=" AND ( pn.tPlanName LIKE '%".$requestData['search']['value']."%' ";   

            $sql.=" OR a.vAgeLevelName LIKE '%".$requestData['search']['value']."%' ";
            
            $sql.=" OR a.tAgeStandardTitle LIKE '%".$requestData['search']['value']."%' "; 

            $sql.=" OR p.tPaperName LIKE '%".$requestData['search']['value']."%' )";  
        }

        $sth = $this->db->prepare($sql);
        $sth->execute();
        $totalFiltered = $sth->rowCount();

        $sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']."   "; 
       
        $sth = $this->db->prepare($sql);
        $sth->execute();
      
        //seriol number print
        $i=1+$requestData['start'];
        $data = array();
        while($row = $sth->fetch()) 
        {  
            $nestedData=array();

            $nestedData[] = $i;
            $nestedData[] = $row["tPlanName"];
            $nestedData[] = $row["vAgeLevelName"].' - '.$row["tAgeStandardTitle"].' '.$row["nStartAge"].' & '.$row["nEndAge"];
            $nestedData[] = $row["tPaperName"];
            $nestedData[] = '<form style="display: inline-table;">
                    <button  data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete" type="button" class="btn btn-outline-warning  m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick="setId('.$row['nPlanPaperIDPK'].')"><i class="fa fa-trash-alt"></i></button>
                </form>';

            $data[] = $nestedData;
            $i++;
        }

        $json_data = array(
                    "draw"            => intval( $requestData['draw'] ),   
                    "recordsTotal"    => intval( $totalData ),  // total number of records
                    "recordsFiltered" => intval( $totalFiltered ), 
                    "data"            => $data   // total data array
                    );
        echo json_encode($json_data);  // send data as json format
    }

    public function check_Plans_Paper_Exist($pPlan,$paperId)
    {
        $sth = $this->db->prepare("SELECT nPlanPaperIDPK FROM tblplanpapers WHERE nPlanIDFK = $pPlan AND nPaperIDFK = $paperId");
        
    
        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
}