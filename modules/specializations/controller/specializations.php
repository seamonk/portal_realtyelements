<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Specializations extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/specializations/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllspecializations();
        $this->view->data = $data;

        $this->view->render($name, "specializations", $header, $footer);
    }

    public function createspecializations() // insert specializations
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                $login = SessionHandling::get('loggedId');
               
                $field = array('tSpecializationName','nCreatedBy');
                $value = array($title, $login);

                $specializationsData = array_combine($field, $value);
                // print_r($specializationsData);
                // exit();
                $specializationsID = $this->model->insert($specializationsData, 'tblspecializations');

                if (isset($specializationsID) && $specializationsID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Specializations");
                    header("Location:../specializations/");
                } else {
                    SessionHandling::set("suc_msg", "Specializations added successfully");
                    header("Location:../specializations/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update specializations
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            $login = SessionHandling::get('loggedId');

            $id = array('nSpecializationIDPK' => $nspecializationsIDPK);

            
                $field = array('tSpecializationName');
                $value = array($title);

            $specializationsData = array_combine($field, $value);
            // print_r($specializationsData);
            // exit();

            $specializationsID = $this->model->update($specializationsData, 'tblspecializations', $id);

            if (isset($specializationsID) && $specializationsID > 0) {
                SessionHandling::set("suc_msg", "Specializations updated successfully");
                header("Location:../specializations/listing");
            } else if (isset($specializationsID) && $specializationsID == 0) {
                SessionHandling::set("suc_msg", "Specializations updated successfully");
                header("Location:../specializations/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Specializations");
                header("Location:../specializations/listing");
            }
        }
    }

    public function listing()  // specializations listing
    {
        $name = 'modules/specializations/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllspecializations();
        $this->view->data = $data;

        $this->view->render($name, "specializations Listing", $header, $footer);

    }

    public function editspecializations()   // edit specializations
    {
        $name = 'modules/specializations/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singlespecializationsData = $this->model->fetchSinglespecializations($specializationsID);
        $this->view->singlespecializations = $singlespecializationsData;
         $data = $this->model->getAllspecializations();
        $this->view->data = $data;

        $this->view->render($name, "Update specializations", $header, $footer);
    }
    public function viewspecializations()   // edit specializations
    {
         
        extract($_POST);
        $singlespecializationsData = $this->model->fetchSinglespecializations($Pid);
        echo json_encode($singlespecializationsData);

       
    }
    

    public function deletespecializations() // delete specializations
    {
        $specializationsID = $_POST['id'];

        $id = array('nSpecializationIDPK' => $specializationsID);

        $field = array('bIsRemoved');
        $value = array(1);

        $specializationsData = array_combine($field, $value);

        $res = $this->model->update($specializationsData, 'tblspecializations', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "specializations deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting specializations");
            echo "false";
        }
    }

}