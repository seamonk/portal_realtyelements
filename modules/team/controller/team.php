<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Team extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/team/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllteam();
        $this->view->data = $data;

        $this->view->render($name, "team", $header, $footer);
    }
    public function designations()
    {
        $name = 'modules/team/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getdesignations();
        $this->view->data = $data;

        $this->view->render($name, "team", $header, $footer);
    }
    public function getdesignations()
    {
        
        $data = $this->model->getdesignations();
        echo json_encode($data);
    }
    
    public function createteam() // insert team
    {
        echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                $login = SessionHandling::get('loggedId');
                $files = $_FILES['team_image']['name'];
                if (!empty($_FILES['team_image']['name'])) 
                {
                    $preImgText = date('ymdhsi');
                    $Folder_name =  Urls::$TEAM;
                   
                    $extension = pathinfo($files);
                    // print_r($extension['extension']);
                    // exit();

                    $imageName = $preImgText.".".$extension['extension'];

                    $targetPath = $Folder_name.$preImgText.".".$extension['extension'];
                    move_uploaded_file($_FILES['team_image']['tmp_name'],$targetPath);
                    # code...
                }
                else
                {
                     $imageName = "";
                }


                $field = array('tMemberName','nDesignationIDFK','tImgPath1');
                $value = array($title,$designations,$imageName); 

                $teamData = array_combine($field, $value);

                // print_r($teamData);
                // exit();

                $teamID = $this->model->insert($teamData, 'tblteam');

                if (isset($teamID) && $teamID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Team Member");
                    header("Location:../team/");
                } else {
                    SessionHandling::set("suc_msg", "Team Member added successfully");
                    header("Location:../team/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update team
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nTeamIDPK' => $nteamIDPK);
                $files = $_FILES['team_image']['name'];

            if (!empty($files)) {

                $preImgText = date('ymdhsi');
                $Folder_name =  Urls::$TEAM;
               
                $extension = pathinfo($files);
                // print_r($extension['extension']);
                // exit();

                $imageName = $preImgText.".".$extension['extension'];

                $targetPath = $Folder_name.$preImgText.".".$extension['extension'];


                $field = array('tMemberName','nDesignationIDFK','tImgPath1');
                $value = array($title,$designations,$imageName); 

                move_uploaded_file($_FILES['team_image']['tmp_name'],$targetPath);
                
            }else{

                $field = array('tMemberName','nDesignationIDFK');
                $value = array($title,$designations); 
            }



            $teamData = array_combine($field, $value);
            // print_r($teamData);
            // exit();

            $teamID = $this->model->update($teamData, 'tblteam', $id);

            if (isset($teamID) && $teamID > 0) {
                SessionHandling::set("suc_msg", "Team Member updated successfully");
                header("Location:../team/listing");
            } else if (isset($teamID) && $teamID == 0) {
                SessionHandling::set("suc_msg", "Team Member updated successfully");
                header("Location:../team/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Team Designation");
                header("Location:../team/listing");
            }
        }
    }

     public function createdesignations() // insert designations
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tName');
                $value = array($title); 

                $designationsData = array_combine($field, $value);
                // print_r($designationsData);
                // exit();
                $designationsID = $this->model->insert($designationsData, 'tblteamdesignation');

                if (isset($designationsID) && $designationsID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Team Designation");
                    header("Location:../team/designations/");
                } else {
                    SessionHandling::set("suc_msg", "Team Designation added successfully");
                    header("Location:../team/designations/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update designations
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nDesignationIDPK' => $ndesignationsIDPK);

            
                $field = array('tName');
                $value = array($title);

            $designationsData = array_combine($field, $value);
            // print_r($designationsData);
            // exit();

            $designationsID = $this->model->update($designationsData, 'tblteamdesignation', $id);

            if (isset($designationsID) && $designationsID > 0) {
                SessionHandling::set("suc_msg", "Team Designation updated successfully");
                header("Location:../team/designations");
            } else if (isset($designationsID) && $designationsID == 0) {
                SessionHandling::set("suc_msg", "Team Designation updated successfully");
                header("Location:../team/designations");
            } else {
                SessionHandling::set("err_msg", "Error while updating Team Designation");
                header("Location:../team/designations");
            }
        }
    }
    public function editdesignations()   // edit designations
    {
        $name = 'modules/team/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singledesignationsData = $this->model->fetchSingledesignations($designationsID);
        $this->view->singledesignations = $singledesignationsData;
         $data = $this->model->getAlldesignations();
        $this->view->data = $data;

        $this->view->render($name, "Update designations", $header, $footer);
    }
    public function deletedesignations() // delete designations
    {
        $designationsID = $_POST['id'];

        $id = array('nDesignationIDPK' => $designationsID);

        $field = array('isRemoved');
        $value = array(1);

        $designationsData = array_combine($field, $value);

        $res = $this->model->update($designationsData, 'tblteamdesignation', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "designations deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting designations");
            echo "false";
        }
    }


    public function listing()  // team listing
    {
        $name = 'modules/team/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllteam();
        $this->view->data = $data;

        $this->view->render($name, "team Listing", $header, $footer);

    }

    public function editteam()   // edit team
    {
        $name = 'modules/team/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleteamData = $this->model->fetchSingleteam($teamID);
        $this->view->singleteam = $singleteamData;

         $data = $this->model->getAllteam();
        $this->view->data = $data;

        $this->view->render($name, "Update team", $header, $footer);
    }
    public function viewteam()   // edit team
    {
         
        extract($_POST);
        $singleteamData = $this->model->fetchSingleteam($Pid);
        echo json_encode($singleteamData);

       
    }
    

    public function deleteteam() // delete team
    {
        $teamID = $_POST['id'];

        $id = array('nTeamIDPK' => $teamID);

        $field = array('bIsRemoved');
        $value = array(1);

        $teamData = array_combine($field, $value);

        $res = $this->model->update($teamData, 'tblteam', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "team deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting team");
            echo "false";
        }
    }

}