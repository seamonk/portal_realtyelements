
<style>
    .m-portlet .m-portlet__body {
        padding: 0.7rem 1.7rem !important;
    }
   /* input {
    text-transform: capitalize; ;
    }
    textarea {
    text-transform: capitalize; ;
    }*/
</style>

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->	
    <div class="row">
        <div class="col-lg-12">
          <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
              <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                  <span class="m-accordion__item-icon">
                      <i class="fa flaticon-edit-1"></i>
                  </span>
                  <span class="m-accordion__item-title"> 
                    
                          <?php if (!empty($this->singleteam)) {
                              echo "Update Team";
                          } else {
                              echo "Add Team";
                          } ?>
                     
                    </span>
                  <span class="m-accordion__item-mode"></span>
              </div>
              <?php if (!empty($this->singleteam)) {
                             ?>
                                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php

                          } else {
                              ?>
                                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php
                          } ?>
                  <div class="m-accordion__item-content">
                      <form id="m_form"  class="m-form m-form--label-align-right" enctype ="multipart/form-data"action="<?php echo Urls::$BASE; ?>team/createteam" method="POST">
                          <div class="m-portlet__body">
                              <?php 
                              if (!empty($this->singleteam)) 
                              {
                                ?>
                                    <input type="hidden" name="nteamIDPK" value="<?php echo $this->singleteam[0]['nTeamIDPK']; ?>">
                                    <div class="form-group row">                
                                        <div class="col-lg-6">
                                            <label>Team Member Name</label>
                                            <input type="text"
                                                   class="form-control m-input"
                                                   name="title" id="title"
                                                    value="<?php echo $this->singleteam[0]['tMemberName']?>">
                                        </div>
                                        <div class="col-lg-6">
                                          <label>Team Designations</label>
                                          <select class="form-control m-select2" style="width: 100%" name="designations" id="designationsID">
                                            <option>-- select --</option>
                                          </select>
                                        </div>
                                        <div class="col-lg-12">
                                          <label><h5>Select Team Member Image</h5></label>
                                          <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                              <span class="" onclick="document.getElementById('team_image').click();">
                                                   <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                              </span>
                                          </div>
                                          <br>
                                          <input style="display:none;" type="file" name="team_image"  id="team_image" onchange="vpb_image_preview(this)">

                                          <div style="width:100%;" align="center" id="vpb-display-preview">
                                             <img id="imgProduct" width="100px;" src="<?php echo Urls::$BASE.Urls::$TEAM.$this->singleteam[0]['tImgPath1']?>" >
                                          </div>
                                          </div>
                                    </div>
                                <?php 
                              } 
                              else 
                              { 
                                ?>
                                <div class="form-group row">
                                  <div class="col-lg-6">
                                      <label>Team Member Name</label>
                                      <input type="text"
                                             class="form-control m-input"
                                             name="title" id="title" 
                                             value="">
                                  </div>
                                  <div class="col-lg-6">
                                    <label>Team Designations</label>
                                    <select class="form-control m-select2" style="width: 100%" name="designations" id="designationsID">
                                      <option>-- select --</option>
                                    </select>
                                  </div>
                                  <br>
                                  <div class="col-lg-12">
                                      <br>                                       
                                      <label><h5>Select Team Member Image</h5></label>
                                      <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                      <span class="" onclick="document.getElementById('team_image').click();">
                                          <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                      </span>
                                      </div>
                                      <br>
                                      <input type="hidden" name="deleteImg" id="deleteImg">
                                      <input type="hidden" name="deleteImgs" id="deleteImgs">
                                      <input style="display:none;" type="file" name="team_image"  id="team_image" onchange="vpb_image_preview(this)">
                                      <div style="width:100%;" align="center" id="vpb-display-preview">
                                      </div>
                                   </div>
                                </div>
                              <?php } ?>
                          </div>
                          <?php if (!empty($this->singleteam)) {

                              ?>
                              <div class="s-portlet__foot s-portlet__foot--fit col-lg-12 row">
                                <div class="col-lg-2"></div>
                                  <div class="s-form__actions col-lg-4" style="text-align: center;">
                                      <button type="submit" class="btn btn-danger btn-block" value="update" id="submit" name="submit">Update
                                           Team
                                      </button>
                                  </div>
                                  <div class="s-form__actions col-lg-4">
                                      <button type="reset" onclick="window.location = '<?php echo Urls::$BASE; ?>team/';" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                      </button>
                                  </div>
                                   <div class="col-lg-2"></div>
                              </div>
                          <?php } else { ?>
                              <div class="s-portlet__foot s-portlet__foot--fit row">
                                <div class="col-lg-2"></div>
                                  <div class="s-form__actions col-lg-4" style="text-align: center;">
                                      <button type="submit" class="btn btn-danger btn-block" value="submit" id="submit" name="submit">Add
                                           Team
                                      </button>
                                  </div>
                                  <div class="s-form__actions col-lg-4">
                                      <button type="reset" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                      </button>
                                  </div>
                                  <div class="col-lg-2"></div>
                              </div>
                             
                          <?php }
                          ?>
                      </form>           
                  </div>
              </div>
             
            </div>
          </div>
        </div>
    </div>
     <div class="row">
                  <div class="col-lg-12">
                      <div class="m-portlet m-portlet--mobile">
                          <div class="m-portlet__head">
                              <div class="m-portlet__head-caption">
                                  <div class="m-portlet__head-title">
                                      <h3 class="m-portlet__head-text">
                                        
                                          Team Listing
                                      </h3>
                                  </div>
                              </div>

                          </div>
                          <div class="m-portlet__body">
                              <div class="table-responsive">
                                  <!--begin: Datatable -->
                                  <table class="table table-striped- table-bordered table-hover table-checkable" border="0" id="s1">
                                      <thead align="center">
                                      <tr>
                                          <th>#</th>
                                          <th>Team Member Name</th>
                                          <th>Team Designation</th>
                                          <th>Team Member Image</th>
                                          
                                          <th width="19%">Actions</th>
                                      </tr>
                                      </thead>
                                      <tbody  align="center">
                                      <?php
                                      if (!empty($this->data)) {
                                          for ($i = 0; $i < count($this->data); $i++) { 
                                              ?>
                                              <tr>
                                                  <td><?php echo $i + 1; ?></td>
                                                  
                                                  <td><?php echo $this->data[$i]['tMemberName']; ?></td>
                                                  <td><?php echo $this->data[$i]['tName']; ?></td>
                                                  <td>
                                                    <?php
                                                    if (!empty($this->data[$i]['tImgPath1'])) 
                                                    {
                                                      ?>
                                                        <img  width="50px;" src="<?php echo Urls::$BASE.Urls::$TEAM.$this->data[$i]['tImgPath1']?>"/>
                                                      <?php
                                                    }
                                                    else{
                                                       ?>
                                                        <img  width="50px;" src="<?php echo Urls::$BASE.Urls::$TEAM.'no-image.png'?>"/>
                                                      <?php
                                                    }


                                                    ?></td>
                                                  
                                                  <td>
                                                      <!-- <a class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash"></i></a> -->
                                                      <form method="post" style="display: inline-table;"
                                                            action="<?php echo Urls::$BASE ?>team/editteam">
                                                          <input type="hidden" name="teamID" value="<?php echo $this->data[$i]['nTeamIDPK']; ?>">
                                                          <button style="background-color: transparent !important;border-color: #f4516c !important;color: #f4516c !important;" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Team" type="submit" class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                                          <!-- <button data-toggle="m-tooltip" data-placement="top" type="button"
                                                                  data-original-title=""
                                                                  class="btn_edit btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                                                              <i class="fa fa-pencil-alt"></i></button> -->
                                                      </form>

                                                      <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Team" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"  onclick="setId(<?php echo $this->data[$i]['nTeamIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                  </td>
                                              </tr>
                                              <?php
                                          }
                                      }
                                      ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
      $("#m_form").validate({
            rules: {
                title: {
                    required: true
                },
                designations: {
                    required: true
                } 
            }
        });
       $('#designationsID').select2();
       getdesignations();
        $("#submit").click(function(e) 
        {
            // var Daylent = $(".chkday:checkbox:checked").length;
            toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-center",
                  "preventDuplicates": false,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": "5000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                };

            

        });
        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 

    });
     function getdesignations() 
    {
        $.ajax({

        url: '<?php echo Urls::$BASE ?>team/getdesignations',
        type: 'POST',
        success: function (data) {
            console.log(data);
           var facing = JSON.parse(data);

            //console.log(country.length);
            $('#designationsID').html('');

            for(var i = 0; i < facing.length; i++)
            {
                var data = "<?php if(isset($this->singleteam)) {echo $this->singleteam[0]['nDesignationIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == facing[i].nDesignationIDPK)
                    $('#designationsID').append('<option value='+facing[i].nDesignationIDPK+' selected>'+facing[i].tName+'</option>');
                else
                    $('#designationsID').append('<option value='+facing[i].nDesignationIDPK+'>'+facing[i].tName+'</option>');
            }
            $("#designationsID").trigger("change");
        }

        });
    }
    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this Team ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>team/deleteteam",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>team/";
                        }
                    }
                });
            }
        });
    }
    function vpb_image_preview(vpb_selector_) 
    {

       
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#vpb-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                    console.log('#vpb-display-preview_0');

                       $('#vpb-display-preview').append(
                       '<div id="selector_'+vpb_o_+'" class="vpb_wrapper"> \
                       <img id="image'+vpb_o_+'" width="100px;" name="image[0][] class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br /> \
                       <a  style="cursor:pointer;padding-top:5px;" title="Click here to remove '+ escape(file.name) +'" \
                       onclick="vpb_remove_selected(\''+vpb_o_+'\',\''+file.name+'\')">Remove</a> \
                       </div>');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });

        
    }
    function vpb_remove_selected(id,name)
    {
        var get_value = $('#deleteImg').val();

        if(get_value == '')
        {
            $('#deleteImg').val(name);
        }
        else
        {
            $('#deleteImg').val(get_value+','+name);
        }

         var ds = Array();
        ds.push(id);
        // delete [ds];
        var deta = Array();
        deta.push($('#deleteImg').val());

        $('#deleteImgs').val(deta);
        $('#selector_'+id).html("");
        $('#team_image').val('');
        document.getElementById('selector_'+id).style.display = "none";
        console.log(deta);

        console.log(ds);
       
        // $('#v-add-'+id).remove();
       
}

</script>