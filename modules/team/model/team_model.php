<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class team_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);s
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    public function delete($id, $table, $whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE " . $whereId . " = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllteam()  // fetch all teams for datatable
    {
        $sth = $this->db->prepare("SELECT
                                        team.nTeamIDPK,
                                        team.tMemberName,
                                        des.tName,
                                        team.tImgPath1,
                                        team.tBio,
                                        team.nDesignationIDFK,
                                        team.bIsRemoved
                                    FROM
                                        `tblteam` as team,
                                        tblteamdesignation as des
                                    WHERE 
                                        team.nDesignationIDFK = des.nDesignationIDPK
                                        AND
                                        `bIsRemoved` = 0
                                   ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getdesignations()
    {
        $sth = $this->db->prepare("SELECT
                                        `nDesignationIDPK`,
                                        `tName`,
                                        `isRemoved`
                                    FROM
                                        `tblteamdesignation`
                                    WHERE 
                                        `isRemoved` = 0
                                   ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAlldesignations()  // fetch all designationss for datatable
    {
        $sth = $this->db->prepare("SELECT
                                        `nDesignationIDPK`,
                                        `tName`,
                                        `isRemoved`
                                    FROM
                                        `tblteamdesignation`
                                    WHERE 
                                        `isRemoved` = 0
                                   ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function fetchSingledesignations($id) // fetch single designations for updation
    {
        $sth = $this->db->prepare("SELECT
                                        `nDesignationIDPK`,
                                        `tName`,
                                        `isRemoved`
                                    FROM
                                        `tblteamdesignation`
                                    WHERE 
                                        `isRemoved` = 0
                                         AND 
                                        `nDesignationIDPK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function fetchSingleteam($id) // fetch single team for updation
    {
        $sth = $this->db->prepare("SELECT
                                        team.nTeamIDPK,
                                        team.tMemberName,
                                        des.tName,
                                        team.tImgPath1,
                                        team.tBio,
                                        team.nDesignationIDFK,
                                        team.bIsRemoved
                                    FROM
                                        `tblteam` as team,
                                        tblteamdesignation as des
                                    WHERE 
                                        team.nDesignationIDFK = des.nDesignationIDPK
                                        AND
                                        `bIsRemoved` = 0
                                         AND 
                                        `nTeamIDPK` = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }




}