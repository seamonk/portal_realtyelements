<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:25 PM
 */

class login_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        extract($_POST);

        $sth = $this->db->prepare("SELECT u.nUserIDPK, u.nUserTypeIDFK, u.vUserEmailID, u.tUserPassword, u.vUserFirstName,u.vUserLastName ,u.bIsActive ,ut.nUserTypeName, u.tUserProfileImgPath FROM tblusers as u,tblusertype as ut WHERE u.nUserTypeIDFK = ut.nUserTypeIDPK AND u.nUserTypeIDFK != 1 AND u.bIsActive = 1 AND vUserEmailID=:vUsername AND tUserPassword=:tPassword AND nIsUserDummyData = :nIsUserDummyData AND bIsUserRemoved = :bIsUserRemoved");


        // print_r($sth);
        $sth->execute(array(":vUsername" => $_POST['email'],":tPassword" => $_POST['password'],":nIsUserDummyData" => 0,":bIsUserRemoved" => 0));
        // print_r($sth);
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function portal_run()
    {
        extract($_POST);

        $sth = $this->db->prepare("SELECT u.nUserIDPK, u.nUserTypeIDFK, u.vUserEmailID, u.tUserPassword, u.vUserFirstName,u.vUserLastName ,u.bIsActive ,ut.nUserTypeName, u.tUserProfileImgPath FROM tblusers as u,tblusertype as ut WHERE u.nUserTypeIDFK = ut.nUserTypeIDPK AND u.bIsActive = 1  AND  vUserEmailID=:vUsername AND tUserPassword=:tPassword AND nIsUserDummyData = :nIsUserDummyData AND bIsUserRemoved = :bIsUserRemoved");


        // print_r($sth);
        $sth->execute(array(":vUsername" => $_POST['email'],":tPassword" => $_POST['password'],":nIsUserDummyData" => 0,":bIsUserRemoved" => 0));
        // print_r($sth);
        // exit();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) {
            return $row;
        } else {
            return NULL;
        }
    }

    // public function forgotPassword($tPassword = ""){
    //     extract($_POST);

    //     $sth = $this->db->prepare("SELECT `iLoginID`, `vUsername`, `vType`, `bActive` FROM login WHERE vEmailAddress=:vEmailAddress");
    //     $sth->execute(array(":vEmailAddress" => $_POST['f_email']));

    //     $total = $sth->rowCount();
    //     $row = $sth->fetch();

    //     if ($total > 0) {
    //         $data = [
    //             'tPassword' => md5($tPassword),
    //             'iLoginID' => $row['iLoginID']
    //         ];
    //         $sql = "UPDATE login SET tPassword=:tPassword WHERE iLoginID=:iLoginID";
    //         $stmt= $this->db->prepare($sql);
    //         $stmt->execute($data);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}