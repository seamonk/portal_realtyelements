<?php


class Login extends Controller
{
    public function __construct()
    {
        parent::__construct();

        SessionHandling::init();

    }

    public function index()
    {
        $name = "modules/login/view/index.php";
        $this->view->render($name, "Login");
    }

    public function run()
    {
        extract($_POST);
        $data = $this->model->run();
     
        if (!empty($data)) 
        {

            SessionHandling::set("loggedIn", true);
            
            SessionHandling::set("userTypeID", $data['nUserTypeIDFK']);
            SessionHandling::set("loggedId", $data['nUserIDPK']);   
            SessionHandling::set("userType", $data['nUserTypeName']);
            SessionHandling::set("userName", $data['vUserFirstName']);
            SessionHandling::set("lastName", $data['vUserLastName']);
            SessionHandling::set("emailID", $data['vUserEmailID']);
            SessionHandling::set("userProfile", $data['tUserProfileImgPath']);
            // SessionHandling::unset_key("redirect");


            if ($data['bIsActive'] == 1) 
            {
                echo json_encode(array('status' => true, 'type' => $data['nUserTypeName']));
            } 
            else 
            {
                echo json_encode(array('status' => false, 'message' => 'false'));
            }
        } 
        else 
        {
            echo json_encode(array('status' => false, 'message' => 'true'));
        }
    }
    public function portal_run()
    {
        extract($_POST);
        $data = $this->model->portal_run();
     
        if (!empty($data)) 
        {

            SessionHandling::set("loggedIn", true);
            
            SessionHandling::set("userTypeID", $data['nUserTypeIDFK']);
            SessionHandling::set("loggedId", $data['nUserIDPK']);   
            SessionHandling::set("userType", $data['nUserTypeName']);
            SessionHandling::set("userName", $data['vUserFirstName']);
            SessionHandling::set("lastName", $data['vUserLastName']);
            SessionHandling::set("emailID", $data['vUserEmailID']);
            SessionHandling::set("userProfile", $data['tUserProfileImgPath']);
            // SessionHandling::unset_key("redirect");


            if ($data['bIsActive'] == 1) 
            {
                echo json_encode(array('status' => true, 'type' => $data['nUserTypeName']));
            } 
            else 
            {
                echo json_encode(array('status' => false, 'message' => 'false'));
            }
        } 
        else 
        {
            echo json_encode(array('status' => false, 'message' => 'true'));
        }
    }


}



