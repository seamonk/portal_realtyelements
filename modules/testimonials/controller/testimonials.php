<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/22/2018
 * Time: 10:10 AM
 */

class Testimonials extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
        $logged = SessionHandling::get('loggedIn');
        if ($logged == false) {
            SessionHandling::destroy();
            header("Location:" . URL . "login");
            exit();
        }
    }

    public function index()
    {
        $name = 'modules/testimonials/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getTestimonials();
        $this->view->data = $data;

        $this->view->render($name,"Testimonials", $header, $footer);
    }

    public function add_testimonial()
    {
        $name = 'modules/testimonials/view/add_testimonial.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $this->view->render($name,"Add Testimonial", $header, $footer);
    }

    public function createTestimonial()
    {
        $preImgText = date('ymdhsi');
        if($_POST['submit'] == "submit")
        {
            if (isset($_POST) && !empty($_POST))
            {

                extract($_POST);

                if($_FILES['testimonialImage']['name'] != "")
                {
                    $img = $preImgText.$_FILES['testimonialImage']['name'];
                }
                else
                {
                    $img = "no-image.png";
                }

                $field = array('tTestimonialCLientName','tTestimonialClientImgPath','tTestimonialDescription');

                $value = array($testimonialName,$img,$testimonialDesc);

                $testimonial_data = array_combine($field, $value);

                $targetPath = Urls::$TESTIMONIALS_UPLAODS.$img;
                move_uploaded_file($_FILES["testimonialImage"]["tmp_name"], $targetPath);

                $testimonial_data = $this->model->insert($testimonial_data, 'tbltestimonials');

                if(isset($testimonial_data) && $testimonial_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Testimonial");
                    header("Location:../testimonials/");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Testimonial added successully");
                    header("Location:../testimonials/");
                }
            }
        }
        else if($_POST['submit'] == "update")
        {

            extract($_POST);

            $id = array('nTestimonialIDPK' => $testimonialID);

            if(isset($_FILES['testimonialImage']['name']) && $_FILES["testimonialImage"]["name"][0] != "")
            {
                $img = $preImgText.$_FILES['testimonialImage']['name'];

                $field = array('tTestimonialCLientName','tTestimonialClientImgPath','tTestimonialDescription');

                $value = array($testimonialName,$img,$testimonialDesc);

                $targetPath = Urls::$TESTIMONIALS_UPLAODS.$img;
                move_uploaded_file($_FILES["testimonialImage"]["tmp_name"], $targetPath);
            }
            else
            {
                $field = array('tTestimonialCLientName','tTestimonialDescription');

                $value = array($testimonialName,$testimonialDesc);
            }

            $testimonial_data = array_combine($field, $value);

            $testimonial_id = $this->model->update($testimonial_data, 'tbltestimonials',$id);

            if($testimonial_id > 0)
            {
                SessionHandling::set("suc_msg", "Testimonial updated successully");
                header("Location:../testimonials/");
            }
            else if($testimonial_id == 0)
            {
                SessionHandling::set("suc_msg", "Testimonial updated successully");
                header("Location:../testimonials/");
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating testimonial");
                header("Location:../testimonials/");
            }
        }
    }

    public function testimonial_details()
    {
        $name = 'modules/testimonials/view/testimonial_detail.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getSingleTestimonial($testID);
        $this->view->data = $data;

        $this->view->render($name, "Testimonial Detail",$header,$footer);
    }

    public function send_testimonial()
    {
        $name = 'modules/testimonials/view/add_testimonial.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getSingleTestimonial($testID);

        $this->view->data = $data;

        $this->view->render($name, "Testimonial Update",$header,$footer);
    }

    public function delete_testimonial()
    {
        $testimonial_id = $_POST['id'];

        $id = array('nTestimonialIDPK' => $testimonial_id);

        $field = array('bIsTestimonialRemoved');
        $value = array(1);
        $testimonial_data = array_combine($field, $value);

        $res = $this->model->update($testimonial_data, 'tbltestimonials',$id);

        if($res > 0)
        {
            SessionHandling::set("suc_msg", "Testimonial deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Testimonial");
            echo "false";
        }
    }
}