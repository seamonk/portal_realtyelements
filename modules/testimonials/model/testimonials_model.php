<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/22/2018
 * Time: 10:10 AM
 */

class Testimonials_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  `" . $table . "` (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  `" . $table . "` WHERE `".$whereId."` = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getTestimonials()
    {
        $sth = $this->db->prepare("SELECT `nTestimonialIDPK`, `tTestimonialCLientName`, `tTestimonialClientImgPath`, `tTestimonialDescription`, DATE_FORMAT(dtTestimonialDateTime, '%d-%m-%Y %H:%i') as dtTestimonialDateTime, `bIsTestimonialRemoved` FROM `tbltestimonials` WHERE `bIsTestimonialRemoved` = 0 ORDER BY dtTestimonialDateTime ASC");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getSingleTestimonial($id)
    {
        $sth = $this->db->prepare("SELECT `nTestimonialIDPK`, `tTestimonialCLientName`, `tTestimonialClientImgPath`, `tTestimonialDescription`, `dtTestimonialDateTime`, `bIsTestimonialRemoved` FROM `tbltestimonials` WHERE `nTestimonialIDPK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
}