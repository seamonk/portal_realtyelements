<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    td{ white-space: nowrap; }
    th{text-align: center;}
    .m-portlet .m-portlet__body {
    color: #000000;
    }
    img {
    transition: -webkit-transform 0.55s ease;
    }

    img:active {
        -webkit-transform: scale(3);
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="flaticon-user"></i> Testimonial Listing</h3>
                            </div>

                            <a href="<?php echo Urls::$BASE; ?>testimonials/add_testimonial" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Add Testimonial">
                                <i class="fa fa-plus"></i>
                                <!--                        <i class="la la-ellipsis-h"></i>-->
                            </a>

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th width="10%">#</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if(!empty($this->data)) {
                                    for ($i = 0; $i < count($this->data); $i++) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td>
                                            <td>
                                                <center>
                                                    <?php
                                                    if (!empty($this->data[$i]['tTestimonialClientImgPath'])) 
                                                    {
                                                        ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$TESTIMONIALS_UPLAODS.$this->data[$i]['tTestimonialClientImgPath']; ?>"style="height: 4em;width: 4em; box-shadow: 1px 0px 9px #060606;"> 
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$TESTIMONIALS_UPLAODS.'no-image.png'; ?>"style="height: 4em;width: 4em; box-shadow: 1px 0px 9px #060606;">    
                                                        <?php
                                                    }


                                                    ?>
                                                </center>
                                            </td>
                                            <td align="center"><?php echo $this->data[$i]['tTestimonialCLientName']; ?></td>
                                            <td align="center"><?php echo $this->data[$i]['dtTestimonialDateTime']; ?></td>
                                            <td align="center">
                                                <form method="post" action="<?php echo Urls::$BASE; ?>testimonials/testimonial_details/" style="display: inline-table;">
                                                    <input type="hidden" name="testID" value="<?php echo $this->data[$i]['nTestimonialIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Testimonial" type="button" class="btn_view btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa  fa-eye"></i></button>
                                                </form>
                                                <form method="post" action="<?php echo Urls::$BASE; ?>testimonials/send_testimonial/" style="display: inline-table;">
                                                    <input type="hidden" name="testID" value="<?php echo $this->data[$i]['nTestimonialIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Testimonial" type="button" class="btn_view btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa  fa-pencil-alt"></i></button>
                                                </form>
                                                <form style="display: inline-table;">
                                                    <button  data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Testimonial" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick="setId(<?php echo $this->data[$i]['nTestimonialIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() {

        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
            ]
        } );
    });

    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });

    $(document).on('click',".btn_view", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });

    function setId(del_id)
    {
        swal({
            title:"Are you sure?",
            text:"You want to delete this Testimonial ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes, delete it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE; ?>testimonials/delete_testimonial",
                    data:{'id':del_id},
                    success: function(data)
                    {
                        if(data == "true")
                        {
                            location.href="<?php echo Urls::$BASE; ?>testimonials/";
                        }
                    }
                });
            }
        });
    }

</script>
