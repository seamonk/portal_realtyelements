<style type="text/css">
    .vpb_wrapper {
        max-width:120px;
        border: solid 1px #cbcbcb;
        background-color: #FFF;
        box-shadow: 0 0px 10px #cbcbcb;
        -moz-box-shadow: 0 0px 10px #cbcbcb;
        -webkit-box-shadow: 0 0px 10px #cbcbcb;
        -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
        text-align:center;
        padding:10px;
        padding-bottom:3px;
        font-family:Verdana, Geneva, sans-serif;
        font-size:13px;
        line-height:25px;
        float:left;
        margin-right:20px;
        margin-bottom:20px;
        word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    label {
        font-weight: 400 !important;
    }
    .m-portlet .m-portlet__body {
    color: #000000;
    }

</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    <i class="flaticon-user"></i>
                                    <?php  if (!empty($this->data)) { ?> Update Testimonial <?php }else{ ?> Add Testimonial<?php }?>
                                </h3>
                            </div>
                            <a href="<?php echo Urls::$BASE; ?>testimonials/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Testimonials Listing">
                                <i class="fa fa-list"></i>
                                <!--                        <i class="la la-ellipsis-h"></i>-->
                            </a>

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <?php
                        if (!empty($this->data))
                        {?>
                            <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>testimonials/createTestimonial" enctype = "multipart/form-data" id="update_form" name="update_form">

                                <div class="form-group  row">
                                    <div class="col-lg-4">
                                        <input type="hidden" id="testimonialID" name="testimonialID" value="<?php echo $this->data[0]['nTestimonialIDPK']; ?>" >
                                        <label>Select Testimonial Image[Optional]</label>
                                        <span class="" onclick="document.getElementById('testimonialImage').click();">
                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px; height: 100%">
                                                <input style="display:none;" type="file" name="testimonialImage" id="testimonialImage" onchange="testimonial_image_preview(this)" />
                                                <div align="center" id="testimonial-display-preview" style="height: 100%;">
                                                     <img src="<?php echo Urls::$BASE.Urls::$TESTIMONIALS_UPLAODS.$this->data[0]['tTestimonialClientImgPath']; ?>" style="height: 100%;" class="vpb_image_style" class="img-thumbnail">
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>Testimonial Title</label>
                                                <textarea class="form-control" rows="3.8" name="testimonialName" id="testimonialName"><?php echo $this->data[0]['tTestimonialCLientName']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label>Testimonial Discription</label>
                                        <textarea class="summernote form-control" id="m_summernote_1" required name="testimonialDesc"><?php echo $this->data[0]['tTestimonialDescription']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update_testimonials" >Update Testimonial</button>
                                    </div>
                                    <div class="col-4">
                                        <button type="reset" class="btn btn-secondary btn-block" onclick="window.location.href = '<?php echo Urls::$BASE; ?>testimonials/'">Cancel</button>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </form>

                            <?php
                        }
                        else
                        {?>
                            <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>testimonials/createTestimonial" enctype = "multipart/form-data" id="add_form" name="add_form">
                                <div class="form-group  row">
                                    <div class="col-lg-4">
                                        <label>Select Testimonial Image[Optional]</label>
                                        <span class="" onclick="document.getElementById('testimonialImage').click();">
                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                <input style="display:none;" type="file" name="testimonialImage" id="testimonialImage" onchange="testimonial_image_preview(this)" />


                                                <div align="center" id="testimonial-display-preview">
                                                    <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>Testimonial Name</label>
                                                <textarea class="form-control" rows="7" name="testimonialName" id="testimonialName"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <label>Testimonial Discription</label>
                                        <textarea class="summernote form-control" id="m_summernote_1" required name="testimonialDesc" > </textarea>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit" >Add Testimonial</button>
                                    </div>
                                    <div class="col-4">
                                        <button type="reset" class="btn btn-secondary btn-block">Cancel</button>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </form>
                            <?php
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Body -->
<script type="text/javascript">
    $(document).ready(function()
    {
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-top-center"
        };
        $("#add_form").validate({
            rules: {
                testimonialName: {
                    required: true
                },
                m_summernote_1 : {
                     required: true
                }
            }
        });
        $("#update_form").validate({
            rules: {
                testimonialName: {
                    required: true
                },
                m_summernote_1 : {
                     required: true
                }
            }
        });
        
    });

    function testimonial_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0)
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                    $('#testimonial-display-preview').html('');

                    var reader = new FileReader();

                    reader.onload = function(e)
                    {
                        $('#testimonial-display-preview').append(
                            ' \
                            <img style="height:100%;width:100%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                    }
                    reader.readAsDataURL(file);
                }
            }
            else {  return false; }
        });
    }
</script>


