<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/16/2018
 * Time: 5:06 PM
 */

class News_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  `" . $table . "` (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    
    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  `" . $table . "` WHERE `".$whereId."` = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getNews()
    {
        $sth = $this->db->prepare("SELECT
                                        `nBlogIDPK`,
                                        `dBlogDate`,
                                        `tImgPath`,
                                        `tBlogHeading`,
                                        `tBlogContent`,
                                        `bIsRemoved`
                                    FROM
                                        `tblblogs` WHERE `bIsRemoved` = 0 ORDER BY dBlogDate DESC");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getSingleNews($id)
    {
        $sth = $this->db->prepare("SELECT
                                        `nBlogIDPK`,
                                        `dBlogDate`,
                                        `tImgPath`,
                                        `tBlogHeading`,
                                        `tBlogContent`,
                                        `bIsRemoved`
                                    FROM
                                        `tblblogs` WHERE `bIsRemoved` = 0 AND `nBlogIDPK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

}