<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/16/2018
 * Time: 5:04 PM
 */

class News extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/news/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        $data = $this->model->getNews();
        $this->view->data = $data;

        $this->view->render($name, "News",$header,$footer);
    }

    public function add_news()
    {
        $name = 'modules/news/view/add_news.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $this->view->render($name, "Add News",$header,$footer);
    }

    public function createNews()
    {   
        // echo "<pre>";
        // // print_r($_FILES);
        // print_r($_POST);
        // exit();

        $preImgText = date('ymdhsi');

        if($_POST['submit'] == "submit")
        {
            if (isset($_POST) && !empty($_POST))
            {

                extract($_POST);
                $files = $_FILES['News-image']['name'];

                $extension = pathinfo($files);

                if($_FILES['News-image']['name'] != "")
                {
                    $img = $preImgText.".".$extension['extension'];
                }
                else
                {
                    $img = "";
                }

                $field = array('dBlogDate','tImgPath','tBlogHeading','tBlogContent');

                $value = array($newsDate,$img,$newsTitle,$newsDesc);

                $news_data = array_combine($field, $value);
                // print_r($news_data);
                // exit();
                $targetPath = Urls::$BLOG_UPLAODS.$img;

                move_uploaded_file($_FILES["News-image"]["tmp_name"], $targetPath);

                $news_id = $this->model->insert($news_data, 'tblblogs');

                if(isset($news_id) && $news_id == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding News");
                    header("Location:../news");
                }
                else
                {
                    SessionHandling::set("suc_msg", "News added successully");
                    header("Location:../news/");
                }
            }
        }
        else if($_POST['submit'] == "update")
        {
            // echo "<pre>";
            // print_r($_FILES);
            // print_r($_POST);
            // exit();

            extract($_POST);

            $id = array('nBlogIDPK' => $newsID);

            if($_FILES["News-image"]["name"][0] != "")
            {
                $files = $_FILES['News-image']['name'];

                $extension = pathinfo($files);

                
                $img = $preImgText.".".$extension['extension'];
                

                $field = array('dBlogDate','tImgPath','tBlogHeading','tBlogContent');

                $value = array($newsDate,$img,$newsTitle,$newsDesc);

                $targetPath = Urls::$BLOG_UPLAODS.$img;

                move_uploaded_file($_FILES["News-image"]["tmp_name"], $targetPath);

            }
            else
            {
                $field = array('dBlogDate','tBlogHeading','tBlogContent');

                $value = array($newsDate,$newsTitle,$newsDesc);
            }

            $news_data = array_combine($field, $value);
            // print_r($news_data);
            // exit();
            $news_id = $this->model->update($news_data, 'tblblogs',$id);

            if($news_id > 0)
            {
                SessionHandling::set("suc_msg", "News updated successully");
                header("Location:../news/");
            }
            else if($news_id == 0)
            {
                SessionHandling::set("suc_msg", "News updated successully");
                header("Location:../news/");
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating news");
                header("Location:../news/");
            }
        }
    }

    public function news_details()
    {
        $name = 'modules/news/view/news_detail.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getSingleNews($blockID);
        $this->view->data = $data;

        $this->view->render($name, "News Detail",$header,$footer);
    }

    public function send_news()
    {
        $name = 'modules/news/view/add_news.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getSingleNews($blockID);

        $this->view->data = $data;

        $this->view->render($name, "News Update",$header,$footer);
    }

    public function delete_news()
    {
        $news_id = $_POST['id'];

        $id = array('nBlogIDPK' => $news_id);

        $field = array('bIsRemoved');
        $value = array(1);
        $news_data = array_combine($field, $value);

        $res = $this->model->update($news_data, 'tblblogs',$id);

        if($res > 0)
        {
            SessionHandling::set("suc_msg", "News deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting News");
            echo "false";
        }
    }

}