<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Companies extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/companies/view/companies.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllcompanies();
        $this->view->data = $data;

        $this->view->render($name, "Company", $header, $footer);
    }

    public function createcompanies() // insert companies
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tCompanyName','tCompanyWebsiteLink');
                $value = array($title,$webLink); 

                $companiesData = array_combine($field, $value);
                // print_r($companiesData);
                // exit();
                $companiesID = $this->model->insert($companiesData, 'tblcompanies');

                if (isset($companiesID) && $companiesID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Company");
                    header("Location:../companies/");
                } else {
                    SessionHandling::set("suc_msg", "Company added successfully");
                    header("Location:../companies/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update companies
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nCompanyIDPK' => $ncompaniesIDPK);

            
                $field = array('tCompanyName','tCompanyWebsiteLink');
                $value = array($title,$webLink); 

            $companiesData = array_combine($field, $value);
            // print_r($companiesData);
            // exit();

            $companiesID = $this->model->update($companiesData, 'tblcompanies', $id);

            if (isset($companiesID) && $companiesID > 0) {
                SessionHandling::set("suc_msg", "Company updated successfully");
                header("Location:../companies/listing");
            } else if (isset($companiesID) && $companiesID == 0) {
                SessionHandling::set("suc_msg", "Company updated successfully");
                header("Location:../companies/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Company");
                header("Location:../companies/listing");
            }
        }
    }

    public function listing()  // companies listing
    {
        $name = 'modules/companies/view/companies.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllcompanies();
        $this->view->data = $data;

        $this->view->render($name, "Company Listing", $header, $footer);

    }

    public function editcompanies()   // edit companies
    {
        $name = 'modules/companies/view/companies.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singlecompaniesData = $this->model->fetchSinglecompanies($companyID);
        $this->view->singlecompanies = $singlecompaniesData;
         $data = $this->model->getAllcompanies();
        $this->view->data = $data;

        $this->view->render($name, "Update Company", $header, $footer);
    }
    public function viewcompanies()   // edit companies
    {
         
        extract($_POST);
        $singlecompaniesData = $this->model->fetchSinglecompanies($Pid);
        echo json_encode($singlecompaniesData);

       
    }
    

    public function deletecompanies() // delete companies
    {
        $companiesID = $_POST['id'];

        $id = array('nCompanyIDPK' => $companiesID);

        $field = array('bIsRemoved');
        $value = array(1);

        $companiesData = array_combine($field, $value);

        $res = $this->model->update($companiesData, 'tblcompanies', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Company deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Company");
            echo "false";
        }
    }

}