<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class career_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    public function delete($id, $table, $whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE " . $whereId . " = $id";

        // print_r($sql);
        // exit();
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getAllcareerDesignations()  // fetch all careers for datatable
    {
        $sth = $this->db->prepare("SELECT
                                        nCareerDesignationIDPK,
                                        tDesignationName,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblcareerdesignations
                                    WHERE
                                        bIsRemoved = 0
                                   ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function fetchSinglecareerDesignations($id) // fetch single career for updation
    {
        $sth = $this->db->prepare("SELECT
                                        nCareerDesignationIDPK,
                                        tDesignationName,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblcareerdesignations
                                    WHERE
                                        bIsRemoved = 0
                                         AND 
                                        nCareerDesignationIDPK = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAllcareer()  // fetch all careers for datatable
    {
        $sth = $this->db->prepare("SELECT
                                        career.nCareerIDPK,
                                        career.nCareerDesignationIDFK,
                                        careerDes.tDesignationName,
                                        career.nNumberOfCareerPosition,
                                        career.nExeperienceFrom,
                                        career.nExeperienceTo,
                                        career.nCityIDFK,
                                        city.nCityIDPK,
                                        city.tCityName,
                                        career.tDescription
                                    FROM
                                        tblcareers AS career,
                                        tblcareerdesignations AS careerDes,
                                        tblcities AS city
                                    WHERE
                                        career.nCareerDesignationIDFK = careerDes.nCareerDesignationIDPK
                                        AND
                                        career.nCityIDFK = city.nCityIDPK
                                        AND
                                        career.bIsRemoved = 0
                                        AND
                                        careerDes.bIsRemoved = 0
                                        AND
                                        city.bIsCityRemoved = 0
                                   ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getAllRequest()  // fetch all position Request for datatable
    {
        $sth = $this->db->prepare("SELECT
                                        req.nPositionRequestIDPK,
                                        req.tCandidateName,
                                        req.tCandidateEmailId,
                                        des.nCareerDesignationIDPK,
                                        des.tDesignationName,
                                        req.tComment,
                                        req.tCVFilePath,
                                        req.tCandidateCVFolderPath,
                                        req.bIsRemoved
                                    FROM
                                        tblapplypositionrequest as req,
                                        tblcareerdesignations as des
                                    WHERE
                                        req.nCareerDesignationIDFK = des.nCareerDesignationIDPK
                                        AND
                                        req.bIsRemoved = 0
                                        AND
                                        des.bIsRemoved = 0
                                   ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function fetchSinglecareer($id) // fetch single career for updation
    {
        $sth = $this->db->prepare("SELECT
                                        career.nCareerIDPK,
                                        career.nCareerDesignationIDFK,
                                        careerDes.tDesignationName,
                                        career.nNumberOfCareerPosition,
                                        career.nExeperienceFrom,
                                        career.nExeperienceTo,
                                        career.nCityIDFK,
                                        city.nCityIDPK,
                                        city.tCityName,
                                        career.tDescription
                                    FROM
                                        tblcareers AS career,
                                        tblcareerdesignations AS careerDes,
                                        tblcities AS city
                                    WHERE
                                        career.nCareerDesignationIDFK = careerDes.nCareerDesignationIDPK
                                        AND
                                        career.nCityIDFK = city.nCityIDPK
                                        AND
                                        career.bIsRemoved = 0
                                        AND
                                        careerDes.bIsRemoved = 0
                                        AND
                                        city.bIsCityRemoved = 0
                                        AND
                                        nCareerIDPK = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }



}