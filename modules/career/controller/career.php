<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class career extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/career/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllcareerDesignations();
        $this->view->data = $data;

        $this->view->render($name, "Career Designations", $header, $footer);
    }

    
    public function createcareer() // insert career
    {
        // echo "<pre>";
         $loggedId = SessionHandling::get('loggedId');
        // extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) 
            {
               
                $field = array('nCareerDesignationIDFK','nNumberOfCareerPosition','nExeperienceFrom','nExeperienceTo','nCityIDFK','tDescription','nCreatedBy');
               
                $value = array($Designation,$careerNumber,$exeperienceFrom,$exeperienceTo,$cityID,$description,$loggedId); 

                $careerData = array_combine($field, $value);
                // print_r($careerData);
                // exit();
                $careerID = $this->model->insert($careerData, 'tblcareers');

                if (isset($careerID) && $careerID == 0) 
                {
                    SessionHandling::set("err_msg", "Error while adding Career");
                    header("Location:./listing");
                } 
                else 
                {
                    SessionHandling::set("suc_msg", "Career added successfully");
                    header("Location:./listing");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update career
            extract($_POST);
            // extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nCareerIDPK' => $careerID);

            $field = array('nCareerDesignationIDFK','nNumberOfCareerPosition','nExeperienceFrom','nExeperienceTo','nCityIDFK','tDescription');
               
            $value = array($Designation,$careerNumber,$exeperienceFrom,$exeperienceTo,$cityID,$description); 


            $careerData = array_combine($field, $value);
            // print_r($careerData);
            // exit();

            $careerID = $this->model->update($careerData, 'tblcareers', $id);

            if (isset($careerID) && $careerID > 0) {
                SessionHandling::set("suc_msg", "Career updated successfully");
                header("Location:../career/listing/");
            } else if (isset($careerID) && $careerID == 0) {
                SessionHandling::set("suc_msg", "Career updated successfully");
                header("Location:../career/listing/");
            } else {
                SessionHandling::set("err_msg", "Error while updating Career");
                header("Location:../career/listing/");
            }
        }
    }
     public function createCareerDesignations() // insert career
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tDesignationName');
                $value = array($title); 

                $careerData = array_combine($field, $value);
                // print_r($careerData);
                // exit();
                $careerID = $this->model->insert($careerData, 'tblcareerdesignations');

                if (isset($careerID) && $careerID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Career Designations");
                    header("Location:../career/");
                } else {
                    SessionHandling::set("suc_msg", "Career Designations added successfully");
                    header("Location:../career/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update career
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nCareerDesignationIDPK' => $ncareerdesignationsIDPK);

            
            $field = array('tDesignationName');
            $value = array($title);

            $careerData = array_combine($field, $value);
            // print_r($careerData);
            // exit();

            $careerID = $this->model->update($careerData, 'tblcareerdesignations', $id);

            if (isset($careerID) && $careerID > 0) {
                SessionHandling::set("suc_msg", "Career Designations updated successfully");
                header("Location:../career/");
            } else if (isset($careerID) && $careerID == 0) {
                SessionHandling::set("suc_msg", "Career Designations updated successfully");
                header("Location:../career/");
            } else {
                SessionHandling::set("err_msg", "Error while updating Career Designations");
                header("Location:../career/");
            }
        }
    }

    public function positionrequest()
    {
       $name = 'modules/career/view/position_request.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllRequest();
        $this->view->data = $data;

        $this->view->render($name, "Position Requests", $header, $footer);
    }
    public function listing()  // career listing
    {
        $name = 'modules/career/view/career.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllcareer();
        $this->view->data = $data;

        $this->view->render($name, "career Listing", $header, $footer);

    }

    public function editCareerDesignations()   // edit career
    {
        $name = 'modules/career/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singlecareerData = $this->model->fetchSinglecareerDesignations($careerdesignationsID);
        $this->view->singleCareerDesignations = $singlecareerData;
         $data = $this->model->getAllcareerDesignations();
        $this->view->data = $data;

        $this->view->render($name, "Update career Designations", $header, $footer);
    }
    public function editCareer()   // edit career
    {
        $name = 'modules/career/view/career.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);
        // print_r($_POST);
        // exit();
        $singlecareerData = $this->model->fetchSinglecareer($careerID);
        $this->view->singleCareer = $singlecareerData;
         $data = $this->model->getAllcareerDesignations();
        $this->view->data = $data;

        $this->view->render($name, "Update career Designations", $header, $footer);
    }
    public function viewcareer()   // edit career
    {
         
        extract($_POST);
        $singlecareerData = $this->model->fetchSinglecareerDesignations($Pid);
        echo json_encode($singlecareerData);

       
    }
     public function getDesignation()   // edit career
    {
         
       
        $getDesignation = $this->model->getAllcareerDesignations();
        echo json_encode($getDesignation);

       
    }
    

    public function deletecareer() // delete career
    {
        $careerID = $_POST['id'];

        $id = array('nCareerDesignationIDPK' => $careerID);

        $field = array('bIsRemoved');
        $value = array(1);

        $careerData = array_combine($field, $value);

        $res = $this->model->update($careerData, 'tblcareer', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Career Designations deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Career Designations");
            echo "false";
        }
    }

}