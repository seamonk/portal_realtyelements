

<style>
    .m-portlet .m-portlet__body {
        padding: 0.7rem 1.7rem !important;
    }
   /* input {
    text-transform: capitalize; ;
    }
    textarea {
    text-transform: capitalize; ;
    }*/
</style>

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->	
    <div class="row">
        <div class="col-lg-12">
            <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
              <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                  <span class="m-accordion__item-icon">
                      <i class="fa flaticon-edit-1"></i>
                  </span>
                  <span class="m-accordion__item-title"> 
                    
                          <?php if (!empty($this->singleCareer)) {
                              echo "Update Career";
                          } else {
                              echo "Add Career";
                          } ?>
                     
                    </span>
                  <span class="m-accordion__item-mode"></span>
              </div>
                         <?php if (!empty($this->singleCareer)) {
                             ?>
                                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php

                          } else {
                              ?>
                                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php
                          } ?>
              
                  <div class="m-accordion__item-content">
                    <form id="m_form" class="m-form m-form--label-align-right" enctype ="multipart/form-data"
                          action="<?php echo Urls::$BASE; ?>career/createcareer"
                          method="POST">

                        <div class="m-portlet__body">
                            <?php if (!empty($this->singleCareer)) {
                                ?>
                                <input type="hidden" name="careerID" value="<?php echo $this->singleCareer[0]['nCareerIDPK']; ?>">
                                <div class="form-group row">
                                  <div class="col-lg-8">
                                      <label>Designation Name</label>
                                      <select class="form-control m-select2" style="width: 100%;" name="Designation" id="Designation">   
                                      </select>
                                  </div>
                                  <div class="col-lg-4">
                                      <label>Number Of Career Position</label>
                                     <input type="text" name="careerNumber" onkeypress="return isNumberKey(event);"  value="<?php echo $this->singleCareer[0]['nNumberOfCareerPosition']; ?>" id="careerNumber" class="form-control m-input">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-lg-4">
                                      <label>Exeperience From</label>
                                     <input type="text" name="exeperienceFrom"  onkeypress="return isNumberKey(event);"  value="<?php echo $this->singleCareer[0]['nExeperienceFrom']; ?>" id="exeperienceFrom" class="form-control m-input">
                                  </div>
                                  <div class="col-lg-4">
                                      <label>Exeperience To</label>
                                     <input type="text" name="exeperienceTo"  onkeypress="return isNumberKey(event);"  value="<?php echo $this->singleCareer[0]['nExeperienceTo']; ?>" id="exeperienceTo" class="form-control m-input datepicker">
                                  </div>
                                  <div class="col-lg-4">
                                      <label>City</label>
                                      <select class="form-control m-select2" style="width: 100%;" name="cityID" id="cityID">   
                                      </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-lg-12">
                                      <label>Description</label>
                                     <input type="text" name="description" value="<?php echo $this->singleCareer[0]['tDescription']; ?>" id="description" class="form-control m-input" >
                                  </div>
                                  
                                </div>
                               
                            <?php } else { ?>
                                <div class="form-group row">
                                  <div class="col-lg-8">
                                      <label>Designation Name</label>
                                      <select class="form-control m-select2" style="width: 100%;" name="Designation" id="Designation">   
                                      </select>
                                  </div>
                                  <div class="col-lg-4">
                                      <label>Number Of Career Position</label>
                                     <input type="text" name="careerNumber" onkeypress="return isNumberKey(event);" id="careerNumber" class="form-control m-input">
                                  </div>
                                </div>
                                <div class="form-group row">
                                   <div class="col-lg-4">
                                      <label>Exeperience From</label>
                                     <input type="text" name="exeperienceFrom"  onkeypress="return isNumberKey(event);" id="exeperienceFrom" class="form-control m-input datepicker">
                                  </div>
                                  <div class="col-lg-4">
                                      <label>Exeperience To</label>
                                     <input type="text" name="exeperienceTo"  onkeypress="return isNumberKey(event);" id="exeperienceTo" class="form-control m-input datepicker">
                                  </div> 
                                  <div class="col-lg-4">
                                      <label>city</label>
                                      <select class="form-control m-select2" style="width: 100%;" name="cityID" id="cityID">   
                                      </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <div class="col-lg-12">
                                      <label>Description</label>
                                     <input type="text" name="description" id="description" class="form-control m-input">
                                  </div> 
                                </div>
                              
                            <?php } ?>

                        </div>
                        <?php if (!empty($this->singleCareer)) {

                            ?>
                            <div class="s-portlet__foot s-portlet__foot--fit col-lg-12 row">
                              <div class="col-lg-2"></div>
                                <div class="s-form__actions col-lg-4" style="text-align: center;">
                                    <button type="submit" class="btn btn-danger btn-block" id="submit" value="update" name="submit">Update Carear
                                    </button>
                                </div>
                                <div class="s-form__actions col-lg-4">
                                  <button type="reset" onclick="window.location = '<?php echo Urls::$BASE; ?>career/listing';" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                    </button>
                                   
                                	
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                        <?php } else { ?>
                            <div class="s-portlet__foot s-portlet__foot--fit row">
                              <div class="col-lg-2"></div>
                                <div class="s-form__actions col-lg-4" style="text-align: center;">
                                    <button type="submit" class="btn btn-danger btn-block" id="submit" value="submit" name="submit">Add  Carear
                                    </button>
                                </div>
                                <div class="s-form__actions col-lg-4">
                                    
                                	 <button type="reset" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                    </button>
                                </div>
                                <div class="col-lg-2"></div>

                            </div>
                           
                        <?php }
                        ?>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div></div></div>
     <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              
                                Career Listing
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead align="center">
                            <tr>
                                <th>#</th>
                                <th>Designation Name</th>
                                <th>Number Career Position</th>
                                <th>Exeperience[Years]</th>
                                
                                <th>City Name</th>
                                <!-- <th>Description</th> -->
                              
                                <th width="19%">Actions</th>
                            </tr>
                            </thead>
                            <tbody  align="center">
                            <?php
                            if (!empty($this->data)) {
                                for ($i = 0; $i < count($this->data); $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        
                                         <td><?php echo $this->data[$i]['tDesignationName']; ?></td>
                                         <td><?php echo $this->data[$i]['nNumberOfCareerPosition']; ?></td>
                                         <td><?php echo $this->data[$i]['nExeperienceFrom'] . " - " .$this->data[$i]['nExeperienceTo'];  ?></td>
                                        
                                         <td><?php echo $this->data[$i]['tCityName']; ?></td>
                                         <!-- <td><?php echo $this->data[$i]['tDescription']; ?></td> -->
                                        <td>
                                            <!-- <a class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash"></i></a> -->
                                            <form method="post" style="display: inline-table;"
                                                  action="<?php echo Urls::$BASE ?>career/editcareer">
                                                <input type="hidden" name="careerID"
                                                       value="<?php echo $this->data[$i]['nCareerIDPK']; ?>">
                                                       <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Designation" type="button
                                                       " class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                               
                                            </form>

                                            <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Designation" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"  onclick="setId(<?php echo $this->data[$i]['nCareerIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

         $("#Designation").select2();
         getCity();
          $("#cityID").select2();
         
           toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
         getDesignation();


          $("#m_form").validate({
            rules: {
                Designation: {
                    required: true
                },
                careerNumber: {
                    required: true
                },
                exeperienceFrom: {
                    required: true
                },
                exeperienceTo: {
                    required: true
                },
                cityID: {
                    required: true
                },
                description: {
                    required: true
                }
            }
        });
        
        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 

    });
    $(document).on('focusout','#exeperienceTo', function()
    { 
      
        var startDate = $("#exeperienceFrom").val();

        var endDate = $("#exeperienceTo").val();
        // alert(endDate);

        if (parseInt(endDate) < parseInt(startDate)) 
        {
          $('#exeperienceTo').focus();
          toastr.error('Experience To Should Be Greater Than  '+' '+startDate,'Experience');
            
          document.getElementById("exeperienceTo").value = "";
        }
    });
   
   function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this careerdesignations ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>career/deletecareerdesignations",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>careerdesignations/";
                        }
                    }
                });
            }
        });
    }
    function getDesignation() 
    {
           $.ajax({
              type: "POST",
              url: "<?php echo Urls::$BASE ?>career/getDesignation",
              success: function (data) {
                  console.log(data);
                  $('#Designation').empty();
                
                  $('#Designation').append('<option value="" disabled>-- Select Designation --</option>');
                  var state = JSON.parse(data);
                  for (var i = 0; i < state.length; i++) {

                      var data2 = "<?php if (isset($this->singleCareer)) {
                          echo $this->singleCareer[0]['nCareerDesignationIDFK'];
                      } else {
                          echo NULL;
                      } ?>";

                      if (data2 == state[i].nCareerDesignationIDPK)
                          $('#Designation').append('<option value=' + state[i].nCareerDesignationIDPK + ' selected>' + state[i].tDesignationName + '</option>');
                      else
                          $('#Designation').append('<option value=' + state[i].nCareerDesignationIDPK + '>' + state[i].tDesignationName + '</option>');
                  }
              }
          });
    }
    function getCity() // fetch state list on the basis of the country id
    {
        

        //alert(countryID);

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/fetchCarearCity',
            type: 'POST',
            success: function (data) {
                console.log(data);
                $('#cityID').empty();
                $('#cityID').append('<option value="">-- Select City --</option>');
                var state = JSON.parse(data);
                for (var i = 0; i < state.length; i++) {

                    var data2 = "<?php if (isset($this->singleCareer)) {
                        echo $this->singleCareer[0]['nCityIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == state[i].nCityIDPK)
                        $('#cityID').append('<option value=' + state[i].nCityIDPK + ' selected>' + state[i].tCityName + '</option>');
                    else
                        $('#cityID').append('<option value=' + state[i].nCityIDPK + '>' + state[i].tCityName + '</option>');
                }
            }

        }).done(function () {

        });
    }
     function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
    
</script>