

<style>
    .m-portlet .m-portlet__body {
        padding: 0.7rem 1.7rem !important;
    }
   /* input {
    text-transform: capitalize; ;
    }
    textarea {
    text-transform: capitalize; ;
    }*/
</style>

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->	
    
     <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              
                              Position Requests
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead align="center">
                            <tr>
                                <th>#</th>
                                <th>Position Name</th>
                                <th>Candidate Name</th>
                                <th>Candidate EmailId</th>
                                <!-- <th>CVFilePath</th> -->
                                <th>Comment</th>
                              
                                <th width="19%">Attachments</th>
                            </tr>
                            </thead>
                            <tbody  align="center">
                            <?php
                            if (!empty($this->data)) {
                                for ($i = 0; $i < count($this->data); $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        
                                         <td><?php echo $this->data[$i]['tDesignationName']; ?></td>
                                         <td><?php echo $this->data[$i]['tCandidateName']; ?></td>
                                         <td><?php echo $this->data[$i]['tCandidateEmailId'];  ?></td>
                                        
                                         <td><?php echo $this->data[$i]['tComment']; ?></td>
                                         <td><a target="_blank" href="<?php echo Urls::$BASE.Urls::$CV_UPLOAD.$this->data[$i]['tCVFilePath']; ?>">View Attachments</a></td>
                                       <!--  <td>
                                            
                                            <form method="post" style="display: inline-table;"
                                                  action="<?php echo Urls::$BASE ?>career/editcareer">
                                                <input type="hidden" name="careerID"
                                                       value="<?php echo $this->data[$i]['nCareerIDPK']; ?>">
                                                       <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Designation" type="button
                                                       " class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                               
                                            </form>

                                            <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Designation" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"  onclick="setId(<?php echo $this->data[$i]['nCareerIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                        </td> -->
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

         $("#Designation").select2();
         getCity();
          $("#cityID").select2();
         
           toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
         getDesignation();


          $("#m_form").validate({
            rules: {
                Designation: {
                    required: true
                },
                careerNumber: {
                    required: true
                },
                exeperienceFrom: {
                    required: true
                },
                exeperienceTo: {
                    required: true
                },
                cityID: {
                    required: true
                },
                description: {
                    required: true
                }
            }
        });
        
        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 

    });
    $(document).on('focusout','#exeperienceTo', function()
    { 
      
        var startDate = $("#exeperienceFrom").val();

        var endDate = $("#exeperienceTo").val();
        // alert(endDate);

        if (parseInt(endDate) < parseInt(startDate)) 
        {
          $('#exeperienceTo').focus();
          toastr.error('Experience To Should Be Greater Than  '+' '+startDate,'Experience');
            
          document.getElementById("exeperienceTo").value = "";
        }
    });
   
   function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this careerdesignations ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>career/deletecareerdesignations",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>careerdesignations/";
                        }
                    }
                });
            }
        });
    }
    function getDesignation() 
    {
           $.ajax({
              type: "POST",
              url: "<?php echo Urls::$BASE ?>career/getDesignation",
              success: function (data) {
                  console.log(data);
                  $('#Designation').empty();
                
                  $('#Designation').append('<option value="" disabled>-- Select Designation --</option>');
                  var state = JSON.parse(data);
                  for (var i = 0; i < state.length; i++) {

                      var data2 = "<?php if (isset($this->singleCareer)) {
                          echo $this->singleCareer[0]['nCareerDesignationIDFK'];
                      } else {
                          echo NULL;
                      } ?>";

                      if (data2 == state[i].nCareerDesignationIDPK)
                          $('#Designation').append('<option value=' + state[i].nCareerDesignationIDPK + ' selected>' + state[i].tDesignationName + '</option>');
                      else
                          $('#Designation').append('<option value=' + state[i].nCareerDesignationIDPK + '>' + state[i].tDesignationName + '</option>');
                  }
              }
          });
    }
    function getCity() // fetch state list on the basis of the country id
    {
        

        //alert(countryID);

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/fetchCarearCity',
            type: 'POST',
            success: function (data) {
                console.log(data);
                $('#cityID').empty();
                $('#cityID').append('<option value="">-- Select City --</option>');
                var state = JSON.parse(data);
                for (var i = 0; i < state.length; i++) {

                    var data2 = "<?php if (isset($this->singleCareer)) {
                        echo $this->singleCareer[0]['nCityIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == state[i].nCityIDPK)
                        $('#cityID').append('<option value=' + state[i].nCityIDPK + ' selected>' + state[i].tCityName + '</option>');
                    else
                        $('#cityID').append('<option value=' + state[i].nCityIDPK + '>' + state[i].tCityName + '</option>');
                }
            }

        }).done(function () {

        });
    }
     function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
    
</script>