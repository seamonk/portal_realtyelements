<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Dashboard extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
//        Session::init();
    }


    public function index()
    {
        $userType = SessionHandling::get('userType');

  
        if($userType == ADMIN)
        {
            $name = 'modules/dashboard/view/index.php';
        }
        else if($userType == AGENT)
        {
            $name = 'modules/dashboard/view/agent.php';
        }
        else if($userType == DEVELOPER)
        {
            $name = 'modules/dashboard/view/agent.php';
        }

        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $property = $this->model->getPropertyCount();
        $this->view->property = $property;

        $Allproperty = $this->model->getALLPropertyCount();
        $this->view->Allproperty = $Allproperty;

        $AllProject = $this->model->getALLProjectCount();
        $this->view->AllProject = $AllProject;
        
 
        $users = $this->model->getUsersCount();
        $this->view->users = $users;

        $Agents = $this->model->getAgentsCount();
        $this->view->Agents = $Agents;

        $Developer = $this->model->getDevelopersCount();
        $this->view->Developer = $Developer;

        $AgentsListingLimit = $this->model->getAgentListingLimit();
        $this->view->AgentsListingLimit = $AgentsListingLimit;
        

        $getAgentsLeads = $this->model->getAgentsLeads();
        $this->view->getAgentsLeads = $getAgentsLeads;

        $events = $this->model->getEventsCount();
        $this->view->events = $events;

        $loggedId = SessionHandling::get("loggedId");
        
        $data = $this->model->getProfileDetail($loggedId);
        $this->view->data = $data;

        $profileViews = $this->model->getProfileViews($loggedId);
        $this->view->profileViews = $profileViews;

        $getAgentContact = $this->model->getAgentContact($loggedId);
        $this->view->getAgentContact = $getAgentContact;

        $getAgentLanguages = $this->model->getAgentLanguages($loggedId);
        $this->view->getAgentLanguages = $getAgentLanguages;

        $getAgentAssociations = $this->model->getAgentAssociations($loggedId);
        $this->view->getAgentAssociations= $getAgentAssociations;

        $getSpecializations = $this->model->getAgentSpecializations($loggedId);
        $this->view->getAgentSpecializations= $getSpecializations;

        $getLocality = $this->model->getAgentLocality($loggedId);
        $this->view->getLocality= $getLocality;
        

        $this->view->render($name,"Home", $header, $footer);
    }

    public function getLeadsCount()
    {
        $getAgentsLeads = $this->model->getAgentsLeads();
        echo json_encode($getAgentsLeads);
    }

    public function logout()
    {
        SessionHandling::destroy();
        header("Location: ".Urls::$BASE);
        exit();
    }   
}