<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class dashboard_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPropertyCount()
    {
        $loggedId = SessionHandling::get("loggedId");
        
        $sth = $this->db->prepare("SELECT
                                        *
                                    FROM
                                        `tblproperties`
                                    WHERE
                                        bIsAvailable = 1 AND bIsRemoved = 0 AND nCreatedBy = $loggedId
                                    GROUP BY
                                        nPropertyIDPK
                                       ");
        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
    public function getALLPropertyCount()
    {
        $loggedId = SessionHandling::get("loggedId"); 
        $sth = $this->db->prepare("SELECT
                                        *
                                    FROM
                                        `tblproperties`
                                    WHERE
                                        bIsAvailable = 1 AND bIsRemoved = 0 AND bIsProperty = 0
                                    GROUP BY
                                        nPropertyIDPK
                                       ");
        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
    public function getALLProjectCount()
    {
        $loggedId = SessionHandling::get("loggedId"); 
        $sth = $this->db->prepare("SELECT
                                        *
                                    FROM
                                        `tblproperties`
                                    WHERE
                                        bIsAvailable = 1 AND bIsRemoved = 0 AND bIsProperty = 1
                                    GROUP BY
                                        nPropertyIDPK
                                       ");
        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
     public function getAgentListingLimit()
    {
        $loggedId = SessionHandling::get("loggedId"); 
        $sth = $this->db->prepare("SELECT `nAddListingUpto` FROM `tblusers` WHERE `bIsUserRemoved` = 0 AND `nIsUserDummyData` = 0 AND nUserIDPK = $loggedId");
        // print_r($sth);
        // exit();
        $sth->execute();
        // $total = $sth->rowCount();
        $row = $sth->fetch();

        if (!empty($row)) 
        {
            return $row['nAddListingUpto'];
        } 
        else 
        {
            return NULL;
        }
    }
    public function getUsersCount()
    {
        $loggedId = SessionHandling::get("loggedId"); 
        $sth = $this->db->prepare("SELECT `tUserName` FROM `tblusers` WHERE `bIsUserRemoved` = 0 AND `nIsUserDummyData` = 0 ");

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }

    public function getAgentsCount()
    {
        $sth = $this->db->prepare("SELECT * FROM `tblusers` WHERE nUserTypeIDFK = 2 AND  bIsActive =1 AND bIsUserRemoved = 0" );

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
     public function getDevelopersCount()
    {
        $sth = $this->db->prepare("SELECT * FROM `tblusers` WHERE nUserTypeIDFK = 3 AND bIsActive =1 AND bIsUserRemoved = 0" );

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
    public function getAgentsLeads()
    {
        $loggedId = SessionHandling::get("loggedId"); 

        $whereSQL = '';

        if(SessionHandling::get('userType'))
        {
            if (SessionHandling::get('userType') == DEVELOPER)
            {
                $whereSQL .= " AND nPropertyIdFK IS NOT NULL ";
            }
        }

        $sth = $this->db->prepare("SELECT
                                        `nLeadIDPK`,
                                        `nUserIDFK`,
                                        `tCustomerName`,
                                        `vCustomerrMobileNo`,
                                        `tCustomerEmailId`,
                                        `dtCreatedOn`,
                                        `bIsRemoved`
                                    FROM
                                        `tblleads`
                                    WHERE
                                       nUserIDFK = $loggedId AND bIsRemoved = 0".$whereSQL);

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }

    public function getEventsCount()
    {
        $sth = $this->db->prepare("SELECT `tEventTitle` FROM `tbleventdetails` WHERE `bIsEventRemoved` = 0 AND `bIsEventDummyData` = 0");

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }

// Start Agent Profile Details

    public function getAgentLanguages($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                    agenlang.nAgentLanguageKnownIDPK,
                                    agenlang.nAgentIDFK,
                                    lang.nLanguageIDPK,
                                    lang.tLanguageName,
                                    agenlang.nLanguageIDFK,
                                    agenlang.dtCreatedOn,
                                    agenlang.nCreatedBy,
                                    agenlang.bIsRemoved,
                                    agenlang.dtRemovedOn,
                                    agenlang.nRemovedBy,
                                    agenlang.bIsDummyData
                                FROM
                                    tblagentlanguagesknown as agenlang,
                                    tbllanguages as lang
                                WHERE
                                    agenlang.nAgentIDFK = $loggedId
                                    AND
                                    agenlang.nLanguageIDFK = lang.nLanguageIDPK
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAgentContact($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                    agentContact.nAgentContactNumberIDPK,
                                    agentContact.nAgentIDFK,
                                    agentContact.vContactNumber,
                                    agentContact.bIsMobileNumberVerified,
                                    agentContact.bIsRemoved,
                                    agentContact.dtRemovedOn,
                                    agentContact.nRemovedBy,
                                    agentContact.dtCreatedBy,
                                    agentContact.dtCreatedOn,
                                    agentContact.bIsDummyData
                                FROM
                                    tblagentcontactnumbers as agentContact ,
                                    tblagents as agent
                                WHERE
                                   
                                    agent.nAgentIDPK = agentContact.nAgentIDFK 
                                    AND
                                    agent.nUserIDFK = $loggedId
                                    ");

        // print_r($sth);
        // exit();  
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getAgentAssociations($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                        agentassoci.nAssociationIDFK,
                                        associ.nAssociationIDPK,
                                        associ.tAssociationName,
                                        associ.nAssociationIconPath,
                                        associ.dtCreatedOn,
                                        associ.nCreatedBy,
                                        associ.bIsRemoved,
                                        associ.dtRemovedOn,
                                        associ.nRemovedBy,
                                        associ.bIsDummyData
                                    FROM
                                        tblassociations AS associ,
                                        tblagentassociations AS agentassoci
                                    WHERE
                                        agentassoci.nAssociationIDFK = associ.nAssociationIDPK 
                                        AND 
                                        agentassoci.nAgentIDFK = $loggedId
                                      ");
        //  print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAgentDesignation($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                        nDesignationIDPK,
                                        tDesignationName,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tbldesignations
                                    WHERE
                                        bIsRemoved = 0
                                         WHERE
                                    nAgentIDFK = $loggedId
                                      ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getAgentSpecializations($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                        
                                        agentSpe.nSpecializationIDFK
                                        
                                    FROM
                                        tblspecializations as spe,
                                        tblagentspecializations agentSpe
                                    WHERE
                                    agentSpe.nAgentIDFK = $loggedId
                                    AND
                                    agentSpe.nSpecializationIDFK = spe.nSpecializationIDPK
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAgentLocality($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                    locexp.nAgentLocalityIDPK,
                                    locexp.nAgentIDFK,
                                    locexp.nLocalityIDFK,
                                    loc.nLocalityIDPK,
                                    loc.nAreaIDFK,
                                    a.tAreaName,
                                    ct.tCityName,
                                    loc.nCityIDFK,
                                    s.tStateName,
                                    loc.nStateIDFK,
                                    loc.tLocalityName,
                                    loc.dLocalityLatitude,
                                    loc.dLocalityLongitude,
                                    loc.dLocalityCreatedOn,
                                    loc.nLocalityCreatedBy,
                                    loc.bIsLocalityRemoved
                                FROM
                                    tbllocalities AS loc,
                                    tblagentlocalitiesexpert as locexp,
                                    tblareas AS a,
                                    tblcities AS ct,
                                    tblstates AS s,
                                    tblcountries AS c
                                WHERE
                                    locexp.nAgentIDFK = $loggedId
                                    AND
                                    locexp.nLocalityIDFK = loc.nLocalityIDPK
                                    AND
                                    loc.nAreaIDFK = a.nAreaIDPK AND loc.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.bIsCityRemoved = 0 AND s.bIsStateRemoved = 0 AND c.bIsCountryRemoved = 0 
                                ORDER BY
                                  a.tAreaName ASC");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getProfileDetail($id)
    {
       $sth = $this->db->prepare("SELECT
                                    tblagents.nAgentIDPK,
                                    tblusers.nUserIDPK,
                                    tblusers.tUserName,
                                    tblusers.vUserFirstName,
                                    tblusers.vUserMiddleName,
                                    tblusers.vUserLastName,
                                    tblusers.eUserGender,
                                    tblusers.tUserProfileImgPath,
                                    tblusers.bIsMobileNumberVerified,
                                    tblusers.bIsEmailIdVerified,
                                    tblusers.tVerificationOTP,
                                    tblusers.vUserEmailID,
                                    tblusers.nUserPhoneNumber,
                                    tblusers.tUserProfileImgPath,
                                    tblagents.nAgentReraID,
                                    tblagents.nCompanyIDFK,
                                    tblagents.nDesignationIDFK,
                                    tblagents.nAgentExperience,
                                    tblagents.tAgentDescription,
                                    tblagents.vAgentMobileNumber2,
                                    tblagents.vAgentLandlineNumber,
                                    tblagents.tAgentAddress,
                                    tblagents.nAgentTotalViews,
                                    tblagents.nAreaIDFK,
                                    tblagents.nCityIDFK,
                                    tblagents.nStateIDFK,
                                    tblagents.nCountryIDFK,
                                    tblagents.bIsAgentReraCertified,
                                    tblagents.tGoogleMapAddressLink,
                                    tblagents.tAgentCompanyWebsiteLink,
                                    tblagents.tAgentFacebookLink,
                                    tblagents.tAgentLinkedinLink,
                                    tblagents.tAgentTwitterLink,
                                    tblagents.tAgentYoutubeLink,
                                    tblagents.tAgentInstaLink,
                                    tblagents.bIsAgentVerifiedFromAdmin,
                                    tbldesignations.tDesignationName,
                                    tblcompanies.tCompanyName,
                                    tblcities.tCityName,
                                    tblstates.tStateName
                                FROM
                                    tblagents
                                LEFT JOIN tblusers
                                ON tblagents.nUserIDFK = tblusers.nUserIDPK
                                LEFT JOIN tbldesignations 
                                ON tblagents.nDesignationIDFK = tbldesignations.nDesignationIDPK
                                LEFT JOIN tblcities
                                ON tblagents.nCityIDFK = tblcities.nCityIDPK
                                LEFT JOIN tblstates
                                ON tblagents.nStateIDFK = tblstates.nStateIDPK
                                LEFT JOIN tblcompanies
                                ON tblagents.nCompanyIDFK = tblcompanies.nCompanyIDPK
                                WHERE
                                    tblusers.nUserIDPK = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetch();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getProfileViews($id)
    {
        $sth = $this->db->prepare("SELECT
                                    
                                    tblagents.nAgentTotalViews
                                   
                                FROM
                                    tblagents
                                LEFT JOIN tblusers
                                ON tblagents.nUserIDFK = tblusers.nUserIDPK
                                LEFT JOIN tbldesignations 
                                ON tblagents.nDesignationIDFK = tbldesignations.nDesignationIDPK
                                LEFT JOIN tblcities
                                ON tblagents.nCityIDFK = tblcities.nCityIDPK
                                LEFT JOIN tblstates
                                ON tblagents.nStateIDFK = tblstates.nStateIDPK
                                LEFT JOIN tblcompanies
                                ON tblagents.nCompanyIDFK = tblcompanies.nCompanyIDPK
                                WHERE
                                    tblusers.nUserIDPK = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetch();
        if (!empty($row)) {
            return $row['nAgentTotalViews'];
        } else {
            return NULL;
        }
    }


// End Agent Profile Details
}