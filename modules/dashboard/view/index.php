<style type="text/css">
    .m-widget24 .m-widget24__item .m-widget24__title {
        font-size: 17px;
    }
    .m-widget24 .m-widget24__item .m-widget24__stats 
    {
        font-size: 1.2rem;
    }
    .m--font-brand
    {
        color: #5f5f5f  !important;
    }
    .m--font-danger
    {
        color: #f4516c  !important;
    }
</style>
<!-- END: Subheader -->
<!-- <?php echo urls::$BASE; ?> -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="m-portlet  m-portlet--unair">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <a href="<?php echo Urls::$BASE; ?>agent/">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title m--font-danger">
                                        Agents
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
                                        <!-- Customer Review -->
                                    </span>
                                    <span class="m-widget24__stats m--font-brand">
                                        <?php if($this->Agents < 10){ echo '0'.$this->Agents;}else{echo $this->Agents;}?>
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar progress-bar-animated progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo $this->Agents/ 10;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <a href="<?php echo Urls::$BASE; ?>users/">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title m--font-danger">
                                        Developer
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
                                        <!-- Customer Review -->
                                    </span>
                                    <span class="m-widget24__stats m--font-brand">
                                        <?php if($this->Developer < 10){ echo '0'.$this->Developer;}else{echo $this->Developer;} ?>
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar  progress-bar-animated progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo $this->Developer / 10;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
                                        <!-- Change -->
                                    </span>
                                    <span class="m-widget24__number">
                                      <!--  65% -->
                                    </span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <a href="<?php echo Urls::$BASE; ?>properties/">
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title m--font-danger">
                                        Properties

                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
                                        <!-- Fresh Order Amount -->
                                    </span>
                                    <span class="m-widget24__stats m--font-brand">
                                        <?php if($this->Allproperty < 10){ echo "0".$this->Allproperty; }else{echo $this->Allproperty;} ?>
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar progress-bar-animated progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo $this->Allproperty / 10;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
                                       <!--  Change -->
                                    </span>
                                    <span class="m-widget24__number">
                                       <!--  56% -->
                                    </span>
                                </div>
                            </a>
                        </div>
                    </div>
                    
                
                    
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <!-- <a href="<?php echo Urls::$BASE; ?>events/"> -->
                                <div class="m-widget24__item">
                                    <h4 class="m-widget24__title m--font-danger">
                                        Projects
                                    </h4>
                                    <br>
                                    <span class="m-widget24__desc">
                                        <!-- Fresh Order Amount -->
                                    </span>
                                    <span class="m-widget24__stats m--font-brand">
                                        <?php if($this->AllProject < 10){ echo '0'.$this->AllProject;}else{echo $this->AllProject;} ?>
                                    </span>
                                    <div class="m--space-10"></div>
                                    <div class="progress m-progress--sm">
                                        <div class="progress-bar progress-bar-animated progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo $this->AllProject / 10;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="m-widget24__change">
                                       <!--  Change -->
                                    </span>
                                    <span class="m-widget24__number">
                                        <!-- 75% -->
                                    </span>
                                </div>
                            <!-- </a> -->
                        </div>
                    </div>
                </div>
            </div>

    </div>
    
    <!--End::Section-->
</div>
