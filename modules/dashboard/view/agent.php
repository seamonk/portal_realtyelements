
<!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> -->
<!-- <link href="<?php //echo Urls::$COMMONS ?>assets/custom/jQuery-plugin-progressbar.css" rel="stylesheet" type="text/css" /> -->
<script type="text/javascript" src="<?php echo Urls::$COMMONS ?>assets/custom/plugin.js"></script>

<style type="text/css">
    .m-widget24 .m-widget24__item .m-widget24__title {
        font-size: 17px;
    }
    .m-widget24 .m-widget24__item .m-widget24__stats 
    {
        font-size: 1.2rem;
    }
    .m--font-brand
    {
        color: #5f5f5f  !important;
    }
    .m--font-danger
    {
        color: #f4516c  !important;
    }
    .circular-progress-bar {
        position: relative;
        margin: 0 auto;
    }

    .progress-percentage, .progress-text {
        position: absolute;
        width: 100%;
        top: 55%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
        padding: 0px 0px;
    }

    .progress-percentage {
        font-size: 18px;
        transform: translate(-50%, -85%);
        color: #5f5f5f !important;
       font-weight: 600 !important
       ;
    }

    .progress-text {
        transform: translate(-50%, 0%);
        color: #f4516c !important;
        font-size: 10px;
    }

</style>
<style>
            * {
                box-sizing: border-box;
                margin: 0;
                padding: 0;
                border: 0;
            }
            .container { margin:150px auto; text-align:center;}
        </style>
<!-- END: Subheader -->
<div class="m-content">
    <!--Begin::Section-->
    <div class="m-portlet  m-portlet--unair">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-6 col-lg-12 col-xl-3">
                    <div class="m-widget24">
                        <a href="<?php echo Urls::$BASE; ?>properties/">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title m--font-danger">
                                    Listings

                                </h4>
                                <br>
                                <span class="m-widget24__desc">
                                    <!-- Fresh Order Amount -->
                                </span>
                                <span class="m-widget24__stats m--font-brand">
                                    <?php 
                                        if($this->property < 10)
                                        {
                                            echo "0".$this->property; 
                                        }
                                        else
                                        {
                                            echo $this->property;
                                        } 
                                    ?>
                                    / 

                                    <?php 
                                        if($this->AgentsListingLimit < 10)
                                        {
                                            echo "0".$this->AgentsListingLimit; 
                                        }
                                        else
                                        {
                                            echo $this->AgentsListingLimit;
                                        }   
                                         // echo $this->AgentsListingLimit;
                                    ?>
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar progress-bar-animated progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo ($this->property / $this->AgentsListingLimit) * 100 ?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                   <!--  Change -->
                                </span>
                                <span class="m-widget24__number">
                                   <!--  56% -->
                                </span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-12 col-xl-3">
                    <div class="m-widget24">
                        <a href="<?php echo Urls::$BASE; ?>leads/">
                            <div class="m-widget24__item">
                                <h3 class="m-widget24__title m--font-danger">
                                    Leads
                                </h3>
                                <br>
                                <span class="m-widget24__desc">
                                    <!-- Customer Review -->
                                </span>
                                <span class="m-widget24__stats m--font-brand">
                                    <?php
                                        if($this->getAgentsLeads < 10)
                                        { 
                                            echo '0'.$this->getAgentsLeads;
                                        }
                                        else
                                        {
                                            echo $this->getAgentsLeads;
                                        } 
                                    ?> 
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar  progress-bar-animated  progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo $this->getAgentsLeads;?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    <!-- Change -->
                                </span>
                                <span class="m-widget24__number">
                                  <!--  65% -->
                                </span>
                            </div>
                        </a>
                    </div>
                </div>
                 <div class="col-md-6 col-lg-12 col-xl-3">
                    <div class="m-widget24">
                        <a href="<?php echo Urls::$BASE; ?>agent/profile/">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title m--font-danger">
                                    Profile Views

                                </h4>
                                <br>
                                <span class="m-widget24__desc">
                                    <!-- Fresh Order Amount -->
                                </span>
                                <span class="m-widget24__stats m--font-brand">
                                    <?php 
                                        if($this->profileViews < 10)
                                        {
                                            echo "0".$this->profileViews; 
                                        }
                                        else
                                        {
                                            echo $this->profileViews;
                                        } 
                                    ?>
                                   
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm ">
                                    <div class="progress-bar progress-bar-animated  progress-bar-striped m--bg-danger" role="progressbar" style="width: <?php echo ($this->profileViews / 100)?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                   <!--  Change -->
                                </span>
                                <span class="m-widget24__number">
                                   <!--  56% -->
                                </span>
                            </div>
                        </a>
                    </div>
                </div>
               
                <div class="col-md-6 col-lg-12 col-xl-3">
                   
                    <div class="m-widget24">
                        <a href="<?php echo Urls::$BASE; ?>agent/profile/">
                            <div class="m-widget24__item ">
                                <h4 class="m-widget24__title m--font-danger">
                                    Profile Completed
                                </h4>
                                <div class="my-progress-bar"></div>
                               
                            </div>
                        </a>
                    </div>
                </div>
            
               <!--  -->
            </div>
        </div>
    </div>
        <!--begin:: Widgets/Application Sales-->
    <div class="m-portlet ">
        <div class="m-portlet__body  m-portlet__body--no-padding">
            <div class="row m-row--no-padding m-row--col-separator-xl">
               
                
            </div>
        </div>
    </div>
        <!--end:: Widgets/Application Sales-->
    <!--End::Section-->
</div>
<script>
       $(document).ready(function() 
       {    

            var data = 
            $(".my-progress-bar").circularProgress({
                line_width: 7,
                color: "#f4516c",
                starting_position: 0, // 12.00 o' clock position, 25 stands for 3.00 o'clock (clock-wise)
                percent: 0,
                counter_clockwise: false,
                percentage: true
            }).circularProgress('animate', <?php
                                                if (SessionHandling::get('userType') == DEVELOPER) 
                                                {
                                                    $profileCOmpleatedcount = 10;
                                                    if (!empty($this->data['tUserProfileImgPath'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                         
                                                    }
                                                    if (!empty($this->data['vUserMiddleName'])) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->data['vUserEmailID'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 10; 
                                                         
                                                    }
                                                    if (!empty($this->data['eUserGender'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 10; 
                                                         
                                                    }
                                                       
                                                    if (!empty($this->data['tAgentDescription']) || !empty($this->data['tAgentLinkedinLink']) && !empty($this->data['tAgentFacebookLink']) && !empty($this->data['tAgentInstaLink']))
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 40; 
                                                         
                                                    }
                                                    if (!empty($this->data['nUserPhoneNumber']) || !empty($this->getAgentContact)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    else{
                                                            //echo $profileCOmpleatedcount.' %';
                                                    }
                                                }
                                                else if (SessionHandling::get('userType') == AGENT) 
                                                {
                                                    $profileCOmpleatedcount = 5;

                                                    if (!empty($this->data['tUserProfileImgPath'])) 
                                                    {
                                                         $profileCOmpleatedcount = 10;
                                                         
                                                    }
                                                    if (!empty($this->data['vUserMiddleName'])) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    }
                                                    if (!empty($this->data['vUserEmailID'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 15; 
                                                         
                                                    }
                                                       
                                                    if (!empty($this->data['tAgentDescription']) || !empty($this->data['tAgentLinkedinLink']) && !empty($this->data['tAgentFacebookLink']) && !empty($this->data['tAgentInstaLink']))
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 5; 
                                                         
                                                    }
                                                    if (!empty($this->data['nAgentReraID'])) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->data['nUserPhoneNumber']) || !empty($this->getAgentContact)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getAgentLanguages)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getAgentAssociations)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getAgentSpecializations)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getLocality))
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    }
                                                    if (!empty($this->data['nCompanyIDFK']) && !empty($this->data['nDesignationIDFK']))
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    }
                                                   
                                                    if (!empty($this->data['nAgentExperience']))
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    } 
                                                    else{
                                                            //echo $profileCOmpleatedcount.' %';
                                                    }
                                                }

                                                echo $profileCOmpleatedcount-0.5;
                                                ?>, 1500);
            
            
        });


</script>