<style type="text/css">
    .paddingLeft {
        padding-left: 0px !important;
    }

    .paddinTop {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head"
                             data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-map-location"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleTypes)) {
                                    echo "Update Derived Types";
                                } else {
                                    echo "Add Derived Types";
                                } ?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" "
                             role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                                <!--                                <div class="m-portlet">-->
                                <!--begin::Form-->
                                <?php
                                if (!empty($this->singleTypes)) {
                                    for ($i = 0; $i < count($this->singleTypes); $i++) {
                                        ?>
                                        <form class="m-form" action="<?php echo Urls::$BASE; ?>properties/createDerivedType" method="POST">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <input type="hidden" name="derivedtypeID"
                                                                   value="<?php echo $this->singleTypes[$i]['nPropertySubTypeIDPK']; ?>">
                                                            <div class="col-lg-6">
                                                                <label>Select Sub Type</label>
                                                                <select class="form-control m-select2" name="SubType" id="select2Country" placeholder="Country Name"></select>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label>Derived Type Name</label>
                                                                <input type="text" class="form-control m-input" name="DerivedType" value="<?php echo $this->singleTypes[$i]['tPropertyDerivedTypeName'] ?>" placeholder="Derived Type Name" id="subtypeName">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update Derived Type
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="reset" class="btn default btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>properties/derivedtype';">

                                                            Cancel
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-2"></div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <form class="m-form" action="<?php echo Urls::$BASE; ?>properties/createDerivedType"
                                          method="POST">
                                        <!--                                            <div class="m-portlet__body">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label>Select Sub Type</label>
                                                        <select class="form-control m-select2" name="SubType" id="select2Country"></select placeholder="Country Name">

                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Derived Type Name</label>
                                                        <input type="text" class="form-control m-input" name="DerivedType" placeholder="SubType Name" id="subtypeName">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--                                            </div>-->
                                        <!--                                            <div class="m-portlet__foot m-portlet__foot--fit">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" id="submit" value="submit">
                                                    Add Derived Type
                                                </button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block" >Cancel
                                                </button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <!--                                            </div>-->
                                    </form>
                                    <?php
                                } ?>
                                <!--end::Form-->
                                <!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sub Type Name</th>
                                    <th>Derived Type Name</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($this->data)) {
                                    for ($i = 0; $i < count($this->data); $i++) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td>
                                            <td><?php echo $this->data[$i]['tPropertySubTypeName']; ?></td>
                                            <td><?php echo $this->data[$i]['tPropertyDerivedTypeName']; ?></td>
                                            <td>
                                                <form method="post" action="<?php echo Urls::$BASE ?>properties/edit_Derivedtype"
                                                      style="display: inline-table;">
                                                    <input type="hidden" name="derivedtypeID"
                                                           value="<?php echo $this->data[$i]['nPropertyDerivedTypeIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title=""
                                                            data-original-title="Edit <?php echo $this->data[$i]['tPropertySubTypeName']; ?> Detail"
                                                            type="button"
                                                            class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                        <i class="fa fa-pencil-alt"></i></button>
                                                </form>

                                                <form style="display: inline-table;">
                                                    <button data-toggle="m-tooltip" data-placement="top" title=""
                                                            data-original-title="Delete SubType" type="button"
                                                            class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                                            onclick="setId(<?php echo $this->data[$i]['nPropertyDerivedTypeIDPK']; ?>)">
                                                        <i class="fa fa-trash-alt"></i></button>
                                                </form>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function ()
    {
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-top-center"
        };

        $("#select2Country").select2({
            placeholder: "-- Select --"
        });

        $('#s1').DataTable({
            "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
            "columnDefs": [
                {"width": "5%", "targets": 0}
            ]
        });

        $("#submit").click(function(e)
        {
            if($('#select2Country').val() == '')
            {
                toastr.error("Please select country name.", "Country Name");
                e.preventDefault();
            }
            else if($('#subtypeName').val() == '')
            {
                toastr.error("Please enter subtype name.", "SubType Name");
                e.preventDefault();
            }
        });

        $("#update").click(function(e)
        {
            if($('#select2Country').val() == '')
            {
                toastr.error("Please select country name.", "Country Name");
                e.preventDefault();
            }
            else if($('#subtypeName').val() == '')
            {
                toastr.error("Please enter subtype name.", "SubType Name");
                e.preventDefault();
            }
        });
    });

    getCountryList();   // fetch all country and append it to the dropdown

    $(document).on('click', ".btn_edit", function (e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });

    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this SubType ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>properties/deleteDerivedType",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>properties/derivedtype";
                        }
                    }
                });
            }
        });
    }

    function getCountryList()   // fetch all country list
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getSubTypeDetailsMain',
            type: 'POST',
            success: function (data) {
                $('#select2Country').empty();
                $('#select2Country').append('<option value="">-- Select Country --</option>');
                var country = JSON.parse(data);
                for (var i = 0; i < country.length; i++) {
                    var data1 = "<?php if (isset($this->singleTypes)) {
                        echo $this->singleTypes[0]['nPropertySubTypeIDPK'];
                    } else {
                        echo NULL;
                    } ?>";
                    if (data1 == country[i].nPropertySubTypeIDPK)
                        $('#select2Country').append('<option value=' + country[i].nPropertySubTypeIDPK + ' selected>' + country[i].tPropertySubTypeName + '</option>');
                    else
                        $('#select2Country').append('<option value=' + country[i].nPropertySubTypeIDPK + '>' + country[i].tPropertySubTypeName + '</option>');
                }
                $("#select2Country").trigger("change");
            }
        });
    }
</script>
