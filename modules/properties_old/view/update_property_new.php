
<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    .buttonDemo{
        background-color: transparent;

    }
    .m-select2{ 
        border-color: #ffffff !important;
      
    }
    .select2{
        width: 100% !important;
    }
    .tclose{
            position: absolute;
            background:black;
            color:white;
            border-radius:50%;
            width:20px;
            height:20px;
            line-height:20px;
            text-align:center;
            font-size:0.5rem !important;
            font-style: unset;
            font-family:'Arial Black', Arial, sans-serif;
            cursor:pointer;
            box-shadow:0 0 10px 0 #7b7e8a;
        }
    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;  -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    label {
    font-weight: 400 !important;
    }
    .m-form .form-control-feedback {
   
    color: red;
}


.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-label {
    font-weight: bold;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span {
    background-color: #ef424a;
}
/*data-toggle="m-tooltip" data-placement="bottom" data-offset="100px 0px" data-skin="dark" title="" data-original-title="Click To Select Image"*/
.tickClassOverImg
{
    position: absolute;
    top: 43%;
    left: 60%;
    right: 32%;
    border-radius: 28px;
    display: none;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--done .m-wizard__step-number > span {
    background-color: #ef424a;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--done .m-wizard__step-label {
    font-weight: bold;
}
.listingForLabel
{
    font-size: 20px;
    font-weight: bold;
    margin: 15px;
}
/*----------------  Tab css -----------------*/

.nav.nav-pills .nav-link, .nav.nav-tabs .nav-link {
    color: #f4516c;
    font-weight: 400;
    font-size: 16px;
}

/*.nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #495057;
    background-color: #fff;
    border-color: #f4516c #f4516c #ffffff;
}*/


 .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        color: #495057;
        /*background-color: #0da4e2;*/
        border-color: #f4516c #f4516c #fff;
    }
    .nav-tabs {
    border-bottom: 1px solid #f4516c !important;
    }

     .nav.nav-tabs {
        margin-bottom: 0px !important;
    }

    .tab-content{
        border-bottom: 1px solid #f4516c;
        border-left: 1px solid #f4516c;
        border-right: 1px solid #f4516c;
    }
    .m-portlet.m-portlet--head-lg .m-portlet__head {
        height: 7rem;
    }
    .m-portlet.m-portlet--sticky .m-portlet__head {
        position: sticky !important;
    }
    .m-portlet.m-portlet--head-lg .m-portlet__head {
         height: auto;
         padding: 10px; 
    }

    /*.m-portlet.m-portlet--head-lg
    {
        left: 307px !important;
    }*/

    #myMap {
           height: 350px;
           width: 100%;
        }
</style>

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--last m-portlet--head-lg m-portlet--responsive-mobile" id="main_portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-progress"></div>
                        <div class="m-portlet__head-wrapper"> 
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Update Property
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                
                                <a href="<?php echo Urls::$BASE; ?>properties" class="btn btn-secondary m-btn m-btn--icon m-btn--wide m-btn--md m--margin-right-10" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listings">
                                    <i class="fa fa-list"></i>              
                                </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" id="update_Basic" class="btn btn-danger m-btn m-btn--custom" style="background-color: #f4516c !important; border-color: #f4516c !important;" name="submit" value="update_basic">
                                    Update
                                </button>
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <form enctype="multipart/form-data" class="m-form m-form--label-align-left- m-form--state-" id="update_Basic_Info">
                            <input type="hidden" name="ID" value="<?php echo $this->data['nPropertyIDPK']?>">

                            <input type="hidden" name="ImageExistFolderName" value="<?php echo $this->data['vFolderName']?>">

                            <div id="tabs">
                                <ul class="nav nav-tabs nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tab1" data-toggle="tab" href="#m_tabs_2_1">1<br>Basic Information</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab2"  data-toggle="tab" href="#m_tabs_2_2">2<br>Location Setup</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab3"  data-toggle="tab" href="#m_tabs_2_3">3<br>Property Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="tab4"  data-toggle="tab" href="#m_tabs_2_4">4<br>Features</a>
                                    </li>
                                </ul>
                                   
                                <div class="tab-content">

                                        <div class="tab-pane active" id="m_tabs_2_1" role="tabpanel">
                                            <div class="m-portlet__body">
                                                <div class="form-group row">
                                                    <div class="col-lg-3"></div>
                                                    <div class="col-lg-3" style="cursor: pointer;">
                                                        <label class="m-option" id="Resale" style="cursor: not-allowed;color: white; padding: 10px !important;">
                                                            <span class="m-option__control" style="width: 100%; text-align: center;">
                                                                <span class="m-radio m-radio--state-brand" style="display: none; ">
                                                                    <input  align="center" type="radio" name="listingFor" id="listingFor" style="width: 0px !important;" disabled  <?php if ($this->data['bIsProperty'] == "0") { echo "checked"; } ?> value="0">
                                                                    <span></span>
                                                                </span>
                                                                Resale
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3 Project" >
                                                        <label class="m-option" id="Project" style="cursor: not-allowed;color:black;padding: 10px !important;">
                                                            <span class="m-option__control" style="width: 100%; text-align: center;">
                                                                <span class="m-radio m-radio--state-brand" style="display: none">
                                                                    <input type="radio" name="listingFor" id="listingFor" <?php if ($this->data['bIsProperty'] == "1") { echo "checked"; }?> value="1" disabled>
                                                                    <span></span>
                                                                </span>
                                                                New Project
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3"></div>                                                     
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Property For</label>
                                                         <select  class="form-control m-select2" name="propFor" id="propFor">
                                                           <option value="0" <?php if ($this->data['nPropertyFor'] == "0") { echo "selected";
                                                            }?>>Buy</option>
                                                           <option value="1" <?php if ($this->data['nPropertyFor'] == "1") { echo "selected";
                                                            }?> >Rent</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Property Type</label>
                                                        <select  class="form-control m-select2" name="propertyType" id="propList" disabled>

                                                           <?php

                                                            if (!empty($this->getPropertyType)) {
                                                                ?>

                                                                <?php
                                                                for ($i=0; $i < count($this->getPropertyType) ; $i++) 
                                                                { 
                                                                    if ($this->data['nPropertyTypeIDFK'] == $this->getPropertyType[$i]['nPropertyTypeIDPK']) {
                                                                        ?>
                                                                            <option selected value="<?php echo $this->getPropertyType[$i]['nPropertyTypeIDPK']?> "><?php echo $this->getPropertyType[$i]['tPropertyTypeName']?></option>
                                                                        <?php
                                                                    }
                                                                    else{
                                                                        ?>
                                                                            <option value="<?php echo $this->getPropertyType[$i]['nPropertyTypeIDPK']?> "><?php echo $this->getPropertyType[$i]['tPropertyTypeName']?></option>
                                                                        <?php

                                                                    }
                                                                }
                                                            }

                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                   <div class="col-lg-12" id="Residential" style="border: 2px solid #f7f7f7; padding: 10px;  text-align: center; vertical-align: middle; margin-top: 20px;">
                                                        <div class="col-lg-12 row" id="propertySubTypes">
                                                        </div>
                                                         <input type="hidden" name="propertiesSubType" id="propertiesSubType" value="">
                                                    </div>
                                                </div>
                                                 
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <br>
                                                        <div class="row">   
                                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RERA Regestration Status:</label>&nbsp;&nbsp;&nbsp;
                                                            <div class="col-lg-3" >
                                                                <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                                    YES
                                                                <input  name="rerayes" id="rerayes" value="yes" <?php if($this->data['bIsPropertyReraCertified'] == 1) { echo "checked"; } ?>  class="isAnswer" type="radio">
                                                                <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-3">
                                                               
                                                                <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                                    NO
                                                                <input  name="rerayes" id="rerayes" value="no" <?php if($this->data['bIsPropertyReraCertified'] == 0) { echo "checked"; } ?>  class="isAnswer" type="radio">
                                                                <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="form-group row" style="display: none;" id="reraid">
                                                    <div class="col-lg-12">
                                                        <label>Please Enter Your RERA Certification ID</label>
                                                        <input type="text" class="form-control m-input" name="reraid" id="reraID" value="<?php echo $this->data['tPropertyReraID'];?>" >
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-4">
                                                        <label>Status</label>
                                                        <select class="form-control m-select2" id="Availability" name="Availability">
                                                          
                                                            <option value="1" <?php if ($this->data['nPropertyAvailability'] == "1") { echo "selected";
                                                            }?>  >Under Constructions</option>
                                                            <option value="0" <?php if ($this->data['nPropertyAvailability'] == "0") { echo "selected";
                                                            }?>  >Ready To Move</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Possession From</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" readonly="" placeholder="Select date" id="possessionDate" name="possessionDate" value="<?php echo $this->data['dtPropertyPossesionDate']?>">
                                                            <div class="input-group-append" style="cursor: pointer;">
                                                                <span class="input-group-text">
                                                                    <i style="font-size: 20px;" class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <br>
                                                
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="m_tabs_2_2" role="tabpanel">
                                            <div class="m-portlet__body">
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <label>Project Name</label>
                                                        <input id="PropertyID" name="PropertyID" type="hidden">
                                                        <input type="text" class="form-control" name="propertyName" id="propertyName" value="<?php echo $this->data['tPropertyName']; ?>"/>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Street Name</label>
                                                       <input type="text" class="form-control" name="streetName" id="streetName"  value="<?php echo $this->data['tPropertyStreetName']; ?>" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12 col-sm-12">
                                                        <label>Location</label>
                                                        <input type="text" class="form-control" name="pAddress" id="pAddress" placeholder="Enter Location" value="<?php echo $this->data['tPropertyAddress']; ?>">
                                                    </div>
                                                </div>

                                                <input type="hidden" name="LocalityID" id="LocalityID" value="<?php echo $this->data['nLocalityIDFK']; ?>">

                                                <input type="hidden" name="cityID" id="cityID" value="<?php echo $this->data['nPropertyCityIDFK']; ?>">

                                                <input type="hidden" name="areaID" id="areaID" value="<?php echo $this->data['nPropertyAreaIDFK']; ?>">

                                                <input type="hidden" name="stateID" id="stateID" value="<?php echo $this->data['nPropertyStateIDFK']; ?>">

                                                <input type="hidden" name="countryID" id="countryID" value="<?php echo $this->data['nPropertyCountryIDFK']; ?>">

                                                <div class="form-group row">
                                                    <div class="col-lg-4">
                                                        <label>Select Country</label>
                                                        <select class="form-control m-select2" style="width: 100%;" name="country" id="select2Country" disabled ></select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Select State</label>
                                                        <select class="form-control m-select2" style="width: 100%;" name="state" id="select2State" disabled ></select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Select City</label>
                                                        <select class="form-control m-select2" style="width: 100%;" name="city" id="select2City" ></select>
                                                    </div>
                                                </div>
                                                <div class="form-group  row">
                                                    <div class="col-lg-6 col-12">
                                                        <label>Select Area</label>
                                                        <div class="input-group" style="width: 100%;flex-wrap:unset;" >
                                                           <select class="form-control m-select2" style="width: 81%;" name="area" id="select2SArea" ></select>
                                                            <div class="input-group-append" onclick="AddLocationArea();" style="cursor: pointer;">
                                                                <span class="input-group-text"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Select locality</label>
                                                        <div class="input-group" style="width: 100%;flex-wrap:unset;" >
                                                          <select class="form-control m-select2" style="width: 85%;" name="localityName" id="localityName">
                                                            </select>
                                                            <div class="input-group-append" onclick="AddLocationLocality();" style="cursor: pointer;">
                                                                <span class="input-group-text"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                       <label>Property Latitude</label>
                                                        <input type="text" class="form-control m-input" name="pLatitutde" id="pLatitutde" placeholder="Enter Property Latitude"  readonly value="<?php echo $this->data['dPropertyLatitude']; ?>">
                                                    </div>
                                                    <div class="col-lg-6">
                                                       <label>Property Longitude</label>
                                                        <input type="text" class="form-control m-input" name="pLongitude" id="pLongitude" placeholder="Enter Property Longitude" readonly value="<?php echo $this->data['dPropertyLongitude']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                       <label>Map Location Link</label>
                                                        <input type="text" class="form-control m-input" name="mapLocationLink" id="mapLocationLink" placeholder="Enter Map Location Link" readonly value="<?php echo $this->data['tPropertyMapLocationLink']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12 col-sm-6">
                                                        <div style="width: 100%">
                                                           <div id="myMap"></div>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                </div>
                                            </div>
                                              
                                        </div>

                                        <div class="tab-pane " id="m_tabs_2_3" role="tabpanel">
                                           
                                            <div class="m-portlet__body">
                                                <div class="m-form__section m-form__section--first">
                                                            <div class="form-group m-form__group">
                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label class="m-option" id="Freehold" style="padding: 10px !important; cursor: pointer;">
                                                                            <span class="m-option__control">
                                                                                <span class="m-radio m-radio--state-brand" style="display: none">
                                                                                    <input  type="radio" name="ownership" id="ownership"  onclick="changeBackgroundOwnership('Freehold')"  value="Freehold">
                                                                                    <span></span>
                                                                                </span>
                                                                            </span>
                                                                            <span class="m-option__label" >
                                                                                <span class="m-option__head">
                                                                                    <span class="m-option__title">
                                                                                        Freehold
                                                                                    </span>
                                                                                </span>  
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label class="m-option" id="Leasehold" style="padding: 10px !important;cursor: pointer;">
                                                                            <span class="m-option__control">
                                                                                <span class="m-radio m-radio--state-brand" style="display: none">
                                                                                    <input type="radio" name="ownership" id="ownership"   onclick="changeBackgroundOwnership('Leasehold')"  value="Leasehold">
                                                                                    <span></span>
                                                                                </span>
                                                                            </span>
                                                                            <span class="m-option__label" >
                                                                                <span class="m-option__head">
                                                                                    <span class="m-option__title">
                                                                                        Leasehold
                                                                                    </span>
                                                                                </span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <label class="m-option" id="Society" style="padding: 10px !important;cursor: pointer;">
                                                                            <span class="m-option__control">
                                                                                <span class="m-radio m-radio--state-brand" style="display: none">
                                                                                    <input type="radio" name="ownership" id="ownership"  onclick="changeBackgroundOwnership('Society')"  value="Co-operative Society">
                                                                                    <span></span>
                                                                                </span>
                                                                            </span>
                                                                            <span class="m-option__label" >
                                                                                <span class="m-option__head">
                                                                                    <span class="m-option__title">
                                                                                        Co-operative Society
                                                                                    </span>
                                                                                </span>
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="m-form__help">
                                                                </div>
                                                            </div>

                                                           
                                                            <div class="row" id="addLayout_Flat" style="display: none; padding: 20px;">
                                                                <div class="col-lg-12" >
                                                                    <i style=" float: right;cursor: pointer;padding: 10px;" class="fa fa-plus"></i>
                                                                    <br>

                                                                </div>
                                                            </div>
                                                            <div class="row"  id="addLayout"  style="display: none; padding: 20px;">
                                                                <div class="col-lg-12" >
                                                                    <i style=" float: right;cursor: pointer;padding: 10px;" onclick="addCommercial('Flat');" 
                                                                    class="fa fa-plus"></i>
                                                                    <br>
                                                                </div>
                                                            </div>
                                                            <div class="row"  id="addOtherslayout"  style="display: none;  padding: 20px;">
                                                                <div class="col-lg-12" >
                                                                    <i style=" float: right;cursor: pointer;padding: 10px;" onclick="addOtherslayout('Flat');" 
                                                                    class="fa fa-plus"></i>
                                                                    <br>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="addLandlayout" style="display: none; padding: 20px;">
                                                                <div class="col-lg-12" >
                                                                    <i style=" float: right;cursor: pointer;padding: 10px;"  onclick="addLandlayout('Flat');" 
                                                                    class="fa fa-plus"></i>
                                                                    <br>
                                                                </div>
                                                            </div>

                                                            <input type="hidden" name="layoutCount" id="layoutCount" value="1">
                                                            <input type="hidden" name="propertyType" id="propertyType" value="<?php echo $this->data['nPropertyTypeIDFK']?>">
                                                            <input type="hidden" name="propertiesSubType" id="propertiesSubType" value="1<?php echo $this->data['nPropertySubTypeIDFK']?>">

                                                            <div id="layoutDetails_Flat">
                                                               
                                                            </div>


                                                </div>
                                               
                                               
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label>Description</label>
                                                        <textarea  class="form-control m-input" name="propertyDescription" id="propertyDescription"   rows="5"> <?php echo $this->data['tPropertyDescription']; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>   
                                        </div>

                                        <div class="tab-pane" id="m_tabs_2_4" role="tabpanel">
                                            <div class="m-portlet__body">
                                                <div class="m-form__heading">
                                                    <h2 class="m-form__heading-title">Features Details :</h2>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-3">
                                                        <label>Property Images</label>
                                                        <span class="" onclick="document.getElementById('property_logo').click();">
                                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                                <input style="display:none;" type="file" name="property_logo[]" id="property_logo" onchange="property_image_preview(this)" accept="image/x-png,image/jpeg,image/jpg" />

                                                                <div align="center">
                                                                    <i class="fa fa-plus" style="font-size: 9.1rem !important;"></i>
                                                                    <label >Click here to upload images</label>
                                                                </div>
                                                            </div>
                                                        </span>
                                                        <br><br>
                                                        <label id="imgCount">Images Selected : ( 0 / 25 )</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-lg-9" style="margin-top: 20px; height: 315px !important; overflow-x: auto !important; ">
                                                        <br>
                                                         <div class="form-group row" id="property_display_preview" style="display: none;padding: 5px !important; padding-left: 10px !important;" >
                                                         </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div style="padding: 10px;border-radius: 10px;width:100%;">
                                                            <h5 style="color: black;"> Uploaded Images :</h5><br>
                                                            <?php
                                                            if (!empty($this->data['ListingImages'])) 
                                                            {   
                                                                 ?>
                                                            <input type="hidden" name="Image_FolderName" value="<?php echo $this->data['ListingImages'][0]['tPropertyImageFolderPath']?>">
                                                            <?php
                                                                for ($i = 0; $i < count($this->data['ListingImages']); $i++) 
                                                                {
                                                                ?>
                                                                    <div id="hide_place_Img<?php echo $this->data['ListingImages'][$i]['nPropertyImageIDPK']; ?>" class="vpb_wrapper">
                                                                        <span onclick="remove_listing_image('<?php echo $this->data['ListingImages'][$i]['nPropertyImageIDPK']; ?>')" class="tclose">X</span>    
                                                                        <a href="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data['ListingImages'][$i]['tPropertyImageFolderPath']; ?>" target="_blank">
                                                                            <img class="vpb_image_style" width="100px" height="100px" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data['ListingImages'][$i]['tPropertyImageFolderPath']; ?>">
                                                                        </a> 
                                                                        <br>
                                                                        <span>Cover Image</span>
                                                                        <br>
                                                                        <?php
                                                                        if ($this->data['ListingImages'][$i]['bIsFrontImage'] == 0) 
                                                                        {?>
                                                                            <label class="m-radio m-radio--state-success">
                                                                                <input type="radio" name="isCoverImage" id="isCoverImage" value="<?php echo $this->data['ListingImages'][$i]['nPropertyImageIDPK']; ?>">
                                                                                <span></span>
                                                                            </label>
                                                                        <?php
                                                                        }
                                                                        else
                                                                        {?>
                                                                            <label class="m-radio m-radio--state-success">
                                                                                <input type="radio" checked name="isCoverImage" id="isCoverImage" value="<?php echo $this->data['ListingImages'][$i]['nPropertyImageIDPK']; ?>">
                                                                                <span></span>
                                                                            </label>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                <?php
                                                                }
                                                            }
                                                            else
                                                            { ?>
                                                               <center><h6>"No Images uploaded"</h6></center>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="from-group">
                                                    <label>Amenities </label>
                                                    <div class="col-lg-12" id="Aminities" style="text-align: center; vertical-align: middle;">
                                                        <div class="col-lg-12">
                                                            <input type="hidden" name="AminitiesHidden[]" id="AminitiesHidden" value="">
                                                            <div class="row" id="Amenities" style="height: 250px !important;overflow:auto;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><br>                                            
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                    <label>Property Brochure</label><br>
                                                    <input  type="file" name="brochurePath[]" id="brochurePath" accept="application/pdf" value="" />
                                                    <?php
                                                        if (!empty($this->data['tPropertyBrochurePath'])) 
                                                        {
                                                            ?>
                                                                <a target="_blank" href="<?php echo urls::$BASE.urls::$IMG_UPLOADS_LISTING.$this->data['tPropertyBrochurePath']; ?>">Uploaded Brochure File</a>
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <a>"No Brochure Uploaded"</a>
                                                            <?php
                                                        }
                                                    ?>                          
                                                    </div>
                                                    <br>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="m-portlet__foot m-portlet__foot--fit">
                                                <div class="m-form__actions">
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-3">
                                                           
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <input type="hidden" name="submit" value="update_basic">
                                                            
                                                        </div>
                                                        <div class="col-lg-3">
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>

<div id="addAreaModal" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="OldCityName" class="modal-title">Add Area on city</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-8">
                        <label>Enter Area name</label>
                        <input type="text" class="form-control m-input" name="newArea" id="newArea">
                    </div>
                    <div class="col-lg-4">
                        <label>Enter Pincode</label>
                        <input type="text" class="form-control m-input" name="newAreaPincode" id="newAreaPincode">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-block" id="AddNewAreaBtn">Add Area</button>
            </div>
        </div>
    </div>
</div>

<div id="addLocalityModal" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="OldAreaName" class="modal-title">Add Locality on Area</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-8">
                        <label>Enter Locality name</label>
                        <input type="text" class="form-control m-input" name="newLocality" id="newLocality">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-block" id="AddNewLocalityBtn">Add Locality</button>
            </div>
        </div>
    </div>
</div>
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZMqWuiutvNXW0cZ-m1IAgsW9FhbP_MSU&sensor=false"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZMqWuiutvNXW0cZ-m1IAgsW9FhbP_MSU&libraries=places&sensor=false"></script>



<script type="text/javascript"> 

    var latitude = $('#pLatitutde').val();
    var longitude = $('#pLongitude').val();
    
    var map;
    var marker;
    var myLatlng = new google.maps.LatLng(latitude, longitude);
    var geocoder;
    var infowindow = new google.maps.InfoWindow();
    var searchBox;

    var map = new google.maps.Map(document.getElementById('myMap'), {
        center: myLatlng,
        zoom: 17,
        mapTypeId: 'roadmap'
    });
    var marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true
    });

    //google.maps.event.addDomListener(window, 'load', initMap);

    function initMap() 
    {
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pAddress');

        searchBox = new google.maps.places.SearchBox(input);
        geocoder = new google.maps.Geocoder();
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
       
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() 
        {
            var places = searchBox.getPlaces();

            if (places.length == 0) 
            {
                return;
            }

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();

            places.forEach(function(place) 
            {
                if (!place.geometry) 
                {
                    //console.log("Returned place contains no geometry");
                    return;
                }

           
                var input1 = place.geometry.location.lat()
                var input2 = place.geometry.location.lng();
                geocodeAddress(geocoder, map);
            });
        }); 
    } 

    function geocodeAddress(geocoder, resultsMap) 
    {
        var address = document.getElementById('pAddress').value;
        geocoder.geocode({'address': address}, function(results, status) 
        {
            if (status === 'OK') 
            {
                resultsMap.setCenter(results[0].geometry.location);
                var input1 = results[0].geometry.location.lat();
                var input2 = results[0].geometry.location.lng();
                // console.log(results[0].geometry.location);
                initialize(input1,input2);
            } 
            else 
            {
                //alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function initialize(input1,input2)
    {
        var latlng = new google.maps.LatLng(input1, input2);
        var map = new google.maps.Map(document.getElementById('myMap'), 
        {
            center: latlng,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var marker = new google.maps.Marker(
        {
            map: map,
            position: latlng,
            draggable: true
        });

        
        geocoder.geocode({'latLng': marker.getPosition() }, function(results, status) 
        {
            if (status == google.maps.GeocoderStatus.OK && results.length) 
            {
                var place_id = results[0].place_id;
                var location = results[0].formatted_address;
                
                results = results[0].address_components;

                var mapLink = 'https://www.google.com/maps/search/?api=1&query='+marker.getPosition().lat()+','+marker.getPosition().lng()+'&query_place_id='+place_id;

                $('#mapLocationLink').val(mapLink);

                //console.log(results);
    
                var city = findResult(results, "locality");
                
                if(city == "")
                {
                   city = findResult(results, "administrative_area_level_2"); 

                   if(city == "")
                   {
                        swal({
                            title:"City Not Found",
                            text:"Please select different location",
                            type:"warning",
                            showCancelButton:!0,                           
                            cancelButtonText:"OK"
                        });     
                    }
                    else
                    {
                        $('#pLatitutde').val(marker.getPosition().lat());
                        $('#pLongitude').val(marker.getPosition().lng());
                    }
                }
                else
                {
                    $('#pLatitutde').val(marker.getPosition().lat());
                    $('#pLongitude').val(marker.getPosition().lng());
                }

                $('#pAddress').val(location);

                infowindow.setContent(location);
                infowindow.open(map, marker);

                var state = findResult(results, "administrative_area_level_1");
                var country = findResult(results, "country");
                var area = findResult(results, "sublocality_level_1");                  
                var route = findResult(results, "route");
                var pincode = findResult(results, "postal_code");

                var latitude = marker.getPosition().lat();
                var longitude = marker.getPosition().lng()

                checkForLocation(country,state,city,area,route,pincode,latitude,longitude);
            } 
        });

        var findResult = function(results, name){

                var result =  _.find(results, function(obj){
                for (var i = 0; i < obj.types.length; i++)
                {
                    if(obj.types[i] == name)
                    {
                       return obj.types[i] == name;
                    } 
                }
            });

            return result ? result.long_name : null;
        };
                       
        google.maps.event.addListener(marker, 'dragend', function() 
        {
            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) 
            {
                if (status == google.maps.GeocoderStatus.OK && results.length) 
                {
                    var place_id = results[0].place_id;
               
                    var location = results[0].formatted_address;
               
                    results = results[0].address_components;
                 
                    var mapLink = 'https://www.google.com/maps/search/?api=1&query='+marker.getPosition().lat()+','+marker.getPosition().lng()+'&query_place_id='+place_id;

                    $('#mapLocationLink').val(mapLink);
                                  
                    var city = findResult(results, "locality");
                    if(city == "")
                    {
                        city = findResult(results, "administrative_area_level_2"); 

                        if(city == "")
                        {
                            swal({
                                title:"City Not Found",
                                text:"Please select different location",
                                type:"warning",
                                showCancelButton:!0,                           
                                cancelButtonText:"OK"
                            });  
                        }
                        else
                        {
                            $('#pLatitutde').val(marker.getPosition().lat());
                            $('#pLongitude').val(marker.getPosition().lng());
                        }
                    }
                    else
                    {
                        $('#pLatitutde').val(marker.getPosition().lat());
                        $('#pLongitude').val(marker.getPosition().lng());
                    }

                    $('#pAddress').val(location);
                    infowindow.setContent(location);
                    infowindow.open(map, marker);

                    var state = findResult(results, "administrative_area_level_1");
                    var country = findResult(results, "country");
                    var area = findResult(results, "sublocality_level_1");                  
                    var route = findResult(results, "route");
                    var pincode = findResult(results, "postal_code");

                    var latitude = marker.getPosition().lat();
                    var longitude = marker.getPosition().lng()

                    checkForLocation(country,state,city,area,route,pincode,latitude,longitude);
                } 
            });
        });
    }
    
    google.maps.event.addDomListener(window, 'load', initMap);

    function checkForLocation(country, state, city = null, area = null, route = null,pincode = null, latitude = null, longitude = null ) 
    {

       $.ajax({
            url: "<?php echo Urls::$BASE; ?>location/checkForLocation",
            type: "POST",
            data: {'country':country,'state':state,'city':city,'area':area,'route':route,'pincode':pincode,'latitude':latitude,'longitude':longitude},
            success: function (data) 
            {
                var json = JSON.parse(data);
                
                $('#areaID').val(json.areaId);
                $('#cityID').val(json.cityId);
                $('#stateID').val(json.stateId);
                $('#countryID').val(json.countryId);
                $('#LocalityID').val(json.localityId);

                getCountryList();      
            }
        });
    }
</script>   
 
<!-- end:: Body -->
<script type="text/javascript">
    $(document).ready(function() 
    {
        $("#select2Country").select2({
            placeholder: "-- Select Country --"
        });

        $("#select2State").select2({
            placeholder: "-- Select State --"
        });

        $("#select2City").select2({
            placeholder: "-- Select City --"
        });
        $("#select2SArea").select2({
            placeholder: "-- Select Area --"
        });
        $("#localityName").select2({
            placeholder: "-- Select Locality --"
        });

        getCountryList();
      
        // WizardDemo.init();
        var loginID = "<?php echo SessionHandling::get('userType');?>";

        if (loginID == "<?php echo DEVELOPER ?>") 
        {
            changeBackground('Resale');
            $('#Resale').css('background-color','#9a9a9a');
            $('#Resale .m-option__title').css('color','#ffff !important');
        }
        else if (loginID == "<?php echo AGENT ?>") 
        {
            $('.Project').css('display','none');
            changeBackground('Resale');
        }

        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
        var propertyFor = '<?php echo $this->data['bIsProperty'] ?>';
        //console.log(propertyFor);
        if (propertyFor == 1) 
        {
            changeBackground('Project');
        }
        else
        {
            changeBackground('Resale');
        }

        var ownership = '<?php echo $this->data['tPropertyOwnership']?>';

        if (ownership == "Freehold") 
        {
            $('#ownership').attr('checked',true);
            changeBackgroundOwnership('Freehold');
        }
        else if (ownership == "Leasehold") 
        {
            $('#ownership').attr('checked',true);
            changeBackgroundOwnership('Leasehold');
        }
        else if (ownership == "Co-operative Society") 
        {
            $('#ownership').attr('checked',true);
            changeBackgroundOwnership('Society');
        }
        $('#propFor').select2();

        /*----------START : LOCAIONS------------*/

            $('#locationID').select2();
            getLocations();
            // var 
            var totalData = <?php echo json_encode($this->data['ListingDetails']); ?>;

            // console.log("data : "+ totalData[1]['nProjectIDPK']);
            //console.log(totalData.length);
            for (var i = 0; i < totalData.length ; i++) 
            {
                AddLayout(i);
            }

        
        /*----------END : LOCAIONS------------*/
        
        $('#layoutPropBHK_House').select2();
        $('#propertyFurnishedID_House').select2();
        $('#Bedrooms_House').select2();
        $('#Bathrooms_House').select2();
        $('#Balconies_House').select2();
        $('#propBHK_House').select2();
        $('#facingID_House').select2();
        $('#UnitID_House').select2();
        $('#bArea_House').select2();
        $('#cArea_House').select2();
        $('#tFloor_House').select2();

        /*End House Layout*/
        /* Ofifce Layout */

        $('#washroom_Office').select2();
        $('#balcony_Office').select2();
        $('#UnitID_Office').select2();
        $('#bArea_Office').select2();
        $('#cArea_Office').select2();
        

            
        /*End Ofifce Layout */
        /* Other Layout  */
        $('#propBHK_Other').select2();
        $('#facingID_Other').select2();
        $('#UnitID_Other').select2();
        $('#tFloor_Other').select2();
        $('#pFloor_Other').select2();
        $('#bArea_Other').select2();
        $('#cArea_Other').select2();
        
        /*End Other Layout  */

        $('#propertyFurnishedID').select2();
        $('#companyID').select2();
        $('#maintenanceFor').select2();
        $('#brokerageAmountIn').select2();
        $('#pCategoryID').select2();
        $('#Availability').select2();
        $('#ownership').select2();
        $('#maintenance').select2();
        // $('#possession').datepicker();
        $('#propBHK').select2();

        $('#layoutPropBHK').select2();

        $('.datepicker').datepicker({
            autoclose:true,
            startDate : 'today',
           format:'dd-mm-yyyy'
        });

        $('#kitchen').select2();
        $('#facingID').select2();
        $('#layoutType').select2();
        $('#UnitID_Land').select2();
        $('#UnitID_Land_com').select2();

        $('#UnitID').select2();
        $('#bArea').select2();
        $('#cArea').select2();
       
        $('#typeprop').select2();
        $('#propType').select2();
        $('#propList').select2();
        $('#propListtwo').select2();
        $('#propTypeone').select2();
        $('#furnished').select2();
        $('#pFloor').select2();
        // $('#tFloor').select2();
        $('#tFloor_Flat').select2();
        $('#tFloor_Land').select2();
       
        $('#Bedrooms').select2();
        $('#Bathrooms').select2();
        $('#Balconies').select2();
        $('#furnished1').select2();
        $('#pReportingTime').select2();


        $('#UnitID').on('change',function(){
            var data = $('#UnitID option:selected').text();
            if ($('#UnitID').val() != "" && $('#UnitID').val() != null) {
                $('#bArea').html('<option value="'+$('#UnitID').val()+'">'+data+'</option>');
                $('#cArea').html('<option value="'+$('#UnitID').val()+'">'+data+'</option>');
            }

        });


        var balconyValue = "<?php if(isset($this->data)) {echo $this->data['nBalconies']; } else { echo NULL; } ?>";

        var bathroomsValue = "<?php if(isset($this->data)) {echo $this->data['nBathrooms']; } else { echo NULL; } ?>";

        var propertyOnFloorValue = "<?php if(isset($this->data)) {echo $this->data['nPropertyOnFloor']; } else { echo NULL; } ?>";

        $('#tFloor').on('change', function(){

            var id = $('#tFloor').val();
          
            for (var i = 1; i <= id; i++) 
            {
                if(propertyOnFloorValue != 'NULL' && (propertyOnFloorValue == i))
                {
                    $('#pFloor').append('<option value='+i+' selected>'+i+'</option>');
                }
                else
                {
                    $('#pFloor').append('<option value='+i+'>'+i+'</option>');
                }                
                
            }
        });

       
        getUnitsDetails();
        getBHKDetails();      
        getPropertyType();
        getAmenities();
        getfacing();
        getLayoutFacing();
        getLayoutBHKDetails();
        $("#propertyName" ).autocomplete(
        {
            source: function (request, response) 
            {
                $("#PropertyID").val('0');
                $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/getSearchProperty",
                    type: "POST",
                    data: request,
                    dataType: 'json',
                    success: function (data) 
                    {
                        response($.map(data, function (el)
                        {
                            return {
                                label: el.tPropertyName,
                                value: el.nPropertyIDPK
                            };
                        }));
                    }
                });
            },
            select:function (e, ui) 
            {
                e.preventDefault(); // uncomment if you want to display name in place of id
                $("#propertyName").val(ui.item.label); 
                $("#PropertyID").val(ui.item.value);// uncomment if you want to display name in place
                getPropertyLocation($("#PropertyID").val());

            },
            focus: function(event, ui) 
            {
                event.preventDefault();
                $("#propertyName").val(ui.item.label);
            }
        });
        
        $('#typeprop').on('change', function(){
            
            $id = $('#typeprop').val();
            // alert($id);
            // console.log($id);
            if ($id == "rent" ) {
                $('#'+$id).css('display','block');
                $('#sale').css('display','none');

            }   
            else if ($id == "sale") {

                 $('#'+$id).css('display','block');
                $('#rent').css('display','none');
            }

        });
     
        $('#propList').on('change', function(){
            getPropertyType();
        });
        
        $('.time-picker').timepicker({ 
            showMeridian: false,
            minuteStep: 5 
        });

        $('.isAnswer').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });

        $('.Parkingyes').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#parking").css("display","block");
            }
            if (this.value === "no") {

                $("#parking").css("display","none");   
            }
        });

        $('.brokerageYes').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#Brokerage").css("display","block");
            }
            if (this.value === "no") {

                $("#Brokerage").css("display","none");   
            }
        });
        
        $('.isParking').change(function()
        {
            // console.log(this.value);
            if (this.value === "yes") {

                $("#isParking").css("display","block");
            }
            if (this.value === "no") {

                $("#isParking").css("display","none");   
            }
        });
        $('.Cparking').change(function()
        {
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });
        $('.Oparking').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });

    
        $("#pLatitutde").focusout(function()
        {
            var latitude = document.getElementById('pLatitutde').value;
           
            var reg = new RegExp("^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$");

            if(latitude != '')
            {
                if(!reg.exec(latitude)) 
                {
                    document.getElementById('pLatitutde').value='';
                    toastr.error("Please Enter valid latitude value.", "latitude");
                }
            } 
        });

        $("#pLongitude").focusout(function()
        {
            var longitude = document.getElementById('pLongitude').value;

            var reg = new RegExp("^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$");

            if(longitude != '')
            {
                if(!reg.exec(longitude)) 
                {
                    document.getElementById('pLongitude').value='';
                    toastr.error("Please Enter valid longitude value.", "longitude");   
                }
            }  
        });
    });

    $('#select2Country').on('change', function () {    // on country dropdown change state
        getStateList();
    });

    $('#select2State').on('change', function () {    // on country dropdown change state
        getCityList();
    });
    $('#select2City').on('change', function () {    // on country dropdown change state
        getAreaList();
    });
     $('#select2SArea').on('change', function () {    // on country dropdown change state
        getlocality();
    });

    function AddLocationArea() 
    {   
        var address = $('#pAddress').val();
        var areaID = $('#areaID').val();
        var cityname = $('#select2City :selected').text();
        // var AreaName = $('#select2City :selected').text();
        var latitude = $('#pLatitutde').val();
        var longitude = $('#pLongitude').val();

        if (address == "")
        {
            toastr.error("Please Select First Location");
        }
        else
        {   
            if (cityname == "-- Select City --") 
            {
                toastr.error("Please Select Different Location","City Not Found");
                $('#pAddress').val('');
            }
            else
            {
                $('#OldCityName').text("Add Area On "+cityname);
                $('#addAreaModal').modal('show');
                
            }
            

        }  
    }
    function AddLocationLocality() 
    {   
        var address = $('#pAddress').val();
        var areaID = $('#areaID').val();
        var LocalityID = $('#LocalityID').val();
        // var  = $('#stateID').val();
        var stateID = $('#select2State :selected').val();
        var cityID = $('#select2City :selected').val();
        var AreaID = $('#select2SArea :selected').val();
        var AreaName = $('#select2SArea :selected').text();
        var localityName = $('#localityName').val();
        var latitude = $('#pLatitutde').val();
        var longitude = $('#pLongitude').val();
        // alert(localityName);

        if (address == "")
        {
            toastr.error("Please Select First Location");
        }
        else
        {   
            if(AreaID == "")
            {
                toastr.error("You Can't Add locality","Area Not Selected");
            }
            else
            {
                $('#OldAreaName').text("Add Locality On "+AreaName);
                $('#addLocalityModal').modal('show');
            }
            
        }  
    }

    function getCountryList()   // fetch all country list insert / update
    {   
        var id =  $('#countryID').val();
        // var country = $('#LocalityID').val();
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function (data) {
                $('#select2Country').empty();
                $('#select2Country').append('<option value="">-- Select Country --</option>');
                var country = JSON.parse(data);
                if (country != null) 
                {
                    for (var i = 0; i < country.length; i++) 
                    {
                        var data1 = id;

                        if (data1 == country[i].nCountryIDPK)
                        {
                            $('#select2Country').append('<option value=' + country[i].nCountryIDPK + ' selected>' + country[i].tCountryName + '</option>');
                        }
                        else
                        {
                            $('#select2Country').append('<option value=' + country[i].nCountryIDPK + '>' + country[i].tCountryName + '</option>');
                        }
                    }
                }
                $("#select2Country").trigger("change");
            }
        }).done(function () {   
            // getStateList();
        });
    }
    function getStateList() // fetch state list on the basis of the country id
    {
        var countryID = $('#select2Country').val();

        var id =  $('#stateID').val();

        // console.log("State ID :   "+id);

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getState',
            type: 'POST',
            data: {countryID: countryID},
            success: function (data) {
                // console.log(data);
                $('#select2State').empty();
                $('#select2State').append('<option value="">-- Select State --</option>');
                var state = JSON.parse(data);

                if (state != null) 
                {
                    for (var i = 0; i < state.length; i++) {

                        var data2 = $('#stateID').val();

                        if (data2 == state[i].nStateIDPK)
                        {
                            $('#select2State').append('<option value=' + state[i].nStateIDPK + ' selected>' + state[i].tStateName + '</option>');
                        }
                        else
                        {
                            $('#select2State').append('<option value=' + state[i].nStateIDPK + '>' + state[i].tStateName + '</option>');
                        }
                    }
                }

                $("#select2State").trigger("change");
            }

        }).done(function () {
             // getCityList();
        });
    }
    function getCityList() // fetch state list on the basis of the country id
    {
        var stateID = $('#select2State').val();
        // console.log("State : "+id);
      
        var id =  $('#cityID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getCity',
            type: 'POST',
            data: {stateID: stateID},
            success: function (data) {
                // console.log(data);
                $('#select2City').empty();
                $('#select2City').append('<option value="">-- Select City --</option>');
                var city = JSON.parse(data);
                if (city != null) 
                {
                    for (var i = 0; i < city.length; i++) {

                        var data2 = id;

                        if (data2 == city[i].nCityIDPK)
                        {
                            $('#select2City').append('<option value=' + city[i].nCityIDPK + ' selected>' + city[i].tCityName + '</option>');
                        }
                        else
                        {
                            $('#select2City').append('<option value=' + city[i].nCityIDPK + '>' + city[i].tCityName + '</option>');
                        }
                    }
                }
                 $("#select2City").trigger("change");
            }           


        }).done(function () {
            // getAreaList();
          
        });
    }
    function getAreaList() // fetch state list on the basis of the country id
    {
        var cityID = $('#select2City').val();

        var id =  $('#areaID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getArea',
            type: 'POST',
            data: {cityID: cityID},
            success: function (data) {
                // console.log(data);
                $('#select2SArea').empty();
                $('#select2SArea').append('<option value="">-- Select Area --</option>');
                var city = JSON.parse(data);
                if (city != null) 
                {
                    for (var i = 0; i < city.length; i++) {

                        var data2 = id;

                        if (data2 == city[i].nAreaIDPK)
                        {
                            $('#select2SArea').append('<option value=' + city[i].nAreaIDPK + ' selected>' + city[i].tAreaName + '</option>');
                        }
                        else
                        {
                            $('#select2SArea').append('<option value=' + city[i].nAreaIDPK + '>' + city[i].tAreaName + '</option>');
                        }
                    }
                }
                $("#select2SArea").trigger("change");
            }

        }).done(function () {
            // getlocality();
        });
    }
    function getlocality() // fetch state list on the basis of the country id
    {
        var areaID = $('#select2SArea').val();
        var id = $('#LocalityID').val();
        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getAllLocalities',
            type: 'POST',
            data: {areaID: areaID},
            success: function (data) {
                // console.log(data);
                $('#localityName').empty();
                $('#localityName').append('<option value="">-- Select Locality --</option>');

                var locality = JSON.parse(data);
                // console.log(locality.length);

                if (locality != null) 
                {
                    // console.log(locality[0].nLocalityIDPK);
                    for (var i = 0; i < locality.length; i++) 
                    {

                        var data2 = id;

                        if (data2 == locality[i].nLocalityIDPK)
                        {
                            $('#localityName').append('<option value=' + locality[i].nLocalityIDPK + ' selected>' + locality[i].tLocalityName + '</option>');
                        }
                        else
                        {
                            $('#localityName').append('<option value=' + locality[i].nLocalityIDPK + '>' + locality[i].tLocalityName + '</option>');
                        }
                    }
                }
                // $("#localityName").trigger("change");   
            }

        }).done(function () {

        });
    }

    $('#AddNewAreaBtn').on('click',function()
    {   
        var cityname = $('#select2City :selected').text();
        var city = $('#select2City :selected').val();
        var areaLatitude = $('#pLatitutde').val();
        var areaLongitude = $('#pLongitude').val();
        var areaName = $('#newArea').val();
        var areaPincode = $('#newAreaPincode').val();
        
        swal({
        title:"Are you sure?",
        text:"You want to Add this Area to "+cityname+" ? ",
        type:"warning", 
        showCancelButton:!0,
        confirmButtonText:"Yes, Add it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>location/createNewArea',
                    type: 'POST',
                    data: {city:city,areaLatitude:areaLatitude,areaLongitude:areaLongitude,areaName:areaName,areaPincode:areaPincode},
                    success: function(data) 
                    {   
                        // console.log(data);
                        if (data.trim() != "Error") 
                        {
                            $('#addAreaModal').modal('hide');
                            $('#areaID').val(data);
                            toastr.success("Area Added successfully","Area");
                            getAreaList();
                        }
                        else
                        {
                            $('#addAreaModal').modal('hide');
                            toastr.error("Error While Adding Area","Area");
                        }
                    }
                });
            }
            else
            {
                $('#addAreaModal').modal('hide');
            }   
       });
        
    });
    $('#AddNewLocalityBtn').on('click',function()
    {   
        var cityname = $('#select2City :selected').text();
        var city = $('#select2City :selected').val();
        var areaLatitude = $('#pLatitutde').val();
        var stateID = $('#select2State :selected').val();
        
        var cityID = $('#select2City :selected').val();
        var areaID = $('#select2SArea :selected').val();
        var areaLongitude = $('#pLongitude').val();
        var LocalityName = $('#newLocality').val();
        var areaPincode = $('#newAreaPincode').val();
        
        swal({
        title:"Are you sure?",
        text:"You want to Add this Locality to "+LocalityName+" ? ",
        type:"warning", 
        showCancelButton:!0,
        confirmButtonText:"Yes, Add it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>location/createNewLocality',
                    type: 'POST',
                    data: {areaID:areaID,stateID:stateID,cityID:cityID,LocalityName:LocalityName,areaLatitude:areaLatitude,areaLongitude:areaLongitude},
                    success: function(data) 
                    {   
                        // console.log(data);
                        if (data.trim() != "Error") 
                        {
                            $('#addLocalityModal').modal('hide');
                            $('#LocalityID').val(data);
                            toastr.success("Locality Added successfully","Locality");
                            getlocality();
                        }
                        else
                        {
                            $('#addLocalityModal').modal('hide');
                            toastr.error("Error While Adding Locality","Locality");
                        }
                     
                    }
                });
            }
            else
            {
                $('#addLocalityModal').modal('hide');
            }   
       });
        
    });


    $('.isLayout_Flat').on('change',function(){
            // console.log("hello");
            var data = $('.isLayout_Flat').val();
            // console.log(data);

            if ($('#isLayoutHidden').val() != "0") 
            {
                $('#isLayoutHidden').val('0');
                $('#m_accordion_4').css('display','block');
                $('#addLayout_Flat').css('display','block');
                $('#layoutPrices').css('display','block');



            } 
            else 
            {
                $('#isLayoutHidden').val('1');
                $('#m_accordion_4').css('display','none');
                $('#addLayout_Flat').css('display','none');
                $('#layoutPrices').css('display','none');
            }
                // alert($('#isLayoutHidden').val());
        });
        $('.isLayout_House').on('change',function(){
            // console.log("hello");
            
                var data = $('.isLayout_House').val();
                // console.log(data);
                
                if ($('#isLayoutHidden_House').val() != "0")  {

                    $('#isLayoutHidden_House').val('0');
                    $('#m_accordion_4_House').css('display','block');
                    $('#addLayout_House').css('display','block');
                    $('#layoutPrices').css('display','block');
                    

                } else {

                    $('#isLayoutHidden_House').val('1');
                    $('#m_accordion_4_House').css('display','none');
                    $('#addLayout_House').css('display','none');
                    $('#layoutPrices').css('display','none');
                    
                }
        });
    
    /*--------------------START : FETCH RECORDS------------------*/

    $('#tFloor').on('input', function(){
            // alert();
            var id = $('#tFloor').val();
            if (id <= 100) 
            {
                $('#pFloor').html('');
                
                $('#pFloor').append('<option value="0">Ground Floor</option>');
                for (var i = 1; i <= id; i++) 
                {   
                    $('#pFloor').append('<option value='+i+'>'+i+'</option>');
                }

            }
            else{
               
                // $('#tFloor').val('');
                $('#tFloor').focus();
                
                $('#pFloor').html('<option value="0">Ground Floor</option>');
            }
        });
    function changeBackground(id) 
    {       
           
            $('#Resale').css('background-color','#ffffff');
            $('#Project').css('background-color','#ffffff');
            $('#Resale').css('color','#000000 !important');
            $('#Project').css('color','#000000 !important');
            $('#'+id).css('background-color','#9a9a9a');
            $('#'+id).css('color','#ffff !important');
            
            $('#Resale :input[type=radio]').attr('checked',false);
            $('#Project :input[type=radio]').attr('checked',false);
            $('#'+id+' :input[type=radio]').attr('checked',true);

            // alert($('#'+id+' :input[type=radio]').val());
    }
    $(window).on('load', function() {
        
        var subType = "<?php if(isset($this->data)) {echo $this->data['nPropertySubTypeIDFK']; } else { echo NULL; } ?>";
        var reraCertified = "<?php if(isset($this->data)) {echo $this->data['bIsPropertyReraCertified']; } else { echo NULL; } ?>";
        var IsBrokerageCharge = "<?php if(isset($this->data)) {echo $this->data['fPropertyBrokerageCharge']; } else { echo NULL; } ?>";

        if(reraCertified != "NULL" && reraCertified == "1")
        {
            $("#reraid").css("display","block");
        }

        if(IsBrokerageCharge != "NULL" && IsBrokerageCharge != "")
        {
            $("#Brokerage").css("display","block");
        }

         

        $("#Bedrooms").trigger("change");

        $('#tFloor').trigger("change");

        var insertedAmenities = Array();

        insertedAmenities = <?php if(isset($this->data)) {echo json_encode($this->data['ListingAmenities']); } else { echo "NULL"; } ?>;

        if(insertedAmenities != 'NULL')
        {
            for (var i = 0; i < insertedAmenities.length; i++) {
                // selectedAmenityArray.push(insertedAmenities[i].nAmenityIDFK);
                showDetails(parseInt(insertedAmenities[i].nAmenityIDFK));
            }
        }
        // console.log(insertedAmenities);
    

        
    });

    /*--------------------END : FETCH RECORDS------------------*/

    function getLayoutUnitsDetails(id) 
    {      
            $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getUnitsDetails',
            type: 'POST',
            success: function(data) 
            {
               
                $('#UnitID_'+id).empty();
             
            
                var country = JSON.parse(data);
                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data['nUnitIDFK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nUnitIDPK)
                    {
                        
                        $('#UnitID_'+id).append('<option value='+country[i].nUnitIDPK+' selected>'+country[i].tUnitName+'</option>');
                    }
                    else
                    {
                      
                        $('#UnitID_'+id).append('<option value='+country[i].nUnitIDPK+'>'+country[i].tUnitName+'</option>');
                       
                    }
                }
              
                $("#UnitID_"+id).trigger("change");

            }
        });
    }
    function getAreaDetails(id) 
    {
        var data = $('#UnitID_'+id+' option:selected').text();

        if ($('#UnitID_'+id).val() != "" && $('#UnitID_'+id).val() != null) {
            $('#bArea_'+id).html('<option value="'+$('#UnitID_'+id).val()+'" Selected>'+data+'</option>');
            $('#cArea_'+id).html('<option value="'+$('#UnitID_'+id).val()+'" Selected>'+data+'</option>');
        }
    }
    function changeBackgroundOwnership(id) 
    {
            $('#Freehold').css('background-color','#ffffff');
            $('#Leasehold').css('background-color','#ffffff');
            $('#Society').css('background-color','#ffffff');
            
            $('#Freehold .m-option__title').css('color','#000000 !important');
            $('#Leasehold .m-option__title').css('color','#000000 !important');
            $('#Society .m-option__title').css('color','#000000 !important');
            $('#'+id).css('background-color','#9a9a9a');
            $('#'+id+' .m-option__title').css('color','#ffff !important');
            // alert($('#'+).val());
    }
     function getLayoutBHKDetails(id) 
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getBHKDetails',
            type: 'POST',
            success: function(data) 
            {
                $('#layoutPropBHK'+id).empty();
               

                var country = JSON.parse(data);  

                //console.log(country.length);

                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data['nBHKIDFK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nBHKIDPK){

                        $('#layoutPropBHK'+id).append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                        
                    }
                    else{

                        $('#layoutPropBHK'+id).append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                      
                    
                    }
                }
                $("#layoutPropBHK"+id).trigger("change");
            }
        });
    }
    var image = Array();
    var files =Array();
    var deletedFiles = Array();

    var count = 0;
    var numFiles = 0;

    var data_image = new FormData();

    // var data_ajax = JSON.stringify(jQuery('#update_Images').serializeArray());
   
   /* var data_image   = new FormData($('#update_Images')[0]);*/
   function deletePropertyImage(id) 
    {
        
        swal({
        title:"Are you sure?",
        text:"You want to delete this Image ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                deletedFiles.push(files[id - 1]);
                // console.log(deletedFiles);
                $('#selector_'+id).remove();

                data_image.append('DeletedFiles[]',files[id - 1]);
                numFiles = numFiles - 1;
                $('#imgCount').text('Images Selected : ( '+numFiles+' / 25 )');
            }   
       });
        
        
    }
    function property_image_preview(vpb_selector_)
    {   

        numFiles = numFiles + $('#property_logo')[0].files.length;

        files.push($('#property_logo')[0].files[0]);

        data_image.append('files[]',$('#property_logo')[0].files[0]);
       // data_image.append('vivek',"vivek");
        //console.log(files);

        if(numFiles <= 25)
        {
            $('#imgCount').text('Images Selected : ( '+numFiles+' / 25 )');
            $('#property_display_preview').css('display','block');
            var id = 1, last_id = last_cid = '';
            $.each(vpb_selector_.files, function(vpb_o_, file)
            {
                if (file.name.length>0) 
                {
                    if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                    else
                    {
                       var reader = new FileReader();
                       
                       reader.onload = function(e) 
                       {    
                            $('#property_display_preview').append(
                           '<div id="selector_'+count+'" class="vpb_wrapper">\
                                <span onclick="deletePropertyImage('+count+')" class="tclose">X</span>\
                                <img class="vpb_image_style" width="100px" height="100px" class="img-thumbnail" src="' + e.target.result + '"\
                                    title="'+escape(file.name) +'" />\
                            </div>');
                       }
                       reader.readAsDataURL(file);
                   }
                }
                else {  return false; }
            });
        }
        else
        {
            swal( {
                title: "", text: "Exceeds the image limits.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
            })
        }
        count = count + 1;

    }

    $('#update_Basic').on('click',function(e){

        e.preventDefault();

        var data_ajax = JSON.stringify(jQuery('#update_Basic_Info').serializeArray());

        $.each(JSON.parse(data_ajax), function(i, val) {
            data_image.append(val.name, val.value);
            // console.log(data_image);   
        });

        for (var i = 0; i < layoutImages.length; i++) 
        {
            if ($('#'+layoutImages[i])[0].files.length != 0) 
            {
                data_image.append('layout_image['+i+']',$('#'+layoutImages[i])[0].files[0]);
            }
            else
            {
                data_image.append('layout_image['+i+']',"NULL");
            }
        }


         $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/updateProperty',
            type: 'POST',
            data: data_image,
            contentType: false,
            processData: false,
            success: function(res) 
            {
                if(res.trim() == "true")
                {

                    swal( {
                        title: "", text: "Listing has been successfully Updated!", type: "success", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                    }
                    )

                    window.setTimeout(function(){
                            // Move to a new location or you can do something else
                            window.location.href = "<?php echo Urls::$BASE; ?>properties";

                        }, 2000);
                }
                else
                {
                   swal( {
                        title: "", text: "Seems something wrong! Try again", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                    }
                    ) 
                }
            }
        });

    });
    $(document).on('input','#possessionDate',function () {

        var str = $(this).val();
        // alert(str.length);
        if(str.length == 2)
        {
            str = str + '-';
        }
        if(str.length == 5)
        {
            str = str + '-';
        }

        $(this).val(str);
        // body...
    });
    function getPropertyType() 
    {

        var id = $('#propList').val();

            $.ajax({
                url: '<?php echo Urls::$BASE ?>properties/getSubTypeDetails',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    // console.log(data);
                    $('#propertySubTypes').html('');
                    // $('#select2State').append('<option value="">-- Select State --</option>');

                    var json = JSON.parse(data);
                    for (var i = 0; i < json.length; i++) {

                        var data = "<?php if(isset($this->data)) {echo $this->data['nPropertySubTypeIDFK']; } else { echo NULL; } ?>";
                        if (data ==json[i].nPropertySubTypeIDPK ) {

                            $('#propertySubTypes').append('\
                                                    <div class="from-group col-lg-2" style="pointer-events: none;" onclick="showselect('+json[i].nPropertySubTypeIDPK+');" id="'+json[i].tPropertySubTypeName+'"><br>\
                                                        <a style="cursor:pointer;pointer-events: none;"  >\
                                                            <img class="ImagesCircle" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING?>'+json[i].tPropertySubTypeImagePath+'" width="40px" height="40px"><i class="fa fa-check-circle tickClassOverImg" id="subTypeImg_'+json[i].nPropertySubTypeIDPK+'"></i>\
                                                            <br><br>\
                                                            <label style="cursor:pointer;" class="subTypeLabelAll" style="color:#818387" id="subTypeLabel_'+json[i].nPropertySubTypeIDPK+'">'+json[i].tPropertySubTypeName+'</label>\
                                                        </a>\
                                                    </div>\ ');

                            $('#subTypeImg_'+json[i].nPropertySubTypeIDPK).css('display','block');

                            showselect(json[i].nPropertySubTypeIDPK);
                        }
                        else{
                            $('#propertySubTypes').append('\
                                                    <div class="from-group col-lg-2" style="cursor:pointer;pointer-events: none;" onclick="showselect('+json[i].nPropertySubTypeIDPK+');" id="'+json[i].tPropertySubTypeName+'"><br>\
                                                        <a style="cursor:pointer;pointer-events: none;"  >\
                                                            <img class="ImagesCircle" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING?>'+json[i].tPropertySubTypeImagePath+'" width="40px" height="40px"><i class="fa fa-check-circle tickClassOverImg" id="subTypeImg_'+json[i].nPropertySubTypeIDPK+'"></i>\
                                                            <br><br>\
                                                            <label style="cursor:pointer;" class="subTypeLabelAll" style="color:#818387" id="subTypeLabel_'+json[i].nPropertySubTypeIDPK+'">'+json[i].tPropertySubTypeName+'</label>\
                                                        </a>\
                                                    </div>\ ');
                        }
                                                
                    }
                }

            });
    }
   

     function SetWashroomValue(id = null) 
    {
        if (id == null) 
        {
            $('#washroom_Office').html('');
            $('#balcony_Office').html('');
            $('#pentry_Office').html('');

            for (var i = 0; i <= 20; i++) 
            {
              
                $('#washroom_Office').append('<option value='+i+'>'+i+'</option>');
                $('#balcony_Office').append('<option value='+i+'>'+i+'</option>');
                $('#pentry_Office').append('<option value='+i+'>'+i+'</option>');
            }
            $('#washroom_Office').append('<option value=21>20+</option>');
            $('#balcony_Office').append('<option value=21>20+</option>');
            $('#pentry_Office').append('<option value=21>20+</option>');


        }
        else
        {
            $('#washroom_Office'+id).html('');
            $('#balcony_Office'+id).html('');
            $('#pentry_Office'+id).html('');

            for (var i = 0; i <= 20; i++) 
            {

                $('#washroom_Office'+id).append('<option value='+i+'>'+i+'</option>');
                $('#balcony_Office'+id).append('<option value='+i+'>'+i+'</option>');
                $('#pentry_Office'+id).append('<option value='+i+'>'+i+'</option>');
            }
            $('#washroom_Office').append('<option value=21>20+</option>');
            $('#balcony_Office').append('<option value=21>20+</option>');
            $('#pentry_Office').append('<option value=21>20+</option>');
        }
    }
    function AddLayout(count) 
    {   

        // var subtype = '<?php echo $this->data['nPropertySubTypeIDFK']; ?>';
        var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';


        //console.log(subTypeName);


        if (subTypeName == "Flat/Apartment") 
        {   
            addLayoutDetials('Flat',count);
        }
        else if (subTypeName == "Penthouse") 
        {
            addLayoutDetials('Flat',count);
        }
        else if (subTypeName == "Independent House") 
        {
            addLayoutDetials('Flat',count);
        }
        else if (subTypeName == "Villa/Bungalow") 
        {
            addLayoutDetials('Flat',count);
        }
        else if (subTypeName == "Row House") 
        {
            addLayoutDetials('Flat',count);
        }
        else if (subTypeName == "Farm House") 
        {
            addLayoutDetials('Flat',count);
        }
        else if (subTypeName == "Plot/Land") 
        {
            addLandlayout('Flat',count);
            // console.log("jslghbfjkfhgjlbgjlfagbfjlabjl");
        }
        else if (subTypeName == "Others") 
        {
            addOtherslayout('Flat',count);
        }
        else if (subTypeName == "Office Space")
        {  
            addCommercial('Flat',count);
        }
        else if (subTypeName == "Shop")
        {
            addCommercial('Flat',count);
        }
        else if (subTypeName == "Showroom") 
        { 
            addCommercial('Flat',count);
        }
        else if (subTypeName == "Corporate House") 
        {
            addCommercial('Flat',count);
        }
        else if (subTypeName == "Industrial Land") 
        {
            addLandlayout('Flat',count);
        }
        else if (subTypeName == "Industrial Factory") 
        { 
            addLandlayout('Flat',count);
        }
        else if (subTypeName == "Warehouse") 
        {
            addLayout('Flat',count);
        }
        else
        {
            var typeID = '<?php echo $this->data['nPropertyTypeIDFK'] ; ?>'; 

            if (typeID == 1)
            {
                addLayoutDetials('Flat',count);
            }
            else if (typeID == 2) 
            {
                addCommercial('Flat',count);
            }
            else 
            {
                addOtherslayout('Flat',count);
            }                                                                                                                                          
        }

    }
    var layoutCount = 2;
    var CommercialLayoutCount = 2;
    var layoutCountHidden = 1;

    function showselect(id) 
    {   
       
        layoutCount = 2;
        // layoutCountHidden = 1;
        CommercialLayoutCount = 2;

        $('#addOtherslayout').css('display','none');
        $('#addLandlayout').css('display','none');
        $('#addCommercial').css('display','none');
        $('#addLayout').css('display','none');
        $('#addLayout_Flat').css('display','none');

        $('#propertiesSubType').val(id);
        
        // alert(listingFor);

        $('#PropoertyDesign_'+id).css('display','block');

        var type = $('#propList').val();

        var listingFor = '<?php echo $this->data['bIsProperty'] ?>';

        // alert(listingFor);

        var subtype = $('#propertiesSubType').val();
        var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';
        // alert(subTypeName);

        
        /* Flat Layout UI  Start */
        if (subTypeName == "Flat/Apartment") 
        {   
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            
            $('#Flat_layout').css('display','block');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Layout_Hidden').css('display','block');
            $('#layoutPrices').css('display','none');
           
           
            $('#villa_Area_details_1').css('display','none');
           
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }       
        }
        else if (subTypeName == "Penthouse") 
        {   
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Flat_layout').css('display','block');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Layout_Hidden').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            
           
            $('#villa_Area_details_1').css('display','none');

            
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
                       
        }
        else if (subTypeName == "Independent House") 
        {   
           $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');

            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Flat_layout').css('display','block');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Layout_Hidden').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#villa_Area_details_1').css('display','block');
            $('#Office').css('display','none');
            
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
                      
        }
        /* Flat Layout UI  End */

        /* Villa Layout UI Start */


        else if (subTypeName == "Villa/Bungalow") 
        {
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Layout_Hidden').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Flat_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
           
            $('#addLayout_Flat').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');
            // $('#villa_Area_details_1').css('display','block');
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
            


             // $(".select2").val("").trigger("change");

            // $('#House_layout').css('display','block');
        }
        else if (subTypeName == "Row House") 
        {
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Layout_Hidden').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Flat_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            
            $('#addLayout_Flat').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');
            $('#villa_Area_details_1').css('display','block');
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
           
            // $('#House_layout').css('display','block');
        }
        else if (subTypeName == "Farm House") 
        {
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Layout_Hidden').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Flat_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
           
            $('#addLayout_Flat').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');
            $('#villa_Area_details_1').css('display','block');
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
           

        }


        /* Villa Layout UI End */

        /* Basic UI Display Start */    
        
        else if (subTypeName == "Plot/Land") 
        {
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Build_Area_Hidden').css('display','none');
            $('#Land_layout').css('display','none');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            $('#addLayout_Flat').css('display','none');
            $('#villa_Area_details_1').css('display','none');
            $('#Carparking').css('display','none');
            if (listingFor == 1) 
            {
                
                $('#addLandlayout').css('display','block');
            }
        }
        
        /* Basic UI Display End */

        /* Others UI Display Start */

        else if (subTypeName == "Others") 
        {
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Others_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            $('#addLayout_Flat').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addOtherslayout').css('display','block');
            }
            
        } 

        /* Others UI Display End */



        /* Office UI Display Start */

        else if (subTypeName == "Office Space") 
        {
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');

            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Office').css('display','block');
            $('#addLayout_Flat').css('display','none');
            $('#layoutPrices').css('display','none');
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','none');
            

            if (listingFor == 1) 
            {
                $('#addLayout').css('display','block');
            }
            
        }
        else if (subTypeName == "Shop")
        {
            // $('#S_Build_Area_Hidden').css('display','block');
            // $('#Build_Area_Hidden').css('display','block');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');

            $('#S_Build_Area_Hidden').css('display','block');
            $('#Build_Area_Hidden').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Office').css('display','block');
            $('#layoutPrices').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            $('#Carparking').css('display','block');
            if (listingFor == 1) 
            {
               
                $('#addLayout').css('display','block');
            }

        }
        else if (subTypeName == "Showroom") 
        {   
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');

            
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Office').css('display','block');
            $('#layoutPrices').css('display','none');
            
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','none');
            // $(".select2").val("").trigger("change");

            if (listingFor == 1) 
            {
                $('#addLayout').css('display','block');
            }
           
           
        }
        else if (subTypeName == "Corporate House") 
        {   
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');

            
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Office').css('display','block');
            $('#layoutPrices').css('display','none');
           
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','block');
            // $(".select2").val("").trigger("change");

            if (listingFor == 1) 
            {
                $('#addLayout').css('display','block');
            }
            
        }

        /* Office UI Display End */

        /* Basic UI Display Start */ 

        else if (subTypeName == "Industrial Land") 
        {   
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Carparking').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            if (listingFor == 1) 
            {
                $('#addLandlayout').css('display','block');
            }
            
           
            $('#layoutPrices').css('display','none');
            
            // $(".select2").val("").trigger("change");

        }
            /* villa_Area_details_1 Show UI Display Start */ 
            else if (subTypeName == "Industrial Factory") 
            {   
                $('#S_Build_Area_Hidden').css('display','none');
                $('#Build_Area_Hidden').css('display','none');

                $('#S_Build_Area_Hidden_1').css('display','block');
                $('#Build_Area_Hidden_1').css('display','none');

                
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Office').css('display','none');
                $('#Ploat_Area').html("Plot Area");
                $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
               
                $('#layoutPrices').css('display','none');
               
                $('#Carparking').css('display','none');
                $('#villa_Area_details_1').css('display','block');
                // $(".select2").val("").trigger("change");

                if (listingFor == 1) 
                {
                    $('#addLandlayout').css('display','block');
                }
                
            }

            else if (subTypeName == "Warehouse") 
            {   
                $('#S_Build_Area_Hidden').css('display','none');
                $('#Build_Area_Hidden').css('display','none');

                $('#S_Build_Area_Hidden_1').css('display','block');
                $('#Build_Area_Hidden_1').css('display','none');

                
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Office').css('display','none');
                $('#Ploat_Area').html("Plot Area");
                $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
               
                $('#layoutPrices').css('display','none');
               
                $('#Carparking').css('display','none');
                $('#villa_Area_details_1').css('display','block');
                // $(".select2").val("").trigger("change");

                if (listingFor == 1) 
                {
                    $('#addLayout').css('display','block');
                }
                
            } 
            /* villa_Area_details Show UI Display Start */ 

        /* Basic UI Display End */

        else
        {
            /* Flat_layout UI Display Start */ 

            if (type == 1)
            {
               
                $('#S_Build_Area_Hidden').css('display','block');
                $('#Build_Area_Hidden').css('display','block');
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Office').css('display','none');
                $('#Ploat_Area').html("Plot Area");
                $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
                $('#Flat_layout').css('display','block');
                $('#layoutPrices').css('display','none');
                
                $('#Carparking').css('display','block');
                $('#villa_Area_details_1').css('display','none');

                if (listingFor == 1) 
                {
                    $('#addLayout_Flat').css('display','block');   
                }
                 
            }

            /* Flat_layout UI Display End */

            /* Office UI Display Start */
            else
            {
                $('#S_Build_Area_Hidden_1').css('display','block');
                $('#Build_Area_Hidden_1').css('display','block');
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Ploat_Area').html("Super Built Up Area");
                $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
                $('#Office').css('display','block'); 
                $('#addLayout').css('display','block');
                $('#layoutPrices').css('display','none'); 
                $('#BuildUpArea').val('');
                $('#carpetArea').val('');
                $('#tFloor_Land').select2().val("1").trigger("change");
                $('#addLayout_Flat').css('display','none');
                $('#Carparking').css('display','block');
                $('#villa_Area_details_1').css('display','none');

                if (listingFor == 1) 
                {
                  
                    $('#addLayout').css('display','block');
                }
                
            }

            /* Office UI Display End */
        }
    }


    function getfacing() 
    {
        $.ajax({

        url: '<?php echo Urls::$BASE ?>properties/getfacingDetails',
        type: 'POST',
        success: function (data) {
            // console.log(data);
           var facing = JSON.parse(data);

            //console.log(country.length);
            $('#facingID').html('');

            for(var i = 0; i < facing.length; i++)
            {
                var data = "<?php if(isset($this->data)) {echo $this->data['nFacingIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == facing[i].nFacingIDPK)
                    $('#facingID').append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');
                else
                    $('#facingID').append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
            }
            $("#facingID").trigger("change");
        }

        });
    }

    function getUnitsDetails() 
    {
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>properties/getUnitsDetails',
        type: 'POST',
        success: function(data) 
        {
            $('#UnitID').empty();
            // $('#UnitID').append('<option value="">Select Unit</option>');

            var units = JSON.parse(data);

            //console.log(country.length);

            for(var i = 0; i < units.length; i++)
            {
                var data = "<?php if(isset($this->data)) {echo $this->data['nUnitIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == units[i].nUnitIDPK)
                    $('#UnitID').append('<option value='+units[i].nUnitIDPK+' selected>'+units[i].tUnitName+'</option>');
                else
                    $('#UnitID').append('<option value='+units[i].nUnitIDPK+'>'+units[i].tUnitName+'</option>');
            }
            $("#UnitID").trigger("change");
        }
        });
    }

    function getAmenities() 
    {   
        var htmlString = '';
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>properties/getAmenitiesDetails',
        type: 'POST',
        success: function(data) 
        {
            $('#Amenities').empty();
            var amenity = JSON.parse(data);
            for(var i = 0; i < amenity.length; i++)
            {
                htmlString = '<div class="col-lg-3"><br><a style="cursor: pointer;" class="buttonDemo" onclick="showDetails('+amenity[i].nAmenityIDPK+');"><img src="<?php echo Urls::$BASE.Urls::$ABOUTUS?>'+amenity[i].tAmenityImagePath+'" width="40px" height="40px" class="unChecked"  id="featureImg_'+amenity[i].nAmenityIDPK+'"><i class="fa fa-check-circle tickClassOverImg" id="iconImg_'+amenity[i].nAmenityIDPK+'" ></i></a><br><br><label style="color:#818387" class="subTypeLabelAll" id="features_'+amenity[i].nAmenityIDPK+'">'+amenity[i].tAmenityName+'</label></div>';  
                // selectedAmenityArray.push(amenity[i].nAmenityIDPK);
                $('#Amenities').append(htmlString);
                
            }  
            // console.log("aminities : "+selectedAmenityArray);

        }
        });
    }
  
    
    var selectedAmenityArray = Array();
  
    function showDetails(id) {             
        
        if($.inArray(id,selectedAmenityArray) != -1)
        {
            var index = selectedAmenityArray.indexOf(id);

            if(index!=-1){

               selectedAmenityArray.splice(index, 1);
            }

        }
        else
        { 
            selectedAmenityArray.push(id);
        }

        var checkForClass = $('#featureImg_'+id).hasClass('unChecked');

        if(checkForClass) 
        {
            $('#featureImg_'+id).removeClass('unChecked');
            $('#iconImg_'+id).css('display','block');
            $('#features_'+id).css('color','black');
        } 
        else
        {
            $('#featureImg_'+id).addClass('unChecked');
            $('#iconImg_'+id).css('display','none');
            $('#features_'+id).css('color','#818387');
        }

        $('#AminitiesHidden').val(selectedAmenityArray);
        // console.log('VALUE : ');
        //console.log(selectedAmenityArray);

    }
   


    function getBHKDetails() 
    {
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>properties/getBHKDetails',
        type: 'POST',
        success: function(data) 
        {
            $('#propBHK').empty();
            // $('#propBHK').append('<option value="">Select BHK</option>');

            var bhkResponse = JSON.parse(data);

            //console.log(country.length);

            for(var i = 0; i < bhkResponse.length; i++)
            {
                var data = "<?php if(isset($this->data)) {echo $this->data['nBHKIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == bhkResponse[i].nBHKIDPK)
                    $('#propBHK').append('<option value='+bhkResponse[i].nBHKIDPK+' selected>'+bhkResponse[i].tBHKName+'</option>');
                else
                    $('#propBHK').append('<option value='+bhkResponse[i].nBHKIDPK+'>'+bhkResponse[i].tBHKName+'</option>');
            }
            $("#propBHK").trigger("change");
        }
        });
    }
    function getPropertyLocation(PropertyID) 
    {
       $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getPropertyLocation',
            type: 'POST',
            data: {PropertyID:PropertyID},
            success: function(data) 
            {   
                var locationInfo = JSON.parse(data);
                
                $('#streetName').val(locationInfo[0].tPropertyStreetName); 
                $('#pAddress').val(locationInfo[0].tPropertyAddress);
                $('#mapLocationLink').val(locationInfo[0].tPropertyMapLocationLink);
                getLocations(locationInfo[0].nLocalityIDFK);
             
            }
        });

    }

    function getLocations(localityid = null)
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getLocations',
            type: 'POST',
            success: function(data) 
            {
                $('#locationID').empty();
               

                var locations = JSON.parse(data);

                //console.log(country.length);

                for(var i = 0; i < locations.length; i++)
                {
                    
                    if (localityid == null) 
                    { 
                        localityid =  "<?php if(isset($this->data)) {echo $this->data['nLocalityIDFK']; } else { echo "Null"; } ?>"; 
                        //console.log(localityid);
                    }

                    if(localityid == locations[i].nLocalityIDPK){

                        $('#locationID').append('<option Selected value='+locations[i].nAreaIDPK+' data-areaID='+locations[i].nAreaIDPK+' data-cityid='+locations[i].nCityIDFK+' data-stateid='+locations[i].nStateIDPK+' data-countryid='+locations[i].nCountryIDFK+' data-localityid='+locations[i].nLocalityIDPK+' data-latitude='+locations[i].dAreaLatitude+' data-longitude='+locations[i].dAreaLongitude+'>'+locations[i].tLocalityName+' , '+locations[i].tAreaName+' , '+locations[i].tCityName+' - '+locations[i].tStateName+'</option>');
                    }
                    else{
                        $('#locationID').append('<option value='+locations[i].nAreaIDPK+' data-areaID='+locations[i].nAreaIDPK+' data-cityid='+locations[i].nCityIDFK+' data-stateid='+locations[i].nStateIDPK+' data-countryid='+locations[i].nCountryIDFK+' data-localityid='+locations[i].nLocalityIDPK+' data-latitude='+locations[i].dAreaLatitude+' data-longitude='+locations[i].dAreaLongitude+'>'+locations[i].tLocalityName+' , '+locations[i].tAreaName+' , '+locations[i].tCityName+' - '+locations[i].tStateName+'</option>');
                    }
                }
                $("#locationID").trigger("change");
            }
        });
    }

    $(document).on('change','#locationID',function () {
        // body...
        // $(this).val('cityID');
        var selectedCityID = $(this).find('option:selected').data('cityid');
        var selectedAreaID = $(this).find('option:selected').data('areaID');

        var selectedlocalityID = $(this).find('option:selected').data('localityid');
        var selectedStateID = $(this).find('option:selected').data('stateid');
        var selectedCountryID = $(this).find('option:selected').data('countryid');
        var latitude = $(this).find('option:selected').data('latitude');
        var longitude = $(this).find('option:selected').data('longitude');

        $('#LocalityID').val(selectedlocalityID);
        $('#areaID').val(selectedAreaID);
        $('#cityID').val(selectedCityID);
        $('#stateID').val(selectedStateID);
        $('#countryID').val(selectedCountryID);
        $('#pLatitutde').val(latitude);
        $('#pLongitude').val(longitude);

    });
    var layoutImages = Array();

    function layout_preview(vpb_selector_,count)
    {   
        // console.log(vpb_selector_);

        
        //console.log(layoutImages);

        var numFiles = $('#layout_image'+count)[0].files.length;
        
            $('#layout_preview_display_'+count).html(''); 
            $('#RemoveImage_'+count).css('display','block');
            $('#EditImage_'+count).css('display','block');

            

            var id = 1, last_id = last_cid = '';
            $.each(vpb_selector_.files, function(vpb_o_, file)
            {
                if (file.name.length>0) 
                {
                    if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                    else
                    {
                        //Clear previous previewed files and start again
                       
                       var reader = new FileReader();
                       
                       reader.onload = function(e) 
                       {
                           $('#layout_preview_display_'+count).append(
                           '  <br><div class="col-lg-8">\
                           <img class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" title="'+ escape(file.name) +'" /><br /></div>  \
                           ');
                       }
                       reader.readAsDataURL(file);
                   }
                }
                else {  return false; }
            });
        
    }
    function removeOpenParking(id) 
    {            
        var value = $('#Cparking'+id).val();
        if (parseInt(value) != -1 ) 
        {
            value = parseInt(value) - 1;    
        }
        $('#Cparking'+id).val(value);
    }
    function RemoveImage(id,count) 
    {
        $('#'+id).val('');
        $('#layout_preview_display_'+count).html('<i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>');
        $('#RemoveImage_'+count).css('display','none');
        $('#EditImage_'+count).css('display','none');

    }
    function addOpenParking(id) 
    {
       var value = $('#Cparking'+id).val();

        if (value < 10) {

            if (parseInt(value) != -1 ) 
            {
                value = parseInt(value) + 1;    
            }
            $('#Cparking'+id).val(value);
        }
    }    
    
   
    function getLayoutFacing(id) 
    {

        $.ajax({
            url: '<?php echo Urls::$BASE ?>properties/getfacingDetails',
            type: 'POST',
            success: function (data) {
                // console.log(data);
               var facing = JSON.parse(data);

                //console.log(country.length);
                $('#facingID'+id).html('');
               

                for(var i = 0; i < facing.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data['nFacingIDFK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == facing[i].nFacingIDPK)
                    { 
                        $('#facingID'+id).append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');
                        

                    }
                    else{

                        $('#facingID'+id).append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
                       
                    }
                }
                $("#facingID").trigger("change");
            }
        });
    }
    function ChangeUnitePriceName(count) 
    {
        var data = $('#UnitID_'+count+' :selected').text();
        // alert(data);
        $('#perUnitPrice').text('Price Per '+data);
    }
     function getBHKDetails() 
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getBHKDetails',
            type: 'POST',
            success: function(data) 
            {
                $('#propBHK').empty();
               
              
                $('#propBHK_Other').empty();
                
                $('#layoutPropBHK').empty();
                
                var country = JSON.parse(data);  

                //console.log(country.length);

                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data['nBHKIDFK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nBHKIDPK){

                        $('#propBHK').append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                       
                        $('#propBHK_Other').append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                        $('#layoutPropBHK').append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                        
                    }
                    else{

                        $('#propBHK').append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                      
                        $('#propBHK_Other').append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                        $('#layoutPropBHK').append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                        
                    
                    }
                }
            }
        });
    }
   

    $(document).on('click','#addLayout_House',function(){

        getLayoutBHKDetails(layoutCount);
        getLayoutFacing(layoutCount);
        addLayoutDetials("House");

    });
    $(document).on('click','#addLayout_Flat',function(){
        
        // getLayoutBHKDetails(layoutCount);
        getLayoutFacing(layoutCount);
        addLayoutDetials("Flat");
    });

    $('.input-group-append').click(function() {
        $(".datepicker").focus();
      });



    function remove_listing_image(id)
    {
        // console.log(id);
        swal({
        title:"Are you sure?",
        text:"You want to delete this Image ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE; ?>properties/delete_listing_image",
                    data:{'id':id},
                    success: function(data)
                    {    
                        if(data == "true")
                        {
                            $('#hide_place_Img'+id).fadeOut();
                            swal({
                                type:"success",
                                title:"Image deleted successully",
                                showConfirmButton:!1,timer:1500
                            });
                        }               
                    }
                }); 
            }   
       }); 
    }

    function addLayoutDetials(id,count= null)
    {      
        $('#layoutCount').val(layoutCountHidden);

        var totalData = <?php echo json_encode($this->data['ListingDetails']); ?>;

        // console.log("data : "+ totalData[count]['nProjectIDPK']);
        var removeDiv = '';
        var listingFor = '<?php echo $this->data['bIsProperty'] ?>';

        var showable = "collapsed";
        var showbody = "";
        if (count == 0) 
        {
            showable = "";
            showbody = "show";
        }
        if (listingFor == 1) 
        {
            removeDiv = '<span class="m-accordion__item-icon" onclick="removeDivTagAjax('+layoutCount+','+totalData[count]["nProjectIDPK"]+')" >\
                                <i class="fa fa-times" style="float: right;font-size: 12px;"></i>\
                            </span>';
        }
        if (count!= null) 
        {
            $('#layoutDetails_'+id).append('\
                                <div id="'+layoutCount+'">\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+layoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head  '+showable+'" role="tab" id="m_accordion_3_item_7_head'+layoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+layoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                               '+removeDiv+'\
                                            </div>\
                                            <div class="m-accordion__item-body collapse '+showbody+'" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+layoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+layoutCount+'" data-parent="#m_accordion_4_'+layoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                    <input id="LayoutID'+layoutCount+'" name="layout[LayoutID][]" type="hidden" value="'+totalData[count]['nProjectIDPK']+'">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+layoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+layoutCount+'" onchange="getAreaDetails('+layoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+layoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+layoutCount+'"  maxlength="6" value="'+totalData[count]["fProjectSuperBuiltArea"]+'" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" id="BuildUpArea_'+layoutCount+'" mxlength="6"class="form-control m-input" value="'+totalData[count]["fProjectBuiltArea"]+'" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" value="'+totalData[count]["fProjectCarpetArea"]+'" id="carpetArea_'+layoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+layoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+layoutCount+'" class="form-control m-input" value="'+totalData[count]["fProjectSuperBuiltArea"]+'" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+layoutCount+'"  maxlength="6" value="'+totalData[count]["fProjectBuiltArea"]+'" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+layoutCount+'" value="'+totalData[count]["fProjectCarpetArea"]+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  value="'+totalData[count]["fConstructionSuperBuiltArea"]+'" id="constructionSuperBuildUpArea_'+layoutCount+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+layoutCount+'""  maxlength="6"  value="'+totalData[count]["fConstructionBuiltArea"]+'"name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+layoutCount+'"" value="'+totalData[count]["fConstructionCarpetArea"]+'" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-lg-4">\
                                                            <label>Select BHK</label>\
                                                            <select style="width: 100%;" name="layout['+"layoutPropBHK"+'][]" class="form-control m-select2" id="layoutPropBHK'+layoutCount+'">\
                                                                <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="3+">3+ </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Select Facing</label>\
                                                            <select style="width: 100%;" name="layout['+"facingID"+'][]" class="form-control m-select2" id="facingID'+layoutCount+'">\
                                                                 <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="4">4 </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                    <label>Property Furnished</label>\
                                                                    <br>\
                                                                    <select id="propertyFurnishedID'+layoutCount+'" name="layout['+"propertyFurnishedID"+'][]" class="form-control m-select2" style="width: 100%;">\
                                                                       <option value="1">Furnished</option>\
                                                                       <option value="0">Un-Furnished </option> \
                                                                       <option value="2">Semi-Furnished </option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+layoutCount+'\','+layoutCount+')" id="RemoveImage_'+layoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+layoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+layoutCount+'\').click();" accept="image/x-png,image/jpeg,image/jpg">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" id="layout_image'+layoutCount+'" onchange="layout_preview(this,'+layoutCount+')" accept="image/x-png,image/jpeg,image/jpg" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+layoutCount+'\').click();" id="layout_preview_display_'+layoutCount+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-8">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Bedrooms</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Bedrooms"+'][]"  id="Bedrooms'+layoutCount+'">\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Bathrooms</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Bathrooms"+'][]" id="Bathrooms'+layoutCount+'">\
                                                                        <option value="0">0</option>\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Balconies</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Balconies"+'][]" id="Balconies'+layoutCount+'">\
                                                                        <option value="0">0</option>\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9">9</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                   <label>Total Floors</label>\
                                                                   <input type="text" onkeypress="return isNumberKey(event);" placeholder="Total Floors" class="form-control m-input" style="width: 100%;" class="form-control m-select2" name="layout['+"tFloor"+'][]"  max="100" id="tFloor'+layoutCount+'" oninput="getPFlor('+layoutCount+');">\
                                                                </div>\
                                                                <div class="col-lg-4" id="Layout_Hidden">\
                                                                    <label>Property On Floor</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"pFloor"+'][]" id="pFloor'+layoutCount+'" style="display: block;">\
                                                                        <option value="0">Ground Floor</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Available Parking</label>&nbsp;&nbsp;&nbsp;\
                                                                    <div class="input-group">\
                                                                        <div class="input-group-append" style="cursor: pointer;" onclick="removeOpenParking('+layoutCount+')">\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-minus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                       <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" id="Cparking'+layoutCount+'" name="layout['+"Cparking"+'][]"  value="0"/>\
                                                                       <div class="input-group-append" style="cursor: pointer;" onclick="addOpenParking('+layoutCount+')"\>\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-plus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <p style="width: 100%;">Store Room</p>\
                                                                    <input type="hidden" name="layout['+"storeRoomAvailable"+'][0]" id="storeRoomAvailable'+layoutCount+'" value="">\
                                                                    <input  name="layout['+"storeRoomAvailable"+'][]" id="StoreRoom'+layoutCount+'" class="StoreRoom" onchange=AllcheckBoxArray("StoreRoom","storeRoomAvailable'+layoutCount+'")  type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <input type="hidden" name="layout['+"poojaRoomAvailable"+'][0]" id="poojaRoomAvailable'+layoutCount+'" value="">\
                                                                    <p style="width: 100%;">Pooja Room</p>\
                                                                    <input  name="layout['+"poojaRoomAvailable"+'][]" id="PoojaRoom'+layoutCount+'" class="PoojaRoom" onchange=AllcheckBoxArray("PoojaRoom","poojaRoomAvailable'+layoutCount+'") type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+layoutCount+'" maxlength="11" oninput="GetProcePerUnit('+layoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+layoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+layoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+layoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]" id="Booking'+layoutCount+'" oninput="convertNumbertoWords(\'Booking'+layoutCount+'\',\'bnumToWords'+layoutCount+'\');" maxlength="9" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+layoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+layoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+layoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+layoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+layoutCount+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+layoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+layoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                ');
            
            
            $('#propertyFurnishedID'+layoutCount).select2();
            $('#propBHK'+layoutCount).select2();
            $('#layoutPropBHK'+layoutCount).select2();

            $('#facingID'+layoutCount).select2();
            $('#layoutType'+layoutCount).select2();
            $('#typeprop'+layoutCount).select2();
            $('#propType'+layoutCount).select2();
            $('#propList'+layoutCount).select2();
            $('#propListtwo'+layoutCount).select2();
            $('#propTypeone'+layoutCount).select2();
            $('#furnished'+layoutCount).select2();
            $('#pFloor'+layoutCount).select2();
          
            $('#Bedrooms'+layoutCount).select2();
            $('#Bathrooms'+layoutCount).select2();
            $('#Balconies'+layoutCount).select2();
            $('#furnished'+layoutCount).select2();
            $('#pReportingTime'+layoutCount).select2();
            $('#UnitID_'+layoutCount).select2();
            $('#bArea_'+layoutCount).select2();
            $('#cArea_'+layoutCount).select2();


            $('#Bedrooms'+layoutCount).select2().val(totalData[count]['nBedrooms']).trigger("change");
            $('#facingID'+layoutCount).select2().val(totalData[count]['nFacingIDFK']).trigger("change");
            $('#Balconies'+layoutCount).select2().val(totalData[count]['nBalconies']).trigger("change");
            $('#Bathrooms'+layoutCount).select2().val(totalData[count]['nBathrooms']).trigger("change");

            if (totalData[count]['tLayoutUmages'] != "") 
            {   
                $('#RemoveImage_'+layoutCount).css('display','block');
                $('#EditImage_'+layoutCount).css('display','block');
                $('#layout_preview_display_'+layoutCount).html('');
                $('#layout_preview_display_'+layoutCount).append('<br><div class="col-lg-8">\
                               <img class="vpb_image_style" class="img-thumbnail" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING; ?>'+totalData[count]['tLayoutUmages']+'" /><br /></div>');
            }
            $('#tFloor'+layoutCount).val(totalData[count]['nTotalFloors']);

            for (var i = 0; i < totalData[count]['nTotalFloors'] ; i++) 
            {   
                if (i == totalData[count]['nPropertyOnFloor']) 
                {
                    $('#pFloor'+layoutCount).append('<option value='+i+'  selected>'+i+'</option>');
                }
                else
                {
                    $('#pFloor'+layoutCount).append('<option value='+i+' >'+i+'</option>');
                }
                
            }
            

            if (totalData[count]['bIsPoojaRoomAvailable'] == 1) 
            {
                $('#PoojaRoom'+layoutCount).attr("checked", "checked");
            }

            if (totalData[count]['bIsStoreRoomAvailable'] == 1) 
            {
                $('#StoreRoom'+layoutCount).attr("checked", "checked");
            }
            if (totalData[count]['bIsExpectedPriceNegotiable'] == 1) 
            {
                $('#ExpectedNegociable'+layoutCount).attr("checked", "checked");
            }

            if (totalData[count]['bIsPriceShow'] == 1) 
            {
                $('#priceDisplayed_'+layoutCount).attr("checked", "checked");
            }

            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+layoutCount);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+layoutCount);
            AllcheckBoxArray('StoreRoom','storeRoomAvailable'+layoutCount);
            AllcheckBoxArray('PoojaRoom','poojaRoomAvailable'+layoutCount);
            layoutImages.push('layout_image'+layoutCount);
            

            $('#Cparking'+layoutCount).val(totalData[count]['nCarParkings']);
            $('#Expected_'+layoutCount).val(totalData[count]['fProjectExpectedPrice']);
            $('#PricePerUnit_'+layoutCount).val(totalData[count]['fPricePerUnitArea']);
            $('#Booking'+layoutCount).val(totalData[count]['fProjectBookingPrice']);

            $('#Expected_'+layoutCount).val(parseInt(totalData[count]['fProjectExpectedPrice']));
            $('#PricePerUnit_'+layoutCount).val(parseInt(totalData[count]['fPricePerUnitArea']));
            $('#Booking'+layoutCount).val(parseInt(totalData[count]['fProjectBookingPrice']));


            convertNumbertoWords('Expected_'+layoutCount,'numToWords'+layoutCount);
            convertNumbertoWords('PricePerUnit_'+layoutCount,'pnumToWords'+layoutCount);
            convertNumbertoWords('Booking'+layoutCount,'bnumToWords'+layoutCount);
            layoutCountHidden = layoutCountHidden + 1;
        }   
        else
        {
            layoutCountHidden = layoutCountHidden + 1;

            $('#layoutDetails_'+id).append('\
                                <div id="'+layoutCountHidden+'">\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+layoutCountHidden+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+layoutCountHidden+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+layoutCountHidden+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                <span class="m-accordion__item-icon" onclick="removeDivTag('+layoutCountHidden+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 12px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+layoutCountHidden+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+layoutCountHidden+'" data-parent="#m_accordion_4_'+layoutCountHidden+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+layoutCountHidden+'" onchange="getAreaDetails('+layoutCountHidden+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+layoutCountHidden+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="carpetArea_'+layoutCountHidden+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+layoutCountHidden+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+layoutCountHidden+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+layoutCountHidden+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+layoutCountHidden+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+layoutCountHidden+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+layoutCountHidden+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-lg-4">\
                                                            <label>Select BHK</label>\
                                                            <select style="width: 100%;" name="layout['+"layoutPropBHK"+'][]" class="form-control m-select2" id="layoutPropBHK'+layoutCountHidden+'">\
                                                                <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="3+">3+ </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Select Facing</label>\
                                                            <select style="width: 100%;" name="layout['+"facingID"+'][]" class="form-control m-select2" id="facingID'+layoutCountHidden+'">\
                                                                 <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="4">4 </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                    <label>Property Furnished</label>\
                                                                    <br>\
                                                                    <select id="propertyFurnishedID'+layoutCountHidden+'" name="layout['+"propertyFurnishedID"+'][]" class="form-control m-select2" style="width: 100%;">\
                                                                       <option value="1">Furnished</option>\
                                                                       <option value="0">Un-Furnished </option> \
                                                                       <option value="2">Semi-Furnished </option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+layoutCountHidden+'\','+layoutCountHidden+')" id="RemoveImage_'+layoutCountHidden+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+layoutCountHidden+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();" accept="image/x-png,image/jpeg,image/jpg">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" id="layout_image'+layoutCountHidden+'" onchange="layout_preview(this,'+layoutCountHidden+')" accept="image/x-png,image/jpeg,image/jpg" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();" id="layout_preview_display_'+layoutCountHidden+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-8">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Bedrooms</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Bedrooms"+'][]"  id="Bedrooms'+layoutCountHidden+'">\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Bathrooms</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Bathrooms"+'][]" id="Bathrooms'+layoutCountHidden+'">\
                                                                        <option value="0">0</option>\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Balconies</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Balconies"+'][]" id="Balconies'+layoutCountHidden+'">\
                                                                        <option value="0">0</option>\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                   <label>Total Floors</label>\
                                                                   <input type="text" onkeypress="return isNumberKey(event);" placeholder="Total Floors" class="form-control m-input" style="width: 100%;" class="form-control m-select2" name="layout['+"tFloor"+'][]"  max="100" id="tFloor'+layoutCountHidden+'" oninput="getPFlor('+layoutCountHidden+');">\
                                                                </div>\
                                                                <div class="col-lg-4" id="Layout_Hidden">\
                                                                    <label>Property On Floor</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"pFloor"+'][]" id="pFloor'+layoutCountHidden+'" style="display: block;">\
                                                                        <option value="0">Ground Floor</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Available Parking</label>&nbsp;&nbsp;&nbsp;\
                                                                    <div class="input-group">\
                                                                        <div class="input-group-append" style="cursor: pointer;" onclick="removeOpenParking('+layoutCountHidden+')">\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-minus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                       <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" id="Cparking'+layoutCountHidden+'" name="layout['+"Cparking"+'][]"  value="0"/>\
                                                                       <div class="input-group-append" style="cursor: pointer;" onclick="addOpenParking('+layoutCountHidden+')"\>\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-plus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="form-group row">\
                                                               <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <p style="width: 100%;">Store Room</p>\
                                                                    <input type="hidden" name="layout['+"storeRoomAvailable"+'][0]" id="storeRoomAvailable'+layoutCountHidden+'" value="">\
                                                                    <input  name="layout['+"storeRoomAvailable"+'][]" id="StoreRoom'+layoutCountHidden+'" class="StoreRoom" onchange=AllcheckBoxArray("StoreRoom","storeRoomAvailable'+layoutCountHidden+'")  type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <input type="hidden" name="layout['+"poojaRoomAvailable"+'][0]" id="poojaRoomAvailable'+layoutCountHidden+'" value="">\
                                                                    <p style="width: 100%;">Pooja Room</p>\
                                                                    <input  name="layout['+"poojaRoomAvailable"+'][]" id="PoojaRoom'+layoutCountHidden+'" class="PoojaRoom" onchange=AllcheckBoxArray("PoojaRoom","poojaRoomAvailable'+layoutCountHidden+'") type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+layoutCountHidden+'" maxlength="11" oninput="GetProcePerUnit('+layoutCountHidden+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+layoutCountHidden+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]" id="Booking'+layoutCountHidden+'" oninput="convertNumbertoWords(\'Booking'+layoutCountHidden+'\',\'bnumToWords'+layoutCountHidden+'\');" maxlength="9" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+layoutCountHidden+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+layoutCountHidden+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+layoutCountHidden+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+layoutCountHidden+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                ');

            $('#propertyFurnishedID'+layoutCountHidden).select2();
            $('#propBHK'+layoutCountHidden).select2();
            $('#layoutPropBHK'+layoutCountHidden).select2();

            

            $('#kitchen'+layoutCountHidden).select2();
            $('#facingID'+layoutCountHidden).select2();
            $('#layoutType'+layoutCountHidden).select2();
            $('#typeprop'+layoutCountHidden).select2();
            $('#propType'+layoutCountHidden).select2();
            $('#propList'+layoutCountHidden).select2();
            $('#propListtwo'+layoutCountHidden).select2();
            $('#propTypeone'+layoutCountHidden).select2();
            $('#furnished'+layoutCountHidden).select2();
            $('#pFloor'+layoutCountHidden).select2();
          
            $('#Bedrooms'+layoutCountHidden).select2();
            $('#Bathrooms'+layoutCountHidden).select2();
            $('#Balconies'+layoutCountHidden).select2();
            $('#furnished1'+layoutCountHidden).select2();
            $('#pReportingTime'+layoutCountHidden).select2();
            $('#UnitID_'+layoutCountHidden).select2();
            $('#bArea_'+layoutCountHidden).select2();
            $('#cArea_'+layoutCountHidden).select2();

            getLayoutUnitsDetails(layoutCountHidden);
            getLayoutBHKDetails(layoutCountHidden);
            getLayoutFacing(layoutCountHidden);

            $('#S_Build_Area_Hidden_'+layoutCountHidden).css('display','block');
            $('#Build_Area_Hidden_'+layoutCountHidden).css('display','block');

            if (totalData[count]['bIsPoojaRoomAvailable'] == 1) 
            {
                $('#PoojaRoom'+layoutCountHidden).attr("checked", "checked");
            }

            if (totalData[count]['bIsStoreRoomAvailable'] == 1) 
            {
                $('#StoreRoom'+layoutCountHidden).attr("checked", "checked");
            }
            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+layoutCountHidden);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+layoutCountHidden);
            AllcheckBoxArray('StoreRoom','storeRoomAvailable'+layoutCountHidden);
            AllcheckBoxArray('PoojaRoom','poojaRoomAvailable'+layoutCountHidden);
            layoutImages.push('layout_image'+layoutCountHidden);

            var subtype = $('#propertiesSubType').val();
            var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';

            if (subTypeName == "Villa/Bungalow") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
            else if (subTypeName == "Row House") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
            else if (subTypeName == "Farm House") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }

        }
       
        
        k = layoutCount;
        // alert(k);

        $('#S_Build_Area_Hidden_'+layoutCount).css('display','block');
        $('#Build_Area_Hidden_'+layoutCount).css('display','block');

        var subtype = $('#propertiesSubType').val();
       var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';

        if (subTypeName == "Villa/Bungalow") 
        {
            $('#villa_Area_details_'+layoutCount+'').css('display','block'); 
            $('#SuperBuildUpArea_'+layoutCount).val('');
            $('#BuildUpArea_'+layoutCount).val('');  
            $('#carpetArea_'+layoutCount).val('');   
            $('#S_Build_Area_Hidden_'+layoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Row House") 
        {
            $('#villa_Area_details_'+layoutCount+'').css('display','block');
            $('#SuperBuildUpArea_'+layoutCount).val('');
            $('#BuildUpArea_'+layoutCount).val('');  
            $('#carpetArea_'+layoutCount).val('');
            $('#S_Build_Area_Hidden_'+layoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Farm House") 
        {
            $('#villa_Area_details_'+layoutCount+'').css('display','block'); 
            $('#SuperBuildUpArea_'+layoutCount).val('');
            $('#BuildUpArea_'+layoutCount).val('');  
            $('#carpetArea_'+layoutCount).val('');
            $('#S_Build_Area_Hidden_'+layoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCount+'').css('display','none');   
        }

        

        $('#propertyFurnishedID'+layoutCount).select2();
        $('#propBHK'+layoutCount).select2();
        $('#layoutPropBHK'+layoutCount).select2();

        $('#kitchen'+layoutCount).select2();
        $('#facingID'+layoutCount).select2();
        $('#layoutType'+layoutCount).select2();
        $('#typeprop'+layoutCount).select2();
        $('#propType'+layoutCount).select2();
        $('#propList'+layoutCount).select2();
        $('#propListtwo'+layoutCount).select2();
        $('#propTypeone'+layoutCount).select2();
        $('#furnished'+layoutCount).select2();
        $('#pFloor'+layoutCount).select2();
      
        $('#Bedrooms'+layoutCount).select2();
        $('#Bathrooms'+layoutCount).select2();
        $('#Balconies'+layoutCount).select2();
        $('#furnished1'+layoutCount).select2();
        $('#pReportingTime'+layoutCount).select2();
        $('#UnitID_'+layoutCount).select2();
        $('#bArea_'+layoutCount).select2();
        $('#cArea_'+layoutCount).select2();


        if (id == "House" ) 
        {
            $('#pLayout_Hidden').css('display','none');
        }

        getLayoutUnitsDetails(k);
        getLayoutBHKDetails(k);
        getLayoutFacing(k);
        

        layoutCount = layoutCount + 1;
        
    }

    function addCommercial(id,count= null) 
    {
        $('#layoutCount').val(layoutCountHidden);
        // console.log("lauoutcount :"+layoutCountHidden);
        var totalData = <?php echo json_encode($this->data['ListingDetails']); ?>;

        var showable = "collapsed";
        var showbody = "";
         var listingFor = '<?php echo $this->data['bIsProperty'] ?>';
        if (count == 0) 
        {
            showable = "";
            showbody = "show";
        }
        if (listingFor == 1) 
        {
            removeDiv = '<span class="m-accordion__item-icon" onclick="removeDivTagAjax('+layoutCount+','+totalData[count]["nProjectIDPK"]+')" >\
                                <i class="fa fa-times" style="float: right;font-size: 12px;"></i>\
                            </span>';
        }

        if (count!= null) 
        {
            $('#layoutDetails_'+id).append('\
                                    <div id="'+CommercialLayoutCount+'">\
                                        <div class="m-accordion m-accordion" id="m_accordion_4_'+CommercialLayoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head  '+showable+'" role="tab" id="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+CommercialLayoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                '+removeDiv+'\
                                            </div>\
                                            <div class="m-accordion__item-body collapse '+showbody+'" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+CommercialLayoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-parent="#m_accordion_4_'+CommercialLayoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                 <input id="LayoutID'+CommercialLayoutCount+'" name="layout[LayoutID][]" type="hidden" value="'+totalData[count]['nProjectIDPK']+'">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+CommercialLayoutCount+'" onchange="getAreaDetails('+CommercialLayoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+CommercialLayoutCount+'" value="'+totalData[count]["fProjectSuperBuiltArea"]+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" value="'+totalData[count]["fProjectBuiltArea"]+'" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" value="'+totalData[count]["fProjectCarpetArea"]+'" id="carpetArea_'+CommercialLayoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+CommercialLayoutCount+'" value="'+totalData[count]["fProjectSuperBuiltArea"]+'"  class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+CommercialLayoutCount+'"   value="'+totalData[count]["fProjectBuiltArea"]+'" maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+CommercialLayoutCount+'" value="'+totalData[count]["fProjectCarpetArea"]+'"  name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+CommercialLayoutCount+'" maxlength="6"class="form-control m-input"  value="'+totalData[count]["fConstructionSuperBuiltArea"]+'"  name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+CommercialLayoutCount+'""  maxlength="6"   value="'+totalData[count]["fConstructionBuiltArea"]+'" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+CommercialLayoutCount+'""   value="'+totalData[count]["fConstructionCarpetArea"]+'" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-8">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Washroom</label>\
                                                                    <select style="width: 100%;" name="layout[washroom][]" class="form-control m-select2" id="washroom_Office_'+CommercialLayoutCount+'">\
                                                                        <option value="0">0 </option>\
                                                                        <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="3+">3+ </option>\
                                                                    </select>\
                                                                </div>\
                                                                 <div class="col-lg-6">\
                                                                    <label>Balcony</label>\
                                                                    <select style="width: 100%;" name="layout[balcony][]" class="form-control m-select2" id="balcony_Office_'+CommercialLayoutCount+'">\
                                                                        <option value="0">0 </option>\
                                                                        <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="4">4 </option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                            <br>\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                   <label>Property Furnished</label>\
                                                                    <br>\
                                                                    <select id="furnished_office_'+CommercialLayoutCount+'" name="layout[Furnished_office][]" class="form-control m-select2" style="width: 100%;">\
                                                                       <option value="1">Furnished</option>\
                                                                       <option value="0">Un-Furnished </option> \
                                                                       <option value="2">Semi-Furnished </option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Pantry</label>\
                                                                    <select style="width: 100%;" name="layout[pentry][]" class="form-control m-select2" id="pentry_Office_'+CommercialLayoutCount+'">\
                                                                        <option value="0">0 </option>\
                                                                         <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="4">4 </option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Available Parking</label>&nbsp;&nbsp;&nbsp;\
                                                                    <div class="input-group">\
                                                                        <div class="input-group-append" style="cursor: pointer;" onclick="removeOpenParking('+CommercialLayoutCount+')">\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-minus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                       <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" id="Cparking'+CommercialLayoutCount+'" name="layout['+"Cparking"+'][]"  value="0"/>\
                                                                       <div class="input-group-append" style="cursor: pointer;" onclick="addOpenParking('+CommercialLayoutCount+')"\>\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-plus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+CommercialLayoutCount+'\','+CommercialLayoutCount+')" id="RemoveImage_'+CommercialLayoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+CommercialLayoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" accept="image/x-png,image/jpeg,image/jpg"id="layout_image'+CommercialLayoutCount+'" onchange="layout_preview(this,'+CommercialLayoutCount+')" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();" id="layout_preview_display_'+CommercialLayoutCount+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div> \
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+CommercialLayoutCount+'" maxlength="11" oninput="GetProcePerUnit('+CommercialLayoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+CommercialLayoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]"maxlength="9"  oninput="convertNumbertoWords(\'Booking'+CommercialLayoutCount+'\',\'bnumToWords'+CommercialLayoutCount+'\');" id="Booking'+CommercialLayoutCount+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <<div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+CommercialLayoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+CommercialLayoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        </div>\
                                    </div>\
                                ');
            SetWashroomValue(CommercialLayoutCount);

           

            $('#Expected_'+CommercialLayoutCount).val(parseInt(totalData[count]['fProjectExpectedPrice']));
            $('#PricePerUnit_'+CommercialLayoutCount).val(parseInt(totalData[count]['fPricePerUnitArea']));
            $('#Booking'+CommercialLayoutCount).val(parseInt(totalData[count]['fProjectBookingPrice']));

            if (totalData[count]['tLayoutUmages'] != "") 
            {   
                $('#RemoveImage_'+CommercialLayoutCount).css('display','block');
                $('#EditImage_'+CommercialLayoutCount).css('display','block');
                $('#layout_preview_display_'+CommercialLayoutCount).html('');
                $('#layout_preview_display_'+CommercialLayoutCount).append('<br><div class="col-lg-8">\
                               <img class="vpb_image_style" class="img-thumbnail" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING; ?>'+totalData[count]['tLayoutUmages']+'" /><br /></div>');
            }
             $('#washroom_Office_'+CommercialLayoutCount).select2().val(totalData[count]['nWashroom']).trigger("change");
            $('#balcony_Office_'+CommercialLayoutCount).select2().val(totalData[count]['nBalconies']).trigger("change");
            $('#furnished_office_'+CommercialLayoutCount).select2().val(totalData[count]['bIsPropertyFurnished']).trigger("change");
            $('#pentry_Office_'+CommercialLayoutCount).select2().val(totalData[count]['nPentry']).trigger("change");
            $('#Cparking'+CommercialLayoutCount).val(totalData[count]['nCarParkings']);
            if (totalData[count]['bIsExpectedPriceNegotiable'] == 1) 
            {
                $('#ExpectedNegociable'+CommercialLayoutCount).attr("checked", "checked");
            }

            if (totalData[count]['bIsPriceShow'] == 1) 
            {
                $('#priceDisplayed_'+CommercialLayoutCount).attr("checked", "checked");
            }


            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+CommercialLayoutCount);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+CommercialLayoutCount);
            layoutImages.push('layout_image'+CommercialLayoutCount);
           

            convertNumbertoWords('Expected_'+CommercialLayoutCount,'numToWords'+CommercialLayoutCount);
            convertNumbertoWords('PricePerUnit_'+CommercialLayoutCount,'pnumToWords'+CommercialLayoutCount);
            convertNumbertoWords('Booking'+CommercialLayoutCount,'bnumToWords'+CommercialLayoutCount);
            layoutCountHidden = layoutCountHidden + 1;
        }
        else
        {
            layoutCountHidden = layoutCountHidden + 1;
            $('#layoutDetails_'+id).append('\
                                    <div id="'+layoutCountHidden+'">\
                                        <div class="m-accordion m-accordion" id="m_accordion_4_'+layoutCountHidden+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+layoutCountHidden+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+layoutCountHidden+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                 <span class="m-accordion__item-icon" onclick="removeDivTag('+layoutCountHidden+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 17px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+layoutCountHidden+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+layoutCountHidden+'" data-parent="#m_accordion_4_'+layoutCountHidden+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+layoutCountHidden+'" onchange="getAreaDetails('+layoutCountHidden+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+layoutCountHidden+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="carpetArea_'+layoutCountHidden+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+layoutCountHidden+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+layoutCountHidden+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+layoutCountHidden+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+layoutCountHidden+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+layoutCountHidden+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+layoutCountHidden+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-8">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Washroom</label>\
                                                                    <select style="width: 100%;" name="layout[washroom][]" class="form-control m-select2" id="washroom_Office_'+layoutCountHidden+'">\
                                                                        <option value="0">0 </option>\
                                                                        <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="3+">3+ </option>\
                                                                    </select>\
                                                                </div>\
                                                                 <div class="col-lg-6">\
                                                                    <label>Balcony</label>\
                                                                    <select style="width: 100%;" name="layout[balcony][]" class="form-control m-select2" id="balcony_Office_'+layoutCountHidden+'">\
                                                                        <option value="0">0 </option>\
                                                                        <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="4">4 </option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                   <label>Property Furnished</label>\
                                                                    <br>\
                                                                    <select id="furnished_office_'+layoutCountHidden+'" name="layout[Furnished_office][]" class="form-control m-select2" style="width: 100%;">\
                                                                       <option value="1">Furnished</option>\
                                                                       <option value="0">Un-Furnished </option> \
                                                                       <option value="2">Semi-Furnished </option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-6">\
                                                                    <label>Pantry</label>\
                                                                    <select style="width: 100%;" name="layout[pentry][]" class="form-control m-select2" id="pentry_Office_'+layoutCountHidden+'">\
                                                                        <option value="0">0 </option>\
                                                                         <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="4">4 </option>\
                                                                    </select>\
                                                                </div>\
                                                                <br>\
                                                                <div class="col-lg-1">\
                                                                </div>\
                                                                <div class="col-lg-6">\
                                                                    <br>\
                                                                    <div class="row">\
                                                                        <label>Available <br> Parking</label>&nbsp;&nbsp;&nbsp;\
                                                                        <div class="col-md-1" style="cursor: pointer;" onclick="removeOpenParking('+layoutCountHidden+')">\
                                                                            <i style="margin-top: 12px;" class="fa fa-minus" ></i>\
                                                                        </div>\
                                                                        <div class="col-md-4">\
                                                                            <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" id="Cparking'+layoutCountHidden+'" name="layout['+"Cparking"+'][]"  value="0"/>\
                                                                        </div>\
                                                                        <div class="col-md-1" style="cursor: pointer;" onclick="addOpenParking('+layoutCountHidden+')">\
                                                                           <i style="margin-top: 12px;" class="fa fa-plus"></i>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                         <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+layoutCountHidden+'\','+layoutCountHidden+')" id="RemoveImage_'+layoutCountHidden+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+layoutCountHidden+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" accept="image/x-png,image/jpeg,image/jpg"id="layout_image'+layoutCountHidden+'" onchange="layout_preview(this,'+layoutCountHidden+')" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();" id="layout_preview_display_'+layoutCountHidden+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div> \
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+layoutCountHidden+'" maxlength="11" oninput="GetProcePerUnit('+layoutCountHidden+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+layoutCountHidden+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]"maxlength="9"  oninput="convertNumbertoWords(\'Booking'+layoutCountHidden+'\',\'bnumToWords'+layoutCountHidden+'\');" id="Booking'+layoutCountHidden+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+layoutCountHidden+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+layoutCountHidden+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+layoutCountHidden+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+layoutCountHidden+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        </div>\
                                    </div>\
                                ');
            // CommercialCount = layoutCountHidden;
            $('#UnitID_'+layoutCountHidden).select2();
            $('#bArea_'+layoutCountHidden).select2();
            $('#cArea_'+layoutCountHidden).select2();
            $('#washroom_Office_'+layoutCountHidden).select2();
            $('#balcony_Office_'+layoutCountHidden).select2();
            $('#furnished_office_'+layoutCountHidden).select2();
            $('#pentry_Office_'+layoutCountHidden).select2();

            $('#UnitID_'+layoutCountHidden).select2().val("0").trigger("change");
            $('#S_Build_Area_Hidden_'+layoutCountHidden).css('display','block');
            $('#Build_Area_Hidden_'+layoutCountHidden).css('display','block');

            
            SetWashroomValue(layoutCountHidden);
            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+layoutCountHidden);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+layoutCountHidden);
            layoutImages.push('layout_image'+layoutCountHidden);
            if (totalData[count]['bIsExpectedPriceNegotiable'] == 1) 
            {
                $('#ExpectedNegociable'+layoutCountHidden).attr("checked", "checked");
            }

            if (totalData[count]['bIsPriceShow'] == 1) 
            {
                $('#priceDisplayed_'+layoutCountHidden).attr("checked", "checked");
            }
            

            var subtype = $('#propertiesSubType').val();
            var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';

            if (subTypeName == "Corporate House") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
            else if (subTypeName == "Industrial Factory") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
            else if (subTypeName == "Warehouse") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }

        }


        CommercialCount = layoutCountHidden;

        $('#UnitID_'+CommercialLayoutCount).select2();
        $('#bArea_'+CommercialLayoutCount).select2();
        $('#cArea_'+CommercialLayoutCount).select2();
        $('#washroom_Office_'+CommercialLayoutCount).select2();
        $('#balcony_Office_'+CommercialLayoutCount).select2();
        $('#furnished_office_'+CommercialLayoutCount).select2();
        $('#pentry_Office_'+CommercialLayoutCount).select2();

        $('#UnitID_'+CommercialLayoutCount).select2().val("0").trigger("change");

        


        getLayoutUnitsDetails(CommercialCount);

        $('#S_Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');
        $('#Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');

        CommercialLayoutCount = CommercialLayoutCount + 1;


        var subtype = $('#propertiesSubType').val();
        var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';

        if (subTypeName == "Corporate House") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');

            $('#SuperBuildUpArea_'+CommercialLayoutCount).val('');
            $('#BuildUpArea_'+CommercialLayoutCount).val('');  
            $('#carpetArea_'+CommercialLayoutCount).val('');  

            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Industrial Factory") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');
            $('#SuperBuildUpArea_'+CommercialLayoutCount).val('');
            $('#BuildUpArea_'+CommercialLayoutCount).val('');  
            $('#carpetArea_'+CommercialLayoutCount).val('');  
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Warehouse") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');
            $('#SuperBuildUpArea_'+CommercialLayoutCount).val('');
            $('#BuildUpArea_'+CommercialLayoutCount).val('');  
            $('#carpetArea_'+CommercialLayoutCount).val('');  
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }

        CommercialLayoutCount = CommercialLayoutCount + 1;
    }

    
    function addOtherslayout(id,count= null) 
    {   
        $('#layoutCount').val(layoutCountHidden);
        var totalData = <?php echo json_encode($this->data['ListingDetails']); ?>;
         var listingFor = '<?php echo $this->data['bIsProperty'] ?>';
        // console.log("data : "+ totalData[count]['nProjectIDPK']);
        var showable = "collapsed";
        var showbody = "";
        var removeDiv = ""
        if (count == 0) 
        {
            showable = "";
            showbody = "show";
        }
        if (listingFor == 1) 
        {
            removeDiv = '<span class="m-accordion__item-icon" onclick="removeDivTagAjax('+layoutCount+','+totalData[count]["nProjectIDPK"]+')" >\
                                <i class="fa fa-times" style="float: right;font-size: 12px;"></i>\
                            </span>';
        }
        if (count!= null) 
        {
            $('#layoutDetails_'+id).append('\<div id="layoutDetails_'+CommercialLayoutCount+'">\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+CommercialLayoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head '+showable+'" role="tab" id="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+CommercialLayoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                '+removeDiv+'\
                                            </div>\
                                            <div class="m-accordion__item-body collapse '+showbody+'" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+CommercialLayoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-parent="#m_accordion_4_'+CommercialLayoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                 <input id="LayoutID" name="layout[LayoutID][]" type="hidden">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+CommercialLayoutCount+'" onchange="getAreaDetails('+CommercialLayoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+CommercialLayoutCount+'"  value="'+parseInt(totalData[count]["fProjectSuperBuiltArea"])+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input"  value="'+parseInt(totalData[count]["fProjectBuiltArea"])+'" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6"  value="'+parseInt(totalData[count]["fProjectCarpetArea"])+'" class="form-control m-input" id="carpetArea_'+CommercialLayoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="col-lg-4">\
                                                        <div class="form-group row">\
                                                            <div class="col-lg-12">\
                                                               <label>Layout Images</label>\
                                                                <span class="">\
                                                                    <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                        <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                            <a onclick="RemoveImage(\'layout_image'+CommercialLayoutCount+'\','+CommercialLayoutCount+')" id="RemoveImage_'+CommercialLayoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                            </a>\
                                                                            <a id="EditImage_'+CommercialLayoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();" accept="image/x-png,image/jpeg,image/jpg">\
                                                                                <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                            </a>\
                                                                       </div>\
                                                                        <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" id="layout_image'+CommercialLayoutCount+'" onchange="layout_preview(this,'+CommercialLayoutCount+')" accept="image/x-png,image/jpeg,image/jpg" />\
                                                                        <div align="center"  onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();" id="layout_preview_display_'+CommercialLayoutCount+'">\
                                                                            <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                        </div>\
                                                                    </div>\
                                                                </span>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-lg-3">\
                                                            <label>Select BHK</label>\
                                                            <select style="width: 100%;" name="layout['+"layoutPropBHK"+'][]" class="form-control m-select2" id="layoutPropBHK'+CommercialLayoutCount+'">\
                                                                <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="3+">3+ </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-3">\
                                                            <label>Select Facing</label>\
                                                            <select style="width: 100%;" name="layout['+"facingID"+'][]" class="form-control m-select2" id="facingID'+CommercialLayoutCount+'">\
                                                                 <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="4">4 </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-3">\
                                                            <div class="col-lg-12">\
                                                               <label>Total Floors</label>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Total Floors" class="form-control m-input" style="width: 100%;" class="form-control m-select2" name="layout['+"tFloor"+'][]"  max="100" id="tFloor'+CommercialLayoutCount+'" oninput="getPFlor('+CommercialLayoutCount+');">\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-3" id="Layout_Hidden">\
                                                            <label>Property On Floor</label>\
                                                            <select style="width: 100%;" class="form-control m-select2" name="layout['+"pFloor"+'][]" id="pFloor'+CommercialLayoutCount+'" style="display: block;">\
                                                                <option value="0">Ground Floor</option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <p style="width: 100%;">Store Room</p>\
                                                                    <input type="hidden" name="layout['+"storeRoomAvailable"+'][0]" id="storeRoomAvailable'+CommercialLayoutCount+'" value="">\
                                                                    <input  name="layout['+"storeRoomAvailable"+'][]" id="StoreRoom'+CommercialLayoutCount+'" class="StoreRoom" onchange=AllcheckBoxArray("StoreRoom","storeRoomAvailable'+CommercialLayoutCount+'")  type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <input type="hidden" name="layout['+"poojaRoomAvailable"+'][0]" id="poojaRoomAvailable'+CommercialLayoutCount+'" value="">\
                                                                    <p style="width: 100%;">Pooja Room</p>\
                                                                    <input  name="layout['+"poojaRoomAvailable"+'][]" id="PoojaRoom'+CommercialLayoutCount+'" class="PoojaRoom" onchange=AllcheckBoxArray("PoojaRoom","poojaRoomAvailable'+CommercialLayoutCount+'") type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+CommercialLayoutCount+'" maxlength="11" oninput="GetProcePerUnit('+CommercialLayoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+CommercialLayoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]" maxlength="9" oninput="convertNumbertoWords(\'Booking'+CommercialLayoutCount+'\',\'bnumToWords'+CommercialLayoutCount+'\');" id="Booking'+CommercialLayoutCount+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+CommercialLayoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+CommercialLayoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\</div>\
                                ');
            $('#LayoutID').val(totalData[count]['nProjectIDPK']);

            $('#UnitID_'+CommercialLayoutCount).select2();
            $('#layoutPropBHK'+CommercialLayoutCount).select2();
            $('#facingID'+CommercialLayoutCount).select2();
            $('#pFloor'+CommercialLayoutCount).select2();
            // $('#pentryry_Office'+CommercialLayoutCount).select2();
            
            
            
            CommercialCount = CommercialLayoutCount;
            getLayoutBHKDetails(CommercialLayoutCount);
            getLayoutUnitsDetails(CommercialCount);
            layoutImages.push('layout_image'+CommercialCount);
            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+CommercialCount);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+CommercialCount);
            AllcheckBoxArray('StoreRoom','storeRoomAvailable'+CommercialCount);
            AllcheckBoxArray('PoojaRoom','poojaRoomAvailable'+CommercialCount);
            // var totalData = <?php echo json_encode($this->data['ListingDetails']); ?>;
            $('#tFloor'+CommercialCount).val(totalData[count]['nTotalFloors']);

            for (var i = 0; i < totalData[count]['nTotalFloors'] ; i++) 
            {   
                if (i == totalData[count]['nPropertyOnFloor']) 
                {
                    $('#pFloor'+CommercialCount).append('<option value='+i+'  selected>'+i+'</option>');
                }
                else
                {
                    $('#pFloor'+CommercialCount).append('<option value='+i+' >'+i+'</option>');
                }
                
            }
            if (totalData[count]['bIsPoojaRoomAvailable'] == 1) 
            {
                $('#PoojaRoom'+CommercialCount).attr("checked", "checked");
            }

            if (totalData[count]['bIsStoreRoomAvailable'] == 1) 
            {
                $('#StoreRoom'+CommercialCount).attr("checked", "checked");
            }
            if (totalData[count]['bIsExpectedPriceNegotiable'] == 1) 
            {
                $('#ExpectedNegociable'+CommercialCount).attr("checked", "checked");
            }

            if (totalData[count]['bIsPriceShow'] == 1) 
            {
                $('#priceDisplayed_'+CommercialCount).attr("checked", "checked");
            }
            getLayoutFacing(CommercialCount);

            
            $('#Expected_'+CommercialLayoutCount).val(parseInt(totalData[count]['fProjectExpectedPrice']));
            $('#PricePerUnit_'+CommercialLayoutCount).val(parseInt(totalData[count]['fPricePerUnitArea']));
            $('#Booking'+CommercialLayoutCount).val(parseInt(totalData[count]['fProjectBookingPrice']));

            convertNumbertoWords('Expected_'+CommercialLayoutCount,'numToWords'+CommercialLayoutCount);
            convertNumbertoWords('PricePerUnit_'+CommercialLayoutCount,'pnumToWords'+CommercialLayoutCount);
            convertNumbertoWords('Booking'+CommercialLayoutCount,'bnumToWords'+CommercialLayoutCount);

            $('#S_Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');
            $('#Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');

            layoutCountHidden = layoutCountHidden + 1;
            if (totalData[count]['tLayoutUmages'] != "") 
            {   
                $('#RemoveImage_'+CommercialLayoutCount).css('display','block');
                $('#EditImage_'+CommercialLayoutCount).css('display','block');
                $('#layout_preview_display_'+CommercialLayoutCount).html('');
                $('#layout_preview_display_'+CommercialLayoutCount).append('<br><div class="col-lg-8">\
                               <img class="vpb_image_style" class="img-thumbnail" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING; ?>'+totalData[count]['tLayoutUmages']+'" /><br /></div>');
            }
        }
        else
        {
            layoutCountHidden = layoutCountHidden + 1;

            $('#layoutDetails_'+id).append('\<div id="layoutDetails_'+layoutCountHidden+'">\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+layoutCountHidden+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head '+showable+'" role="tab" id="m_accordion_3_item_7_head'+layoutCountHidden+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+layoutCountHidden+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                <span class="m-accordion__item-icon" onclick="removeDivTagAjax('+layoutCountHidden+','+totalData[count]["nProjectIDPK"]+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 17px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse '+showbody+'" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+layoutCountHidden+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+layoutCountHidden+'" data-parent="#m_accordion_4_'+layoutCountHidden+'">\
                                                <div class="m-accordion__item-content">\
                                                 <input id="LayoutID" name="layout[LayoutID][]" type="hidden">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+layoutCountHidden+'" onchange="getAreaDetails('+layoutCountHidden+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+layoutCountHidden+'"  value="'+parseInt(totalData[count]["fProjectSuperBuiltArea"])+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input"  value="'+parseInt(totalData[count]["fProjectBuiltArea"])+'" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6"  value="'+parseInt(totalData[count]["fProjectCarpetArea"])+'" class="form-control m-input" id="carpetArea_'+layoutCountHidden+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="col-lg-4">\
                                                        <div class="form-group row">\
                                                            <div class="col-lg-12">\
                                                               <label>Layout Images</label>\
                                                                <span class="">\
                                                                    <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                        <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                            <a onclick="RemoveImage(\'layout_image'+layoutCountHidden+'\','+layoutCountHidden+')" id="RemoveImage_'+layoutCountHidden+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                            </a>\
                                                                            <a id="EditImage_'+layoutCountHidden+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();" accept="image/x-png,image/jpeg,image/jpg">\
                                                                                <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                            </a>\
                                                                       </div>\
                                                                        <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" id="layout_image'+layoutCountHidden+'" onchange="layout_preview(this,'+layoutCountHidden+')" accept="image/x-png,image/jpeg,image/jpg" />\
                                                                        <div align="center"  onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();" id="layout_preview_display_'+layoutCountHidden+'">\
                                                                            <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                        </div>\
                                                                    </div>\
                                                                </span>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-lg-3">\
                                                            <label>Select BHK</label>\
                                                            <select style="width: 100%;" name="layout['+"layoutPropBHK"+'][]" class="form-control m-select2" id="layoutPropBHK'+layoutCountHidden+'">\
                                                                <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="3+">3+ </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-3">\
                                                            <label>Select Facing</label>\
                                                            <select style="width: 100%;" name="layout['+"facingID"+'][]" class="form-control m-select2" id="facingID'+layoutCountHidden+'">\
                                                                 <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="4">4 </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-3">\
                                                            <div class="col-lg-12">\
                                                               <label>Total Floors</label>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Total Floors" class="form-control m-input" style="width: 100%;" class="form-control m-select2" name="layout['+"tFloor"+'][]"  max="100" id="tFloor'+layoutCountHidden+'" oninput="getPFlor('+layoutCountHidden+');">\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-3" id="Layout_Hidden">\
                                                            <label>Property On Floor</label>\
                                                            <select style="width: 100%;" class="form-control m-select2" name="layout['+"pFloor"+'][]" id="pFloor'+layoutCountHidden+'" style="display: block;">\
                                                                <option value="0">Ground Floor</option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <br>\
                                                            <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                            <p style="width: 100%;">Store Room</p>\
                                                            <input type="hidden" name="layout['+"storeRoomAvailable"+'][0]" id="storeRoomAvailable'+layoutCountHidden+'" value="">\
                                                            <input  name="layout['+"storeRoomAvailable"+'][]" id="StoreRoom'+layoutCountHidden+'" class="StoreRoom" onchange=AllcheckBoxArray("StoreRoom","storeRoomAvailable'+layoutCountHidden+'")  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <br>\
                                                            <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                            <input type="hidden" name="layout['+"poojaRoomAvailable"+'][0]" id="poojaRoomAvailable'+layoutCountHidden+'" value="">\
                                                            <p style="width: 100%;">Pooja Room</p>\
                                                            <input  name="layout['+"poojaRoomAvailable"+'][]" id="PoojaRoom'+layoutCountHidden+'" class="PoojaRoom" onchange=AllcheckBoxArray("PoojaRoom","poojaRoomAvailable'+layoutCountHidden+'") type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+layoutCountHidden+'" maxlength="11" oninput="GetProcePerUnit('+layoutCountHidden+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+layoutCountHidden+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]" maxlength="9" oninput="convertNumbertoWords(\'Booking'+layoutCountHidden+'\',\'bnumToWords'+layoutCountHidden+'\');" id="Booking'+layoutCountHidden+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+layoutCountHidden+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+layoutCountHidden+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+layoutCountHidden+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+layoutCountHidden+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\</div>\
                                ');
            layoutCountHidden = layoutCountHidden + 1; 
        }
        

        CommercialCount = CommercialLayoutCount;
        $('#UnitID_'+CommercialLayoutCount).select2();
        $('#bArea_'+CommercialLayoutCount).select2();
        $('#cArea_'+CommercialLayoutCount).select2();
        $('#washroom_Office_'+CommercialLayoutCount).select2();
        $('#balcony_Office_'+CommercialLayoutCount).select2();
        $('#furnished_office'+CommercialLayoutCount).select2();
        $('#pentry_Office'+CommercialLayoutCount).select2();
        layoutImages.push('layout_image'+CommercialLayoutCount);
        

         getLayoutUnitsDetails(CommercialCount);
        $('#S_Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');
        $('#Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');

        CommercialLayoutCount = CommercialLayoutCount + 1;

    }

    function addLandlayout(id,count= null) 
    {
        $('#layoutCount').val(layoutCountHidden);
        //console.log("layout count"+$('#layoutCount').val());
         var listingFor = '<?php echo $this->data['bIsProperty'] ?>';   
        var totalData = <?php echo json_encode($this->data['ListingDetails']); ?>;
        // console.log("data : "+ totalData[count]['nProjectIDPK']);
        var removeDiv = ""
        var showable = "collapsed";
        var showbody = "";
        if (count == 0) 
        {
            showable = "";
            showbody = "show";
        }
        if (listingFor == 1) 
        {
            removeDiv = '<span class="m-accordion__item-icon" onclick="removeDivTagAjax('+layoutCount+','+totalData[count]["nProjectIDPK"]+')" >\
                                <i class="fa fa-times" style="float: right;font-size: 12px;"></i>\
                            </span>';
        }
        if (count!= null) 
        {
            $('#layoutDetails_'+id).append('\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+CommercialLayoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head  '+showable+' " role="tab" id="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+CommercialLayoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                 '+removeDiv+'\
                                            </div>\
                                            <div class="m-accordion__item-body collapse '+showbody+' " style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+CommercialLayoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-parent="#m_accordion_4_'+CommercialLayoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                 <input id="LayoutID'+layoutCount+'" name="layout[LayoutID][]" type="hidden" value="'+totalData[count]['nProjectIDPK']+'">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+CommercialLayoutCount+'" onchange="getAreaDetails('+CommercialLayoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+CommercialLayoutCount+'"  value="'+totalData[count]["fProjectSuperBuiltArea"]+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input"  value="'+totalData[count]["fProjectBuiltArea"]+'" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6"  value="'+totalData[count]["fProjectCarpetArea"]+'" class="form-control m-input" id="carpetArea_'+CommercialLayoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"    value="'+totalData[count]["fProjectSuperBuiltArea"]+'"  maxlength="6" id="ploatSuperBuildUpArea_'+CommercialLayoutCount+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   value="'+totalData[count]["fProjectBuiltArea"]+'" class="form-control m-input" id="ploatBuildUpArea_'+CommercialLayoutCount+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"   value="'+totalData[count]["fProjectCarpetArea"]+'"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+CommercialLayoutCount+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+CommercialLayoutCount+'" maxlength="6"class="form-control m-input"  value="'+totalData[count]["fConstructionSuperBuiltArea"]+'"   name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"  value="'+totalData[count]["fConstructionBuiltArea"]+'"  class="form-control m-input" id="constructionBuildUpArea_'+CommercialLayoutCount+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6"  value="'+totalData[count]["fConstructionCarpetArea"]+'"  class="form-control m-input" id="constructioncarpetArea_'+CommercialLayoutCount+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                     <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+CommercialLayoutCount+'\','+CommercialLayoutCount+')" id="RemoveImage_'+CommercialLayoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+CommercialLayoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" accept="image/x-png,image/jpeg,image/jpg"id="layout_image'+CommercialLayoutCount+'" onchange="layout_preview(this,'+CommercialLayoutCount+')" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();" id="layout_preview_display_'+CommercialLayoutCount+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+CommercialLayoutCount+'" maxlength="11" oninput="GetProcePerUnit('+CommercialLayoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+CommercialLayoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]" maxlength="9" oninput="convertNumbertoWords(\'Booking'+CommercialLayoutCount+'\',\'bnumToWords'+CommercialLayoutCount+'\');" id="Booking'+CommercialLayoutCount+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+CommercialLayoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+CommercialLayoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                ');
            $('#Expected_'+CommercialLayoutCount).val(parseInt(totalData[count]['fProjectExpectedPrice']));
            $('#PricePerUnit_'+CommercialLayoutCount).val(parseInt(totalData[count]['fPricePerUnitArea']));
            $('#Booking'+CommercialLayoutCount).val(parseInt(totalData[count]['fProjectBookingPrice']));
            if (totalData[count]['tLayoutUmages'] != "") 
            {   
                $('#RemoveImage_'+CommercialLayoutCount).css('display','block');
                $('#EditImage_'+CommercialLayoutCount).css('display','block');
                $('#layout_preview_display_'+CommercialLayoutCount).html('');
                $('#layout_preview_display_'+CommercialLayoutCount).append('<br><div class="col-lg-8">\
                               <img class="vpb_image_style" class="img-thumbnail" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING; ?>'+totalData[count]['tLayoutUmages']+'" /><br /></div>');
            }

            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+CommercialLayoutCount);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+CommercialLayoutCount);
            layoutImages.push('layout_image'+CommercialLayoutCount);
            // console.log($('#m_accordion_4_'+CommercialLayoutCount).html());
            

            if (totalData[count]['bIsExpectedPriceNegotiable'] == 1) 
            {
                $('#ExpectedNegociable'+layoutCount).attr("checked", "checked");
            }

            if (totalData[count]['bIsPriceShow'] == 1) 
            {
                $('#priceDisplayed_'+layoutCount).attr("checked", "checked");
            }

            convertNumbertoWords('Expected_'+CommercialLayoutCount,'numToWords'+CommercialLayoutCount);
            convertNumbertoWords('PricePerUnit_'+CommercialLayoutCount,'pnumToWords'+CommercialLayoutCount);
            convertNumbertoWords('Booking'+CommercialLayoutCount,'bnumToWords'+CommercialLayoutCount);
            layoutCountHidden = layoutCountHidden + 1;
        }
        else
        {   
            layoutCountHidden = layoutCountHidden + 1;

            $('#layoutDetails_'+id).append('\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+layoutCountHidden+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+layoutCountHidden+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+layoutCountHidden+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                 <span class="m-accordion__item-icon" onclick="removeDivTag('+layoutCountHidden+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 17px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+layoutCountHidden+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+layoutCountHidden+'" data-parent="#m_accordion_4_'+layoutCountHidden+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+layoutCountHidden+'" onchange="getAreaDetails('+layoutCountHidden+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+layoutCountHidden+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="carpetArea_'+layoutCountHidden+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+layoutCountHidden+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+layoutCountHidden+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+layoutCountHidden+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+layoutCountHidden+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+layoutCountHidden+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+layoutCountHidden+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+layoutCountHidden+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                     <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+layoutCountHidden+'\','+layoutCountHidden+')" id="RemoveImage_'+layoutCountHidden+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+layoutCountHidden+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" accept="image/x-png,image/jpeg,image/jpg"id="layout_image'+layoutCountHidden+'" onchange="layout_preview(this,'+layoutCountHidden+')" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+layoutCountHidden+'\').click();" id="layout_preview_display_'+layoutCountHidden+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" class="form-control m-input" name="layout[Expected][]" id="Expected_'+layoutCountHidden+'" maxlength="11" oninput="GetProcePerUnit('+layoutCountHidden+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+layoutCountHidden+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" class="form-control m-input" name="layout[BookingPrize][]" maxlength="9" oninput="convertNumbertoWords(\'Booking'+layoutCountHidden+'\',\'bnumToWords'+layoutCountHidden+'\');" id="Booking'+layoutCountHidden+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+layoutCountHidden+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+layoutCountHidden+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+layoutCountHidden+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+layoutCountHidden+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+layoutCountHidden+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+layoutCountHidden+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                ');
            $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','block'); 

            var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';
            layoutImages.push('layout_image'+layoutCountHidden);
            AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+layoutCountHidden);
            AllcheckBoxArray('priceDisplayed','priceDisplay_'+layoutCountHidden);
            
            if (subTypeName == "Corporate House") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
            else if (subTypeName == "Industrial Factory") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
            else if (subTypeName == "Warehouse") 
            {
                $('#villa_Area_details_'+layoutCountHidden+'').css('display','block');   
                $('#S_Build_Area_Hidden_'+layoutCountHidden+'').css('display','block');   
                $('#Build_Area_Hidden_'+layoutCountHidden+'').css('display','none');   
            }
             $('#UnitID_'+layoutCountHidden).select2();
              getLayoutUnitsDetails(layoutCountHidden);

        }
        

        CommercialCount = CommercialLayoutCount;

       
        $('#UnitID_'+CommercialLayoutCount).select2();
        // $('#bArea_'+CommercialLayoutCount).select2();
        // $('#cArea_'+CommercialLayoutCount).select2();
        
        

        getLayoutUnitsDetails(CommercialCount);

        var subtype = $('#propertiesSubType').val();
        var subTypeName = '<?php echo $this->data['tPropertySubTypeName']; ?>';
        //console.log(subTypeName);

        $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
        $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block'); 

         if (subTypeName == "Corporate House") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');
            $('#SuperBuildUpArea_'+CommercialLayoutCount).val('');
            $('#BuildUpArea_'+CommercialLayoutCount).val('');  
            $('#carpetArea_'+CommercialLayoutCount).val('');  
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Industrial Factory") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');
            $('#SuperBuildUpArea_'+CommercialLayoutCount).val('');
            $('#BuildUpArea_'+CommercialLayoutCount).val('');  
            $('#carpetArea_'+CommercialLayoutCount).val('');  
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Warehouse") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');
            $('#SuperBuildUpArea_'+CommercialLayoutCount).val('');
            $('#BuildUpArea_'+CommercialLayoutCount).val('');  
            $('#carpetArea_'+CommercialLayoutCount).val('');  
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        CommercialLayoutCount = CommercialLayoutCount + 1;

        
        // console.log("layout : "+layoutCountHidden);

    }
    function GetProcePerUnit(id) 
    {  
        var listingFor = $('#listingFor:checked').val();

        // alert($('#Expected_'+id).val());
        // if ($('#SuperBuildUpArea_'+id).val() > "0") 
        // { 

        // }
        // else
        // { 
        //     $('#SuperBuildUpArea_'+id).val('0');
        // }

        if ($('#Expected_'+id).val() > "10") 
        {

            convertNumbertoWords('Expected_'+id,'numToWords'+id);


            if($('#villa_Area_details_'+id+':visible').length == 1)
            {   

                // alert($('#villa_Area_details_'+id+':visible').length);
                var SuperBuildUpArea = 0;
                var carpetArea = $('#ploatcarpetArea_'+id).val();



                // if ($('#SuperBuildUpArea_'+id).val() > "0") 
                // {   
                   
                //     if ($('#ploatcarpetArea_'+id).val() == "") 
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                //     else
                //     {
                //         if ($('#ploatcarpetArea_'+id).val() > "2") 
                //         {
                //             SuperBuildUpArea = $('#ploatcarpetArea_'+id).val();
                //         }
                //         else
                //         {
                //             $('#ploatcarpetArea_'+id).focus();
                //         }
                //         // SuperBuildUpArea = $('#ploatcarpetArea_'+id).val();
                //     }
                // }
                // else
                // {   
                //     if ($('#SuperBuildUpArea_'+id).val() > "2") 
                //     {
                //         SuperBuildUpArea = $('#SuperBuildUpArea_'+id).val();
                //     }
                //     else
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                // }
                if ($('#ploatcarpetArea_'+id).val() > "2") 
                {
                    SuperBuildUpArea = $('#ploatcarpetArea_'+id).val();
                }
                else
                {
                    $('#ploatcarpetArea_'+id).val('');
                    $('#ploatcarpetArea_'+id).focus();
                }

            }
            else
            {
                var SuperBuildUpArea = 0;

                var carpetArea = $('#ploatcarpetArea_'+id).val();
                // if (SuperBuildUpArea != "" && carpetArea != "") 
                // {
                //     alert("both");
                // }

                // if ($('#SuperBuildUpArea_'+id).val() > "0") 
                // {   
                   
                //     if ($('#carpetArea_'+id).val() == "") 
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                //     else
                //     {   
                //         if ($('#carpetArea_'+id).val() > "0") 
                //         {
                //             SuperBuildUpArea = $('#carpetArea_'+id).val();
                //         }
                //         else
                //         {
                //             $('#carpetArea_'+id).focus();
                //         }
                //         // SuperBuildUpArea = $('#carpetArea_'+id).val();
                //     }
                // }
                // else
                // {   
                //     if ($('#SuperBuildUpArea_'+id).val() > "0") 
                //     {
                //         SuperBuildUpArea = $('#SuperBuildUpArea_'+id).val();
                //     }
                //     else
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                    
                // }
                if ($('#carpetArea_'+id).val() > "0") 
                {
                    SuperBuildUpArea = $('#carpetArea_'+id).val();
                }
                else
                {   
                    $('#carpetArea_'+id).val('');
                    $('#carpetArea_'+id).focus();
                }
               
            }

            if (listingFor == 1)
            {   
                var Expected = $('#Expected_'+id).val();
                var UnitID = $('#UnitID :Selected').text();
                var PricePerUnit = parseInt(Expected) / parseInt(SuperBuildUpArea);
                if (SuperBuildUpArea  != "") 
                {
                   
                    $('#PricePerUnit_'+id).val(parseInt(PricePerUnit));
                }
               
            }
            else
            {
                var Expected = $('#Expected_'+id).val();
                var UnitID = $('#UnitID :Selected').text();
                var PricePerUnit = parseInt(Expected) / parseInt(SuperBuildUpArea);
                if (SuperBuildUpArea  != "") 
                {
                    $('#PricePerUnit_'+id).val(parseInt(PricePerUnit));
                } 
            }
             convertNumbertoWords('PricePerUnit_'+id,'pnumToWords'+id);
        }
        
    }
    function AllcheckBoxArray(className,id) {
        
        var checkedValue = Array(); 

        var inputElements = document.getElementsByClassName(className);

        // console.log(inputElements);

        for(var i=0; inputElements[i]; ++i)
        {
              if(inputElements[i].checked)
              {  
                // console.log("checked");
                checkedValue.push(1);   
              }
              else
              {
                // console.log("unChecked"); 
                checkedValue.push(0);
              }
        }

        $('#'+id).val(checkedValue);

        //console.log(className+" :  "+$('#'+id).val());
    }
    function SetWashroomValue(id = null) 
    {   
       
        if (id == null) 
        {
            $('#washroom_Office').html('');
            $('#balcony_Office').html('');
            $('#pentry_Office').html('');

            for (var i = 0; i <= 20; i++) 
            {
                $('#washroom_Office').append('<option value='+i+'>'+i+'</option>');
                $('#balcony_Office').append('<option value='+i+'>'+i+'</option>');
                $('#pentry_Office').append('<option value='+i+'>'+i+'</option>');
            }
            $('#washroom_Office').append('<option value=21>20+</option>');
            $('#balcony_Office').append('<option value=21>20+</option>');
            $('#pentry_Office').append('<option value=21>20+</option>');


        }
        else
        {
            $('#washroom_Office_'+id).html('');
            $('#balcony_Office_'+id).html('');
            $('#pentry_Office_'+id).html('');
             // alert(id);
            for (var i = 0; i <= 20; i++) 
            {
                $('#washroom_Office_'+id).append('<option value='+i+'>'+i+'</option>');
                $('#balcony_Office_'+id).append('<option value='+i+'>'+i+'</option>');
                $('#pentry_Office_'+id).append('<option value='+i+'>'+i+'</option>');
            }
            $('#washroom_Office_'+id).append('<option value=21>20+</option>');
            $('#balcony_Office_'+id).append('<option value=21>20+</option>');
            $('#pentry_Office_'+id).append('<option value=21>20+</option>');
        }
    }
    function removeDivTag(id) 
    {   
        layoutCountHidden = layoutCountHidden - 1;
        // alert(layoutCountHidden);
        $('#m_accordion_4_'+id).remove();

        $('#layoutCount').val(layoutCountHidden);
        // alert(layoutCountHidden - 1);
        var ids = "layout_image"+id;

        if($.inArray(ids,layoutImages) != -1)
        {
            var index = layoutImages.indexOf(ids);

            if(index!=-1)
            {
               layoutImages.splice(index, 1);
            }
        }
    }

    function removeDivTagAjax(count,id) 
    {
      

        // alert(id);
        swal({
        title:"Are you sure?",
        text:"You want to delete this Layout ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>properties/RemovePropertyLayout',
                    type: 'POST',
                    data: {id : id},
                    success: function(res) 
                    {
                        if(res.trim() == "true")
                        {

                            swal( {
                                title: "", text: "Project Layout has been successfully added!", type: "success", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                            }
                            )
                                layoutCountHidden = layoutCountHidden - 1;
                                // alert(layoutCountHidden);
                                $('#m_accordion_4_'+count).remove();

                                $('#layoutCount').val(layoutCountHidden);
                        }
                        else
                        {
                           swal( {
                                title: "", text: "Seems something wrong! Try again", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                            }
                            ) 
                        }
                    }
                });
            }   
       });
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
    function convertNumbertoWords(id,wordId) 
    {   
        // alert($('#'+id).val());
        document.getElementById(wordId).innerHTML = price_in_words(document.getElementById(id).value);
    }
    function price_in_words(price) {
        var sglDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"],
            dblDigit = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"],
            tensPlace = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"],
            handle_tens = function(dgt, prevDgt) {
                return 0 == dgt ? "" : " " + (1 == dgt ? dblDigit[prevDgt] : tensPlace[dgt])
            },
            handle_utlc = function(dgt, nxtDgt, denom) {
                return (0 != dgt && 1 != nxtDgt ? " " + sglDigit[dgt] : "") + (0 != nxtDgt || dgt > 0 ? " " + denom : "")
            };

            var str = "",
                digit = 0,
                digitIdx = 0,
                nxtDigit = 0,
                words = [];
            if (price += "", isNaN(parseInt(price))) str = "";
            else if (parseInt(price) > 0 && price.length <= 10) {
                for (digitIdx = price.length - 1; digitIdx >= 0; digitIdx--) switch (digit = price[digitIdx] - 0, nxtDigit = digitIdx > 0 ? price[digitIdx - 1] - 0 : 0, price.length - digitIdx - 1) {
                    case 0:
                        words.push(handle_utlc(digit, nxtDigit, ""));
                        break;
                    case 1:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 2:
                        words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] && 0 != price[digitIdx + 2] ? " and" : "") : "");
                        break;
                    case 3:
                        words.push(handle_utlc(digit, nxtDigit, "Thousand"));
                        break;
                    case 4:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 5:
                        words.push(handle_utlc(digit, nxtDigit, "Lakh"));
                        break;
                    case 6:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 7:
                        words.push(handle_utlc(digit, nxtDigit, "Crore"));
                        break;
                    case 8:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 9:
                        words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] || 0 != price[digitIdx + 2] ? " and" : " Crore") : "")
                }
                str = words.reverse().join("")
            } else str = "";
            return str

    }
    //  function convertNumbertoWords(id,wordId) 
    // {   
    //     // alert($('#'+id).val());
    //     document.getElementById(wordId).innerHTML = inWords(document.getElementById(id).value);
    // }
    // function inWords (num)
    // {
    //     var a = ['','One ','Two ','Three ','Four ', 'Five ','Six ','Seven ','Eight ','Nine ','Ten ','Eleven ','Twelve ','Thirteen ','Fourteen ','Fifteen ','Sixteen ','Seventeen ','Eighteen ','Nineteen '];
    //     var b = ['', '', 'Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety'];
    //     num = parseInt(num);
        
    //     if ((num = num.toString()).length > 11) return 'Out of bound';
    //     n = ('00000000000' + num).substr(-11).match(/^(\d{2})(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    //     if (!n) return; var str = '';
    //     str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'arab ' : '';
    //     str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Crore ' : '';
    //     str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Lakh ' : '';
    //     str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Thousand ' : '';
    //     str += (n[5] != 0) ? (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'Hundred ' : '';
    //     str += (n[6] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[6])] || b[n[6][0]] + ' ' + a[n[6][1]]) + 'only ' : '';

    //     return str;
    // }
    function getPFlor(id) 
    {
        var count = $('#tFloor'+id).val();

        if (parseInt(count) <= 100) 
        {   
            $('#pFloor'+id).html('');
            $('#pFloor'+id).append('<option value="0">Ground Floor</option>');
            for (var i = 1; i <= count; i++) 
            {   
                $('#pFloor'+id).append('<option value='+i+'>'+i+'</option>');
            }
        }
        else
        {   
            $('#tFloor'+id).focus();
            
            $('#pFloor'+id).html('<option value="0">Ground Floor</option>');
        }
    }
</script>

<script src="<?php echo Urls::$COMMONS ?>assets/custom/map_find.js" type="text/javascript"></script>