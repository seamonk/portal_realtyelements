<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-app"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleTypes))
                                {
                                    echo "Update Amenity";
                                }
                                else
                                {
                                    echo "Add Amenity";
                                }?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <?php
                        if (!empty($this->singleTypes))
                        {
                            ?>
                             <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <?php
                            
                        }
                        else
                        {
                            ?>
                             <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <?php
                        }?>
                       
                            <div class="m-accordion__item-content">
                                
                                    <?php
                                    if (!empty($this->singleTypes))
                                    {
                                        for ($i = 0; $i < count($this->singleTypes); $i++) 
                                        {
                                        ?> 
                                            <form id="m_form" enctype ="multipart/form-data" class="m-form" action="<?php echo Urls::$BASE; ?>properties/createAmenity" method="POST">
                                                <div class="m-portlet__body">
                                                    <div class="form-group m-form__group row">
                                                        <div class="form-group col-lg-12">
                                                                         

                                                            <input type="hidden" name="amenityID" value="<?php echo $this->singleTypes[$i]['nAmenityIDPK']; ?>">
                                                            <label><h5>Select Amenity Icon</h5></label>
                                                            
                                                            <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                                <span class="" onclick="document.getElementById('banner_image').click();">
                                                                     <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                                </span>
                                                            </div>
                                                            <br>
                                                            <input style="display:none;" type="file" name="banner_image"  id="banner_image" onchange="vpb_image_preview(this)">

                                                            <div style="width:100%;" align="center" id="vpb-display-preview">
                                                               
                                                               <img id="imgProduct" width="100px;" src="<?php echo Urls::$BASE.Urls::$ABOUTUS.$this->singleTypes[0]['tAmenityImagePath']?>" >

                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="row">
                                                               
                                                                <div class="col-lg-12">
                                                                    <br>
                                                                    <label>Amenity Title</label>
                                                                    <input type="text" class="form-control m-input" value="<?php echo $this->singleTypes[$i]['tAmenityName']; ?>" name="amenity" id="amenity">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-2"></div>
                                                        <div class="col-lg-4">
                                                            <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update Amenity</button>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <button type="reset" class="btn default btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>properties/amenity';">Cancel</button>
                                                        </div>
                                                        <div class="col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php 
                                        }
                                    }
                                    else
                                    {?>
                                        <form id="m_form" enctype ="multipart/form-data" class="m-form" action="<?php echo Urls::$BASE; ?>properties/createAmenity" method="POST">
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                                               
                                                    <label><h5>Select Amenity Icon</h5></label>
                                                    
                                                    <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">

                                                    <input style="display:none;" type="file" name="banner_image"  id="banner_image" onchange="vpb_image_preview(this)">

                                                    <div style="width:100%;" align="center" id="vpb-display-preview">
                                                        <span class="" onclick="document.getElementById('banner_image').click();">
                                                             <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                        </span>

                                                       
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-12">

                                                            <br>
                                                            <label>Amenity Title</label>
                                                            <input type="text" class="form-control m-input" name="amenity" id="amenity">
                                                            <!-- oninput="check_Type_exist()" -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4">
                                                    <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">Add Amenity</button>
                                                </div>
                                                <div class="col-lg-4">
                                                    <button type="reset" class="btn default btn-block" >Cancel</button>
                                                </div>
                                                <div class="col-lg-2"></div>
                                            </div>
                                        </form>
                                    <?php
                                    }?>
                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Amenity Title</th>
                                        <th>Amenity Image</th>
                                        <th width="15%;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($this->data)) 
                                    {
                                        for ($i = 0; $i < count($this->data); $i++) 
                                        {
                                            ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td> 
                                            <td><?php echo $this->data[$i]['tAmenityName']; ?></td>
                                             <td><center>
                                                    <?php
                                                    if (!empty($this->data[$i]['tAmenityImagePath'])) 
                                                    {
                                                        ?>
                                                            <img width="32px;" class="m--marginless" src="<?php echo Urls::$BASE.Urls::$ABOUTUS.$this->data[$i]['tAmenityImagePath']; ?>"> 
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                            <img width="32px;" class="m--marginless" src="<?php echo Urls::$BASE.Urls::$ABOUTUS.'no-image.png'; ?>">    
                                                        <?php
                                                    }


                                                    ?>
                                                </center>
                                            <td>
                                                <form method="post" action="<?php echo Urls::$BASE ?>properties/edit_Amenity" style="display: inline-table;">
                                                    <input type="hidden" name="typeID" value="<?php echo $this->data[$i]['nAmenityIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit <?php echo $this->data[$i]['tAmenityName']; ?> Detail" type="button" class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                                </form>

                                                <form style="display: inline-table;">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Type" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick="setId(<?php echo $this->data[$i]['nAmenityIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                </form>
                                                
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                    }
                                    ?>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() 
    {
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $('#s1').DataTable({
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
           "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]},
                {"orderable": false,"targets": [0,3]}                   
            ],
        } ); 
        
        $("#m_form").validate({
            rules: {
                amenity: {
                    required: true
                }
            }
        });


    }); 
    
    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });      
    function vpb_image_preview(vpb_selector_) 
    {

       
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#vpb-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                    console.log('#vpb-display-preview_0');

                       $('#vpb-display-preview').append(
                       '\
                       <img id="image'+vpb_o_+'" width="100px;" name="image[0][] class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br /> \
                       ');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });

        
    }
    function vpb_remove_selected(id,name)
    {
        var get_value = $('#deleteImg').val();

        if(get_value == '')
        {
            $('#deleteImg').val(name);
        }
        else
        {
            $('#deleteImg').val(get_value+','+name);
        }

         var ds = Array();
        ds.push(id);
        // delete [ds];
        var deta = Array();
        deta.push($('#deleteImg').val());

        $('#deleteImgs').val(deta);
        $('#selector_'+id).html("");
        document.getElementById('selector_'+id).style.display = "none";
        console.log(deta);

        console.log(ds);
       
        // $('#v-add-'+id).remove();
       
    }
    function setId (del_id)
    {
        swal({
        title:"Are you sure?",
        text:"You want to delete this Amenity ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>properties/deleteAmenity",
                    data:{'id':del_id},
                    success: function(data)
                    {     
                      if(data == "true")
                      {
                        location.href="<?php echo Urls::$BASE ?>properties/amenity";
                      }               
                    }
                });
            }   
       });
    }  
</script>
