 <?php

 public function updateProperty()
    {
        // echo "<pre>";
        $dummyID = DUMMY_DATA;       
        extract($_POST);


        $loggedId = SessionHandling::get("loggedId");

        $id = array('nPropertyIDPK' => $_POST['ID']);


        if ($_POST['submit'] == "update_basic") 
        {

           
            $date = str_replace('/', '-', $possessionDate );
            $possessionDate = date("Y-m-d", strtotime($date));
            if(isset($rerayes))
            {
                if($rerayes != "yes")
                {   
                    $rerayes = 0;
                    $reraid = "";
                }
                else
                {
                    $rerayes = 1;
                }
            }
            else
            {
                $rerayes = 0;
                $reraid = "";
            } 
            $currentDate = date('Y-m-d H:i:s');

            $field = array('dtPropertyPossesionDate','nPropertyFor','nPropertyAvailability','bIsPropertyReraCertified','tPropertyReraID');
            $value = array($possessionDate,$propFor,$Availability,$rerayes,$reraid);

            $data = array_combine($field,$value);
            $res = $this->model->update($data, 'tblproperties',$id);
        }
        else if ($_POST['submit'] == "Update_location") 
        {   
            // print_r($_FILES); 
            // print_r($_POST);
            // exit(); //
           
            $field = array(
                            'tPropertyName',
                            'tPropertyStreetName', 
                            'tPropertyAddress', 
                            'nLocalityIDFK',
                            'nPropertyAreaIDFK',
                            'nPropertyCityIDFK', 
                            'nPropertyStateIDFK', 
                            'nPropertyCountryIDFK', 
                            'dPropertyLatitude', 
                            'dPropertyLongitude', 
                            'tPropertyMapLocationLink');
            $value = array(
                        $propertyName,
                        $streetName,
                        $pAddress,
                        $LocalityID,
                        $locationID,
                        $cityID,
                        $stateID,
                        $countryID,
                        $pLatitutde,
                        $pLongitude,
                        $mapLocationLink);

            $data = array_combine($field,$value);
            // print_r($data);
            // exit();
            $res = $this->model->update($data, 'tblproperties',$id);
        }
        else if ($_POST['submit'] == "Update_Property_Details") 
        {
            // echo "<pre>";
            // print_r($_POST);
            // print_r($_FILES);
            // exit();
            // $deleteAlllaypout = $this->model->delete($ID,'tblprojectlayouts','nPropertyIDFK');
            $field = array('tPropertyOwnership','tPropertyDescription');
            $value = array($ownership,$propertyDescription);
            $data = array_combine($field,$value);
            // print_r($data);
            // exit();
            $res = $this->model->update($data, 'tblproperties',$id);

            for ($j=0; $j < $layoutCount; $j++) 
            { 

                if(isset($layout['ExpectedNegociable']))
                { 
                    if(isset($layout['ExpectedNegociable'][$j]) && $layout['ExpectedNegociable'][$j] = "yes")
                    {   
                        
                        $ExpectedNegociable = 1;
                    }
                    else{

                        $ExpectedNegociable = 0;
                    }
                }
                
                if(isset($layout['Cparking']))
                { 
                    if(isset($layout['Cparking'][$j]) && $layout['Cparking'][$j] > 0)
                    {   
                        
                        $Cparking = $layout['Cparking'][$j];
                    }
                    else{

                        $Cparking = 0;
                    }
                }
                if(isset($layout['Bathrooms']))
                { 
                    if(isset($layout['Cparking'][$j]) && $layout['Cparking'][$j] > 0)
                    {   
                        
                        $Cparking = $layout['Cparking'][$j];
                    }
                    else{

                        $Cparking = 0;
                    }
                }
                else
                {
                    $layout['Bathrooms'][$j] = 0;
                }
                if(isset($layout['priceDisplayed']))
                { 
                    if(isset($layout['priceDisplayed'][$j]) && $layout['priceDisplayed'][$j] == 'yes')
                    {   
                        
                        $layout['priceDisplayed'][$j] = 1;
                    }
                    else{

                        $layout['priceDisplayed'][$j] = 0;
                    }
                }
                else
                {
                    $layout['priceDisplayed'][$j] = 0;
                }


                if(isset($layout['BuildUpArea'][$j]))
                {  
                    if (isset($layout['BuildUpArea'][$j]) && !empty($layout['BuildUpArea'][$j])) 
                    {
                         $BuildUpArea = $layout['BuildUpArea'][$j];
                    }
                    else
                    {
                        if(isset($layout['ploatBuildUpArea'][$j]))
                        { 
                            if(!empty($layout['ploatBuildUpArea'][$j]))
                            {   
                                
                                $BuildUpArea = $layout['ploatBuildUpArea'][$j];
                            }
                            else
                            {

                                $BuildUpArea = 0;
                            }
                        }
                        else
                        {
                            $BuildUpArea = 0;
                        }
                    }
                }
                else
                {
                     $BuildUpArea = 0;
                    
                }
                if(isset($layout['SuperBuildUpArea'][$j]))
                {   
                    if(empty($layout['SuperBuildUpArea'][$j]))
                    {   
                        if(isset($layout['ploatSuperBuildUpArea'][$j]))
                        { 
                            if(!empty($layout['ploatSuperBuildUpArea'][$j]))
                            {   
                                
                                $SuperBuildUpArea = $layout['ploatSuperBuildUpArea'][$j];
                            }
                            else
                            {
                                $SuperBuildUpArea = 0;
                                
                            }
                        }
                        else
                        {
                            $SuperBuildUpArea = 0;
                        }
                    }
                    else
                    {
                        $SuperBuildUpArea = $layout['SuperBuildUpArea'][$j];
                    }
                }
                else
                {
                     $SuperBuildUpArea = 0;
                    
                }

                if(isset($layout['carpetArea'][$j]))
                {   
                    if($layout['carpetArea'][$j] == "")
                    {   
                        if (isset($layout['ploatcarpetArea'][$j]) && !empty($layout['ploatcarpetArea'][$j])) 
                        {
                             $carpetArea = $layout['ploatcarpetArea'][$j];
                        }
                        else
                        {
                           if(isset($layout['ploatcarpetArea'][$j]))
                            { 
                                if(!empty($layout['ploatcarpetArea'][$j]))
                                {   
                                    
                                    $carpetArea = $layout['ploatcarpetArea'][$j];
                                }
                                else
                                {

                                    $carpetArea = 0;
                                }
                            }
                            else
                            {
                                $carpetArea = 0;
                            }
                        }   
                    }
                    else
                    {
                        $carpetArea = $layout['carpetArea'][$j];
                    }
                }
                else
                {
                     $carpetArea = 0;
                    
                }
                
                if(isset($layout['layoutPropBHK'][$j]))
                { 
                    if(!empty($layout['layoutPropBHK']))
                    {   
                        
                        $layoutPropBHK = $layout['layoutPropBHK'][$j];
                    }
                    else
                    {

                        $layoutPropBHK = 0;
                    }
                }
                else
                {
                    $layoutPropBHK = 0;
                }

                if(isset($layout['constructionSuperBuildUpArea'][$j]))
                { 
                    if(!empty($layout['constructionSuperBuildUpArea'][$j]))
                    {   
                        
                        $constructionSuperBuiltArea = $layout['constructionSuperBuildUpArea'][$j];
                    }
                    else
                    {

                        $constructionSuperBuiltArea = 0;
                    }
                }
                else
                {
                    $constructionSuperBuiltArea = 0;
                }

                if(isset($layout['constructioncarpetArea'][$j]))
                { 
                    if(!empty($layout['constructioncarpetArea'][$j]))
                    {   
                        
                        $constructionCarpetArea = $layout['constructioncarpetArea'][$j];
                    }
                    else
                    {

                        $constructionCarpetArea = 0;
                    }
                }
                else
                {
                    $constructionCarpetArea = 0;
                }
                
                if(isset($layout['layoutPropBHK'][$j]))
                { 
                    if(!empty($layout['layoutPropBHK'][$j]))
                    {   
                        
                        $layoutPropBHK = $layout['layoutPropBHK'][$j];
                    }
                    else
                    {

                        $layoutPropBHK = 0;
                    }
                }
                else
                {
                    $layoutPropBHK = 0;
                }
                if ($propertyType == 2) 
                {
                    
                    if(!empty($layout['balcony'][$j]))
                    {   
                        if($layout['balcony'][$j] == "" && $layout['balcony'][$j] == 0)
                        {   
                            $balcony = 0;
                            
                        }
                        else
                        {
                            $balcony = $layout['balcony'][$j];
                        }
                    }
                    else
                    {
                         $balcony = 0;
                        
                    }
                }
                else
                {
                    if(isset($layout['Balconies'][$j]))
                    {   
                        if($layout['Balconies'][$j] == "" && $layout['Balconies'][$j] == 0)
                        {   
                            $balcony = 0;
                            
                        }
                        else
                        {
                            $balcony = $layout['Balconies'][$j];
                        }
                    }
                    else
                    {
                         $balcony = 0;
                        
                    }
                }

                if(isset($layout['storeRoomAvailable'][$j]))
                {  
                    $layout['storeRoomAvailable'][$j] = 1;
                }
                else
                {
                    $layout['storeRoomAvailable'][$j] = 0;
                } 


                if(isset($layout['poojaRoomAvailable'][$j]))
                {  
                    $layout['poojaRoomAvailable'][$j] = 1;
                }
                else
                {
                    $layout['poojaRoomAvailable'][$j] = 0;
                } 

                if(isset($layout['Bathrooms'][$j]))
                {  
                    $Bathrooms = 1;
                }
                else
                {
                    $Bathrooms = 0;
                }
                if(isset($layout['Bathrooms'][$j]))
                {  
                    $Bathrooms = $layout['Bathrooms'][$j];
                }
                else
                {
                    $Bathrooms = 0;
                }
                if(isset($layout['pentry'][$j]))
                {  
                    $pentry = $layout['pentry'][$j];
                }
                else
                {
                    $pentry = 0;
                }
                if(isset($layout['Bedrooms'][$j]))
                {  
                    $Bedrooms = $layout['Bedrooms'][$j];
                }
                else
                {
                    $Bedrooms = 0;
                }
                if(isset($layout['washroom'][$j]))
                {  
                    $washroom = $layout['washroom'][$j];
                }
                else
                {
                    $washroom = 0;
                }
                if(isset($layout['Cparking'][$j]))
                {  
                    $Cparking = $layout['Cparking'][$j];
                }
                else
                {
                    $Cparking = 0;
                }
                if(isset($layout['tFloor'][$j]))
                {  
                    $tFloor = $layout['tFloor'][$j];
                }
                else
                {
                    $tFloor = 0;
                }
                if(isset($layout['pFloor'][$j]))
                {  
                    $pFloor = $layout['pFloor'][$j];
                }
                else
                {
                    $pFloor = 0;
                }  
                if(isset($layout['ExpectedNegociable'][$j]))
                { 
                    if(isset($layout['ExpectedNegociable'][$j]) && $layout['ExpectedNegociable'][$j] = "yes")
                    {   
                        
                        $ExpectedNegociable = 1;
                    }
                    else{

                        $ExpectedNegociable = 0;
                    }
                }
                else
                {
                     $ExpectedNegociable = 0;
                }
                if(isset($layout['propertyFurnishedID'][$j]))
                { 
                    if(isset($layout['propertyFurnishedID'][$j]) && $layout['propertyFurnishedID'][$j] = "yes")
                    {   
                        
                        $propertyFurnishedID = 1;
                    }
                    else{

                        $propertyFurnishedID = 0;
                    }
                }
                else
                {
                     $propertyFurnishedID = 0;
                }
                if(isset($layout['constructionBuildUpArea'][$j]))
                { 
                    if(!empty($layout['constructionBuildUpArea'][$j]))
                    {   
                        
                        $constructionBuiltArea = $layout['constructionBuildUpArea'][$j];
                    }
                    else
                    {

                        $constructionBuiltArea = 0;
                    }
                }
                else
                {
                    $constructionBuiltArea = 0;
                }

                $field = array(
                    'nPropertyIDFK',
                    'nProjectSubTypeIDFK',
                    'nBHKIDFK',
                    'fProjectSuperBuiltArea',
                    'fProjectBuiltArea',
                    'fProjectCarpetArea',
                    'nUnitIDFK',
                    'bIsPropertyFurnished',
                    'nBedrooms',
                    'nBalconies',
                    'nBathrooms',
                    'nWashroom',
                    'nCarParkings',
                    'nTotalFloors',
                    'nPropertyOnFloor',
                    'nPentry',
                    'bIsStoreRoomAvailable',
                    'bIsPoojaRoomAvailable',

                    'fProjectExpectedPrice',

                    'bIsExpectedPriceNegotiable',
                    'fPricePerUnitArea',
                    'fProjectBookingPrice',
                    'bIsPriceShow',
                    'fConstructionSuperBuiltArea',
                    'fConstructionBuiltArea',
                    'fConstructionCarpetArea'
                );


                $value = array(
                    $ID,
                    $propertiesSubType,
                    $layoutPropBHK,
                    $SuperBuildUpArea,
                    $BuildUpArea,
                    $carpetArea,
                    $layout['UnitID'][$j],
                    $propertyFurnishedID,
                    $Bedrooms ,
                    $balcony,
                    $Bathrooms,
                    $washroom,
                    $Cparking,
                    $tFloor,
                    $pFloor,
                    $pentry,
                    $layout['storeRoomAvailable'][$j],
                    $layout['poojaRoomAvailable'][$j],
                    $layout['Expected'][$j],
                    $ExpectedNegociable,
                    $layout['PricePerUnit'][$j],
                    $layout['BookingPrize'][$j],
                    $layout['priceDisplayed'][$j],
                    $constructionSuperBuiltArea,
                    $constructionBuiltArea,
                    $constructionCarpetArea
                );

                $amenityValues = array_combine($field, $value);

                // print_r($amenityValues);
                // exit();
                
                if (isset($layout['LayoutID'][$j]) && !empty($layout['LayoutID'][$j])) 
                {
                    $id = array('nProjectIDPK' => $layout['LayoutID'][$j]);
                    $res = $this->model->update($amenityValues, 'tblprojectlayouts',$id);
                }
                else
                {
                    $res = $this->model->insert($amenityValues, 'tblprojectlayouts');
                   // print_r($tFloor);
                }

                $preImgText = date('ymdhsi');
                
                $folderPathURL = Urls::$IMG_UPLOADS_LISTING;

                $folder_name = $folderPathURL.$preImgText.$loggedId."/";

                if (file_exists($folder_name)) {
                   
                }
                else{

                    mkdir($folder_name, 0777, true);
                }

                if(isset($_FILES['layout_image']['name'][$j]) && !empty($layout['LayoutID'][$j])) 
                {
                    if (!empty($_FILES['layout_image']['name'][$j])) {

                        $propertyImages = $_FILES['layout_image']['name'][$j];

                        $fileExtension = pathinfo($propertyImages,PATHINFO_EXTENSION);

                        $propertyFileName = $preImgText.$loggedId.'/'.$preImgText.$loggedId.$j.'.'.$fileExtension;

                        $targetPath1 = $folder_name.$preImgText.$loggedId.$j.'.'.$fileExtension;
                     
                        move_uploaded_file($_FILES['layout_image']['tmp_name'][$j], $targetPath1);

                        $field = array('tLayoutUmages');
                        $value = array($propertyFileName);

                        $Values = array_combine($field, $value);
                        // print_r($Values);
                        if (isset($layout['LayoutID'][$j]) && !empty($layout['LayoutID'][$j])) 
                        {
                            $id = array('nProjectIDPK' => $layout['LayoutID'][$j]);
                            $res = $this->model->update($Values, 'tblprojectlayouts',$id);
                        }
                        else
                        {
                            $res = $this->model->update($Values, 'tblprojectlayouts',$res);
                        }
                    }
                }
            }
            // exit();


        }
        else if ($submit == "Update_Property_Images") 
        {
            // print_r($_POST);
            // exit();
            $preImgText = date('ymdhsi');
                    
            $folderPathURL = Urls::$IMG_UPLOADS_LISTING;
            $folder_name = $folderPathURL.$preImgText.$loggedId."/";

            mkdir($folder_name, 0777, true);


            $data = $this->model->delete($ID,'tblpropertyamenities','nPropertyIDFK');

            // print_r($data);
            // exit();

            if(isset($AminitiesHidden) && !empty($AminitiesHidden))
            {
                $amenities = explode(',', $AminitiesHidden[1]);

                for ($j=0; $j < count($amenities); $j++) { 
                    
                    $field = array('nPropertyIDFK', 'nAmenityIDFK','nCreatedBy','bIsDummyData');

                    $value = array($ID,$amenities[$j],$loggedId,$dummyID);

                    $amenityValues = array_combine($field, $value);

                    // print_r($amenityValues);
                    // exit();

                    $res = $this->model->insert($amenityValues, 'tblpropertyamenities');
                }
            }
            if (isset($_FILES['files']['name'][0])) 
            {
                
                if($_FILES['files']['name'] != '')
                {  

                    $imagedelete = array();

                    // $imagedelete = explode(',' , $_FILES['DeletedFiles']);
                    if (isset($_FILES['DeletedFiles']['name'][0])) 
                    {
                        $recordsData = array_diff($_FILES['files']['name'],$_FILES['DeletedFiles']['name']);
                       
                    }
                    else{

                        $recordsData = $_FILES['files']['name'];
                    }
                    // print_r($recordsData);
                    // exit();
                    for ($i=0; $i < count($_FILES['files']['name']) ; $i++)
                    { 
                        if (isset($recordsData[$i])) 
                        {

                            $propertyImages = $_FILES['files']['name'][$i];

                            $fileExtension = pathinfo($propertyImages,PATHINFO_EXTENSION);

                            $propertyFileName = $preImgText.$loggedId.'/'.$preImgText.$loggedId.$i.'.'.$fileExtension;

                            $targetPath1 = $folder_name.$preImgText.$loggedId.$i.'.'.$fileExtension;
                         
                            move_uploaded_file($_FILES['files']["tmp_name"][$i], $targetPath1);

                            $field = array('nPropertyIDFK', 'tPropertyImageFolderPath','nCreatedBy','bIsDummyData');

                            $value = array($ID,$propertyFileName,$loggedId,$dummyID);

                            $listingImgValues = array_combine($field, $value);

                            // print_r($listingImgValues);
                            // exit(); 

                            $res = $this->model->insert($listingImgValues, 'tblpropertyimages');                     
                        }
                        else{


                        }
                    }


                }
            }


            if(isset($_FILES['brochurePath']['name']) && $_FILES['brochurePath']['name'] != '')
            {    
                $brochureFileName = $preImgText.$loggedId.'/'.$preImgText.$loggedId.'.pdf';               

                $targetPath1 = $folder_name.$preImgText.$loggedId.'.pdf';

                move_uploaded_file($_FILES['brochurePath']["tmp_name"][0], $targetPath1);                 
            }
            else
            {
               
                $field = array('tPropertyBrochurePath');
                $value = array($brochureFileName);
                $data = array_combine($field,$value);
                
                $res = $this->model->update($data, 'tblproperties',$id);


            }
        }

        if($res > 0)
        {
            echo "true";
        }
        else if($res == 0)
        {
            echo "true";            
        }
        else
        {
            echo "false";
        } 

    }

    ?>