<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class Properties_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
            // print_r($key ." : ".$val);
            // echo "string";
        }
        // print_r($query);
        //     exit();
        $insert = $query->execute();

        if ($insert > 0) {
            return $this->db->lastInsertId();         
        }
        return 0;
    }

   

    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE ".$whereId." = $id";
        // print_r($sql);
        // exit();
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getPropertyType()
    {
        $sth = $this->db->prepare("SELECT
                                  nPropertyTypeIDPK,
                                  tPropertyTypeName,
                                  bIsPropertyTypeActive,
                                  dPropertyTypeCreatedOn,
                                  bIsPropertyTypeRemoved
                                FROM
                                  tblpropertytypes
                                WHERE
                                bIsPropertyTypeRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getCompanyNames()
    {
      $DUMMY_DATA = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT
                                  nCompanyIDPK,
                                  tCompanyName                                 
                                FROM
                                  tblcompanies
                                WHERE
                                  bIsRemoved = 0
                                AND 
                                  bIsDummyData = $DUMMY_DATA");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getPropertyBHK()
    {
        $sth = $this->db->prepare("SELECT
                                    nBHKIDPK,
                                    tBHKName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblbhk
                                  WHERE
                                    bIsRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
    
     public function getAmenitiesDetails()
    {
        $sth = $this->db->prepare("SELECT
                                      nAmenityIDPK,
                                      tAmenityName,
                                      tAmenityImagePath,
                                      dtCreatedOn,
                                      nCreatedBy,
                                      bIsRemoved,
                                      dtRemovedOn,
                                      nRemovedBy,
                                      bIsDummyData
                                    FROM
                                      tblamenities
                                    WHERE
                                      bIsRemoved = 0");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
    public function getSinglePropertyBHK($id)
    {
        $sth = $this->db->prepare("SELECT
                                    nBHKIDPK,
                                    tBHKName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblbhk
                                  WHERE
                                    bIsRemoved = 0
                                    AND
                                    nBHKIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
    public function getPropertyUnit()
    {
        $sth = $this->db->prepare("SELECT
                                    nUnitIDPK,
                                    tUnitName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblunits
                                  WHERE
                                    bIsRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
    public function getSinglePropertyUnit($id)
    {
        $sth = $this->db->prepare("SELECT
                                    nUnitIDPK,
                                    tUnitName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblunits
                                  WHERE
                                    bIsRemoved = 0
                                    AND
                                    nUnitIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
    
    public function getPropertyAmenity()
    {
        $sth = $this->db->prepare("SELECT
                                      nAmenityIDPK,
                                      tAmenityName,
                                      tAmenityImagePath,
                                      dtCreatedOn,
                                      nCreatedBy,
                                      bIsRemoved,
                                      dtRemovedOn,
                                      nRemovedBy,
                                      bIsDummyData
                                    FROM
                                      tblamenities
                                    WHERE
                                        bIsRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getSingleAmenity($id)
    {
        $sth = $this->db->prepare("SELECT
                                      nAmenityIDPK,
                                      tAmenityName,
                                      tAmenityImagePath,
                                      dtCreatedOn,
                                      nCreatedBy,
                                      bIsRemoved,
                                      dtRemovedOn,
                                      nRemovedBy,
                                      bIsDummyData
                                    FROM
                                      tblamenities
                                    WHERE
                                        bIsRemoved = 0
                                        AND
                                        nAmenityIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getSingleTypes($id)
    {
        $sth = $this->db->prepare("SELECT nPropertyTypeIDPK, tPropertyTypeName, bIsPropertyTypeActive, dPropertyTypeCreatedOn, bIsPropertyTypeRemoved FROM tblpropertytypes WHERE nPropertyTypeIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getSingleSubTypes($id)
    {
        $sth = $this->db->prepare("SELECT
                                      sub.nPropertySubTypeIDPK,
                                      type.nPropertyTypeIDPK,
                                      type.tPropertyTypeName,
                                      sub.tPropertySubTypeName,
                                      sub.tPropertySubTypeImagePath,
                                      sub.dtPropertSubTypeCreatedOn,
                                      sub.bIsPropertySubTypeRemoved
                                    FROM
                                      tblpropertysubtypes as sub,
                                      tblpropertytypes as type
                                    WHERE
                                     sub.nPropertyTypeIDFK = type.nPropertyTypeIDPK
                                     AND
                                      nPropertySubTypeIDPK = $id
                                      AND
                                        sub.bIsPropertySubTypeRemoved = 0");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getSingleDerivedTypes($id)
    {
        $sth = $this->db->prepare("SELECT
                                      derived.nPropertyDerivedTypeIDPK,
                                      sub.nPropertySubTypeIDPK,
                                      sub.tPropertySubTypeName,
                                      derived.tPropertyDerivedTypeName,
                                      sub.tPropertySubTypeImagePath,
                                      derived.dtPropertyDerivedTypeCreatedOn,
                                      derived.bIsPropertyDerivedTypeRemoved
                                    FROM
                                      tblpropertyderivedtype as derived,
                                      tblpropertysubtypes as sub
                                    WHERE
                                      derived.nPropertySubTypeIDFK = sub.nPropertySubTypeIDPK
                                      AND
                                      derived.bIsPropertyDerivedTypeRemoved = 0
                                      AND
                                      derived.nPropertyDerivedTypeIDPK = $id
                                      ");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getPropertySubType()
    {
        $sth = $this->db->prepare("SELECT
                                      sub.nPropertySubTypeIDPK,
                                      type.nPropertyTypeIDPK,
                                      type.tPropertyTypeName,
                                      sub.tPropertySubTypeName,
                                      sub.tPropertySubTypeImagePath,
                                      sub.dtPropertSubTypeCreatedOn,
                                      sub.bIsPropertySubTypeRemoved
                                    FROM
                                      tblpropertysubtypes as sub,
                                      tblpropertytypes as type
                                    WHERE
                                     sub.nPropertyTypeIDFK = type.nPropertyTypeIDPK
                                     AND
                                    sub.bIsPropertySubTypeRemoved = 0");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getPropertyDerivedType()
    {
        $sth = $this->db->prepare("SELECT
                                      derived.nPropertyDerivedTypeIDPK,
                                      sub.nPropertySubTypeIDPK,
                                      sub.tPropertySubTypeName,
                                      derived.tPropertyDerivedTypeName,
                                      derived.dtPropertyDerivedTypeCreatedOn,
                                      derived.bIsPropertyDerivedTypeRemoved
                                    FROM
                                      tblpropertyderivedtype as derived,
                                      tblpropertysubtypes as sub
                                    WHERE
                                      derived.nPropertySubTypeIDFK = sub.nPropertySubTypeIDPK
                                      AND
                                      derived.bIsPropertyDerivedTypeRemoved = 0");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getLayoutType()
    {
        $sth = $this->db->prepare("SELECT
                                      derived.nPropertyDerivedTypeIDPK,
                                      sub.nPropertySubTypeIDPK,
                                      sub.tPropertySubTypeName,
                                      derived.tPropertyDerivedTypeName,
                                      derived.dtPropertyDerivedTypeCreatedOn,
                                      derived.bIsPropertyDerivedTypeRemoved
                                    FROM
                                      tblpropertyderivedtype as derived,
                                      tblpropertysubtypes as sub
                                    WHERE
                                      derived.nPropertySubTypeIDFK = sub.nPropertySubTypeIDPK
                                      AND
                                      derived.bIsPropertyDerivedTypeRemoved = 0");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getSinglePropertySubType($id)
    {
        $sth = $this->db->prepare("SELECT nPropertySubTypeIDPK, nPropertyTypeIDFK,tPropertySubTypeImagePath, tPropertySubTypeName, dtPropertSubTypeCreatedOn, bIsPropertySubTypeRemoved FROM tblpropertysubtypes WHERE nPropertySubTypeIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getDerivedTypeDetails($id)
    {
        $sth = $this->db->prepare("SELECT
                                      derived.nPropertyDerivedTypeIDPK,
                                      sub.nPropertySubTypeIDPK,
                                      sub.tPropertySubTypeName,
                                      derived.tPropertyDerivedTypeName,
                                      derived.dtPropertyDerivedTypeCreatedOn,
                                      derived.bIsPropertyDerivedTypeRemoved
                                    FROM
                                      tblpropertyderivedtype as derived,
                                      tblpropertysubtypes as sub
                                    WHERE
                                      derived.nPropertySubTypeIDFK = sub.nPropertySubTypeIDPK
                                      AND
                                      derived.bIsPropertyDerivedTypeRemoved = 0
                                      AND
                                      derived.nPropertySubTypeIDFK = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    } 

    public function getSubTypeDetails($id)
    {
        $sth = $this->db->prepare("SELECT
                                      sub.nPropertySubTypeIDPK,
                                      sub.tPropertySubTypeImagePath,
                                      sub.tPropertySubTypeName, 
                                      sub.dtPropertSubTypeCreatedOn,
                                      sub.bIsPropertySubTypeRemoved
                                    FROM
                                      tblpropertysubtypes as sub
                                    
                                    WHERE
                                    sub.bIsPropertySubTypeRemoved = 0
                                    AND
                                    sub.nPropertyTypeIDFK = $id");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    } 

    public function getfacingDetails()
    {
        $sth = $this->db->prepare("SELECT
                                    nFacingIDPK,
                                    tFacingName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblfacings
                                  WHERE
                                    bIsRemoved = 0");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    } 

    public function getSubTypeDetailsMain()
    {
        $sth = $this->db->prepare("SELECT
                                      sub.nPropertySubTypeIDPK,
                                      sub.tPropertySubTypeImagePath,
                                      sub.tPropertySubTypeName, 
                                      sub.dtPropertSubTypeCreatedOn,
                                      sub.bIsPropertySubTypeRemoved
                                    FROM
                                      tblpropertysubtypes as sub
                                    
                                    WHERE
                                    sub.bIsPropertySubTypeRemoved = 0
                                   ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    } 
    public function getPropertyLocation($PropertyID)
    {
        $sth = $this->db->prepare("SELECT
                                      nPropertyIDPK,
                                      nLocalityIDFK,
                                      tPropertyStreetName,
                                      tPropertyAddress,
                                      tPropertyMapLocationLink,
                                      nCreatedBy,
                                      bIsRemoved,
                                      bIsDummyData
                                  FROM
                                      tblproperties
                                  WHERE
                                  nPropertyIDPK = $PropertyID
                                   ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    } 
    
    
    public function getTypeDetails()
    {
        $sth = $this->db->prepare("SELECT
                                      nPropertyTypeIDPK,
                                      tPropertyTypeName,
                                      bIsPropertyTypeActive,
                                      dPropertyTypeCreatedOn,
                                      bIsPropertyTypeRemoved
                                    FROM
                                      tblpropertytypes
                                    WHERE
                                      bIsPropertyTypeRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getPropertyFacing()
    {
        $sth = $this->db->prepare("SELECT
                                    nFacingIDPK,
                                    tFacingName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblfacings
                                  WHERE
                                    bIsRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getSinglePropertyFacing($id)
    {
        $sth = $this->db->prepare("SELECT
                                    nFacingIDPK,
                                    tFacingName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblfacings
                                  WHERE
                                    bIsRemoved = 0
                                    AND
                                    nFacingIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
     

    public function getUnitsDetails()
    {
        $sth = $this->db->prepare("SELECT
                                    nUnitIDPK,
                                    tUnitName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblunits
                                    WHERE
                                    bIsRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getBHKDetails()
    {
        $sth = $this->db->prepare("SELECT
                                    nBHKIDPK,
                                    tBHKName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                  FROM
                                    tblbhk
                                  WHERE
                                    bIsRemoved = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }
    public function getPropertiesCount($loggedId,$userType)
    {
        $whereSql = '';

        if($userType == ADMIN)
        {
          $whereSql = '';
        }
        else if($userType == AGENT)
        {
          $whereSql = " AND prop.nCreatedBy = ".$loggedId;  
        }
        else if($userType == DEVELOPER)		
        {		
          $whereSql = " AND prop.nCreatedBy = ".$loggedId;  		
        }

        $sth = $this->db->prepare("SELECT
                                            prop.nPropertyIDPK,
                                            prop.tPropertyName,
                                            prop.nPropertyAddedBy,
                                            prop.tPropertyNumber,
                                            prop.tPropertyAddress,
                                            prop.fPropertySuperBuiltArea,
                                            prop.tPropertyReraID,
                                            prop.fPropertyBrokerageCharge,
                                            type.tPropertyTypeName,
                                            prop.nPropertyFor,
                                            subtype.tPropertySubTypeName,
                                            prop.bIsPropertyVerifiedByAdmin,
                                            prop.bIsPropertyReraCertified,
                                            prop.nPropertyTotalViews,
                                            prop.dtCreatedOn,
                                            area.tAreaName,
                                            city.tCityName,
                                            state.tStateName 
                                        FROM
                                            tblproperties as prop,
                                            tblpropertytypes as type,
                                            tblpropertysubtypes as subtype,
                                            tblcities as city,
                                            tblareas as area,
                                            tblstates as state
                                        WHERE
                                            prop.nPropertyTypeIDFK = type.nPropertyTypeIDPK
                                          AND
                                          subtype.nPropertyTypeIDFK = type.nPropertyTypeIDPK
                                          AND
                                            area.nAreaIDPK = prop.nPropertyAreaIDFK
                                          AND
                                            city.nCityIDPK = prop.nPropertyCityIDFK
                                          AND
                                            state.nStateIDPK = prop.nPropertyStateIDFK
                                          AND
                                            prop.bIsAvailable = 1
                                          AND
                                            type.bIsPropertyTypeRemoved = 0
                                          AND
                                            prop.bIsRemoved = 0
                                          $whereSql
                                          GROUP BY 
                                            prop.nPropertyIDPK
                                          ORDER BY  prop.nPropertyIDPK DESC");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) 
        {
            return $sth->rowCount();
        } else {
            return 0;
        }      
    }

    public function getProperties($loggedId,$userType)
    { 
        $requestData= $_REQUEST;
        // $req = $_REQUEST;

        // print_r($requestData);

        // exit();


        $columns = array( 

            0=> 'prop.nPropertyIDPK'

        );

        $whereSql = '';

        if($userType == ADMIN)
        {
          $whereSql = '';
        }
        else if($userType == AGENT)
        {
          $whereSql = " AND prop.nCreatedBy = ".$loggedId;  
        }
        else if($userType == DEVELOPER)
        {
          $whereSql = " AND prop.nCreatedBy = ".$loggedId;  
        }



        $sql = "SELECT
                    prop.nPropertyIDPK,
                    prop.tPropertyName,
                    prop.nPropertyAddedBy,
                    prop.tPropertyNumber,
                    users.vUserEmailID,
                    users.vUserFirstName,
                    users.vUserLastName,
                    prop.tPropertyAddress,
                    prop.tPropertyReraID,
                    prop.fPropertyBrokerageCharge,
                    type.tPropertyTypeName,
                    type.nPropertyTypeIDPK,
                    prop.bIsProperty,
                    subtype.tPropertySubTypeName,
                    prop.nPropertyFor,
                    prop.nPropertyTotalViews,
                    prop.fPropertyExpectedPrice,
                    prop.fPropertySuperBuiltArea,
                    prop.fPropertyBuiltArea,
                    prop.fPropertyCarpetArea,
                    prop.bIsPropertyVerifiedByAdmin,
                    prop.bIsFeaturedListing,
                    prop.bIsPropertyReraCertified,
                    DATE_FORMAT(prop.dtCreatedOn, '%d-%m-%Y') as dtCreatedOn,
                    prop.nCreatedBy,
                    DATE_FORMAT(prop.dtPropertyExpireOn, '%d-%m-%Y') as dtPropertyExpireOn,
                    area.tAreaName,
                    city.tCityName,
                    state.tStateName       
                FROM
                    tblproperties as prop,
                    tblusers as users,
                    tblpropertytypes as type,
                    tblpropertysubtypes as subtype,
                    tblcities as city,
                    tblareas as area,
                    tblstates as state
                    
                WHERE
                    prop.nPropertyTypeIDFK = type.nPropertyTypeIDPK
                  AND
                    subtype.nPropertyTypeIDFK = type.nPropertyTypeIDPK
                  AND
                    prop.nPropertySubTypeIDFK = subtype.nPropertySubTypeIDPK
                  AND
                    prop.nCreatedBy = users.nUserIDPK
                  AND
                    area.nAreaIDPK = prop.nPropertyAreaIDFK
                  AND
                    city.nCityIDPK = prop.nPropertyCityIDFK
                  AND
                    state.nStateIDPK = prop.nPropertyStateIDFK
                  AND
                    prop.bIsAvailable = 1
                  AND
                    type.bIsPropertyTypeRemoved = 0
                  AND
                    prop.bIsRemoved = 0
                  $whereSql
                  
                ";

        if(!empty($requestData['search']['value']) ) 
        {    

            $sql.=" AND (prop.tPropertyName LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR  prop.tPropertyReraID LIKE '%".$requestData['search']['value']."%' "; 
            $sql.=" OR type.tPropertyTypeName LIKE '%".$requestData['search']['value']."%'"; 
            $sql.=" OR prop.nPropertyFor LIKE '%".$requestData['search']['value']."%'"; 
            $sql.=" OR prop.fPropertyExpectedPrice LIKE '%".$requestData['search']['value']."%'"; 
            $sql.=" OR prop.dtCreatedOn LIKE '%".$requestData['search']['value']."%'"; 
            $sql.=" OR prop.dtPropertyExpireOn LIKE '%".$requestData['search']['value']."%'"; 
            $sql.=" OR subtype.tPropertySubTypeName LIKE '%".$requestData['search']['value']."%' )"; 
        }


        $sth = $this->db->prepare($sql);

        $sth->execute();

        $totalFiltered = $sth->rowCount();
        $sql.="GROUP BY  prop.nPropertyIDPK";
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 

       

        $sth = $this->db->prepare($sql);

        // print_r($sth);
        // exit();
        $sth->execute();
        $totalData = $sth->rowCount();

        
          $i=1+$requestData['start'];

          $data = array();
          while($row = $sth->fetch()) 
          { 
                $images = array();

                $id = $row['nPropertyIDPK'];
                $sth1 = $this->db->prepare("SELECT
                                             propImg.nPropertyImageIDPK,
                                              propImg.nPropertyIDFK,
                                              propImg.nImageLabelIDFK,
                                              propImg.tImageLabelName,
                                              propImg.tPropertyImageFolderPath,
                                              propImg.bIsFrontImage,
                                              propImg.bIsRemoved,
                                              prop.nPropertyIDPK,
                                              prop.tPropertyName

                                          FROM
                                              tblpropertyimages as propImg,
                                              tblproperties as prop
                                         
                                          WHERE
                                            propImg.nPropertyIDFK = prop.nPropertyIDPK AND propImg.nPropertyIDFK = $id");
                $sth1->execute();
                $row1 = $sth1->fetchAll();
                // print_r(count($row1));
                // exit();
               
                for ($img=0; $img < count($row1) ; $img++)
                { 
                    $dataImage = "<div class='mySlides_".$i."'>
                                    <a class='prev' style='cursor: pointer;' onclick='plusSlides(\"mySlides_".$i.",-1\")'>
                                        <i style='font-size: 20px;' class='fa fa-angle-left'></i>
                                    </a>
                                    <img align='center' width='60px' height='60px' src=".Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$row1[$img]['tPropertyImageFolderPath'].">

                                    <a class='next' onclick='plusSlides(\"mySlides_".$i.",1\")' style='cursor: pointer;'>
                                      <i style='font-size: 20px;' class='fa fa-angle-right'></i>
                                    </a>
                                  </div>";

                    array_push($images,$dataImage);
                }


            $id = $row['nPropertyIDPK'];
            $sth2 = $this->db->prepare("SELECT
                                          propLayout.nProjectIDPK,
                                          propLayout.nPropertyIDFK,
                                          propLayout.nProjectSubTypeIDFK,
                                          propLayout.nBHKIDFK,
                                          prop.nPropertyIDPK,
                                          prop.tPropertyName,
                                          propLayout.fProjectSuperBuiltArea,
                                          propLayout.fProjectBuiltArea,
                                          propLayout.fProjectCarpetArea,
                                          propLayout.fConstructionSuperBuiltArea,
                                          propLayout.fConstructionBuiltArea,
                                          propLayout.fConstructionCarpetArea,
                                          propLayout.nUnitIDFK,
                                          propLayout.bIsPropertyFurnished,
                                          propLayout.nBedrooms,
                                          propLayout.nKitchens,
                                          propLayout.nBalconies,
                                          propLayout.nBathrooms,
                                          propLayout.nCarParkings,
                                          propLayout.tLayoutUmages,
                                          propLayout.fProjectExpectedPrice,
                                          propLayout.bIsExpectedPriceNegotiable,
                                          propLayout.fPricePerUnitArea,
                                          propLayout.dtCreatedOn,
                                          propLayout.nCreatedBy,
                                          propLayout.bIsRemoved,
                                          propLayout.dtRemovedOn,
                                          propLayout.nRemovedBy,
                                          propLayout.bIsDummyData
                                      FROM
                                          tblprojectlayouts as propLayout,
                                          tblproperties as prop
                                         
                                          WHERE
                                            propLayout.nPropertyIDFK = prop.nPropertyIDPK 
                                            AND 
                                            propLayout.nPropertyIDFK = $id
                                            AND
                                            prop.bIsRemoved = 0
                                           ");
            // print_r($sth2);
            $sth2->execute();
            $row2 = $sth2->fetchAll();
             if (!empty($row2)) 
             {
                $row['Property_Layout'] = $row2;
             }

            // print_r($row['Property_Layout'][0]['fProjectSuperBuiltArea']);
            // echo "<br>";
            // print_r($row['nPropertyIDPK']);
            // echo "<br>";
            // exit();

            $nestedData=array();

            $str = implode('', $images);
            // ltrim($str,',');
            // print_r($str);
            // exit();
            $createdDate = $row['dtCreatedOn'];

            if ($row['nPropertyFor'] == 1) 
            {
              $row['nPropertyFor'] = "Rent";
            }
            else{
              $row['nPropertyFor'] = "Buy";
            }

            if ($row['tPropertyReraID'] == "") 
            {
              $row['tPropertyReraID'] = " Not Available";
            }

            $userFullName = $row['vUserFirstName'];

            
            $expireDate = date('Y-m-d', strtotime($createdDate. ' + 60 days'));

            if ($row['dtPropertyExpireOn'] == null && $row['dtPropertyExpireOn'] == "")
            {
               $row['dtPropertyExpireOn'] = $expireDate ;
            }
            $PropertyVerified="";
             $propertyVerify = "";
              $propertyFeature = "";
              $dataAction = "";
            if(SessionHandling::get('userType'))
            {

                if(SessionHandling::get('userType') == ADMIN)
                {   
                    if ($row['bIsPropertyVerifiedByAdmin'] == 1) 
                    {
                        
                        $propertyVerify = "<label > Property Verified: </label><br><span style='float:left;padding: 0px !important;'class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger' style='padding:none !important;'>
                                            <label title='Verified'>
                                                <input type='checkbox' checked  name='verified' id='".$row['nPropertyIDPK']."'  onchange=PropertyVerified(".$row['nPropertyIDPK'].",0,'".$row['vUserEmailID']."','".$userFullName."');>
                                                <span ></span>
                                            </label>
                                        </span>
                                        <br>
                                        ";
                       
                    }
                    else{
                        $propertyVerify = "<label> Verify Property : </label><br><span style='padding: 0px !important;' class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                            <label title='Not Verified'>
                                                <input type='checkbox'  name='verified' id='".$row['nPropertyIDPK']."' onchange=PropertyVerified(".$row['nPropertyIDPK'].",1,'".$row['vUserEmailID']."','".$userFullName."');>
                                                <span ></span>
                                            </label>
                                        </span>
                                        ";
                    }
                    if ($row['bIsProperty'] == 1) 
                    {
                     

                        if ($row['bIsFeaturedListing'] == 1) 
                        {
                            $propertyFeature = "<label > Featued Project : </label><br>
                                            <span style='padding: 0px !important;'class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger' style='padding:none !important;'>
                                                <label title='Verified'>
                                                    <input type='checkbox' checked  name='verified' id='".$row['nPropertyIDPK']."'  onchange='PropertyFeature(".$row['nPropertyIDPK'].",0);'>
                                                    <span ></span>
                                                </label>
                                            </span>";
                             
                        }
                        else
                        {
                            $propertyFeature = "
                                        <label> Add Feature Project: </label><br><span style='float:left;padding: 0px !important;' class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                                <label title='Not Verified'>
                                                    <input type='checkbox'  name='verified' id='".$row['nPropertyIDPK']."' onchange='PropertyFeature(".$row['nPropertyIDPK'].",1);'>
                                                    <span ></span>
                                                </label>
                                            </span>";
                        }
                    }
                    
                    $dataAction = "<div class='col-lg-2' style='padding: 0px !important; float:right;padding-right: 15px !important;'>
                                    <div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push' m-dropdown-toggle='hover' aria-expanded='true' style='float: right;'>
                                        <a href='#' class='m-portlet__nav-link btn btn-lg btn-secondary btn-metal m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle' style='width: 35px !important; height: 35px  !important;'>
                                        <i class='la la-plus m--hide'></i>
                                        <i class='la la-ellipsis-h'></i>
                                        </a>
                                        <div class='m-dropdown__wrapper' style='z-index: 101; width:50px;padding-top:10px;'>
                                           
                                            <div class='m-dropdown__inner'   style='height: 90px;padding: 5px; vertical-align: middle;text-align: center;padding-top: 10px !important;'>
                                              <div class='m-dropdown__body'>
                                                <div class='m-dropdown__content'>
                                                  <ul class='m-nav' >
                                                    <li class='m-nav__section m-nav__section--first m--hide'>
                                                      <span class='m-nav__section-text'>Quick Actions</span>
                                                    </li>

                                                    <li class='m-nav__item'>
                                                        <form class='m-form' action='".Urls::$WEBSITE_BASE_URL."property/details' target='_blank' method='post'> 
                                                            <input type='hidden' name='propertyId' id='propertyId' value=".$row['nPropertyIDPK'].">
                                                            <input type='hidden' name='userId' id='userId' value=".$row['nCreatedBy'].">
                                                            <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tPropertyName']." Detail' data-original-title='View ".$row['tPropertyName']." Detail' type='button' class='btn_view btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-eye'></i></button>
                                                        </form>
                                                    </li>
                                                    
                                                    <li class='m-nav__item'>
                                                        <form style='display: inline-table;'>
                                                            <button data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Delete Property' type='button' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' onclick='setId(".$row['nPropertyIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                                                        </form>
                                                    </li>
                                                  </ul>
                                                </div>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>";
                }
                elseif (SessionHandling::get('userType') == AGENT)
                {   
                   
                    if ($row['bIsPropertyVerifiedByAdmin'] == 1) 
                    {
                        $PropertyVerified = "<label style='cursor: default;float:right;font-size: 12px; color:green; margin-right:-70px; ' class='btn m-btn--pill m-btn--air btn-secondary' disabled>Verified</label>";
                    }
                    else
                    {
                        $PropertyVerified = "<label style='cursor: default;font-size: 12px;float:right; margin-right:-70px;color:red; ' class='btn m-btn--pill m-btn--air btn-secondary' disabled>Not Verified</label>";
                    }
                    if ($row['bIsPropertyVerifiedByAdmin'] == 1) 
                    {
                         $dataAction = "<div class='col-lg-2' style='padding: 0px !important; float:right;padding-right: 15px !important;'>
                                                <div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push' m-dropdown-toggle='hover' aria-expanded='true' style='float: right;'>
                                                    <a href='#' class='m-portlet__nav-link btn btn-lg btn-secondary btn-metal m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle' style='width: 35px !important; height: 35px  !important;'>
                                                    <i class='la la-plus m--hide'></i>
                                                    <i class='la la-ellipsis-h'></i>
                                                    </a>
                                                    <div class='m-dropdown__wrapper' style='z-index: 101; width:50px;padding-top:10px;'>
                                                       
                                                        <div class='m-dropdown__inner'   style='height: 120px;padding: 5px; vertical-align: middle;text-align: center;padding-top: 10px !important;'>
                                                          <div class='m-dropdown__body'>
                                                            <div class='m-dropdown__content'>
                                                              <ul class='m-nav' >
                                                                <li class='m-nav__section m-nav__section--first m--hide'>
                                                                  <span class='m-nav__section-text'>Quick Actions</span>
                                                                </li>
                                                                <li class='m-nav__item'>
                                                                    <form class='m-form' action='".Urls::$WEBSITE_BASE_URL."property/details' target='_blank' method='post'> 
                                                                        <input type='hidden' name='propertyId' id='propertyId' value=".$row['nPropertyIDPK'].">
                                                                        <input type='hidden' name='userId' id='userId' value=".$row['nCreatedBy'].">
                                                                        <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tPropertyName']." Detail' data-original-title='View ".$row['tPropertyName']." Detail' type='button' class='btn_view btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-eye'></i></button>
                                                                    </form>
                                                                </li>
                                                                <li class='m-nav__item'>
                                                                    <form method='post' action='".Urls::$BASE."properties/editProperty' style='display: inline-table;'>
                                                                        <input type='hidden' name='listingID' value='".$row['nPropertyIDPK']."'>
                                                                        <button data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Edit Detail' type='button' class='btn btn_edit btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-pencil-alt'></i></button>

                                                                    </form>
                                                                </li>
                                                                <li class='m-nav__item'>
                                                                    <form style='display: inline-table;'>
                                                                        <button  data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Delete Property' type='button' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' onclick='setId(".$row['nPropertyIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                                                                    </form>
                                                                </li>
                                                              </ul>
                                                            </div>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                    }
                    else
                    {
                         $dataAction = "";
                    } 
                    
                }  
                elseif (SessionHandling::get('userType') == DEVELOPER)
                {   
                   
                    if ($row['bIsPropertyVerifiedByAdmin'] == 1) 
                    {
                        $PropertyVerified = "<label style='cursor: default;float:right;font-size: 12px; color:green; margin-right:-70px; ' class='btn m-btn--pill m-btn--air btn-secondary' disabled>Verified</label>";
                    }
                    else
                    {
                        $PropertyVerified = "<label style='cursor: default;font-size: 12px;float:right; margin-right:-70px;color:red; ' class='btn m-btn--pill m-btn--air btn-secondary' disabled>Not Verified</label>";
                    }
                    if ($row['bIsPropertyVerifiedByAdmin'] == 1) 
                    {
                         $dataAction = "<div class='col-lg-2' style='padding: 0px !important; float:right;padding-right: 15px !important;'>
                                                <div class='m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push' m-dropdown-toggle='hover' aria-expanded='true' style='float: right;'>
                                                    <a href='#' class='m-portlet__nav-link btn btn-lg btn-secondary btn-metal m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill m-dropdown__toggle' style='width: 35px !important; height: 35px  !important;'>
                                                    <i class='la la-plus m--hide'></i>
                                                    <i class='la la-ellipsis-h'></i>
                                                    </a>
                                                    <div class='m-dropdown__wrapper' style='z-index: 101; width:50px;padding-top:10px;'>
                                                       
                                                        <div class='m-dropdown__inner'   style='height: 120px;padding: 5px; vertical-align: middle;text-align: center;padding-top: 10px !important;'>
                                                          <div class='m-dropdown__body'>
                                                            <div class='m-dropdown__content'>
                                                              <ul class='m-nav' >
                                                                <li class='m-nav__section m-nav__section--first m--hide'>
                                                                  <span class='m-nav__section-text'>Quick Actions</span>
                                                                </li>

                                                                <li class='m-nav__item'>
                                                                    <form class='m-form' action='".Urls::$WEBSITE_BASE_URL."property/details' target='_blank' method='post'> 
                                                                        <input type='hidden' name='propertyId' id='propertyId' value=".$row['nPropertyIDPK'].">
                                                                        <input type='hidden' name='userId' id='userId' value=".$row['nCreatedBy'].">
                                                                        <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tPropertyName']." Detail' data-original-title='View ".$row['tPropertyName']." Detail' type='button' class='btn_view btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-eye'></i></button>
                                                                    </form>
                                                                </li>
                                                                <li class='m-nav__item'>
                                                                    <form method='post' action='".Urls::$BASE."properties/editProperty' style='display: inline-table;'>
                                                                        <input type='hidden' name='listingID' value='".$row['nPropertyIDPK']."'>
                                                                        <button data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Edit Detail' type='button' class='btn btn_edit btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-pencil-alt'></i></button>

                                                                    </form>
                                                                </li>
                                                                <li class='m-nav__item'>
                                                                    <form style='display: inline-table;'>
                                                                        <button  data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Delete Property' type='button' class='btn btn-outline-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' onclick='setId(".$row['nPropertyIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                                                                    </form>
                                                                </li>
                                                              </ul>
                                                            </div>
                                                          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                    }
                    else
                    {
                         $dataAction = "";
                    } 
                    
                }                                   
            }

           

                              
            $nestedData[] = "<div class='col-lg-12 col-12'>
                                <div class='form-group row' style='margin-top: 10px;'>
                                    <div class='col-lg-8 col-6 col-8 col-10 col-12'>     
                                          <b><label>".$row['tPropertyName']."</label></b>
                                    </div>
                                    <div class='col-lg-2 col-8' style='padding: 0px !important;float:right;'>
                                        ".$PropertyVerified."
                                    </div>
                                    ".$dataAction."
                                    <div class='col-lg-12'>
                                        <label >Price :  &nbsp;".$row['fPropertyExpectedPrice']."&nbsp;</label>
                                        <label > &nbsp;&nbsp;|&nbsp;&nbsp; </label>
                                        <label >Super Built Up Area : &nbsp; ".$row['fPropertySuperBuiltArea']."&nbsp | &nbsp;</label>
                                        <label >Carpet Area : &nbsp; ".$row['fPropertyCarpetArea']."&nbsp;</label>
                                        
                                        &nbsp;&nbsp; &nbsp;&nbsp;<label style='cursor: default;font-size: 12px; padding: 6px 10px 6px 10px !important;' class='btn m-btn--pill m-btn--air btn-secondary' disabled>".$row['tPropertyTypeName']."</label>

                                        &nbsp;&nbsp;<label style='cursor: default;font-size: 12px; padding: 6px 10px 6px 10px !important;' class='btn m-btn--pill m-btn--air btn-secondary'  disabled>".$row['tPropertySubTypeName']."</label>

                                        &nbsp;&nbsp;<label style='cursor: default;font-size: 12px; padding: 6px 10px 6px 10px !important;' class='btn m-btn--pill m-btn--air btn-secondary m-btn m-btn--custom' disabled>".$row['nPropertyFor']."</label>
                                    </div>
                                    <div class='col-lg-12'>
                                        <label >Rera ID :  &nbsp;".$row['tPropertyReraID']."&nbsp;&nbsp
                                    </div>
                                    <div class='col-lg-12'>
                                        <label style='color:green;'>Posted On : &nbsp;&nbsp;".$row['dtCreatedOn']."</label>
                                        <label > &nbsp;&nbsp;| &nbsp;&nbsp;</label>
                                        <label style='color:red;'>Expire On :  &nbsp;&nbsp;".$row['dtPropertyExpireOn']."&nbsp;&nbsp;</label>
                                    </div>

                                    <div class='col-lg-6 col-12 col-6 col-8'>
                                      
                                      <label >&nbsp;&nbsp;<i class='fa fa-eye'></i> &nbsp;:  &nbsp;".$row['nPropertyTotalViews']." &nbsp;&nbsp; </label>
                                    </div>
                                    <div class='col-lg-1 col-8'>

                                    </div>
                                    <div class='col-lg-2 col-8'>
                                    ".$propertyVerify."
                                    </div>
                                    <div class='col-lg-3 col-8'>
                                    ".$propertyFeature."
                                    </div>
                                </div>";

            $data[] = $nestedData;
            $i++;
          }
          $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);  // send data as json format


    }
    
    public function getSinglePropertyDetail($listingID)
    {
      $DUMMY_DATA = DUMMY_DATA;
      $sth = $this->db->prepare("SELECT 
                                      tblproperties.nPropertyIDPK, 
                                      tblproperties.vFolderName,
                                      tblproperties.tPropertyName, 
                                      tblproperties.bIsProperty, 
                                      tblproperties.nPropertyTypeIDFK, 
                                      tblproperties.nPropertySubTypeIDFK, 
                                      tblpropertysubtypes.tPropertySubTypeName,
                                      tblproperties.nPropertyDerivedTypeIDFK, 
                                      tblproperties.nPropertyAddedBy, 
                                      tblproperties.nPropertyFor, 
                                      tblproperties.tPropertyName, 
                                      tblproperties.nCompanyIDFK, 
                                      tblproperties.nLocalityIDFK, 
                                      tblproperties.nPropertyAreaIDFK, 
                                      tblproperties.nPropertyCityIDFK, 
                                      tblproperties.nPropertyStateIDFK, 
                                      tblproperties.nPropertyCountryIDFK, 
                                      tblproperties.dPropertyLatitude, 
                                      tblproperties.dPropertyLongitude, 
                                      tblproperties.tPropertyNumber, 
                                      tblproperties.tPropertyStreetName, 
                                      tblproperties.tPropertyAddress,
                                      tblproperties.tPropertyMapLocationLink, 
                                      tblproperties.fPropertySuperBuiltArea, 
                                      tblproperties.fPropertyBuiltArea, 
                                      tblproperties.fPropertyCarpetArea, 
                                      tblproperties.nUnitIDFK, 
                                      tblproperties.bIsPropertyFurnished, 
                                      tblproperties.nTotalFloors, 
                                      tblproperties.nPropertyOnFloor,
                                      tblproperties.nBedrooms, 
                                      tblproperties.nKitchens,
                                      tblproperties.nBalconies,
                                      tblproperties.nBathrooms, 
                                      tblproperties.bIsStoreRoomAvailable,
                                      tblproperties.bIsPoojaRoomAvailable, 
                                      tblproperties.nBHKIDFK, 
                                      tblproperties.nCarParkings, 
                                      tblproperties.bIsPropertyReraCertified, 
                                      tblproperties.tPropertyReraID,
                                      tblproperties.nPropertyAvailability,
                                      DATE_FORMAT(tblproperties.dtPropertyPossesionDate, '%d-%m-%Y') as dtPropertyPossesionDate,
                                      tblproperties.tPropertyOwnership, 
                                      tblproperties.fPropertyExpectedPrice, 
                                      tblproperties.bIsExpectedPriceNegotiable,
                                      tblproperties.nPropertyBookingPrice, 
                                      tblproperties.fPricePerUnitArea, 
                                      tblproperties.fPropertyMaintenancePrice, 
                                      tblproperties.nPropertyMaintenanceFor,
                                      tblproperties.tPropertyDescription, 
                                      tblproperties.nFacingIDFK, 
                                      tblproperties.fPropertyRoadWidth, 
                                      tblproperties.fPropertyPlotSize, 
                                      tblproperties.bIsPropertyVerifiedByAdmin,
                                      tblproperties.tPropertyBrochurePath, 
                                      tblproperties.fPropertyBrokerageCharge, 
                                      tblproperties.bIsPropertyBrokerageChargeNegotiable, 
                                      tblproperties.bIsPropertyBrokerageChargeIn, 
                                      tblproperties.bIsAvailable, 
                                      tblproperties.bIsFeaturedListing
                                    FROM 
                                      tblproperties,
                                      tblpropertysubtypes
                                    WHERE
                                      tblproperties.nPropertySubTypeIDFK = tblpropertysubtypes.nPropertySubTypeIDPK
                                    AND
                                      tblproperties.bIsRemoved = 0
                                    AND
                                      tblproperties.bIsDummyData =  $DUMMY_DATA
                                    AND 
                                      tblproperties.nPropertyIDPK = $listingID
                                  ");     
        $sth->execute();
        $row = $sth->fetch();
             
        if (!empty($row)) {

              $propertyID = $row['nPropertyIDPK'];

              $getListingImages = $this->getListingImages($propertyID);
              $getListingLayout = $this->getListingLayout($propertyID);
              $getListingAmenities = $this->getListingAmenities($propertyID); 
              if (!empty($getListingImages)) 
              {
                $row['ListingImages'] =  $getListingImages;
               
              }
              else
              {
                 $row['ListingImages'] = [];
              }
              if (!empty($getListingLayout)) 
              {
                $row['ListingDetails'] =  $getListingLayout;
               
              }
              else
              {
                 $row['ListingDetails'] = [];
              }
              if (!empty($getListingAmenities)) 
              {
                $row['ListingAmenities'] =  $getListingAmenities;
               
              }
              else
              {
                 $row['ListingAmenities'] = [];
              }
              // echo "<pre>";
              // print_r($row);
              // exit();
              
              return $row;
        } else {
            return NULL;
        }    
    }

  
    public function getListingImages($propertyID)
    {
      $DUMMY_DATA = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT 
                                      nPropertyImageIDPK, 
                                      nPropertyIDFK,
                                      bIsFrontImage,
                                      tPropertyImageFolderPath
                                    FROM 
                                      tblpropertyimages
                                    WHERE
                                      bIsRemoved = 0
                                    AND 
                                      bIsDummyData = $DUMMY_DATA
                                    AND
                                      nPropertyIDFK = $propertyID
                                  ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getListingLayout($propertyID)
    {
      $DUMMY_DATA = DUMMY_DATA;
        $sth = $this->db->prepare(" SELECT
                                          propLayout.nProjectIDPK,
                                          propLayout.nPropertyIDFK,
                                          propLayout.nProjectSubTypeIDFK,
                                          propLayout.nBHKIDFK,
                                          prop.nPropertyIDPK,
                                          prop.tPropertyName,
                                          propLayout.fProjectSuperBuiltArea,
                                          propLayout.fProjectBuiltArea,
                                          propLayout.fProjectCarpetArea,
                                          propLayout.fConstructionSuperBuiltArea,
                                          propLayout.fConstructionBuiltArea,
                                          propLayout.fConstructionCarpetArea,
                                          propLayout.nUnitIDFK,
                                          propLayout.bIsPropertyFurnished,
                                          propLayout.nBedrooms,
                                          propLayout.nFacingIDFK,
                                          propLayout.nKitchens,
                                          propLayout.nBalconies,
                                          propLayout.nBathrooms,
                                          propLayout.nWashroom,
                                          propLayout.nTotalFloors,
                                          propLayout.nPropertyOnFloor,
                                          propLayout.nPentry,
                                          propLayout.nCarParkings,
                                          propLayout.bIsPoojaRoomAvailable,
                                          propLayout.bIsStoreRoomAvailable,
                                          propLayout.tLayoutUmages,
                                          propLayout.fProjectExpectedPrice,
                                          propLayout.bIsExpectedPriceNegotiable,
                                          propLayout.fPricePerUnitArea,
                                          propLayout.fProjectBookingPrice,
                                          propLayout.bIsPriceShow,
                                          propLayout.dtCreatedOn,
                                          propLayout.nCreatedBy,
                                          propLayout.bIsRemoved,
                                          propLayout.dtRemovedOn,
                                          propLayout.nRemovedBy,
                                          propLayout.bIsDummyData
                                      FROM
                                          tblprojectlayouts as propLayout,
                                          tblproperties as prop
                                      WHERE
                                        propLayout.nPropertyIDFK = prop.nPropertyIDPK 
                                        AND
                                        propLayout.bIsRemoved = 0
                                        AND 
                                        propLayout.nPropertyIDFK = $propertyID
                                  ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
   

    public function getListingAmenities($propertyID)
    {
      $DUMMY_DATA = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT 
                                      nPropertyAmenityIDPK, 
                                      nPropertyIDFK,
                                      nAmenityIDFK
                                    FROM 
                                       tblpropertyamenities
                                    WHERE
                                      bIsRemoved = 0
                                    AND 
                                      bIsDummyData = $DUMMY_DATA
                                    AND
                                      nPropertyIDFK = $propertyID
                                  ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }   
  
    public function getLocations()
    {
      $sth = $this->db->prepare("SELECT 
                                    area.nAreaIDPK,
                                    area.nCityIDFK,
                                    state.nStateIDPK,
                                    state.nCountryIDFK,
                                    area.tAreaName, 
                                    city.tCityName, 
                                    state.tStateName,
                                    area.dAreaLatitude,
                                    area.dAreaLongitude,
                                    locality.tLocalityName,
                                    locality.nLocalityIDPK
                                FROM 
                                    tbllocalities as locality,
                                      tblareas as area, 
                                      tblcities as city, 
                                      tblstates as state 
                                WHERE 
                                  city.nStateIDFK = state.nStateIDPK 
                                  AND 
                                  area.nCityIDFK = city.nCityIDPK 
                                  AND
                                  locality.nAreaIDFK = area.nAreaIDPK
                                ORDER BY area.tAreaName");
      $sth->execute();
      $row = $sth->fetchAll();
      if (!empty($row)) {
          return $row;
      } else {
          return NULL;
      }       
    }

    public function checkListingLimit($userID)
    {
      $sth = $this->db->prepare("SELECT  nAddListingUpto FROM tblusers WHERE nUserIDPK = $userID");
      $sth->execute();
      $row = $sth->fetch();
      if (!empty($row)) {
          return $row;
      } else {
          return NULL;
      }       
    }  
}