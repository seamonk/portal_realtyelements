<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class languages extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/languages/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAlllanguages();
        $this->view->data = $data;

        $this->view->render($name, "Languages", $header, $footer);
    }

    public function createlanguages() // insert languages
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                $login = SessionHandling::get('loggedId');
               
                $field = array('tLanguageName','nCreatedBy');
                $value = array($title, $login);

                $languagesData = array_combine($field, $value);
                // print_r($languagesData);
                // exit();
                $languagesID = $this->model->insert($languagesData, 'tbllanguages');

                if (isset($languagesID) && $languagesID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Languages");
                    header("Location:../languages/");
                } else {
                    SessionHandling::set("suc_msg", "Languages added successfully");
                    header("Location:../languages/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update languages
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            $login = SessionHandling::get('loggedId');

            $id = array('nLanguageIDPK' => $nlanguagesIDPK);

            
                $field = array('tLanguageName');
                $value = array($title);

            $languagesData = array_combine($field, $value);
            // print_r($languagesData);
            // exit();

            $languagesID = $this->model->update($languagesData, 'tbllanguages', $id);

            if (isset($languagesID) && $languagesID > 0) {
                SessionHandling::set("suc_msg", "Languages updated successfully");
                header("Location:../languages/listing");
            } else if (isset($languagesID) && $languagesID == 0) {
                SessionHandling::set("suc_msg", "Languages updated successfully");
                header("Location:../languages/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Languages");
                header("Location:../languages/listing");
            }
        }
    }

    public function listing()  // languages listing
    {
        $name = 'modules/languages/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAlllanguages();
        $this->view->data = $data;

        $this->view->render($name, "Languages Listing", $header, $footer);

    }

    public function editlanguages()   // edit languages
    {
        $name = 'modules/languages/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singlelanguagesData = $this->model->fetchSinglelanguages($languagesID);
        $this->view->singlelanguages = $singlelanguagesData;
         $data = $this->model->getAlllanguages();
        $this->view->data = $data;

        $this->view->render($name, "Update Languages", $header, $footer);
    }
    public function viewlanguages()   // edit languages
    {
         
        extract($_POST);
        $singlelanguagesData = $this->model->fetchSinglelanguages($Pid);
        echo json_encode($singlelanguagesData);

       
    }
    

    public function deletelanguages() // delete languages
    {
        $languagesID = $_POST['id'];

        $id = array('nLanguageIDPK' => $languagesID);

        $field = array('bIsRemoved');
        $value = array(1);

        $languagesData = array_combine($field, $value);

        $res = $this->model->update($languagesData, 'tbllanguages', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Languages deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Languages");
            echo "false";
        }
    }

}