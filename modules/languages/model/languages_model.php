<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class languages_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

   
    public function delete($id, $table, $whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE " . $whereId . " = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getAlllanguages()  // fetch all languagess for datatable
    {
        $sth = $this->db->prepare("SELECT
                                      `nLanguageIDPK`,
                                      `tLanguageName`,
                                      `dtCreatedOn`,
                                      `nCreatedBy`,
                                      `bIsRemoved`,
                                      `dtRemovedOn`,
                                      `nRemovedBy`,
                                      `bIsDummyData`
                                    FROM
                                      `tbllanguages`
                                    WHERE
                                         `bIsRemoved` = 0 AND bIsDummyData = 0
                                   ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function fetchSinglelanguages($id) // fetch single languages for updation
    {
        $sth = $this->db->prepare("SELECT
                                      `nLanguageIDPK`,
                                      `tLanguageName`,
                                      `dtCreatedOn`,
                                      `nCreatedBy`,
                                      `bIsRemoved`,
                                      `dtRemovedOn`,
                                      `nRemovedBy`,
                                      `bIsDummyData`
                                    FROM
                                      `tbllanguages`
                                    WHERE
                                      `bIsRemoved` = 0 AND bIsDummyData = 0 AND `nLanguageIDPK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }




}