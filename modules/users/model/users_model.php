<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class users_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();         
        }
        return 0;
    }


    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE ".$whereId." = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function check_UserType_Exist($usertype)
    {
        $sth = $this->db->prepare("SELECT  `nUserTypeName` FROM `tblusertype` WHERE `nUserTypeName` = '$usertype'");

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $row;
        } 
        else 
        {
            return NULL;
        }
    }

    public function check_UserType_Exist_update($usertype,$id)
    {
        $sth = $this->db->prepare("SELECT  `nUserTypeName` FROM `tblusertype` WHERE `nUserTypeName` = '$usertype' AND `nUserTypeIDPK` != $id");

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $row;
        } 
        else 
        {
            return NULL;
        }
    }

    public function getUserTypes()
    {
        $sth = $this->db->prepare("SELECT `nUserTypeIDPK`, `nUserTypeName` FROM `tblusertype` WHERE `bIsUserTypeRemoved` = 0 AND bIsUserTypeDummyData = 0 ORDER BY `nUserTypeName` ASC");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function singleUserType($id)
    {
        $sth = $this->db->prepare("SELECT `nUserTypeIDPK`, `nUserTypeName` FROM `tblusertype` WHERE `nUserTypeIDPK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getPlaces()
    {
        $sth = $this->db->prepare("SELECT `nPlaceIDPK`, `tPlaceName` FROM `tblplaces` WHERE `bIsPlaceRemoved` = 0 AND `bIsPlaceActive` = 1 AND `nIsPlaceDummyData` = 0");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }      
    }

    public function getUsers()
    {
        $sth = $this->db->prepare("SELECT u.nUserIDPK, u.tUserName, u.vUserEmailID, u.nUserPhoneNumber, u.tUserProfileImgPath, u.bIsActive, ut.nUserTypeName, u.vUserFirstName, u.vUserLastName FROM tblusers as u,tblusertype as ut WHERE  u.bIsUserRemoved = 0 AND u.nIsUserDummyData = 0 AND u.nUserTypeIDFK = ut.nUserTypeIDPK and u.nUserTypeIDFK != 1");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }    
    }

    public function getSingleUsersView($id)
    {
        /*$sth = $this->db->prepare("SELECT u.tUserName, u.vUserEmailID, u.nUserPhoneNumber, u.tUserProfileImgPath, ut.nUserTypeName, u.vUserFirstName, u.vUserLastName,p.tPlaceName, u.vUserMiddleName, u.tUserPassword FROM tblusers as u,tblusertype as ut,tblplaces as p WHERE  u.bIsUserRemoved = 0 AND u.nIsUserDummyData = 0 AND u.nUserTypeIDFK = ut.nUserTypeIDPK AND u.nPlaceIDFK = p.nPlaceIDPK AND u.nUserIDPK = $id");*/


        $sth = $this->db->prepare("SELECT u.tUserName, u.vUserEmailID, u.nUserPhoneNumber, u.tUserProfileImgPath, ut.nUserTypeName, u.vUserFirstName, u.vUserLastName,u.vUserMiddleName, u.tUserPassword FROM tblusers as u LEFT JOIN tblusertype as ut ON u.`nUserTypeIDFK` = ut.`nUserTypeIDPK`  where u.nUserIDPK = $id");

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }   
    }

    public function getSingleUsers($id)
    {
        $sth = $this->db->prepare("SELECT `nUserIDPK`, `nUserTypeIDFK`,`tUserName`, `vUserEmailID`, `tUserPassword`, `vUserFirstName`, `vUserMiddleName`, `vUserLastName`, `nUserPhoneNumber`, `tUserProfileImgPath` FROM `tblusers` WHERE `nUserIDPK` = $id");
        
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }   
    }
}