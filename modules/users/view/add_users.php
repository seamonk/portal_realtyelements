<style type="text/css">
    
    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    label {
    font-weight: 400 !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    <i class="fa fa-user-tie"></i> 
                                     <?php  if (!empty($this->data)) { ?> Update User <?php }else{ ?> Add User<?php }?>
                            </h3>
                            </div>
                            <a href="<?php echo Urls::$BASE; ?>users/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Users Listing">
                                <i class="fa fa-list"></i>
                                <!--                        <i class="la la-ellipsis-h"></i>-->
                            </a>
                               
                        </div>
                    </div>
                    <!-- END: Subheader -->
                    <div class="m-portlet__body">
                        <?php  
                        if (!empty($this->data)) 
                        {?>
                            <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>users/createUser" enctype = "multipart/form-data" id="update_form" name="update_form">
                                <input type="hidden" name="userID" value="<?php echo $this->data[0]['nUserIDPK']; ?>">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label>Select Profile Image</label>
                                        <span class="" onclick="document.getElementById('User-profile').click();">
                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                <input style="display:none;" type="file" name="User-profile" id="User-profile" onchange="user_image_preview(this)" />

                                                <div align="center" id="user-display-preview">
                                                    <?php
                                                    if($this->data[0]['tUserProfileImgPath'] == '')
                                                    {?>
                                                        <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                    <?php
                                                    }
                                                    else
                                                    {?>
                                                        <img style="height:100%;width:100%;" class="vpb_image_style" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$this->data[0]['tUserProfileImgPath'];
                                                         ?>" />
                                                    <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>Select User Type</label>
                                                <select class="form-control m-select2" id="uUserType" name="uUserType" style="width: 100% !important" >
                                                    <option value="">Select User Type</option>
                                                    <?php
                                                        if (!empty($this->getUserTypes)) 
                                                        {
                                                            for ($i = 0; $i < count($this->getUserTypes); $i++) 
                                                            {
                                                                if($this->data[0]['nUserTypeIDFK'] == $this->getUserTypes[$i]['nUserTypeIDPK'])
                                                                {
                                                                ?>
                                                                    <option value="<?php echo $this->getUserTypes[$i]['nUserTypeIDPK']; ?>" selected><?php echo $this->getUserTypes[$i]['nUserTypeName']; ?></option>
                                                                <?php
                                                                }
                                                                else
                                                                {?>
                                                                    <option value="<?php echo $this->getUserTypes[$i]['nUserTypeIDPK']; ?>" ><?php echo $this->getUserTypes[$i]['nUserTypeName']; ?></option>
                                                                <?php
                                                                } 
                                                            }
                                                        }
                                                    ?>    
                                                </select>
                                            </div>                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>User Name</label>
                                                <input type="text" class="form-control m-input" name="uUserName" placeholder="User Name" value="<?php echo $this->data[0]['tUserName']; ?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>First Name</label>
                                                <input type="text" class="form-control m-input" name="uFirstName" placeholder="First Name" id="uFirstName" value="<?php echo $this->data[0]['vUserFirstName']; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Middle Name</label>
                                                <input type="text" class="form-control m-input" name="uMiddleName" placeholder="Middle Name" id="uMiddleName" value="<?php echo $this->data[0]['vUserMiddleName']; ?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Last Name</label>
                                                <input class="form-control m-input" id="uLastName" name="uLastName" value="<?php echo $this->data[0]['vUserLastName']; ?>"  placeholder="Last Name" type="text" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Phone Number</label>
                                                <input type="text" class="form-control m-input" name="uPhoneNumber" placeholder="Phone Number" id="uPhoneNumber" onkeypress="return isNumberKey(event);" value="<?php echo $this->data[0]['nUserPhoneNumber']; ?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Email Id</label>
                                                <input type="text" class="form-control m-input" name="uEmailId" placeholder="Email Id" id="uEmailId" value="<?php echo $this->data[0]['vUserEmailID']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update_users" id="update_users">Update User</button>
                                    </div>
                                    <div class="col-4">
                                        <button type="reset" class="btn btn-secondary btn-block">Cancel</button>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </form>

                        <?php  
                        }
                        else
                        {?> 
                            <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>users/createUser" enctype = "multipart/form-data" id="add_form" name="add_form">
                                <div class="form-group  row">
                                    <div class="col-lg-4">
                                        <label>Select Profile Image</label>
                                        <span class="" onclick="document.getElementById('User-profile').click();">
                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                <input style="display:none;" type="file" name="User-profile" id="User-profile" onchange="user_image_preview(this)" />

                                                
                                                <div align="center" id="user-display-preview">
                                                    <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>Select User Type</label>
                                                <select class="form-control m-select2" id="uUserType" name="uUserType" style="width: 100% !important" >
                                                    <option value="">Select User Type</option>
                                                   <?php
                                                    if (!empty($this->getUserTypes)) {
                                                        for ($i = 0; $i < count($this->getUserTypes); $i++) {
                                                            ?>
                                                        <option value="<?php echo $this->getUserTypes[$i]['nUserTypeIDPK']; ?>"><?php echo $this->getUserTypes[$i]['nUserTypeName']; ?></option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>   
                                                </select>
                                            </div>                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>User Name</label>
                                                <input type="text" class="form-control m-input" name="uUserName" placeholder="User Name">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>First Name</label>
                                                <input type="text" class="form-control m-input" name="uFirstName" placeholder="First Name" id="uFirstName">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Middle Name</label>
                                                <input type="text" class="form-control m-input" name="uMiddleName" placeholder="Middle Name" id="uMiddleName">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Last Name</label>
                                                <input class="form-control m-input" id="uLastName" name="uLastName"  placeholder="Last Name" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" style="padding-top: 0px;">
                                    <div class="col-lg-4">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control m-input" name="uPhoneNumber" placeholder="Phone Number" id="uPhoneNumber" onkeypress="return isNumberKey(event);">
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Email Id</label>
                                        <input type="text" class="form-control m-input" name="uEmailId" placeholder="Email Id" id="uEmailId">
                                    </div>
                                    <div class="col-lg-4">
                                        <label>Password</label>
                                        <input type="password" class="form-control m-input" name="uPassword" placeholder="Password" id="uPassword">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-4">
                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">Add User</button>
                                    </div>
                                    <div class="col-4">
                                        <button type="reset" class="btn btn-secondary btn-block">Cancel</button>
                                    </div>
                                    <div class="col-lg-2"></div>
                                </div>
                            </form>
                        <?php
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Body -->
<script type="text/javascript">
    $(document).ready(function() 
    {
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $('#uUserType').select2();
        $('#uPlaceID').select2();

        $("#add_form").validate({
            rules: {
                uUserType: {
                    required: true
                },
                uUserName: {
                    required: true
                },
                uFirstName: {
                    required: true
                },
                uMiddleName: {
                    required: true
                },
                uLastName: {
                    required: true
                },
                uPhoneNumber: {
                    required: true
                },
                uEmailId: {
                    required: true
                },
                uPassword: {
                    required: true
                }
                
            }
        });
        $("#update_form").validate({
            rules: {
                uUserType: {
                    required: true
                },
                uUserName: {
                    required: true
                },
                uFirstName: {
                    required: true
                },
                uMiddleName: {
                    required: true
                },
                uLastName: {
                    required: true
                },
                uPhoneNumber: {
                    required: true
                },
                uEmailId: {
                    required: true
                },
                uPassword: {
                    required: true
                }
                
            }
        });
    });


    function user_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#user-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                       $('#user-display-preview').append(
                       ' \
                       <img style="height:100%;width:100%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });
    }

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
</script>
   

