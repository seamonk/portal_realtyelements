<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    table.dataTable td {
      white-space: nowrap;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="fa fa-user"></i> Users Listing</h3>
                            </div>
                            
                          <!--   <a href="<?php echo Urls::$BASE; ?>users/add_user" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Add Users">
                                <i class="fa fa-plus"></i>
                                
                            </a> -->
                            
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>User Type</th>
                                        <th>Name </th>
                                        <!-- <th>User Name</th> -->
                                        <th>Phone Number</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>   
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($this->data)) 
                                    {
                                        for ($i = 0; $i < count($this->data); $i++) 
                                        {
                                            ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td> 
                                            
                                            <td style="text-align: center;">
                                                <?php
                                                    if (!empty($this->data[$i]['tUserProfileImgPath'])) 
                                                    {
                                                        ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$this->data[$i]['tUserProfileImgPath']; ?>"
                                                                style="height: 4em;width: 4em; box-shadow: 1px 0px 9px #060606;"> 

                                                        <?php 
                                                    }
                                                    else
                                                    {
                                                         ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER."user.jpg"; ?>"
                                                                style="height: 4em;width: 4em; box-shadow: 1px 0px 9px #060606;"> 

                                                        <?php 
                                                    }
                                                ?>
                                            </td>
                                            <td><?php echo $this->data[$i]['nUserTypeName']; ?></td>
                                            <td><?php echo $this->data[$i]['vUserFirstName'].' '.$this->data[$i]['vUserLastName']; ?></td>
                                            <!-- <td><?php echo $this->data[$i]['tUserName']; ?></td> -->
                                            <td><?php echo $this->data[$i]['nUserPhoneNumber']; ?></td>
                                            <td><?php echo $this->data[$i]['vUserEmailID']; ?></td>
                                            <td style="text-align: center;">
                                                <?php
                                                if(SessionHandling::get('loggedId') == $this->data[$i]['nUserIDPK'])
                                                {
                                                    echo '<span style="color:green;" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="LoggedIn"><i class="fa fa-user-check"></i></span>';
                                                }
                                                else
                                                {?>
                                                    <span class="m-switch m-switch--outline m-switch--icon m-switch--danger">
                                                    <label>
                                                        <?php

                                                        if($this->data[$i]['bIsActive'] == 1)
                                                        {?>

                                                            <input type="checkbox" checked="checked" name="uUserActive_<?php echo $this->data[$i]['nUserIDPK']; ?>" id="uUserActive_<?php echo $this->data[$i]['nUserIDPK']; ?>" onclick="UserActiveInactive('<?php echo $this->data[$i]['nUserIDPK']; ?>',0)">
                                                        <?php
                                                        }
                                                        else
                                                        {?>
                                                            <input type="checkbox" name="uUserActive_<?php echo $this->data[$i]['nUserIDPK']; ?>" id="uUserActive_<?php echo $this->data[$i]['nUserIDPK']; ?>" onclick="UserActiveInactive('<?php echo $this->data[$i]['nUserIDPK']; ?>',1)">
                                                        <?php
                                                        }

                                                        ?>
                                                        <span ></span>
                                                    </label>
                                                </span>
                                                <?php  
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <form method="post" action="<?php echo Urls::$BASE; ?>users/view_users" style="display: inline-table;">
                                                    <input type="hidden" name="userID" value="<?php echo $this->data[$i]['nUserIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="View <?php echo $this->data[$i]['tUserName']; ?> Detail" type="button" class="btn_view btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa  fa-eye"></i></button>
                                                </form>

                                                <!-- <form method="post" action="<?php echo Urls::$BASE; ?>users/edit_users" style="display: inline-table;">
                                                    <input type="hidden" name="userID" value="<?php echo $this->data[$i]['nUserIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit <?php echo $this->data[$i]['tUserName']; ?> Detail" type="button" class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                                </form> -->

                                                <form style="display: inline-table;">
                                                    <?php 
                                                     if(SessionHandling::get('userType') != ADMIN)
                                                    {?>
                                                        <button  data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete User" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" disabled><i class="fa fa-trash-alt"></i></button>
                                                    <?php
                                                    }
                                                    else
                                                    {?>
                                                        <button  data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete User" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick="setId(<?php echo $this->data[$i]['nUserIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                    <?php
                                                    }?>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                    }
                                    ?>   
                                </tbody>
                            </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() {

        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 
    });
    
    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });  

    $(document).on('click',".btn_view", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });    

    function setId (del_id)
    {
        swal({
        title:"Are you sure?",
        text:"You want to delete this User ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>users/delete_users",
                    data:{'id':del_id},
                    success: function(data)
                    {     
                      if(data == "true")
                      {
                        location.href="<?php echo Urls::$BASE ?>users/";
                      }               
                    }
                });
            }   
       });
    }  

    function UserActiveInactive(id,status)
    {
        if(status == 1)
        {
            var active = "You want to Active this User ?";
        }
        else
        {
            var active = "You want to Inactive this User ?";
        }

        swal({
        title:"Are you sure?",
        text:active,
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes",
        cancelButtonText:"No",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>users/active_inactive_users",
                    data:{'id':id,'status':status},
                    success: function(data)
                    {  
                      if(data == "true")
                      {
                        location.href="<?php echo Urls::$BASE ?>users/";
                      }               
                    }
                });
            } 
            else
            {
                if(status == 1)
                {
                    document.getElementById("uUserActive_"+id).checked = false;
                }
                else
                {
                    document.getElementById("uUserActive_"+id).checked = true;
                } 
            }
       });
    }
</script>
