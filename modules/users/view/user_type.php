<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">

        <!-- <div class="row">
            <div class="col-lg-12"> 
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-app"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleCategories))
                                {
                                    echo "Update User Type";
                                }
                                else
                                {
                                    echo "Add User Type";
                                }?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                               
                                <?php
                                if (!empty($this->singleUserType))
                                {
                                    for ($i = 0; $i < count($this->singleUserType); $i++)
                                    {
                                        ?>
                                        <form class="m-form" action="<?php echo Urls::$BASE; ?>users/createUserType" method="POST">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <input type="hidden" name="userTypeID" id="userTypeID" value="<?php echo $this->singleUserType[$i]['nUserTypeIDPK']; ?>">
                                                            <div class="col-lg-12">
                                                                <label>User Type</label>
                                                                <input type="text" class="form-control m-input" value="<?php echo $this->singleUserType[$i]['nUserTypeName']; ?>" name="userType" id="userType">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update</button>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="reset" class="btn default btn-block"  onclick="window.location = '<?php echo Urls::$BASE; ?>users/user_type';">Cancel</button>
                                                    </div>
                                                    <div class="col-lg-2"></div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                }
                                else
                                {?>
                                    <form class="m-form" action="<?php echo Urls::$BASE; ?>users/createUserType" method="POST">
                                    
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>User Type</label>
                                                        <input type="text" class="form-control m-input" name="userType" id="userType">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">Add User Type</button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block" >Cancel</button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                      
                                    </form>
                                    <?php
                                }?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="fa fa-user-tie"></i> Users Types</h3>
                            </div>
                                                       
                        </div>
                    </div>

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Types</th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($this->data))
                                {
                                    for ($i = 0; $i < count($this->data); $i++)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td>
                                            <td><?php echo $this->data[$i]['nUserTypeName']; ?></td>
                                           
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() 
    {
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
            ]
        } );

        $("#submit").click(function(e) 
        {
            if($('#userType').val() == '')
            {
                toastr.error("Please enter user type.", "User Type");
                e.preventDefault();
            }
            var usertype = $('#userType').val();

            $.ajax({
                type: "POST",
                url: "<?php echo Urls::$BASE ?>users/check_usertype",
                data:{'usertype':usertype,'type':'submit'},
                async : false,
                success: function(data)
                {     
                    //console.log(data);
                    if(data == 'true')
                    {
                        toastr.error("User Type already exist.", "User Type");
                        e.preventDefault();
                    }         
                }
            });
        });

        $("#update").click(function(e) 
        {
            if($('#userType').val() == '')
            {
                toastr.error("Please enter user type.", "User Type");
                e.preventDefault();
            }

            var usertype = $('#userType').val();
            var usertypeID = $('#userTypeID').val();
            
            $.ajax({
                type: "POST",
                url: "<?php echo Urls::$BASE ?>users/check_usertype",
                data:{'usertype':usertype,'type':'update','id':usertypeID},
                async : false,
                success: function(data)
                {     
                    //console.log(data);
                    if(data == 'true')
                    {
                        toastr.error("User Type already exist.", "User Type");
                        e.preventDefault();
                    }         
                }
            });
        });

    });

    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });

    function setId (del_id)
    {
        swal({
            title:"Are you sure?",
            text:"You want to delete this User Type ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes, delete it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>users/delete_user_type",
                    data:{'id':del_id},
                    success: function(data)
                    {
                        if(data == "true")
                        {
                            location.href="<?php echo Urls::$BASE ?>users/user_type/";
                        }
                    }
                });
            }
        });
    }
</script>
