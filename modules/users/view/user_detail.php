<style type="text/css">
    
    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;ssssss
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}
       label {
    font-weight: 400 !important;
}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    <i class="fa fa-user"></i> 
                                    User Detail
                            </h3>
                            </div>
                            <a href="<?php echo Urls::$BASE; ?>users/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Users Listing">
                                <i class="fa fa-list"></i>
                                <!--                        <i class="la la-ellipsis-h"></i>-->
                            </a>
                               
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <?php  
                        if (!empty($this->data)) 
                        {?> 
                            <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>users/createUser" enctype = "multipart/form-data" id="add_form" name="add_form">
                                <div class="form-group m-form__group row">
                                    <div class="col-lg-4">
                                        <label>Profile Image</label>
                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                            <div align="center" id="user-display-preview">
                                                <?php
                                                    if (!empty($this->data[0]['tUserProfileImgPath'])) 
                                                    {
                                                        ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$this->data[0]['tUserProfileImgPath']; ?>"
                                                                style="height:100%;width:100%; box-shadow: 1px 0px 9px #060606;"> 

                                                        <?php 
                                                    }
                                                    else
                                                    {
                                                         ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER."agentimage.jpg"; ?>"
                                                                style="height:100%;width:100%; box-shadow: 1px 0px 9px #060606;"> 

                                                        <?php 
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label>User Type</label>
                                                <input type="text" class="form-control m-input" name="uUserType" placeholder="User Type" disabled value="<?php echo $this->data[0]['nUserTypeName']; ?>">
                                            </div>                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>User Name</label>
                                                <input type="text" class="form-control m-input" name="uUserName" placeholder="User Name" disabled value="<?php echo $this->data[0]['tUserName']; ?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>First Name</label>
                                                <input type="text" class="form-control m-input" name="uFirstName" placeholder="First Name" id="uFirstName" disabled value="<?php echo $this->data[0]['vUserFirstName']; ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Middle Name</label>
                                                <input type="text" class="form-control m-input" name="uMiddleName" placeholder="Middle Name" id="uMiddleName" disabled value="<?php echo $this->data[0]['vUserMiddleName']; ?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Last Name</label>
                                                <input class="form-control m-input" id="uLastName" name="uLastName"  placeholder="Last Name" type="text" disabled value="<?php echo $this->data[0]['vUserLastName']; ?>" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Phone Number</label>
                                                <input type="text" class="form-control m-input" name="uPhoneNumber" placeholder="Phone Number" id="uPhoneNumber" onkeypress="return isNumberKey(event);" disabled value="<?php echo $this->data[0]['nUserPhoneNumber']; ?>">
                                            </div>
                                            <div class="col-lg-6">
                                                <label>Email Id</label>
                                                <input type="text" class="form-control m-input" name="uEmailId" placeholder="Email Id" id="uEmailId" disabled value="<?php echo $this->data[0]['vUserEmailID']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Body -->


