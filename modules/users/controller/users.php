<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Users extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
//        Session::init();
    }

    public function index()
    {
        $name = 'modules/users/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getUsers();
        $this->view->data = $data;

        $this->view->render($name,"User Listing", $header, $footer);
    }

    public function add_user()
    {
        $name = 'modules/users/view/add_users.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $getUserTypes = $this->model->getUserTypes();
        $this->view->getUserTypes = $getUserTypes;

        $getPlaces = $this->model->getPlaces();
        $this->view->getPlaces = $getPlaces;

        $this->view->render($name,"Add Users", $header, $footer);
    }

    public function user_type()
    {
        $name = 'modules/users/view/user_type.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getUserTypes();
        $this->view->data = $data;

        $this->view->render($name,"User Type", $header, $footer);
    }

    public function edit_user_type()
    {
        $name = 'modules/users/view/user_type.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleUserType = $this->model->singleUserType($editUserTypeID);
        $this->view->singleUserType = $singleUserType;

        $data = $this->model->getUserTypes();
        $this->view->data = $data;

        $this->view->render($name,"Places Categories", $header, $footer);
    }

    public function createUserType()
    {
        $loggedId = SessionHandling::get("loggedId");

        if($_POST['submit'] == "submit")
        {
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);

                $field = array('nUserTypeName','dUserTypeCreatedBy');

                $value = array($userType,$loggedId);

                $user_type_data = array_combine($field, $value);

                $user_type_id = $this->model->insert($user_type_data, 'tblusertype');

                if(isset($user_type_id) && $user_type_id == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding User Type");
                    header("Location:user_type/");
                }
                else
                {
                    SessionHandling::set("suc_msg", "User Type added successully");
                    header("Location:user_type/");
                }
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);

            $id = array('nUserTypeIDPK' => $userTypeID);

            $field = array('nUserTypeName');

            $value = array($userType);

            $user_type_data = array_combine($field, $value);

            $user_type_id = $this->model->update($user_type_data, 'tblusertype',$id);

            if($user_type_id > 0)
            {
                SessionHandling::set("suc_msg", "User Type updated successully");
                header("Location:user_type/");
            }
            else if($user_type_id == 0)
            {
                SessionHandling::set("suc_msg", "User Type updated successully");
                header("Location:user_type/");
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating User Type");
                header("Location:user_type/");
            }
        }
    }

    public function check_usertype()
    {
        $usertype = $_POST['usertype'];
        

        if ($_POST['type'] == 'submit') 
        {
            $result = $this->model->check_UserType_Exist($usertype);
        }
        else if($_POST['type'] == 'update')
        {

            //print_r($_REQUEST);
            $id = $_POST['id'];
            $result = $this->model->check_UserType_Exist_update($usertype,$id);
        }

        if($result)
        {
            echo "true";
        }
        else
        {
            echo "false";
        }
    }

    public function delete_user_type()
    {
        $user_type_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nUserTypeIDPK' => $user_type_id);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsUserTypeRemoved','dtUserTypeRemovedOn','nUserTypeRemovedBy');
        $value = array(1,$currentDate,$loggedId);
        $user_type_data = array_combine($field, $value);

        $res = $this->model->update($user_type_data, 'tblusertype',$id);

        if($res > 0)
        {
            SessionHandling::set("suc_msg", "User Type deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting User Type");
            echo "false";
        }
    }

   public function createUser()
    {

        $loggedId = SessionHandling::get("loggedId");

        $preImgText = date('ymdhsi');

        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);   

                $Folder_name = Urls::$IMG_UPLOADS_USER;
                
                if($_FILES['User-profile']['name'] != '')
                {
                    $user_img_folder = $preImgText.$_FILES['User-profile']['name'];

                    $targetPath1 = $Folder_name.'/'.$preImgText.$_FILES['User-profile']['name'];
                 
                    move_uploaded_file($_FILES['User-profile']["tmp_name"], $targetPath1);
                }
                else
                {
                    $user_img_folder = 'user.png';
                }
                
                $field = array('nUserTypeIDFK','tUserName', 'vUserFirstName', 'vUserMiddleName', 'vUserLastName', 'nUserPhoneNumber','vUserEmailID','tUserPassword','tUserProfileImgPath','nUserCreatedBy');

                $value = array($uUserType,$uUserName,$uFirstName,$uMiddleName,$uLastName, $uPhoneNumber,$uEmailId,$uPassword,$user_img_folder,$loggedId);

                $user_data = array_combine($field, $value);

                $user_id = $this->model->insert($user_data, 'tblusers');

                if(isset($user_id) && $user_id == 0)
                {
                    $field = array('nUserIDFK');

                    $value = array($user_id);

                    $agentData = array_combine($field, $value);

                    $agentId = $this->model->insert($agentData, 'tblagents');

                    SessionHandling::set("err_msg", "Error while adding User");
                    header("Location:./");
                }
                else
                {
                    SessionHandling::set("suc_msg", "User added successully");
                    header("Location:./");
                }              
            }
        }
        else if($_POST['submit'] == "update_users")
        {
            extract($_POST);

            $id = array('nUserIDPK' => $userID);

            $Folder_name = Urls::$IMG_UPLOADS_USER;
                
            if($_FILES['User-profile']['name'] != '')
            {
                $user_img_folder = $preImgText.$_FILES['User-profile']['name'];

                $targetPath1 = $Folder_name.'/'.$preImgText.$_FILES['User-profile']['name'];
             
                move_uploaded_file($_FILES['User-profile']["tmp_name"], $targetPath1);

                $field = array('nUserTypeIDFK','tUserName', 'vUserFirstName', 'vUserMiddleName', 'vUserLastName', 'nUserPhoneNumber','vUserEmailID','tUserProfileImgPath');

                $value = array($uUserType,$uUserName,$uFirstName,$uMiddleName,$uLastName, $uPhoneNumber,$uEmailId,$user_img_folder);
            }
            else
            {
                $field = array('nUserTypeIDFK','tUserName', 'vUserFirstName', 'vUserMiddleName', 'vUserLastName', 'nUserPhoneNumber','vUserEmailID');

                $value = array($uUserType,$uUserName,$uFirstName,$uMiddleName,$uLastName, $uPhoneNumber,$uEmailId);
            }
            $user_data = array_combine($field, $value);

            $user_id = $this->model->update($user_data, 'tblusers',$id);

            if($user_id > 0)
            {
                SessionHandling::set("suc_msg", "User updated successully");
                header("Location:./");
            }
            else if($user_id == 0)
            {
                SessionHandling::set("suc_msg", "User updated successully");
                header("Location:./");
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating User");
                header("Location:./");
            }
        }
    }

    public function active_inactive_users()
    {
        $userId = $_POST['id'];
        $userStatus = $_POST['status'];

        $id = array('nUserIDPK' => $userId);

        $field = array('bIsActive');
        $value = array($userStatus);

        $user_data = array_combine($field, $value);

        $res = $this->model->update($user_data, 'tblusers',$id);

        if($res > 0)
        {
            SessionHandling::set("suc_msg", "User status change successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while change User status");
            echo "false";
        }
    }

    public function delete_users()
    {
        $userId = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nUserIDPK' => $userId);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsUserRemoved','dtUserRemovedOn','nUserRemovedBy');
        $value = array(1,$currentDate,$loggedId);
        $user_data = array_combine($field, $value);

        $res = $this->model->update($user_data, 'tblusers',$id);

        if($res > 0)
        {
            SessionHandling::set("suc_msg", "User deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting User");
            echo "false";
        }
    }

    public function view_users()
    {
        $userID = $_POST['userID'];

        $name = 'modules/users/view/user_detail.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getSingleUsersView($userID);
        $this->view->data = $data;

        $this->view->render($name,"Users Detail", $header, $footer);
    }

    public function edit_users()
    {
        $id = $_POST['userID'];

        $name = 'modules/users/view/add_users.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getSingleUsers($id);
        $this->view->data = $data;

        $getUserTypes = $this->model->getUserTypes();
        $this->view->getUserTypes = $getUserTypes;
  

        $this->view->render($name,"Update User", $header, $footer);
    }
}