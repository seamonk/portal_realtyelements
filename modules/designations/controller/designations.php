<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Designations extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/designations/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAlldesignations();
        $this->view->data = $data;

        $this->view->render($name, "designations", $header, $footer);
    }

    public function createdesignations() // insert designations
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tDesignationName');
                $value = array($title); 

                $designationsData = array_combine($field, $value);
                // print_r($designationsData);
                // exit();
                $designationsID = $this->model->insert($designationsData, 'tbldesignations');

                if (isset($designationsID) && $designationsID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Designation");
                    header("Location:../designations/");
                } else {
                    SessionHandling::set("suc_msg", "Designation added successfully");
                    header("Location:../designations/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update designations
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nDesignationIDPK' => $ndesignationsIDPK);

            
                $field = array('tDesignationName');
                $value = array($title);

            $designationsData = array_combine($field, $value);
            // print_r($designationsData);
            // exit();

            $designationsID = $this->model->update($designationsData, 'tbldesignations', $id);

            if (isset($designationsID) && $designationsID > 0) {
                SessionHandling::set("suc_msg", "Designation updated successfully");
                header("Location:../designations/listing");
            } else if (isset($designationsID) && $designationsID == 0) {
                SessionHandling::set("suc_msg", "Designation updated successfully");
                header("Location:../designations/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Designation");
                header("Location:../designations/listing");
            }
        }
    }

    public function listing()  // designations listing
    {
        $name = 'modules/designations/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAlldesignations();
        $this->view->data = $data;

        $this->view->render($name, "designations Listing", $header, $footer);

    }

    public function editdesignations()   // edit designations
    {
        $name = 'modules/designations/view/designations.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singledesignationsData = $this->model->fetchSingledesignations($designationsID);
        $this->view->singledesignations = $singledesignationsData;
         $data = $this->model->getAlldesignations();
        $this->view->data = $data;

        $this->view->render($name, "Update designations", $header, $footer);
    }
    public function viewdesignations()   // edit designations
    {
         
        extract($_POST);
        $singledesignationsData = $this->model->fetchSingledesignations($Pid);
        echo json_encode($singledesignationsData);

       
    }
    

    public function deletedesignations() // delete designations
    {
        $designationsID = $_POST['id'];

        $id = array('nDesignationIDPK' => $designationsID);

        $field = array('bIsRemoved');
        $value = array(1);

        $designationsData = array_combine($field, $value);

        $res = $this->model->update($designationsData, 'tbldesignations', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "designations deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting designations");
            echo "false";
        }
    }

}