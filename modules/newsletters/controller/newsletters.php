<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class newsLetters extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/newsletters/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllnewsletters();
        $this->view->data = $data;

        $this->view->render($name, "NewsLetter", $header, $footer);
    }

    public function createNewsLetters() // insert newsletters
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tNewsLetterEmailID');
                $value = array($title); 

                $companiesData = array_combine($field, $value);
                // print_r($companiesData);
                // exit();
                $companiesID = $this->model->insert($companiesData, 'tblnewsletters');

                if (isset($companiesID) && $companiesID == 0) {
                    SessionHandling::set("err_msg", "Error while adding NewsLetter");
                    header("Location:../newsletters/");
                } else {
                    SessionHandling::set("suc_msg", "NewsLetter added successfully");
                    header("Location:../newsletters/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update newsletters
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nNewsLettersIDPK' => $nNewsLettersIDPK);

            
                $field = array('tNewsLetterEmailID');
                $value = array($title); 

                $companiesData = array_combine($field, $value);
                // print_r($companiesData);
                // exit();
                $companiesID = $this->model->update($companiesData, 'tblnewsletters',$id);


            if (isset($companiesID) && $companiesID > 0) {
                SessionHandling::set("suc_msg", "NewsLetter updated successfully");
                header("Location:../newsletters/");
            } else if (isset($companiesID) && $companiesID == 0) {
                SessionHandling::set("suc_msg", "NewsLetter updated successfully");
                header("Location:../newsletters/");
            } else {
                SessionHandling::set("err_msg", "Error while updating NewsLetter");
                header("Location:../newsletters/");
            }
        }
    }

    public function listing()  // newsletters listing
    {
        $name = 'modules/newsletters/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllcompanies();
        $this->view->data = $data;

        $this->view->render($name, "NewsLetter Listing", $header, $footer);

    }

    public function editNewsLetters()   // edit newsletters
    {
        $name = 'modules/newsletters/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singlecompaniesData = $this->model->fetchSinglenewsletters($newsLettersID);
        $this->view->singlecompanies = $singlecompaniesData;
         $data = $this->model->getAllnewsletters();
        $this->view->data = $data;

        $this->view->render($name, "Update NewsLetter", $header, $footer);
    }
    public function viewewsLetters()   // edit newsletters
    {
         
        extract($_POST);
        $singlecompaniesData = $this->model->fetchSinglecompanies($Pid);
        echo json_encode($singlecompaniesData);

       
    }
    

    public function deleteNewsLetters() // delete newsletters
    {
        $nNewsLetterIDPK = $_POST['id'];

        $id = array('nNewsLettersIDPK' => $nNewsLetterIDPK);

        $field = array('bIsNewsLetterRemoved');
        $value = array(1);

        $companiesData = array_combine($field, $value);
        
        $res = $this->model->update($companiesData, 'tblnewsletters', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "NewsLetter deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting NewsLetter");
            echo "false";
        }
    }

}