<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class newsletters_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

   
    public function delete($id, $table, $whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE " . $whereId . " = $id";

        // print_r($sql);
        // exit();
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllnewsletters()  // fetch all newsletterss for datatable
    {
        $sth = $this->db->prepare(" SELECT
                                        `nNewsLettersIDPK`,
                                        `tNewsLetterEmailID`,
                                        `dtNewsLetterDateTime`,
                                        `bIsNewsLetterRemoved`
                                    FROM
                                        `tblnewsletters`
                                    WHERE
                                        `bIsNewsLetterRemoved` = 0
                                   ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    

    public function fetchSinglenewsletters($id) // fetch single newsletters for updation
    {
        $sth = $this->db->prepare("SELECT
                                        `nNewsLettersIDPK`,
                                        `tNewsLetterEmailID`,
                                        `dtNewsLetterDateTime`,
                                        `bIsNewsLetterRemoved`
                                    FROM
                                        `tblnewsletters`
                                    WHERE
                                        `bIsNewsLetterRemoved` = 0
                                         AND 
                                        `nNewsLettersIDPK` = $id");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }




}