<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/16/2018
 * Time: 5:04 PM
 */

class Blogs extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/blogs/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        $data = $this->model->getBlogs();
        $this->view->data = $data;

        $this->view->render($name, "Blogs",$header,$footer);
    }

    public function add_blog()
    {
        $name = 'modules/blogs/view/add_blog.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $this->view->render($name, "Add Blog",$header,$footer);
    }

    public function createBlog()
    {
        $preImgText = date('ymdhsi');

        if($_POST['submit'] == "submit")
        {
            if (isset($_POST) && !empty($_POST))
            {

                extract($_POST);

                if($_FILES['Blog-image']['name'] != "")
                {
                    $img = $preImgText.$_FILES['Blog-image']['name'];
                }
                else
                {
                    $img = "";
                }

                $field = array('dBlogDate','tImgPath','tBlogHeading','tBlogContent');

                $value = array($blogDate,$img,$blogTitle,$blogDesc);

                $blog_data = array_combine($field, $value);

                $targetPath = Urls::$BLOG_UPLAODS.$img;
                move_uploaded_file($_FILES["Blog-image"]["tmp_name"], $targetPath);

                $blog_id = $this->model->insert($blog_data, 'tblblogs');

                if(isset($blog_id) && $blog_id == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Blog");
                    header("Location:../blogs/");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Blog added successully");
                    header("Location:../blogs/");
                }
            }
        }
        else if($_POST['submit'] == "update")
        {

            extract($_POST);

            $id = array('nBlogIDPK' => $blogID);

            if($_FILES["Blog-image"]["name"][0] != "")
            {
                $img = $preImgText.$_FILES['Blog-image']['name'];

                $field = array('dBlogDate','tImgPath','tBlogHeading','tBlogContent');

                $value = array($blogDate,$img,$blogTitle,$blogDesc);

                $targetPath = Urls::$BLOG_UPLAODS.$img;
                move_uploaded_file($_FILES["Blog-image"]["tmp_name"], $targetPath);
            }
            else
            {
                $field = array('dBlogDate','tBlogHeading','tBlogContent');

                $value = array($blogDate,$blogTitle,$blogDesc);
            }

            $blog_data = array_combine($field, $value);

            $blog_id = $this->model->update($blog_data, 'tblblogs',$id);

            if($blog_id > 0)
            {
                SessionHandling::set("suc_msg", "Blog updated successully");
                header("Location:../blogs/");
            }
            else if($blog_id == 0)
            {
                SessionHandling::set("suc_msg", "Blog updated successully");
                header("Location:../blogs/");
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating blog");
                header("Location:../blogs/");
            }
        }
    }

    public function blog_details()
    {
        $name = 'modules/blogs/view/blog_detail.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getSingleBlog($blockID);
        $this->view->data = $data;

        $this->view->render($name, "Blog Detail",$header,$footer);
    }

    public function send_blog()
    {
        $name = 'modules/blogs/view/add_blog.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getSingleBlog($blockID);

        $this->view->data = $data;

        $this->view->render($name, "Blog Update",$header,$footer);
    }

    public function delete_blog()
    {
        $blog_id = $_POST['id'];

        $id = array('nBlogIDPK' => $blog_id);

        $field = array('bIsRemoved');
        $value = array(1);
        $blog_data = array_combine($field, $value);

        $res = $this->model->update($blog_data, 'tblblogs',$id);

        if($res > 0)
        {
            SessionHandling::set("suc_msg", "Blog deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Blog");
            echo "false";
        }
    }

}