<style type="text/css">

    .vpb_wrapper {
        max-width:120px;
        border: solid 1px #cbcbcb;
        background-color: #FFF;
        box-shadow: 0 0px 10px #cbcbcb;
        -moz-box-shadow: 0 0px 10px #cbcbcb;
        -webkit-box-shadow: 0 0px 10px #cbcbcb;
        -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
        text-align:center;
        padding:10px;
        padding-bottom:3px;
        font-family:Verdana, Geneva, sans-serif;
        font-size:13px;
        line-height:25px;
        float:left;
        margin-right:20px;
        margin-bottom:20px;
        word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    label {
        font-weight: 400 !important;
    }
    .m-portlet .m-portlet__body {
    color: #000000;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    <i class="fa fa-newspaper"></i>
                                    Blog Detail
                                </h3>
                            </div>
                            <a href="<?php echo Urls::$BASE; ?>blogs/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Blogs Listing">
                                <i class="fa fa-list"></i>
                                <!--                        <i class="la la-ellipsis-h"></i>-->
                            </a>

                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <?php
                        if (!empty($this->data))
                        {?>
                            <div class="form-group  row">
                                <div class="col-lg-4">
                                    <label>Select Blog Image[Optional]</label>
                                    <span class="" onclick="document.getElementById('Blog-image').click();">
                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px; height: 100%">
                                            <div align="center" id="blog-display-preview" style="height: 100%;">
                                                 <img src="<?php echo Urls::$BASE.Urls::$BLOGS.$this->data[0]['tImgPath']; ?>" class="vpb_image_style" class="img-thumbnail">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>Blog Date</label>
                                            <input class="form-control date-picker" id="oStartDate" name="blogDate"  placeholder="Start Date" type="text" value="<?php echo $this->data[0]['dBlogDate']; ?>" disabled/>
                                        </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>Blog Title</label>
                                            <textarea class="form-control" rows="3.8" name="blogName" id="blogName" disabled><?php echo $this->data[0]['tBlogHeading']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label>Blog Discription</label>
                                    <textarea class="summernote form-control" id="m_summernote_1" name="blogDesc"><?php echo $this->data[0]['tBlogContent']; ?></textarea>
                                </div>
                            </div>
                            <?php
                        }
                       ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">
    $(document).ready(function()
    {
        $('#m_summernote_1').summernote('disable');
    });
</script>



