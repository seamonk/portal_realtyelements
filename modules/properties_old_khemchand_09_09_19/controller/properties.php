<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

use PHPMailer\PHPMailer;



use PHPMailer\Exception;



require './commons/PHPMailer/src/Exception.php';

require './commons/PHPMailer/src/PHPMailer.php';

require './commons/PHPMailer/src/SMTP.php';


class Properties extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set("Asia/Kolkata");
        }
    }

    public function index()
    {
        $name = 'modules/properties/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $loggedId = SessionHandling::get('loggedId');
        $userType = SessionHandling::get('userType');

        $data = $this->model->getPropertiesCount($loggedId,$userType);
        $this->view->propertiesCount = $data;
        $this->view->render($name,"Property Listing", $header, $footer);
    }
    public function getUserProperty()
    {   
        $loggedId = SessionHandling::get('loggedId');
        $userType = SessionHandling::get('userType');
        extract($_GET);
       /* print_r($_GET);
        exit();*/
        $data = $this->model->getProperties($loggedId,$userType,$PropertyName);
    }

    public function listing()
    {
        $name = 'modules/properties/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $loggedId = SessionHandling::get('loggedId');
        $userType = SessionHandling::get('userType');



        $data = $this->model->getPropertiesCount($loggedId,$userType,);
        $this->view->propertiesCount = $data;

        $this->view->render($name,"Property Listing", $header, $footer);
    }
    public function verifiedFromAdmin()
    {
        extract($_POST);
        $userid = array('nPropertyIDPK' => $_POST['id']);

        $field = array('bIsPropertyVerifiedByAdmin');

        $value = array($status);

        $dataUpdate = array_combine($field, $value);
        $res =  $this->model->update($dataUpdate, 'tblproperties',$userid);


        if ($status == 0) 
        {
            $email = $this->sendMail($emailID,"Property/Project Un-Verified By Agnpoint"," Hello ".$userName."<br><br> Welcome To Agenpoint. Due to Some un-authorized Data Your Property/project has been Un-Verified by Agenpoint Now you can't see this property/project on our Website<br><br>Regards,<br><br>Agenpoint");
        }
        else
        {
            $email = $this->sendMail($emailID,"Property/Project Verified By Agnpoint","Hello ".$userName."<br><br> Welcome To Agenpoint. Your Property/project has been Verified by Agenpoint Now you can see your property/project on our Website<br><br>Regards,<br><br>Agenpoint");
        }

        
        if($res > 0)
        {
            echo "true";     
        }
        else if($res == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        } 
    }
     public function sendMail($emailId,$subject,$body)
    {
        $mail = new PHPMailer\PHPMailer(true);                          

        try 
        {
            $mail->SMTPDebug = 2;                                 
            $mail->Host = 'seamonksolution.com';  
            $mail->SMTPAuth = true; 

            $mail->Username = 'agenpoint@seamonksolution.com';                 
            $mail->Password = 'agenpoint@2019';    

            $mail->SMTPSecure = 'ssl';               
            $mail->Port = 465;                                   
            $mail->setfrom("agenpoint@seamonksolution.com", 'Agenpoint');
            $mail->addAddress($emailId, $emailId);     
            $mail->isHTML(true);                           
            $mail->Subject = $subject;
            $mail->Body = $body;

            if($mail->send())
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        catch (Exception $e) 
        {
            echo 'false';
        }
    }
    public function featureFromAdmin()
    {
        extract($_POST);
        $userid = array('nPropertyIDPK' => $_POST['id']);

        $field = array('bIsFeaturedListing');

        $value = array($status);

        $dataUpdate = array_combine($field, $value);


       $res =  $this->model->update($dataUpdate, 'tblproperties',$userid);
        if($res > 0)
        {
            echo "true";     
        }
        else if($res == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        } 
    }
    public function facing()
    {
        $name = 'modules/properties/view/facing.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getPropertyFacing();
        $this->view->data = $data;

       
                   
                   
        $this->view->render($name,"Properties Listing", $header, $footer);
    }
    public function edit_facing()
    {
        $name = 'modules/properties/view/facing.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);
         $SinglePropertyFacing = $this->model->getSinglePropertyFacing($typeID);
        $this->view->SinglePropertyFacing = $SinglePropertyFacing;
                   
        $this->view->render($name,"Properties Listing", $header, $footer);
    }
    public function delete_facings()
    {
        $facingId = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nFacingIDPK' => $facingId);

        // $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsRemoved','nRemovedBy');
        $value = array(1,$loggedId);
        $data = array_combine($field, $value);

        $res = $this->model->update($data, 'tblfacings',$id);
      
        if($res > 0)
        {
            SessionHandling::set("suc_msg", "Facings deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Facings");
            echo "false";
        } 
    }

    public function createFacing()
    {
        $loggedId = SessionHandling::get("loggedId");  

       
        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                

                $field = array('tFacingName','nCreatedBy');

                $value = array($FacingName,$loggedId);

                $Types_data = array_combine($field, $value);
                //  print_r($Types_data);
                // exit();
                $Types_data = $this->model->insert($Types_data, 'tblfacings');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Derived Facings");
                    header("Location:../facing");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Facings added successully");
                    header("Location:./facing");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nFacingIDPK' => $typeID);

             $field = array('tFacingName');

            $value = array($FacingName);

            $Types_data = array_combine($field, $value);
             // print_r($Types_data);
             //    exit();
            $Types_id = $this->model->update($Types_data, 'tblfacings',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "Facings updated successully");
                header("Location:./facing");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "Facings updated successully");
                header("Location:./facing");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating Facings");
                header("Location:./facing");  
            }    
        }
    }

    public function units()
    {
        $name = 'modules/properties/view/unit.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getPropertyUnit();
        $this->view->data = $data;
                   
        $this->view->render($name,"Properties Listing", $header, $footer);
    }
    public function edit_unit()
    {
        $name = 'modules/properties/view/unit.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);
        $SinglePropertyunit = $this->model->getSinglePropertyunit($typeID);
        $this->view->SinglePropertyunit = $SinglePropertyunit;

        $data = $this->model->getPropertyUnit();
        $this->view->data = $data;
                   
        $this->view->render($name,"Properties Listing", $header, $footer);
    }
    public function delete_units()
    {
        $unitId = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nUnitIDPK' => $unitId);

        // $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsRemoved','nRemovedBy');
        $value = array(1,$loggedId);
        $data = array_combine($field, $value);

        $res = $this->model->update($data, 'tblunits',$id);
      
        if($res > 0)
        {
            SessionHandling::set("suc_msg", "units deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting units");
            echo "false";
        } 
    }
    public function createunit()
    {
        $loggedId = SessionHandling::get("loggedId");  

       
        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                

                $field = array('tUnitName','nCreatedBy');

                $value = array($UnitName,$loggedId);

                $Types_data = array_combine($field, $value);
                //  print_r($Types_data);
                // exit();
                $Types_data = $this->model->insert($Types_data, 'tblunits');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Derived units");
                    header("Location:../units");
                }
                else
                {
                    SessionHandling::set("suc_msg", "units added successully");
                    header("Location:./units");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nUnitIDPK' => $typeID);

             $field = array('tUnitName');

            $value = array($UnitName);

            $Types_data = array_combine($field, $value);
             // print_r($Types_data);
             //    exit();
            $Types_id = $this->model->update($Types_data, 'tblunits',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "units updated successully");
                header("Location:./units");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "units updated successully");
                header("Location:./units");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating units");
                header("Location:./units");  
            }    
        }
    }
    
    public function propertyBHK()
    {
        $name = 'modules/properties/view/propertyBHK.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getPropertyBHK();
        $this->view->data = $data;
                   
        $this->view->render($name,"Properties Listing", $header, $footer);
    }
    public function edit_propertyBHK()
    {
        $name = 'modules/properties/view/propertyBHK.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

         $SinglePropertypropertyBHK = $this->model->getSinglePropertyBHK($typeID);
        $this->view->SinglePropertyBHK = $SinglePropertypropertyBHK;

        $data = $this->model->getPropertyBHK();
        $this->view->data = $data;
                   
        $this->view->render($name,"Properties Listing", $header, $footer);
    }
    public function delete_propertyBHKs()
    {
        $propertyBHKId = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nBHKIDPK' => $propertyBHKId);

        // $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsRemoved','nRemovedBy');
        $value = array(1,$loggedId);
        $data = array_combine($field, $value);

        $res = $this->model->update($data, 'tblbhk',$id);
      
        if($res > 0)
        {
            SessionHandling::set("suc_msg", "propertyBHKs deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting propertyBHKs");
            echo "false";
        } 
    }
    public function createpropertyBHK()
    {
        // echo "<pre>";
        // print_r($_POST);
       
        // print_r($_FILES);
        // exit();
        $loggedId = SessionHandling::get("loggedId");  

       
        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                

                $field = array('tBHKName','nCreatedBy');

                $value = array($propertyBHKName,$loggedId);

                $Types_data = array_combine($field, $value);
                //  print_r($Types_data);
                // exit();
                $Types_data = $this->model->insert($Types_data, 'tblbhk');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Derived propertyBHKs");
                    header("Location:../propertyBHK");
                }
                else
                {
                    SessionHandling::set("suc_msg", "propertyBHKs added successully");
                    header("Location:./propertyBHK");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nBHKIDPK' => $typeID);

             $field = array('tBHKName');

            $value = array($propertyBHKName);

            $Types_data = array_combine($field, $value);
             // print_r($Types_data);
             //    exit();
            $Types_id = $this->model->update($Types_data, 'tblbhk',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "propertyBHKs updated successully");
                header("Location:./propertyBHK");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "propertyBHKs updated successully");
                header("Location:./propertyBHK");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating propertyBHKs");
                header("Location:./propertyBHK");  
            }    
        }
    }

    public function add_properties()
    {
        $name = 'modules/properties/view/add_property.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $loggedId = SessionHandling::get("loggedId");
        $userType = SessionHandling::get('userType');

        $checkListingLimit = $this->model->checkListingLimit($loggedId);
        $this->view->checkListingLimit = $checkListingLimit['nAddListingUpto'];

        $getPropertiesCount = $this->model->getPropertiesCount($loggedId,$userType);
        $this->view->getPropertiesCount = $getPropertiesCount;
        
        // print_r($checkListingLimit['nAddListingUpto']);
        // echo "<br>";
        // print_r($getPropertiesCount);
        // exit();

        $getCompanyNames = $this->model->getCompanyNames();
        $this->view->getCompanyNames = $getCompanyNames;

        $getPropertyType = $this->model->getPropertyType();
        $this->view->getPropertyType = $getPropertyType;

        $getPropertyAmenity = $this->model->getPropertyAmenity();
        $this->view->getPropertyAmenity = $getPropertyAmenity;

        $getPropertySubType = $this->model->getPropertySubType();
        $this->view->getPropertySubType = $getPropertySubType;

        /*$getPropertyDerivedType = $this->model->getPropertyDerivedType();
        $this->view->getPropertyDerivedType = $getPropertyDerivedType;*/

        $this->view->render($name,"Add Properties", $header, $footer);
    }

    public function createproperty()
    {   
        
        extract($_POST);

        $bIScoverImage = explode("," ,$coverImage[0]);
        // print_r($_POST);
        // exit();

        if (isset($layout['storeRoomAvailable'][0]) && !empty($layout['storeRoomAvailable'][0])) 
        {
            $layout['storeRoomAvailable'] = explode("," ,$layout['storeRoomAvailable'][0]); 
        }
        if (isset($layout['poojaRoomAvailable'][0]) && !empty($layout['poojaRoomAvailable'][0])) 
        {
           $layout['poojaRoomAvailable'] = explode("," ,$layout['poojaRoomAvailable'][0]); 
        }
        if(isset($layout['ExpectedNegociable'][0]) && !empty($layout['ExpectedNegociable'][0]))
        {   
           $layout['ExpectedNegociable'] = explode("," ,$layout['ExpectedNegociable'][0]); 
        }

        if(isset($layout['priceDisplayed'][0]) && !empty($layout['priceDisplayed'][0]))
        {   
            $layout['priceDisplayed'] = explode("," ,$layout['priceDisplayed'][0]); 
        }
        

        // print_r($_FILES);
        // exit();

        $dummyID = DUMMY_DATA;       

        $loggedId = SessionHandling::get("loggedId");  

        $userTypeID = SessionHandling::get('userTypeID');  

        $addedBy = 2;
        if (SessionHandling::get('userType') == AGENT) 
        {
           $addedBy = 0;
        }
        else if (SessionHandling::get('userType') == DEVELOPER)
        {
             $addedBy = 1;
        }

        $folderPathURL = Urls::$IMG_UPLOADS_LISTING;

        $preImgText = date('ymdhsi');  
       
        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                $expireDate = date('Y-m-d', strtotime($createOnDate. ' + 60 days'));
                // extract($_FILES);

                
                // print_r($expireDate);
                
                
                $folder_name = $folderPathURL.$preImgText.$loggedId."/";

                mkdir($folder_name, 0777, true);         


                if(isset($_FILES['brochurePath']['name']) && $_FILES['brochurePath']['name'] != '')
                {    
                    $brochureFileName = $preImgText.$loggedId.'/'.$preImgText.$loggedId.'.pdf';               

                    $targetPath1 = $folder_name.$preImgText.$loggedId.'.pdf';

                    move_uploaded_file($_FILES['brochurePath']["tmp_name"][0], $targetPath1);                 
                }
                else
                {
                    $brochureFileName = '';
                }   

                if(isset($rerayes))
                {
                    if($rerayes != "yes")
                    {   
                        $rerayes = 0;
                        $reraid = "";
                    }
                    else
                    {
                        $rerayes = 1;
                    }
                }
                else
                {
                    $rerayes = 0;
                   $reraid = "";
                } 
                if(isset($layout['ExpectedNegociable'][0]))
                {   
                    
                    $ExpectedNegociable = $layout['ExpectedNegociable'][0];
                }
                else{

                    $ExpectedNegociable = 0;
                }

                if (isset($nbrokerage)) {
                    if($nbrokerage == "yes")
                    {
                        $nbrokerage = 1;
                    }
                    else
                    {
                        $nbrokerage = 0;
                    }
                }
                else{
                    $nbrokerage = 0;
                }

                                 

                if(isset($brokerageYes))
                {   
                    if($brokerageYes != "yes")
                    {   
                        $brokerageYes = 0;
                        $brokerageAmount = "";
                        $brokerageAmountIn = "";
                    }
                    else{

                        $brokerageYes = 1;
                        $brokerageAmount = "";
                        
                    }
                }
                else{

                    $brokerageYes = 0;
                    $brokerageAmount = "";
                    $brokerageAmountIn = "";
                }

                if(isset($layout['washroom'][0]))
                {   
                    if($layout['washroom'][0] == "" && $layout['washroom'][0] == 0 )
                    {   
                        $washroom = 0;
                        
                    }
                    else
                    {
                        $washroom = $layout['washroom'][0];
                    }
                }
                else
                {
                     $washroom = 0;
                    
                }
                $balcony = 0;
                if(isset($layout['Balconies'][0]))
                {   
                    if($layout['Balconies'][0] == "" && $layout['Balconies'][0] == 0)
                    {   
                        $balcony = 0;
                        
                    }
                    else
                    {
                        $balcony = $layout['Balconies'][0];
                    }
                }
                else
                {
                     $balcony = 0;
                    
                } 

                if(isset($layout['BuildUpArea'][0]))
                {  
                    if($layout['BuildUpArea'][0] == "")
                    {   
                         if (isset($layout['ploatBuildUpArea'][0]) && !empty($layout['ploatBuildUpArea'][0])) 
                        {
                             $BuildUpArea = $layout['ploatBuildUpArea'][0];
                        }
                        else
                        {
                            $BuildUpArea = 0;
                        }
                       
                        
                    }
                    else
                    {
                        $BuildUpArea = $layout['BuildUpArea'][0];
                    }
                }
                else
                {
                     $BuildUpArea = 0;
                    
                }
                if(isset($layout['SuperBuildUpArea'][0]))
                {   
                    if($layout['SuperBuildUpArea'][0] == "")
                    {   
                        if (isset($layout['ploatSuperBuildUpArea'][0]) && !empty($layout['ploatSuperBuildUpArea'][0])) 
                        {
                            $SuperBuildUpArea = $layout['ploatSuperBuildUpArea'][0];
                        }
                        else
                        {
                            $SuperBuildUpArea = 0;
                        }
                    }
                    else
                    {
                        $SuperBuildUpArea = $layout['SuperBuildUpArea'][0];
                    }
                }
                else
                {
                     $SuperBuildUpArea = 0;
                    
                }

                if(isset($layout['carpetArea'][0]))
                {   
                    if($layout['carpetArea'][0] == "")
                    {   
                        if (isset($layout['ploatcarpetArea'][0]) && !empty($layout['ploatcarpetArea'][0])) 
                        {
                             $carpetArea = $layout['ploatcarpetArea'][0];
                        }
                        else
                        {
                            $carpetArea = 0;
                        }   
                    }
                    else
                    {
                        $carpetArea = $layout['carpetArea'][0];
                    }
                }
                else
                {
                     $carpetArea = 0;
                    
                } 
                if(isset($tFloor))
                {   
                    if($tFloor[0] == "")
                    {   
                        $totalFloors = 0;
                        
                    }
                    else{
                        $totalFloors = $tFloor[0];
                    }
                }
                else
                {
                     $totalFloors = 0;
                    
                } 
                
                
                if(isset($propertyNumber))
                {
                    
                }
                else
                {
                    $propertyNumber = "";
                }
                if(isset($MaintenanceAmount))
                {
                    
                }
                else
                {
                    $MaintenanceAmount = "";
                } 
                
                if(isset($maintenanceFor))
                {
                    
                }
                else
                {
                    $maintenanceFor = "";
                } 

                if(isset($ExpectedNegociable))
                {
                    $isPriceNegotiable = 1;
                }
                else
                {
                    $isPriceNegotiable = 0;
                } 

                if(isset($storeRoomAvailable))
                {  
                    $bIsStoreRoomAvailable = 1;
                }
                else
                {
                    $bIsStoreRoomAvailable = 0;
                }

                if(isset($poojaRoomAvailable))
                {  
                    $bIsPoojaRoomAvailable = 1;
                }
                else
                {
                    $bIsPoojaRoomAvailable = 0;
                } 
                
                $date = str_replace('/', '-', $possessionDate );
                $possessionDate = date("Y-m-d", strtotime($date));
                // print_r($possessionDate);
                // exit();
                
                
                $field = array(
                    'bIsProperty',
                    'tPropertyName', 
                    'dtPropertyPossesionDate', 
                    'tPropertyOwnership',
                    'nPropertyFor',
                    'nPropertyAvailability', 
                    'nPropertyTypeIDFK', 
                    'nPropertySubTypeIDFK', 

                    'tPropertyStreetName', 
                    'tPropertyAddress', 
                    'nLocalityIDFK',
                    'nPropertyAreaIDFK',
                    'nPropertyCityIDFK', 
                    'nPropertyStateIDFK', 
                    'nPropertyCountryIDFK', 
                    'dPropertyLatitude', 
                    'dPropertyLongitude', 
                    'tPropertyMapLocationLink', 

                    'fPropertySuperBuiltArea',
                    'fPropertyBuiltArea',
                    'fPropertyCarpetArea',
                    'nUnitIDFK',
                    'nWashroom',
                    'nBalconies',
                    'nTotalFloors',
                    'bIsPropertyReraCertified', 
                    'tPropertyReraID', 
                    'tPropertyDescription', 

                    
                    'fPropertyExpectedPrice',
                    'bIsExpectedPriceNegotiable',
                    'nPropertyBookingPrice',
                    'fPricePerUnitArea', 
                    
                    'nCreatedBy',
                    'tPropertyBrochurePath',
                    'dtPropertyExpireOn',
                    'nPropertyAddedBy',
                    'bIsDummyData'
                );

                $value = array(
                    
                    $listingFor,
                    $propertyName,
                    $possessionDate,
                    $ownership,
                    $propFor,
                    $Availability,
                    $propertyType,
                    $propertiesSubType,

                    $streetName,
                    $pAddress,
                    $localityName,
                    $area,
                    $city,
                    $stateID,
                    $countryID,
                    $pLatitutde,
                    $pLongitude,
                    $mapLocationLink,

                    $SuperBuildUpArea,
                    $BuildUpArea,
                    $carpetArea,
                    $layout['UnitID'][0],
                    $washroom,
                    $balcony,
                    $totalFloors,
                    $rerayes,
                    $reraid,
                    $propertyDescription,

                    $layout['Expected'][0],
                    $ExpectedNegociable,
                    $layout['BookingPrize'][0],
                    $layout['PricePerUnit'][0],
                    $loggedId,
                    $brochureFileName,
                    $expireDate,
                    $addedBy,
                    $dummyID
                );

                $listingValues = array_combine($field, $value);
                // print_r($listingValues);
                // exit();
                $listingId = $this->model->insert($listingValues, 'tblproperties');
                // $listingId = 1;
                if(isset($listingId) && $listingId == 0)
                {
                   echo "false";
                }
                else
                {   
                    if (isset($_FILES['files']['name'][0])) 
                    {
                        
                        if($_FILES['files']['name'] != '')
                        {  

                            $imagedelete = array();

                            // $imagedelete = explode(',' , $_FILES['DeletedFiles']);
                            if (isset($_FILES['DeletedFiles']['name'][0])) 
                            {
                                $recordsData = array_diff($_FILES['files']['name'],$_FILES['DeletedFiles']['name']);
                               
                            }
                            else{

                                $recordsData = $_FILES['files']['name'];
                            }
                            // print_r($recordsData);
                            // exit();

                            for ($i=0; $i < count($_FILES['files']['name']) ; $i++)
                            { 
                                if (isset($recordsData[$i])) 
                                {

                                    $propertyImages = $_FILES['files']['name'][$i];

                                    $fileExtension = pathinfo($propertyImages,PATHINFO_EXTENSION);

                                    $propertyFileName = $preImgText.$loggedId.'/'.$preImgText.$loggedId.$i.'.'.$fileExtension;

                                    $targetPath1 = $folder_name.$preImgText.$loggedId.$i.'.'.$fileExtension;
                                 
                                    move_uploaded_file($_FILES['files']["tmp_name"][$i], $targetPath1);

                                    $field = array('nPropertyIDFK','bIsFrontImage','tPropertyImageFolderPath','nCreatedBy','bIsDummyData');

                                    $value = array($listingId,$bIScoverImage[$i],$propertyFileName,$loggedId,$dummyID);

                                    $listingImgValues = array_combine($field, $value);

                                    // print_r($listingImgValues);
                                    // exit(); 

                                    $listingImages = $this->model->insert($listingImgValues, 'tblpropertyimages');                     
                                }
                                else{


                                }
                            }

                        }
                    }
                     // exit();
                    if(isset($AminitiesHidden) && !empty($AminitiesHidden))
                    {
                        $amenities = explode(',', $AminitiesHidden[0]);

                        for ($j=0; $j < count($amenities); $j++) { 
                            
                            $field = array('nPropertyIDFK', 'nAmenityIDFK','nCreatedBy','bIsDummyData');

                            $value = array($listingId,$amenities[$j],$loggedId,$dummyID);

                            $amenityValues = array_combine($field, $value);

                            // print_r($amenityValues);
                            // exit();

                            $amenityId = $this->model->insert($amenityValues, 'tblpropertyamenities');
                        }
                    } 

                        for ($j=0; $j < $layoutCount; $j++) 
                        { 

                            if(isset($layout['ExpectedNegociable']))
                            { 
                                if(isset($layout['ExpectedNegociable'][$j]))
                                {   
                                    
                                    $ExpectedNegociable = $layout['ExpectedNegociable'][$j];
                                }
                                else{

                                    $ExpectedNegociable = 0;
                                }
                            }
                            else
                            {

                                $ExpectedNegociable = 0;
                            }
                            
                            if(isset($layout['Cparking']))
                            { 
                                if(isset($layout['Cparking'][$j]) && $layout['Cparking'][$j] > 0)
                                {   
                                    
                                    $Cparking = $layout['Cparking'][$j];
                                }
                                else{

                                    $Cparking = 0;
                                }
                            }
                            else
                            {
                                $Cparking = 0;
                            }
                            if(isset($layout['Bathrooms']))
                            { 
                                if(isset($layout['Bathrooms'][$j]) && $layout['Bathrooms'][$j] > 0)
                                {   
                                    
                                    $Bathrooms = $layout['Bathrooms'][$j];
                                }
                                else{

                                    $Bathrooms = 0;
                                }
                            }
                            else
                            {
                               $Bathrooms = 0;
                            }
                            if(isset($layout['priceDisplayed']))
                            { 
                                if(isset($layout['priceDisplayed'][$j]))
                                {   
                                    
                                    $layout['priceDisplayed'][$j] = $layout['priceDisplayed'][$j];
                                }
                                else{

                                    $layout['priceDisplayed'][$j] = 0;
                                }
                            }
                            else
                            {
                                $layout['priceDisplayed'][$j] = 0;
                            }


                            if(isset($layout['BuildUpArea'][$j]))
                            {  
                                if(!empty($layout['BuildUpArea'][$j]))
                                {   
                                    if (isset($layout['ploatBuildUpArea'][$j]) && !empty($layout['ploatBuildUpArea'][$j])) 
                                    {
                                         $BuildUpArea = $layout['ploatBuildUpArea'][$j];
                                    }
                                    else
                                    {
                                        $BuildUpArea = 0;
                                    }
                                }
                                else
                                {
                                    $BuildUpArea = $layout['BuildUpArea'][$j];
                                }
                            }
                            else
                            {
                                 $BuildUpArea = 0;
                                
                            }
                            if(isset($layout['SuperBuildUpArea'][$j]))
                            {   
                                if($layout['SuperBuildUpArea'][$j] == "")
                                {   
                                    if (isset($layout['ploatSuperBuildUpArea'][$j]) && !empty($layout['ploatSuperBuildUpArea'][$j])) 
                                    {
                                        $SuperBuildUpArea = $layout['ploatSuperBuildUpArea'][$j];
                                    }
                                    else
                                    {
                                        $SuperBuildUpArea = 0;
                                    }
                                }
                                else
                                {
                                    $SuperBuildUpArea = $layout['SuperBuildUpArea'][$j];
                                }
                            }
                            else
                            {
                                 $SuperBuildUpArea = 0;
                                
                            }

                            if(isset($layout['carpetArea'][$j]))
                            {   
                                if($layout['carpetArea'][$j] == "")
                                {   
                                    if (isset($layout['ploatcarpetArea'][$j]) && !empty($layout['ploatcarpetArea'][$j])) 
                                    {
                                         $carpetArea = $layout['ploatcarpetArea'][$j];
                                    }
                                    else
                                    {
                                        $carpetArea = 0;
                                    }   
                                }
                                else
                                {
                                    $carpetArea = $layout['carpetArea'][$j];
                                }
                            }
                            else
                            {
                                 $carpetArea = 0;
                                
                            }
                            if(isset($layout['constructionBuildUpArea'][$j]))
                            { 
                                if(!empty($layout['constructionBuildUpArea'][$j]))
                                {   
                                    
                                    $constructionBuiltArea = $layout['constructionBuildUpArea'][$j];
                                }
                                else
                                {

                                    $constructionBuiltArea = 0;
                                }
                            }
                            else
                            {
                                $constructionBuiltArea = 0;
                            }

                            if(isset($layout['layoutPropBHK'][$j]))
                            { 
                                if(!empty($layout['layoutPropBHK']))
                                {   
                                    
                                    $layoutPropBHK = $layout['layoutPropBHK'][$j];
                                }
                                else
                                {

                                    $layoutPropBHK = 0;
                                }
                            }
                            else
                            {
                                $layoutPropBHK = 0;
                            }

                            if(isset($layout['constructionSuperBuildUpArea'][$j]))
                            { 
                                if(!empty($layout['constructionSuperBuildUpArea'][$j]))
                                {   
                                    
                                    $constructionSuperBuiltArea = $layout['constructionSuperBuildUpArea'][$j];
                                }
                                else
                                {

                                    $constructionSuperBuiltArea = 0;
                                }
                            }
                            else
                            {
                                $constructionSuperBuiltArea = 0;
                            }

                            if(isset($layout['constructioncarpetArea'][$j]))
                            { 
                                if(!empty($layout['constructioncarpetArea'][$j]))
                                {   
                                    
                                    $constructionCarpetArea = $layout['constructioncarpetArea'][$j];
                                }
                                else
                                {

                                    $constructionCarpetArea = 0;
                                }
                            }
                            else
                            {
                                $constructionCarpetArea = 0;
                            }
                            
                            if(isset($layout['layoutPropBHK'][$j]))
                            { 
                                if(!empty($layout['layoutPropBHK'][$j]))
                                {   
                                    
                                    $layoutPropBHK = $layout['layoutPropBHK'][$j];
                                }
                                else
                                {

                                    $layoutPropBHK = 0;
                                }
                            }
                            else
                            {
                                $layoutPropBHK = 0;
                            }
                            if ($propertyType == 2) 
                            {
                                
                                if(!empty($layout['balcony'][$j]))
                                {   
                                    if($layout['balcony'][$j] == "" && $layout['balcony'][$j] == 0)
                                    {   
                                        $balcony = 0;
                                        
                                    }
                                    else
                                    {
                                        $balcony = $layout['balcony'][$j];
                                    }
                                }
                                else
                                {
                                     $balcony = 0;
                                    
                                }
                            }
                            else
                            {
                                if(isset($layout['Balconies'][$j]))
                                {   
                                    if($layout['Balconies'][$j] == "" && $layout['Balconies'][$j] == 0)
                                    {   
                                        $balcony = 0;
                                        
                                    }
                                    else
                                    {
                                        $balcony = $layout['Balconies'][$j];
                                    }
                                }
                                else
                                {
                                     $balcony = 0;
                                    
                                }
                            }
                            if(isset($layout['Balconies'][$j]))
                            {  
                                $layout['Balconies'][$j] = 1;
                            }
                            else
                            {
                                $layout['Balconies'][$j] = 0;
                            }

                            if(isset($layout['storeRoomAvailable'][$j]))
                            {  
                               $layout['storeRoomAvailable'][$j] = $layout['storeRoomAvailable'][$j];
                            }
                            else
                            {
                                $layout['storeRoomAvailable'][$j] = 0;
                            } 
                            if(isset($layout['poojaRoomAvailable'][$j]))
                            {  
                                $layout['poojaRoomAvailable'][$j] = $layout['poojaRoomAvailable'][$j];
                            }
                            else
                            {
                                $layout['poojaRoomAvailable'][$j] = 0;
                            } 

                            if (isset($_FILES['layout_image']['name'][$j])) 
                            {
                                # code...
                                $propertyImages = $_FILES['layout_image']['name'][$j];

                                $fileExtension = pathinfo($propertyImages,PATHINFO_EXTENSION);

                                $propertyFileName = $preImgText.$loggedId.'/'.$preImgText.$loggedId.$j.'.'.$fileExtension;

                                $targetPath1 = $folder_name.$preImgText.$loggedId.$j.'.'.$fileExtension;
                             
                                move_uploaded_file($_FILES['layout_image']["tmp_name"][$j], $targetPath1);
                            }
                            else
                            {
                                $propertyFileName = "";
                            }
                            if(isset($layout['washroom'][$j]))
                            {  
                                $washroom = $layout['washroom'][$j];
                            }
                            else
                            {
                                $washroom = 0;
                            }
                             
                            if(isset($layout['pentry'][$j]))
                            {  
                                $pentry = $layout['pentry'][$j];
                            }
                            else
                            {
                                $pentry = 0;
                            }
                            if(isset($layout['propertyFurnishedID'][$j]))
                            { 
                                if(isset($layout['propertyFurnishedID'][$j]) && $layout['propertyFurnishedID'][$j] = "yes")
                                {   
                                    
                                    $propertyFurnishedID = 1;
                                }
                                else{

                                    $propertyFurnishedID = 0;
                                }
                            }
                            else
                            {
                                 $propertyFurnishedID = 0;
                            }
                            if(isset($layout['Bedrooms'][$j]))
                            {  
                                $Bedrooms = $layout['Bedrooms'][$j];
                            }
                            else
                            {
                                $Bedrooms = 0;
                            }
                            if(isset($layout['tFloor'][$j]))
                            {  
                                $tFloor = $layout['tFloor'][$j];
                            }
                            else
                            {
                                $tFloor = 0;
                            }
                            if(isset($layout['pFloor'][$j]))
                            {  
                                $pFloor = $layout['pFloor'][$j];
                            }
                            else
                            {
                                $pFloor = 0;
                            }  

                           
                            $field = array(
                                'nPropertyIDFK',
                                'nProjectSubTypeIDFK',
                                'nBHKIDFK',
                                'fProjectSuperBuiltArea',
                                'fProjectBuiltArea',
                                'fProjectCarpetArea',
                                'nUnitIDFK',
                                'bIsPropertyFurnished',
                                'nBedrooms',
                                'nBalconies',
                                'nBathrooms',
                                'nWashroom',
                                'nCarParkings',
                                'tLayoutUmages',
                                'nTotalFloors',
                                'nPropertyOnFloor',
                                'nPentry',
                                'bIsStoreRoomAvailable',
                                'bIsPoojaRoomAvailable',
                                'fProjectExpectedPrice',
                                'bIsExpectedPriceNegotiable',
                                'fPricePerUnitArea',
                                'fProjectBookingPrice',
                                'bIsPriceShow',
                                'fConstructionSuperBuiltArea',
                                'fConstructionBuiltArea',
                                'fConstructionCarpetArea'
                            );


                            $value = array(
                                $listingId,
                                $propertiesSubType,
                                $layoutPropBHK,
                                $SuperBuildUpArea,
                                $BuildUpArea,
                                $carpetArea,
                                $layout['UnitID'][$j],
                                $propertyFurnishedID,
                                $Bedrooms,
                                $balcony,
                                $Bathrooms,
                                $washroom,
                                $Cparking,
                                $propertyFileName,
                                $tFloor,
                                $pFloor,
                                $pentry,
                                $layout['storeRoomAvailable'][$j],
                                $layout['poojaRoomAvailable'][$j],
                                $layout['Expected'][$j],
                                $ExpectedNegociable,
                                $layout['PricePerUnit'][$j],
                                $layout['BookingPrize'][$j],
                                $layout['priceDisplayed'][$j],
                                $constructionSuperBuiltArea,
                                $constructionBuiltArea,
                                $constructionCarpetArea
                            );

                            $amenityValues = array_combine($field, $value);

                            // print_r($amenityValues);
                            

                            $amenityId = $this->model->insert($amenityValues, 'tblprojectlayouts');
                        }

                        // exit();
                   echo "true";

                  
                }               
            }
        }
                
    }
    public function updateProperty()
    {
        // echo "<pre>";
        $dummyID = DUMMY_DATA;       
        extract($_POST);

        if (isset($layout['storeRoomAvailable'][0]) || !empty($layout['storeRoomAvailable'][0])) 
        {
            $layout['storeRoomAvailable'] = explode("," ,$layout['storeRoomAvailable'][0]); 
        }
        if (isset($layout['poojaRoomAvailable'][0]) || !empty($layout['poojaRoomAvailable'][0])) 
        {
           $layout['poojaRoomAvailable'] = explode("," ,$layout['poojaRoomAvailable'][0]); 
        }
        if(isset($layout['ExpectedNegociable'][0]) && !empty($layout['ExpectedNegociable'][0]))
        {   
           $layout['ExpectedNegociable'] = explode("," ,$layout['ExpectedNegociable'][0]); 
        }
        if(isset($layout['priceDisplayed'][0]) && !empty($layout['priceDisplayed'][0]))
        {   
            $layout['priceDisplayed'] = explode("," ,$layout['priceDisplayed'][0]); 
        }

        // echo "<pre>";
        // print_r($layout['ExpectedNegociable']);
        // print_r($layout['priceDisplayed']);
        // print_r($layout['poojaRoomAvailable']);
        // print_r($layout['storeRoomAvailable']);
        // print_r($_POST);
        // print_r($_FILES);
        // exit();

        $Image_FolderName = explode("/", $Image_FolderName);
        $FolderName = $Image_FolderName[0];
        // print_r($FolderName);
        // exit();
            $loggedId = SessionHandling::get("loggedId");

            $id = array('nPropertyIDPK' => $_POST['ID']);

            $date = str_replace('/', '-', $possessionDate );
            $possessionDate = date("Y-m-d", strtotime($date));
            if(isset($rerayes))
            {
                if($rerayes != "yes")
                {   
                    $rerayes = 0;
                    $reraid = "";
                }
                else
                {
                    $rerayes = 1;
                }
            }
            else
            {
                $rerayes = 0;
                $reraid = "";
            } 
            if(isset($layout['BuildUpArea'][0]))
                {  
                    if($layout['BuildUpArea'][0] == "")
                    {   
                         if (isset($layout['ploatBuildUpArea'][0]) && !empty($layout['ploatBuildUpArea'][0])) 
                        {
                             $BuildUpArea = $layout['ploatBuildUpArea'][0];
                        }
                        else
                        {
                            $BuildUpArea = 0;
                        }
                       
                        
                    }
                    else
                    {
                        $BuildUpArea = $layout['BuildUpArea'][0];
                    }
                }
                else
                {
                     $BuildUpArea = 0;
                    
                }
                if(isset($layout['SuperBuildUpArea'][0]))
                {   
                    if($layout['SuperBuildUpArea'][0] == "")
                    {   
                        if (isset($layout['ploatSuperBuildUpArea'][0]) && !empty($layout['ploatSuperBuildUpArea'][0])) 
                        {
                            $SuperBuildUpArea = $layout['ploatSuperBuildUpArea'][0];
                        }
                        else
                        {
                            $SuperBuildUpArea = 0;
                        }
                    }
                    else
                    {
                        $SuperBuildUpArea = $layout['SuperBuildUpArea'][0];
                    }
                }
                else
                {
                     $SuperBuildUpArea = 0;
                    
                }

                if(isset($layout['carpetArea'][0]))
                {   
                    if($layout['carpetArea'][0] == "")
                    {   
                        if (isset($layout['ploatcarpetArea'][0]) && !empty($layout['ploatcarpetArea'][0])) 
                        {
                             $carpetArea = $layout['ploatcarpetArea'][0];
                        }
                        else
                        {
                            $carpetArea = 0;
                        }   
                    }
                    else
                    {
                        $carpetArea = $layout['carpetArea'][0];
                    }
                }
                else
                {
                     $carpetArea = 0;
                    
                } 

            $currentDate = date('Y-m-d H:i:s');

            $field = array('dtPropertyPossesionDate','nPropertyFor','nPropertyAvailability','bIsPropertyReraCertified','tPropertyReraID');
            $value = array($possessionDate,$propFor,$Availability,$rerayes,$reraid);

            $data = array_combine($field,$value);
            $res = $this->model->update($data, 'tblproperties',$id);
        

            $field = array(
                            'tPropertyName',
                            'tPropertyStreetName', 
                            'tPropertyAddress', 
                            'nLocalityIDFK',
                            'nPropertyAreaIDFK',
                            'nPropertyCityIDFK', 
                            'nPropertyStateIDFK', 
                            'nPropertyCountryIDFK', 
                            'dPropertyLatitude',
                            'dPropertyLongitude', 
                            'fPropertySuperBuiltArea',
                            'fPropertyBuiltArea',
                            'fPropertyCarpetArea',
                            'tPropertyMapLocationLink');
            $value = array(
                        $propertyName,
                        $streetName,
                        $pAddress,
                        $LocalityID,
                        $locationID,
                        $cityID,
                        $stateID,
                        $countryID,
                        $pLatitutde,
                        $pLongitude,
                        $SuperBuildUpArea,
                        $BuildUpArea,
                        $carpetArea,
                        $mapLocationLink);

            $data = array_combine($field,$value);
            // print_r($data);
            // exit();
            $res = $this->model->update($data, 'tblproperties',$id);
            $field = array('tPropertyOwnership','tPropertyDescription');
            $value = array($ownership,$propertyDescription);
            $data = array_combine($field,$value);
            // print_r($data);
            // exit();
            $res = $this->model->update($data, 'tblproperties',$id);

            for ($j=0; $j < $layoutCount; $j++) 
            { 

                if(isset($layout['ExpectedNegociable']))
                { 
                    if(isset($layout['ExpectedNegociable'][$j]))
                    {   
                        $ExpectedNegociable = $layout['ExpectedNegociable'][$j];
                    }
                    else{

                        $ExpectedNegociable = 0;
                    }
                }
                else
                {

                    $ExpectedNegociable = 0;
                }
                
                if(isset($layout['Cparking']))
                { 
                    if(isset($layout['Cparking'][$j]) && $layout['Cparking'][$j] > 0)
                    {   
                        
                        $Cparking = $layout['Cparking'][$j];
                    }
                    else{

                        $Cparking = 0;
                    }
                }
                if(isset($layout['Bathrooms']))
                { 
                    if(isset($layout['Bathrooms'][$j]) && $layout['Bathrooms'][$j] > 0)
                    {   
                        
                        $Bathrooms = $layout['Bathrooms'][$j];
                    }
                    else{

                        $Bathrooms = 0;
                    }
                }
                else
                {
                    $Bathrooms = 0;
                }
                if(isset($layout['priceDisplayed']))
                { 
                    if(isset($layout['priceDisplayed'][$j]))
                    {   
                        
                        $layout['priceDisplayed'][$j] = $layout['priceDisplayed'][$j];
                    }
                    else{

                        $layout['priceDisplayed'][$j] = 0;
                    }
                }
                else
                {
                    $layout['priceDisplayed'][$j] = 0;
                }


                if(isset($layout['BuildUpArea'][$j]))
                {  
                    if (isset($layout['BuildUpArea'][$j]) && !empty($layout['BuildUpArea'][$j])) 
                    {
                         $BuildUpArea = $layout['BuildUpArea'][$j];
                    }
                    else
                    {
                        if(isset($layout['ploatBuildUpArea'][$j]))
                        { 
                            if(!empty($layout['ploatBuildUpArea'][$j]))
                            {   
                                
                                $BuildUpArea = $layout['ploatBuildUpArea'][$j];
                            }
                            else
                            {

                                $BuildUpArea = 0;
                            }
                        }
                        else
                        {
                            $BuildUpArea = 0;
                        }
                    }
                }
                else
                {
                     $BuildUpArea = 0;
                    
                }
                if(isset($layout['SuperBuildUpArea'][$j]))
                {   
                    if(empty($layout['SuperBuildUpArea'][$j]))
                    {   
                        if(isset($layout['ploatSuperBuildUpArea'][$j]))
                        { 
                            if(!empty($layout['ploatSuperBuildUpArea'][$j]))
                            {   
                                
                                $SuperBuildUpArea = $layout['ploatSuperBuildUpArea'][$j];
                            }
                            else
                            {
                                $SuperBuildUpArea = 0;
                                
                            }
                        }
                        else
                        {
                            $SuperBuildUpArea = 0;
                        }
                    }
                    else
                    {
                        $SuperBuildUpArea = $layout['SuperBuildUpArea'][$j];
                    }
                }
                else
                {
                     $SuperBuildUpArea = 0;
                    
                }

                if(isset($layout['carpetArea'][$j]))
                {   
                    if($layout['carpetArea'][$j] == "")
                    {   
                        if (isset($layout['ploatcarpetArea'][$j]) && !empty($layout['ploatcarpetArea'][$j])) 
                        {
                             $carpetArea = $layout['ploatcarpetArea'][$j];
                        }
                        else
                        {
                           if(isset($layout['ploatcarpetArea'][$j]))
                            { 
                                if(!empty($layout['ploatcarpetArea'][$j]))
                                {   
                                    
                                    $carpetArea = $layout['ploatcarpetArea'][$j];
                                }
                                else
                                {

                                    $carpetArea = 0;
                                }
                            }
                            else
                            {
                                $carpetArea = 0;
                            }
                        }   
                    }
                    else
                    {
                        $carpetArea = $layout['carpetArea'][$j];
                    }
                }
                else
                {
                     $carpetArea = 0;
                    
                }
                
                if(isset($layout['layoutPropBHK'][$j]))
                { 
                    if(!empty($layout['layoutPropBHK']))
                    {   
                        
                        $layoutPropBHK = $layout['layoutPropBHK'][$j];
                    }
                    else
                    {

                        $layoutPropBHK = 0;
                    }
                }
                else
                {
                    $layoutPropBHK = 0;
                }

                if(isset($layout['constructionSuperBuildUpArea'][$j]))
                { 
                    if(!empty($layout['constructionSuperBuildUpArea'][$j]))
                    {   
                        
                        $constructionSuperBuiltArea = $layout['constructionSuperBuildUpArea'][$j];
                    }
                    else
                    {

                        $constructionSuperBuiltArea = 0;
                    }
                }
                else
                {
                    $constructionSuperBuiltArea = 0;
                }

                if(isset($layout['constructioncarpetArea'][$j]))
                { 
                    if(!empty($layout['constructioncarpetArea'][$j]))
                    {   
                        
                        $constructionCarpetArea = $layout['constructioncarpetArea'][$j];
                    }
                    else
                    {

                        $constructionCarpetArea = 0;
                    }
                }
                else
                {
                    $constructionCarpetArea = 0;
                }
                
                if(isset($layout['layoutPropBHK'][$j]))
                { 
                    if(!empty($layout['layoutPropBHK'][$j]))
                    {   
                        
                        $layoutPropBHK = $layout['layoutPropBHK'][$j];
                    }
                    else
                    {

                        $layoutPropBHK = 0;
                    }
                }
                else
                {
                    $layoutPropBHK = 0;
                }
                if ($propertyType == 2) 
                {
                    
                    if(!empty($layout['balcony'][$j]))
                    {   
                        if($layout['balcony'][$j] == "" && $layout['balcony'][$j] == 0)
                        {   
                            $balcony = 0;
                            
                        }
                        else
                        {
                            $balcony = $layout['balcony'][$j];
                        }
                    }
                    else
                    {
                         $balcony = 0;
                        
                    }
                }
                else
                {
                    if(isset($layout['Balconies'][$j]))
                    {   
                        if($layout['Balconies'][$j] == "" && $layout['Balconies'][$j] == 0)
                        {   
                            $balcony = 0;
                            
                        }
                        else
                        {
                            $balcony = $layout['Balconies'][$j];
                        }
                    }
                    else
                    {
                         $balcony = 0;
                        
                    }
                }

                if(isset($layout['storeRoomAvailable'][$j]))
                {  
                   $layout['storeRoomAvailable'][$j] = $layout['storeRoomAvailable'][$j];
                }
                else
                {
                    $layout['storeRoomAvailable'][$j] = 0;
                } 
                if(isset($layout['poojaRoomAvailable'][$j]))
                {  
                    $layout['poojaRoomAvailable'][$j] = $layout['poojaRoomAvailable'][$j];
                }
                else
                {
                    $layout['poojaRoomAvailable'][$j] = 0;
                } 

                if(isset($layout['Bathrooms'][$j]))
                {  
                    $Bathrooms = 1;
                }
                else
                {
                    $Bathrooms = 0;
                }
                if(isset($layout['Bathrooms'][$j]))
                {  
                    $Bathrooms = $layout['Bathrooms'][$j];
                }
                else
                {
                    $Bathrooms = 0;
                }
                if(isset($layout['pentry'][$j]))
                {  
                    $pentry = $layout['pentry'][$j];
                }
                else
                {
                    $pentry = 0;
                }
                if(isset($layout['Bedrooms'][$j]))
                {  
                    $Bedrooms = $layout['Bedrooms'][$j];
                }
                else
                {
                    $Bedrooms = 0;
                }
                if(isset($layout['washroom'][$j]))
                {  
                    $washroom = $layout['washroom'][$j];
                }
                else
                {
                    $washroom = 0;
                }
                if(isset($layout['Cparking'][$j]))
                {  
                    $Cparking = $layout['Cparking'][$j];
                }
                else
                {
                    $Cparking = 0;
                }
                if(isset($layout['tFloor'][$j]))
                {  
                    $tFloor = $layout['tFloor'][$j];
                }
                else
                {
                    $tFloor = 0;
                }
                if(isset($layout['pFloor'][$j]))
                {  
                    $pFloor = $layout['pFloor'][$j];
                }
                else
                {
                    $pFloor = 0;
                }  
                if(isset($layout['ExpectedNegociable'][$j]))
                { 
                    $ExpectedNegociable = $layout['ExpectedNegociable'][$j];
                }
                else
                {
                    $ExpectedNegociable = 0;
                }
               
                if(isset($layout['propertyFurnishedID'][$j]))
                { 
                    if(isset($layout['propertyFurnishedID'][$j]) && $layout['propertyFurnishedID'][$j] = "yes")
                    {   
                        
                        $propertyFurnishedID = 1;
                    }
                    else{

                        $propertyFurnishedID = 0;
                    }
                }
                else
                {
                     $propertyFurnishedID = 0;
                }
                if(isset($layout['constructionBuildUpArea'][$j]))
                { 
                    if(!empty($layout['constructionBuildUpArea'][$j]))
                    {   
                        
                        $constructionBuiltArea = $layout['constructionBuildUpArea'][$j];
                    }
                    else
                    {

                        $constructionBuiltArea = 0;
                    }
                }
                else
                {
                    $constructionBuiltArea = 0;
                }

                $field = array(
                    'nPropertyIDFK',
                    'nProjectSubTypeIDFK',
                    'nBHKIDFK',
                    'fProjectSuperBuiltArea',
                    'fProjectBuiltArea',
                    'fProjectCarpetArea',
                    'nUnitIDFK',
                    'bIsPropertyFurnished',
                    'nBedrooms',
                    'nBalconies',
                    'nBathrooms',
                    'nWashroom',
                    'nCarParkings',
                    'nTotalFloors',
                    'nPropertyOnFloor',
                    'nPentry',
                    'bIsStoreRoomAvailable',
                    'bIsPoojaRoomAvailable',
                    'fProjectExpectedPrice',
                    'bIsExpectedPriceNegotiable',
                    'fPricePerUnitArea',
                    'fProjectBookingPrice',
                    'bIsPriceShow',
                    'fConstructionSuperBuiltArea',
                    'fConstructionBuiltArea',
                    'fConstructionCarpetArea'
                );


                $value = array(
                    $ID,
                    $propertiesSubType,
                    $layoutPropBHK,
                    $SuperBuildUpArea,
                    $BuildUpArea,
                    $carpetArea,
                    $layout['UnitID'][$j],
                    $propertyFurnishedID,
                    $Bedrooms ,
                    $balcony,
                    $Bathrooms,
                    $washroom,
                    $Cparking,
                    $tFloor,
                    $pFloor,
                    $pentry,
                    $layout['storeRoomAvailable'][$j],
                    $layout['poojaRoomAvailable'][$j],
                    $layout['Expected'][$j],
                    $ExpectedNegociable,
                    $layout['PricePerUnit'][$j],
                    $layout['BookingPrize'][$j],
                    $layout['priceDisplayed'][$j],
                    $constructionSuperBuiltArea,
                    $constructionBuiltArea,
                    $constructionCarpetArea
                );

                $amenityValues = array_combine($field, $value);

                // print_r($amenityValues);
                // exit();
                
                if (isset($layout['LayoutID'][$j]) && !empty($layout['LayoutID'][$j])) 
                {
                    $id = array('nProjectIDPK' => $layout['LayoutID'][$j]);
                    $res = $this->model->update($amenityValues, 'tblprojectlayouts',$id);
                }
                else
                {
                    $res = $this->model->insert($amenityValues, 'tblprojectlayouts');
                   // print_r($tFloor);
                }

                $preImgText = date('ymdhsi');
                
                $folderPathURL = Urls::$IMG_UPLOADS_LISTING;

                $folder_name = $folderPathURL.$FolderName."/";

                if (file_exists($folder_name)) {
                   
                }
                else{

                    mkdir($folder_name, 0777, true);
                }

                if(isset($_FILES['layout_image']['name'][$j]) && !empty($layout['LayoutID'][$j])) 
                {
                    if (!empty($_FILES['layout_image']['name'][$j])) {

                        $propertyImages = $_FILES['layout_image']['name'][$j];

                        $fileExtension = pathinfo($propertyImages,PATHINFO_EXTENSION);

                        $propertyFileName = $FolderName.'/'.$preImgText.$loggedId.$j.'.'.$fileExtension;

                        $targetPath1 = $folder_name.$preImgText.$loggedId.$j.'.'.$fileExtension;
                     
                        move_uploaded_file($_FILES['layout_image']['tmp_name'][$j], $targetPath1);

                        $field = array('tLayoutUmages');
                        $value = array($propertyFileName);

                        $Values = array_combine($field, $value);
                        // print_r($Values);
                        if (isset($layout['LayoutID'][$j]) && !empty($layout['LayoutID'][$j])) 
                        {
                            $id = array('nProjectIDPK' => $layout['LayoutID'][$j]);
                            $res = $this->model->update($Values, 'tblprojectlayouts',$id);
                        }
                        else
                        {
                            $res = $this->model->update($Values, 'tblprojectlayouts',$res);
                        }
                    }
                }
            }
            // exit();


            $preImgText = date('ymdhsi');
                    
            $folderPathURL = Urls::$IMG_UPLOADS_LISTING;
            $folder_name = $folderPathURL.$FolderName."/";
            
            if (file_exists($folder_name)) 
            {
                
            }
            else{

            mkdir($folder_name, 0777, true);
            }


            $data = $this->model->delete($ID,'tblpropertyamenities','nPropertyIDFK');

            // print_r($data);
            // exit();

            if(isset($AminitiesHidden) && !empty($AminitiesHidden))
            {
                $amenities = explode(',', $AminitiesHidden[0]);

                for ($j=0; $j < count($amenities); $j++) { 
                    
                    $field = array('nPropertyIDFK', 'nAmenityIDFK','nCreatedBy','bIsDummyData');

                    $value = array($ID,$amenities[$j],$loggedId,$dummyID);

                    $amenityValues = array_combine($field, $value);

                    // print_r($amenityValues);
                    // exit();

                    $res = $this->model->insert($amenityValues, 'tblpropertyamenities');
                }
            }
            if (isset($_FILES['files']['name'][0])) 
            {
                
                if($_FILES['files']['name'] != '')
                {  

                    $imagedelete = array();

                    // $imagedelete = explode(',' , $_FILES['DeletedFiles']);
                    if (isset($_FILES['DeletedFiles']['name'][0])) 
                    {
                        $recordsData = array_diff($_FILES['files']['name'],$_FILES['DeletedFiles']['name']);
                       
                    }
                    else{

                        $recordsData = $_FILES['files']['name'];
                    }
                    // print_r($recordsData);
                    // exit();
                    for ($i=0; $i < count($_FILES['files']['name']) ; $i++)
                    { 
                        if (isset($recordsData[$i])) 
                        {

                            $propertyImages = $_FILES['files']['name'][$i];

                            $fileExtension = pathinfo($propertyImages,PATHINFO_EXTENSION);

                            $propertyFileName = $FolderName.'/'.$preImgText.$loggedId.$i.'.'.$fileExtension;

                            $targetPath1 = $folder_name.$preImgText.$loggedId.$i.'.'.$fileExtension;
                         
                            move_uploaded_file($_FILES['files']["tmp_name"][$i], $targetPath1);

                            $field = array('nPropertyIDFK', 'tPropertyImageFolderPath','nCreatedBy','bIsDummyData');

                            $value = array($ID,$propertyFileName,$loggedId,$dummyID);

                            $listingImgValues = array_combine($field, $value);

                            // print_r($listingImgValues);
                            // exit(); 

                            $res = $this->model->insert($listingImgValues, 'tblpropertyimages');                     
                        }
                        else{


                        }
                    }


                }
            }


            if(isset($_FILES['brochurePath']['name']) && $_FILES['brochurePath']['name'] != '')
            {    
                $brochureFileName = $FolderName.'/'.$preImgText.$loggedId.'.pdf';               

                $targetPath1 = $folder_name.$preImgText.$loggedId.'.pdf';

                move_uploaded_file($_FILES['brochurePath']["tmp_name"][0], $targetPath1);                 
                $field = array('tPropertyBrochurePath');
                $value = array($brochureFileName);
                $data = array_combine($field,$value);
                
                $res = $this->model->update($data, 'tblproperties',$id);
            }

        if($res > 0)
        {
            echo "true";
        }
        else if($res == 0)
        {
            echo "true";            
        }
        else
        {
            echo "false";
        } 

    }

    public function RemovePropertyLayout()
    {      
        extract($_POST);
        // print_r($_POST);
        // exit();
       $loggedId = SessionHandling::get("loggedId");


        $res = $this->model->delete($id,'tblprojectlayouts','nProjectIDPK');
      
        if($res > 0)
        {
            
            echo "true";
        }
        else
        {
            
            echo "false";
        } 
    }
    public function deleteListing()
    {
        $loggedId = SessionHandling::get("loggedId");

        $id = array('nPropertyIDPK' => $_POST['id']);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsRemoved','dtRemovedOn','nRemovedBy');
        $value = array(1,$currentDate,$loggedId);
        $data = array_combine($field,$value);

        $res = $this->model->update($data, 'tblproperties',$id);
      
        if($res > 0)
        {
            
            echo "true";
        }
        else
        {
            
            echo "false";
        } 
    }
    public function viewListing()
    {
        $listingID = $_POST['listingID'];

        $name = 'modules/properties/view/property_detail.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';


        $data = $this->model->getSinglePropertysView($listingID);
        $this->view->data = $data;

        $getTypeDetails = $this->model->getPropertyType();
        $this->view->getTypes = $getTypeDetails;

        $this->view->render($name,"Properties Detail", $header, $footer);
    }
    public function editProperty()
    {
        if(isset($_POST['listingID']))
        {
            $listingID = $_POST['listingID'];
        }
        else
        {
            header('Location:./');
        }

        $name = 'modules/properties/view/update_property_new.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $getPropertyType = $this->model->getPropertyType();
        $this->view->getPropertyType = $getPropertyType;

        $getPropertyAmenity = $this->model->getPropertyAmenity();
        $this->view->getPropertyAmenity = $getPropertyAmenity;

        $getPropertySubType = $this->model->getPropertySubType();
        $this->view->getPropertySubType = $getPropertySubType;

        /*$getPropertyDerivedType = $this->model->getPropertyDerivedType();
        $this->view->getPropertyDerivedType = $getPropertyDerivedType;
*/
        $data = $this->model->getSinglePropertyDetail($listingID);
        $this->view->data = $data;

        /*echo '<pre>';

        print_r($getPropertyType);
        print_r($getPropertyAmenity);
        print_r($getPropertySubType);
        print_r($data);

        exit;*/

        $this->view->render($name,"Update Property", $header, $footer);

    }
    public function delete_listing_image()
    {
        $listingImgID = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nPropertyImageIDPK' => $listingImgID);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsRemoved','dtRemovedOn','nRemovedBy');
        $value = array(1,$currentDate,$loggedId);
        $image_data = array_combine($field, $value);

        $res = $this->model->update($image_data, 'tblpropertyimages',$id);
      
        if($res > 0)
        {
            echo "true";
        }
        else
        {
            echo "false";
        } 
    }


    public function getTypeDetails()
    {
        $getTypeDetails = $this->model->getTypeDetails();
        echo json_encode($getTypeDetails);
    }
    public function getUnitsDetails()
    {
        $getUnitsDetails = $this->model->getUnitsDetails();
        echo json_encode($getUnitsDetails);
    }
    public function getBHKDetails()
    {
        $getBHKDetails = $this->model->getBHKDetails();
        echo json_encode($getBHKDetails);
    }
    
    public function getSubTypeDetailsMain()
    {
        // extract($_POST);
        $getSubTypeDetails = $this->model->getSubTypeDetailsMain();
        echo json_encode($getSubTypeDetails);
    }
    public function getPropertyLocation()
    {
        extract($_POST);
        // print_r($_POST);
        // exit();
        $getPropertyLocation = $this->model->getPropertyLocation($PropertyID);
        echo json_encode($getPropertyLocation);
    }
    
    public function getSubTypeDetails()
    {
        extract($_POST);
        $getSubTypeDetails = $this->model->getSubTypeDetails($id);
        echo json_encode($getSubTypeDetails);
    } 
    // public function getLayoutType()
    // {
    //     // extract($_POST);
    //     $getLayoutType = $this->model->getLayoutType();
    //     echo json_encode($getLayoutType);
    // }  
       
    public function getfacingDetails()
    {
        // extract($_POST);
        $getAmenitiesDetails = $this->model->getfacingDetails();
        echo json_encode($getAmenitiesDetails);
    }
    public function getDerivedTypeDetails()
    {
        extract($_POST);
        $getDerivedTypeDetails = $this->model->getDerivedTypeDetails($id);
        echo json_encode($getDerivedTypeDetails);
    }        
    public function getAmenitiesDetails()
    {
        // extract($_POST);
        $getAmenitiesDetails = $this->model->getAmenitiesDetails();
        echo json_encode($getAmenitiesDetails);
    }    
    public function getLocations()
    {
        $getLocations = $this->model->getLocations();
        echo json_encode($getLocations);
    } 

    public function types()
    {
        $name = 'modules/properties/view/types.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getPropertyType();
        $this->view->data = $data;

        $this->view->render($name,"Property Types", $header, $footer);
    }
    public function amenity()
    {
        $name = 'modules/properties/view/amenity.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getPropertyAmenity();
        $this->view->data = $data;

        $this->view->render($name,"Property Types", $header, $footer);
    }

    public function subtypes()
    {
        $name = 'modules/properties/view/subtypes.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $getPropertyType = $this->model->getPropertyType();
        $this->view->getPropertyType = $getPropertyType;

        $data = $this->model->getPropertySubType();
        $this->view->data = $data;

        $this->view->render($name,"Property Sub Types", $header, $footer);
    }
    public function derivedtype()
    {
        $name = 'modules/properties/view/derivedtypes.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $getPropertyType = $this->model->getPropertyType();
        $this->view->getPropertyType = $getPropertyType;

        /*$data = $this->model->getPropertyDerivedType();
        $this->view->data = $data;*/

        $this->view->render($name,"Property Sub Types", $header, $footer);
    }

    public function edit_type()
    {
        $name = 'modules/properties/view/types.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleTypes = $this->model->getSingleTypes($typeID);
        $this->view->singleTypes = $singleTypes;

        $data = $this->model->getPropertyType();
        $this->view->data = $data;

        $this->view->render($name,"Properties Types", $header, $footer);
    }
    public function edit_Amenity()
    {
        $name = 'modules/properties/view/amenity.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleAmenity = $this->model->getSingleAmenity($typeID);
        $this->view->singleTypes = $singleAmenity;

        $data = $this->model->getPropertyAmenity();
        $this->view->data = $data;

        $this->view->render($name,"Properties Types", $header, $footer);
    }
     public function edit_Subtype()
    {
        $name = 'modules/properties/view/subtypes.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleTypes = $this->model->getSingleSubTypes($editSubTypeID);
        $this->view->singleTypes = $singleTypes;

        $data = $this->model->getPropertySubType();
        $this->view->data = $data;

        $this->view->render($name,"Properties Types", $header, $footer);
    }
    public function edit_Derivedtype()
    {
        $name = 'modules/properties/view/derivedtypes.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleTypes = $this->model->getSingleDerivedTypes($derivedtypeID);
        $this->view->singleTypes = $singleTypes;

        /*$data = $this->model->getPropertyDerivedType();
        $this->view->data = $data;*/

        $this->view->render($name,"Properties Types", $header, $footer);
    }

    public function check_type()
    {
        $type = $_POST['type'];
        

        if ($_POST['type'] == 'submit') 
        {
            $result = $this->model->check_Type_Exist($type);
        }
        else if($_POST['type'] == 'update')
        {

            //print_r($_REQUEST);
            $typeID = $_POST['id'];
            $result = $this->model->check_Type_Exist_update($type,$typeID);
        }

        if($result)
        {
            echo "true";
        }
        else
        {
            echo "false";
        }
    }
       
    public function createDerivedType()
    {
        $loggedId = SessionHandling::get("loggedId");  

       
        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                

                $field = array('nPropertySubTypeIDFK','tPropertyDerivedTypeName');

                $value = array($SubType,$DerivedType);

                $Types_data = array_combine($field, $value);
                //  print_r($Types_data);
                // exit();
                $Types_data = $this->model->insert($Types_data, 'tblpropertyderivedtype');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Derived Type");
                    header("Location:derivedtype");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Derived Type added successully");
                    header("Location:derivedtype");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nPropertyDerivedTypeIDPK' => $derivedtypeID);

             $field = array('nPropertySubTypeIDFK','tPropertyDerivedTypeName');

            $value = array($SubType,$DerivedType);

            $Types_data = array_combine($field, $value);
             // print_r($Types_data);
             //    exit();
            $Types_id = $this->model->update($Types_data, 'tblpropertyderivedtype',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "Derived Type updated successully");
                header("Location:derivedtype");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "Derived Type updated successully");
                header("Location:derivedtype");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating Derived Type");
                header("Location:derivedtype");  
            }    
        }
    }
    public function createSubType()
    {
        $loggedId = SessionHandling::get("loggedId");  

      
        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                
                $preImgText = date('ymdhsi');
            
                $Folder_name = Urls::$IMG_UPLOADS_LISTING;

                $property_logo = $_FILES['property_logo']['name'];
                if (!empty($_FILES['property_logo']['name'])) 
                {
                    $property_logo = $_FILES['property_logo']['name'];
                    $extension = pathinfo($property_logo,PATHINFO_EXTENSION);
                    $imageName = $preImgText.".".$extension;
                    $targetPath = $Folder_name.$preImgText.".".$extension;
                    move_uploaded_file($_FILES['property_logo']["tmp_name"], $targetPath);
                }
                else
                {
                     $property_logo = "";
                }
                $extension = end(explode(".", $property_logo));
                $imageName = $preImgText.".".$extension;
                $targetPath = $Folder_name.$preImgText.".".$extension;
                move_uploaded_file($_FILES['property_logo']["tmp_name"], $targetPath);
                $field = array('nPropertyTypeIDFK','tPropertySubTypeName','tPropertySubTypeImagePath');

                $value = array($category,$subtypeName,$imageName);

                $Types_data = array_combine($field, $value);
                //  print_r($Types_data);
                // exit();
                $Types_data = $this->model->insert($Types_data, 'tblpropertysubtypes');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Sub Types");
                      header("Location:subtypes");    
                }
                else
                {
                    SessionHandling::set("suc_msg", "Sub Types added successully");
                     header("Location:subtypes");    
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nPropertySubTypeIDPK' => $subtypeID);
            $preImgText = date('ymdhsi');
            
            $Folder_name = Urls::$IMG_UPLOADS_LISTING;

            $files = $_FILES['property_logo']['name'];

            if ($files != "") {

                $extension = end(explode(".", $files));
                
                $imageName = $preImgText.".".$extension;
                
                $targetPath = $Folder_name.$preImgText.".".$extension;
                $field = array('nPropertyTypeIDFK','tPropertySubTypeName','tPropertySubTypeImagePath');

                $value = array($category,$subtypeName,$imageName);
                move_uploaded_file($_FILES['property_logo']["tmp_name"], $targetPath);  
               
            }
            else{

                $field = array('nPropertyTypeIDFK','tPropertySubTypeName');

                $value = array($category,$subtypeName);
            }

            $Types_data = array_combine($field, $value);
             // print_r($Types_data);
             //    exit();
            $Types_id = $this->model->update($Types_data, 'tblpropertysubtypes',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "Types updated successully");
                header("Location:subtypes");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "Types updated successully");
                header("Location:subtypes");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating Types");
                header("Location:types");  
            }    
        }
    }
    public function createType()
    {
        $loggedId = SessionHandling::get("loggedId");  

        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                

                $field = array('tPropertyTypeName');

                $value = array($typeName);

                $Types_data = array_combine($field, $value);
    
                $Types_data = $this->model->insert($Types_data, 'tblpropertytypes');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Types");
                    header("Location:types");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Types added successully");
                    header("Location:types");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nPropertyTypeIDPK' => $typeID);

            $field = array('tPropertyTypeName');

            $value = array($typeName);

            $Types_data = array_combine($field, $value);

            $Types_id = $this->model->update($Types_data, 'tblpropertytypes',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "Types updated successully");
                header("Location:types");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "Types updated successully");
                header("Location:types");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating Types");
                header("Location:types");  
            }    
        }
    }
    public function createAmenity()
    {
        $loggedId = SessionHandling::get("loggedId");  
        extract($_POST);      
        extract($_FILES);

        // print_r($_FILES);
        // print_r($_POST);
        // exit();
        if($_POST['submit'] == "submit")
        {    
            $loggedId = SessionHandling::get("loggedId"); 
            if (isset($_POST) && !empty($_POST))
            {

                $FolderName =  Urls::$ABOUTUS;

                // if (!is_dir($FolderName)) 
                // {  
                //     mkdir($FolderName, 0777, true);     
                //     copy(Urls::$IMG_UPLOADS_LISTING.'no_image.png',$FolderName.'/no_image.png');
                // }   

                $files = $_FILES['banner_image']['name'];
                $preImgText = date('ymdhsi');
                $extension = pathinfo($files,PATHINFO_EXTENSION);
                $imageName = $preImgText.".".$extension;
                $targetPath = $FolderName.$preImgText.".".$extension;


                move_uploaded_file($_FILES['banner_image']['tmp_name'],$targetPath);

                $field = array('tAmenityName','tAmenityImagePath','nCreatedBy');

                $value = array($amenity,$imageName,$loggedId);

                $Types_data = array_combine($field, $value);

                // print_r($Types_data);
                // exit();
    
                $Types_data = $this->model->insert($Types_data, 'tblamenities');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Amenity");
                    header("Location:amenity");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Amenity added successully");
                    header("Location:amenity");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nAmenityIDPK' => $amenityID);

            if (!empty($_FILES['banner_image']['name'])) {

                $FolderName =  Urls::$ABOUTUS;
                $files = $_FILES['banner_image']['name'];
                $preImgText = date('ymdhsi');
                $extension = end(explode(".", $files));
                $imageName = $preImgText.".".$extension;
                $targetPath = $FolderName.$preImgText.".".$extension;


                move_uploaded_file($_FILES['banner_image']['tmp_name'],$targetPath);
                 $field = array('tAmenityName','tAmenityImagePath');

                $value = array($amenity,$imageName);

                # code...
            }
            else{
                
                $field = array('tAmenityName');

                $value = array($amenity);

            }

                $Types_data = array_combine($field, $value);
                // print_r($Types_data);
                // exit();

            $Types_id = $this->model->update($Types_data, 'tblamenities',$id);
                     
            if($Types_id > 0)
            {   
                SessionHandling::set("suc_msg", "Amenity updated successully");
                header("Location:amenity");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "Amenity updated successully");
                header("Location:amenity");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating Amenity");
                header("Location:amenity");  
            }    
        }
    }
    public function deleteAmenity()
    {
        $Types_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");  

        $id = array('nAmenityIDPK' => $Types_id);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsRemoved');
        $value = array(1);
        $Types_data = array_combine($field, $value);

        $res = $this->model->update($Types_data, 'tblamenities',$id);
      
        if($res > 0)    
        {
            SessionHandling::set("suc_msg", "Amenity deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Amenity");
            echo "false";
        } 
    }
    public function delete_type()
    {
        $Types_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");  

        $id = array('nPropertyTypeIDPK' => $Types_id);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsPropertyTypeRemoved');
        $value = array(1);
        $Types_data = array_combine($field, $value);

        $res = $this->model->update($Types_data, 'tblpropertytypes',$id);
      
        if($res > 0)    
        {
            SessionHandling::set("suc_msg", "Types deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Types");
            echo "false";
        } 
    }
    public function deleteSubType()
    {
        // print_r($_POST);
        // exit();
        $Types_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");  

        $id = array('nPropertySubTypeIDPK' => $Types_id);


        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsPropertySubTypeRemoved');
        $value = array(1);
        $Types_data = array_combine($field, $value);
        // print_r($Types_data);
        // exit();
        $res = $this->model->update($Types_data, 'tblpropertysubtypes',$id);
      
        if($res > 0)    
        {
            SessionHandling::set("suc_msg", "Types deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Types");
            echo "false";
        } 
    }
    public function deleteDerivedType()
    {
        $Types_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");  

        $id = array('nPropertyDerivedTypeIDPK' => $Types_id);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsPropertyDerivedTypeRemoved');
        $value = array(1);
        $Types_data = array_combine($field, $value);

        $res = $this->model->update($Types_data, 'tblpropertyderivedtype',$id);
      
        if($res > 0)    
        {
            SessionHandling::set("suc_msg", "Types deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Types");
            echo "false";
        } 
    }    

    
}