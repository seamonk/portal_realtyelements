<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}
    label {
    font-weight: 400 !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">
                    <i style="font-size: 20px;" class="fa fa-home"></i> 
                     Properties Detail
            </h3>
            </div>
            <a href="<?php echo Urls::$BASE; ?>properties/" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listing">
                <i class="fa fa-list"></i>
                <!--                        <i class="la la-ellipsis-h"></i>-->
            </a> 
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
            <?php  
            if (!empty($this->data)) 
            {
               /* print_r($this->data);*/
            ?>
            <form class="m-form m-form--label-align-right" method="POST" action="<?php echo Urls::$BASE ?>properties/createproperty" enctype = "multipart/form-data" id="add_form" name="add_form">
                <!--      START : Basic Details-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                            <div class="m-accordion__item">
                                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
                                    <span class="m-accordion__item-icon">
                                        <i class="fa flaticon-edit-1"></i>
                                    </span>
                                    <span class="m-accordion__item-title">Basic Detail</span>
                                    <span class="m-accordion__item-mode"></span>
                                </div>
                                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                                    <div class="m-accordion__item-content">
                                        <!--begin::Form-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label>property Logo[Optional]</label>
                                                <span class="" onclick="document.getElementById('property_logo').click();">
                                                    <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                        <!-- <input style="display:none;" type="file" name="property_logo" id="property_logo" onchange="property_image_preview(this)" /> -->

                                                       <div align="center" id="property-display-preview">
                                                            
                                                                <img style="height:100%;width:100%;" class="vpb_image_style" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data[0]['tPropertyFrontImageFolderPath'];
                                                             ?>" />
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>Select Catogory</label>
                                                        <select class="form-control m-select2" id="pCategoryID" name="pCategoryID" disabled >
                                                           
                                                           <?php
                                                            if (!empty($this->getTypes)) {
                                                                for ($i = 0; $i < count($this->getTypes); $i++) {
                                                                    if ($this->data[0]['tPropertyTypeIDFK'] == $this->getTypes[$i]['nPropertyTypeIDPK']) {
                                                                       ?>
                                                                        <option value="<?php echo $this->getTypes[$i]['nPropertyTypeIDPK']; ?>" selected><?php echo $this->getTypes[$i]['tPropertyTypeName']; ?></option>
                                                                       <?php
                                                                    }
                                                                    else
                                                                    {
                                                                    ?>
                                                                <option value="<?php echo $this->getTypes[$i]['nPropertyTypeIDPK']; ?>"><?php echo $this->getTypes[$i]['tPropertyTypeName']; ?></option>
                                                            <?php
                                                                }
                                                            }
                                                        }
                                                            ?>   
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>property Name</label>
                                                        <input type="text" class="form-control m-input" value="<?php echo $this->data[0]['tPropertyName']; ?>" name="pName" placeholder="property Name" id="pName" disabled >
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>property Title</label>
                                                        <input type="text" class="form-control m-input" value="<?php echo $this->data[0]['tPropertyTitle']; ?>" name="pTitle" placeholder="property Title" disabled >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <div class="col-lg-12">
                                                <label>property Discription</label>
                                                <textarea class="summernote form-control" id="m_summernote_1" name="pDescription" > </textarea>
                                            </div>
                                        </div> -->
                                        <!--end::Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--      END : Basic Details-->
                <!--      START : Address-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-accordion m-accordion--default" id="m_accordion_2" role="tablist">
                            <div class="m-accordion__item">
                                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_2_item_2_head" data-toggle="collapse" href="#m_accordion_2_item_2_body" aria-expanded="    false">
                                    <span class="m-accordion__item-icon">
                                        <i class="fa flaticon-home-2"></i>
                                    </span>
                                    <span class="m-accordion__item-title">Address</span>
                                    <span class="m-accordion__item-mode"></span>
                                </div>
                                <div class="m-accordion__item-body collapse" id="m_accordion_2_item_2_body" class=" " role="tabpanel" aria-labelledby="m_accordion_2_item_2_head" data-parent="#m_accordion_1">
                                    <div class="m-accordion__item-content">
                                        <!--begin::Form-->
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <label>Address 1</label>
                                                <textarea class="form-control"  disabled name="pAddress" rows="3"><?php echo $this->data[0]['tPropertyAddress1']; ?></textarea>
                                                <!--                                        <span class="m-form__help">Please enter place description</span>-->
                                            </div>
                                        </div>
                                         <div class="form-group row">
                                            <div class="col-lg-12">
                                                <label>Address 2</label>
                                                <textarea class="form-control"  disabled name="sAddress" rows="3"><?php echo $this->data[0]['tPropertyAddress2']; ?></textarea>
                                                <!--                                        <span class="m-form__help">Please enter place description</span>-->
                                            </div>
                                        </div>
                                        <div class="form-group  row">
                                            <div class="col-lg-3">
                                                <label>Select Country</label>
                                                <select class="form-control m-select2" id="pCountryID" name="pCountryID" value="<?php echo $this->data[0]['nPropertyCountryIDFK']; ?>"  style="width: 100% !important"  disabled >
                                                    <option value="">Select Country</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Select State</label>
                                                <select class="form-control m-select2" id="pStateID" name="pStateID" value="<?php echo $this->data[0]['nPropertyStateIDFK']; ?>"  style="width: 100% !important"  disabled >
                                                    <option value="">Select State</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Select City</label>
                                                <select class="form-control m-select2" id="pCityID" disabled name="pCityID" value="<?php echo $this->data[0]['nPropertyCityIDFK']; ?>"  style="width: 100% !important" >
                                                    <option value="">Select City</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-3">
                                                <label>Select Area</label>
                                                <select class="form-control m-select2" id="pAreaID" disabled  name="pAreaID" value="<?php echo $this->data[0]['nPropertyAreaIDFK']; ?>"  style="width: 100% !important">
                                                    <option value="">Select Area</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-6">
                                                <label>property Latitude</label>
                                                <input type="text" class="form-control m-input" disabled  name="pLatitutde"  value="<?php echo $this->data[0]['dPropertyLatitude']; ?>" id="pLatitutde" placeholder="property Latitude" >
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <label>property Longitude</label>
                                                <input type="text" class="form-control m-input"  disabled name="pLongitude" value="<?php echo $this->data[0]['dPropertyLongitude']; ?>"  id="pLongitude" placeholder="property Longitude">
                                                
                                            </div>
                                        </div>
                                        <!--end::Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--      END : Address-->

                

                <!--      START : Others-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="m-accordion m-accordion--default" id="m_accordion_3" role="tablist">
                            <div class="m-accordion__item">
                                <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_3_item_3_head" data-toggle="collapse" href="#m_accordion_3_item_3_body" aria-expanded="false">
                                    <span class="m-accordion__item-icon">
                                        <i class="fa flaticon-list-2"></i>
                                    </span>
                                    <span class="m-accordion__item-title">Others</span>
                                    <span class="m-accordion__item-mode"></span>
                                </div>
                                <div class="m-accordion__item-body collapse" id="m_accordion_3_item_3_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_3_head" data-parent="#m_accordion_1">
                                    <div class="m-accordion__item-content">
                                        <!--begin::Form-->
                                      
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label>Property Description</label>
                                                <input type="text" class="form-control m-input" name="PropertyDescription" value="<?php echo $this->data[0]['tPropertyDescription']; ?>" disabled  placeholder="Property Description" value="" >
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property BHK</label>
                                                <input type="text" class="form-control m-input" disabled  name="PropertyBHK"  value="<?php echo $this->data[0]['tPropertyBHK']; ?>"placeholder="Property BHK">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Furnished</label>
                                                <br>
                                                <select id="furnished" name="furnished" class="form-control m-select2" value="<?php echo $this->data[0]['bIsPropertyFurnished']; ?>"style="width: 100%;" disabled >
                                                   <option value="1">Furnished</option>
                                                   <option value="0">Not Furnished </option> 
                                                   <option value="2">Semi-Furnished </option>
                                                </select>
                                                <!-- <input type="checkbox" class="form-control s-checkbox-list" name="IsPropertyFurnished" placeholder="Property Furnished"> -->
                                            </div>

                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label>Property Block</label>
                                                <input type="text" class="form-control m-input" name="PropertyBlock"  value="<?php echo $this->data[0]['tPropertyBlock']; ?>"placeholder="Propery Block"  disabled onkeypress="return isNumberKey(event);">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Unit Number</label>
                                                <input type="text" class="form-control m-input" name="PropertyUnitNumber"  value="<?php echo $this->data[0]['tPropertyUnitNumber']; ?>"placeholder="Property Unit Number"  disabled onkeypress="return isNumberKey(event);">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Floor</label>
                                                <input type="text" class="form-control m-input" name="PropertyFloor"  value="<?php echo $this->data[0]['tPropertyFloor']; ?>"placeholder="Property Floor" disabled  onkeypress="return isNumberKey(event);">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label>Property Total Floors</label>
                                                <input type="text" class="form-control m-input" name="PropertyTotalFloors" value="<?php echo $this->data[0]['nPropertyTotalFloors']; ?>" disabled  placeholder="Property Total Floors" onkeypress="return isNumberKey(event);" maxlength="2">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Building Name</label>
                                                <input type="text" class="form-control m-input" name="BuildingName" value="<?php echo $this->data[0]['tBuildingName']; ?>" placeholder="Building Name" disabled >
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Price</label>
                                                <input type="text" class="form-control m-input" name="PropertyPrice"  value="<?php echo $this->data[0]['fPropertyPrice']; ?>"placeholder="Property Price" disabled  onkeypress="return isNumberKey(event);">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label>Property Rent</label>
                                                <input type="text" class="form-control m-input" name="PropertyRent" value="<?php echo $this->data[0]['fPropertyRent']; ?>" placeholder="Property Rent" disabled  onkeypress="return isNumberKey(event);">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Fixed Price</label>
                                                <input type="text" class="form-control m-input" name="PropertyFixedPrice" value="<?php echo $this->data[0]['fPropertyFixedPrice']; ?>" placeholder="Property Fixed Price" disabled  onkeypress="return isNumberKey(event);">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Negotiable Price</label>
                                                <input type="text" class="form-control m-input" name="PropertyNegotiablePrice" value="<?php echo $this->data[0]['fPropertyNegotiablePrice']; ?>" placeholder="Property Negotiable Price" disabled  onkeypress="return isNumberKey(event);">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label>Property Possession Status</label>
                                                <input type="text" class="form-control m-input" name="PropertyPossessionStatus" value="<?php echo $this->data[0]['tPropertyPossessionStatus']; ?>" placeholder="Property Possession Status" disabled >
                                            </div>
                                            <div class="col-lg-4">
                                                <label>property Possesion Year</label>
                                                <input type="text" class="form-control m-input" name="PropertyPossesionYear" value="<?php echo $this->data[0]['yPropertyPossesionYear']; ?>" placeholder="Property Possesion Year" disabled  onkeypress="return isNumberKey(event);">
                                            </div>
                                            <div class="col-lg-4">
                                                <label>Property Company Name</label>
                                                <input type="text" class="form-control m-input" name="PropertyCompanyName" disabled  value="<?php echo $this->data[0]['tPropertyCompanyName']; ?>" placeholder="Property Company Name">
                                            </div>
                                        </div>
                                        <!--end::Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--      END : Others-->
                
                <!-- <div class="form-group m-form__group row">
                    <div class="col-6">
                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">Add property</button>
                    </div>
               
                    <div class="col-6">
                        <button type="reset" class="btn btn-secondary btn-block">Cancel</button>
                    </div>
                </div> -->
            </form>
            <?php  
            }
            ?>
        </div>
                <!--end::Portlet-->
</div>

<!-- end:: Body -->
<script type="text/javascript">
     $(document).ready(function() 
    {
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
        // $('#m_summernote_1').summernote('disable');
        // document.getElementById("pShowBookingCost").disabled = true;
           // $('#pCategoryID').select2();

        $('#pCategoryID').select2();
        $('#pCountryID').select2();
        $('#pStateID').select2();
        $('#pCityID').select2();
        $('#pAreaID').select2();
        $('#furnished').select2();
        $('#pReportingTime').select2();


        getCountryList();

        $('#pCountryID').on('change', function(){
            getStateList();
        });

        $('#pStateID').on('change', function(){
            getCityList();
        });

        $('#pCityID').on('change', function(){
            getAreaList();
        });

        $('.time-picker').timepicker({ 
            showMeridian: false,
            minuteStep: 5 
        });

        $("#submit").click(function(e) 
        {
            // var Daylent = $(".chkday:checkbox:checked").length;

            if($('#pCategoryID').val() == '')
            {
                toastr.error("Please select category.", "Category");
                e.preventDefault();
            }
            else if($('#pName').val() == '')
            {
                toastr.error("Please enter property name.", "property Name");
                e.preventDefault();
            }
            else if($('#pCountryID').val() == '')
            {
                toastr.error("Please select country.", "Country");
                e.preventDefault();
            }
            else if($('#pStateID').val() == '')
            {
                toastr.error("Please select state.", "State");
                e.preventDefault();
            }
            else if($('#pCityID').val() == '')
            {
                toastr.error("Please select city.", "City");
                e.preventDefault();
            }
            else if($('#pAreaID').val() == '')
            {
                toastr.error("Please select area.", "Area");
                e.preventDefault();
            }
            // else if(Daylent < 1)
            // {
            //     toastr.error("Please select atleast one day.", "Select Day");
            //     e.preventDefault();
            // }
            mUtil.scrollTo("add_form", -200);  
        });

        $("#update_basic").click(function(e) 
        {
            if($('#pCategoryID').val() == '')
            {
                toastr.error("Please select category.", "Category");
                e.preventDefault();
            }
            else if($('#pName').val() == '')
            {
                toastr.error("Please enter property name.", "property Name");
                e.preventDefault();
            }
            mUtil.scrollTo("update_basic_form", -200);
        });


        $("#update_address").click(function(e) 
        {
            if($('#pCountryID').val() == '')
            {
                toastr.error("Please select country.", "Country");
                e.preventDefault();
            }
            else if($('#pStateID').val() == '')
            {
                toastr.error("Please select state.", "State");
                e.preventDefault();
            }
            else if($('#pCityID').val() == '')
            {
                toastr.error("Please select city.", "City");
                e.preventDefault();
            }
            else if($('#pAreaID').val() == '')
            {
                toastr.error("Please select area.", "Area");
                e.preventDefault();
            }
            mUtil.scrollTo("update_address_form", -200);
        });

        $("#update_times").click(function(e) 
        {
            var Daylent = $(".chkday:checkbox:checked").length;
            if(Daylent < 1)
            {
                toastr.error("Please select atleast one day.", "Select Day");
                e.preventDefault();
            }
            mUtil.scrollTo("update_time_form", -200);
        });

    
        $("#pLatitutde").focusout(function()
        {
            var latitude = document.getElementById('pLatitutde').value;
           
            var reg = new RegExp("^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$");

            if(latitude != '')
            {
                if(!reg.exec(latitude)) 
                {
                    document.getElementById('pLatitutde').value='';
                    toastr.error("Please enter valid latitude value.", "latitude");
                }
            } 
        });

        $("#pLongitude").focusout(function()
        {
            var longitude = document.getElementById('pLongitude').value;

            var reg = new RegExp("^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$");

            if(longitude != '')
            {
                if(!reg.exec(longitude)) 
                {
                    document.getElementById('pLongitude').value='';
                    toastr.error("Please enter valid longitude value.", "longitude");
                }
            }  
        });
    });


    function getCountryList()
    {



        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function(data) 
            {
                $('#pCountryID').empty();
                $('#pCountryID').append('<option value="">Select Country</option>');

                var country = JSON.parse(data);

                //console.log(country.length);

                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nPropertyCountryIDFK'];} else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nCountryIDPK)
                        $('#pCountryID').append('<option value='+country[i].nCountryIDPK+' selected>'+country[i].tCountryName+'</option>');
                    else
                        $('#pCountryID').append('<option value='+country[i].nCountryIDPK+'>'+country[i].tCountryName+'</option>');
                }
                $("#pCountryID").trigger("change");
            }
        });
    }


   function getStateList()
    {
        var countryID = $('#pCountryID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getState',
            type: 'POST',
            data: {countryID: countryID},
            success: function(data) {
                $('#pStateID').empty();
                $('#pStateID').append('<option value="">Select State</option>');

                var state = JSON.parse(data);

                if(state != null)
                {
                    for(var i = 0; i < state.length; i++)
                    {
                        var data1 = "<?php if(isset($this->data)) {echo $this->data[0]['nPropertyStateIDFK'];} else {echo NULL; } ?>";
                        //console.log(data1);
                        if(data1 == state[i].nStateIDPK)
                            $('#pStateID').append('<option value='+state[i].nStateIDPK+' selected>'+state[i].tStateName+'</option>');
                        else
                            $('#pStateID').append('<option value='+state[i].nStateIDPK+'>'+state[i].tStateName+'</option>');
                    }
                    $("#pStateID").trigger("change");
                }  
            }
        });
    }

     function getCityList()
    {
        var stateID = $('#pStateID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCity',
            type: 'POST',
            data: {stateID: stateID},
            success: function(data) {
                $('#pCityID').empty();
                $('#pCityID').append('<option value="">Select City</option>');

                var city = JSON.parse(data);
                if(city != null)
                {
                    for(var i = 0; i < city.length; i++)
                    {
                        var data2 = "<?php if(isset($this->data)) {echo $this->data[0]['nPropertyCityIDFK'];} else {echo NULL; } ?>";
                        //console.log(data2);
                        if(data2 == city[i].nCityIDPK)
                            $('#pCityID').append('<option value='+city[i].nCityIDPK+' selected>'+city[i].tCityName+'</option>');
                        else
                            $('#pCityID').append('<option value='+city[i].nCityIDPK+'>'+city[i].tCityName+'</option>');
                    }
                    $("#pCityID").trigger("change");
                }
            }
        });
    }

    function getAreaList()
    {
        var cityID = $('#pCityID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getArea',
            type: 'POST',
            data: {cityID: cityID},
            success: function(data) {
                $('#pAreaID').empty();
                $('#pAreaID').append('<option value="">Select Area</option>');

                var area = JSON.parse(data);

                if(area != null)
                {
                    for(var i = 0; i < area.length; i++)
                    {
                        var data3 = "<?php if(isset($this->data)) {echo $this->data[0]['nPropertyAreaIDFK'];} else {echo NULL; } ?>";
                        if(data3 == area[i].nAreaIDPK)
                            $('#pAreaID').append('<option value='+area[i].nAreaIDPK+' selected>'+area[i].tAreaName+'</option>');
                        else
                            $('#pAreaID').append('<option value='+area[i].nAreaIDPK+'>'+area[i].tAreaName+'</option>');
                    }
                }
            }
        });
    }

    function property_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#property-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                       $('#property-display-preview').append(
                       ' \
                       <img style="height:100%;width:100%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });
    }

    function property_front_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#property-front-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                       $('#property-front-display-preview').append(
                       ' \
                       <img style="height:100%;width:100%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });
    }
    
</script>
   

