<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-app"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleTypes))
                                {
                                    echo "Update Property Type";
                                }
                                else
                                {
                                    echo "Add Property Type";
                                }?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

<!--                                <div class="m-portlet">-->
                                    <!--begin::Form-->
                                    <?php
                                    if (!empty($this->singleTypes))
                                    {
                                        for ($i = 0; $i < count($this->singleTypes); $i++) 
                                        {
                                        ?> 
                                            <form class="m-form" action="<?php echo Urls::$BASE; ?>properties/createType" method="POST">
                                                <div class="m-portlet__body">
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-12">
                                                            <div class="row">
                                                                <input type="hidden" name="typeID" value="<?php echo $this->singleTypes[$i]['nPropertyTypeIDPK']; ?>" id="typeID">
                                                                <div class="col-lg-12">
                                                                    <label>Place Type</label>
                                                                    <input type="text" class="form-control m-input" value="<?php echo $this->singleTypes[$i]['tPropertyTypeName']; ?>" name="typeName" id="typeName">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <div class="col-lg-2"></div>
                                                        <div class="col-lg-4">
                                                            <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update</button>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <button type="reset" class="btn default btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>properties/categories';">Cancel</button>
                                                        </div>
                                                        <div class="col-lg-2"></div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php 
                                        }
                                    }
                                    else
                                    {?>
                                        <form class="m-form" action="<?php echo Urls::$BASE; ?>properties/createType" method="POST">
<!--                                            <div class="m-portlet__body">-->
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label>Property Type</label>
                                                                <input type="text" class="form-control m-input" name="typeName" id="typeName">
                                                                <!-- oninput="check_Type_exist()" -->
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
<!--                                            </div>-->
<!--                                            <div class="m-portlet__foot m-portlet__foot--fit">-->
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="submit" id="submit">Add Property Type</button>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="reset" class="btn default btn-block" >Cancel</button>
                                                    </div>
                                                    <div class="col-lg-2"></div>
                                                </div>
<!--                                            </div>-->
                                        </form>
                                    <?php
                                    }?>
                                    <!--end::Form-->
<!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Property Types</th>
                                        <th width="15%;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($this->data)) 
                                    {
                                        for ($i = 0; $i < count($this->data); $i++) 
                                        {
                                            ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td> 
                                            <td><?php echo $this->data[$i]['tPropertyTypeName']; ?></td>
                                            <td>
                                                <form method="post" action="<?php echo Urls::$BASE ?>properties/edit_type" style="display: inline-table;">
                                                    <input type="hidden" name="typeID" value="<?php echo $this->data[$i]['nPropertyTypeIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit <?php echo $this->data[$i]['tPropertyTypeName']; ?> Detail" type="button" class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                                </form>

                                                <form style="display: inline-table;">
                                                    <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Type" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" onclick="setId(<?php echo $this->data[$i]['nPropertyTypeIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                </form>
                                                
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                    }
                                    ?>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() 
    {
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 


    }); 
    
    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });      

    function setId (del_id)
    {
        swal({
        title:"Are you sure?",
        text:"You want to delete this categories ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>properties/delete_type",
                    data:{'id':del_id},
                    success: function(data)
                    {     
                      if(data == "true")
                      {
                        location.href="<?php echo Urls::$BASE ?>properties/types";
                      }               
                    }
                });
            }   
       });
    }  
</script>
