<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    img {
    transition: -webkit-transform 0.55s ease;
    }

    img:active {
        -webkit-transform: scale(5);
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                       Property Listing
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a href="<?php echo Urls::$BASE; ?>properties/add_properties" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Add Property">
                                        <i class="fa fa-plus"></i>
                                        <!--                        <i class="la la-ellipsis-h"></i>-->
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        for ($i=0; $i < count($this->data) ; $i++) 
                                        {
                                        ?> 
                                            <tr>
                                                <td style='vertical-align: middle; color: #000000;border: 2px solid #f6f6f6;'>
                                                    <div class='col-lg-12 col-12'>
                                                        <div class='form-group row' style='margin-top: 10px;'>
                                                            <div class='col-lg-12 col-6 col-8 col-10 col-12'>
                                                                <label class='m-checkbox m-checkbox--solid m-checkbox--brand' style='font-weight: bold;'>
                                                                    <input  name='isLayout' id='isLayout_Flat' class='isLayout_Flat' type='checkbox'>
                                                                    <span></span>
                                                                    <label><?php echo $this->data[$i]['tPropertyName']; ?></label>
                                                                </label>
                                                               
                                                                <div class='col-lg-2 col-8 col-12' style='padding: 0px !important;float: right;'>

                                                                    <a target='_blank' href='<?php echo WEBSITE_BASE_URL?>property/details?propertyId=<?php echo$this->data[$i]['nPropertyIDPK'];?>&userId=<?php echo$this->data[$i]['nCreatedBy'];?>' data-toggle='m-tooltip' data-placement='top' title='' data-original-title='View Detail' class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-eye'></i></a>
                                                                  
                                                                    
                                                                    
                                                                    <form method='post' action='<?php echo Urls::$BASE; ?>properties/editProperty' style='display: inline-table;'>
                                                                        <input type='hidden' name='listingID' value='<?php echo $this->data[$i]['nPropertyIDPK']; ?>'>
                                                                        <button data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Edit Detail' type='button' class='btn btn_edit btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air'><i class='fa fa-pencil-alt'></i></button>

                                                                    </form>
                                                                    <form style='display: inline-table;'>
                                                                        <button  data-toggle='m-tooltip' data-placement='top' title='' data-original-title='Delete Place' type='button' class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air' onclick='setId(<?php echo $this->data[$i]['nPropertyIDPK']; ?>)'><i class='fa fa-trash-alt'></i></button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12'>
                                                               
                                                                <label >Price :  &nbsp;&nbsp;Rs. 2.17 Lac</label>
                                                                <label > &nbsp;&nbsp;|&nbsp;&nbsp; </label>
                                                                <label >Build Up Area : &nbsp;&nbsp;<?php echo $this->data[$i]['fPropertySuperBuiltArea']; ?></label>
                                                                <label>
                                                                    <i class='fa fa-check-circle'></i>
                                                                </label>
                                                                 &nbsp;&nbsp; &nbsp;&nbsp;<label style='background-color: #49494e; color: #fff; font-size: 10px; padding: 5px 10px !important;' class='btn m-btn--pill m-btn--air active m-btn--custom'><?php echo $this->data[$i]['tPropertyTypeName']; ?></label>
                                                               
                                                            </div>
                                                            <div class='col-lg-8'>
                                                                <label >Rera ID :  &nbsp;<?php echo $this->data[$i]['tPropertyReraID']; ?> &nbsp;:&nbsp;   <?php if ($this->data[$i]['bIsPropertyReraCertified'] == 1) 
                                                                    {
                                                                        ?>
                                                                        <i style='color: green;'>Verified</i>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                        <?php
                                                                    }
                                                                    else{
                                                                        ?>
                                                                         <i style='color: red;'>Not Verified</i>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                                        <?php
                                                                    } ?>
                                                               
                                                                </label>
                                                               <br>
                                                                <label >Posted On : &nbsp;&nbsp;<?php echo $this->data[$i]['dtCreatedOn']; ?></label>
                                                                <label > &nbsp;&nbsp;| &nbsp;&nbsp;</label>
                                                                <label >Expire On :  &nbsp;&nbsp;<?php echo $this->data[$i]['dtCreatedOn']; ?> &nbsp;|&nbsp;</label>
                                                                <!-- <label>&nbsp;&nbsp;&nbsp;&nbsp;<a target='_blank' href='<?php echo WEBSITE_BASE_URL?>property/details?propertyId=<?php echo$this->data[$i]['nPropertyIDPK'];?>&userId=<?php echo$this->data[$i]['nCreatedBy'];?>' onclick='GetLayoutList('<?php echo $this->data[$i]['nPropertyIDPK']; ?>')'>Layout Listing</a></label> -->
                                                            </div>
                                                            <div class='col-lg-2 col-6'>
                                                                <br>
                                                               
                                                                <div class='progress m-progress--sm'>
                                                                    <div class='progress-bar m--bg-danger' role='progressbar' style='width:80%;' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                            <div class='col-lg-2 col-6' style='float: left;'>
                                                               
                                                                <label> 80% Completed</label>
                                                                <label><a href=''>Complete Listing</a></label>
                                                                <br>
                                                            </div>  
                                                        
                                                            <div class='col-lg-6 col-12 col-6 col-8'>
                                                                
                                                                <label >&nbsp;&nbsp;<i class='fa fa-eye'></i> &nbsp;:  &nbsp;30 &nbsp;&nbsp; </label>
                                                                <label > &nbsp;&nbsp;| &nbsp;&nbsp;</label>
                                                                <label >&nbsp;&nbsp;<i class='fa fa-heart'></i> &nbsp;: &nbsp;50</label>
                                                                <br>
                                                                
                                                                <label ><a href=''>View 5 Response</a></label>
                                                            </div>
                                                            <div class='col-lg-6 col-10 col-12' style='float: right;'>
                                                                <div class='slideshow-container'>

                                                                    <?php
                                                                    if (!empty($this->data[$i]['Property_Images']) && isset($this->data[$i]['Property_Images'])) 
                                                                    {
                                                                        for ($j=0; $j < count($this->data[$i]['Property_Images']) ; $j++) 
                                                                        { 
                                                                        ?>
                                                                            <div class='mySlides_<?php echo $i;?>' >
                                                                                <a class='prev' style='cursor: pointer;' onclick='plusSlides('mySlides_<?php echo $i;?>',-1)'>
                                                                                    <i style='font-size: 20px;' class='fa fa-angle-left'></i>
                                                                                </a>

                                                                                <img align='center'  width='60px' height='60px' src='<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data[$i]['Property_Images'][$j]['tPropertyImageFolderPath']?>'>

                                                                                <a class='next' style='cursor: pointer;'  onclick='plusSlides('mySlides_<?php echo $i;?>',1)'>
                                                                                    <i style='font-size: 20px;' class='fa fa-angle-right'></i>
                                                                                </a>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                    }
                                                                    else{
                                                                        ?>
                                                                            <label>No Images Available</label>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() {

        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 
    });
    
    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });  

    $(document).on('click',".btn_view", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });    

    function setId (del_id)
    {
        swal({
        title:"Are you sure?",
        text:"You want to delete this Property ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>properties/deleteListing",
                    data:{'id':del_id},
                    success: function(data)
                    {     
                      if(data == "true")
                      {
                        location.href="<?php echo Urls::$BASE ?>properties/";
                      }               
                    }
                });
            }   
       });
    }  
</script>
