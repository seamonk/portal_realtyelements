<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    
    label i {
        color: #575962;
    }

    label a 
    {
        color: #575962;
        font-stretch: semi-expanded;
        text-decoration:underline;
        cursor: pointer !important;
    }
    .btn.m-btn--air.btn-default.focus, .btn.m-btn--air.btn-default:focus, .btn.m-btn--air.btn-default:hover:not(:disabled):not(.active), .btn.m-btn--air.btn-secondary.focus, .btn.m-btn--air.btn-secondary:focus, .btn.m-btn--air.btn-secondary:hover:not(:disabled):not(.active) {
        background-color: #fff !important;
        border-color: #fff !important;
        color: black !important;
    }
    .m-dropdown .m-dropdown__wrapper .m-dropdown__body {
        padding: 0px !important;
    }
    .m-dropdown .m-dropdown__wrapper .m-dropdown__inner .mCSB_container, .m-dropdown .m-dropdown__wrapper .m-dropdown__inner .mCustomScrollBox, .m-dropdown .m-dropdown__wrapper .m-dropdown__inner .m-dropdown__content, .m-dropdown .m-dropdown__wrapper .m-dropdown__inner .m-dropdown__scrollable {
        vertical-align: middle !important;
        text-align: -webkit-center !important;

    }
    .m-dropdown.m-dropdown--align-right.m-dropdown--align-push .m-dropdown__wrapper {
        margin-right: -5px !important;
    }
    .m-checkbox.m-checkbox--brand.m-checkbox--solid>input:checked ~ span {
        background: #49494e;
    }
    .m-dropdown__arrow .m-dropdown__arrow--right .m-dropdown__arrow--adjust{

        right: 5.5px !important;
    }
    .btn-sm, .btn-group-sm>.btn 
    {
        padding: 0.45rem 0.9rem 0.2em 0.9em;
        font-size: .875rem;
        line-height: 1.5;
        border-radius: .2rem;
    }    
    .btn-metal 
    {
        color: #212529;
        background-color: #49494e;
        border-color: #c4c5d6;
    }
    .dataTables_wrapper .dataTable {
        width: 100% !important;
        border-collapse: initial !important;
        border-spacing: 5px !important;
        margin: 1rem 0 !important;
    }
    td{
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
    img {
        transition: -webkit-transform 0.55s ease;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -o-user-select: none;
        user-select: none;
    }
    a{
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -o-user-select: none;
        user-select: none;
    }


    img:active {
        -webkit-transform: scale(2.5);
    }
    td {
     -moz-box-shadow: inset 0 0 5px #888;
     -webkit-box-shadow: inset 0 0 4px #888;
     box-shadow: inner 0 0 20px #888;
     
    }
    td,th{text-align: unset;}
    label{
        padding-top: 2px;
    }
</style>
<!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                       Property Listing
                                    </h3>
                                </div>
                            </div>
                            
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <?php
                                         $userType = SessionHandling::get('userType');

                                        if($userType == ADMIN)
                                        {   

                                        }
                                        else
                                        {
                                          ?>
                                              <a href="<?php echo Urls::$BASE; ?>properties/add_properties" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Add Property">
                                            <i class="fa fa-plus"></i>
                                            <!--                        <i class="la la-ellipsis-h"></i>-->
                                            </a>
                                          <?php
                                        }
                                        ?>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                    <div class="m-portlet__body">
                        <?php
                            if (SessionHandling::get('userType') != AGENT) 
                            {
                                ?>
                                <div class="row">
                                    <div class="col-lg-4 col-1"></div>
                                    <div class="col-lg-3 col-10">
                                        <label class="">Property Listing For</label>
                                        <select class="form-control m-select2 select2-selection__rendered" id="PropertyName" name="PropertyName">
                                            <option value="-1">All</option>
                                            <option value="0">Resale</option>
                                            <option value="1">Project</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 col-1"></div>
                                </div>
                                <?php
                            }
                        ?>
                        <div class="table-responsive">
                            <?php 
                                if ($this->propertiesCount == 0) 
                                {   
                                    if(SessionHandling::get('userType') == ADMIN)
                                    {
                                        ?>
                                        <div class="m-portlet__body">
                                            <br>
                                            
                                            <br>
                                            <br><br><br><br>
                                            
                                            <div class="col-lg-12">
                                                <h4 style="text-align: center;color: #ef424a ;">Oh Snap! Don't Have Any Listing Yet!</h4>
                                                <br>
                                                <!-- <center><h5 style="text-align: center;color: #ef424a ;">For Add New Listing <a  href="<?php urls::$BASE?>add_properties">Click Here</a></h5></center> -->
                                            </div>
                                            <br>
                                            <br><br><br><br>
                                            <br>
                                           
                                        </div>

                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <div class="m-portlet__body">
                                            <br>
                                            
                                            <br>
                                            <br><br><br><br>
                                            
                                            <div class="col-lg-12">
                                                <h4 style="text-align: center;color: #ef424a ;">Oh Snap! You Don't Have Any Listing Yet!</h4>
                                                <br>
                                                <center><h5 style="text-align: center;color: #ef424a ;">For Add New Listing <a  href="<?php urls::$BASE?>add_properties">Click Here</a></h5></center>
                                            </div>
                                            <br>
                                            <br><br><br><br>
                                            <br>
                                           
                                        </div>

                                        <?php
                                    }
                                    
                                }
                                else{

                                    ?>
                                    
                                    
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                        <thead>
                                            <tr>
                                            <th>Listing</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                        </tbody>
                                    </table>
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- end:: Body -->

<script type="text/javascript">

    $(document).ready(function() {

        loadDatatable();
        // showSlides("mySlides_1",1);
        $('#PropertyName').select2();
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
        
    });
    $(document).on('change',"#PropertyName", function(e){
        loadDatatable();
    });
        // var slideIndex = 1;
        // // // showSlides(slideIndex);

        // function plusSlides(id,n) {
        // var array =id.split(",");
        //     // alert(array)
        //     console.log(array);

        //   showSlides(array[0],slideIndex += array[1]);
        // }


        // function currentSlide(n) {
        //   showSlides(slideIndex = n);
        // }

        // function showSlides(id,n) {

        //   var i;
          
        //   var slides = document.getElementsByClassName(id);
            
        //   // alert(slides);
        //   if (n > slides.length) {slideIndex = 1}    
        //   if (n < 1) {slideIndex = slides.length}
        //   for (j = 0; j < slides.length; j++) {
        //       slides[i].style.display = "none";  
        //   }
        //   console.log(slides);
        //   slides[slideIndex-1].style.display = block;  
        //   console.log(slides[slideIndex-1].style.display);
        // }
    
    $(document).on('click',".btn_edit", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });  

    $(document).on('click',".btn_view", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });    

    function loadDatatable() 
    {   

        var PropertyName = $('#PropertyName').val();
        $('#s1').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo Urls::$BASE ?>properties/getUserProperty?PropertyName="+PropertyName,
            "type": "POST",
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            // "stateSave": true,
            "order":[[0,"desc"]],
            "destroy": true
        });
    }

    function setId (del_id)
    {
        swal({
        title:"Are you sure?",
        text:"You want to delete this Property ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>properties/deleteListing",
                    data:{'id':del_id},
                    success: function(data)
                    {   
                        $('#s1').DataTable().ajax.reload();
                        toastr.success("Property Deleted Successfully","Property" );
                        // window.setTimeout(function(){location.reload();}, 2000);
                        // loadDatatable();
                                     
                    }
                });
            }   
       });
    }
    
    function PropertyFeature(id,status) 
    {   
        
        if(status == 1)
        {
            swal({
            title:"Are you sure?",
            text:"You want to Feature this Property ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                     $('#'+id).val('1');
                    var status = $('#'+id).val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>properties/featureFromAdmin",
                        data:{id:id,status:status},
                        success: function(data)
                        {        
                            if (data.trim() == "true") 
                            {   
                                loadDatatable();
                                toastr.success("Property Feature Sucessfully", "Property");
                                // $('#subscriptionModal').modal('hide');              
                            }
                            else
                            {
                                toastr.error("Error While Featureing Property", "Property"); 
                            }
                            // mUtil.scrollTo("subscriptionForm", -200);    
                        }
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
           
        }
        else if(status == 0)
        {
            swal({
            title:"Are you sure?",
            text:"You want to Un-Feature this Property ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    $('#'+id).val('0');
                    var status = $('#'+id).val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>properties/featureFromAdmin",
                        data:{id:id,status:status},
                        success: function(data)
                        {        
                            if (data.trim() == "true") 
                            {   
                                loadDatatable();
                                toastr.success("Property Un-Feature Sucessfully", "Property");
                                // $('#subscriptionModal').modal('hide');              
                            }
                            else
                            {
                                toastr.error("Error While Un-Featureing Property", "Property"); 
                            }
                            // mUtil.scrollTo("subscriptionForm", -200);    
                        }
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
            
        }
    }
    function PropertyVerified(id,status,emailID,UserName) 
    {   
        
        if(status == 1)
        {
            swal({
            title:"Are you sure?",
            text:"You want to Verified this Property ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                     $('#'+id).val('1');
                    var status = $('#'+id).val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>properties/verifiedFromAdmin",
                        data:{id:id,status:status,emailID:emailID,userName:UserName},
                        success: function(data)
                        {        
                            if (data.trim() == "true") 
                            {   
                                loadDatatable();
                                toastr.success("Property Verified Sucessfully", "Property");
                                // $('#subscriptionModal').modal('hide');              
                            }
                            else
                            {
                                toastr.error("Error While Verifing Property", "Property"); 
                            }
                            // mUtil.scrollTo("subscriptionForm", -200);    
                        }
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
           
        }
        else if(status == 0)
        {
            swal({
            title:"Are you sure?",
            text:"You want to Un-Verified this Property ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    $('#'+id).val('0');
                    var status = $('#'+id).val();

                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>properties/verifiedFromAdmin",
                        data:{id:id,status:status,emailID:emailID,userName:UserName},
                        success: function(data)
                        {        
                            if (data.trim() == "true") 
                            {   
                                loadDatatable();
                                toastr.success("Property Un-Verified Sucessfully", "Property");
                                // $('#subscriptionModal').modal('hide');              
                            }
                            else
                            {
                                toastr.error("Error While Un-Verifing Property", "Property"); 
                            }
                            // mUtil.scrollTo("subscriptionForm", -200);    
                        }
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
            
        }
    }
   function GetLayoutList(id) {
      

   }

</script>
