<style type="text/css">
    .paddingLeft {
        padding-left: 0px !important;
    }

    .paddinTop {
        padding-top: 50px !important;
    }
    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    td,th{text-align: center;}
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
   
    <!-- END: Subheader -->
    <div class="m-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
                    <div class="m-accordion__item">
                        <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head"
                             data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="    false">
							<span class="m-accordion__item-icon">
								<i class="fa flaticon-map-location"></i>
							</span>
                            <span class="m-accordion__item-title">
                                <?php
                                if (!empty($this->singleTypes)) {
                                    echo "Update SubType";
                                } else {
                                    echo "Add SubType";
                                } ?>
                            </span>
                            <span class="m-accordion__item-mode"></span>
                        </div>
                        <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" "
                             role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                            <div class="m-accordion__item-content">

                                <!--                                <div class="m-portlet">-->
                                <!--begin::Form-->
                                <?php
                                if (!empty($this->singleTypes)) {
                                    for ($i = 0; $i < count($this->singleTypes); $i++) {
                                        ?>
                                        <form id="m_form" class="m-form" enctype="multipart/form-data" action="<?php echo Urls::$BASE; ?>properties/createSubType" method="POST">
                                            <div class="m-portlet__body">
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                        <div class="row">

                                                            <input type="hidden" name="subtypeID"
                                                                   value="<?php echo $this->singleTypes[$i]['nPropertySubTypeIDPK']; ?>">
                                                                   <div class="col-lg-6">
                                                                       <label>Sub Type Image</label>
                                                                        <span class="" onclick="document.getElementById('property_logo').click();">
                                                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                                                <input style="display:none;" type="file" name="property_logo" id="property_logo" onchange="property_image_preview(this)" />

                                                                                <div align="center" id="property-display-preview">
                                                                                    <?php
                                                                                    if($this->singleTypes[0]['tPropertySubTypeImagePath'] == '')
                                                                                    {?>
                                                                                        <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                                                    <?php
                                                                                    }
                                                                                    else
                                                                                    {?>
                                                                                        <img width="100px" height="100px" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->singleTypes[0]['tPropertySubTypeImagePath'];
                                                                                     ?>" />
                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-lg-8">
                                                                        <div class="col-lg-8">
                                                                            <label>Select Type</label>
                                                                            <select  placeholder="Country Name" class="form-control m-select2" name="category" id="category">
                                                                                
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-lg-8">
                                                                            <br>
                                                                            <label>SubType Name</label>
                                                                            <input type="text" class="form-control m-input" name="subtypeName" id="subtypeName" placeholder="SubType Name" value="<?php echo $this->singleTypes[0]['tPropertySubTypeName'];
                                                                                     ?>" id="subtypeName">
                                                                        </div>
                                                                    </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2"></div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-danger btn-block" name="submit" value="update" id="update">Update Sub Type
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="reset" class="btn default btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>properties/subtypes';">
                                                            Cancel
                                                        </button>
                                                    </div>
                                                    <div class="col-lg-2"></div>
                                                </div>
                                            </div>
                                        </form>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <form id="m_form" class="m-form" enctype ="multipart/form-data" action="<?php echo Urls::$BASE; ?>properties/createSubType"
                                          method="POST">
                                        <!--                                            <div class="m-portlet__body">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                       <label>Sub Type Image</label>
                                                        <span class="" onclick="document.getElementById('property_logo').click();">
                                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                                <input style="display:none;" type="file" name="property_logo" id="property_logo" onchange="vpb_image_preview(this)" />

                                                                <div align="center" id="vpb_image_preview">
                                                                    <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="col-lg-8">
                                                            <label>Select Type</label>
                                                            <select  placeholder="Country Name" class="form-control m-select2" name="category" id="category">
                                                                
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <br>
                                                            <label>SubType Name</label>
                                                            <input type="text" class="form-control m-input" name="subtypeName" placeholder="SubType Name" id="subtypeName">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--                                            </div>-->
                                        <!--                                            <div class="m-portlet__foot m-portlet__foot--fit">-->
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-danger btn-block" name="submit" id="submit" value="submit">
                                                    Add Sub Type
                                                </button>
                                            </div>
                                            <div class="col-lg-4">
                                                <button type="reset" class="btn default btn-block" >Cancel
                                                </button>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                        <!--                                            </div>-->
                                    </form>
                                    <?php
                                } ?>
                                <!--end::Form-->
                                <!--                                </div>  </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Begin::Section-->

        <!--End::Section-->

        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">

                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <!--begin: Datatable -->
                            <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sub Type Name</th>
                                    <th>Type Name</th>
                                    <th>Sub Type Image</th>
                                    <!-- <th>Actions</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($this->data)) {
                                    for ($i = 0; $i < count($this->data); $i++) {
                                        ?>
                                        <tr>
                                            <td><?php echo $i + 1; ?></td>
                                            <td><?php echo $this->data[$i]['tPropertySubTypeName']; ?></td>
                                            <td><?php echo $this->data[$i]['tPropertyTypeName']; ?></td>
                                            <td><center>
                                                    <?php
                                                    if (!empty($this->data[$i]['tPropertySubTypeImagePath'])) 
                                                    {
                                                        ?>
                                                            <img width="50px" height="50px" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data[$i]['tPropertySubTypeImagePath'];?>"> 
                                                        <?php
                                                    }
                                                    else{
                                                        ?>
                                                            <img class="m--img-rounded m--marginless" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.'no-image.png'; ?>"style="height: 4em;width: 4em; box-shadow: 1px 0px 9px #060606;">    
                                                        <?php
                                                    }


                                                    ?>
                                                </center></td>
                                            <!-- <td>
                                                <form method="post" action="<?php echo Urls::$BASE ?>properties/edit_Subtype"
                                                      style="display: inline-table;">
                                                    <input type="hidden" name="editSubTypeID"
                                                           value="<?php echo $this->data[$i]['nPropertySubTypeIDPK']; ?>">
                                                    <button data-toggle="m-tooltip" data-placement="top" title=""
                                                            data-original-title="Edit <?php echo $this->data[$i]['tPropertySubTypeName']; ?> Detail"
                                                            type="button"
                                                            class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                        <i class="fa fa-pencil-alt"></i></button>
                                                </form>

                                                <form style="display: inline-table;">
                                                    <button data-toggle="m-tooltip" data-placement="top" title=""
                                                            data-original-title="Delete SubType" type="button"
                                                            class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"
                                                            onclick="setId(<?php echo $this->data[$i]['nPropertySubTypeIDPK']; ?>)">
                                                        <i class="fa fa-trash-alt"></i></button>
                                                </form>

                                            </td> -->
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function ()
    {
        toastr.options = {
            "closeButton": true,
            "progressBar": true,
            "positionClass": "toast-top-center"
        };

        $("#category").select2({
            placeholder: "-- Select --"
        });

        $('#s1').DataTable({
            "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
            "columnDefs": [
                {"orderable": false,"targets": [0,0]}   
            ]
        });

        $("#m_form").validate({
            rules: {
                category: {
                    required: true
                },
                subtypeName :{
                    required: true
                }
            }
        });


        $("#update").click(function(e)
        {
            if($('#select2Country').val() == '')
            {
                toastr.error("Please select country name.", "Country Name");
                e.preventDefault();
            }
            else if($('#subtypeName').val() == '')
            {
                toastr.error("Please enter subtype name.", "SubType Name");
                e.preventDefault();
            }
        });
    });

    getTypeDetails();   // fetch all country and append it to the dropdown

    $(document).on('click', ".btn_edit", function (e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        // console.log(l);
        //console.log(v);

        form.submit();
    });

    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this SubType ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>properties/deleteSubType",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>properties/subtypes";
                        }
                    }
                });
            }
        });
    }

    function getTypeDetails()   // fetch all country list
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getTypeDetails',
            type: 'POST',
            success: function (data) {
                $('#category').empty();
                $('#category').append('<option value="">-- Select category --</option>');
                var country = JSON.parse(data);
                for (var i = 0; i < country.length; i++) {
                    var data1 = "<?php if (isset($this->singleTypes)) {
                        echo $this->singleTypes[0]['nPropertyTypeIDPK'];
                    } else {
                        echo NULL;
                    } ?>";
                    if (data1 == country[i].nPropertyTypeIDPK)
                        $('#category').append('<option value=' + country[i].nPropertyTypeIDPK + ' selected>' + country[i].tPropertyTypeName + '</option>');
                    else
                        $('#category').append('<option value=' + country[i].nPropertyTypeIDPK + '>' + country[i].tPropertyTypeName + '</option>');
                }
                $("#category").trigger("change");
            }
        });
    }
    function property_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#property-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                       $('#property-display-preview').append(
                       ' \
                       <img style="height:100%;width:100%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });
    }
     function vpb_image_preview(vpb_selector_) 
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#vpb_image_preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                       $('#vpb_image_preview').append(
                       '<div id="selector_'+vpb_o_+'" class="vpb_wrapper"> \
                       <img class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br /> \
                       <a style="cursor:pointer;padding-top:5px;" title="Click here to remove '+ escape(file.name) +'" \
                       onclick="vpb_remove_selected(\''+vpb_o_+'\',\''+file.name+'\')">Remove</a> \
                       </div>');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });
    }

    //Remove Previewed File only from the screen but will be uploaded when you click on the start upload button
    function vpb_remove_selected(id,name)
    {
        var get_value = $('#deleteImg').val();

        if(get_value == '')
        {
            $('#deleteImg').val(name);
        }
        else
        {
            $('#deleteImg').val(get_value+','+name);
        }
        $('#v-add-'+id).remove();
        $('#selector_'+id).fadeOut();
    }
</script>
