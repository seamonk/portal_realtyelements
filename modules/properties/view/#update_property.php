
<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    .buttonDemo{
        background-color: transparent;

    }
    .m-select2{ 
        border-color: #ffffff !important;
    }
    
    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;  -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    label {
    font-weight: 400 !important;
    }
    .m-form .form-control-feedback {
   
    color: red;
}

.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-label {
    font-weight: bold;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span {
    background-color: #ef424a;
}
/*data-toggle="m-tooltip" data-placement="bottom" data-offset="100px 0px" data-skin="dark" title="" data-original-title="Click To Select Image"*/
.tickClassOverImg
{
    position: absolute;
    top: 43%;
    left: 60%;
    right: 32%;
    border-radius: 28px;
    display: none;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--done .m-wizard__step-number > span {
    background-color: #ef424a;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--done .m-wizard__step-label {
    font-weight: bold;
}
.listingForLabel
{
    font-size: 20px;
    font-weight: bold;
    margin: 15px;
}
</style>


<div class="m-content">
    <!--Begin::Main Portlet-->
    <div class="m-portlet m-portlet--full-height">
        <!--begin: Portlet Head-->
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Add Property
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <a href="<?php echo Urls::$BASE; ?>properties" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listings">
                            <i class="fa fa-list"></i>              
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!--end: Portlet Head-->
        <!--begin: Portlet Body-->
        <div class="m-portlet__body m-portlet__body--no-padding">            
            <!--begin: Form Wizard-->
            <div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">
                <!--begin: Message container -->
                <div class="m-portlet__padding-x">                  
                </div>
                <!--end: Message container -->
                <div class="row m-row--no-padding">
                    <div class="col-lg-6 col-lg-12">

                        <!--begin: Form Wizard Head -->
                        <div class="m-wizard__head">

                            <!--begin: Form Wizard Progress -->
                            <div class="m-wizard__progress">
                                <div class="progress">
                                    <div class="progress-bar" style="background-color: #ef424a;" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>

                            <!--end: Form Wizard Progress -->

                            <!--begin: Form Wizard Nav -->
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number" >
                                                <span style="background-color: #ef424a;">
                                                    <span>1</span>
                                                </span>
                                            </a>                                           
                                            <div class="m-wizard__step-label">
                                                Basic Information
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>2</span>
                                                </span>
                                            </a>                                           
                                            <div class="m-wizard__step-label">
                                                Location Setup
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>3</span>
                                                </span>
                                            </a>                                            
                                            <div class="m-wizard__step-label">
                                                Property Details
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>4</span>
                                                </span>
                                            </a>                                           
                                            <div class="m-wizard__step-label">
                                                Pricing
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_5">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>5</span>
                                                </span>
                                            </a>                                           
                                            <div class="m-wizard__step-label">
                                                Features
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--end: Form Wizard Nav -->
                        </div>

                        <!--end: Form Wizard Head -->
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                    <?php if(!empty($this->data)) { ?>
                        <!--begin: Form Wizard Form-->
                        <div class="m-wizard__form">
                            <form class="m-form" enctype="multipart/form-data" id="m_form">
                                <!--begin: Form Body -->
                                <div class="m-portlet__body m-portlet__body">
                                    <!--begin: Form Wizard Step 1-->
                                    <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                        <div class=" m-form__section m-form__section--first">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>Property For</label>
                                                     <select  class="form-control m-select2" name="propFor" id="propFor">
                                                       <option value="0" <?php if($this->data['nPropertyFor'] == "0" ){ echo "selected"; } ?> >Buy</option>
                                                       <option value="1" <?php if($this->data['nPropertyFor'] == "1" ){ echo "selected"; } ?> >Rent</option>
                                                    </select>
                                                </div>     
                                                <div class="col-lg-6">
                                                    <label>Property Type</label>
                                                    <select  class="form-control m-select2" name="propertyType" id="propList">
                                                       <?php
                                                        if (!empty($this->getPropertyType)) {    
                                                            for ($i=0; $i < count($this->getPropertyType) ; $i++) 
                                                            { 
                                                            ?>
                                                                <option value="<?php echo $this->getPropertyType[$i]['nPropertyTypeIDPK']?>" <?php if($this->data['nPropertyTypeIDFK'] == $this->getPropertyType[$i]['nPropertyTypeIDPK']){ echo "selected"; } ?> ><?php echo $this->getPropertyType[$i]['tPropertyTypeName']?></option>
                                                            <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                               <div class="col-lg-12" id="Residential" style="border: 2px solid #f7f7f7; padding: 10px;  text-align: center; vertical-align: middle; margin-top: 20px;">
                                                    <div class="col-lg-12 row" id="propertySubTypes">
                                                    </div>
                                                     <input type="hidden" name="propertiesSubType" id="propertiesSubType" value="">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group row" id="selectTagProp" style="display: none;">
                                                <div class="col-lg-6">

                                                    <label id="PropTypeName"></label>
                                                    <br>
                                                    <select name="typeofmain" style="width: 100%; display: none;" class="form-control m-select2" id="typeofmain">

                                                        <!-- <option>-- Select --</option> -->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 1-->
                                    <!--begin: Form Wizard Step 2-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>House No.</label>
                                                    <input type="text" class="form-control" name="propertyNumber" id="propertyNumber" value="<?php echo $this->data['tPropertyNumber']; ?>" />
                                                    
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Street Name</label>
                                                   <input type="text" class="form-control" name="streetName" id="streetName"  value="<?php echo $this->data['tPropertyStreetName']; ?>" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <label>Address 1</label>
                                                    <textarea class="form-control" name="pAddress" id="pAddress" rows="3"><?php echo $this->data['tPropertyAddress']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group  row">
                                                <div class="col-lg-12">
                                                    <label>Select Location</label>
                                                    <select class="form-control m-select2" id="locationID" name="locationID" style="width: 100% !important" >
                                                        <option selected>Enter Area Name</option>
                                                    </select>
                                                    <input type="hidden" name="cityID" id="cityID" value="<?php echo $this->data['nPropertyCityIDFK']; ?>">
                                                    <input type="hidden" name="stateID" id="stateID" value="<?php echo $this->data['nPropertyStateIDFK']; ?>">
                                                    <input type="hidden" name="countryID" id="countryID" value="<?php echo $this->data['nPropertyCountryIDFK']; ?>">
                                                </div>                                               
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                   <label>Property Latitude</label>
                                                    <input type="text" class="form-control m-input" name="pLatitutde" id="pLatitutde" placeholder="Enter Property Latitude" >
                                                </div>
                                                <div class="col-lg-6">
                                                   <label>Property Longitude</label>
                                                    <input type="text" class="form-control m-input" name="pLongitude" id="pLongitude" placeholder="Enter Property Longitude">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                   <label>Map Location Link</label>
                                                    <input type="text" class="form-control m-input" name="mapLocationLink" id="mapLocationLink" placeholder="Enter Map Location Link" value="<?php echo $this->data['tPropertyMapLocationLink']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 2-->
                                    <!--begin: Form Wizard Step 3-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_3">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>Super Build Up Area</label>
                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Build Up Area" class="form-control m-input" id="SuperBuildUpArea" name="SuperBuildUpArea" value="<?php echo $this->data['fPropertySuperBuiltArea']; ?>">
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Area Unit</label>
                                                    <br>
                                                    <select style="width: 100%;" name="UnitID" id="UnitID" class="form-control m-select2">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="m-accordion m-accordion" id="m_accordion_3" style="margin-bottom: 20px;">
                                                <div class="m-accordion__item">
                                                    <div class="m-accordion__item-head" role="tab" id="m_accordion_3_item_3_head" data-toggle="collapse" href="#m_accordion_3_item_3_body" aria-expanded="false">
                                                        <span class="m-accordion__item-icon">
                                                            <i class="la la-angle-down" style="font-size: 17px;"></i>
                                                        </span>
                                                        <span class="m-accordion__item" style="margin-left: -10%;">Add More Details About Area</span>
                                                    </div>
                                                    <div class="m-accordion__item-body" id="m_accordion_3_item_3_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_3_head" data-parent="#m_accordion_1">
                                                        <div class="m-accordion__item-content">
                                                             <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <label>Build Up Area</label>
                                                                        <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Build Up Area" class="form-control m-input" name="BuildUpArea" value="<?php echo $this->data['fPropertyBuiltArea']; ?>">
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <label>Area Unit</label>
                                                                        <select style="width: 100%;" class="form-control m-select2" name="bArea" id="bArea" disabled>
                                                                            <option>-- Select --</option>
                                                                        </select>
                                                                    </div>                         
                                                                    <div class="col-lg-6">
                                                                        <br>
                                                                        <label>Carpet Area</label>
                                                                        <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Build Up Area" class="form-control m-input" id="carpetArea" name="carpetArea" value="<?php echo $this->data['fPropertyCarpetArea']; ?>">
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <br>
                                                                        <label>Area Unit</label>
                                                                        <select style="width: 100%;" class="form-control m-select2" name="cArea" id="cArea" disabled>
                                                                            <option>-- Select --</option>
                                                                        </select>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Property Furnished</label>
                                                    <br>
                                                    <select id="propertyFurnishedID" name="propertyFurnishedID" class="form-control m-select2" style="width: 100%;">
                                                       <option value="1">Furnished</option>
                                                       <option value="0">Not Furnished </option> 
                                                       <option value="2">Semi-Furnished </option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Select BHK</label>
                                                    <select style="width: 100%;" name="propBHK" class="form-control m-select2" id="propBHK">
                                                        <option value="">-- Select --</option>
                                                        <option value="1">1 </option>
                                                        <option value="2">2 </option>
                                                        <option value="3">3 </option>
                                                        <option value="3+">3+ </option>
                                                    </select>
                                                </div>
                                                 <div class="col-lg-4">
                                                    <label>Select Facing</label>
                                                    <select style="width: 100%;" name="facingID" class="form-control m-select2" id="facingID">
                                                        <option value="">-- Select --</option>
                                                         <option value="1">1 </option>
                                                        <option value="2">2 </option>
                                                        <option value="3">3 </option>
                                                        <option value="4">4 </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="m-accordion m-accordion" id="m_accordion_4" style="margin-bottom: 20px;">
                                                <div class="m-accordion__item">
                                                    <div class="m-accordion__item-head" role="tab" id="m_accordion_3_item_4_head" data-toggle="collapse" href="#m_accordion_3_item_4_body" aria-expanded="false">
                                                        <span class="m-accordion__item-icon">
                                                            <i class="la la-angle-down" style="font-size: 17px;"></i>
                                                        </span>
                                                        <span class="m-accordion__item" style="margin-left: -10%;">Add More Details About BHK</span>
                                                    </div>
                                                    <div class="m-accordion__item-body" id="m_accordion_3_item_4_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_4_head" data-parent="#m_accordion_4">
                                                        <div class="m-accordion__item-content">
                                                            <div class="form-group row">
                                                               <div class="col-lg-4">
                                                                    <label>Bedrooms</label>
                                                                    <select style="width: 100%;" class="form-control m-select2" name="Bedrooms" id="Bedrooms">
                                                                        <option value="">-- Select --</option>
                                                                        <option value="1" <?php if($this->data['nBedrooms'] == "1"){ echo "selected"; } ?> >1</option>
                                                                        <option value="2" <?php if($this->data['nBedrooms'] == "2"){ echo "selected"; } ?> >2</option>
                                                                        <option value="3" <?php if($this->data['nBedrooms'] == "3"){ echo "selected"; } ?> >3</option>
                                                                        <option value="4" <?php if($this->data['nBedrooms'] == "4"){ echo "selected"; } ?> >4</option>
                                                                        <option value="5" <?php if($this->data['nBedrooms'] == "5"){ echo "selected"; } ?> >5</option>
                                                                        <option value="6" <?php if($this->data['nBedrooms'] == "6"){ echo "selected"; } ?> >6</option>
                                                                        <option value="7" <?php if($this->data['nBedrooms'] == "7"){ echo "selected"; } ?> >7</option>
                                                                        <option value="8" <?php if($this->data['nBedrooms'] == "8"){ echo "selected"; } ?> >8</option>
                                                                        <option value="9" <?php if($this->data['nBedrooms'] == "9"){ echo "selected"; } ?> >9</option>
                                                                        <option value="9+" <?php if($this->data['nBedrooms'] == "9+"){ echo "selected"; } ?> >9+</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label>Bathrooms</label>
                                                                    <select style="width: 100%;" class="form-control m-select2" name="Bathrooms" id="Bathrooms">
                                                                        <option value="">-- Select --</option>
                                                                       
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label>Balconies</label>
                                                                    <select style="width: 100%;" class="form-control m-select2" name="Balconies" id="Balconies">
                                                                        <option value="">-- Select --</option>
                                                                        
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-lg-4">
                                                                    <label>Kitchens</label>
                                                                    <select style="width: 100%;" class="form-control m-select2" name="kitchen" id="kitchen">
                                                                        <option value="">-- Select --</option>                                               <option value="1" <?php if($this->data['nKitchens'] == "1"){ echo "selected"; } ?> >1 </option>
                                                                            <option value="2" <?php if($this->data['nKitchens'] == "2"){ echo "selected"; } ?> >2 </option>
                                                                            <option value="3" <?php if($this->data['nKitchens'] == "3"){ echo "selected"; } ?> >3 </option>
                                                                            <option value="3+" <?php if($this->data['nKitchens'] == "3+"){ echo "selected"; } ?> >3+ </option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                   <label>Property Total Floors</label>
                                                                    <select style="width: 100%;" class="form-control m-select2" name="tFloor" id="tFloor">
                                                                        <option value="">-- Select --</option>
                                                                        <option value="1" <?php if($this->data['nPropertyOnFloor'] == "1"){ echo "selected"; } ?> >1</option>
                                                                        <option value="2" <?php if($this->data['nPropertyOnFloor'] == "2"){ echo "selected"; } ?> >2</option>
                                                                        <option value="3" <?php if($this->data['nPropertyOnFloor'] == "3"){ echo "selected"; } ?> >3</option>
                                                                        <option value="4" <?php if($this->data['nPropertyOnFloor'] == "4"){ echo "selected"; } ?> >4</option>
                                                                        <option value="5" <?php if($this->data['nPropertyOnFloor'] == "5"){ echo "selected"; } ?> >5</option>
                                                                        <option value="6" <?php if($this->data['nPropertyOnFloor'] == "6"){ echo "selected"; } ?> >6</option>
                                                                        <option value="7" <?php if($this->data['nPropertyOnFloor'] == "7"){ echo "selected"; } ?> >7</option>
                                                                        <option value="8" <?php if($this->data['nPropertyOnFloor'] == "8"){ echo "selected"; } ?> >8</option>
                                                                        <option value="9" <?php if($this->data['nPropertyOnFloor'] == "9"){ echo "selected"; } ?> >9</option>
                                                                        <option value="10" <?php if($this->data['nPropertyOnFloor'] == "10"){ echo "selected"; } ?> >10</option>
                                                                        <option value="11" <?php if($this->data['nPropertyOnFloor'] == "11"){ echo "selected"; } ?> >11</option>
                                                                        <option value="12" <?php if($this->data['nPropertyOnFloor'] == "12"){ echo "selected"; } ?> >12</option>
                                                                        <option value="13" <?php if($this->data['nPropertyOnFloor'] == "13"){ echo "selected"; } ?> >13</option>
                                                                        <option value="14" <?php if($this->data['nPropertyOnFloor'] == "14"){ echo "selected"; } ?> >14</option>
                                                                        <option value="15" <?php if($this->data['nPropertyOnFloor'] == "15"){ echo "selected"; } ?> >15</option>
                                                                        <option value="16" <?php if($this->data['nPropertyOnFloor'] == "16"){ echo "selected"; } ?> >16</option>
                                                                        <option value="17" <?php if($this->data['nPropertyOnFloor'] == "17"){ echo "selected"; } ?> >17</option>
                                                                        <option value="18" <?php if($this->data['nPropertyOnFloor'] == "18"){ echo "selected"; } ?> >18</option>
                                                                        <option value="19" <?php if($this->data['nPropertyOnFloor'] == "19"){ echo "selected"; } ?> >19</option>
                                                                        <option value="19+" <?php if($this->data['nPropertyOnFloor'] == "19+"){ echo "selected"; } ?> >19+</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label>Property Floor</label>
                                                                    <select style="width: 100%;" class="form-control m-select2" name="pFloor" id="pFloor">
                                                                         <option value="">-- Select --</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <br>                         
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">
                                                                    <p style="width: 100%;">Store Room</p>
                                                                    <input  name="storeRoomAvailable" id="Room" value="1" class="nbrokerage"  type="checkbox" <?php if($this->data['bIsStoreRoomAvailable'] == 1) { echo "checked=checked"; } ?> >
                                                                    <span></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <br>
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">
                                                                    <p style="width: 100%;">Pooja Room</p>
                                                                    <input  name="poojaRoomAvailable" id="Room" value="1" class="nbrokerage"  type="checkbox" <?php if($this->data['bIsPoojaRoomAvailable'] == 1) { echo "checked=checked"; } ?> >
                                                                    <span></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <br>
                                                <label>RERA Regestration Status:</label>&nbsp;&nbsp;&nbsp;
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <br>
                                                        <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                            YES
                                                        <input  name="rerayes" id="rerayes" value="yes" class="isAnswer"  type="radio" <?php if($this->data['bIsPropertyReraCertified'] == 1) { echo "checked=checked"; } ?>>
                                                        <span></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <br>
                                                        <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                            NO
                                                        <input  name="rerayes" id="rerayes" value="no" class="isAnswer" type="radio" <?php if($this->data['bIsPropertyReraCertified'] == 0) { echo "checked=checked"; } ?>>
                                                        <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="display: none;" id="reraid">
                                            <div class="col-lg-12">
                                                <label>Please Enter Your RERA Certification ID</label>
                                                <input type="text" class="form-control m-input" name="reraid" id="reraID" value="<?php echo $this->data['tPropertyReraID']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <label>Description</label>
                                                <textarea  class="form-control m-input" name="propertyDescription" id="propertyDescription" rows="5"><?php echo $this->data['tPropertyDescription']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-12">
                                                <br>
                                                <label>Are You Have Parking ?</label>&nbsp;&nbsp;&nbsp;
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <br>
                                                        <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                         YES
                                                        <input  name="Parkingyes" id="Parkingyes" value="yes" class="Parkingyes"  type="radio" <?php if($this->data['nCarParkings'] == 1) { echo "checked=checked"; } ?> >
                                                        <span></span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <br>
                                                        <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                        NO
                                                        <input  name="Parkingyes" id="Parkingyes" value="no" class="Parkingyes" type="radio" <?php if($this->data['nCarParkings'] == 0) { echo "checked=checked"; } ?> >
                                                        <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>       
                                    </div>
                                    <!--end: Form Wizard Step 3-->
                                    <!--begin: Form Wizard Step 4-->
                                    <div class="m-wizard__form-step " id="m_wizard_form_step_4">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Availability</label>
                                                    <br>
                                                    <select style="width: 100%;" class="form-control m-select2" id="Availability" name="Availability">
                                                        <option value="">-- Select --</option>
                                                        <option value="1" <?php if($this->data['nPropertyAvailability'] == 1) { echo "selected"; } ?> >Under Constructions</option>
                                                        <option value="0" <?php if($this->data['nPropertyAvailability'] == 0) { echo "selected"; } ?> >Ready To Move</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Possession By</label>
                                                    <input type="text" style="width: 100%;" class="form-control datepicker" id="possession" name="possession" value="<?php echo $this->data['dtPropertyPossesionDate']; ?>">
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Ownership</label>
                                                    <select style="width: 100%;" class="form-control m-select2" id="ownership" name="ownership">
                                                        <option value="">-- Select --</option>
                                                        <option value="Freehold" <?php if($this->data['tPropertyOwnership'] == "Freehold") { echo "selected"; } ?> >Freehold </option>
                                                        <option value="Leasehold" <?php if($this->data['tPropertyOwnership'] == "Leasehold") { echo "selected"; } ?> >Leasehold </option>
                                                        <option value="Co-operative Society" <?php if($this->data['tPropertyOwnership'] == "Co-operative Society") { echo "selected"; } ?> >Co-operative Society </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-4">
                                                    <label>Expected Price</label>
                                                    <input type="text" class="form-control m-input" name="Expected" id="Expected" onkeypress="return isNumberKey(event);" value="<?php echo $this->data['fPropertyExpectedPrice']; ?>">

                                                </div>
                                                <div class="col-lg-4" style="padding-top: 5%;">
                                                    <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                                    Expected Price [Negotiable]
                                                    <input  name="ExpectedNegociable" id="ExpectedNegociable" value="yes" class="ExpectedNegociable"  type="checkbox" <?php if($this->data['bIsExpectedPriceNegotiable'] == 1) { echo "checked=checked"; } ?> >
                                                    <span></span>
                                                    </label>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>Booking Price</label>
                                                    <input type="text" class="form-control m-input" name="BookingPrize" id="Booking" onkeypress="return isNumberKey(event);" value="<?php echo $this->data['nPropertyBookingPrice']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <br>
                                                    <label>Do You Have Brokerage Charge?</label>&nbsp;&nbsp;&nbsp;
                                                    <div class="row">
                                                        <div class="col-lg-3">
                                                            <br>
                                                            <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                             YES
                                                                <input  name="brokerageYes" id="brokerageYes" value="yes" class="brokerageYes"  type="radio" <?php if($this->data['fPropertyBrokerageCharge'] != "") { echo "checked=checked"; } ?> >
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <br>
                                                            <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                                NO
                                                                <input  name="brokerageYes" id="brokerageYes" value="no" class="brokerageYes" type="radio" <?php if($this->data['fPropertyBrokerageCharge'] == "") { echo "checked=checked"; } ?> >
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="display: none;" id="Brokerage">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <label>Brokerage Amount</label>
                                                            <input type="text" class="form-control m-input" name="brokerageAmount" id="brokerage" placeholder="Enter Brokerage Amount" value="<?php echo $this->data['fPropertyBrokerageCharge']; ?>">
                                                        </div>
                                                        <div class="col-lg-4" style="padding-top: 5%;">
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                                             Brokerage Charge [Negotiable]
                                                                <input  name="nbrokerage" id="nbrokerage" value="yes" class="nbrokerage"  type="checkbox" <?php if($this->data['bIsPropertyBrokerageChargeNegotiable'] == 1) { echo "checked=checked"; } ?> >
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label>Brokerage Amount In</label>
                                                            <select style="width: 100%;" class="form-control m-select2" id="brokerageAmountIn" name="brokerageAmountIn">                 
                                                                <option value="1" <?php if($this->data['bIsPropertyBrokerageChargeIn'] == 1) { echo "selected"; } ?>  >Percentage (%)</option>
                                                                <option value="0" <?php if($this->data['bIsPropertyBrokerageChargeIn'] == 0) { echo "selected"; } ?>  >Fixed Amount (Rs)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-accordion m-accordion" id="m_accordion_1" style="margin-bottom: 20px;">
                                                <div class="m-accordion__item">
                                                    <div class="m-accordion__item-head" role="tab" id="m_accordion_3_item_1_head" data-toggle="collapse" href="#m_accordion_3_item_1_body" aria-expanded="false">
                                                        <span class="m-accordion__item-icon">
                                                            <i class="la la-angle-down" style="font-size: 17px;"></i>
                                                        </span>
                                                        <span class="m-accordion__item"> Add More Details About Price [Recommended]</span>
                                                    </div>
                                                    <div class="m-accordion__item-body" id="m_accordion_3_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_1_head" data-parent="#m_accordion_1">
                                                        <div class="m-accordion__item-content">
                                                             <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-lg-4">
                                                                        <label id="perUnitPrice">Price Per Unit</label>
                                                                        <input type="text" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="PricePerUnit" id="PricePerUnit" value="<?php echo $this->data['fPricePerUnitArea']; ?>">
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <label>Maintenance</label>
                                                                        <input type="text" onkeypress="return isNumberKey(event);" placeholder="Maintenance" class="form-control m-input" name="MaintenanceAmount" id="MaintenanceAmount" value="<?php echo $this->data['fPropertyMaintenancePrice']; ?>">
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <label>Maintenance For</label>
                                                                        <select style="width: 100%;"     class="form-control m-select2" name="maintenanceFor" id="maintenanceFor">
                                                                            <option value="">-- Select --</option>
                                                                            <option value="0" <?php if($this->data['nPropertyMaintenanceFor'] == 0) { echo "selected"; } ?> >Monthly</option>
                                                                            <option value="1" <?php if($this->data['nPropertyMaintenanceFor'] == 1) { echo "selected"; } ?> >Yearly</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 4-->
                                    <!--begin: Form Wizard Step 5-->
                                    <div class="m-wizard__form-step " id="m_wizard_form_step_5">
                                        <div class="m-form__section m-form__section--first">
                                            <div class="m-form__heading">
                                                <h2 class="m-form__heading-title">Features Details</h2>
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                   <label>Property Images</label>
                                                    <span class="" onclick="document.getElementById('property_logo').click();">
                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                            <input style="display:none;" type="file" name="property_logo[]" id="property_logo" onchange="property_image_preview(this)" multiple />

                                                            <div align="center" id="property-display-preview">
                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                                <br>
                                                <br>
                                            </div>
                                            <label id="imgCount">Images Selected : ( 0 / 25 )</label>
                                            <!-- <br><br> -->

                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                    <div style="padding: 10px;border-radius: 10px;width:100%;">
                                                        <h5 style="color: black;"> Uploaded Images :</h5><br>
                                                        <?php
                                                        if (!empty($this->data['ListingImages'])) 
                                                        {
                                                            ?>
                                                            <input type="hidden" name="Image_FolderName" value="<?php echo $this->data['ListingImages'][0]['tPropertyImageFolderPath']?>">
                                                            <?php
                                                            for ($i = 0; $i < count($this->data['ListingImages']); $i++) 
                                                            {
                                                            ?>
                                                               <div id="hide_place_Img<?php echo $this->data['ListingImages'][$i]['nPropertyImageIDPK']; ?>" class="vpb_wrapper">     <a href="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data['ListingImages'][$i]['tPropertyImageFolderPath']; ?>" target="_blank">
                                                                        <img class="vpb_image_style" class="img-thumbnail" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data['ListingImages'][$i]['tPropertyImageFolderPath']; ?> " />
                                                                    </a><br /> 
                                                                    <?php
                                                                        if ($this->data['ListingImages'][$i]['bIsFrontImage'] == 0) 
                                                                        {
                                                                           ?>
                                                                            <a style="cursor:pointer;padding-top:5px;" title="Click here to remove" onclick="remove_listing_image('<?php echo $this->data['ListingImages'][$i]['nPropertyImageIDPK']; ?>')">
                                                                                Remove
                                                                            </a> 
                                                                           <?php
                                                                        }
                                                                    ?>
                                                                    
                                                                </div>
                                                            <?php
                                                            }
                                                        }
                                                        else
                                                        { ?>
                                                           <center><h6>"No Images uploaded"</h6></center>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="from-group">
                                                <label>Amenities </label>
                                                <div class="col-lg-12" id="Aminities" style="text-align: center; vertical-align: middle;">
                                                    <div class="col-lg-12">
                                                        <input type="hidden" name="AminitiesHidden" id="AminitiesHidden" value="">
                                                        <div class="row" id="Amenities">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br><br>                                            
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">
                                                   <label>Property Brochure</label><br>
                                                <input  type="file" name="brochurePath" id="brochurePath" accept="application/pdf" />                            
                                                </div>                                                
                                            </div>
                                            <div class="form-group m-form__group row">
                                                <div class="col-lg-12">                        
                                                 Uploaded Brochure : <a href="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING.$this->data['tPropertyBrochurePath']; ?>" id="" target="_blank">View Brochure</a>
                                             </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 5-->
                                </div>
                                <!--end: Form Body -->
                                <!--begin: Form Actions -->
                                <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-6 m--align-left">
                                                <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                    <span>
                                                        <i class="la la-arrow-left"></i>&nbsp;&nbsp;
                                                        <span>Back</span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="col-lg-6 m--align-right">
                                                <input type="hidden" name="submit" value="submit">
                                                <button class="btn btn-danger btn-block" data-wizard-action="submit" name="submit" value="submit" type="button">Add Property</button>
                                                <a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                    <span>
                                                        <span>Save & Continue</span>&nbsp;&nbsp;
                                                        <i class="la la-arrow-right"></i>
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Actions -->
                            </form>
                        </div>
                        <!--end: Form Wizard Form-->
                    <?php } ?>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
            <!--end: Form Wizard-->
        </div>
        <!--end: Portlet Body-->
    </div>
    <!--End::Main Portlet-->
</div>


<script type="text/javascript">
    $(document).ready(function() 
    {
      
        WizardDemo.init();

        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
        
        $('#propFor').select2();

        /*----------START : LOCAIONS------------*/

            $('#locationID').select2();
            getLocations();

        /*----------END : LOCAIONS------------*/
        
        $('#propertyFurnishedID').select2();
        $('#maintenanceFor').select2();
        $('#brokerageAmountIn').select2();
        $('#pCategoryID').select2();
        $('#Availability').select2();
        $('#ownership').select2();
        $('#maintenance').select2();      
        $('#propBHK').select2();  
        $('#kitchen').select2();        
        $('#facingID').select2();
        $('#UnitID').select2();
        $('#bArea').select2();
        $('#cArea').select2();
        $('#typeprop').select2();
        $('#propType').select2();
        $('#propList').select2();
        $('#propListtwo').select2();
        $('#propTypeone').select2();
        $('#furnished').select2();
        $('#pFloor').select2();
        $('#tFloor').select2();
        $('#Bedrooms').select2();
        $('#Bathrooms').select2();
        $('#Balconies').select2();
        $('#furnished1').select2();
        $('#pReportingTime').select2();

        $('#UnitID').on('change',function(){
            var data = $('#UnitID option:selected').text();
            if ($('#UnitID').val() != "" && $('#UnitID').val() != null) {
                $('#bArea').html('<option value="'+$('#UnitID').val()+'">'+data+'</option>');
                $('#cArea').html('<option value="'+$('#UnitID').val()+'">'+data+'</option>');
            }

        });

        var balconyValue = "<?php if(isset($this->data)) {echo $this->data['nBalconies']; } else { echo NULL; } ?>";

        var bathroomsValue = "<?php if(isset($this->data)) {echo $this->data['nBathrooms']; } else { echo NULL; } ?>";

        var propertyOnFloorValue = "<?php if(isset($this->data)) {echo $this->data['nPropertyOnFloor']; } else { echo NULL; } ?>";
            

        $('#Bedrooms').on('change', function(){

            var id = $('#Bedrooms').val();
            $('#Bathrooms').html('');
            $('#Balconies').html('');
            
            $('#Bathrooms').append('<option value="">-- Select --</option>');
            $('#Balconies').append('<option value="">-- Select --</option>');


            for (var i = 1; i <= id; i++) 
            { 
                if(bathroomsValue != 'NULL' && (bathroomsValue == i))
                {
                    $('#Bathrooms').append('<option value='+i+' selected>'+i+'</option>'); 
                }
                else
                {
                    $('#Bathrooms').append('<option value='+i+'>'+i+'</option>');
                }
                if(balconyValue != 'NULL' && (balconyValue == i))
                {
                    $('#Balconies').append('<option value='+i+' selected>'+i+'</option>');
                }
                else
                {
                    $('#Balconies').append('<option value='+i+'>'+i+'</option>');
                }
                                
            }        
        });

        $('#tFloor').on('change', function(){

            var id = $('#tFloor').val();
          
            for (var i = 1; i <= id; i++) 
            {
                if(propertyOnFloorValue != 'NULL' && (propertyOnFloorValue == i))
                {
                    $('#pFloor').append('<option value='+i+' selected>'+i+'</option>');
                }
                else
                {
                    $('#pFloor').append('<option value='+i+'>'+i+'</option>');
                }                
                
            }
        });

        $('#possession').datepicker({

            autoclose:true,

            startDate : 'today',

            format:'yyyy-mm-dd'

        });
        
        getUnitsDetails();
        getBHKDetails();      
        getPropertyType();
        getAmenities();
        getfacing();
        
        $('#typeprop').on('change', function(){
            
            $id = $('#typeprop').val();
            // alert($id);
            // console.log($id);
            if ($id == "rent" ) {
                $('#'+$id).css('display','block');
                $('#sale').css('display','none');

            }   
            else if ($id == "sale") {

                 $('#'+$id).css('display','block');
                $('#rent').css('display','none');
            }

        });
     
        $('#propList').on('change', function(){
            getPropertyType();
        });
        
        $('.time-picker').timepicker({ 
            showMeridian: false,
            minuteStep: 5 
        });

        $('.isAnswer').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });

        $('.Parkingyes').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#parking").css("display","block");
            }
            if (this.value === "no") {

                $("#parking").css("display","none");   
            }
        });

        $('.brokerageYes').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#Brokerage").css("display","block");
            }
            if (this.value === "no") {

                $("#Brokerage").css("display","none");   
            }
        });
        
        $('.isParking').change(function()
        {
            // console.log(this.value);
            if (this.value === "yes") {

                $("#isParking").css("display","block");
            }
            if (this.value === "no") {

                $("#isParking").css("display","none");   
            }
        });
        $('.Cparking').change(function()
        {
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });
        $('.Oparking').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });

    
        $("#update_basic").click(function(e) 
        {
            if($('#pCategoryID').val() == '')
            {
                toastr.error("Please select category.", "Category");
                e.preventDefault();
            }
            else if($('#pName').val() == '')
            {
                toastr.error("Please Enter Property name.", "Property Name");
                e.preventDefault();
            }
            mUtil.scrollTo("update_basic_form", -200);
        });


        $("#update_address").click(function(e) 
        {
            if($('#pCountryID').val() == '')
            {
                toastr.error("Please select country.", "Country");
                e.preventDefault();
            }
            else if($('#pStateID').val() == '')
            {
                toastr.error("Please select state.", "State");
                e.preventDefault();
            }
            else if($('#pCityID').val() == '')
            {
                toastr.error("Please select city.", "City");
                e.preventDefault();
            }
            else if($('#pAreaID').val() == '')
            {
                toastr.error("Please select area.", "Area");
                e.preventDefault();
            }
            mUtil.scrollTo("update_address_form", -200);
        });

        $("#update_times").click(function(e) 
        {
            var Daylent = $(".chkday:checkbox:checked").length;
            if(Daylent < 1)
            {
                toastr.error("Please select atleast one day.", "Select Day");
                e.preventDefault();
            }
            mUtil.scrollTo("update_time_form", -200);
        });

    
        $("#pLatitutde").focusout(function()
        {
            var latitude = document.getElementById('pLatitutde').value;
           
            var reg = new RegExp("^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$");

            if(latitude != '')
            {
                if(!reg.exec(latitude)) 
                {
                    document.getElementById('pLatitutde').value='';
                    toastr.error("Please Enter valid latitude value.", "latitude");
                }
            } 
        });

        $("#pLongitude").focusout(function()
        {
            var longitude = document.getElementById('pLongitude').value;

            var reg = new RegExp("^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$");

            if(longitude != '')
            {
                if(!reg.exec(longitude)) 
                {
                    document.getElementById('pLongitude').value='';
                    toastr.error("Please Enter valid longitude value.", "longitude");
                }
            }  
        });
    });
    
    /*--------------------START : FETCH RECORDS------------------*/
      
    $(window).on('load', function() {
        
        var subType = "<?php if(isset($this->data)) {echo $this->data['nPropertySubTypeIDFK']; } else { echo NULL; } ?>";
        var reraCertified = "<?php if(isset($this->data)) {echo $this->data['bIsPropertyReraCertified']; } else { echo NULL; } ?>";
        var IsBrokerageCharge = "<?php if(isset($this->data)) {echo $this->data['fPropertyBrokerageCharge']; } else { echo NULL; } ?>";

        if(subType != 'NULL')
        {
            showselect(subType);
        }

        if(reraCertified != "NULL" && reraCertified == "1")
        {
            $("#reraid").css("display","block");
        }

        if(IsBrokerageCharge != "NULL" && IsBrokerageCharge != "")
        {
            $("#Brokerage").css("display","block");
        }

         

        $("#Bedrooms").trigger("change");

        $('#tFloor').trigger("change");

        var insertedAmenities = Array();

        insertedAmenities = <?php if(isset($this->data)) {echo json_encode($this->data['ListingAmenities']); } else { echo NULL; } ?>;

        if(insertedAmenities != 'NULL')
        {
            for (var i = 0; i < insertedAmenities.length; i++)
            {               
                showDetails(insertedAmenities[i].nAmenityIDFK);
            }
        }
    

        
    });

    /*--------------------END : FETCH RECORDS------------------*/

    function getPropertyType() 
    {
        var htmlContent = '';
        var id = $('#propList').val();
        $.ajax({
            url: '<?php echo Urls::$BASE ?>properties/getSubTypeDetails',
            type: 'POST',
            data: {id: id},
            success: function (data) {        
                $('#propertySubTypes').html('');           
                var json = JSON.parse(data);
                for (var i = 0; i < json.length; i++)
                {
                    htmlContent = '<div class="from-group col-lg-2 " id="'+json[i].tPropertySubTypeName+'"><br><a style="cursor:pointer;" onclick="showselect('+json[i].nPropertySubTypeIDPK+');" ><img class="ImagesCircle" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING?>'+json[i].tPropertySubTypeImagePath+'" width="40px" height="40px"><i class="fa fa-check-circle tickClassOverImg"  id="subTypeImg_'+json[i].nPropertySubTypeIDPK+'"></i></a><br><br><label class="subTypeLabelAll" style="color:#818387" id="subTypeLabel_'+json[i].nPropertySubTypeIDPK+'">'+json[i].tPropertySubTypeName+'</label></div>'
                    $('#propertySubTypes').append(htmlContent);
                }
            }
        });
    }

    function showselect(id) 
    {        
        $('.subTypeLabelAll').css('color','#818387');
        $('#subTypeLabel_'+id).css('color','black');
        $('.ImagesCircle').css('border','none');
        $('.tickClassOverImg').css('display','none');
        $('#subTypeImg_'+id).css('display','block');
        $('#propertiesSubType').val(id);     
        $.ajax({
            url: '<?php echo Urls::$BASE ?>properties/getDerivedTypeDetails',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                // console.log(data);
                $('#typeofmain').html('');
                $('#selectTagProp').css("display","block");         
                var json = JSON.parse(data);            
                if (json != null) {
                    for (var i = 0; i < json.length; i++) {

                        var data = "<?php if(isset($this->data)) {echo $this->data['nPropertyDerivedTypeIDFK']; } else { echo NULL; } ?>";
                        if (json[i].nPropertyDerivedTypeIDPK != "" && json[i].nPropertyDerivedTypeIDPK != 0) {

                            $('#PropTypeName').html("Type Of "+json[i].tPropertySubTypeName);
                             
                            $('#typeofmain').select2();

                            $('#typeofmain').css("display","block");

                                if(data == json[i].nPropertyDerivedTypeIDPK)
                                {
                                    $('#typeofmain').append('<option value=' + json[i].nPropertyDerivedTypeIDPK + ' selected>' + json[i].tPropertyDerivedTypeName + '</option>');
                                }
                                else
                                {
                                    $('#typeofmain').append('<option value=' + json[i].nPropertyDerivedTypeIDPK + '>' + json[i].tPropertyDerivedTypeName + '</option>');
                                }
                        }
                        else{

                            $('#PropTypeName').html("No Type Found");

                            $('#selectTagProp').css("display","none");
                        }
                    }

                }
                else
                {
                    $('#PropTypeName').html("No "+name+" Types Found");                 
                    $('#typeofmain').css("display","none");

                }
            }
        });
    }


    function getfacing() 
    {
        $.ajax({

        url: '<?php echo Urls::$BASE ?>properties/getfacingDetails',
        type: 'POST',
        success: function (data) {
            // console.log(data);
           var facing = JSON.parse(data);

            //console.log(country.length);
            $('#facingID').html('');

            for(var i = 0; i < facing.length; i++)
            {
                var data = "<?php if(isset($this->data)) {echo $this->data['nFacingIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == facing[i].nFacingIDPK)
                    $('#facingID').append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');
                else
                    $('#facingID').append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
            }
            $("#facingID").trigger("change");
        }

        });
    }

    function getUnitsDetails() 
    {
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>properties/getUnitsDetails',
        type: 'POST',
        success: function(data) 
        {
            $('#UnitID').empty();
            $('#UnitID').append('<option value="">Select Unit</option>');

            var units = JSON.parse(data);

            //console.log(country.length);

            for(var i = 0; i < units.length; i++)
            {
                var data = "<?php if(isset($this->data)) {echo $this->data['nUnitIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == units[i].nUnitIDPK)
                    $('#UnitID').append('<option value='+units[i].nUnitIDPK+' selected>'+units[i].tUnitName+'</option>');
                else
                    $('#UnitID').append('<option value='+units[i].nUnitIDPK+'>'+units[i].tUnitName+'</option>');
            }
            $("#UnitID").trigger("change");
        }
        });
    }

    function getAmenities() 
    {   
        var htmlString = '';
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>properties/getAmenitiesDetails',
        type: 'POST',
        success: function(data) 
        {
            $('#Amenities').empty();
            var amenity = JSON.parse(data);
            for(var i = 0; i < amenity.length; i++)
            {
                htmlString = '<div class="col-lg-3"><br><a style="cursor: pointer;" class="buttonDemo" onclick="showDetails('+amenity[i].nAmenityIDPK+');"><img src="<?php echo Urls::$BASE.Urls::$ABOUTUS?>'+amenity[i].tAmenityImagePath+'" width="40px" height="40px" class="unChecked"  id="featureImg_'+amenity[i].nAmenityIDPK+'"><i class="fa fa-check-circle tickClassOverImg" id="iconImg_'+amenity[i].nAmenityIDPK+'" ></i></a><br><br><label style="color:#818387" class="subTypeLabelAll" id="features_'+amenity[i].nAmenityIDPK+'">'+amenity[i].tAmenityName+'</label></div>';  

                $('#Amenities').append(htmlString);
                
            }        
        }
        });
    }
  
    
    var selectedAmenityArray = Array();
  
    function showDetails(id)
    { 
        // console.log(id);  
        if($.inArray(id,selectedAmenityArray) != -1)
        {
            var index = selectedAmenityArray.indexOf(id);

            if(index!=-1){

               selectedAmenityArray.splice(index, 1);
            }
        }
        else
        { 
            selectedAmenityArray.push(id);
        }

        var checkForClass = $('#featureImg_'+id).hasClass('unChecked');

        // console.log(checkForClass);

        if(checkForClass) 
        {
            $('#featureImg_'+id).removeClass('unChecked');
            $('#iconImg_'+id).css('display','block');
            $('#features_'+id).css('color','black');
        } 
        else
        {
            $('#featureImg_'+id).addClass('unChecked');
            $('#iconImg_'+id).css('display','none');
            $('#features_'+id).css('color','#818387');
        }
        $('#AminitiesHidden').val(selectedAmenityArray);
        console.log(selectedAmenityArray); 
    }


    function getBHKDetails() 
    {
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>properties/getBHKDetails',
        type: 'POST',
        success: function(data) 
        {
            $('#propBHK').empty();
            $('#propBHK').append('<option value="">Select BHK</option>');

            var bhkResponse = JSON.parse(data);

            //console.log(country.length);

            for(var i = 0; i < bhkResponse.length; i++)
            {
                var data = "<?php if(isset($this->data)) {echo $this->data['nBHKIDFK']; } else { echo NULL; } ?>";
                //console.log(data);
                if(data == bhkResponse[i].nBHKIDPK)
                    $('#propBHK').append('<option value='+bhkResponse[i].nBHKIDPK+' selected>'+bhkResponse[i].tBHKName+'</option>');
                else
                    $('#propBHK').append('<option value='+bhkResponse[i].nBHKIDPK+'>'+bhkResponse[i].tBHKName+'</option>');
            }
            $("#propBHK").trigger("change");
        }
        });
    }

    function getLocations()
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getLocations',
            type: 'POST',
            success: function(data) 
            {
                $('#locationID').empty();
                $('#locationID').append('<option value="">Enter Area Name</option>');

                var locations = JSON.parse(data);

                //console.log(country.length);

                for(var i = 0; i < locations.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data['nPropertyAreaIDFK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == locations[i].nAreaIDPK)
                        $('#locationID').append('<option value='+locations[i].nAreaIDPK+' data-cityid='+locations[i].nCityIDFK+' data-stateid='+locations[i].nStateIDPK+' data-countryid='+locations[i].nCountryIDFK+' data-latitude='+locations[i].dAreaLatitude+' data-longitude='+locations[i].dAreaLongitude+' selected>'+locations[i].tAreaName+', '+locations[i].tCityName+'-'+locations[i].tStateName+'</option>');
                    else
                        $('#locationID').append('<option value='+locations[i].nAreaIDPK+' data-cityid='+locations[i].nCityIDFK+' data-stateid='+locations[i].nStateIDPK+' data-countryid='+locations[i].nCountryIDFK+' data-latitude='+locations[i].dAreaLatitude+' data-longitude='+locations[i].dAreaLongitude+'>'+locations[i].tAreaName+', '+locations[i].tCityName+'-'+locations[i].tStateName+'</option>');
                }
                $("#locationID").trigger("change");
            }
        });
    }

    $(document).on('change','#locationID',function () {
       
        var selectedCityID = $(this).find('option:selected').data('cityid');
        var selectedStateID = $(this).find('option:selected').data('stateid');
        var selectedCountryID = $(this).find('option:selected').data('countryid');
        var latitude = $(this).find('option:selected').data('latitude');
        var longitude = $(this).find('option:selected').data('longitude');
        $('#cityID').val(selectedCityID);
        $('#stateID').val(selectedStateID);
        $('#countryID').val(selectedCountryID);
        $('#pLatitutde').val(latitude);
        $('#pLongitude').val(longitude);
    });
   
    function removeOpenParking(id) 
    {            
       var value = $('#'+id).val();

       if (parseInt(value) != 1) {

           value = parseInt(value) - 1;    

       }
        $('#'+id).val(value);
    }
    
    function addOpenParking(id) 
    {
        var value = $('#'+id).val();

        if (parseInt(value) != 0 ) {
           value = parseInt(value) + 1;    
        }

        $('#'+id).val(value);
    }    
 
    function property_image_preview(vpb_selector_)
    {
        var numFiles = $('#property_logo')[0].files.length;
        if(numFiles <= 25)
        {
            $('#imgCount').text('Images Selected : ( '+numFiles+' / 25 )');
            var id = 1, last_id = last_cid = '';
            $.each(vpb_selector_.files, function(vpb_o_, file)
            {
                if (file.name.length>0) 
                {
                    if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                    else
                    {
                        //Clear previous previewed files and start again
                       $('#property-display-preview').html(''); 
                       
                       var reader = new FileReader();
                       
                       reader.onload = function(e) 
                       {
                           $('#property-display-preview').append(
                           ' <div class="col-lg-3 ">\
                           <img style="height:30%;width:30%;" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" title="'+ escape(file.name) +'" /><br /></div>  \
                           ');
                       }
                       reader.readAsDataURL(file);
                   }
                }
                else {  return false; }
            });
        }
        else
        {
            swal( {
                title: "", text: "Exceeds the image limits.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
            })
        }
    }

    function remove_listing_image(id)
    {
        // console.log(id);
        swal({
        title:"Are you sure?",
        text:"You want to delete this Image ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE; ?>properties/delete_listing_image",
                    data:{'id':id},
                    success: function(data)
                    {    
                        if(data == "true")
                        {
                            $('#hide_place_Img'+id).fadeOut();
                            swal({
                                type:"success",
                                title:"Image deleted successully",
                                showConfirmButton:!1,timer:1500
                            });
                        }               
                    }
                }); 
            }   
       }); 
    }

 
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
 
   
var WizardDemo=function() {
$("#m_wizard");
var e,
r,
i=$("#m_form");
 var postData = new FormData();
 $.each(i, function(i, val) {
            postData.append(val.name, val.value);
});
return {
    init:function() {
        var n;
        $("#m_wizard"),
        i=$("#m_form"),
        (r=new mWizard("m_wizard", {
            startStep: 5
        }
        )).on("beforeNext", function(r) {
            !0!==e.form()&&r.stop()
        }
        ),
        r.on("change", function(e) {
            mUtil.scrollTop()
        }
        ),
        r.on("change", function(e) {
            1===e.getStep()
            // console.log(r);
        }
        ),
        e=i.validate( {
            ignore:":hidden", rules: {
               
                // propList: {
                //     required: !0
                // }
                // , propertiesSubType: {
                //     required: !0,
                // }
                // , typeofmain: {
                //     required: !0
                // }
                // , pAddress: {
                //     required: !0
                // }
                // , pCountryID: {
                //     required: !0
                // }
                // , pStateID: {
                //     required: !0
                // }
                // , pCityID: {
                //     required: !0
                // }
                // , pAreaID: {
                //     required: !0
                // }
                // , SuperBuildUpArea: {
                //     required: !0, maxlength: 6,
                // }
                // , Area: {
                //     required: !0
                // }
                // , bArea: {
                //     required: !0
                // }
                // , carpetArea: {
                //     required: !0, maxlength: 6
                // }
                // , cArea: {
                //     required: !0
                // }
                // , Bedrooms: {
                //     required: !0
                // }
                // , Bathrooms: {
                //     required: !0
                // }
                // , Balconies: {
                //     required: !0
                // }
                // , kitchen: {
                //     required: !0
                // }
                // , tFloor: {
                //     required: !0
                // }
                // , pFloor: {
                //     required: !0
                // }
                // , Room: {
                //     required: !0
                // }
                // , propertyDescription: {
                //     required: !0
                // }
                // , Availability: {
                //     required: !0
                // }
                // , possession: {
                //     required: !0
                // }
                // , ownership: {
                //     required: !0
                // }
                // , Expected: {
                //     required: !0
                // }
                // , Booking: {
                //     required: !0
                // }
                // , PricePerUnit: {
                //     required: !0
                // }

            }
            , messages: {
                "account_communication[]": {
                    required: "You must select at least one communication option"
                }
                , accept: {
                    required: "You must accept the Terms and Conditions agreement!"
                }
            }
            , invalidHandler:function(e, r) {
                mUtil.scrollTop()
                // mUtil.scrollTop(), swal( {
                //     title: "", text: "There are some errors in your submission. Please correct them.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                // }
                // )
            }
            , submitHandler:function(e) {}
        }
        ),
        (n=i.find('[data-wizard-action="submit"]')).on("click", function(r) {

            r.preventDefault(), e.form()&&(mApp.progress(n), i.ajaxSubmit( {
                // url:'<?php echo Urls::$BASE; ?>properties/createproperty',type:'POST',data:i.serialize(),
                success:function(res) {

                    if(res.trim() == "true"){

                        swal( {
                            title: "", text: "Listing has been successfully added!", type: "success", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                        }
                        )

                        window.setTimeout(function(){
                                // Move to a new location or you can do something else
                                window.location.href = "<?php echo Urls::$BASE; ?>properties";

                            }, 2000);
                    }
                    else
                    {
                       swal( {
                            title: "", text: "Seems something wrong! Try again", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                        }
                        ) 
                    }
                }
            }
            ))
        }
        )
    }
}
}   

();
</script>