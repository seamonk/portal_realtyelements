<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    .buttonDemo{
        background-color: transparent;

    }
    .m-select2{ 
        border-color: #ffffff !important;
    }
    @media (min-width: 992px){
        .col-lg-9 {
          
            max-width: 70% !important;
        }

    }
    .btn-success{
        background-color: #f4516c !important;
        border-color: #f4516c !important;
    }
     #myMap {
           height: 350px;
           width: 100%;
        }
    /*.select2-search .select2-search--dropdown{
        display: none !important; 
    }*/

    .vpb_wrapper {
      max-width:114px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;  -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:5px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .tclose{
            position: absolute;
            background:black;
            color:white;
            border-radius:50%;
            width:20px;
            height:20px;
            line-height:20px;
            text-align:center;
            font-size:0.5rem !important;
            font-style: unset;
            font-family:'Arial Black', Arial, sans-serif;
            cursor:pointer;
            box-shadow:0 0 10px 0 #7b7e8a;
        }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ padding: 5px; width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:105px; height:105px;border:2px dashed #ebedf2 !important;border-radius: 10px;}

    label {
    font-weight: 400 !important;
    }
    .m-form .form-control-feedback {
   
    color: red;
}

.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-label {
    font-weight: bold;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--current .m-wizard__step-number > span {
    background-color: #ef424a;
}
/*data-toggle="m-tooltip" data-placement="bottom" data-offset="100px 0px" data-skin="dark" title="" data-original-title="Click To Select Image"*/
.tickClassOverImg
{
    position: absolute;
    top: 43%;
    left: 60%;
    right: 32%;
    border-radius: 28px;
    display: none;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--done .m-wizard__step-number > span {
    background-color: #ef424a;
}
.m-wizard.m-wizard--2.m-wizard--success .m-wizard__head .m-wizard__steps .m-wizard__step.m-wizard__step--done .m-wizard__step-label {
    font-weight: bold;
}
.listingForLabel
{
    font-size: 20px;
    font-weight: bold;
    margin: 15px;
}
.m-wizard.m-wizard--2 .m-wizard__head {
    
    margin: 5rem 0 3rem 0 !important;
   
}
</style>



    <div class="m-content">
        <!--Begin::Main Portlet-->
        <div class="m-portlet m-portlet--full-height">
            <!--begin: Portlet Head-->
            <?php

            if ($this->checkListingLimit > $this->getPropertiesCount) 
            {
                
            ?>
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add Property
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <!-- <a href="<?php echo Urls::$BASE; ?>properties" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Listings">
                                <i class="fa fa-list"></i>              
                            </a> -->
                        </li>
                    </ul>
                </div>
            </div>
            <!--end: Portlet Head-->
            <!--begin: Portlet Body-->
            <div class="m-portlet__body m-portlet__body--no-padding">            
                
                <div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">
                    <!--begin: Message container -->
                    <div class="m-portlet__padding-x">                  
                    </div>
                    <!--end: Message container -->
                    <div class="row m-row--no-padding">
                        <div class="col-lg-6 col-lg-12">

                            <!--begin: Form Wizard Head -->
                            <div class="m-wizard__head">

                                <!--begin: Form Wizard Progress -->
                                <div class="m-wizard__progress">
                                    <div class="progress">
                                        <div class="progress-bar" style="background-color: #ef424a;" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <div class="m-wizard__nav">
                                    <div class="m-wizard__steps">
                                        <div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number" >
                                                    <span style="background-color: #ef424a;">
                                                        <span>1</span>
                                                    </span>
                                                </a>                                           
                                                <div class="m-wizard__step-label">
                                                    Basic Information
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_2">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>2</span>
                                                    </span>
                                                </a>                                           
                                                <div class="m-wizard__step-label">
                                                    Location Setup
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>3</span>
                                                    </span>
                                                </a>                                       
                                                <div class="m-wizard__step-label">
                                                    Property Details
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>4</span>
                                                    </span>
                                                </a>                                           
                                                <div class="m-wizard__step-label">
                                                    Features
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="m-wizard__step" m-wizard-target="m_wizard_form_step_5">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>5</span>
                                                    </span>
                                                </a>                                           
                                                <div class="m-wizard__step-label">
                                                    Pricing
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <!--end: Form Wizard Nav -->
                            </div>
                            <!--end: Form Wizard Head -->
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <!--begin: Form Wizard Form-->
                            <div class="m-wizard__form">
                                <form class="m-form" enctype="multipart/form-data" id="m_form">
                                    <!--begin: Form Body -->
                                    <div class="m-portlet__body m-portlet__body">
                                        <!--begin: Form Wizard Step 1-->
                                        <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                            <div class=" m-form__section m-form__section--first">
                                                <div class="form-group row">
                                                    <div class="col-lg-3"></div>
                                                    <div class="col-lg-3" style="cursor: pointer;">
                                                        <label class="m-option" id="Resale" style="cursor: pointer;color: white; padding: 10px !important;">
                                                            <span class="m-option__control" style="width: 100%; text-align: center;">
                                                                <span class="m-radio m-radio--state-brand" style="display: none; ">
                                                                    <input  align="center" checked type="radio" name="listingFor" id="listingFor" style="width: 0px !important;"  onclick="changeBackground('Resale')" value="0">
                                                                    <span></span>
                                                                </span>
                                                                Resale
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3 Project" style="display: block;">
                                                        <label class="m-option" id="Project" style="cursor: pointer;color:black;padding: 10px !important;">
                                                            <span class="m-option__control" style="width: 100%; text-align: center;">
                                                                <span class="m-radio m-radio--state-brand" style="display: none">
                                                                    <input type="radio" name="listingFor" id="listingFor"   onclick="changeBackground('Project')" value="1">
                                                                    <span></span>
                                                                </span>
                                                                New Project
                                                            </span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-3"></div>                                                     
                                                </div>
                                               
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-2">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Property For</label>
                                                         <select  class="form-control m-select2" name="propFor" id="propFor">
                                                           <option value="0">Buy</option>
                                                           <option value="1">Rent</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Property Type</label>
                                                        <select  class="form-control m-select2" name="propertyType" id="propList">

                                                           <?php

                                                            if (!empty($this->getPropertyType)) {
                                                                ?>

                                                                <?php
                                                                for ($i=0; $i < count($this->getPropertyType) ; $i++) 
                                                                { 
                                                                  ?>
                                                                     <option value="<?php echo $this->getPropertyType[$i]['nPropertyTypeIDPK']?>"><?php echo $this->getPropertyType[$i]['tPropertyTypeName']?></option>
                                                                  <?php
                                                                }
                                                            }

                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                   <div class="col-lg-12" id="Residential" style="border: 2px solid #f7f7f7; padding: 20px;  text-align: center;">
                                                        <div class="col-lg-12 row" id="propertySubTypes">
                                                        </div>
                                                         <input type="hidden" name="propertiesSubType" id="propertiesSubType" value="">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-6 col-12">
                                                        <div class="row">   
                                                        <label>RERA Regestration Status:</label>
                                                            <div class="col-lg-3">
                                                                <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                                    YES
                                                                <input  name="rerayes" id="rerayes" value="yes" class="isAnswer" type="radio">
                                                                <span></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <label class="m-radio m-radio--single m-radio--solid m-radio--brand">
                                                                    NO
                                                                <input  name="rerayes" id="rerayes" checked value="no" class="isAnswer" type="radio">
                                                                <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="display: none;" id="reraid">
                                                    <div class="col-lg-12">
                                                        <label>Please Enter Your RERA Certification ID</label>
                                                        <input type="text" class="form-control m-input" required name="reraid" id="reraID" >
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <div class="col-lg-4">
                                                        <label>Status</label>
                                                        <select class="form-control m-select2" id="Availability" name="Availability">
                                                           
                                                            <option value="1">Under Constructions</option>
                                                            <option value="0">Ready To Move</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">

                                                        <label>Possession From</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control datepicker" placeholder="Select date" id="possessionDate" name="possessionDate" maxlength="10">
                                                            <div class="input-group-append" style="cursor: pointer;">
                                                                <span class="input-group-text">
                                                                    <i style="font-size: 20px;" class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <label style="font-size: 11.5px;">Date format :  dd-mm-yyyy</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 1-->
                                        
                                        <!--begin: Form Wizard Step 2-->
                                        <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                            <div class="m-form__section m-form__section--first">
                                                
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <?php
                                                        if (SessionHandling::get('userType') == DEVELOPER) 
                                                        {
                                                            ?>
                                                                <label>Project Name</label>
                                                                <input id="PropertyID" name="PropertyID" type="hidden">
                                                                <input type="text" class="form-control m-input" name="propertyName" id="propertyName"/>
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                                <label>Project Name</label>
                                                                <input id="PropertyID" name="PropertyID" type="hidden">
                                                                <input type="text" required class="form-control m-input" name="propertyName" id="propertyName"/>
                                                            <?php
                                                        }
                                                        ?>
                                                       
                                                    </div>
                                                   <!--  <div class="col-lg-6">
                                                        <label>Unit No.</label>
                                                        <input type="text" class="form-control" name="propertyNumber" id="propertyNumber"/>
                                                        
                                                    </div> -->
                                                    <div class="col-lg-6">
                                                        <label>Street Name</label>
                                                       <input type="text" class="form-control" name="streetName" id="streetName" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12 col-sm-12">
                                                        <label>Location</label>
                                                        <input type="text" class="form-control" name="pAddress" id="pAddress" placeholder="Enter Location">
                                                    </div>
                                                </div>

                                                <input type="hidden" name="LocalityID" id="LocalityID">
                                                <input type="hidden" name="cityID" id="cityID">
                                                <input type="hidden" name="areaID" id="areaID">
                                                <input type="hidden" name="stateID" id="stateID">
                                                <input type="hidden" name="countryID" id="countryID">
                                                
                                                <div class="form-group row">
                                                    <div class="col-lg-4">
                                                        <label>Select Country</label>
                                                        <select class="form-control m-select2" style="width: 100%;" name="country" id="select2Country" disabled ></select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Select State</label>
                                                        <select class="form-control m-select2" style="width: 100%;" name="state" id="select2State" disabled ></select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <label>Select City</label>
                                                        <select class="form-control m-select2" style="width: 100%;" name="city" id="select2City" ></select>
                                                    </div>
                                                </div>
                                                <div class="form-group  row">
                                                    <div class="col-lg-6 col-12">
                                                        <label>Select Area</label>
                                                        <div class="input-group" style="width: 100%;" >
                                                           <select class="form-control m-select2" style="width: 81%;" name="area" id="select2SArea" ></select>
                                                            <div class="input-group-append" onclick="AddLocationArea();" style="cursor: pointer;">
                                                                <span class="input-group-text"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Select locality</label>
                                                        <div class="input-group" style="width: 100%;" >
                                                          <select class="form-control m-select2" style="width: 85%;" name="localityName" id="localityName">
                                                            </select>
                                                            <div class="input-group-append" onclick="AddLocationLocality();" style="cursor: pointer;">
                                                                <span class="input-group-text"><i class="fa fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                       <label>Property Latitude</label>
                                                        <input type="text" class="form-control m-input" name="pLatitutde" id="pLatitutde" placeholder="Enter Property Latitude"  readonly>
                                                    </div>
                                                    <div class="col-lg-6">
                                                       <label>Property Longitude</label>
                                                        <input type="text" class="form-control m-input" name="pLongitude" id="pLongitude" placeholder="Enter Property Longitude" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                       <label>Map Location Link</label>
                                                        <input type="text" class="form-control m-input" name="mapLocationLink" id="mapLocationLink" placeholder="Enter Map Location Link" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12 col-sm-6">
                                                        <div style="width: 100%">
                                                           <div id="myMap"></div>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 2-->

                                        <!--begin: Form Wizard Step 3-->
                                        <div class="m-wizard__form-step " id="m_wizard_form_step_3">
                                            <div class="m-form__section m-form__section--first">
                                                <div class="form-group m-form__group">
                                                    <div class="row">
                                                        
                                                        <div class="col-lg-3">
                                                            <label class="m-option" id="Freehold" style="padding: 10px !important; cursor: pointer;">
                                                                <span class="m-option__control">
                                                                    <span class="m-radio m-radio--state-brand" style="display: none">
                                                                        <input checked type="radio" name="ownership" id="ownership"  onclick="changeBackgroundOwnership('Freehold')" value="Freehold">
                                                                        <span></span>
                                                                    </span>
                                                                </span>
                                                                <span class="m-option__label" >
                                                                    <span class="m-option__head">
                                                                        <span class="m-option__title">
                                                                            Freehold
                                                                        </span>
                                                                    </span>  
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label class="m-option" id="Leasehold" style="padding: 10px !important;cursor: pointer;">
                                                                <span class="m-option__control">
                                                                    <span class="m-radio m-radio--state-brand" style="display: none">
                                                                        <input type="radio" name="ownership" id="ownership"   onclick="changeBackgroundOwnership('Leasehold')" value="Leasehold">
                                                                        <span></span>
                                                                    </span>
                                                                </span>
                                                                <span class="m-option__label" >
                                                                    <span class="m-option__head">
                                                                        <span class="m-option__title">
                                                                            Leasehold
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <label class="m-option" id="Society" style="padding: 10px !important;cursor: pointer;">
                                                                <span class="m-option__control">
                                                                    <span class="m-radio m-radio--state-brand" style="display: none">
                                                                        <input type="radio" name="ownership" id="ownership"  onclick="changeBackgroundOwnership('Society')" value="Co-operative Society">
                                                                        <span></span>
                                                                    </span>
                                                                </span>
                                                                <span class="m-option__label" >
                                                                    <span class="m-option__head">
                                                                        <span class="m-option__title">
                                                                            Co-operative Society
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="m-form__help">
                                                    </div>
                                                </div>

                                               
                                                <div class="row" id="addLayout_Flat" style="display: none; cursor: pointer;padding: 20px;">
                                                    <div class="col-lg-12" >
                                                        <i style=" float: right;" class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                                <div class="row"  id="addLayout" onclick="addCommercial('Flat');" style="display: none; cursor: pointer;padding: 20px;">
                                                    <div class="col-lg-12" >
                                                        <i style=" float: right;" class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                                <div class="row"  id="addOtherslayout" onclick="addOtherslayout('Flat');" style="display: none; cursor: pointer; padding: 20px;">
                                                    <div class="col-lg-12" >
                                                        <i style=" float: right;" class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                                <div class="row" id="addLandlayout" onclick="addLandlayout('Flat');" style="display: none; cursor: pointer;padding: 20px;">
                                                    <div class="col-lg-12" >
                                                        <i style=" float: right;" class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="layoutCount" id="layoutCount" value="1">
                                                <div id="layoutDetails_Flat">
                                                    <div class="m-accordion m-accordion" id="m_accordion_4" role="tablist" style="margin-bottom: 20px;">
                                                        <div class="m-accordion__item">
                                                            <div class="m-accordion__item-head" role="tab" id="m_accordion_3_item_4_head" data-toggle="collapse" href="#m_accordion_3_item_4_body" style="background-color: #f2f2f2;" aria-expanded="false">
                                                                <span class="m-accordion__item-icon">
                                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>
                                                                </span>
                                                                <span class="m-accordion__item" >Add Layout Details</span>
                                                                
                                                            </div>
                                                            <div class="m-accordion__item-body collapse show" style="background-color: #fcfcfc;" id="m_accordion_3_item_4_body" class=" " role="tabpanel"aria-labelledby="m_accordion_3_item_4_head" data-parent="#m_accordion_4">
                                                                <div class="m-accordion__item-content">
                                                                    <div class="form-group row" id="S_Build_Area_Hidden_1" style="display: none;">
                                                                        <div class="col-lg-12">
                                                                            <div class="row">
                                                                                <div class="col-lg-4">
                                                                                    <label>Area Unit</label>
                                                                                    <br>
                                                                                    <select onchange="ChangeUnitePriceName('1');" style="width: 100%;" name="layout[UnitID][]" id="UnitID_1" class="form-control m-select2">

                                                                                        <option  >-- Select --</option>
                                                                                        <option>SQ.fit</option>
                                                                                        <option>SQ.Yards</option>
                                                                                        <option>SQ.Meter</option>
                                                                                        <option>Cents</option>
                                                                                        <option>Ares</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row" id="Build_Area_Hidden_1" style="display: none;">
                                                                        <div class="col-lg-12">
                                                                            <div class="row">
                                                                                <div class="col-lg-4">
                                                                                    <label id="Ploat_Area">Super Built Up Area</label>
                                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" maxlength="5" id="SuperBuildUpArea_1"  class="form-control m-input" name="layout[SuperBuildUpArea][]">
                                                                                </div>
                                                                                <div class="col-lg-4">
                                                                                    <label>Built Up Area</label>
                                                                                    <input type="text"  value="0"onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input"  maxlength="6" name="layout[BuildUpArea][]">
                                                                                </div>
                                                                                <div class="col-lg-4">
                                                                                    <label>Carpet Area</label>
                                                                                    <input type="text"  onkeypress="return isNumberKey(event);" required placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="carpetArea_1" name="layout[carpetArea][]">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row" id="villa_Area_details_1" style="display: none;">
                                                                        <div class="col-lg-12">
                                                                            <div class="col-lg-3">
                                                                                <label>Plot Area : </label>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-3">
                                                                                    
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label>Super Built Up Area</label>
                                                                                    <input type="text"value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6"class="form-control m-input" id="ploatSuperBuildUpArea_1" name="layout[ploatSuperBuildUpArea][]">
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label >Built Up Area</label>
                                                                                    <input type="text"value="0" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input"   maxlength="6" name="layout[ ploatBuildUpArea][]">
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label>Carpet Area</label>
                                                                                    <input type="text" onkeypress="return isNumberKey(event);" required placeholder="Carpet Area" id="ploatcarpetArea_1" maxlength="6" class="form-control m-input" name="layout[ploatcarpetArea][]">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                                <div class="col-lg-3">
                                                                                    <label>Construction Area : </label>
                                                                                </div>
                                                                            <div class="row">
                                                                                <div class="col-lg-3">
                                                                                    
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label>Super Built Up Area</label>
                                                                                    <input type="text"  value="0"onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  maxlength="6" class="form-control m-input " name="layout[constructionSuperBuildUpArea][]">
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label >Built Up Area</label>
                                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input"  maxlength="6" name="layout[constructionBuildUpArea][]">
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label>Carpet Area</label>
                                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" required placeholder="Carpet Area"  maxlength="6" class="form-control m-input"  name="layout[constructioncarpetArea][]">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <br>
                                                                        <div class="col-lg-4">
                                                                            <div class="form-group row">
                                                                                <div class="col-lg-12">
                                                                                   <label>Layout Images</label>
                                                                                    <span>
                                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px; " >

                                                                                           <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">
                                                                                                <a onclick="RemoveImage('layout_image1',1);" id="RemoveImage_1" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">
                                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>
                                                                                                </a>
                                                                                                <a id="EditImage_1" style="display: none;cursor: pointer;" onclick="document.getElementById('layout_image1').click();">
                                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>
                                                                                                </a>
                                                                                           </div>
                                                                                            <input style="display:none;" type="file" name="layout[layout_image][]" id="layout_image1" onchange="layout_preview(this,1)" accept="image/x-png,image/jpeg,image/jpg" />


                                                                                            <div align="center" style="cursor: pointer;" id="layout_preview_display_1" onclick="document.getElementById('layout_image1').click();">
                                                                                              
                                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;cursor: pointer;"></i>
                                                                                            </div>
                                                                                        </div>
                                                                                    </span>
                                                                                </div>
                                                                                <br>
                                                                                <div class="col-lg-10" id="Carparking" style="display: none;">
                                                                                    <br>
                                                                                    <label>Available Parking</label>
                                                                                    <div class="input-group">
                                                                                        <div class="input-group-append" style="cursor: pointer;" onclick="removeOpenParking(0)">
                                                                                            <span class="input-group-text">
                                                                                                 <i class="fa fa-minus"></i>
                                                                                            </span>
                                                                                        </div>
                                                                                       <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" readonly id="Cparking0" name="layout[Cparking][]" max="10"  value="0" readonly />
                                                                                        <div class="input-group-append" onclick="addOpenParking('0')" style="cursor: pointer;">
                                                                                            <span class="input-group-text">
                                                                                                 <i class="fa fa-plus"></i>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-8 col-12" id="Flat_layout" style="display: none;">
                                                                            <div class="from-group row">
                                                                                <div class="col-lg-4">
                                                                                    <label>Select BHK</label>
                                                                                    <select style="width: 100%;" name="layout[layoutPropBHK][]" class="form-control m-select2" id="layoutPropBHK">
                                                                                       
                                                                                        <option value="1">1 </option>
                                                                                        <option value="2">2 </option>
                                                                                        <option value="3">3 </option>
                                                                                        <option value="3+">3+ </option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-4">
                                                                                    <label>Select Facing</label>
                                                                                    <select style="width: 100%;" name="layout[facingID][]" class="form-control m-select2" id="facingID">
                                                                                        
                                                                                         <option value="1">1 </option>
                                                                                        <option value="2">2 </option>
                                                                                        <option value="3">3 </option>
                                                                                        <option value="4">4 </option>
                                                                                    </select>
                                                                                </div>                                                                     
                                                                                <div class="col-lg-4">
                                                                                    <div class="form-group row">
                                                                                        <div class="col-lg-12">
                                                                                            <label>Property Furnished</label>
                                                                                            <select id="propertyFurnishedID" name="layout[propertyFurnishedID][]" class="form-control m-select2" style="width: 100%;">
                                                                                                 
                                                                                               <option value="1">Furnished</option>
                                                                                               <option value="0">Un-Furnished </option> 
                                                                                               <option value="2">Semi-Furnished </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                                <div class="col-lg-4">
                                                                                    <label>Bedrooms</label>
                                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout[Bedrooms][]" id="Bedrooms">
                                                                                        
                                                                                        <option value="0">0</option>
                                                                                        <option value="1">1</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="5">5</option>
                                                                                        <option value="6">6</option>
                                                                                        <option value="7">7</option>
                                                                                        <option value="8">8</option>
                                                                                        <option value="9">9</option>
                                                                                        <option value="9+">9+</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-4">
                                                                                    <label>Bathrooms</label>
                                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout[Bathrooms][]" id="Bathrooms">
                                                                                       
                                                                                        <option value="0">0</option>
                                                                                        <option value="1">1</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="5">5</option>
                                                                                        <option value="6">6</option>
                                                                                        <option value="7">7</option>
                                                                                        <option value="8">8</option>
                                                                                        <option value="9">9</option>
                                                                                        <option value="9+">9+</option>
                                                                                       
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-4">
                                                                                    <label>Balconies</label>
                                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout[Balconies][]" id="Balconies">
                                                                                        
                                                                                        <option value="0">0</option>
                                                                                        <option value="1">1</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="5">5</option>
                                                                                        <option value="6">6</option>
                                                                                        <option value="7">7</option>
                                                                                        <option value="8">8</option>
                                                                                        <option value="9">9</option>
                                                                                        <option value="9+">9+</option>
                                                                                        
                                                                                    </select>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div class="col-lg-6">
                                                                                    <br>
                                                                                   <label>Total Floors</label>
                                                                                   <input type="text" value="0" class="form-control m-input" name="layout[tFloor][]" id="tFloor" onkeypress="return isNumberKey(event);" placeholder="Total Floors" style="width: 100%;" max="100" id="tFloor" maxlength="3">

                                                                                </div>
                                                                                <div class="col-lg-6" id="Layout_Hidden">
                                                                                    <br>
                                                                                    <label>Property On Floor</label>
                                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout[pFloor][]" id="pFloor" style="display: block;">
                                                                                         
                                                                                    </select>
                                                                                </div>
                                                                                
                                                                            </div>
                                                                           
                                                                            <div class="from-group row" style="padding: 5px !important;">
                                                                                <div class="col-lg-6">   
                                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">
                                                                                    <p style="width: 100%;">Store Room</p>
                                                                                    <input type="hidden" name="layout[storeRoomAvailable][0]" id="storeRoomAvailable" value="">
                                                                                    <input  name="layout[storeRoomAvailable][]" id="Room"  class="StoreRoom" onchange="AllcheckBoxArray('StoreRoom','storeRoomAvailable')" type="checkbox">
                                                                                    <span></span>
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    
                                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">
                                                                                    <p style="width: 100%;">Pooja Room</p>
                                                                                    <input type="hidden" name="layout[poojaRoomAvailable][0]" id="poojaRoomAvailable" value="">
                                                                                    <input  name="layout[poojaRoomAvailable][]" id="Room"  class="PoojaRoom" onchange="AllcheckBoxArray('PoojaRoom','poojaRoomAvailable')" type="checkbox">
                                                                                    <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-8 col-12" id="Others_layout" style="display: none;">
                                                                            <div class="form-group row">
                                                                                
                                                                                <div class="col-lg-3">
                                                                                    <label>Select BHK</label>
                                                                                    <select style="width: 100%;" name="layout[propBHK][]" class="form-control m-select2" id="propBHK_Other">
                                                                                        
                                                                                        <option value="1">1 </option>
                                                                                        <option value="2">2 </option>
                                                                                        <option value="3">3 </option>
                                                                                        <option value="3+">3+ </option>
                                                                                    </select>
                                                                                </div>
                                                                                 <div class="col-lg-3">
                                                                                    <label>Select Facing</label>
                                                                                    <select style="width: 100%;" name="facingID" class="form-control m-select2" id="facingID_Other">
                                                                                       
                                                                                         <option value="1">1 </option>
                                                                                        <option value="2">2 </option>
                                                                                        <option value="3">3 </option>
                                                                                        <option value="4">4 </option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                   <label>Total Floors</label>
                                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Total Floors" class="form-control m-input" style="width: 100%;"  name="layout[tFloor][]" id="tFloor_Other"  max="100">
                                                                                   
                                                                                </div>
                                                                                <div class="col-lg-3">
                                                                                    <label>Property On Floor</label>
                                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout[pFloor][]" id="pFloor_Other">
                                                                                          
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row" style="padding: 5px !important;">
                                                                                <div class="col-lg-6">         
                                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">
                                                                                    <p style="width: 100%;">Store Room</p>
                                                                                    <input type="hidden" name="layout[storeRoomAvailable][0]" id="storeRoomAvailable_other" value="">
                                                                                    <input  name="layout[storeRoomAvailable][]" id="Room_Other" onclick="AllcheckBoxArray('StoreRoom_other','storeRoomAvailable_other')"class="StoreRoom_other"  type="checkbox">
                                                                                    <span></span>
                                                                                    </label>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">
                                                                                    <p style="width: 100%;">Pooja Room</p>
                                                                                    <input type="hidden" name="layout[poojaRoomAvailable][0]" id="poojaRoomAvailable_other" value="">
                                                                                    <input  name="layout[poojaRoomAvailable][]" id="Room_Other" onclick="AllcheckBoxArray('PoojaRoom_other','poojaRoomAvailable_other')" class="PoojaRoom_other"  type="checkbox">
                                                                                    <span></span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-8 col-12" id="Office" style="display: none;">
                                                                            <div class="form-group row">
                                                                                <div class="col-lg-12">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-4">
                                                                                            <label>Washroom</label>
                                                                                            <select style="width: 100%;" name="layout[washroom][]" class="form-control m-select2" id="washroom_Office">
                                                                                               
                                                                                                <option value="0">0 </option>
                                                                                                <option value="1">1 </option>
                                                                                                <option value="2">2 </option>
                                                                                                <option value="3">3 </option>
                                                                                                <option value="3+">3+ </option>
                                                                                            </select>
                                                                                        </div>
                                                                                         <div class="col-lg-4">
                                                                                            <label>Balcony</label>
                                                                                            <select style="width: 100%;" name="layout[balcony][]" class="form-control m-select2" id="balcony_Office">
                                                                                                
                                                                                                <option value="0">0 </option>
                                                                                                <option value="1">1 </option>
                                                                                                <option value="2">2 </option>
                                                                                                <option value="3">3 </option>
                                                                                                <option value="4">4 </option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
                                                                                           <label>Property Furnished</label>
                                                                                            <br>
                                                                                            <select id="furnished_office" name="layout[Furnished_office][]" class="form-control m-select2" style="width: 100%;">
                                                                                                 
                                                                                               <option value="1">Furnished</option>
                                                                                               <option value="0">Un-Furnished </option> 
                                                                                               <option value="2">Semi-Furnished </option>
                                                                                            </select>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
                                                                                            <label>Pentry</label>
                                                                                            <select style="width: 100%;" name="layout[pentry][]" class="form-control m-select2" id="pentry_Office">
                                                                                               
                                                                                                <option value="0">0 </option>
                                                                                                <option value="1">1 </option>
                                                                                                <option value="2">2 </option>
                                                                                                <option value="3">3 </option>
                                                                                                <option value="4">4 </option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                
                                                                            </div> 
                                                                        </div>
                                                                        
                                                                    </div>

                                                                    <div class="form-group row">
                                                                            <div class="col-lg-4">
                                                                                <label>Expected Price</label>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-append">
                                                                                        <span class="input-group-text">
                                                                                            <i style="font-size: 15px;" class="la la-rupee"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                    <input value="0" type="text" maxlength="10" class="form-control m-input" placeholder="Expected Price"  name="layout[Expected][]" required id="Expected_1" oninput="GetProcePerUnit('1');" onkeypress="return isNumberKey(event);">
                                                                                </div>
                                                                                <span id="numToWords1"></span>
                                                                               
                                                                            </div>
                                                                            <div class="col-lg-4">
                                                                               
                                                                                <label id="perUnitPrice_1">Price Per Unit</label>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-append">
                                                                                        <span class="input-group-text">
                                                                                            <i style="font-size: 15px;" class="la la-rupee"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                   <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" readonly name="layout[PricePerUnit][]" id="PricePerUnit_1">
                                                                                </div>
                                                                                <span id="pnumToWords1"></span>
                                                                            </div>
                                                                            
                                                                            <div class="col-lg-4">
                                                                                <label>Booking Price</label>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-append">
                                                                                        <span class="input-group-text">
                                                                                            <i style="font-size: 15px;" class="la la-rupee"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                    <input type="text" value="0" class="form-control m-input" name="layout[BookingPrize][]" maxlength="10" id="Booking1" placeholder="Booking Price" oninput=" convertNumbertoWords('Booking1','bnumToWords1');" onkeypress="return isNumberKey(event);">
                                                                                </div>
                                                                                <span id="bnumToWords1"></span>
                                                                            </div>
                                                                            <div class="col-lg-4" style="padding-top: 5%;">
                                                                                <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                                                                    Negotiable ? 
                                                                                    <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_1" value="">
                                                                                    <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable_1" class="ExpectedNegociable" onchange="AllcheckBoxArray('ExpectedNegociable','PriceNegociable_1')" type="checkbox">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-8" style="padding-top: 5%;">
                                                                                <label class="m-checkbox m-checkbox--solid m-checkbox--brand">
                                                                                Don`t Display Price 
                                                                                <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay" value="">
                                                                                <input  name="layout[priceDisplayed][]" id="priceDisplayed_1" onchange="AllcheckBoxArray('priceDisplayed','priceDisplay')" class="priceDisplayed"  type="checkbox">
                                                                                <span></span>
                                                                                </label>
                                                                            </div>
                                                                    </div>`
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label>Description</label>
                                                        <textarea  class="form-control m-input" name="propertyDescription" id="propertyDescription" rows="5"></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 3-->
                                        
                                        <!--begin: Form Wizard Step 4-->
                                        <div class="m-wizard__form-step " id="m_wizard_form_step_4">
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-form__heading">
                                                    <h2 class="m-form__heading-title">Features Details</h2>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-3">
                                                       <label>Property Images</label>
                                                        <span class="" style="cursor: pointer;" onclick="document.getElementById('property_logo').click();">
                                                            <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                                                <input style="display:none;" type="file" name="property_logo[]" id="property_logo" onchange="property_image_preview(this)" accept="image/x-png,image/jpeg,image/jpg" required />

                                                                <div align="center">
                                                                    <i class="fa fa-plus" style="font-size: 9.1rem !important;"></i>
                                                                    <label  style="cursor: pointer;">Click here to upload images</label>
                                                                </div>
                                                                <input type="hidden" name="images[]" id="images">
                                                                <input type="hidden" name="coverImage[]" required id="coverImage">
                                                            </div>
                                                        </span>
                                                        <br><br>
                                                        <label id="imgCount">Images Selected : ( 0 / 25 )</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-lg-9" style="margin-top: 20px; height: 315px !important; overflow-x: auto !important; ">
                                                        <br>
                                                         <div class="form-group row" id="property_display_preview" style="display: none;padding: 5px !important; padding-left: 10px !important;" >
                                                         </div>
                                                    </div>
                                                    <br>
                                                    <br>
                                                </div>
                                               
                                                <div class="from-group">
                                                    <label>select Amenities : </label>
                                                    <div class="col-lg-12" id="Aminities" style="text-align: center; vertical-align: middle;">
                                                        <div class="col-lg-12">
                                                            <input type="hidden" name="AminitiesHidden[]" id="AminitiesHidden" value="">
                                                            <div class="row" id="Amenities" style="height: 250px !important;overflow:auto;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><br>                                            
                                                <div class="form-group m-form__group row">
                                                    <div class="col-lg-12">
                                                    <label>Property Brochure</label><br>
                                                    <input  type="file" name="brochurePath[]" id="brochurePath" accept="application/pdf" />                            
                                                    </div>
                                                    <br>
                                                    <input type="hidden" name="createOnDate" value="<?php echo date('Y-m-d');?>">
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 4-->
                                        
                                    </div>
                                    <!--end: Form Body -->
                                    <!--begin: Form Actions -->
                                    <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-10">
                                        <div class="m-form__actions">
                                            <div class="row">
                                                <div class="col-lg-6 m--align-left">
                                                    <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                        <span>
                                                            <i class="la la-arrow-left"></i>&nbsp;&nbsp;
                                                            <span>Back</span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-6 m--align-right">
                                                    <input type="hidden" name="submit" value="submit">
                                                    <button class="btn btn-danger btn-block AddProperty" data-wizard-action="submit" name="submit" value="submit" type="button">Add Property</button>
                                                    <a style="background-color: #3a3a3a;" href="#" class="btn btn-metal m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                        <span>
                                                            <span>Save & Continue</span>&nbsp;&nbsp;
                                                            <i class="la la-arrow-right"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Actions -->
                                </form>
                            </div>
                            <!--end: Form Wizard Form-->
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
               
                <!--end: Form Wizard-->
            </div>
            <!--end: Portlet Body-->
            <?php
            }
            else{

                ?>
                
                <div class="m-portlet__body">
                    <br>
                    <br><br><br><br>
                    <br>
                    <br><br><br><br>
                    
                    <div class="col-lg-12">
                        <h4 style="text-align: center;color: #ef424a ;">Oh Snap! You cannot post a property as you have no listing left!</h4>
                    </div>
                    <br>
                    <br><br><br><br>
                    <br>
                    <br><br><br><br>
                </div>
                <?php
            }

            ?>
        </div>
        <!--End::Main Portlet-->
    </div>
<div id="addAreaModal" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="OldCityName" class="modal-title">Add Area on city</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-8">
                        <label>Enter Area name</label>
                        <input type="text" class="form-control m-input" name="newArea" id="newArea">
                    </div>
                    <div class="col-lg-4">
                        <label>Enter Pincode</label>
                        <input type="text" class="form-control m-input" name="newAreaPincode" id="newAreaPincode">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-block" id="AddNewAreaBtn">Add Area</button>
            </div>
        </div>
    </div>
</div>

<div id="addLocalityModal" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="OldAreaName" class="modal-title">Add Locality on Area</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-8">
                        <label>Enter Locality name</label>
                        <input type="text" class="form-control m-input" name="newLocality" id="newLocality">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success btn-block" id="AddNewLocalityBtn">Add Locality</button>
            </div>
        </div>
    </div>
</div>
 <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZMqWuiutvNXW0cZ-m1IAgsW9FhbP_MSU&sensor=false"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZMqWuiutvNXW0cZ-m1IAgsW9FhbP_MSU&libraries=places&sensor=false"></script>

<script type="text/javascript"> 
    
    var map;
    var marker;
    // var myLatlng = new google.maps.LatLng(22.2466679,75.6296507);
    var geocoder;
    var infowindow = new google.maps.InfoWindow();
    var searchBox;

    function initMap() 
    {
        map = new google.maps.Map(document.getElementById('myMap'), {
          center:{lat:24.067945, lng:78.871125},
          zoom: 5.2,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pAddress');

        searchBox = new google.maps.places.SearchBox(input);
        geocoder = new google.maps.Geocoder();
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
       

      
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

         

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();

          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }

            // console.log(place);
            var input1 = place.geometry.location.lat()
            var input2 = place.geometry.location.lng();
            geocodeAddress(geocoder, map);
            // console.log(input2);
               
          });
         
        });
        
    } 
     
    // $('#pAddress').on('keyup', function(event) {
    //         // console.log('press enter');
    //     if (event.keyCode == 13) {
            
    //         var address = $('#pAddress').val();

    //         if (address == "") 
    //         {
    //             toastr.error("Please Enter Address to set location","Location");
    //             $('#pAddress').focus();
    //         }
    //         else
    //         {
    //             geocodeAddress(geocoder, map);

    //         }
    //     }
    // });

    // $(document).on('focusout','#pAddress',function()
    // {
        
    //     geocodeAddress(geocoder, map);
    // });
    // $('#pAddress').change(function()
    // {
    //   var address = $('#pAddress').val();
    //     if (address == "") 
    //     {
    //         toastr.error("Please Enter Address to set location","Location");
    //         $('#pAddress').focus();
    //     }
    //     else
    //     {
    //         geocodeAddress(geocoder, map);
    //     } 
       
    // });
    // $(document).on('select','#pAddress',function()
    // {
    //     var address = $('#pAddress').val();
    //     if (address == "") 
    //     {
    //         toastr.error("Please Enter Address to set location","Location");
    //         $('#pAddress').focus();
    //     }
    //     else
    //     {
    //         geocodeAddress(geocoder, map);
    //     }   
    // });
   
    // function initMap() {
    //     var map = new google.maps.Map(document.getElementById('myMap'), {
    //       zoom: 5,
    //       center: {lat:22.2466679, lng: 75.6296507}
    //     });

    //     var geocoder = new google.maps.Geocoder();

    //     $('#submit').on('click',function()
    //     {
    //         var address = $('#address').val();

    //         if (address == "") 
    //         {
    //             alert("Please Enter Address to set location");
    //         }
    //         else
    //         {
    //             geocodeAddress(geocoder, map);

    //         }
    //     });
    //   }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('pAddress').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            var input1 = results[0].geometry.location.lat();
            var input2 = results[0].geometry.location.lng();
            // console.log(results[0].geometry.location);
            initialize(input1,input2);
          } else {
            // alert('Geocode was not successful for the following reason: ' + status);
          }
        });
    }
    function initialize(input1,input2)
    {
        var mapOptions = {
            zoom: 18,
            center: new google.maps.LatLng(parseFloat(input1),parseFloat(input2)),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
       
        map = new google.maps.Map(document.getElementById("myMap"), mapOptions);
        
        

        marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(parseFloat(input1),parseFloat(input2)),
            draggable: true 
        });     
        
        geocoder.geocode({'latLng': marker.getPosition() }, function(results, status) {
            // if (status == google.maps.GeocoderStatus.OK) {
            //     if (results[0]) {
            //         // console.log(results[0]);
            //         $('#pAddress').val(results[0].formatted_address);
            //         $('#pLatitutde').val(marker.getPosition().lat());
            //         $('#pLongitude').val(marker.getPosition().lng());
            //         infowindow.setContent(results[0].formatted_address);
            //         infowindow.open(map, marker);
            //     }
            // }
               if (status == google.maps.GeocoderStatus.OK && results.length) {

                var place_id = results[0].place_id;
               

                var location = results[0].formatted_address;
                

                results = results[0].address_components;
                // console.log(results);

                var mapLink = 'https://www.google.com/maps/search/?api=1&query='+marker.getPosition().lat()+','+marker.getPosition().lng()+'&query_place_id='+place_id;

                $('#mapLocationLink').val(mapLink);
                // console.log(mapLink);

                var city = findResult(results, "locality");
                
                if(city == "")
                {
                   city = findResult(results, "administrative_area_level_2"); 

                   if(city == "")
                   {
                          swal({
                            title:"City Not Found",
                            text:"Please select different location",
                            type:"warning",
                            showCancelButton:!0,                           
                            cancelButtonText:"OK"
                        });     
                    }
                    else
                    {
                        // $('#pAddress').val(location);
                        $('#pLatitutde').val(marker.getPosition().lat());
                        $('#pLongitude').val(marker.getPosition().lng());
                        // infowindow.setContent(results[0].formatted_address);
                        // infowindow.open(map, marker);
                    }
                }
                else
                {
                   
                    $('#pLatitutde').val(marker.getPosition().lat());
                    $('#pLongitude').val(marker.getPosition().lng());
                    // infowindow.setContent(results[0].formatted_address);
                    // infowindow.open(map, marker);
                }

                $('#pAddress').val(location);
                infowindow.setContent(location);
                infowindow.open(map, marker);
                var state = findResult(results, "administrative_area_level_1");
                var country = findResult(results, "country");
                var area = findResult(results, "sublocality_level_1");                  
                var route = findResult(results, "route");
                var pincode = findResult(results, "postal_code");

                // console.log('City : '+city);
                // console.log('Area : '+area);
                // console.log('Locality : '+route);

                var latitude = marker.getPosition().lat();
                var longitude = marker.getPosition().lng()

                checkForLocation(country,state,city,area,route,pincode,latitude,longitude);
              

                } 
        });

        var findResult = function(results, name){

                var result =  _.find(results, function(obj){
                for (var i = 0; i < obj.types.length; i++)
                {
                    if(obj.types[i] == name)
                    {
                       return obj.types[i] == name;
                    } 
                }
            });

            return result ? result.long_name : null;
        };
                       
        google.maps.event.addListener(marker, 'dragend', function() 
        {

            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                // if (status == google.maps.GeocoderStatus.OK) {
                //     if (results[0]) {
                //         console.log(results[0]);
                //         $('#pAddress').val(results[0].formatted_address);
                //         $('#pLatitutde').val(marker.getPosition().lat());
                //         $('#pLongitude').val(marker.getPosition().lng());
                //         infowindow.setContent(results[0].formatted_address);
                //         infowindow.open(map, marker);
                //     }
                // }
                if (status == google.maps.GeocoderStatus.OK && results.length) {
                 var place_id = results[0].place_id;
               

                var location = results[0].formatted_address;
               
               
                results = results[0].address_components;
                 // console.log(results);

                var mapLink = 'https://www.google.com/maps/search/?api=1&query='+marker.getPosition().lat()+','+marker.getPosition().lng()+'&query_place_id='+place_id;

                $('#mapLocationLink').val(mapLink);
                // console.log(mapLink);

                                         
                var city = findResult(results, "locality");
                if(city == "")
                {
                   city = findResult(results, "administrative_area_level_2"); 

                   if(city == "")
                   {
                         swal({
                            title:"City Not Found",
                            text:"Please select different location",
                            type:"warning",
                            showCancelButton:!0,                           
                            cancelButtonText:"OK"
                        });
                           
                   }
                   else
                    {
                        // $('#pAddress').val(location);
                        $('#pLatitutde').val(marker.getPosition().lat());
                        $('#pLongitude').val(marker.getPosition().lng());
                        // infowindow.setContent(location);
                        // infowindow.open(map, marker);
                    }
                }
                else
                {
                    // $('#pAddress').val(location);
                    $('#pLatitutde').val(marker.getPosition().lat());
                    $('#pLongitude').val(marker.getPosition().lng());
                    // infowindow.setContent(location);
                    // infowindow.open(map, marker);
                }

                $('#pAddress').val(location);
                infowindow.setContent(location);
                infowindow.open(map, marker);
                var state = findResult(results, "administrative_area_level_1");
                var country = findResult(results, "country");
                var area = findResult(results, "sublocality_level_1");                  
                var route = findResult(results, "route");
                var pincode = findResult(results, "postal_code");

                // console.log('City : '+city);
                // console.log('Area : '+area);
                // console.log('Locality : '+route);

                var latitude = marker.getPosition().lat();
                var longitude = marker.getPosition().lng()

                checkForLocation(country,state,city,area,route,pincode,latitude,longitude);
               
                } 
            });
        });
    }
    
    google.maps.event.addDomListener(window, 'load', initMap);


    function checkForLocation(country, state, city = null, area = null, route = null,pincode = null, latitude = null, longitude = null ) {

       $.ajax({
            url: "<?php echo Urls::$BASE; ?>location/checkForLocation",
            type: "POST",
            data: {'country':country,'state':state,'city':city,'area':area,'route':route,'pincode':pincode,'latitude':latitude,'longitude':longitude},
            success: function (data) 
            {
                var json = JSON.parse(data);
                
                $('#areaID').val(json.areaId);
                $('#cityID').val(json.cityId);
                $('#stateID').val(json.stateId);
                $('#countryID').val(json.countryId);
                $('#LocalityID').val(json.localityId);

                getCountryList();
                // getStateList(json.stateId);
                // getCityList(json.cityId);
                // getAreaList(json.areaId);
                // getlocality(json.localityId);               
            }
        });
    }
</script>  

<script type="text/javascript">
    $(document).ready(function() 
    {
        $("#select2Country").select2({
            placeholder: "-- Select Country --"
        });

        $("#select2State").select2({
            placeholder: "-- Select State --"
        });

        $("#select2City").select2({
            placeholder: "-- Select City --"
        });
        $("#select2SArea").select2({
            placeholder: "-- Select Area --"
        });
        $("#localityName").select2({
            placeholder: "-- Select Locality --"
        });
        getCountryList();

        var loginID = "<?php echo SessionHandling::get('userType');?>";

        if (loginID == "<?php echo DEVELOPER ?>") 
        {
            changeBackground('Resale');
            $('#Resale').css('background-color','#9a9a9a');
            $('#Resale .m-option__title').css('color','#ffff !important');
        }
        else if (loginID == "<?php echo AGENT ?>") 
        {
            $('.Project').css('display','none');
            changeBackground('Resale');
        }
        SetWashroomValue();
        WizardDemo.init();
        

        // $(".m-select2").select2({
        //     allowClear: true
        
        // });

        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };
        
        $('#propFor').select2();

        /*----------START : LOCAIONS------------*/

            // $('#locationID').select2();
            // getLocations();

        /*----------END : LOCAIONS------------*/

        /*House layout*/

        $('#layoutPropBHK_House').select2();
        $('#propertyFurnishedID_House').select2();
        $('#Bedrooms_House').select2();
        $('#Bathrooms_House').select2();
        $('#Balconies_House').select2();
        $('#propBHK_House').select2();
        $('#facingID_House').select2();
        $('#furnished_office').select2();
        $('#pentry_Office').select2();
        

       
        $('#bArea_House').select2();
        $('#cArea_House').select2();
        // $('#tFloor_House').select2();

        /*End House Layout*/
        /* Ofifce Layout */

        $('#washroom_Office').select2();

      

        $('#balcony_Office').select2();
       
        $('#bArea_Office').select2();
        $('#cArea_Office').select2();
        

            
        /*End Ofifce Layout */
        /* Other Layout  */
        $('#propBHK_Other').select2();
        $('#facingID_Other').select2();
        
        // $('#tFloor_Other').select2();
        $('#pFloor_Other').select2();
        $('#bArea_Other').select2();
        $('#cArea_Other').select2();
        
        /*End Other Layout  */

        $('#propertyFurnishedID').select2();
        // $('#companyID').select2();
        $('#maintenanceFor').select2();
        $('#brokerageAmountIn').select2();
        $('#pCategoryID').select2();
        $('#Availability').select2();
        // $('#ownership').select2();
        $('#maintenance').select2();
        // $('#possession').datepicker();
        $('#propBHK').select2();

        $('#layoutPropBHK').select2();
       

        $('.datepicker').datepicker({
            autoclose:true,
            startDate : 'today',
            format:'dd-mm-yyyy'
        });

        $('#kitchen').select2();
        $('#facingID').select2();
        $('#layoutType').select2();
       

        $('#UnitID').select2();
        $('#bArea').select2();
        $('#cArea').select2();
        $('#UnitID_1').select2();
        $('#bArea_1').select2();
        $('#cArea_1').select2();
        $('#UnitID_0').select2();
        $('#bArea_0').select2();
        $('#cArea_0').select2();

        $('#typeprop').select2();
        $('#propType').select2();
        $('#propList').select2();
        $('#propListtwo').select2();
        $('#propTypeone').select2();
        $('#furnished').select2();
        $('#pFloor').select2();
        // $('#tFloor').select2();
        // $('#tFloor_Flat').select2();
        // $('#tFloor_Land').select2();
       
        $('#Bedrooms').select2();
        $('#Bathrooms').select2();
        $('#Balconies').select2();
        $('#furnished1').select2();
        $('#pReportingTime').select2();

        $('#UnitID').on('change',function(){
            var data = $('#UnitID option:selected').text();
            if ($('#UnitID').val() != "" && $('#UnitID').val() != null) {
                $('#bArea').html('<option value="'+$('#UnitID').val()+'">'+data+'</option>');
                $('#cArea').html('<option value="'+$('#UnitID').val()+'">'+data+'</option>');
            }

        });
        
        
       

        
        $('#tFloor').on('input', function(){
            // alert();
            var id = $('#tFloor').val();
            if (id <= 100) 
            {
                $('#pFloor').html('');
                
                $('#pFloor').append('<option value="0">Ground Floor</option>');
                for (var i = 1; i <= id; i++) 
                {   
                    $('#pFloor').append('<option value='+i+'>'+i+'</option>');
                }

            }
            else{
               
                // $('#tFloor').val('');
                $('#tFloor').focus();
                
                $('#pFloor').html('<option value="0">Ground Floor</option>');
            }
        });

        $('#tFloor_Other').on('input', function(){
            // alert();
            var id = $('#tFloor_Other').val();
            if (id <= 100) 
            {
                $('#pFloor_Other').html('');
               
                $('#pFloor_Other').append('<option value="0">Ground Floor</option>');
                for (var i = 1; i <= id; i++) 
                {   
                    $('#pFloor_Other').append('<option value='+i+'>'+i+'</option>');
                }

            }
            else{
               
                // $('#tFloor').val('');
                $('#tFloor_Other').focus();
               
                $('#pFloor_Other').html('<option value="0">Ground Floor</option>');
            }
        });



        
        getUnitsDetails();
        getLayoutUnitsDetails(1);
        getLayoutUnitsDetails(0);
        getBHKDetails();
        // getLayoutBHKDetails();
        // getCountryList();
        // getLayoutType();
        AllcheckBoxArray("ExpectedNegociable","PriceNegociable_1");
        AllcheckBoxArray("priceDisplayed","priceDisplay");

        getPropertyType();
        getAmenities();
        getfacing();
        BathroomsDetsils();
       
        $("#propertyName" ).autocomplete(
        {
            source: function (request, response) 
            {
                $("#PropertyID").val('0');
                $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/getSearchProperty",
                    type: "POST",
                    data: request,
                    dataType: 'json',
                    success: function (data) 
                    {
                        response($.map(data, function (el)
                        {
                            return {
                                label: el.tPropertyName,
                                value: el.nPropertyIDPK
                            };
                        }));
                    }
                });
            },
            select:function (e, ui) 
            {
                e.preventDefault(); // uncomment if you want to display name in place of id
                $("#propertyName").val(ui.item.label); 
                $("#PropertyID").val(ui.item.value);// uncomment if you want to display name in place
                getPropertyLocation($("#PropertyID").val());

            },
            focus: function(event, ui) 
            {
                event.preventDefault();
                $("#propertyName").val(ui.item.label);
            }
        });

        $('.isLayout_Flat').on('change',function(){
            // console.log("hello");
            var data = $('.isLayout_Flat').val();
            // console.log(data);

            if ($('#isLayoutHidden').val() = "0") 
            {
                $('#isLayoutHidden').val('0');
                $('#m_accordion_4').css('display','block');
                $('#addLayout_Flat').css('display','block');
                $('#layoutPrices').css('display','block');
            } 
            else 
            {
                $('#isLayoutHidden').val('1');
                $('#m_accordion_4').css('display','none');
                $('#addLayout_Flat').css('display','none');
                $('#layoutPrices').css('display','none');
            }
                // alert($('#isLayoutHidden').val());
        });
        $('.isLayout_House').on('change',function(){
            // console.log("hello");
            
                var data = $('.isLayout_House').val();
                // console.log(data);
                
                if ($('#isLayoutHidden_House').val() != "0")  {

                    $('#isLayoutHidden_House').val('0');
                    $('#m_accordion_4_House').css('display','block');
                    $('#addLayout_House').css('display','block');
                    $('#layoutPrices').css('display','block');
                    

                } else {

                    $('#isLayoutHidden_House').val('1');
                    $('#m_accordion_4_House').css('display','none');
                    $('#addLayout_House').css('display','none');
                    $('#layoutPrices').css('display','none');
                    
                }
        });
        $('#typeprop').on('change', function(){
            
            $id = $('#typeprop').val();
            if ($id == "rent" ) {
                $('#'+$id).css('display','block');
                $('#sale').css('display','none');
            }   
            else if ($id == "sale") {
                $('#'+$id).css('display','block');
                $('#rent').css('display','none');
            }
        });
        $('#propList').on('change', function(){
            getPropertyType();
        });
        $('.time-picker').timepicker({ 
            showMeridian: false,
            minuteStep: 5 
        });
        $('.isAnswer').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {
                $("#reraid").css("display","block");
            }
            if (this.value === "no") 
            {
                $("#reraid").css("display","none");   
            }
        });
           
        
        $('.brokerageYes').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#Brokerage").css("display","block");
            }
            if (this.value === "no") {

                $("#Brokerage").css("display","none");   
            }
        });
        
       
        $('.Cparking').change(function()
        {
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });
        $('.Oparking').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#reraid").css("display","block");
            }
            if (this.value === "no") {

                $("#reraid").css("display","none");   
            }
        });
    
        $("#pLatitutde").focusout(function()
        {
            var latitude = document.getElementById('pLatitutde').value;
           
            var reg = new RegExp("^-{0,1}((90|90.[0]{1,20}|[0-9]|[1-8][0-9])|(89|[0-9]|[1-8][0-9])[.]{1}[0-9]{1,20}){1}$");

            if(latitude != '')
            {
                if(!reg.exec(latitude)) 
                {
                    document.getElementById('pLatitutde').value='';
                    toastr.error("Please Enter valid latitude value.", "latitude");
                }
            } 
        });

        $("#pLongitude").focusout(function()
        {
            var longitude = document.getElementById('pLongitude').value;

            var reg = new RegExp("^-{0,1}((180|180.[0]{1,20}|[0-9]|([0-9][0-9])|([1][0-7][0-9]))|(179|[0-9]|([0-9][0-9])|([1][0-7][0-9]))[.]{1}[0-9]{1,20}){1}$");

            if(longitude != '')
            {
                if(!reg.exec(longitude)) 
                {
                    document.getElementById('pLongitude').value='';
                    toastr.error("Please Enter valid longitude value.", "longitude");   
                }
            }  
        });
    });
    
    $('#select2Country').on('change', function () {    // on country dropdown change state
        getStateList();
    });

    $('#select2State').on('change', function () {    // on country dropdown change state
        getCityList();
    });
    $('#select2City').on('change', function () {    // on country dropdown change state
        getAreaList();
    });
     $('#select2SArea').on('change', function () {    // on country dropdown change state
        getlocality();
    });

    
    function AddLocationArea() 
    {   
        var address = $('#pAddress').val();
        var areaID = $('#areaID').val();
        var cityname = $('#select2City :selected').text();
        // var AreaName = $('#select2City :selected').text();
        var latitude = $('#pLatitutde').val();
        var longitude = $('#pLongitude').val();

        if (address == "")
        {
            toastr.error("Please Select First Location");
        }
        else
        {   
            if (cityname == "-- Select City --") 
            {
                toastr.error("Please Select Different Location","City Not Found");
                $('#pAddress').val('');
            }
            else
            {
                if (areaID == "") 
                {
                    $('#OldCityName').text("Add Area On "+cityname);
    
                    $('#addAreaModal').modal('show');
                }
                else
                {
                    toastr.error("You Can't Add Area","Area Found");
                }
            }
            

        }  
    }
    function AddLocationLocality() 
    {   
        var address = $('#pAddress').val();
        var areaID = $('#areaID').val();
        var LocalityID = $('#LocalityID').val();
        // var  = $('#stateID').val();
        var stateID = $('#select2State :selected').val();
        var cityID = $('#select2City :selected').val();
        var AreaID = $('#select2SArea :selected').val();
        var AreaName = $('#select2SArea :selected').text();
        var localityName = $('#localityName').val();
        var latitude = $('#pLatitutde').val();
        var longitude = $('#pLongitude').val();
        // alert(localityName);

        if (address == "")
        {
            toastr.error("Please Select First Location");
        }
        else
        {   
            if(AreaID == "")
            {
                toastr.error("You Can't Add locality","Area Not Selected");
            }
            else
            {
                if (localityName == "")
                {
                    $('#OldAreaName').text("Add Locality On "+AreaName);
                    $('#addLocalityModal').modal('show');
                }
                else
                {
                    toastr.error("You Can't Add locality","Locality Found");
                }
            }
            
        }  
    }

    $('#AddNewAreaBtn').on('click',function()
    {   
        var cityname = $('#select2City :selected').text();
        var city = $('#select2City :selected').val();
        var areaLatitude = $('#pLatitutde').val();
        var areaLongitude = $('#pLongitude').val();
        var areaName = $('#newArea').val();
        var areaPincode = $('#newAreaPincode').val();
        
        swal({
        title:"Are you sure?",
        text:"You want to Add this Area to "+cityname+" ? ",
        type:"warning", 
        showCancelButton:!0,
        confirmButtonText:"Yes, Add it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>location/createNewArea',
                    type: 'POST',
                    data: {city:city,areaLatitude:areaLatitude,areaLongitude:areaLongitude,areaName:areaName,areaPincode:areaPincode},
                    success: function(data) 
                    {   
                        // console.log(data);
                        if (data.trim() != "Error") 
                        {
                            $('#addAreaModal').modal('hide');
                            $('#areaID').val(data);
                            toastr.success("Area Added successfully","Area");
                            getAreaList();
                        }
                        else
                        {
                            $('#addAreaModal').modal('hide');
                            toastr.error("Error While Adding Area","Area");
                        }
                    }
                });
            }
            else
            {
                $('#addAreaModal').modal('hide');
            }   
       });
        
    });
    $('#AddNewLocalityBtn').on('click',function()
    {   
        var cityname = $('#select2City :selected').text();
        var city = $('#select2City :selected').val();
        var areaLatitude = $('#pLatitutde').val();
        var stateID = $('#select2State :selected').val();
        
        var cityID = $('#select2City :selected').val();
        var areaID = $('#select2SArea :selected').val();
        var areaLongitude = $('#pLongitude').val();
        var LocalityName = $('#newLocality').val();
        var areaPincode = $('#newAreaPincode').val();
        
        swal({
        title:"Are you sure?",
        text:"You want to Add this Locality to "+LocalityName+" ? ",
        type:"warning", 
        showCancelButton:!0,
        confirmButtonText:"Yes, Add it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>location/createNewLocality',
                    type: 'POST',
                    data: {areaID:areaID,stateID:stateID,cityID:cityID,LocalityName:LocalityName,areaLatitude:areaLatitude,areaLongitude:areaLongitude},
                    success: function(data) 
                    {   
                        // console.log(data);
                        if (data.trim() != "Error") 
                        {
                            $('#addLocalityModal').modal('hide');
                            $('#LocalityID').val(data);
                            toastr.success("Locality Added successfully","Locality");
                            getlocality();
                        }
                        else
                        {
                            $('#addLocalityModal').modal('hide');
                            toastr.error("Error While Adding Locality","Locality");
                        }
                     
                    }
                });
            }
            else
            {
                $('#addLocalityModal').modal('hide');
            }   
       });
        
    });

    // $(document).on('input','#Expected',function(){

    //     var Expected = $('#Expected').val();
    //     var UnitID = $('#UnitID :Selected').text();
    //     var SuperBuildUpArea= $('#SuperBuildUpArea').val();
    //     var PricePerUnit = parseInt(Expected) / parseInt(SuperBuildUpArea);
    //     $('#PricePerUnit').val(PricePerUnit)

    // });
    var propertySubTypeID = Array();


    function BathroomsDetsils()
        {
            
            var id = $('#tFloor').val();

            $('pFloor').html('');
         
            $('#pFloor').append('<option value="0">Ground Floor</option>');
            for (var i = 1; i <= id; i++) 
            {   
                $('#pFloor').append('<option value='+i+'>'+i+'</option>');
            }

            var id = $('#tFloor_Other').val();

            $('#pFloor_Other').html('');
           
            $('#pFloor_Other').append('<option value="0">Ground Floor</option>');
            for (var i = 1; i <= id; i++) 
            {   
                $('#pFloor_Other').append('<option value='+i+'>'+i+'</option>');
            }
        };
    function GetProcePerUnit(id) 
    {  
        var listingFor = $('#listingFor:checked').val();

        // alert($('#Expected_'+id).val());
        // if ($('#SuperBuildUpArea_'+id).val() > "0") 
        // { 

        // }
        // else
        // { 
        //     $('#SuperBuildUpArea_'+id).val('0');
        // }

        if ($('#Expected_'+id).val() > "10") 
        {

            convertNumbertoWords('Expected_'+id,'numToWords'+id);


            if($('#villa_Area_details_'+id+':visible').length == 1)
            {   

                // alert($('#villa_Area_details_'+id+':visible').length);
                var SuperBuildUpArea = 0;
                var carpetArea = $('#ploatcarpetArea_'+id).val();



                // if ($('#SuperBuildUpArea_'+id).val() > "0") 
                // {   
                   
                //     if ($('#ploatcarpetArea_'+id).val() == "") 
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                //     else
                //     {
                //         if ($('#ploatcarpetArea_'+id).val() > "2") 
                //         {
                //             SuperBuildUpArea = $('#ploatcarpetArea_'+id).val();
                //         }
                //         else
                //         {
                //             $('#ploatcarpetArea_'+id).focus();
                //         }
                //         // SuperBuildUpArea = $('#ploatcarpetArea_'+id).val();
                //     }
                // }
                // else
                // {   
                //     if ($('#SuperBuildUpArea_'+id).val() > "2") 
                //     {
                //         SuperBuildUpArea = $('#SuperBuildUpArea_'+id).val();
                //     }
                //     else
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                // }
                if ($('#ploatcarpetArea_'+id).val() != null) 
                {
                    SuperBuildUpArea = $('#ploatcarpetArea_'+id).val();
                }
                else
                {
                    $('#ploatcarpetArea_'+id).val('');
                    $('#ploatcarpetArea_'+id).focus();
                }

            }
            else
            {
                var SuperBuildUpArea = 0;

                var carpetArea = $('#ploatcarpetArea_'+id).val();
                // if (SuperBuildUpArea != "" && carpetArea != "") 
                // {
                //     alert("both");
                // }

                // if ($('#SuperBuildUpArea_'+id).val() > "0") 
                // {   
                   
                //     if ($('#carpetArea_'+id).val() == "") 
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                //     else
                //     {   
                //         if ($('#carpetArea_'+id).val() > "0") 
                //         {
                //             SuperBuildUpArea = $('#carpetArea_'+id).val();
                //         }
                //         else
                //         {
                //             $('#carpetArea_'+id).focus();
                //         }
                //         // SuperBuildUpArea = $('#carpetArea_'+id).val();
                //     }
                // }
                // else
                // {   
                //     if ($('#SuperBuildUpArea_'+id).val() > "0") 
                //     {
                //         SuperBuildUpArea = $('#SuperBuildUpArea_'+id).val();
                //     }
                //     else
                //     {
                //         $('#SuperBuildUpArea_'+id).focus();
                //     }
                    
                // }
                if ($('#carpetArea_'+id).val() != null) 
                {
                    SuperBuildUpArea = $('#carpetArea_'+id).val();
                }
                else
                {   
                    $('#carpetArea_'+id).val('');
                    $('#carpetArea_'+id).focus();
                }
               
            }

            if (listingFor == 1)
            {   
                var Expected = $('#Expected_'+id).val();
                var UnitID = $('#UnitID :Selected').text();
                var PricePerUnit = parseInt(Expected) / parseInt(SuperBuildUpArea);
                if (SuperBuildUpArea  != "") 
                {
                   
                    $('#PricePerUnit_'+id).val(parseInt(PricePerUnit));
                }
               
            }
            else
            {
                var Expected = $('#Expected_'+id).val();
                var UnitID = $('#UnitID :Selected').text();
                var PricePerUnit = parseInt(Expected) / parseInt(SuperBuildUpArea);
                if (SuperBuildUpArea  != "") 
                {
                    $('#PricePerUnit_'+id).val(parseInt(PricePerUnit));
                } 
            }
             convertNumbertoWords('PricePerUnit_'+id,'pnumToWords'+id);
        }
        
    }
    function changeBackground(id) 
    {       
            document.getElementById("m_form").reset();
            $('.m-select2').trigger('change');

            $('#Resale').css('background-color','#ffffff');
            $('#Project').css('background-color','#ffffff');
            $('#Resale').css('color','#000000 !important');
            $('#Project').css('color','#000000 !important');
            $('#'+id).css('background-color','#9a9a9a');
            $('#'+id).css('color','#ffff !important');
            
            $('#Resale :input[type=radio]').attr('checked',false);
            $('#Project :input[type=radio]').attr('checked',false);

            $('#'+id+' :input[type=radio]').attr('checked',true);

            // alert($('#'+).val());
    }
    function changeBackgroundOwnership(id) 
    {
            $('#Freehold').css('background-color','#ffffff');
            $('#Leasehold').css('background-color','#ffffff');
            $('#Society').css('background-color','#ffffff');
            
            $('#Freehold .m-option__title').css('color','#000000 !important');
            $('#Leasehold .m-option__title').css('color','#000000 !important');
            $('#Society .m-option__title').css('color','#000000 !important');
            $('#'+id).css('background-color','#9a9a9a');
            $('#'+id+' .m-option__title').css('color','#ffff !important');
            // alert($('#'+).val());
    }
    $(document).on('change','#listingFor',function()
    {
        document.getElementById("m_form").reset();
        $('.m-select2').trigger('change');
        $('#property_display_preview').html('');
        numFiles = 0;
        $('#imgCount').text('Images Selected : ( 0 / 25 )');
        deletedFiles = [];
        files = [];
        $('#numToWords1').html('');
        $('#pnumToWords1').html('');
        $('#bnumToWords1').html('');
        $('#propertiesSubType').val('');
        // console.log(deletedFiles);
        var listingFor = $('#listingFor:checked').val();
        var type = $('#propList').val();
        
        $('#propList').trigger('change');


        if (listingFor == 1) 
        {   
            
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            getPropertyType();
        }
        else
        {
           
            $('#S_Build_Area_Hidden_1').css('display','none');
            $('#Build_Area_Hidden_1').css('display','none');
            $('#S_Build_Area_Hidden').css('display','block');
            $('#Build_Area_Hidden').css('display','block');
            getPropertyType();
            
        }

    });
    var layoutCount = 2;
    var CommercialLayoutCount = 2;
    var layoutCountHidden = 2;
    

    function showselect(id) 
    {   
        // document.getElementById("m_form").reset();
        // document.getElementById("m_form").reset();
        // $('.m-select2').trigger('change');
        // $('form').find("input[type=text], textarea,select").reset;
        var type = $('#propList').val();

        var listingFor = $('#listingFor:checked').val();
        var countSubtype =  $('#propertiesSubType').val();
        // console.log(countSubtype);
        // console.log(countSubtype);
        // console.log(propertySubTypeID);
        $('.subTypeLabelAll').css('color','#818387');
        $('.tickClassOverImg').css('display','none');
        $('#propertiesSubType').val(id);

        $('#subTypeLabel_'+id).css('color','black');
        $('.ImagesCircle').css('border','none');
        $('#subTypeImg_'+id).css('display','block');



        changeBackgroundOwnership('Freehold');
        $('#numToWords1').html('');
        $('#pnumToWords1').html('');
        $('#bnumToWords1').html('');


        layoutCount = 2;
        layoutCountHidden = 1;
        CommercialLayoutCount = 2;

        $("#layoutDetails_Flat").children(":not(#m_accordion_4)").remove();

        
       
        
       

        $('#addOtherslayout').css('display','none');
        $('#addLandlayout').css('display','none');
        $('#addCommercial').css('display','none');
        $('#addLayout').css('display','none');
        $('#addLayout_Flat').css('display','none');

        // $("#propFor").select2().val('0').trigger('change');
        $("#washroom_Office").select2().val("0").trigger("change");
        $("#balcony_Office").select2().val("0").trigger("change");
        $("#Availability").select2().val("1").trigger("change");
        $("#UnitID_1").select2().val("1").trigger("change");
        $("#layoutPropBHK").select2().val("1").trigger("change");
        $("#facingID").select2().val("1").trigger("change");
        $("#propertyFurnishedID").select2().val("1").trigger("change");
        $("#Bedrooms").select2().val("0").trigger("change");
        $("#Bathrooms").select2().val("0").trigger("change");
        $("#Balconies").select2().val("0").trigger("change");
        $("#pFloor").select2().val("0").trigger("change");
        $("#Cparking0").val("0").trigger("change");
        $("#Room").val("0").trigger("change");
        $("#propBHK_Other").select2().val("0").trigger("change");
        $("#facingID_Other").select2().val("0").trigger("change");
        $("#pFloor_Other").select2().val("0").trigger("change");
        $("#Room_Other").val("0").trigger("change");
        $("#washroom_Office").select2().val("0").trigger("change");
        $("#balcony_Office").select2().val("0").trigger("change");
        $("#furnished_office").select2().val("0").trigger("change");
        $("#pentry_Office").select2().val("0").trigger("change");             

       
        
        // alert(listingFor);

        $('#PropoertyDesign_'+id).css('display','block');

        

        // alert(listingFor);

        var subtype = $('#propertiesSubType').val();
        var subTypeName = $('#subTypeLabel_'+subtype).text();

        
        /* Flat Layout UI  Start */
        if (subTypeName == "Flat/Apartment") 
        {   
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            
            $('#Flat_layout').css('display','block');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Layout_Hidden').css('display','block');
            $('#layoutPrices').css('display','none');
           
           
            $('#villa_Area_details_1').css('display','none');
           
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }       
        }
        else if (subTypeName == "Penthouse") 
        {   
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Flat_layout').css('display','block');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Layout_Hidden').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            
           
            $('#villa_Area_details_1').css('display','none');

            
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
                       
        }
        else if (subTypeName == "Independent House") 
        {   
           $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');

            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Flat_layout').css('display','block');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Layout_Hidden').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#villa_Area_details_1').css('display','block');
            $('#Office').css('display','none');
            
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
                      
        }
        /* Flat Layout UI  End */

        /* Villa Layout UI Start */


        else if (subTypeName == "Villa/Bungalow") 
        {
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Layout_Hidden').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Flat_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
           
            $('#addLayout_Flat').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');
            // $('#villa_Area_details_1').css('display','block');
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
            


             // $(".select2").val("").trigger("change");

            // $('#House_layout').css('display','block');
        }
        else if (subTypeName == "Row House") 
        {
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Layout_Hidden').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Flat_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            
            $('#addLayout_Flat').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');
            $('#villa_Area_details_1').css('display','block');
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
           
            // $('#House_layout').css('display','block');
        }
        else if (subTypeName == "Farm House") 
        {
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Layout_Hidden').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Flat_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
           
            $('#addLayout_Flat').css('display','none');
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');
            $('#villa_Area_details_1').css('display','block');
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addLayout_Flat').css('display','block');   
            }
           

            // $('.m-select2 :selected').prop('selected', function() {
            //     return this.defaultSelected;
            // }).trigger('change');


            // $(".m-select2 :selected").removeAttr("selected");

            // $("select").each(function () {
            //         $(this).select2('val', '');
            //     });
            // $(".m-select2 :selected").removeAttr("selected").trigger('change');
            // $("select").val('0').reset();
            // $('.m-select2').reset().trigger('change');
            // $('.m-select2').val().trigger('change');

            // $("select.m-select2").select2({ allowClear: true }); // re-init to show default status
        }


        /* Villa Layout UI End */

        /* Basic UI Display Start */    
        
        else if (subTypeName == "Plot/Land") 
        {
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Build_Area_Hidden').css('display','none');
            $('#Land_layout').css('display','none');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
           
            $('#addLayout_Flat').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            $('#Carparking').css('display','none');
            if (listingFor == 1) 
            {
                
                $('#addLandlayout').css('display','block');
               
            }
        }
        
        /* Basic UI Display End */

        /* Others UI Display Start */

        else if (subTypeName == "Others") 
        {
            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Others_layout').css('display','block');
            $('#layoutPrices').css('display','none');
            $('#Office').css('display','none');
            $('#addLayout_Flat').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            $('#Carparking').css('display','block');

            if (listingFor == 1) 
            {
                $('#addOtherslayout').css('display','block');
            }
            
        } 

        /* Others UI Display End */



        /* Office UI Display Start */

        else if (subTypeName == "Office Space") 
        {
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');

            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Office').css('display','block');
            $('#addLayout_Flat').css('display','none');
            $('#layoutPrices').css('display','none');
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','none');
            

            if (listingFor == 1) 
            {
                $('#addLayout').css('display','block');
            }
            
        }
        else if (subTypeName == "Shop")
        {
            // $('#S_Build_Area_Hidden').css('display','block');
            // $('#Build_Area_Hidden').css('display','block');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');

            $('#S_Build_Area_Hidden').css('display','block');
            $('#Build_Area_Hidden').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Ploat_Area').html("Super Built Up Area");
            $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
            $('#Office').css('display','block');
            $('#layoutPrices').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            $('#Carparking').css('display','block');
            if (listingFor == 1) 
            {
               
                $('#addLayout').css('display','block');
            }

        }
        else if (subTypeName == "Showroom") 
        {   
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');

            
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Office').css('display','block');
            $('#layoutPrices').css('display','none');
            
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','none');
            // $(".select2").val("").trigger("change");

            if (listingFor == 1) 
            {
                $('#addLayout').css('display','block');
            }
           
           
        }
        else if (subTypeName == "Corporate House") 
        {   
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','none');

            
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Office').css('display','block');
            $('#layoutPrices').css('display','none');
           
            $('#Carparking').css('display','block');
            $('#villa_Area_details_1').css('display','block');
            // $(".select2").val("").trigger("change");

            if (listingFor == 1) 
            {
                $('#addLayout').css('display','block');
            }
            
        }

        /* Office UI Display End */

        /* Basic UI Display Start */ 

        else if (subTypeName == "Industrial Land") 
        {   
            $('#S_Build_Area_Hidden').css('display','none');
            $('#Build_Area_Hidden').css('display','none');

            $('#S_Build_Area_Hidden_1').css('display','block');
            $('#Build_Area_Hidden_1').css('display','block');
            $('#Flat_layout').css('display','none');
            $('#Land_layout').css('display','none');
            $('#House_layout').css('display','none');
            $('#Others_layout').css('display','none');
            $('#Office').css('display','none');
            $('#Ploat_Area').html("Plot Area");
            $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
            $('#Carparking').css('display','none');
             $('#villa_Area_details_1').css('display','none');
            if (listingFor == 1) 
            {
                $('#addLandlayout').css('display','block');
            }
            
           
            $('#layoutPrices').css('display','none');
            
            // $(".select2").val("").trigger("change");

        }
            /* villa_Area_details_1 Show UI Display Start */ 
            else if (subTypeName == "Industrial Factory") 
            {   
                $('#S_Build_Area_Hidden').css('display','none');
                $('#Build_Area_Hidden').css('display','none');

                $('#S_Build_Area_Hidden_1').css('display','block');
                $('#Build_Area_Hidden_1').css('display','none');

                
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Office').css('display','none');
                $('#Ploat_Area').html("Plot Area");
                $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
               
                $('#layoutPrices').css('display','none');
               
                $('#Carparking').css('display','none');
                $('#villa_Area_details_1').css('display','block');
                // $(".select2").val("").trigger("change");

                if (listingFor == 1) 
                {
                    $('#addLandlayout').css('display','block');
                }
                
            }

            else if (subTypeName == "Warehouse") 
            {   
                $('#S_Build_Area_Hidden').css('display','none');
                $('#Build_Area_Hidden').css('display','none');

                $('#S_Build_Area_Hidden_1').css('display','block');
                $('#Build_Area_Hidden_1').css('display','none');

                
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Office').css('display','none');
                $('#Ploat_Area').html("Plot Area");
                $('#SuperBuildUpArea').attr("placeholder", "Plot Area");
               
                $('#layoutPrices').css('display','none');
               
                $('#Carparking').css('display','none');
                $('#villa_Area_details_1').css('display','block');
                // $(".select2").val("").trigger("change");

                if (listingFor == 1) 
                {
                    $('#addLayout').css('display','block');
                }
                
            } 
            /* villa_Area_details Show UI Display Start */ 

        /* Basic UI Display End */

        else
        {
            /* Flat_layout UI Display Start */ 

            if (type == 1) 
            {
               
                $('#S_Build_Area_Hidden').css('display','block');
                $('#Build_Area_Hidden').css('display','block');
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Office').css('display','none');
                $('#Ploat_Area').html("Plot Area");
                $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
                $('#Flat_layout').css('display','block');
                $('#layoutPrices').css('display','none');
                
                $('#Carparking').css('display','block');
                $('#villa_Area_details_1').css('display','none');

                if (listingFor == 1) 
                {
                    $('#addLayout_Flat').css('display','block');   
                }
                 
            }

            /* Flat_layout UI Display End */

            /* Office UI Display Start */
            else
            {
                $('#S_Build_Area_Hidden_1').css('display','block');
                $('#Build_Area_Hidden_1').css('display','block');
                $('#Flat_layout').css('display','none');
                $('#Land_layout').css('display','none');
                $('#House_layout').css('display','none');
                $('#Others_layout').css('display','none');
                $('#Ploat_Area').html("Super Built Up Area");
                $('#SuperBuildUpArea').attr("placeholder", "Super Built Up Area");
                $('#Office').css('display','block'); 
                $('#addLayout').css('display','block');
                $('#layoutPrices').css('display','none'); 
                $('#BuildUpArea').val('');
                $('#carpetArea').val('');
                $('#tFloor_Land').select2().val("1").trigger("change");
                $('#addLayout_Flat').css('display','none');
                $('#Carparking').css('display','block');
                $('#villa_Area_details_1').css('display','none');

                if (listingFor == 1) 
                {
                  
                    $('#addLayout').css('display','block');
                }
                

            }

            /* Office UI Display End */
        }
    }
   
   
    function getPropertyType() 
    {

        var id = $('#propList').val();

            $.ajax({

            url: '<?php echo Urls::$BASE ?>properties/getSubTypeDetails',
            type: 'POST',
            data: {id: id},
            success: function (data) {
                // console.log(data);
                $('#propertySubTypes').html('');

                $('#propertiesSubType').val('');
                propertySubTypeID = [];

                // $('#select2State').append('<option value="">-- Select State --</option>');

                var json = JSON.parse(data);
                for (var i = 0; i < json.length; i++) {

                    var data = "<?php if(isset($this->data)) {echo $this->data['nPropertySubTypeIDFK']; } else { echo NULL; } ?>";
                   
                    if (i == 0) 
                    {
                         $('#propertySubTypes').append('\
                                                <div class="from-group col-lg-2" style="cursor:pointer;" onclick="showselect('+json[i].nPropertySubTypeIDPK+');" id="'+json[i].tPropertySubTypeName+'"><br>\
                                                    <a style="cursor:pointer;pointer-events: none;" >\
                                                        <img class="ImagesCircle" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING?>'+json[i].tPropertySubTypeImagePath+'" width="40px" height="40px"><i class="fa fa-check-circle tickClassOverImg" id="subTypeImg_'+json[i].nPropertySubTypeIDPK+'"></i>\
                                                        <br><br>\
                                                        <label style="cursor:pointer;" class="subTypeLabelAll" style="color:#818387" id="subTypeLabel_'+json[i].nPropertySubTypeIDPK+'">'+json[i].tPropertySubTypeName+'</label>\
                                                    </a>\
                                                </div>\ ');
                        showselect(json[i].nPropertySubTypeIDPK);
                        $('#subTypeImg_'+json[i].nPropertySubTypeIDPK).css('display','block');
                        $('#propertiesSubType').val(json[i].nPropertySubTypeIDPK);
                        $('#subTypeLabel_'+json[i].nPropertySubTypeIDPK).css('color','black');
                    }
                    else{
                        $('#propertySubTypes').append('\
                                                <div class="from-group col-lg-2" style="cursor:pointer;" onclick="showselect('+json[i].nPropertySubTypeIDPK+');" id="'+json[i].tPropertySubTypeName+'"><br>\
                                                    <a style="cursor:pointer;"  >\
                                                        <img class="ImagesCircle" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_LISTING?>'+json[i].tPropertySubTypeImagePath+'" width="40px" height="40px"><i class="fa fa-check-circle tickClassOverImg" id="subTypeImg_'+json[i].nPropertySubTypeIDPK+'"></i>\
                                                        <br><br>\
                                                        <label style="cursor:pointer;" class="subTypeLabelAll" style="color:#818387" id="subTypeLabel_'+json[i].nPropertySubTypeIDPK+'">'+json[i].tPropertySubTypeName+'</label>\
                                                    </a>\
                                                </div>\ ');
                    }
                                            
                }
            }

            });
    }
    
    // function getLayoutType() 
    // {
    //     $.ajax({
    //         url: '<?php echo Urls::$BASE ?>properties/getLayoutType',
    //         type: 'POST',
    //         success: function (data) {
    //             // console.log(data);
    //            var layoutType = JSON.parse(data);

    //             //console.log(country.length);
    //             $('#layoutType').html('');

    //             for(var i = 0; i < layoutType.length; i++)
    //             {
    //                 var data = "<?php if(isset($this->data)) {echo $this->data[0]['nPropertyDerivedTypeIDPK']; } else { echo NULL; } ?>";
    //                 //console.log(data);
    //                 if(data == layoutType[i].nPropertyDerivedTypeIDPK)
    //                     $('#layoutType').append('<option value='+layoutType[i].nPropertyDerivedTypeIDPK+' selected>'+layoutType[i].tPropertyDerivedTypeName+'</option>');
    //                 else
    //                     $('#layoutType').append('<option value='+layoutType[i].nPropertyDerivedTypeIDPK+'>'+layoutType[i].tPropertyDerivedTypeName+'</option>');
    //             }
    //             $("#layoutType").trigger("change");
    //         }

    //         });
    // }
    $('.input-group-append').click(function() {
        $(".datepicker").focus();
      });
   
    function getfacing() 
    {
        $.ajax({
            url: '<?php echo Urls::$BASE ?>properties/getfacingDetails',
            type: 'POST',
            success: function (data) {
                // console.log(data);
               var facing = JSON.parse(data);

                //console.log(country.length);
                $('#facingID').html('');
                $('#facingID_House').html('');
                $('#facingID_Other').html('');
                $('#facingID'+k).html('');

                    



                for(var i = 0; i < facing.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nFacingIDPK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == facing[i].nFacingIDPK)
                    { 
                        $('#facingID').append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');
                        $('#facingID_House').append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');
                        $('#facingID_Other').append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');

                    }
                    else{

                        $('#facingID').append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
                        $('#facingID_House').append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
                        $('#facingID_Other').append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
                    }
                }
                $("#facingID").trigger("change");
            }

            });
    }
    
    
    function getLayoutUnitsDetails(id) 
    {      
            $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getUnitsDetails',
            type: 'POST',
            success: function(data) 
            {
               
                $('#UnitID_'+id).empty();
             
            
                var country = JSON.parse(data);
                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nUnitIDPK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nUnitIDPK)
                    {
                        
                        $('#UnitID_'+id).append('<option value='+country[i].nUnitIDPK+' selected>'+country[i].tUnitName+'</option>');
                    }
                    else
                    {
                      
                        $('#UnitID_'+id).append('<option value='+country[i].nUnitIDPK+'>'+country[i].tUnitName+'</option>');
                       
                    }
                }
              
                $("#UnitID_"+id).trigger("change");

            }
        });
    }
    function getUnitsDetails() 
    {   
            $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getUnitsDetails',
            type: 'POST',
            success: function(data) 
            {
                $('#UnitID').empty();
              
                
            
                var country = JSON.parse(data);
                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nUnitIDPK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nUnitIDPK)
                    {
                        $('#UnitID').append('<option value='+country[i].nUnitIDPK+' selected>'+country[i].tUnitName+'</option>');
                        
                    }
                    else
                    {
                        $('#UnitID').append('<option value='+country[i].nUnitIDPK+'>'+country[i].tUnitName+'</option>');
                      
                       
                    }
                }
                $("#UnitID").trigger("change");
             
            }
        });
    }
    $('#UnitID_1').on('change',function(){

            var data = $('#UnitID_1 option:selected').text();

            if ($('#UnitID_1').val() != "" && $('#UnitID_1').val() != null) {
                $('#bArea_1').html('<option value="'+$('#UnitID_1').val()+'" Selected>'+data+'</option>');
                $('#cArea_1').html('<option value="'+$('#UnitID_1').val()+'" Selected>'+data+'</option>');
            }
    });
    $('#UnitID_0').on('change',function(){

            var data = $('#UnitID_0 option:selected').text();

            if ($('#UnitID_0').val() != "" && $('#UnitID_0').val() != null) {
                $('#bArea_0').html('<option value="'+$('#UnitID_0').val()+'" Selected>'+data+'</option>');
                $('#cArea_0').html('<option value="'+$('#UnitID_0').val()+'" Selected>'+data+'</option>');
            }
    });
    function getAmenities() 
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getAmenitiesDetails',
            type: 'POST',
            success: function(data) 
            {
                $('#Amenities').empty();
                // $('#Amenities').append('<option value="">Select Unit</option>');

                var amenity = JSON.parse(data);

                //console.log(country.length);

                for(var i = 0; i < amenity.length; i++)
                {
                    
                        $('#Amenities').append('<div class="col-lg-3">\
                                            <br>\
                                            \
                                            <a style="cursor: pointer;" class="buttonDemo" onclick="showDetails('+amenity[i].nAmenityIDPK+');">\
                                            <img src="<?php echo Urls::$BASE.Urls::$ABOUTUS?>'+amenity[i].tAmenityImagePath+'" width="40px" height="40px" class="unChecked"  id="featureImg_'+amenity[i].nAmenityIDPK+'"><i class="fa fa-check-circle tickClassOverImg" id="iconImg_'+amenity[i].nAmenityIDPK+'" ></i>\
                                                </a>\  <br><br>\<label style="color:#818387" class="subTypeLabelAll" id="features_'+amenity[i].nAmenityIDPK+'">'+amenity[i].tAmenityName+'</label>\
                                            </div>');
                    
                }
                // $("#Amenities").trigger("change");
            }
        });
    }
    
    var selectedAmenityArray = Array();
    
    function ChangeUnitePriceName(count) 
    {
        var data = $('#UnitID_'+count+' :selected').text();
        // alert(data);
        $('#perUnitPrice_'+count).text('Price Per '+data);
    }

    function showDetails(id) {             
        
        if($.inArray(id,selectedAmenityArray) != -1)
        {
            var index = selectedAmenityArray.indexOf(id);

            if(index!=-1)
            {
                selectedAmenityArray.splice(index, 1);
            }

        }
        else
        { 
            selectedAmenityArray.push(id);
        }

        var checkForClass = $('#featureImg_'+id).hasClass('unChecked');

        if(checkForClass) 
        {
            $('#featureImg_'+id).removeClass('unChecked');
            $('#iconImg_'+id).css('display','block');
            $('#features_'+id).css('color','black');
        } 
        else
        {
            $('#featureImg_'+id).addClass('unChecked');
            $('#iconImg_'+id).css('display','none');
            $('#features_'+id).css('color','#818387');
        }

        $('#AminitiesHidden').val(selectedAmenityArray);
        // console.log(selectedAmenityArray);

    }
   
   
    var CommercialCount = 0;
    var k = 0;
    var id = "";
   

    $(document).on('click','#addLayout_House',function(){

        getLayoutBHKDetails(layoutCount);
        getLayoutFacing(layoutCount);
        addLayoutDetials("House");

    });
    $(document).on('click','#addLayout_Flat',function(){
        
        getLayoutBHKDetails(layoutCount);
        getLayoutFacing(layoutCount);
        addLayoutDetials("Flat");

    });
    
    var layoutImages = Array();
    layoutImages.push('layout_image1');
    // console.log($('#'+layoutImages[0]));
    
   
    function addLayoutDetials(id)
    {      
        $('#layoutCount').val(layoutCountHidden + 1);
        // alert(layoutCountHidden);

        $('#layoutDetails_'+id).append('\
                                <div id="'+layoutCount+'">\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+layoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+layoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+layoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                <span class="m-accordion__item-icon" onclick="removeDivTag('+layoutCount+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 12px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+layoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+layoutCount+'" data-parent="#m_accordion_4_'+layoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+layoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+layoutCount+'" onchange="ChangeUnitePriceName('+layoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+layoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+layoutCount+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" required id="carpetArea_'+layoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+layoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text"  value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+layoutCount+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" value="0"  onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+layoutCount+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text"   onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" required id="ploatcarpetArea_'+layoutCount+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text"  value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+layoutCount+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" value="0"  onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+layoutCount+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" value="0"  onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" required id="constructioncarpetArea_'+layoutCount+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="row">\
                                                        <div class="col-lg-4">\
                                                            <label>Select BHK</label>\
                                                            <select style="width: 100%;" name="layout['+"layoutPropBHK"+'][]" class="form-control m-select2" id="layoutPropBHK'+layoutCount+'">\
                                                                <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="3+">3+ </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Select Facing</label>\
                                                            <select style="width: 100%;" name="layout['+"facingID"+'][]" class="form-control m-select2" id="facingID'+layoutCount+'">\
                                                                 <option value="1">1 </option>\
                                                                <option value="2">2 </option>\
                                                                <option value="3">3 </option>\
                                                                <option value="4">4 </option>\
                                                            </select>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                    <label>Property Furnished</label>\
                                                                    <br>\
                                                                    <select id="propertyFurnishedID'+layoutCount+'" name="layout['+"propertyFurnishedID"+'][]" class="form-control m-select2" style="width: 100%;">\
                                                                       <option value="1">Furnished</option>\
                                                                       <option value="0">Un-Furnished </option> \
                                                                       <option value="2">Semi-Furnished </option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+layoutCount+'\','+layoutCount+')" id="RemoveImage_'+layoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+layoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+layoutCount+'\').click();" accept="image/x-png,image/jpeg,image/jpg">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" id="layout_image'+layoutCount+'" onchange="layout_preview(this,'+layoutCount+')" accept="image/x-png,image/jpeg,image/jpg" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+layoutCount+'\').click();" id="layout_preview_display_'+layoutCount+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-8">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                    <label>Bedrooms</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Bedrooms"+'][]"  id="Bedrooms'+layoutCount+'">\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Bathrooms</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Bathrooms"+'][]" id="Bathrooms'+layoutCount+'">\
                                                                        <option value="0">0</option>\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Balconies</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"Balconies"+'][]" id="Balconies'+layoutCount+'">\
                                                                        <option value="0">0</option>\
                                                                        <option value="1">1</option>\
                                                                        <option value="2">2</option>\
                                                                        <option value="3">3</option>\
                                                                        <option value="4">4</option>\
                                                                        <option value="5">5</option>\
                                                                        <option value="6">6</option>\
                                                                        <option value="7">7</option>\
                                                                        <option value="8">8</option>\
                                                                        <option value="9+">9+</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                   <label>Total Floors</label>\
                                                                   <input type="text" onkeypress="return isNumberKey(event);" placeholder="Total Floors" class="form-control m-input" style="width: 100%;" value="0" class="form-control m-select2" name="layout['+"tFloor"+'][]"  max="100" id="tFloor'+layoutCount+'" oninput="getPFlor('+layoutCount+');">\
                                                                </div>\
                                                                <div class="col-lg-4" id="Layout_Hidden">\
                                                                    <label>Property On Floor</label>\
                                                                    <select style="width: 100%;" class="form-control m-select2" name="layout['+"pFloor"+'][]" id="pFloor'+layoutCount+'" style="display: block;">\
                                                                        <option value="0">Ground Floor</option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Available Parking</label>&nbsp;&nbsp;&nbsp;\
                                                                    <div class="input-group">\
                                                                        <div class="input-group-append" style="cursor: pointer;" onclick="removeOpenParking('+layoutCount+')">\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-minus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                       <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" id="Cparking'+layoutCount+'" name="layout['+"Cparking"+'][]"  value="0"/>\
                                                                       <div class="input-group-append" style="cursor: pointer;" onclick="addOpenParking('+layoutCount+')"\>\
                                                                            <span class="input-group-text">\
                                                                                 <i class="fa fa-plus"></i>\
                                                                            </span>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <p style="width: 100%;">Store Room</p>\
                                                                    <input type="hidden" name="layout['+"storeRoomAvailable"+'][0]" id="storeRoomAvailable'+layoutCount+'" value="">\
                                                                    <input  name="layout['+"storeRoomAvailable"+'][]" id="Room'+layoutCount+'" class="StoreRoom" onchange=AllcheckBoxArray("StoreRoom","storeRoomAvailable'+layoutCount+'")  type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <br>\
                                                                    <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                                    <input type="hidden" name="layout['+"poojaRoomAvailable"+'][0]" id="poojaRoomAvailable'+layoutCount+'" value="">\
                                                                    <p style="width: 100%;">Pooja Room</p>\
                                                                    <input  name="layout['+"poojaRoomAvailable"+'][]" id="Room'+layoutCount+'" class="PoojaRoom" onchange=AllcheckBoxArray("PoojaRoom","poojaRoomAvailable'+layoutCount+'") type="checkbox">\
                                                                    <span></span>\
                                                                    </label>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text"  value="0"class="form-control m-input" name="layout[Expected][]" required id="Expected_'+layoutCount+'" maxlength="11" oninput="GetProcePerUnit('+layoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+layoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice_'+layoutCount+'">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+layoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+layoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" value="0" class="form-control m-input" name="layout[BookingPrize][]" id="Booking'+layoutCount+'" oninput="convertNumbertoWords(\'Booking'+layoutCount+'\',\'bnumToWords'+layoutCount+'\');" maxlength="10" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+layoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+layoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+layoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+layoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+layoutCount+'" value="">\
                                                            <input name="layout[priceDisplayed][]" id="priceDisplayed_'+layoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+layoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                                ');
         var listingFor = $('#listingFor:checked').val();
        
        k = layoutCount;

        layoutImages.push('layout_image'+layoutCount);
        // alert(k);
        AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+layoutCount);
        AllcheckBoxArray('priceDisplayed','priceDisplay_'+layoutCount);

        AllcheckBoxArray('StoreRoom','storeRoomAvailable'+layoutCount);
        AllcheckBoxArray('PoojaRoom','poojaRoomAvailable'+layoutCount);

        $('#S_Build_Area_Hidden_'+layoutCount).css('display','block');
        $('#Build_Area_Hidden_'+layoutCount).css('display','block');

        var subtype = $('#propertiesSubType').val();
        var subTypeName = $('#subTypeLabel_'+subtype).text();

        // ChangeUnitePriceName(layoutCount);

        if (subTypeName == "Villa/Bungalow") 
        {
            $('#villa_Area_details_'+layoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+layoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Row House") 
        {
            $('#villa_Area_details_'+layoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+layoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Farm House") 
        {
            $('#villa_Area_details_'+layoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+layoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+layoutCount+'').css('display','none');   
        }

        

        $('#propertyFurnishedID'+layoutCount).select2();
        $('#propBHK'+layoutCount).select2();
        $('#layoutPropBHK'+layoutCount).select2();

        $('#kitchen'+layoutCount).select2();
        $('#facingID'+layoutCount).select2();
        $('#layoutType'+layoutCount).select2();
        $('#typeprop'+layoutCount).select2();
        $('#propType'+layoutCount).select2();
        $('#propList'+layoutCount).select2();
        $('#propListtwo'+layoutCount).select2();
        $('#propTypeone'+layoutCount).select2();
        $('#furnished'+layoutCount).select2();
        $('#pFloor'+layoutCount).select2();
      
        $('#Bedrooms'+layoutCount).select2();
        $('#Bathrooms'+layoutCount).select2();
        $('#Balconies'+layoutCount).select2();
        $('#furnished1'+layoutCount).select2();
        $('#pReportingTime'+layoutCount).select2();
        $('#UnitID_'+layoutCount).select2();
        $('#bArea_'+layoutCount).select2();
        $('#cArea_'+layoutCount).select2();

        getLayoutUnitsDetails(k);

        if (id == "House" ) 
        {
            $('#pLayout_Hidden').css('display','none');
        }

        

        layoutCount = layoutCount + 1;
        layoutCountHidden = layoutCountHidden + 1; 
        // alert(layoutCountHidden);

        // alert('#Parkingyes'+k);
    }

    function addCommercial(id) 
    {
        $('#layoutCount').val(layoutCountHidden + 1);
        $('#layoutDetails_'+id).append('\
                                    <div id="'+CommercialLayoutCount+'">\
                                        <div class="m-accordion m-accordion" id="m_accordion_4_'+CommercialLayoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+CommercialLayoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                 <span class="m-accordion__item-icon" onclick="removeDivTag('+CommercialLayoutCount+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 17px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+CommercialLayoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-parent="#m_accordion_4_'+CommercialLayoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+CommercialLayoutCount+'" onchange="ChangeUnitePriceName('+CommercialLayoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+CommercialLayoutCount+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" value="0"onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text"  onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" required id="carpetArea_'+CommercialLayoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+CommercialLayoutCount+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text"  value="0"onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+CommercialLayoutCount+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" required id="ploatcarpetArea_'+CommercialLayoutCount+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Construction Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text"  value="0"onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+CommercialLayoutCount+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+CommercialLayoutCount+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" value="0" required onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+CommercialLayoutCount+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-8">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Washroom</label>\
                                                                    <select style="width: 100%;" name="layout[washroom][]" class="form-control m-select2" id="washroom_Office_'+CommercialLayoutCount+'">\
                                                                        <option value="0">0 </option>\
                                                                        <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="3+">3+ </option>\
                                                                    </select>\
                                                                </div>\
                                                                 <div class="col-lg-6">\
                                                                    <label>Balcony</label>\
                                                                    <select style="width: 100%;" name="layout[balcony][]" class="form-control m-select2" id="balcony_Office_'+CommercialLayoutCount+'">\
                                                                        <option value="0">0 </option>\
                                                                        <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="4">4 </option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                   <label>Property Furnished</label>\
                                                                    <br>\
                                                                    <select id="furnished_office_'+CommercialLayoutCount+'" name="layout[Furnished_office][]" class="form-control m-select2" style="width: 100%;">\
                                                                       <option value="1">Furnished</option>\
                                                                       <option value="0">Un-Furnished </option> \
                                                                       <option value="2">Semi-Furnished </option>\
                                                                    </select>\
                                                                </div>\
                                                                <div class="col-lg-6">\
                                                                    <label>Pentry</label>\
                                                                    <select style="width: 100%;" name="layout[pentry][]" class="form-control m-select2" id="pentry_Office_'+CommercialLayoutCount+'">\
                                                                        <option value="0">0 </option>\
                                                                         <option value="1">1 </option>\
                                                                        <option value="2">2 </option>\
                                                                        <option value="3">3 </option>\
                                                                        <option value="4">4 </option>\
                                                                    </select>\
                                                                </div>\
                                                                <br>\
                                                                <div class="col-lg-1">\
                                                                </div>\
                                                                <div class="col-lg-6">\
                                                                    <br>\
                                                                    <div class="row">\
                                                                        <label>Available <br> Parking</label>&nbsp;&nbsp;&nbsp;\
                                                                        <div class="col-md-1" style="cursor: pointer;" onclick="removeOpenParking('+CommercialLayoutCount+')">\
                                                                            <i style="margin-top: 12px;" class="fa fa-minus" ></i>\
                                                                        </div>\
                                                                        <div class="col-md-4">\
                                                                            <input class="form-control" align="center" style="text-align: center;vertical-align: middle;" id="Cparking'+CommercialLayoutCount+'" name="layout['+"Cparking"+'][]"  value="0"/>\
                                                                        </div>\
                                                                        <div class="col-md-1" style="cursor: pointer;" onclick="addOpenParking('+CommercialLayoutCount+')">\
                                                                           <i style="margin-top: 12px;" class="fa fa-plus"></i>\
                                                                        </div>\
                                                                    </div>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+CommercialLayoutCount+'\','+CommercialLayoutCount+')" id="RemoveImage_'+CommercialLayoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+CommercialLayoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" accept="image/x-png,image/jpeg,image/jpg"id="layout_image'+CommercialLayoutCount+'" onchange="layout_preview(this,'+CommercialLayoutCount+')" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();" id="layout_preview_display_'+CommercialLayoutCount+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div> \
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" class="form-control m-input" name="layout[Expected][]" required id="Expected_'+CommercialLayoutCount+'" maxlength="10" oninput="GetProcePerUnit('+CommercialLayoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice_'+CommercialLayoutCount+'">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" readonly class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+CommercialLayoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" value="0"  class="form-control m-input" name="layout[BookingPrize][]"maxlength="10" oninput="convertNumbertoWords(\'Booking'+CommercialLayoutCount+'\',\'bnumToWords'+CommercialLayoutCount+'\');"  id="Booking'+CommercialLayoutCount+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+CommercialLayoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+CommercialLayoutCount+'" value="">\
                                                            <input name="layout[priceDisplayed][]" id="priceDisplayed_'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+CommercialLayoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                        </div>\
                                    </div>\
                                ');

        CommercialCount = CommercialLayoutCount;
        $('#UnitID_'+CommercialLayoutCount).select2();
        $('#bArea_'+CommercialLayoutCount).select2();
        $('#cArea_'+CommercialLayoutCount).select2();
        $('#washroom_Office_'+CommercialLayoutCount).select2();
        $('#balcony_Office_'+CommercialLayoutCount).select2();
        $('#furnished_office_'+CommercialLayoutCount).select2();
        $('#pentry_Office_'+CommercialLayoutCount).select2();

         // ChangeUnitePriceName(CommercialLayoutCount);
        
        AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+CommercialLayoutCount);
        AllcheckBoxArray('priceDisplayed','priceDisplay_'+CommercialLayoutCount);

        getLayoutUnitsDetails(CommercialCount);

        layoutImages.push('layout_image'+CommercialLayoutCount);

        $('#S_Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');
        $('#Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');

        CommercialLayoutCount = CommercialLayoutCount + 1;
        layoutCountHidden = layoutCountHidden + 1;

        var subtype = $('#propertiesSubType').val();
        var subTypeName = $('#subTypeLabel_'+subtype).text();

        SetWashroomValue(CommercialLayoutCount);

        if (subTypeName == "Corporate House") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Industrial Factory") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Warehouse") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }

    }
    function addOtherslayout(id) 
    {   
        $('#layoutCount').val(layoutCountHidden + 1);
        $('#layoutDetails_'+id).append('\<div id="layoutDetails_'+CommercialLayoutCount+'">\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+CommercialLayoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+CommercialLayoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                <span class="m-accordion__item-icon" onclick="removeDivTag('+CommercialLayoutCount+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 17px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+CommercialLayoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-parent="#m_accordion_4_'+CommercialLayoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+CommercialLayoutCount+'" onchange="ChangeUnitePriceName('+CommercialLayoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+CommercialLayoutCount+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" value="0"onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text"  onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" required id="carpetArea_'+CommercialLayoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="col-lg-4">\
                                                        <label>Select BHK</label>\
                                                        <select style="width: 100%;" name="layout['+"layoutPropBHK"+'][]" class="form-control m-select2" id="layoutPropBHK'+CommercialLayoutCount+'">\
                                                            <option value="1">1 </option>\
                                                            <option value="2">2 </option>\
                                                            <option value="3">3 </option>\
                                                            <option value="3+">3+ </option>\
                                                        </select>\
                                                    </div>\
                                                    <div class="col-lg-4">\
                                                        <label>Select Facing</label>\
                                                        <select style="width: 100%;" name="layout['+"facingID"+'][]" class="form-control m-select2" id="facingID'+CommercialLayoutCount+'">\
                                                             <option value="1">1 </option>\
                                                            <option value="2">2 </option>\
                                                            <option value="3">3 </option>\
                                                            <option value="4">4 </option>\
                                                        </select>\
                                                    </div>\
                                                    <div class="col-lg-6">\
                                                       <label>Total Floors</label>\
                                                        <div class="col-lg-6">\
                                                           <label>Total Floors</label>\
                                                           <input type="text" onkeypress="return isNumberKey(event);" placeholder="Total Floors" value="0" class="form-control m-input" style="width: 100%;" class="form-control m-select2" name="layout['+"tFloor"+'][]"  max="100" id="tFloor'+CommercialLayoutCount+'" oninput="getPFlor('+CommercialLayoutCount+');">\
                                                        </div>\
                                                    </div>\
                                                    <div class="col-lg-6" id="Layout_Hidden">\
                                                        <label>Property On Floor</label>\
                                                        <select style="width: 100%;" class="form-control m-select2" name="layout['+"pFloor"+'][]" id="pFloor'+CommercialLayoutCount+'" style="display: block;">\
                                                            <option value="0">Ground Floor</option>\
                                                        </select>\
                                                    </div>\
                                                    <div class="col-lg-4">\
                                                        <br>\
                                                        <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                        <p style="width: 100%;">Store Room</p>\
                                                        <input type="hidden" name="layout['+"storeRoomAvailable"+'][0]" id="storeRoomAvailable'+CommercialLayoutCount+'" value="">\
                                                        <input  name="layout['+"storeRoomAvailable"+'][]" id="Room'+CommercialLayoutCount+'" class="StoreRoom" onchange=AllcheckBoxArray("StoreRoom","storeRoomAvailable'+CommercialLayoutCount+'")  type="checkbox">\
                                                        <span></span>\
                                                        </label>\
                                                    </div>\
                                                    <div class="col-lg-4">\
                                                        <br>\
                                                        <label style="width: 100%;" class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand">\
                                                        <input type="hidden" name="layout['+"poojaRoomAvailable"+'][0]" id="poojaRoomAvailable'+CommercialLayoutCount+'" value="">\
                                                        <p style="width: 100%;">Pooja Room</p>\
                                                        <input  name="layout['+"poojaRoomAvailable"+'][]" id="Room'+CommercialLayoutCount+'" class="PoojaRoom" onchange=AllcheckBoxArray("PoojaRoom","poojaRoomAvailable'+CommercialLayoutCount+'") type="checkbox">\
                                                        <span></span>\
                                                        </label>\
                                                    </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" class="form-control m-input" name="layout[Expected][]" required id="Expected_'+CommercialLayoutCount+'" maxlength="10" oninput="GetProcePerUnit('+CommercialLayoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice_'+CommercialLayoutCount+'">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+CommercialLayoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" value="0" class="form-control m-input" name="layout[BookingPrize][]" maxlength="10" oninput="convertNumbertoWords(\'Booking'+CommercialLayoutCount+'\',\'bnumToWords'+CommercialLayoutCount+'\');" id="Booking'+CommercialLayoutCount+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+CommercialLayoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[priceDisplayed][]" id="priceDisplayed_'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+CommercialLayoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\</div>\
                                ');

        CommercialCount = CommercialLayoutCount;
        $('#UnitID_'+CommercialLayoutCount).select2();
        $('#bArea_'+CommercialLayoutCount).select2();
        $('#cArea_'+CommercialLayoutCount).select2();
        $('#washroom_Office_'+CommercialLayoutCount).select2();
        $('#balcony_Office_'+CommercialLayoutCount).select2();
        $('#furnished_office'+CommercialLayoutCount).select2();
        $('#pentry_Office'+CommercialLayoutCount).select2();

        // ChangeUnitePriceName(CommercialLayoutCount);
        AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+CommercialLayoutCount);
        AllcheckBoxArray('priceDisplayed','priceDisplay_'+CommercialLayoutCount);
        AllcheckBoxArray('StoreRoom','storeRoomAvailable'+CommercialLayoutCount);
        AllcheckBoxArray('PoojaRoom','poojaRoomAvailable'+CommercialLayoutCount);
        getLayoutUnitsDetails(CommercialCount);

        $('#S_Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');
        $('#Build_Area_Hidden_'+CommercialLayoutCount).css('display','block');

        CommercialLayoutCount = CommercialLayoutCount + 1;
        layoutCountHidden = layoutCountHidden + 1; 

    }

    function addLandlayout(id) 
    {
        $('#layoutCount').val(layoutCountHidden + 1);
        $('#layoutDetails_'+id).append('\
                                    <div class="m-accordion m-accordion" id="m_accordion_4_'+CommercialLayoutCount+'" style="margin-bottom: 20px;">\
                                        <div class="m-accordion__item show">\
                                            <div class="m-accordion__item-head " role="tab" id="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-toggle="collapse" href="#m_accordion_3_item_7_body'+CommercialLayoutCount+'" style="background-color: #f2f2f2;;" aria-expanded="false">\
                                                <span class="m-accordion__item-icon">\
                                                    <i class="la la-angle-down" style="font-size: 17px;"></i>\
                                                </span>\
                                                <span class="m-accordion__item" >Add Layout Details</span>\
                                                 <span class="m-accordion__item-icon" onclick="removeDivTag('+CommercialLayoutCount+')" >\
                                                    <i class="fa fa-times" style="float: right;font-size: 17px;"></i>\
                                                </span>\
                                            </div>\
                                            <div class="m-accordion__item-body collapse" style="background-color: #fcfcfc;" id="m_accordion_3_item_7_body'+CommercialLayoutCount+'" class=" " role="tabpanel" aria-labelledby="m_accordion_3_item_7_head'+CommercialLayoutCount+'" data-parent="#m_accordion_4_'+CommercialLayoutCount+'">\
                                                <div class="m-accordion__item-content">\
                                                    <div class="form-group row" id="S_Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-6">\
                                                                    <label>Area Unit</label>\
                                                                    <br>\
                                                                    <select style="width: 100%;" name="layout['+"UnitID"+'][]" id="UnitID_'+CommercialLayoutCount+'" onchange="ChangeUnitePriceName('+CommercialLayoutCount+')" class="form-control m-select2">\
                                                                        <option  >-- Select --</option>\
                                                                        <option>SQ.fit</option>\
                                                                        <option>SQ.Yards</option>\
                                                                        <option>SQ.Meter</option>\
                                                                        <option>Cents</option>\
                                                                        <option>Ares</option>\
                                                                    </select>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="Build_Area_Hidden_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="row">\
                                                                <div class="col-lg-4">\
                                                                    <label >Super Built Up Area</label>\
                                                                    <input type="text" value="0"  onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area" class="form-control m-input" id="SuperBuildUpArea_'+CommercialLayoutCount+'"  maxlength="6" name="layout['+"SuperBuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Built Up Area"   maxlength="6"class="form-control m-input" name="layout['+"BuildUpArea"+'][]">\
                                                                </div>\
                                                                <div class="col-lg-4">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text"  onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="carpetArea_'+CommercialLayoutCount+'" name="layout['+"carpetArea"+'][]">\
                                                                </div>\
                                                            </div> \
                                                        </div>\
                                                    </div>\
                                                    <div class="form-group row" id="villa_Area_details_'+CommercialLayoutCount+'" style="display: none;">\
                                                        <div class="col-lg-12">\
                                                            <div class="col-lg-3">\
                                                                <label>Plot Area : </label>\
                                                            </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Super Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"   maxlength="6" id="ploatSuperBuildUpArea_'+CommercialLayoutCount+'" class="form-control m-input" name="layout[ploatSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="ploatBuildUpArea_'+CommercialLayoutCount+'"  maxlength="6" name="layout[ploatBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="ploatcarpetArea_'+CommercialLayoutCount+'" name="layout[ploatcarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-lg-12">\
                                                                <div class="col-lg-3">\
                                                                    <label>Construction Area : </label>\
                                                                </div>\
                                                            <div class="row">\
                                                                <div class="col-lg-3">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>SuperBuilt Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Super Built Up Area"  id="constructionSuperBuildUpArea_'+CommercialLayoutCount+'" maxlength="6"class="form-control m-input" name="layout[constructionSuperBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label id="Ploat_Area">Built Up Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Built Up Area" class="form-control m-input" id="constructionBuildUpArea_'+CommercialLayoutCount+'""  maxlength="6" name="layout[constructionBuildUpArea][]">\
                                                                </div>\
                                                                <div class="col-lg-3">\
                                                                    <label>Carpet Area</label>\
                                                                    <input type="text" onkeypress="return isNumberKey(event);" placeholder="Carpet Area"  maxlength="6" class="form-control m-input" id="constructioncarpetArea_'+CommercialLayoutCount+'"" name="layout[constructioncarpetArea][]">\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                      <div class="col-lg-4">\
                                                            <div class="form-group row">\
                                                                <div class="col-lg-12">\
                                                                   <label>Layout Images</label>\
                                                                    <span class="">\
                                                                        <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">\
                                                                            <div class="row" style="float: right; padding-right: 20px; padding-top: -10px;">\
                                                                                <a onclick="RemoveImage(\'layout_image'+CommercialLayoutCount+'\','+CommercialLayoutCount+')" id="RemoveImage_'+CommercialLayoutCount+'" style=" display: none; cursor: pointer;display: none; padding: 0px !important;">\
                                                                                    &nbsp;&nbsp;&nbsp;<i style="padding: 2px;font-size: 15px;" class="fa fa-times">&nbsp;&nbsp;&nbsp;</i>\
                                                                                </a>\
                                                                                <a id="EditImage_'+CommercialLayoutCount+'" style="display: none;cursor: pointer;" onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();">\
                                                                                    <i style=" padding: 2px; font-size: 12px;" class="fa fa-pencil-alt"></i>\
                                                                                </a>\
                                                                           </div>\
                                                                            <input style="display:none;" type="file" name="layout['+"layout_image"+'][]" accept="image/x-png,image/jpeg,image/jpg"id="layout_image'+CommercialLayoutCount+'" onchange="layout_preview(this,'+CommercialLayoutCount+')" />\
                                                                            <div align="center"  onclick="document.getElementById(\'layout_image'+CommercialLayoutCount+'\').click();" id="layout_preview_display_'+CommercialLayoutCount+'">\
                                                                                <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>\
                                                                            </div>\
                                                                        </div>\
                                                                    </span>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    <div class="form-group row">\
                                                        <div class="col-lg-4">\
                                                            <label>Expected Price</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" class="form-control m-input" name="layout[Expected][]" required id="Expected_'+CommercialLayoutCount+'" maxlength="10" oninput="GetProcePerUnit('+CommercialLayoutCount+')" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="numToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label id="perUnitPrice_'+CommercialLayoutCount+'">Price Per Unit</label>\
                                                            <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                               <input type="text" value="0" onkeypress="return isNumberKey(event);" placeholder="Price Per Unit" class="form-control m-input" name="layout[PricePerUnit][]" id="PricePerUnit_'+CommercialLayoutCount+'">\
                                                            </div>\
                                                            <span id="pnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4">\
                                                            <label>Booking Price</label>\
                                                             <div class="input-group">\
                                                                <div class="input-group-append">\
                                                                    <span class="input-group-text">\
                                                                        <i style="font-size: 15px;" class="la la-rupee"></i>\
                                                                    </span>\
                                                                </div>\
                                                                <input type="text" value="0" class="form-control m-input" name="layout[BookingPrize][]" maxlength="10" oninput="convertNumbertoWords(\'Booking'+CommercialLayoutCount+'\',\'bnumToWords'+CommercialLayoutCount+'\');" id="Booking'+CommercialLayoutCount+'" onkeypress="return isNumberKey(event);">\
                                                            </div>\
                                                            <span id="bnumToWords'+CommercialLayoutCount+'"></span>\
                                                        </div>\
                                                        <div class="col-lg-4" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Negotiable ?\
                                                            <input type="hidden" name="layout[ExpectedNegociable][0]" id="PriceNegociable_'+CommercialLayoutCount+'" value="">\
                                                            <input  name="layout[ExpectedNegociable][]" id="ExpectedNegociable'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("ExpectedNegociable","PriceNegociable_'+CommercialLayoutCount+'") class="ExpectedNegociable"   type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                        <div class="col-lg-8" style="padding-top: 5%;">\
                                                            <label class="m-checkbox m-checkbox--solid m-checkbox--brand">\
                                                            Don`t Display Price\
                                                            <input type="hidden" name="layout[priceDisplayed][0]" id="priceDisplay_'+CommercialLayoutCount+'" value="">\
                                                            <input name="layout[priceDisplayed][]" id="priceDisplayed_'+CommercialLayoutCount+'" onchange=AllcheckBoxArray("priceDisplayed","priceDisplay_'+CommercialLayoutCount+'") class="priceDisplayed"  type="checkbox">\
                                                            <span></span>\
                                                            </label>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                ');

        CommercialCount = CommercialLayoutCount;

       
        $('#UnitID_'+CommercialLayoutCount).select2();
        $('#bArea_'+CommercialLayoutCount).select2();
        $('#cArea_'+CommercialLayoutCount).select2();
        layoutImages.push('layout_image'+CommercialLayoutCount);
        AllcheckBoxArray('ExpectedNegociable','PriceNegociable_'+CommercialLayoutCount);
        AllcheckBoxArray('priceDisplayed','priceDisplay_'+CommercialLayoutCount);
        // ChangeUnitePriceName(CommercialLayoutCount);
        
            

        getLayoutUnitsDetails(CommercialCount);
        var subtype = $('#propertiesSubType').val();
        var subTypeName = $('#subTypeLabel_'+subtype).text();
        $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
        $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');  
        if (subTypeName == "Corporate House") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Industrial Factory") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        else if (subTypeName == "Warehouse") 
        {
            $('#villa_Area_details_'+CommercialLayoutCount+'').css('display','block');   
            $('#S_Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','block');   
            $('#Build_Area_Hidden_'+CommercialLayoutCount+'').css('display','none');   
        }
        CommercialLayoutCount = CommercialLayoutCount + 1;
        layoutCountHidden = layoutCountHidden + 1; 

    }


    function removeDivTag(id) 
    {   
        layoutCountHidden = layoutCountHidden - 1;
        // alert(layoutCountHidden);
        $('#m_accordion_4_'+id).remove();

        $('#layoutCount').val(layoutCountHidden);
        // alert(layoutCountHidden - 1);
        var ids = "layout_image"+id;
        if($.inArray(ids,layoutImages) != -1)
        {
            var index = layoutImages.indexOf(ids);

            if(index!=-1)
            {
               layoutImages.splice(index, 1);
            }
        }
    }
    function getAreaDetails(id) 
    {
        var data = $('#UnitID_'+id+' option:selected').text();

        if ($('#UnitID_'+id).val() != "" && $('#UnitID_'+id).val() != null) {
            $('#bArea_'+id).html('<option value="'+$('#UnitID_'+id).val()+'" Selected>'+data+'</option>');
            $('#cArea_'+id).html('<option value="'+$('#UnitID_'+id).val()+'" Selected>'+data+'</option>');
        }
    }
     
    $('.Parkingyes').change(function(){
            // console.log(this.value);
            if (this.value === "yes") {

                $("#parking").css("display","block");
            }
            if (this.value === "no") {

                $("#parking").css("display","none");   
            }
        });

    function getPFlor(id) 
    {
        var count = $('#tFloor'+id).val();

        if (parseInt(count) <= 100) 
        {   
            $('#pFloor'+id).html('');
            $('#pFloor'+id).append('<option value="0">Ground Floor</option>');
            for (var i = 1; i <= count; i++) 
            {   
                $('#pFloor'+id).append('<option value='+i+'>'+i+'</option>');
            }
        }
        else
        {   
            $('#tFloor'+id).focus();
            
            $('#pFloor'+id).html('<option value="0">Ground Floor</option>');
        }
    }
    function SetWashroomValue(id = null) 
    {
        if (id == null) 
        {
            $('#washroom_Office').html('');
            $('#balcony_Office').html('');
            $('#pentry_Office').html('');

            for (var i = 0; i <= 20; i++) 
            {
              
                $('#washroom_Office').append('<option value='+i+'>'+i+'</option>');
                $('#balcony_Office').append('<option value='+i+'>'+i+'</option>');
                $('#pentry_Office').append('<option value='+i+'>'+i+'</option>');
            }
            $('#washroom_Office').append('<option value=21>20+</option>');
            $('#balcony_Office').append('<option value=21>20+</option>');
            $('#pentry_Office').append('<option value=21>20+</option>');


        }
        else
        {
            $('#washroom_Office'+id).html('');
            $('#balcony_Office'+id).html('');
            $('#pentry_Office'+id).html('');

            for (var i = 0; i <= 20; i++) 
            {

                $('#washroom_Office'+id).append('<option value='+i+'>'+i+'</option>');
                $('#balcony_Office'+id).append('<option value='+i+'>'+i+'</option>');
                $('#pentry_Office'+id).append('<option value='+i+'>'+i+'</option>');
            }
            $('#washroom_Office').append('<option value=21>20+</option>');
            $('#balcony_Office').append('<option value=21>20+</option>');
            $('#pentry_Office').append('<option value=21>20+</option>');
        }
    }
    function getLayoutFacing(id) 
    {

        // var id = $('#propList').val();
        // alert('#facingID'+id);

        $.ajax({
            url: '<?php echo Urls::$BASE ?>properties/getfacingDetails',
            type: 'POST',
            success: function (data) {
                // console.log(data);
               var facing = JSON.parse(data);

                //console.log(country.length);
                $('#facingID'+id).html('');
               

                for(var i = 0; i < facing.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nFacingIDPK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == facing[i].nFacingIDPK)
                    { 
                        $('#facingID'+id).append('<option value='+facing[i].nFacingIDPK+' selected>'+facing[i].tFacingName+'</option>');
                        

                    }
                    else{

                        $('#facingID'+id).append('<option value='+facing[i].nFacingIDPK+'>'+facing[i].tFacingName+'</option>');
                       
                    }
                }
                $("#facingID").trigger("change");
            }
        });
    }
    $(document).on('change','.Parkingyes2', function()
    {
            alert('.Parkingyes'+layoutCount);
       if (this.value === "yes") {
            $('#parking'+k).css("display","block");
        }
        if (this.value === "no") {

            $('#parking'+k).css("display","none");   
        }
    });
    
    $(document).on('change','#Parkingyes_House',function()
    {
        if (this.value === "yes") {
            // alert(k);
            $('#parking_House').css("display","block");
        }
        if (this.value === "no") {

            $('#parking_House').css("display","none");   
        }

    });

    // $(document).on('input','#possessionDate',function () {

    //     var str = $(this).val();
    //     // alert(str.length);
    //     if(str.length == 2)
    //     {
    //         str = str + '-';
    //     }
    //     if(str.length == 5)
    //     {
    //         str = str + '-';
    //     }

    //     $(this).val(str);
    //     // body...
    // });
    function getBHKDetails() 
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getBHKDetails',
            type: 'POST',
            success: function(data) 
            {
                $('#propBHK').empty();
               
              
                $('#propBHK_Other').empty();
                
                $('#layoutPropBHK').empty();
                
                var country = JSON.parse(data);  

                //console.log(country.length);

                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nBHKIDPK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nBHKIDPK){

                        $('#propBHK').append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                       
                        $('#propBHK_Other').append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                        $('#layoutPropBHK').append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                        
                    }
                    else{

                        $('#propBHK').append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                      
                        $('#propBHK_Other').append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                        $('#layoutPropBHK').append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                        
                    
                    }
                }
            }
        });
    }
    function getLayoutBHKDetails(id) 
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getBHKDetails',
            type: 'POST',
            success: function(data) 
            {
                $('#layoutPropBHK'+id).empty();
               

                var country = JSON.parse(data);  

                //console.log(country.length);

                for(var i = 0; i < country.length; i++)
                {
                    var data = "<?php if(isset($this->data)) {echo $this->data[0]['nBHKIDPK']; } else { echo NULL; } ?>";
                    //console.log(data);
                    if(data == country[i].nBHKIDPK){

                        $('#layoutPropBHK'+id).append('<option value='+country[i].nBHKIDPK+' selected>'+country[i].tBHKName+'</option>');
                        
                    }
                    else{

                        $('#layoutPropBHK'+id).append('<option value='+country[i].nBHKIDPK+'>'+country[i].tBHKName+'</option>');
                      
                    
                    }
                }
                $("#layoutPropBHK"+id).trigger("change");
            }
        });
    }
    function getCountryList()   // fetch all country list insert / update
    {   
       
        var id =  $('#countryID').val();
        // var country = $('#LocalityID').val();
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function (data) {
                $('#select2Country').empty();
                $('#select2Country').append('<option value="">-- Select Country --</option>');
                var country = JSON.parse(data);
                // console.log(country);
                if (country != null) 
                {
                    for (var i = 0; i < country.length; i++) 
                    {
                        var data1 = id;

                        if (data1 == country[i].nCountryIDPK)
                        {
                            $('#select2Country').append('<option value=' + country[i].nCountryIDPK + ' selected>' + country[i].tCountryName + '</option>');
                        }
                        else
                        {
                            $('#select2Country').append('<option value=' + country[i].nCountryIDPK + '>' + country[i].tCountryName + '</option>');
                        }
                    }
                }
                $("#select2Country").trigger("change");
            }
        }).done(function () {   
            // getStateList();
        });
    }
    function getStateList() // fetch state list on the basis of the country id
    {
        var countryID = $('#select2Country').val();

        var id =  $('#stateID').val();

        // console.log("State ID :   "+id);

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getState',
            type: 'POST',
            data: {countryID: countryID},
            success: function (data) {
                // console.log(data);
                $('#select2State').empty();
                $('#select2State').append('<option value="">-- Select State --</option>');
                var state = JSON.parse(data);

                if (state != null) 
                {
                    for (var i = 0; i < state.length; i++) {

                        var data2 = $('#stateID').val();

                        if (data2 == state[i].nStateIDPK)
                        {
                            $('#select2State').append('<option value=' + state[i].nStateIDPK + ' selected>' + state[i].tStateName + '</option>');
                        }
                        else
                        {
                            $('#select2State').append('<option value=' + state[i].nStateIDPK + '>' + state[i].tStateName + '</option>');
                        }
                    }
                }

                $("#select2State").trigger("change");
            }

        }).done(function () {
             // getCityList();
        });
    }
    function getCityList() // fetch state list on the basis of the country id
    {
        var stateID = $('#select2State').val();
        // console.log("State : "+id);
      
        var id =  $('#cityID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getCity',
            type: 'POST',
            data: {stateID: stateID},
            success: function (data) {
                // console.log(data);
                $('#select2City').empty();
                $('#select2City').append('<option value="">-- Select City --</option>');
                var city = JSON.parse(data);
                if (city != null) 
                {
                    for (var i = 0; i < city.length; i++) {

                        var data2 = id;

                        if (data2 == city[i].nCityIDPK)
                        {
                            $('#select2City').append('<option value=' + city[i].nCityIDPK + ' selected>' + city[i].tCityName + '</option>');
                        }
                        else
                        {
                            $('#select2City').append('<option value=' + city[i].nCityIDPK + '>' + city[i].tCityName + '</option>');
                        }
                    }
                }
                 $("#select2City").trigger("change");
            }           


        }).done(function () {
            // getAreaList();
          
        });
    }
    function getAreaList() // fetch state list on the basis of the country id
    {
        var cityID = $('#select2City').val();

        var id =  $('#areaID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getArea',
            type: 'POST',
            data: {cityID: cityID},
            success: function (data) {
                // console.log(data);
                $('#select2SArea').empty();
                $('#select2SArea').append('<option value="">-- Select Area --</option>');
                var city = JSON.parse(data);
                if (city != null) 
                {
                    for (var i = 0; i < city.length; i++) {

                        var data2 = id;

                        if (data2 == city[i].nAreaIDPK)
                        {
                            $('#select2SArea').append('<option value=' + city[i].nAreaIDPK + ' selected>' + city[i].tAreaName + '</option>');
                        }
                        else
                        {
                            $('#select2SArea').append('<option value=' + city[i].nAreaIDPK + '>' + city[i].tAreaName + '</option>');
                        }
                    }
                }
                $("#select2SArea").trigger("change");
            }

        }).done(function () {
            // getlocality();
        });
    }
    function getlocality() // fetch state list on the basis of the country id
    {
        var areaID = $('#select2SArea').val();
        var id = $('#LocalityID').val();
        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getAllLocalities',
            type: 'POST',
            data: {areaID: areaID},
            success: function (data) {
                // console.log(data);
                $('#localityName').empty();
                $('#localityName').append('<option value="">-- Select Locality --</option>');

                var locality = JSON.parse(data);
                // console.log(locality.length);

                if (locality != null) 
                {
                    // console.log(locality[0].nLocalityIDPK);
                    for (var i = 0; i < locality.length; i++) 
                    {

                        var data2 = id;

                        if (data2 == locality[i].nLocalityIDPK)
                        {
                            $('#localityName').append('<option value=' + locality[i].nLocalityIDPK + ' selected>' + locality[i].tLocalityName + '</option>');
                        }
                        else
                        {
                            $('#localityName').append('<option value=' + locality[i].nLocalityIDPK + '>' + locality[i].tLocalityName + '</option>');
                        }
                    }
                }
                // $("#localityName").trigger("change");   
            }

        }).done(function () {

        });
    }

    // function getLocations(localityid = null)
    // {
    //     $.ajax({
    //         url: '<?php echo Urls::$BASE; ?>properties/getLocations',
    //         type: 'POST',
    //         success: function(data) 
    //         {
    //             $('#locationID').empty();
               

    //             var locations = JSON.parse(data);

    //             //console.log(country.length);

    //             for(var i = 0; i < locations.length; i++)
    //             {
                    
    //                 //console.log(data);
    //                 if(localityid == locations[i].nLocalityIDPK){

    //                     $('#locationID').append('<option Selected value='+locations[i].nAreaIDPK+' data-areaID='+locations[i].nAreaIDPK+' data-cityid='+locations[i].nCityIDFK+' data-stateid='+locations[i].nStateIDPK+' data-countryid='+locations[i].nCountryIDFK+' data-localityid='+locations[i].nLocalityIDPK+' data-latitude='+locations[i].dAreaLatitude+' data-longitude='+locations[i].dAreaLongitude+'>'+locations[i].tLocalityName+' , '+locations[i].tAreaName+' , '+locations[i].tCityName+' - '+locations[i].tStateName+'</option>');
    //                 }
    //                 else{
    //                     $('#locationID').append('<option value='+locations[i].nAreaIDPK+' data-areaID='+locations[i].nAreaIDPK+' data-cityid='+locations[i].nCityIDFK+' data-stateid='+locations[i].nStateIDPK+' data-countryid='+locations[i].nCountryIDFK+' data-localityid='+locations[i].nLocalityIDPK+' data-latitude='+locations[i].dAreaLatitude+' data-longitude='+locations[i].dAreaLongitude+'>'+locations[i].tLocalityName+' , '+locations[i].tAreaName+' , '+locations[i].tCityName+' - '+locations[i].tStateName+'</option>');
    //                 }
    //             }
    //             $("#locationID").trigger("change");
    //         }
    //     });
    // }

    // $(document).on('change','#locationID',function () {
    //     // body...
    //     // $(this).val('cityID');
    //     var selectedCityID = $(this).find('option:selected').data('cityid');
    //     var selectedAreaID = $(this).find('option:selected').data('areaID');

    //     var selectedlocalityID = $(this).find('option:selected').data('localityid');
    //     var selectedStateID = $(this).find('option:selected').data('stateid');
    //     var selectedCountryID = $(this).find('option:selected').data('countryid');
    //     var latitude = $(this).find('option:selected').data('latitude');
    //     var longitude = $(this).find('option:selected').data('longitude');
    //     $('#LocalityID').val(selectedlocalityID);
    //     $('#areaID').val(selectedAreaID);
    //     $('#cityID').val(selectedCityID);
    //     $('#stateID').val(selectedStateID);
    //     $('#countryID').val(selectedCountryID);
    //     $('#pLatitutde').val(latitude);
    //     $('#pLongitude').val(longitude);

    // });

   
    function removeOpenParking(id) 
    {
        var value = $('#Cparking'+id).val();

        if (parseInt(value) != 0 ) 
        {
            value = parseInt(value) - 1;    
        }

        $('#Cparking'+id).val(value);
    }
    
    function addOpenParking(id) 
    {        
        var value = $('#Cparking'+id).val();

        if (value < 50) {

            if (parseInt(value) != -1 ) 
            {
                value = parseInt(value) + 1;    
            }
            $('#Cparking'+id).val(value);
        }


    }
    
    function getPropertyLocation(PropertyID) 
    {
       $.ajax({
            url: '<?php echo Urls::$BASE; ?>properties/getPropertyLocation',
            type: 'POST',
            data: {PropertyID:PropertyID},
            success: function(data) 
            {   
                var locationInfo = JSON.parse(data);
                
                $('#streetName').val(locationInfo[0].tPropertyStreetName); 
                $('#pAddress').val(locationInfo[0].tPropertyAddress);
                $('#mapLocationLink').val(locationInfo[0].tPropertyMapLocationLink);
                // getLocations(locationInfo[0].nLocalityIDFK);
             
            }
        });

    }
    var image = Array();
    var files =Array();
    var deletedFiles = Array();

    var count = 0;
    var numFiles = 0;
    var data_image   = new FormData();

    function property_image_preview(vpb_selector_)
    {   

        numFiles = numFiles + $('#property_logo')[0].files.length;

        files.push($('#property_logo')[0].files[0]);
        data_image.append('files[]',$('#property_logo')[0].files[0]);

        // console.log(files);

        if(numFiles <= 25)
        {
            $('#imgCount').text('Images Selected : ( '+numFiles+' / 25 )');
            $('#property_display_preview').css('display','block');
            var id = 1, last_id = last_cid = '';
            $.each(vpb_selector_.files, function(vpb_o_, file)
            {
                if (file.name.length>0) 
                {
                    if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                    else
                    {
                       var reader = new FileReader();
                       
                       reader.onload = function(e) 
                       {    
                            if (numFiles == 1) 
                            {
                               $('#property_display_preview').append(
                               '<div id="selector_'+count+'" class="vpb_wrapper">\
                                    <span onclick="deletePropertyImage('+count+')" class="tclose">X</span>\
                                    <img class="vpb_image_style" width="100px" height="100px" class="img-thumbnail" src="' + e.target.result + '"\
                                        title="'+escape(file.name) +'" />\
                                        <label>Cover Image</label>\
                                        <label class="m-radio m-radio--state-success " style="padding: 10px 10px 0px;">\
                                            <input type="radio" checked name="messageCheckbox" id="messageCheckbox" class="messageCheckbox">\
                                           <span></span>\
                                        </label>\
                                </div>');
                               checkBoxArray();

                            }
                            else{
                                $('#property_display_preview').append(
                               '<div  id="selector_'+count+'" class="vpb_wrapper">\
                                    <span onclick="deletePropertyImage('+count+')" class="tclose">X</span>\
                                    <img class="vpb_image_style" width="100px" height="100px" class="img-thumbnail" src="' + e.target.result + '"\
                                        title="'+escape(file.name) +'" />\
                                        <label >Cover Image</label>\
                                        <br>\
                                        <label class="m-radio m-radio--state-success" style="padding: 5px 5px 0px;">\
                                            <input type="radio" name="messageCheckbox" id="messageCheckbox" class="messageCheckbox">\
                                           <span></span>\
                                        </label>\
                                </div>');
                            checkBoxArray();
                            }
                       }
                       reader.readAsDataURL(file);
                   }
                }
                else {  return false; }
            });
        }
        else
        {
            swal( {
                title: "", text: "Exceeds the image limits.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
            })
        }
        count = count + 1;

    }
     $(document).on('change','.messageCheckbox',function(){
        checkBoxArray();
    });

    function checkBoxArray() {
        
        var checkedValue = Array(); 

        var inputElements = document.getElementsByClassName('messageCheckbox');
        // console.log(inputElements);
        for(var i=0; inputElements[i]; ++i)
        {
              if(inputElements[i].checked)
              {                   
                checkedValue.push(1);   
              }
              else
              {
                checkedValue.push(0);
              }
        }
        // console.log(checkedValue);
        $('#coverImage').val(checkedValue);
    }

    // $(document).on('change','.StoreRoom',function(){
    //     AllcheckBoxArray("StoreRoom");
    // });
    // $(document).on('change','.PoojaRoom',function(){
    //     AllcheckBoxArray("PoojaRoom");
    // });
    // $(document).on('change','.StoreRoom_other',function(){
    //     AllcheckBoxArray("StoreRoom_other");
    // });
    // $(document).on('change','.PoojaRoom_other',function(){
    //     AllcheckBoxArray("PoojaRoom_other");
    // });
    // $(document).on('change','.StoreRoom',function(){
    //     AllcheckBoxArray("StoreRoom");
    // });
    // $(document).on('change','.StoreRoom',function(){
    //     AllcheckBoxArray("StoreRoom");
    // });


    function AllcheckBoxArray(className,id) {
        
        var checkedValue = Array(); 

        var inputElements = document.getElementsByClassName(className);

        // console.log(inputElements);

        for(var i=0; inputElements[i]; ++i)
        {
              if(inputElements[i].checked)
              {  
                // console.log("checked");
                checkedValue.push(1);   
              }
              else
              {
                // console.log("unChecked"); 
                checkedValue.push(0);
              }
        }

        $('#'+id).val(checkedValue);

        // console.log($('#'+id).val());
    }   

    function RemoveImage(id,count) 
    {
        $('#'+id).val('');
        $('#layout_preview_display_'+count).html('<i class="fa fa-camera-retro" style="font-size: 11.1rem !important;color: black;"></i>');
        $('#RemoveImage_'+count).css('display','none');
        $('#EditImage_'+count).css('display','none');

    }
    function deletePropertyImage(id) 
    {
        
        swal({
        title:"Are you sure?",
        text:"You want to delete this Image ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                deletedFiles.push(files[id - 1]);
                // console.log(deletedFiles);
                $('#selector_'+id).remove();

                data_image.append('DeletedFiles[]',files[id - 1]);
                numFiles = numFiles - 1;
                $('#imgCount').text('Images Selected : ( '+numFiles+' / 25 )');
            }   
       });
        
        
    }


    function layout_preview(vpb_selector_,count)
    {   
        // console.log(vpb_selector_);

        // layoutImages.push('layout_image'+count);
        // console.log(layoutImages);

        var numFiles = $('#layout_image'+count)[0].files.length;
        
            $('#layout_preview_display_'+count).html(''); 

            $('#RemoveImage_'+count).css('display','block');
            $('#EditImage_'+count).css('display','block');

            var id = 1, last_id = last_cid = '';
            $.each(vpb_selector_.files, function(vpb_o_, file)
            {
                if (file.name.length>0) 
                {
                    if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                    else
                    {
                        //Clear previous previewed files and start again
                       
                       var reader = new FileReader();
                       
                       reader.onload = function(e) 
                       {
                           $('#layout_preview_display_'+count).append(
                           '  <br><div class="col-lg-8">\
                           <img class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" title="'+ escape(file.name) +'" /><br /></div>  \
                           ');
                       }
                       reader.readAsDataURL(file);
                   }
                }
                else {  return false; }
            });
        
    }
    function convertNumbertoWords(id,wordId) 
    {   
        // alert($('#'+id).val());
        document.getElementById(wordId).innerHTML = price_in_words(document.getElementById(id).value);
    }
    function price_in_words(price) {
        var sglDigit = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"],
            dblDigit = ["Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"],
            tensPlace = ["", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"],
            handle_tens = function(dgt, prevDgt) {
                return 0 == dgt ? "" : " " + (1 == dgt ? dblDigit[prevDgt] : tensPlace[dgt])
            },
            handle_utlc = function(dgt, nxtDgt, denom) {
                return (0 != dgt && 1 != nxtDgt ? " " + sglDigit[dgt] : "") + (0 != nxtDgt || dgt > 0 ? " " + denom : "")
            };

            var str = "",
                digitIdx = 0,
                digit = 0,
                nxtDigit = 0,
                words = [];
            if (price += "", isNaN(parseInt(price))) str = "";
            else if (parseInt(price) > 0 && price.length <= 10) {
                for (digitIdx = price.length - 1; digitIdx >= 0; digitIdx--) switch (digit = price[digitIdx] - 0, nxtDigit = digitIdx > 0 ? price[digitIdx - 1] - 0 : 0, price.length - digitIdx - 1) {
                    case 0:
                        words.push(handle_utlc(digit, nxtDigit, ""));
                        break;
                    case 1:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 2:
                        words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] && 0 != price[digitIdx + 2] ? " and" : "") : "");
                        break;
                    case 3:
                        words.push(handle_utlc(digit, nxtDigit, "Thousand"));
                        break;
                    case 4:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 5:
                        words.push(handle_utlc(digit, nxtDigit, "Lakh"));
                        break;
                    case 6:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 7:
                        words.push(handle_utlc(digit, nxtDigit, "Crore"));
                        break;
                    case 8:
                        words.push(handle_tens(digit, price[digitIdx + 1]));
                        break;
                    case 9:
                        words.push(0 != digit ? " " + sglDigit[digit] + " Hundred" + (0 != price[digitIdx + 1] || 0 != price[digitIdx + 2] ? " and" : " Crore") : "")
                }
                str = words.reverse().join("")
            } else str = "";
            return str

    }

    // alert(price_in_words(1250000000));
    // function inWords (num) 
    // {
    //     var a = ['','One ','Two ','Three ','Four ', 'Five ','Six ','Seven ','Eight ','Nine ','Ten ','Eleven ','Twelve ','Thirteen ','Fourteen ','Fifteen ','Sixteen ','Seventeen ','Eighteen ','Nineteen '];
    //     var b = ['', '', 'Twenty','Thirty','Forty','Fifty', 'Sixty','Seventy','Eighty','Ninety'];
        
    //     if ((num = num.toString()).length > 10) return 'Out of bound';
    //     n = ('00000000000' + num).substr(-10).match(/^(\d{1})(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    //     if (!n) return; var str = '';
    //     str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'arab ' : '';
    //     str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Crore ' : '';
    //     str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Lakh ' : '';
    //     str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Thousand ' : '';
    //     str += (n[5] != 0) ? (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'Hundred ' : '';
    //     str += (n[6] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[6])] || b[n[6][0]] + ' ' + a[n[6][1]]) + 'only ' : '';

    //     return str;
    // }

    function remove_listing_image(id)
    {
        // console.log(id);
        swal({
        title:"Are you sure?",
        text:"You want to delete this Image ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE; ?>properties/delete_listing_image",
                    data:{'id':id},
                    success: function(data)
                    {    
                        if(data == "true")
                        {
                            $('#hide_place_Img'+id).fadeOut();
                            swal({
                                type:"success",
                                title:"Image deleted successully",
                                showConfirmButton:!1,timer:1500
                            });
                        }               
                    }
                }); 
            }   
       }); 
    }
 
    function isNumberKey(evt)
    {
        if (evt.which != 8 && evt.which != 0 && (evt.which < 48 || evt.which > 57)) 
        {
            return false;
        }
    }
   
    var WizardDemo=function() 
    {
        $("#m_wizard");
        var e,
        r,
        i=$("#m_form");
        //  var postData = new FormData();
        //  $.each(i, function(i, val) {
        //     data_image.append(val.name, val.value);
        // });
         
       /* var data   = new FormData();
        
        // files.forEach(function(files, i) {

            data.append('files[]',files);*/
        
        // });

        return {
            init:function() {
                var n;

                $("#m_wizard"),
                i=$("#m_form"),
                (r=new mWizard("m_wizard", {
                    startStep: 1
                }
                )).on("beforeNext", function(r) {
                    !0!==e.form()&&r.stop()
                }
                ),
                r.on("change", function(e) {
                    mUtil.scrollTop()
                }
                ),
                r.on("change", function(e) {
                    1===e.getStep()
                }
                ),
                e=i.validate( {
                    ignore:":hidden", rules: {
                       
                        // propList: {
                        //     required: !0
                        // },
                        possessionDate:{
                            required:!0
                        },
                        // ,
                        //  propertiesSubType: {
                        //     required: !0,
                        // }
                        // , typeofmain: {
                        //     required: !0
                        // },
                        pAddress:{
                            required:!0
                        },
                        streetName:{
                            required:!0
                        },
                        localityName:{
                            required:!0
                        }, 
                        mapLocationLink:{
                            required:!0
                        }
                        , area: {
                            required: !0
                        }
                        , city: {
                            required: !0
                        }
                        , state: {
                            required: !0
                        }
                        , country: { 
                            required: !0
                        }
                        // , SuperBuildUpArea: {
                        //     required: !0, maxlength: 6,
                        // }

                        , pLatitutde: {
                            required: !0
                        }
                        , pLongitude: {
                            required: !0
                        }
                        // , carpetArea: {
                        //     required: !0, maxlength: 6
                        // }
                        // , cArea: {
                        //     required: !0
                        // }
                        // , Bedrooms: {
                        //     required: !0
                        // }
                        // , Bathrooms: {
                        //     required: !0
                        // }
                        // , Balconies: {
                        //     required: !0
                        // }
                        // , kitchen: {
                        //     required: !0
                        // }
                        // , tFloor: {
                        //     required: !0
                        // }
                        // , pFloor: {
                        //     required: !0
                        // }
                        // , Room: {
                        //     required: !0
                        // }
                        // , propertyDescription: {
                        //     required: !0
                        // }
                        // , Availability: {
                        //     required: !0
                        // }
                        // , possession: {
                        //     required: !0
                        // }
                        // , ownership: {
                        //     required: !0
                        // }
                        // , Expected: {
                        //     required: !0
                        // }
                        // , Booking: {
                        //     required: !0
                        // }
                        // , PricePerUnit: {
                        //     required: !0
                        // }

                    }
                    , messages: {
                        "account_communication[]": {
                            required: "You must select at least one communication option"
                        }
                        , accept: {
                            required: "You must accept the Terms and Conditions agreement!"
                        }
                    }
                    , invalidHandler:function(e, r) {
                        mUtil.scrollTop(250);
                        // mUtil.scrollTop(), swal( {
                        //     title: "", text: "There are some errors in your submission. Please correct them.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                        // }
                        // )
                    }
                    , submitHandler:function(e) 
                    {

                    }
                }
                ),
                (n=i.find('[data-wizard-action="submit"]')).on("click", function(r) 
                {

                     // data_image.append('data',$('#m_form').serializeArray());
                    var data_ajax = JSON.stringify(jQuery('#m_form').serializeArray());

                    // data_image.append('data',data_ajax);
                    data_image.append('brochurePath[]',$('#brochurePath')[0].files[0]);

                    // data_image.append('layout_image[]',$('#layout_image1')[0].files[0]);
                    for (var i = 0; i < layoutImages.length; i++) 
                    {   
                        if ($('#'+layoutImages[i])[0].files.length != 0) 
                        {
                            data_image.append('layout_image['+i+']',$('#'+layoutImages[i])[0].files[0]);
                            
                        }
                        else
                        {
                            data_image.append('layout_image['+i+']',"NULL");
                        }
                        // console.log($('#'+layoutImages[i])[0].files[0]);
                    }

                     $.each(JSON.parse(data_ajax), function(i, val) {
                        data_image.append(val.name, val.value);
                        // console.log(data_image);   
                    });
                     // console.log(data_i  mage);

                    r.preventDefault(), e.form()&&(mApp.progress(n), $.ajax( {url:'<?php echo Urls::$BASE; ?>properties/createproperty',
                        type: 'post',
                        data: data_image,
                        contentType: false,
                        processData: false,
                        success:function(res) 
                        {

                            if(res.trim() == "true"){

                                swal( {
                                    title: "", text: "Listing has been successfully added!", type: "success", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                                }
                                )

                                window.setTimeout(function(){
                                        // Move to a new location or you can do something else
                                        window.location.href = "<?php echo Urls::$BASE; ?>properties";

                                    }, 2000);
                            }
                            else
                            {
                               swal( {
                                    title: "", text: "Seems something wrong! Try again", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
                                }
                                ) 
                            }
                        }
                    }
                    ))
                }
                )
            }
        }
    }
    ();
</script>

<script type="text/javascript">
    //     Underscore.js 1.9.1
    //     http://underscorejs.org
    //     (c) 2009-2018 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
    //     Underscore may be freely distributed under the MIT license.

    (function() {

      // Baseline setup
      // --------------

      // Establish the root object, `window` (`self`) in the browser, `global`
      // on the server, or `this` in some virtual machines. We use `self`
      // instead of `window` for `WebWorker` support.
      var root = typeof self == 'object' && self.self === self && self ||
                typeof global == 'object' && global.global === global && global ||
                this ||
                {};

      // Save the previous value of the `_` variable.
      var previousUnderscore = root._;

      // Save bytes in the minified (but not gzipped) version:
      var ArrayProto = Array.prototype, ObjProto = Object.prototype;
      var SymbolProto = typeof Symbol !== 'undefined' ? Symbol.prototype : null;

      // Create quick reference variables for speed access to core prototypes.
      var push = ArrayProto.push,
          slice = ArrayProto.slice,
          toString = ObjProto.toString,
          hasOwnProperty = ObjProto.hasOwnProperty;

      // All **ECMAScript 5** native function implementations that we hope to use
      // are declared here.
      var nativeIsArray = Array.isArray,
          nativeKeys = Object.keys,
          nativeCreate = Object.create;

      // Naked function reference for surrogate-prototype-swapping.
      var Ctor = function(){};

      // Create a safe reference to the Underscore object for use below.
      var _ = function(obj) {
        if (obj instanceof _) return obj;
        if (!(this instanceof _)) return new _(obj);
        this._wrapped = obj;
      };

      // Export the Underscore object for **Node.js**, with
      // backwards-compatibility for their old module API. If we're in
      // the browser, add `_` as a global object.
      // (`nodeType` is checked to ensure that `module`
      // and `exports` are not HTML elements.)
      if (typeof exports != 'undefined' && !exports.nodeType) {
        if (typeof module != 'undefined' && !module.nodeType && module.exports) {
          exports = module.exports = _;
        }
        exports._ = _;
      } else {
        root._ = _;
      }

      // Current version.
      _.VERSION = '1.9.1';

      // Internal function that returns an efficient (for current engines) version
      // of the passed-in callback, to be repeatedly applied in other Underscore
      // functions.
      var optimizeCb = function(func, context, argCount) {
        if (context === void 0) return func;
        switch (argCount == null ? 3 : argCount) {
          case 1: return function(value) {
            return func.call(context, value);
          };
          // The 2-argument case is omitted because we’re not using it.
          case 3: return function(value, index, collection) {
            return func.call(context, value, index, collection);
          };
          case 4: return function(accumulator, value, index, collection) {
            return func.call(context, accumulator, value, index, collection);
          };
        }
        return function() {
          return func.apply(context, arguments);
        };
      };

      var builtinIteratee;

      // An internal function to generate callbacks that can be applied to each
      // element in a collection, returning the desired result — either `identity`,
      // an arbitrary callback, a property matcher, or a property accessor.
      var cb = function(value, context, argCount) {
        if (_.iteratee !== builtinIteratee) return _.iteratee(value, context);
        if (value == null) return _.identity;
        if (_.isFunction(value)) return optimizeCb(value, context, argCount);
        if (_.isObject(value) && !_.isArray(value)) return _.matcher(value);
        return _.property(value);
      };

      // External wrapper for our callback generator. Users may customize
      // `_.iteratee` if they want additional predicate/iteratee shorthand styles.
      // This abstraction hides the internal-only argCount argument.
      _.iteratee = builtinIteratee = function(value, context) {
        return cb(value, context, Infinity);
      };

      // Some functions take a variable number of arguments, or a few expected
      // arguments at the beginning and then a variable number of values to operate
      // on. This helper accumulates all remaining arguments past the function’s
      // argument length (or an explicit `startIndex`), into an array that becomes
      // the last argument. Similar to ES6’s "rest parameter".
      var restArguments = function(func, startIndex) {
        startIndex = startIndex == null ? func.length - 1 : +startIndex;
        return function() {
          var length = Math.max(arguments.length - startIndex, 0),
              rest = Array(length),
              index = 0;
          for (; index < length; index++) {
            rest[index] = arguments[index + startIndex];
          }
          switch (startIndex) {
            case 0: return func.call(this, rest);
            case 1: return func.call(this, arguments[0], rest);
            case 2: return func.call(this, arguments[0], arguments[1], rest);
          }
          var args = Array(startIndex + 1);
          for (index = 0; index < startIndex; index++) {
            args[index] = arguments[index];
          }
          args[startIndex] = rest;
          return func.apply(this, args);
        };
      };

      // An internal function for creating a new object that inherits from another.
      var baseCreate = function(prototype) {
        if (!_.isObject(prototype)) return {};
        if (nativeCreate) return nativeCreate(prototype);
        Ctor.prototype = prototype;
        var result = new Ctor;
        Ctor.prototype = null;
        return result;
      };

      var shallowProperty = function(key) {
        return function(obj) {
          return obj == null ? void 0 : obj[key];
        };
      };

      var has = function(obj, path) {
        return obj != null && hasOwnProperty.call(obj, path);
      }

      var deepGet = function(obj, path) {
        var length = path.length;
        for (var i = 0; i < length; i++) {
          if (obj == null) return void 0;
          obj = obj[path[i]];
        }
        return length ? obj : void 0;
      };

      // Helper for collection methods to determine whether a collection
      // should be iterated as an array or as an object.
      // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
      // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
      var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
      var getLength = shallowProperty('length');
      var isArrayLike = function(collection) {
        var length = getLength(collection);
        return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
      };

      // Collection Functions
      // --------------------

      // The cornerstone, an `each` implementation, aka `forEach`.
      // Handles raw objects in addition to array-likes. Treats all
      // sparse array-likes as if they were dense.
      _.each = _.forEach = function(obj, iteratee, context) {
        iteratee = optimizeCb(iteratee, context);
        var i, length;
        if (isArrayLike(obj)) {
          for (i = 0, length = obj.length; i < length; i++) {
            iteratee(obj[i], i, obj);
          }
        } else {
          var keys = _.keys(obj);
          for (i = 0, length = keys.length; i < length; i++) {
            iteratee(obj[keys[i]], keys[i], obj);
          }
        }
        return obj;
      };

      // Return the results of applying the iteratee to each element.
      _.map = _.collect = function(obj, iteratee, context) {
        iteratee = cb(iteratee, context);
        var keys = !isArrayLike(obj) && _.keys(obj),
            length = (keys || obj).length,
            results = Array(length);
        for (var index = 0; index < length; index++) {
          var currentKey = keys ? keys[index] : index;
          results[index] = iteratee(obj[currentKey], currentKey, obj);
        }
        return results;
      };

      // Create a reducing function iterating left or right.
      var createReduce = function(dir) {
        // Wrap code that reassigns argument variables in a separate function than
        // the one that accesses `arguments.length` to avoid a perf hit. (#1991)
        var reducer = function(obj, iteratee, memo, initial) {
          var keys = !isArrayLike(obj) && _.keys(obj),
              length = (keys || obj).length,
              index = dir > 0 ? 0 : length - 1;
          if (!initial) {
            memo = obj[keys ? keys[index] : index];
            index += dir;
          }
          for (; index >= 0 && index < length; index += dir) {
            var currentKey = keys ? keys[index] : index;
            memo = iteratee(memo, obj[currentKey], currentKey, obj);
          }
          return memo;
        };

        return function(obj, iteratee, memo, context) {
          var initial = arguments.length >= 3;
          return reducer(obj, optimizeCb(iteratee, context, 4), memo, initial);
        };
      };

      // **Reduce** builds up a single result from a list of values, aka `inject`,
      // or `foldl`.
      _.reduce = _.foldl = _.inject = createReduce(1);

      // The right-associative version of reduce, also known as `foldr`.
      _.reduceRight = _.foldr = createReduce(-1);

      // Return the first value which passes a truth test. Aliased as `detect`.
      _.find = _.detect = function(obj, predicate, context) {
        var keyFinder = isArrayLike(obj) ? _.findIndex : _.findKey;
        var key = keyFinder(obj, predicate, context);
        if (key !== void 0 && key !== -1) return obj[key];
      };

      // Return all the elements that pass a truth test.
      // Aliased as `select`.
      _.filter = _.select = function(obj, predicate, context) {
        var results = [];
        predicate = cb(predicate, context);
        _.each(obj, function(value, index, list) {
          if (predicate(value, index, list)) results.push(value);
        });
        return results;
      };

      // Return all the elements for which a truth test fails.
      _.reject = function(obj, predicate, context) {
        return _.filter(obj, _.negate(cb(predicate)), context);
      };

      // Determine whether all of the elements match a truth test.
      // Aliased as `all`.
      _.every = _.all = function(obj, predicate, context) {
        predicate = cb(predicate, context);
        var keys = !isArrayLike(obj) && _.keys(obj),
            length = (keys || obj).length;
        for (var index = 0; index < length; index++) {
          var currentKey = keys ? keys[index] : index;
          if (!predicate(obj[currentKey], currentKey, obj)) return false;
        }
        return true;
      };

      // Determine if at least one element in the object matches a truth test.
      // Aliased as `any`.
      _.some = _.any = function(obj, predicate, context) {
        predicate = cb(predicate, context);
        var keys = !isArrayLike(obj) && _.keys(obj),
            length = (keys || obj).length;
        for (var index = 0; index < length; index++) {
          var currentKey = keys ? keys[index] : index;
          if (predicate(obj[currentKey], currentKey, obj)) return true;
        }
        return false;
      };

      // Determine if the array or object contains a given item (using `===`).
      // Aliased as `includes` and `include`.
      _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
        if (!isArrayLike(obj)) obj = _.values(obj);
        if (typeof fromIndex != 'number' || guard) fromIndex = 0;
        return _.indexOf(obj, item, fromIndex) >= 0;
      };

      // Invoke a method (with arguments) on every item in a collection.
      _.invoke = restArguments(function(obj, path, args) {
        var contextPath, func;
        if (_.isFunction(path)) {
          func = path;
        } else if (_.isArray(path)) {
          contextPath = path.slice(0, -1);
          path = path[path.length - 1];
        }
        return _.map(obj, function(context) {
          var method = func;
          if (!method) {
            if (contextPath && contextPath.length) {
              context = deepGet(context, contextPath);
            }
            if (context == null) return void 0;
            method = context[path];
          }
          return method == null ? method : method.apply(context, args);
        });
      });

      // Convenience version of a common use case of `map`: fetching a property.
      _.pluck = function(obj, key) {
        return _.map(obj, _.property(key));
      };

      // Convenience version of a common use case of `filter`: selecting only objects
      // containing specific `key:value` pairs.
      _.where = function(obj, attrs) {
        return _.filter(obj, _.matcher(attrs));
      };

      // Convenience version of a common use case of `find`: getting the first object
      // containing specific `key:value` pairs.
      _.findWhere = function(obj, attrs) {
        return _.find(obj, _.matcher(attrs));
      };

      // Return the maximum element (or element-based computation).
      _.max = function(obj, iteratee, context) {
        var result = -Infinity, lastComputed = -Infinity,
            value, computed;
        if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
          obj = isArrayLike(obj) ? obj : _.values(obj);
          for (var i = 0, length = obj.length; i < length; i++) {
            value = obj[i];
            if (value != null && value > result) {
              result = value;
            }
          }
        } else {
          iteratee = cb(iteratee, context);
          _.each(obj, function(v, index, list) {
            computed = iteratee(v, index, list);
            if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
              result = v;
              lastComputed = computed;
            }
          });
        }
        return result;
      };

      // Return the minimum element (or element-based computation).
      _.min = function(obj, iteratee, context) {
        var result = Infinity, lastComputed = Infinity,
            value, computed;
        if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
          obj = isArrayLike(obj) ? obj : _.values(obj);
          for (var i = 0, length = obj.length; i < length; i++) {
            value = obj[i];
            if (value != null && value < result) {
              result = value;
            }
          }
        } else {
          iteratee = cb(iteratee, context);
          _.each(obj, function(v, index, list) {
            computed = iteratee(v, index, list);
            if (computed < lastComputed || computed === Infinity && result === Infinity) {
              result = v;
              lastComputed = computed;
            }
          });
        }
        return result;
      };

      // Shuffle a collection.
      _.shuffle = function(obj) {
        return _.sample(obj, Infinity);
      };

      // Sample **n** random values from a collection using the modern version of the
      // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
      // If **n** is not specified, returns a single random element.
      // The internal `guard` argument allows it to work with `map`.
      _.sample = function(obj, n, guard) {
        if (n == null || guard) {
          if (!isArrayLike(obj)) obj = _.values(obj);
          return obj[_.random(obj.length - 1)];
        }
        var sample = isArrayLike(obj) ? _.clone(obj) : _.values(obj);
        var length = getLength(sample);
        n = Math.max(Math.min(n, length), 0);
        var last = length - 1;
        for (var index = 0; index < n; index++) {
          var rand = _.random(index, last);
          var temp = sample[index];
          sample[index] = sample[rand];
          sample[rand] = temp;
        }
        return sample.slice(0, n);
      };

      // Sort the object's values by a criterion produced by an iteratee.
      _.sortBy = function(obj, iteratee, context) {
        var index = 0;
        iteratee = cb(iteratee, context);
        return _.pluck(_.map(obj, function(value, key, list) {
          return {
            value: value,
            index: index++,
            criteria: iteratee(value, key, list)
          };
        }).sort(function(left, right) {
          var a = left.criteria;
          var b = right.criteria;
          if (a !== b) {
            if (a > b || a === void 0) return 1;
            if (a < b || b === void 0) return -1;
          }
          return left.index - right.index;
        }), 'value');
      };

      // An internal function used for aggregate "group by" operations.
      var group = function(behavior, partition) {
        return function(obj, iteratee, context) {
          var result = partition ? [[], []] : {};
          iteratee = cb(iteratee, context);
          _.each(obj, function(value, index) {
            var key = iteratee(value, index, obj);
            behavior(result, value, key);
          });
          return result;
        };
      };

      // Groups the object's values by a criterion. Pass either a string attribute
      // to group by, or a function that returns the criterion.
      _.groupBy = group(function(result, value, key) {
        if (has(result, key)) result[key].push(value); else result[key] = [value];
      });

      // Indexes the object's values by a criterion, similar to `groupBy`, but for
      // when you know that your index values will be unique.
      _.indexBy = group(function(result, value, key) {
        result[key] = value;
      });

      // Counts instances of an object that group by a certain criterion. Pass
      // either a string attribute to count by, or a function that returns the
      // criterion.
      _.countBy = group(function(result, value, key) {
        if (has(result, key)) result[key]++; else result[key] = 1;
      });

      var reStrSymbol = /[^\ud800-\udfff]|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff]/g;
      // Safely create a real, live array from anything iterable.
      _.toArray = function(obj) {
        if (!obj) return [];
        if (_.isArray(obj)) return slice.call(obj);
        if (_.isString(obj)) {
          // Keep surrogate pair characters together
          return obj.match(reStrSymbol);
        }
        if (isArrayLike(obj)) return _.map(obj, _.identity);
        return _.values(obj);
      };

      // Return the number of elements in an object.
      _.size = function(obj) {
        if (obj == null) return 0;
        return isArrayLike(obj) ? obj.length : _.keys(obj).length;
      };

      // Split a collection into two arrays: one whose elements all satisfy the given
      // predicate, and one whose elements all do not satisfy the predicate.
      _.partition = group(function(result, value, pass) {
        result[pass ? 0 : 1].push(value);
      }, true);

      // Array Functions
      // ---------------

      // Get the first element of an array. Passing **n** will return the first N
      // values in the array. Aliased as `head` and `take`. The **guard** check
      // allows it to work with `_.map`.
      _.first = _.head = _.take = function(array, n, guard) {
        if (array == null || array.length < 1) return n == null ? void 0 : [];
        if (n == null || guard) return array[0];
        return _.initial(array, array.length - n);
      };

      // Returns everything but the last entry of the array. Especially useful on
      // the arguments object. Passing **n** will return all the values in
      // the array, excluding the last N.
      _.initial = function(array, n, guard) {
        return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
      };

      // Get the last element of an array. Passing **n** will return the last N
      // values in the array.
      _.last = function(array, n, guard) {
        if (array == null || array.length < 1) return n == null ? void 0 : [];
        if (n == null || guard) return array[array.length - 1];
        return _.rest(array, Math.max(0, array.length - n));
      };

      // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
      // Especially useful on the arguments object. Passing an **n** will return
      // the rest N values in the array.
      _.rest = _.tail = _.drop = function(array, n, guard) {
        return slice.call(array, n == null || guard ? 1 : n);
      };

      // Trim out all falsy values from an array.
      _.compact = function(array) {
        return _.filter(array, Boolean);
      };

      // Internal implementation of a recursive `flatten` function.
      var flatten = function(input, shallow, strict, output) {
        output = output || [];
        var idx = output.length;
        for (var i = 0, length = getLength(input); i < length; i++) {
          var value = input[i];
          if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
            // Flatten current level of array or arguments object.
            if (shallow) {
              var j = 0, len = value.length;
              while (j < len) output[idx++] = value[j++];
            } else {
              flatten(value, shallow, strict, output);
              idx = output.length;
            }
          } else if (!strict) {
            output[idx++] = value;
          }
        }
        return output;
      };

      // Flatten out an array, either recursively (by default), or just one level.
      _.flatten = function(array, shallow) {
        return flatten(array, shallow, false);
      };

      // Return a version of the array that does not contain the specified value(s).
      _.without = restArguments(function(array, otherArrays) {
        return _.difference(array, otherArrays);
      });

      // Produce a duplicate-free version of the array. If the array has already
      // been sorted, you have the option of using a faster algorithm.
      // The faster algorithm will not work with an iteratee if the iteratee
      // is not a one-to-one function, so providing an iteratee will disable
      // the faster algorithm.
      // Aliased as `unique`.
      _.uniq = _.unique = function(array, isSorted, iteratee, context) {
        if (!_.isBoolean(isSorted)) {
          context = iteratee;
          iteratee = isSorted;
          isSorted = false;
        }
        if (iteratee != null) iteratee = cb(iteratee, context);
        var result = [];
        var seen = [];
        for (var i = 0, length = getLength(array); i < length; i++) {
          var value = array[i],
              computed = iteratee ? iteratee(value, i, array) : value;
          if (isSorted && !iteratee) {
            if (!i || seen !== computed) result.push(value);
            seen = computed;
          } else if (iteratee) {
            if (!_.contains(seen, computed)) {
              seen.push(computed);
              result.push(value);
            }
          } else if (!_.contains(result, value)) {
            result.push(value);
          }
        }
        return result;
      };

      // Produce an array that contains the union: each distinct element from all of
      // the passed-in arrays.
      _.union = restArguments(function(arrays) {
        return _.uniq(flatten(arrays, true, true));
      });

      // Produce an array that contains every item shared between all the
      // passed-in arrays.
      _.intersection = function(array) {
        var result = [];
        var argsLength = arguments.length;
        for (var i = 0, length = getLength(array); i < length; i++) {
          var item = array[i];
          if (_.contains(result, item)) continue;
          var j;
          for (j = 1; j < argsLength; j++) {
            if (!_.contains(arguments[j], item)) break;
          }
          if (j === argsLength) result.push(item);
        }
        return result;
      };

      // Take the difference between one array and a number of other arrays.
      // Only the elements present in just the first array will remain.
      _.difference = restArguments(function(array, rest) {
        rest = flatten(rest, true, true);
        return _.filter(array, function(value){
          return !_.contains(rest, value);
        });
      });

      // Complement of _.zip. Unzip accepts an array of arrays and groups
      // each array's elements on shared indices.
      _.unzip = function(array) {
        var length = array && _.max(array, getLength).length || 0;
        var result = Array(length);

        for (var index = 0; index < length; index++) {
          result[index] = _.pluck(array, index);
        }
        return result;
      };

      // Zip together multiple lists into a single array -- elements that share
      // an index go together.
      _.zip = restArguments(_.unzip);

      // Converts lists into objects. Pass either a single array of `[key, value]`
      // pairs, or two parallel arrays of the same length -- one of keys, and one of
      // the corresponding values. Passing by pairs is the reverse of _.pairs.
      _.object = function(list, values) {
        var result = {};
        for (var i = 0, length = getLength(list); i < length; i++) {
          if (values) {
            result[list[i]] = values[i];
          } else {
            result[list[i][0]] = list[i][1];
          }
        }
        return result;
      };

      // Generator function to create the findIndex and findLastIndex functions.
      var createPredicateIndexFinder = function(dir) {
        return function(array, predicate, context) {
          predicate = cb(predicate, context);
          var length = getLength(array);
          var index = dir > 0 ? 0 : length - 1;
          for (; index >= 0 && index < length; index += dir) {
            if (predicate(array[index], index, array)) return index;
          }
          return -1;
        };
      };

      // Returns the first index on an array-like that passes a predicate test.
      _.findIndex = createPredicateIndexFinder(1);
      _.findLastIndex = createPredicateIndexFinder(-1);

      // Use a comparator function to figure out the smallest index at which
      // an object should be inserted so as to maintain order. Uses binary search.
      _.sortedIndex = function(array, obj, iteratee, context) {
        iteratee = cb(iteratee, context, 1);
        var value = iteratee(obj);
        var low = 0, high = getLength(array);
        while (low < high) {
          var mid = Math.floor((low + high) / 2);
          if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
        }
        return low;
      };

      // Generator function to create the indexOf and lastIndexOf functions.
      var createIndexFinder = function(dir, predicateFind, sortedIndex) {
        return function(array, item, idx) {
          var i = 0, length = getLength(array);
          if (typeof idx == 'number') {
            if (dir > 0) {
              i = idx >= 0 ? idx : Math.max(idx + length, i);
            } else {
              length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
            }
          } else if (sortedIndex && idx && length) {
            idx = sortedIndex(array, item);
            return array[idx] === item ? idx : -1;
          }
          if (item !== item) {
            idx = predicateFind(slice.call(array, i, length), _.isNaN);
            return idx >= 0 ? idx + i : -1;
          }
          for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
            if (array[idx] === item) return idx;
          }
          return -1;
        };
      };

      // Return the position of the first occurrence of an item in an array,
      // or -1 if the item is not included in the array.
      // If the array is large and already in sort order, pass `true`
      // for **isSorted** to use binary search.
      _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
      _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

      // Generate an integer Array containing an arithmetic progression. A port of
      // the native Python `range()` function. See
      // [the Python documentation](http://docs.python.org/library/functions.html#range).
      _.range = function(start, stop, step) {
        if (stop == null) {
          stop = start || 0;
          start = 0;
        }
        if (!step) {
          step = stop < start ? -1 : 1;
        }

        var length = Math.max(Math.ceil((stop - start) / step), 0);
        var range = Array(length);

        for (var idx = 0; idx < length; idx++, start += step) {
          range[idx] = start;
        }

        return range;
      };

      // Chunk a single array into multiple arrays, each containing `count` or fewer
      // items.
      _.chunk = function(array, count) {
        if (count == null || count < 1) return [];
        var result = [];
        var i = 0, length = array.length;
        while (i < length) {
          result.push(slice.call(array, i, i += count));
        }
        return result;
      };

      // Function (ahem) Functions
      // ------------------

      // Determines whether to execute a function as a constructor
      // or a normal function with the provided arguments.
      var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
        if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
        var self = baseCreate(sourceFunc.prototype);
        var result = sourceFunc.apply(self, args);
        if (_.isObject(result)) return result;
        return self;
      };

      // Create a function bound to a given object (assigning `this`, and arguments,
      // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
      // available.
      _.bind = restArguments(function(func, context, args) {
        if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
        var bound = restArguments(function(callArgs) {
          return executeBound(func, bound, context, this, args.concat(callArgs));
        });
        return bound;
      });

      // Partially apply a function by creating a version that has had some of its
      // arguments pre-filled, without changing its dynamic `this` context. _ acts
      // as a placeholder by default, allowing any combination of arguments to be
      // pre-filled. Set `_.partial.placeholder` for a custom placeholder argument.
      _.partial = restArguments(function(func, boundArgs) {
        var placeholder = _.partial.placeholder;
        var bound = function() {
          var position = 0, length = boundArgs.length;
          var args = Array(length);
          for (var i = 0; i < length; i++) {
            args[i] = boundArgs[i] === placeholder ? arguments[position++] : boundArgs[i];
          }
          while (position < arguments.length) args.push(arguments[position++]);
          return executeBound(func, bound, this, this, args);
        };
        return bound;
      });

      _.partial.placeholder = _;

      // Bind a number of an object's methods to that object. Remaining arguments
      // are the method names to be bound. Useful for ensuring that all callbacks
      // defined on an object belong to it.
      _.bindAll = restArguments(function(obj, keys) {
        keys = flatten(keys, false, false);
        var index = keys.length;
        if (index < 1) throw new Error('bindAll must be passed function names');
        while (index--) {
          var key = keys[index];
          obj[key] = _.bind(obj[key], obj);
        }
      });

      // Memoize an expensive function by storing its results.
      _.memoize = function(func, hasher) {
        var memoize = function(key) {
          var cache = memoize.cache;
          var address = '' + (hasher ? hasher.apply(this, arguments) : key);
          if (!has(cache, address)) cache[address] = func.apply(this, arguments);
          return cache[address];
        };
        memoize.cache = {};
        return memoize;
      };

      // Delays a function for the given number of milliseconds, and then calls
      // it with the arguments supplied.
      _.delay = restArguments(function(func, wait, args) {
        return setTimeout(function() {
          return func.apply(null, args);
        }, wait);
      });

      // Defers a function, scheduling it to run after the current call stack has
      // cleared.
      _.defer = _.partial(_.delay, _, 1);

      // Returns a function, that, when invoked, will only be triggered at most once
      // during a given window of time. Normally, the throttled function will run
      // as much as it can, without ever going more than once per `wait` duration;
      // but if you'd like to disable the execution on the leading edge, pass
      // `{leading: false}`. To disable execution on the trailing edge, ditto.
      _.throttle = function(func, wait, options) {
        var timeout, context, args, result;
        var previous = 0;
        if (!options) options = {};

        var later = function() {
          previous = options.leading === false ? 0 : _.now();
          timeout = null;
          result = func.apply(context, args);
          if (!timeout) context = args = null;
        };

        var throttled = function() {
          var now = _.now();
          if (!previous && options.leading === false) previous = now;
          var remaining = wait - (now - previous);
          context = this;
          args = arguments;
          if (remaining <= 0 || remaining > wait) {
            if (timeout) {
              clearTimeout(timeout);
              timeout = null;
            }
            previous = now;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
          } else if (!timeout && options.trailing !== false) {
            timeout = setTimeout(later, remaining);
          }
          return result;
        };

        throttled.cancel = function() {
          clearTimeout(timeout);
          previous = 0;
          timeout = context = args = null;
        };

        return throttled;
      };

      // Returns a function, that, as long as it continues to be invoked, will not
      // be triggered. The function will be called after it stops being called for
      // N milliseconds. If `immediate` is passed, trigger the function on the
      // leading edge, instead of the trailing.
      _.debounce = function(func, wait, immediate) {
        var timeout, result;

        var later = function(context, args) {
          timeout = null;
          if (args) result = func.apply(context, args);
        };

        var debounced = restArguments(function(args) {
          if (timeout) clearTimeout(timeout);
          if (immediate) {
            var callNow = !timeout;
            timeout = setTimeout(later, wait);
            if (callNow) result = func.apply(this, args);
          } else {
            timeout = _.delay(later, wait, this, args);
          }

          return result;
        });

        debounced.cancel = function() {
          clearTimeout(timeout);
          timeout = null;
        };

        return debounced;
      };

      // Returns the first function passed as an argument to the second,
      // allowing you to adjust arguments, run code before and after, and
      // conditionally execute the original function.
      _.wrap = function(func, wrapper) {
        return _.partial(wrapper, func);
      };

      // Returns a negated version of the passed-in predicate.
      _.negate = function(predicate) {
        return function() {
          return !predicate.apply(this, arguments);
        };
      };

      // Returns a function that is the composition of a list of functions, each
      // consuming the return value of the function that follows.
      _.compose = function() {
        var args = arguments;
        var start = args.length - 1;
        return function() {
          var i = start;
          var result = args[start].apply(this, arguments);
          while (i--) result = args[i].call(this, result);
          return result;
        };
      };

      // Returns a function that will only be executed on and after the Nth call.
      _.after = function(times, func) {
        return function() {
          if (--times < 1) {
            return func.apply(this, arguments);
          }
        };
      };

      // Returns a function that will only be executed up to (but not including) the Nth call.
      _.before = function(times, func) {
        var memo;
        return function() {
          if (--times > 0) {
            memo = func.apply(this, arguments);
          }
          if (times <= 1) func = null;
          return memo;
        };
      };

      // Returns a function that will be executed at most one time, no matter how
      // often you call it. Useful for lazy initialization.
      _.once = _.partial(_.before, 2);

      _.restArguments = restArguments;

      // Object Functions
      // ----------------

      // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
      var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
      var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
        'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

      var collectNonEnumProps = function(obj, keys) {
        var nonEnumIdx = nonEnumerableProps.length;
        var constructor = obj.constructor;
        var proto = _.isFunction(constructor) && constructor.prototype || ObjProto;

        // Constructor is a special case.
        var prop = 'constructor';
        if (has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

        while (nonEnumIdx--) {
          prop = nonEnumerableProps[nonEnumIdx];
          if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
            keys.push(prop);
          }
        }
      };

      // Retrieve the names of an object's own properties.
      // Delegates to **ECMAScript 5**'s native `Object.keys`.
      _.keys = function(obj) {
        if (!_.isObject(obj)) return [];
        if (nativeKeys) return nativeKeys(obj);
        var keys = [];
        for (var key in obj) if (has(obj, key)) keys.push(key);
        // Ahem, IE < 9.
        if (hasEnumBug) collectNonEnumProps(obj, keys);
        return keys;
      };

      // Retrieve all the property names of an object.
      _.allKeys = function(obj) {
        if (!_.isObject(obj)) return [];
        var keys = [];
        for (var key in obj) keys.push(key);
        // Ahem, IE < 9.
        if (hasEnumBug) collectNonEnumProps(obj, keys);
        return keys;
      };

      // Retrieve the values of an object's properties.
      _.values = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var values = Array(length);
        for (var i = 0; i < length; i++) {
          values[i] = obj[keys[i]];
        }
        return values;
      };

      // Returns the results of applying the iteratee to each element of the object.
      // In contrast to _.map it returns an object.
      _.mapObject = function(obj, iteratee, context) {
        iteratee = cb(iteratee, context);
        var keys = _.keys(obj),
            length = keys.length,
            results = {};
        for (var index = 0; index < length; index++) {
          var currentKey = keys[index];
          results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
        }
        return results;
      };

      // Convert an object into a list of `[key, value]` pairs.
      // The opposite of _.object.
      _.pairs = function(obj) {
        var keys = _.keys(obj);
        var length = keys.length;
        var pairs = Array(length);
        for (var i = 0; i < length; i++) {
          pairs[i] = [keys[i], obj[keys[i]]];
        }
        return pairs;
      };

      // Invert the keys and values of an object. The values must be serializable.
      _.invert = function(obj) {
        var result = {};
        var keys = _.keys(obj);
        for (var i = 0, length = keys.length; i < length; i++) {
          result[obj[keys[i]]] = keys[i];
        }
        return result;
      };

      // Return a sorted list of the function names available on the object.
      // Aliased as `methods`.
      _.functions = _.methods = function(obj) {
        var names = [];
        for (var key in obj) {
          if (_.isFunction(obj[key])) names.push(key);
        }
        return names.sort();
      };

      // An internal function for creating assigner functions.
      var createAssigner = function(keysFunc, defaults) {
        return function(obj) {
          var length = arguments.length;
          if (defaults) obj = Object(obj);
          if (length < 2 || obj == null) return obj;
          for (var index = 1; index < length; index++) {
            var source = arguments[index],
                keys = keysFunc(source),
                l = keys.length;
            for (var i = 0; i < l; i++) {
              var key = keys[i];
              if (!defaults || obj[key] === void 0) obj[key] = source[key];
            }
          }
          return obj;
        };
      };

      // Extend a given object with all the properties in passed-in object(s).
      _.extend = createAssigner(_.allKeys);

      // Assigns a given object with all the own properties in the passed-in object(s).
      // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
      _.extendOwn = _.assign = createAssigner(_.keys);

      // Returns the first key on an object that passes a predicate test.
      _.findKey = function(obj, predicate, context) {
        predicate = cb(predicate, context);
        var keys = _.keys(obj), key;
        for (var i = 0, length = keys.length; i < length; i++) {
          key = keys[i];
          if (predicate(obj[key], key, obj)) return key;
        }
      };

      // Internal pick helper function to determine if `obj` has key `key`.
      var keyInObj = function(value, key, obj) {
        return key in obj;
      };

      // Return a copy of the object only containing the whitelisted properties.
      _.pick = restArguments(function(obj, keys) {
        var result = {}, iteratee = keys[0];
        if (obj == null) return result;
        if (_.isFunction(iteratee)) {
          if (keys.length > 1) iteratee = optimizeCb(iteratee, keys[1]);
          keys = _.allKeys(obj);
        } else {
          iteratee = keyInObj;
          keys = flatten(keys, false, false);
          obj = Object(obj);
        }
        for (var i = 0, length = keys.length; i < length; i++) {
          var key = keys[i];
          var value = obj[key];
          if (iteratee(value, key, obj)) result[key] = value;
        }
        return result;
      });

      // Return a copy of the object without the blacklisted properties.
      _.omit = restArguments(function(obj, keys) {
        var iteratee = keys[0], context;
        if (_.isFunction(iteratee)) {
          iteratee = _.negate(iteratee);
          if (keys.length > 1) context = keys[1];
        } else {
          keys = _.map(flatten(keys, false, false), String);
          iteratee = function(value, key) {
            return !_.contains(keys, key);
          };
        }
        return _.pick(obj, iteratee, context);
      });

      // Fill in a given object with default properties.
      _.defaults = createAssigner(_.allKeys, true);

      // Creates an object that inherits from the given prototype object.
      // If additional properties are provided then they will be added to the
      // created object.
      _.create = function(prototype, props) {
        var result = baseCreate(prototype);
        if (props) _.extendOwn(result, props);
        return result;
      };

      // Create a (shallow-cloned) duplicate of an object.
      _.clone = function(obj) {
        if (!_.isObject(obj)) return obj;
        return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
      };

      // Invokes interceptor with the obj, and then returns obj.
      // The primary purpose of this method is to "tap into" a method chain, in
      // order to perform operations on intermediate results within the chain.
      _.tap = function(obj, interceptor) {
        interceptor(obj);
        return obj;
      };

      // Returns whether an object has a given set of `key:value` pairs.
      _.isMatch = function(object, attrs) {
        var keys = _.keys(attrs), length = keys.length;
        if (object == null) return !length;
        var obj = Object(object);
        for (var i = 0; i < length; i++) {
          var key = keys[i];
          if (attrs[key] !== obj[key] || !(key in obj)) return false;
        }
        return true;
      };


      // Internal recursive comparison function for `isEqual`.
      var eq, deepEq;
      eq = function(a, b, aStack, bStack) {
        // Identical objects are equal. `0 === -0`, but they aren't identical.
        // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
        if (a === b) return a !== 0 || 1 / a === 1 / b;
        // `null` or `undefined` only equal to itself (strict comparison).
        if (a == null || b == null) return false;
        // `NaN`s are equivalent, but non-reflexive.
        if (a !== a) return b !== b;
        // Exhaust primitive checks
        var type = typeof a;
        if (type !== 'function' && type !== 'object' && typeof b != 'object') return false;
        return deepEq(a, b, aStack, bStack);
      };

      // Internal recursive comparison function for `isEqual`.
      deepEq = function(a, b, aStack, bStack) {
        // Unwrap any wrapped objects.
        if (a instanceof _) a = a._wrapped;
        if (b instanceof _) b = b._wrapped;
        // Compare `[[Class]]` names.
        var className = toString.call(a);
        if (className !== toString.call(b)) return false;
        switch (className) {
          // Strings, numbers, regular expressions, dates, and booleans are compared by value.
          case '[object RegExp]':
          // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
          case '[object String]':
            // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
            // equivalent to `new String("5")`.
            return '' + a === '' + b;
          case '[object Number]':
            // `NaN`s are equivalent, but non-reflexive.
            // Object(NaN) is equivalent to NaN.
            if (+a !== +a) return +b !== +b;
            // An `egal` comparison is performed for other numeric values.
            return +a === 0 ? 1 / +a === 1 / b : +a === +b;
          case '[object Date]':
          case '[object Boolean]':
            // Coerce dates and booleans to numeric primitive values. Dates are compared by their
            // millisecond representations. Note that invalid dates with millisecond representations
            // of `NaN` are not equivalent.
            return +a === +b;
          case '[object Symbol]':
            return SymbolProto.valueOf.call(a) === SymbolProto.valueOf.call(b);
        }

        var areArrays = className === '[object Array]';
        if (!areArrays) {
          if (typeof a != 'object' || typeof b != 'object') return false;

          // Objects with different constructors are not equivalent, but `Object`s or `Array`s
          // from different frames are.
          var aCtor = a.constructor, bCtor = b.constructor;
          if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                                   _.isFunction(bCtor) && bCtor instanceof bCtor)
                              && ('constructor' in a && 'constructor' in b)) {
            return false;
          }
        }
        // Assume equality for cyclic structures. The algorithm for detecting cyclic
        // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

        // Initializing stack of traversed objects.
        // It's done here since we only need them for objects and arrays comparison.
        aStack = aStack || [];
        bStack = bStack || [];
        var length = aStack.length;
        while (length--) {
          // Linear search. Performance is inversely proportional to the number of
          // unique nested structures.
          if (aStack[length] === a) return bStack[length] === b;
        }

        // Add the first object to the stack of traversed objects.
        aStack.push(a);
        bStack.push(b);

        // Recursively compare objects and arrays.
        if (areArrays) {
          // Compare array lengths to determine if a deep comparison is necessary.
          length = a.length;
          if (length !== b.length) return false;
          // Deep compare the contents, ignoring non-numeric properties.
          while (length--) {
            if (!eq(a[length], b[length], aStack, bStack)) return false;
          }
        } else {
          // Deep compare objects.
          var keys = _.keys(a), key;
          length = keys.length;
          // Ensure that both objects contain the same number of properties before comparing deep equality.
          if (_.keys(b).length !== length) return false;
          while (length--) {
            // Deep compare each member
            key = keys[length];
            if (!(has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
          }
        }
        // Remove the first object from the stack of traversed objects.
        aStack.pop();
        bStack.pop();
        return true;
      };

      // Perform a deep comparison to check if two objects are equal.
      _.isEqual = function(a, b) {
        return eq(a, b);
      };

      // Is a given array, string, or object empty?
      // An "empty" object has no enumerable own-properties.
      _.isEmpty = function(obj) {
        if (obj == null) return true;
        if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
        return _.keys(obj).length === 0;
      };

      // Is a given value a DOM element?
      _.isElement = function(obj) {
        return !!(obj && obj.nodeType === 1);
      };

      // Is a given value an array?
      // Delegates to ECMA5's native Array.isArray
      _.isArray = nativeIsArray || function(obj) {
        return toString.call(obj) === '[object Array]';
      };

      // Is a given variable an object?
      _.isObject = function(obj) {
        var type = typeof obj;
        return type === 'function' || type === 'object' && !!obj;
      };

      // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError, isMap, isWeakMap, isSet, isWeakSet.
      _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error', 'Symbol', 'Map', 'WeakMap', 'Set', 'WeakSet'], function(name) {
        _['is' + name] = function(obj) {
          return toString.call(obj) === '[object ' + name + ']';
        };
      });

      // Define a fallback version of the method in browsers (ahem, IE < 9), where
      // there isn't any inspectable "Arguments" type.
      if (!_.isArguments(arguments)) {
        _.isArguments = function(obj) {
          return has(obj, 'callee');
        };
      }

      // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
      // IE 11 (#1621), Safari 8 (#1929), and PhantomJS (#2236).
      var nodelist = root.document && root.document.childNodes;
      if (typeof /./ != 'function' && typeof Int8Array != 'object' && typeof nodelist != 'function') {
        _.isFunction = function(obj) {
          return typeof obj == 'function' || false;
        };
      }

      // Is a given object a finite number?
      _.isFinite = function(obj) {
        return !_.isSymbol(obj) && isFinite(obj) && !isNaN(parseFloat(obj));
      };

      // Is the given value `NaN`?
      _.isNaN = function(obj) {
        return _.isNumber(obj) && isNaN(obj);
      };

      // Is a given value a boolean?
      _.isBoolean = function(obj) {
        return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
      };

      // Is a given value equal to null?
      _.isNull = function(obj) {
        return obj === null;
      };

      // Is a given variable undefined?
      _.isUndefined = function(obj) {
        return obj === void 0;
      };

      // Shortcut function for checking if an object has a given property directly
      // on itself (in other words, not on a prototype).
      _.has = function(obj, path) {
        if (!_.isArray(path)) {
          return has(obj, path);
        }
        var length = path.length;
        for (var i = 0; i < length; i++) {
          var key = path[i];
          if (obj == null || !hasOwnProperty.call(obj, key)) {
            return false;
          }
          obj = obj[key];
        }
        return !!length;
      };

      // Utility Functions
      // -----------------

      // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
      // previous owner. Returns a reference to the Underscore object.
      _.noConflict = function() {
        root._ = previousUnderscore;
        return this;
      };

      // Keep the identity function around for default iteratees.
      _.identity = function(value) {
        return value;
      };

      // Predicate-generating functions. Often useful outside of Underscore.
      _.constant = function(value) {
        return function() {
          return value;
        };
      };

      _.noop = function(){};

      // Creates a function that, when passed an object, will traverse that object’s
      // properties down the given `path`, specified as an array of keys or indexes.
      _.property = function(path) {
        if (!_.isArray(path)) {
          return shallowProperty(path);
        }
        return function(obj) {
          return deepGet(obj, path);
        };
      };

      // Generates a function for a given object that returns a given property.
      _.propertyOf = function(obj) {
        if (obj == null) {
          return function(){};
        }
        return function(path) {
          return !_.isArray(path) ? obj[path] : deepGet(obj, path);
        };
      };

      // Returns a predicate for checking whether an object has a given set of
      // `key:value` pairs.
      _.matcher = _.matches = function(attrs) {
        attrs = _.extendOwn({}, attrs);
        return function(obj) {
          return _.isMatch(obj, attrs);
        };
      };

      // Run a function **n** times.
      _.times = function(n, iteratee, context) {
        var accum = Array(Math.max(0, n));
        iteratee = optimizeCb(iteratee, context, 1);
        for (var i = 0; i < n; i++) accum[i] = iteratee(i);
        return accum;
      };

      // Return a random integer between min and max (inclusive).
      _.random = function(min, max) {
        if (max == null) {
          max = min;
          min = 0;
        }
        return min + Math.floor(Math.random() * (max - min + 1));
      };

      // A (possibly faster) way to get the current timestamp as an integer.
      _.now = Date.now || function() {
        return new Date().getTime();
      };

      // List of HTML entities for escaping.
      var escapeMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#x27;',
        '`': '&#x60;'
      };
      var unescapeMap = _.invert(escapeMap);

      // Functions for escaping and unescaping strings to/from HTML interpolation.
      var createEscaper = function(map) {
        var escaper = function(match) {
          return map[match];
        };
        // Regexes for identifying a key that needs to be escaped.
        var source = '(?:' + _.keys(map).join('|') + ')';
        var testRegexp = RegExp(source);
        var replaceRegexp = RegExp(source, 'g');
        return function(string) {
          string = string == null ? '' : '' + string;
          return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
        };
      };
      _.escape = createEscaper(escapeMap);
      _.unescape = createEscaper(unescapeMap);

      // Traverses the children of `obj` along `path`. If a child is a function, it
      // is invoked with its parent as context. Returns the value of the final
      // child, or `fallback` if any child is undefined.
      _.result = function(obj, path, fallback) {
        if (!_.isArray(path)) path = [path];
        var length = path.length;
        if (!length) {
          return _.isFunction(fallback) ? fallback.call(obj) : fallback;
        }
        for (var i = 0; i < length; i++) {
          var prop = obj == null ? void 0 : obj[path[i]];
          if (prop === void 0) {
            prop = fallback;
            i = length; // Ensure we don't continue iterating.
          }
          obj = _.isFunction(prop) ? prop.call(obj) : prop;
        }
        return obj;
      };

      // Generate a unique integer id (unique within the entire client session).
      // Useful for temporary DOM ids.
      var idCounter = 0;
      _.uniqueId = function(prefix) {
        var id = ++idCounter + '';
        return prefix ? prefix + id : id;
      };

      // By default, Underscore uses ERB-style template delimiters, change the
      // following template settings to use alternative delimiters.
      _.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
      };

      // When customizing `templateSettings`, if you don't want to define an
      // interpolation, evaluation or escaping regex, we need one that is
      // guaranteed not to match.
      var noMatch = /(.)^/;

      // Certain characters need to be escaped so that they can be put into a
      // string literal.
      var escapes = {
        "'": "'",
        '\\': '\\',
        '\r': 'r',
        '\n': 'n',
        '\u2028': 'u2028',
        '\u2029': 'u2029'
      };

      var escapeRegExp = /\\|'|\r|\n|\u2028|\u2029/g;

      var escapeChar = function(match) {
        return '\\' + escapes[match];
      };

      // JavaScript micro-templating, similar to John Resig's implementation.
      // Underscore templating handles arbitrary delimiters, preserves whitespace,
      // and correctly escapes quotes within interpolated code.
      // NB: `oldSettings` only exists for backwards compatibility.
      _.template = function(text, settings, oldSettings) {
        if (!settings && oldSettings) settings = oldSettings;
        settings = _.defaults({}, settings, _.templateSettings);

        // Combine delimiters into one regular expression via alternation.
        var matcher = RegExp([
          (settings.escape || noMatch).source,
          (settings.interpolate || noMatch).source,
          (settings.evaluate || noMatch).source
        ].join('|') + '|$', 'g');

        // Compile the template source, escaping string literals appropriately.
        var index = 0;
        var source = "__p+='";
        text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
          source += text.slice(index, offset).replace(escapeRegExp, escapeChar);
          index = offset + match.length;

          if (escape) {
            source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
          } else if (interpolate) {
            source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
          } else if (evaluate) {
            source += "';\n" + evaluate + "\n__p+='";
          }

          // Adobe VMs need the match returned to produce the correct offset.
          return match;
        });
        source += "';\n";

        // If a variable is not specified, place data values in local scope.
        if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

        source = "var __t,__p='',__j=Array.prototype.join," +
          "print=function(){__p+=__j.call(arguments,'');};\n" +
          source + 'return __p;\n';

        var render;
        try {
          render = new Function(settings.variable || 'obj', '_', source);
        } catch (e) {
          e.source = source;
          throw e;
        }

        var template = function(data) {
          return render.call(this, data, _);
        };

        // Provide the compiled source as a convenience for precompilation.
        var argument = settings.variable || 'obj';
        template.source = 'function(' + argument + '){\n' + source + '}';

        return template;
      };

      // Add a "chain" function. Start chaining a wrapped Underscore object.
      _.chain = function(obj) {
        var instance = _(obj);
        instance._chain = true;
        return instance;
      };

      // OOP
      // ---------------
      // If Underscore is called as a function, it returns a wrapped object that
      // can be used OO-style. This wrapper holds altered versions of all the
      // underscore functions. Wrapped objects may be chained.

      // Helper function to continue chaining intermediate results.
      var chainResult = function(instance, obj) {
        return instance._chain ? _(obj).chain() : obj;
      };

      // Add your own custom functions to the Underscore object.
      _.mixin = function(obj) {
        _.each(_.functions(obj), function(name) {
          var func = _[name] = obj[name];
          _.prototype[name] = function() {
            var args = [this._wrapped];
            push.apply(args, arguments);
            return chainResult(this, func.apply(_, args));
          };
        });
        return _;
      };

      // Add all of the Underscore functions to the wrapper object.
      _.mixin(_);

      // Add all mutator Array functions to the wrapper.
      _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
          var obj = this._wrapped;
          method.apply(obj, arguments);
          if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
          return chainResult(this, obj);
        };
      });

      // Add all accessor Array functions to the wrapper.
      _.each(['concat', 'join', 'slice'], function(name) {
        var method = ArrayProto[name];
        _.prototype[name] = function() {
          return chainResult(this, method.apply(this._wrapped, arguments));
        };
      });

      // Extracts the result from a wrapped and chained object.
      _.prototype.value = function() {
        return this._wrapped;
      };

      // Provide unwrapping proxy for some methods used in engine operations
      // such as arithmetic and JSON stringification.
      _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

      _.prototype.toString = function() {
        return String(this._wrapped);
      };

      // AMD registration happens at the end for compatibility with AMD loaders
      // that may not enforce next-turn semantics on modules. Even though general
      // practice for AMD registration is to be anonymous, underscore registers
      // as a named module because, like jQuery, it is a base library that is
      // popular enough to be bundled in a third party lib, but not be part of
      // an AMD load request. Those cases could generate an error when an
      // anonymous define() is called outside of a loader request.
      if (typeof define == 'function' && define.amd) {
        define('underscore', [], function() {
          return _;
        });
      }
    }());

</script>