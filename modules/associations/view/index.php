
<style>
    .m-portlet .m-portlet__body {
        padding: 0.7rem 1.7rem !important;
    }
   /* input {
    text-transform: capitalize; ;
    }
    textarea {
    text-transform: capitalize; ;
    }*/
</style>

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->	
    <div class="row">
        <div class="col-lg-12">
          <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
              <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                  <span class="m-accordion__item-icon">
                      <i class="fa flaticon-edit-1"></i>
                  </span>
                  <span class="m-accordion__item-title"> 
                    
                          <?php if (!empty($this->singleassociations)) {
                              echo "Update Associations";
                          } else {
                              echo "Add Associations";
                          } ?>
                     
                    </span>
                  <span class="m-accordion__item-mode"></span>
              </div>
              <?php if (!empty($this->singleassociations)) {
                             ?>
                                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php

                          } else {
                              ?>
                                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php
                          } ?>
                  <div class="m-accordion__item-content">
                      <form  id="m_form"  class="m-form m-form--label-align-right" enctype ="multipart/form-data"action="<?php echo Urls::$BASE; ?>associations/createassociations" method="POST">
                          <div class="m-portlet__body">
                              <?php if (!empty($this->singleassociations)) {
                                  ?>
                                  <input type="hidden" name="nassociationsIDPK" value="<?php echo $this->singleassociations[0]['nAssociationIDPK']; ?>">
                                  <div class="form-group">                
                                      <div class="col-lg-12">
                                          <label>Associations Title</label>
                                          <input type="text"
                                                 class="form-control m-input"
                                                 name="title"
                                                  value="<?php echo $this->singleassociations[0]['tAssociationName']?>">
                                      </div>
                                      <br> 
                                      <div class="col-lg-12">
                                        <label><h5>Select Associations Icon</h5></label>
                                        <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                            <span class="" onclick="document.getElementById('associations_image').click();">
                                                 <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                            </span>
                                        </div>
                                        <br>
                                        <input style="display:none;" type="file" name="associations_image"  id="associations_image" onchange="vpb_image_preview(this)">

                                        <div style="width:100%;" align="center" id="vpb-display-preview">
                                           <img id="imgProduct" width="100px;" src="<?php echo Urls::$BASE.Urls::$ASSOCIATIONS.$this->singleassociations[0]['nAssociationIconPath']?>" >
                                        </div>
                                        </div>
                                  </div>
                                  <?php } else { ?>
                                <div class="form-group row">
                                  <div class="col-lg-12">
                                      <label>Associations Title</label>
                                      <input type="text"
                                             class="form-control m-input"
                                             name="title" id="title" 
                                             value="">
                                  </div>
                                  <br>
                                  <div class="col-lg-12">
                                      <br>                                       
                                      <label><h5>Select associations Banner Image</h5></label>
                                      <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                      <span class="" onclick="document.getElementById('associations_image').click();">
                                          <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                      </span>
                                      </div>
                                      <br>
                                      <input type="hidden" name="deleteImg" id="deleteImg">
                                      <input type="hidden" name="deleteImgs" id="deleteImgs">
                                      <input style="display:none;" type="file" name="associations_image"  id="associations_image" onchange="vpb_image_preview(this)">
                                      <div style="width:100%;" align="center" id="vpb-display-preview">
                                      </div>
                                   </div>
                                </div>
                              <?php } ?>
                          </div>
                          <?php if (!empty($this->singleassociations)) {

                              ?>
                              <div class="s-portlet__foot s-portlet__foot--fit col-lg-12 row">
                                <div class="col-lg-2"></div>
                                  <div class="s-form__actions col-lg-4" style="text-align: center;">
                                      <button type="submit" class="btn btn-danger btn-block" value="update" name="submit">Update
                                           Associations
                                      </button>
                                  </div>
                                  <div class="s-form__actions col-lg-4">
                                      <button type="reset" onclick="window.location = '<?php echo Urls::$BASE; ?>associations/';" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                      </button>
                                  </div>
                                  <div class="col-lg-2"></div>
                              </div>
                          <?php } else { ?>
                              <div class="s-portlet__foot s-portlet__foot--fit row">
                                <div class="col-lg-2"></div>
                                  <div class="s-form__actions col-lg-4" style="text-align: center;">
                                      <button type="submit" class="btn btn-danger btn-block" value="submit" name="submit">Add
                                           Associations
                                      </button>
                                  </div>
                                  <div class="s-form__actions col-lg-4">
                                      <button type="reset" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                      </button>
                                  </div>
                                  <div class="col-lg-2"></div>
                              </div>
                             
                          <?php }
                          ?>
                      </form>           
                  </div>
              </div>
             
            </div>
          </div>
        </div>
    </div>
     <div class="row">
                  <div class="col-lg-12">
                      <div class="m-portlet m-portlet--mobile">
                          <div class="m-portlet__head">
                              <div class="m-portlet__head-caption">
                                  <div class="m-portlet__head-title">
                                      <h3 class="m-portlet__head-text">
                                        
                                          Associations Listing
                                      </h3>
                                  </div>
                              </div>

                          </div>
                          <div class="m-portlet__body">
                              <div class="table-responsive">
                                  <!--begin: Datatable -->
                                  <table class="table table-striped- table-bordered table-hover table-checkable" border="0" id="s1">
                                      <thead align="center">
                                      <tr>
                                          <th>#</th>
                                          <th>Associations Title</th>
                                          <!-- <th>Associations Description</th> -->
                                          <th>Associations Icon Image</th>
                                          
                                          <th width="19%">Actions</th>
                                      </tr>
                                      </thead>
                                      <tbody  align="center">
                                      <?php
                                      if (!empty($this->data)) {
                                          for ($i = 0; $i < count($this->data); $i++) {
                                              ?>
                                              <tr>
                                                  <td><?php echo $i + 1; ?></td>
                                                  
                                                   <td><?php echo $this->data[$i]['tAssociationName']; ?></td>
                                                  <!-- <td><?php echo $this->data[$i]['tassociationsDescription']; ?></td> -->

                                                  <td><?php
                                                  if (empty($this->data[$i]['nAssociationIconPath'])) 
                                                  {
                                                      ?><img  width="50px;" src="<?php echo Urls::$BASE.Urls::$ASSOCIATIONS."no-image.png";?>"/>
                                                      <?php 
                                                  }
                                                  else{
                                                      ?><img  width="50px;" src="<?php echo Urls::$BASE.Urls::$ASSOCIATIONS.$this->data[$i]['nAssociationIconPath']?>"/> 
                                                      <?php
                                                  }
                                                  ?></td>
                                                  
                                                  <td>
                                                      <!-- <a class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash"></i></a> -->
                                                      <form method="post" style="display: inline-table;"
                                                            action="<?php echo Urls::$BASE ?>associations/editassociations">
                                                          <input type="hidden" name="associationsID"
                                                                 value="<?php echo $this->data[$i]['nAssociationIDPK']; ?>">
                                                                 <button style="background-color: #fff !important;border-color:  #ff5a5e !important;" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Associations" type="submit" class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                                          <!-- <button data-toggle="m-tooltip" data-placement="top" type="button"
                                                                  data-original-title=""
                                                                  class="btn_edit btn btn-warning m-btn m-btn--icon btn-sm m-btn--icon-only">
                                                              <i class="fa fa-pencil-alt"></i></button> -->
                                                      </form>

                                                      <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Associations" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"  onclick="setId(<?php echo $this->data[$i]['nAssociationIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                                  </td>
                                              </tr>
                                              <?php
                                          }
                                      }
                                      ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {

       
        $("#submit").click(function(e) 
        {
            // var Daylent = $(".chkday:checkbox:checked").length;

            if($('#title').val() == '')
            {
                toastr.error("Please Enter Associations Title.", "Associations Title");
                e.preventDefault();
            }
            mUtil.scrollTo("add_form", -200);  
        });
        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } ); 
        $("#m_form").validate({
            rules: {
                title: {
                    required: true
                }
            }
        });

    });
    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this Associations ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>associations/deleteassociations",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>associations/";
                        }
                    }
                });
            }
        });
    }
    function vpb_image_preview(vpb_selector_) 
    {

       
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#vpb-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                    console.log('#vpb-display-preview_0');

                       $('#vpb-display-preview').append(
                       '<div id="selector_'+vpb_o_+'" class="vpb_wrapper"> \
                       <img id="image'+vpb_o_+'" width="100px;" name="image[0][] class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br /> \
                       <a  style="cursor:pointer;padding-top:5px;" title="Click here to remove '+ escape(file.name) +'" \
                       onclick="vpb_remove_selected(\''+vpb_o_+'\',\''+file.name+'\')">Remove</a> \
                       </div>');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });

        
    }
    function vpb_remove_selected(id,name)
    {
        var get_value = $('#deleteImg').val();

        if(get_value == '')
        {
            $('#deleteImg').val(name);
        }
        else
        {
            $('#deleteImg').val(get_value+','+name);
        }

         var ds = Array();
        ds.push(id);
        // delete [ds];
        var deta = Array();
        deta.push($('#deleteImg').val());

        $('#deleteImgs').val(deta);
        $('#selector_'+id).html("");
        document.getElementById('selector_'+id).style.display = "none";
        console.log(deta);

        console.log(ds);
       
        // $('#v-add-'+id).remove();
       
}

</script>