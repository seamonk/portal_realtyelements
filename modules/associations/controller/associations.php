<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Associations extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/associations/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllassociations();
        $this->view->data = $data;

        $this->view->render($name, "Associations", $header, $footer);
    }

    public function createassociations() // insert associations
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                $FolderName =  Urls::$ASSOCIATIONS;

                if (!is_dir($FolderName)) 
                {  
                    mkdir($FolderName, 0777, true);     
                    copy(Urls::$BASE.Urls::$IMG_UPLOADS.'no_image.png',$FolderName.'/no_image.png');
                }   

                $files = $_FILES['associations_image']['name'];

                $preImgText = date('ymdhsi');

                $Folder_name = Urls::$ASSOCIATIONS;
                // $Login 
                $login = SessionHandling::get('loggedId');
                // $get_deleted_Img = $_POST['deleteFoodImg'];

                // $images = $_FILES['Meetus_logo1']['name'];

                $fileExtension = pathinfo($files,PATHINFO_EXTENSION);
                
                if (!empty($_FILES['associations_image']['name']) )
                {
                    $imageName = $preImgText.".".$extension;
                }
                else
                {
                    $imageName = "";
                }
               
                $targetPath = $Folder_name.$preImgText.".".$extension;

                move_uploaded_file($_FILES['associations_image']['tmp_name'],$targetPath);
                

                $field = array('tAssociationName', 'nAssociationIconPath', 'nCreatedBy');
                $value = array($title, $imageName, $login);

                $associationsData = array_combine($field, $value);
                // print_r($associationsData);
                // exit();
                $associationsID = $this->model->insert($associationsData, 'tblassociations');

                if (isset($associationsID) && $associationsID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Associations");
                    header("Location:../associations/");
                } else {
                    SessionHandling::set("suc_msg", "Associations added successfully");
                    header("Location:../associations/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update Associations
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            $login = SessionHandling::get('loggedId');

            $id = array('nAssociationIDPK' => $nassociationsIDPK);

            $FolderName =  Urls::$ASSOCIATIONS;

            if (!is_dir($FolderName)) 
            {  
                mkdir($FolderName, 0777, true);     
                copy(Urls::$IMG_UPLOADS.'no_image.png',$FolderName.'/no_image.png');
            }   

            $files = $_FILES['associations_image']['name']; 


            if ($files != "") {
                        
                $preImgText = date('ymdhsi');

                $Folder_name = Urls::$ASSOCIATIONS;

                $extension = pathinfo($files,PATHINFO_EXTENSION);
                $imageName = $preImgText.".".$extension;
                $targetPath = $Folder_name.$preImgText.".".$extension;
                move_uploaded_file($_FILES['associations_image']['tmp_name'],$targetPath);

                $field = array('tAssociationName', 'nAssociationIconPath');
                $value = array($title,$imageName);
            } else {
                 $field = array('tAssociationName');
                $value = array($title);

            }

            $associationsData = array_combine($field, $value);
            // print_r($associationsData);
            // exit();

            $associationsID = $this->model->update($associationsData, 'tblassociations', $id);

            if (isset($associationsID) && $associationsID > 0) {
                SessionHandling::set("suc_msg", "Associations updated successfully");
                header("Location:../associations/");
            } else if (isset($associationsID) && $associationsID == 0) {
                SessionHandling::set("suc_msg", "Associations updated successfully");
                header("Location:../associations/");
            } else {
                SessionHandling::set("err_msg", "Error while updating Associations");
                header("Location:../associations/");
            }
        }
    }

    public function listing()  // associations listing
    {
        $name = 'modules/associations/view/associations_listing.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllassociations();
        $this->view->data = $data;

        $this->view->render($name, "Associations Listing", $header, $footer);

    }

    public function editassociations()   // edit associations
    {
        $name = 'modules/associations/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $data = $this->model->getAllassociations();
        $this->view->data = $data;

        $singleassociationsData = $this->model->fetchSingleassociations($associationsID);
        $this->view->singleassociations = $singleassociationsData;

        $this->view->render($name, "Update Associations", $header, $footer);
    }
    public function viewassociations()   // edit associations
    {
         
        extract($_POST);
        $singleassociationsData = $this->model->fetchSingleassociations($Pid);
        echo json_encode($singleassociationsData);

       
    }
    

    public function deleteassociations() // delete associations
    {
        $associationsID = $_POST['id'];

        $id = array('nAssociationIDPK' => $associationsID);

        $field = array('bIsRemoved');
        $value = array(1);

        $associationsData = array_combine($field, $value);

        $res = $this->model->update($associationsData, 'tblassociations', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Associations deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Associations");
            echo "false";
        }
    }

}