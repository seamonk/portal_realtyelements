<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */
class agent_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) 
        {
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) 
        {
            return $this->db->lastInsertId();         
        }
        return 0;
    }

    
    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE ".$whereId." = $id";
        // print_r($sql);
        // exit();
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // start For count in Profile

    public function getPropertyCount()
    {
        $sth = $this->db->prepare("SELECT
                                      nPropertyIDPK,
                                      tPropertyTypeIDFK,
                                      vPropertyFor,
                                      tPropertyName,
                                      tPropertyTitle,
                                      tPropertyFrontImageFolderPath,
                                      tPropertyAddress1,
                                      tPropertyAddress2,
                                      tPropertyLocality,
                                      nPropertyAreaIDFK,
                                      nPropertyCityIDFK,
                                      nPropertyStateIDFK,
                                      nPropertyCountryIDFK,
                                      dPropertyLatitude,
                                      dPropertyLongitude,
                                      tPropertyDescription,
                                      tPropertyBHK,
                                      bIsPropertyFurnished,
                                      tPropertyBlock,
                                      tPropertyUnitNumber,
                                      tPropertyFloor,
                                      nPropertyTotalFloors,
                                      tBuildingName,
                                      fPropertyPrice,
                                      fPropertyRent,
                                      fPropertyFixedPrice,
                                      fPropertyNegotiablePrice,
                                      tPropertyPossessionStatus,
                                      yPropertyPossesionYear,
                                      tPropertyCompanyName,
                                      bIsPropertyActive,
                                      dPropertyCreatedOn,
                                      nPropertyCreatedBy
                                    FROM
                                      tblproperties
                                    WHERE
                                      bIsPropertyActive = 1");

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
    public function getSubscritionDetails($id)
    {
        $dummy = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT
                                        nSubscriptionIDPK,
                                        tSubscriptionName,
                                        fSubscriptionPrice,
                                        fOtherCountrySubscriptionPrice,
                                        nListingLimit,
                                        dtSubscriptionExpireOn,
                                        dtListingExpireOn,
                                        bIsActive,
                                        dCreatedOn,
                                        dtRemovedOn,
                                        bIsRemoved,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblsubcription
                                    WHERE
                                        bIsActive = 1
                                        AND
                                        bIsRemoved = 0
                                        AND
                                        bIsDummyData = $dummy
                                        AND
                                        nSubscriptionIDPK = $id
                                    ");
        // print_r($sth);
        // exit();

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) 
        {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function oldSubcritionPlanDetails($id)
    {
       $dummy = DUMMY_DATA;

        $sth = $this->db->prepare("SELECT
                                        
                                        nSubscriptionIDFK,
                                        nUserListingLimit,
                                        dtSubscriptionExpireOn,
                                        nSubscriptionDaysCount
                                    FROM
                                        tblagentsubscription
                                    WHERE
                                        nUserIDFK = $id
                                        AND
                                        bIsActive = 1
                                    ");
        // print_r($sth);
        // exit();

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) 
        {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAgentSubscritionDetails($id)
    {
        $dummy = DUMMY_DATA;

        $sth = $this->db->prepare("SELECT
                                        
                                        nSubscriptionIDFK
                                    FROM
                                        tblagentsubscription
                                    WHERE
                                        nUserIDFK = $id
                                        AND
                                        bIsActive = 1
                                    ");
        // print_r($sth);
        // exit();

        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) 
        {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getUsersCount()
    {
        $sth = $this->db->prepare("SELECT tUserName FROM tblusers WHERE bIsUserRemoved = 0 AND nIsUserDummyData = 0");

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }

    public function getAgentsCount()
    {
        $sth = $this->db->prepare("SELECT * FROM tblagents WHERE  bIsAgentActive = 1 AND bIsAgentRemoved = 0" );

        $sth->execute();
        $total = $sth->rowCount();
        $row = $sth->fetch();

        if ($total > 0) 
        {
            return $total;
        } 
        else 
        {
            return NULL;
        }
    }
 
    public function getContactUsDetails()
    {
        $sth = $this->db->prepare("SELECT
                                        nAgentContactUsIDPK,
                                        agent.vAgentFirstName,
                                        agent.vAgentMiddleName, 
                                        agent.vAgentLastName,
                                        tCustomerName,
                                        vCustomerrMobileNo,
                                        tCustomerEmailId,
                                        dtCreatedOn,
                                        bIsRemoved
                                    FROM
                                        tblagentcontactus as agentcontact,
                                        tblagents as agent
                                        WHERE
                                        agentcontact.nAgentIDFK = agent.nAgentIDPK
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    // End For count in Profile
    
    public function checkPassword($password)
    {   
        $loggedId = SessionHandling::get("loggedId"); 

        $sth = $this->db->prepare("SELECT
                                      
                                        tUserPassword                                      
                                    FROM
                                        tblusers
                                    WHERE
                                        nUserIDPK = $loggedId
                                        AND
                                        tUserPassword = '$password'
                                        AND
                                        bIsUserRemoved = 0
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
       
    }
    
     public function getSearchCompany($Like)
    {
        $sth = $this->db->prepare("SELECT
                                        nCompanyIDPK,
                                        tCompanyName,
                                        tCompanyWebsiteLink,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblcompanies
                                        WHERE
                                    bIsRemoved = 0 AND $Like
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getSearchProperty($Like)
    {

        $loggedId = SessionHandling::get("loggedId");

        $sth = $this->db->prepare("SELECT
                                        nPropertyIDPK,
                                        tPropertyName,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        nCreatedBy,
                                        bIsDummyData
                                    FROM
                                        tblproperties
                                    WHERE
                                        
                                    bIsRemoved = 0 
                                    
                                    AND $Like 

                                    GROUP BY tPropertyName
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getLanguages()
    {
        $sth = $this->db->prepare("SELECT
                                    nLanguageIDPK,
                                    tLanguageName,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                FROM
                                    tbllanguages
                                 WHERE
                                    bIsRemoved = 0
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAssociations()
    {
        $sth = $this->db->prepare("SELECT
                                    nAssociationIDPK,
                                    tAssociationName,
                                    nAssociationIconPath,
                                    dtCreatedOn,
                                    nCreatedBy,
                                    bIsRemoved,
                                    dtRemovedOn,
                                    nRemovedBy,
                                    bIsDummyData
                                FROM
                                    tblassociations
                                WHERE
                                    bIsRemoved = 0
                                      ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getSearchDesignation($Like)
    {
        $sth = $this->db->prepare("SELECT
                                        nDesignationIDPK,
                                        tDesignationName,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tbldesignations
                                    WHERE
                                        bIsRemoved = 0 AND $Like
                                      ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getSpecializations()
    {
        $sth = $this->db->prepare("SELECT
                                        nSpecializationIDPK,
                                        tSpecializationName,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblspecializations
                                    WHERE
                                    bIsRemoved = 0
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getLocality()
    {
        $sth = $this->db->prepare("SELECT
                                    loc.nLocalityIDPK,
                                    loc.nAreaIDFK,
                                    a.tAreaName,
                                    ct.tCityName,
                                    loc.nCityIDFK,
                                    s.tStateName,
                                    loc.nStateIDFK,
                                    loc.tLocalityName,
                                    loc.dLocalityLatitude,
                                    loc.dLocalityLongitude,
                                    loc.dLocalityCreatedOn,
                                    loc.nLocalityCreatedBy,
                                    loc.bIsLocalityRemoved
                                FROM
                                    tbllocalities AS loc,
                                    tblareas AS a,
                                    tblcities AS ct,
                                    tblstates AS s,
                                    tblcountries AS c
                                WHERE
                                    loc.nAreaIDFK = a.nAreaIDPK AND loc.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.bIsCityRemoved = 0 AND s.bIsStateRemoved = 0 AND c.bIsCountryRemoved = 0 AND loc.bIsLocalityRemoved = 0
                                ORDER BY
                                  a.tAreaName ASC");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    



// Start Agent Profile Details
    public function getAgentLanguages($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                    agenlang.nAgentLanguageKnownIDPK,
                                    agenlang.nAgentIDFK,
                                    lang.nLanguageIDPK,
                                    lang.tLanguageName,
                                    agenlang.nLanguageIDFK,
                                    agenlang.dtCreatedOn,
                                    agenlang.nCreatedBy,
                                    agenlang.bIsRemoved,
                                    agenlang.dtRemovedOn,
                                    agenlang.nRemovedBy,
                                    agenlang.bIsDummyData
                                FROM
                                    tblagentlanguagesknown as agenlang,
                                    tbllanguages as lang
                                WHERE
                                    agenlang.nAgentIDFK = $loggedId
                                    AND
                                    agenlang.nLanguageIDFK = lang.nLanguageIDPK
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAgentContact($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                    agentContact.nAgentContactNumberIDPK,
                                    agentContact.nAgentIDFK,
                                    agentContact.vContactNumber,
                                    agentContact.bIsMobileNumberVerified,
                                    agentContact.bIsRemoved,
                                    agentContact.dtRemovedOn,
                                    agentContact.nRemovedBy,
                                    agentContact.dtCreatedBy,
                                    agentContact.dtCreatedOn,
                                    agentContact.bIsDummyData
                                FROM
                                    tblagentcontactnumbers as agentContact,
                                    tblagents as agent
                                WHERE
                                    agent.nAgentIDPK = agentContact.nAgentIDFK 
                                    AND
                                    agent.nUserIDFK = $loggedId
                                    ");

        // print_r($sth);
        // exit();  
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    // getAgentContact
    public function getAgentAssociations($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                        agentassoci.nAssociationIDFK,
                                        associ.nAssociationIDPK,
                                        associ.tAssociationName,
                                        associ.nAssociationIconPath,
                                        associ.dtCreatedOn,
                                        associ.nCreatedBy,
                                        associ.bIsRemoved,
                                        associ.dtRemovedOn,
                                        associ.nRemovedBy,
                                        associ.bIsDummyData
                                    FROM
                                        tblassociations AS associ,
                                        tblagentassociations AS agentassoci
                                    WHERE
                                        agentassoci.nAssociationIDFK = associ.nAssociationIDPK 
                                        AND 
                                        agentassoci.nAgentIDFK = $loggedId
                                        AND
                                        associ.bIsRemoved = 0
                                      ");
        //  print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getAgentDesignation($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                        nDesignationIDPK,
                                        tDesignationName,
                                        dtCreatedOn,
                                        nCreatedBy,
                                        bIsRemoved,
                                        dtRemovedOn,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tbldesignations
                                    WHERE
                                        bIsRemoved = 0
                                         WHERE
                                    nAgentIDFK = $loggedId
                                      ");
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    
    public function getAgentSpecializations($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                        agentSpe.nSpecializationIDFK
                                    FROM
                                        tblspecializations as spe,
                                        tblagentspecializations agentSpe
                                    WHERE
                                    agentSpe.nAgentIDFK = $loggedId
                                    AND
                                    agentSpe.nSpecializationIDFK = spe.nSpecializationIDPK
                                    AND
                                    spe.bIsRemoved = 0
                                      ");

          
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function findMobileNumber($number)
    {
        $sth = $this->db->prepare("SELECT
                                        nUserPhoneNumber
                                    FROM
                                        tblusers
                                    WHERE
                                        nUserPhoneNumber = $number
                                        AND
                                        bIsUserRemoved = 0
                                        AND
                                        bIsActive = 1
                                      ");

        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }
    public function checkExitsEmail($email)
    {
        $sth = $this->db->prepare("SELECT
                                        vUserEmailID
                                    FROM
                                        tblusers
                                    WHERE
                                        vUserEmailID = $email
                                        AND
                                        bIsUserRemoved = 0
                                        AND
                                        bIsActive = 1
                                      ");

        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function checkOthermobileno($number)
    {   
        $sth = $this->db->prepare("SELECT
                                        nUserPhoneNumber
                                    FROM
                                        tblusers
                                    WHERE
                                        nUserPhoneNumber = $number
                                        AND
                                        bIsUserRemoved = 0
                                        AND
                                        bIsActive = 1
                                        GROUP BY nUserPhoneNumber
                                      ");

        $sth->execute();
        $row = $sth->fetch();
        if (!empty($row)) 
        {
            $data = true;
        }
        else
        {
            $data = false;

            $sth1 = $this->db->prepare("SELECT
                                            agentNum.vContactNumber
                                        FROM  
                                            tblagentcontactnumbers AS agentNum,
                                            tblagents as agent
                                        WHERE
                                            agentNum.vContactNumber = $number
                                            AND
                                            agentNum.bIsRemoved = 0
                                           AND
                                            agentNum.bIsActive = 1
                                            AND
                                            agent.bIsRemoved = 0
                                          ");
            $sth1->execute();
            $row = $sth1->fetch();
            if (!empty($row)) 
            {
                $data = true;
            }
            else{
                 $data = false;
            }
        }
        return $data;
        
    }
    
    
    public function getAgentLocality($loggedId)
    {
        $sth = $this->db->prepare("SELECT
                                    locexp.nAgentLocalityIDPK,
                                    locexp.nAgentIDFK,
                                    locexp.nLocalityIDFK,
                                    loc.nLocalityIDPK,
                                    loc.nAreaIDFK,
                                    a.tAreaName,
                                    ct.tCityName,
                                    loc.nCityIDFK,
                                    s.tStateName,
                                    loc.nStateIDFK,
                                    loc.tLocalityName,
                                    loc.dLocalityLatitude,
                                    loc.dLocalityLongitude,
                                    loc.dLocalityCreatedOn,
                                    loc.nLocalityCreatedBy,
                                    loc.bIsLocalityRemoved
                                FROM
                                    tbllocalities AS loc,
                                    tblagentlocalitiesexpert as locexp,
                                    tblareas AS a,
                                    tblcities AS ct,
                                    tblstates AS s,
                                    tblcountries AS c
                                WHERE
                                    locexp.nAgentIDFK = $loggedId
                                    AND
                                    locexp.nLocalityIDFK = loc.nLocalityIDPK
                                    AND
                                    loc.nAreaIDFK = a.nAreaIDPK AND loc.nCityIDFK = ct.nCityIDPK AND ct.nStateIDFK = s.nStateIDPK AND s.nCountryIDFK = c.nCountryIDPK AND ct.bIsCityRemoved = 0 AND s.bIsStateRemoved = 0 AND c.bIsCountryRemoved = 0 AND loc.bIsLocalityRemoved = 0
                                ORDER BY
                                  a.tAreaName ASC");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }
    public function getProfileDetail($id)
    {
       $sth = $this->db->prepare("SELECT
                                    tblagents.nAgentIDPK,
                                    tblusers.nUserIDPK,
                                    tblusers.tUserName,
                                    tblusers.vUserFirstName,
                                    tblusers.vUserMiddleName,
                                    tblusers.vUserLastName,
                                    tblusers.eUserGender,
                                    tblusers.tUserProfileImgPath,
                                    tblusers.bIsMobileNumberVerified,
                                    tblusers.bIsEmailIdVerified,
                                    tblusers.tVerificationOTP,
                                    tblusers.vUserEmailID,
                                    tblusers.nUserPhoneNumber,
                                    tblusers.tUserProfileImgPath,
                                    tblagents.nAgentReraID,
                                    tblagents.nCompanyIDFK,
                                    tblagents.nDesignationIDFK,
                                    tblagents.nAgentExperience,
                                    tblagents.tAgentDescription,
                                    tblagents.vAgentMobileNumber2,
                                    tblagents.vAgentLandlineNumber,
                                    tblagents.tAgentAddress,
                                    tblagents.nAgentTotalViews,
                                    tblagents.nAreaIDFK,
                                    tblagents.nCityIDFK,
                                    tblagents.nStateIDFK,
                                    tblagents.nCountryIDFK,
                                    tblagents.bIsAgentReraCertified,
                                    tblagents.tGoogleMapAddressLink,
                                    tblagents.tAgentCompanyWebsiteLink,
                                    tblagents.tAgentFacebookLink,
                                    tblagents.tAgentLinkedinLink,
                                    tblagents.tAgentTwitterLink,
                                    tblagents.tAgentYoutubeLink,
                                    tblagents.tAgentInstaLink,
                                    tblagents.bIsAgentVerifiedFromAdmin,
                                    tbldesignations.tDesignationName,
                                    tblcompanies.tCompanyName,
                                    tblcities.tCityName,
                                    tblstates.tStateName
                                FROM
                                    tblagents
                                LEFT JOIN tblusers
                                ON tblagents.nUserIDFK = tblusers.nUserIDPK AND tblusers.bIsUserRemoved = 0
                                LEFT JOIN tbldesignations
                                ON tblagents.nDesignationIDFK = tbldesignations.nDesignationIDPK  AND tbldesignations.bIsRemoved = 0
                                LEFT JOIN tblcities
                                ON tblagents.nCityIDFK = tblcities.nCityIDPK  AND tblcities.bIsCityRemoved = 0
                                LEFT JOIN tblstates
                                ON tblagents.nStateIDFK = tblstates.nStateIDPK  AND tblstates.bIsStateRemoved = 0
                                LEFT JOIN tblcompanies
                                ON tblagents.nCompanyIDFK = tblcompanies.nCompanyIDPK AND tblcompanies.bIsRemoved = 0
                                WHERE
                                    tblusers.nUserIDPK = $id
                                    ");
        // echo "<pre>";
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetch();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }


// End Agent Profile Details


    //stat subcription Plans

    public function getAllSubscritionPlans()
    {
        $dummy = DUMMY_DATA;
        $sth = $this->db->prepare("SELECT
                                        nSubscriptionIDPK,
                                        tSubscriptionName,
                                        fSubscriptionPrice,
                                        fOtherCountrySubscriptionPrice,
                                        nListingLimit,
                                        dtSubscriptionExpireOn,
                                        dtListingExpireOn,
                                        bIsActive,
                                        dCreatedOn,
                                        dtRemovedOn,
                                        bIsRemoved,
                                        nRemovedBy,
                                        bIsDummyData
                                    FROM
                                        tblsubcription
                                    WHERE
                                        bIsActive = 1
                                        AND
                                        bIsRemoved = 0
                                        AND
                                        bIsDummyData = $dummy
                                    ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) 
        {
            return $row;
        } else {
            return NULL;
        }
    }

    //

     public function getAgent()
    {
        $requestData= $_REQUEST;
       
        $columns = array( 

            0=> 'agent.nAgentIDPK',
            1=> '',
            2=> 'type.nUserTypeName',
            3=> 'users.vUserFirstName',
            4=> 'users.vUserEmailID',
            5=> 'users.nAddListingUpto',
            6=> 'agent.bIsAgentVerifiedFromAdmin',
            7=> 'agent.bIsAgentFeatured',
            8=> ''
            
        );
       $sql = "SELECT
                                        agent.nAgentIDPK,
                                        users.nUserIDPK,
                                        users.tUserName,
                                        type.nUserTypeName,
                                        users.vUserFirstName,
                                        users.vUserMiddleName,
                                        users.vUserLastName,
                                        users.vUserEmailID,
                                        users.nUserPhoneNumber,
                                        users.tUserProfileImgPath,
                                        users.nAddListingUpto,
                                        users.bIsMobileNumberVerified,
                                        users.bIsEmailIdVerified,
                                        agent.nAgentReraID,
                                        agent.nDesignationIDFK,
                                        agent.nAgentExperience,
                                        agent.tAgentDescription,
                                        agent.nCompanyIDFK,
                                        agent.vAgentMobileNumber2,
                                        agent.vAgentLandlineNumber,
                                        agent.tAgentAddress,
                                        agent.nAreaIDFK,
                                        agent.nCityIDFK,
                                        agent.nStateIDFK,
                                        agent.nCountryIDFK,
                                        agent.bIsAgentReraCertified,
                                        agent.tGoogleMapAddressLink,
                                        agent.tAgentCompanyWebsiteLink,
                                        agent.tAgentFacebookLink,
                                        agent.tAgentLinkedinLink,
                                        agent.tAgentTwitterLink,
                                        agent.tAgentYoutubeLink,
                                        agent.tAgentInstaLink,
                                        agent.bIsAgentVerifiedFromAdmin,
                                        agent.bIsAgentActive,
                                        agent.bIsAgentFeatured,
                                        agent.dtAgentFeaturedExpireOn,
                                        agent.dtCreatedOn,
                                        agent.bIsRemoved,
                                        agent.nRemovedBy,
                                        agent.dtRemovedOn,
                                        agent.bIsDummyData
                                    FROM
                                        tblagents as agent,
                                        tblusers as users,
                                        tblusertype as type
                                    WHERE
                                        users.nUserIDPK = agent.nUserIDFK
                                        AND
                                        users.nUserTypeIDFK = type.nUserTypeIDPK 
                                        AND
                                        agent.bIsRemoved = 0
                                        AND
                                        agent.bIsAgentActive = 1
                                        AND
                                        users.bIsActive = 1
                                        ";


        if(!empty($requestData['search']['value']) ) 
        {    
            $sql.=" AND (users.tUserName LIKE '%".$requestData['search']['value']."%' "; 

            $sql.=" OR users.vUserEmailID LIKE '%".$requestData['search']['value']."%' "; 

            $sql.=" OR users.nUserPhoneNumber LIKE '%".$requestData['search']['value']."%' "; 

            $sql.="OR type.nUserTypeName LIKE '%".$requestData['search']['value']."%' "; 

            $sql.=" OR users.vUserFirstName LIKE '%".$requestData['search']['value']."%'"; 

            $sql.=" OR users.vUserMiddleName LIKE '%".$requestData['search']['value']."%'"; 

            $sql.=" OR users.vUserLastName LIKE '%".$requestData['search']['value']."%'";

            $sql.=" OR users.nUserPhoneNumber LIKE '%".$requestData['search']['value']."%'";

            $sql.=" OR agent.nAgentReraID LIKE '%".$requestData['search']['value']."%'"; 

            $sql.=" OR users.nAddListingUpto LIKE '%".$requestData['search']['value']."%' )"; 
        }
        $sth = $this->db->prepare($sql);

        $sth->execute();
        

        $totalFiltered = $sth->rowCount();
        $sql.=" ORDER BY ".$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir']." LIMIT ".$requestData['start']." ,".$requestData['length']." "; 
        $sth = $this->db->prepare($sql);

        $sth->execute();
        $totalData = $sth->rowCount();

        $i=1+$requestData['start'];

        $data = array();

        while($row = $sth->fetch()) 
        {   
            if (empty($row['tUserProfileImgPath'])) 
            {
               $row['tUserProfileImgPath'] = "user.jpg";
            }
            $nestedData=array();

            $nestedData[] = $i;
            $nestedData[] = "<img src='".Urls::$BASE.Urls::$IMG_UPLOADS_USER.$row['tUserProfileImgPath']."'/>";

            $middleName = '';

            if($row['vUserMiddleName'] != '')
            {
                $middleName .= $row['vUserMiddleName']."<br>";
            }

            $nestedData[] = "<span style='color:red;'>".$row['nUserTypeName']."</span>";
            if ($row['bIsMobileNumberVerified'] == 1 && $row['bIsEmailIdVerified'] == 1) 
            {
                $nestedData[] = "<span style='color:Green;'>".$row['vUserFirstName']."<br>".$middleName.$row['vUserLastName']." </span>"; 
            }
            else
            {
                $nestedData[] = "<span style='color:red;'>".$row['vUserFirstName']."<br>".$middleName.$row['vUserLastName']."</span>";
            }
            
            $nestedData[] = $row['vUserEmailID'];
            $nestedData[] = $row['nAddListingUpto'];
            
            $fName =  ucfirst($row['vUserFirstName']);
            
            $lName =  ucfirst($row['vUserLastName']);
            
            if ($row['bIsAgentVerifiedFromAdmin'] == 1) 
            {
                $nestedData[] = "<span class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                    <label>
                                        <input type='checkbox' name='verified' checked disabled id='".$row['nUserIDPK']."' onchange='VerifyAgent(".$row['nUserIDPK'].");'>
                                        <span ></span>
                                    </label>
                                </span>";
            }
            else{
                 $nestedData[] = "<span class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                    <label>
                                        <input type='checkbox' name='verified' id='".$row['nUserIDPK']."' onchange=VerifyAgent(".$row['nUserIDPK'].",'".$row['vUserEmailID']."','".$fName."','".$lName."');>
                                        <span ></span>
                                    </label>
                                </span>";
            }
            if ($row['bIsMobileNumberVerified'] == 1 && $row['bIsEmailIdVerified'] == 1) {
              
                
                if ($row['bIsAgentFeatured'] == 1) 
                {
                    $nestedData[] = "<span class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                        <label>
                                            <input type='checkbox' name='verified' checked  id='".$row['nUserIDPK']."' onchange=FeaturedAgent(".$row['nUserIDPK'].",0,'".$row['vUserEmailID']."','".$row['vUserFirstName']."');>
                                           <span > </span>
                                        </label>
                                           <br><span style='font-size:12px;'>Exp On: ".$row['dtAgentFeaturedExpireOn']."</span>
                                    </span>";
                }
                else{
                     $nestedData[] = "<span class='m-switch m-switch--outline m-switch--sm m-switch--icon m-switch--danger'>
                                        <label>
                                            <input type='checkbox' name='verified'  id='".$row['nUserIDPK']."' onchange=FeaturedAgent(".$row['nUserIDPK'].",1,'".$row['vUserEmailID']."','".$row['vUserFirstName']."');>
                                            <span ></span>
                                        </label>
                                           
                                    </span>";
                }
            }
            else
            {
                 // $nestedData[] = "";
                 $nestedData[] = "";

            }
            
            if ($row['bIsMobileNumberVerified'] == 1 && $row['bIsEmailIdVerified'] == 1) 
            {
                $Controll = "<form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Change Subscription' data-original-title='Add Subscription' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='addsubscription(\"".$row['nUserIDPK'].','.$row['nAddListingUpto']."\")'><i class='fa fa-plus'></i></button>
                            </form>
                            ";
            }
            else
            {
                $Controll= "";
            }
            $nestedData[] = "
                            ".$Controll."
                            <form method='post' action='".Urls::$BASE."agent/view_Agent' style='display: inline-table;'>
                                <input type='hidden' name='agentID' value='".$row['nUserIDPK']."'>
                                <button data-toggle='m-tooltip' data-placement='top' title='View ".$row['tUserName']." Detail' data-original-title='View ".$row['tUserName']." Detail' type='button' class='btn_view btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill'><i class='fa fa-eye'></i></button>
                            </form>
                            <form style='display: inline-table;'>
                                <button  data-toggle='m-tooltip' data-placement='top' title='Delete Agent' data-original-title='Delete Agent' type='button' class='btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill' onclick='setId(".$row['nAgentIDPK'].",".$row['nUserIDPK'].")'><i class='fa fa-trash-alt'></i></button>
                            </form>
                            ";

            $data[] = $nestedData;

            $i++;
        }

         $json_data = array(

                    "draw"            => intval( $requestData['draw'] ),   

                    "recordsTotal"    => intval( $totalData ),  // total number of records

                    "recordsFiltered" => intval( $totalFiltered ), 

                    "data"            => $data   // total data array

                    );

        echo json_encode($json_data);   
    }

    function sendOTPOverSMS ( $mobile ) {

        $OTP = rand ( 1000, 9999 ) ;
        $message            = "Welcome to Agenpoint. ".$OTP . " " . MESSAGE_FOR_OTP_OVER_SMS. " Thanks.";         
     
        $fields = http_build_query([
            'APIKey'        => SMS_API,
            'senderid'      => 'SMSTST',
            'channel'       => 2,
            'DCS'           => 0,
            'flashsms'      => 0,
            'number'        => $mobile,
            'text'          => $message
        ]);
        
        $url = SMS_API_URL.'?'.$fields;
      
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ]);
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $resp = json_decode($resp);
       

        if($resp->ErrorCode == "000")
        {
           return $OTP; 
        }
        else
        {
            echo "error";
        }

    }

    public function getUserPhoneNumbers($agentID,$OtherPhoneNo)
    {
        $sth = $this->db->prepare("SELECT
                                    agentContact.nAgentContactNumberIDPK                                   
                                FROM
                                    tblagentcontactnumbers as agentContact                                   
                                WHERE                                  
                                    agentContact.nAgentIDFK = $agentID
                                AND
                                    agentContact.vContactNumber = $OtherPhoneNo
                                    ");

        $sth->execute();
        $row = $sth->rowCount();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

}