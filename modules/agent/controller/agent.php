<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */
use PHPMailer\PHPMailer;



use PHPMailer\Exception;



require './commons/PHPMailer/src/Exception.php';

require './commons/PHPMailer/src/PHPMailer.php';

require './commons/PHPMailer/src/SMTP.php';


class Agent extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
        if(function_exists('date_default_timezone_set'))
        {
            date_default_timezone_set("Asia/Kolkata");
        }
    }
    public function contactUs()
    {
        $name = 'modules/agent/view/contactUs.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        // $id = $_POST['userID'];
        // print_r($id);
        // exit();
        $data = $this->model->getContactUsDetails();
        // print_r($data);
        // exit();
        $this->view->data = $data;

        $this->view->render($name, "Contactus Listing", $header, $footer);
    }

    public function sendMail($emailId,$subject,$body)
    {
        $mail = new PHPMailer\PHPMailer(true);                          

        try 
        {
            $mail->SMTPDebug = 2;                                 
            $mail->Host = 'seamonksolution.com';  
            $mail->SMTPAuth = true; 

            $mail->Username = 'agenpoint@seamonksolution.com';                 
            $mail->Password = 'agenpoint@2019';    

            $mail->SMTPSecure = 'ssl';               
            $mail->Port = 465;                                   
            $mail->setfrom("agenpoint@seamonksolution.com", 'Agenpoint');
            $mail->addAddress($emailId, $emailId);     
            $mail->isHTML(true);                           
            $mail->Subject = $subject;
            $mail->Body = $body;

            if($mail->send())
            {
                return true;
            }
            else
            {
                return false;
            }
        } 
        catch (Exception $e) 
        {
            echo 'false';
        }
    }
    
    public function index()
    {
        $name = 'modules/agent/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        $getAllSubscritionPlans = $this->model->getAllSubscritionPlans();
        $this->view->getAllSubscritionPlans = $getAllSubscritionPlans;

        $this->view->render($name,"Agent Listing", $header, $footer);
    }
    public function getAgentDetails()
    {
        $data = $this->model->getAgent();
        $this->view->data = $data;
    }
    public function getAllSubscritionPlans()
    {
        $getAllSubscritionPlans = $this->model->getAllSubscritionPlans();
        // print_r($getAllSubscritionPlans);
        // exit();
        echo json_encode($getAllSubscritionPlans);
    }
    public function delete_listing_image()
    {
        
        $data= '';
        $loggedId = SessionHandling::get("loggedId");

        $id = array('nUserIDPK' => $loggedId);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('tUserProfileImgPath');
        $value = array($data);

        $image_data = array_combine($field, $value);

        $res = $this->model->update($image_data, 'tblusers',$id);
      
        if($res > 0)
        {
            echo "user.png";
        }
        else
        {
            echo "false";
        } 
    }
     public function verifiedFromAdmin()
    {
        extract($_POST);
        $userid = array('nUserIDFK' => $_POST['id']);

        $field = array('bIsAgentVerifiedFromAdmin');

        $value = array($status);

        $dataUpdate = array_combine($field, $value);


        $res =  $this->model->update($dataUpdate, 'tblagents',$userid);
        
        $body = '';

        $body .= 'Hello '.$name.',';

        $body .= '<br><br>';

        $body .= 'Welcome To Agenpoint. We are happy to have you on board.';

        $body .= '<br><br>';

        $body .= 'Your Account has been verified by Agenpoint. Now you can post properties on our website and promote yourself.';

        $body .= '<br><br>';

        $body .= 'Thanks,';
        $body .= '<br>';

        $body .= '<a href="www.agenpoint.com">www.agenpoint.com</a>';

        $email = $this->sendMail($emailID,"Account Verified From Agnpoint",$body);
        
        if($res > 0)
        {
            echo "true";     
        }
        else if($res == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        } 
    }
    public function FeaturedAgentUpdate()
    {
        extract($_POST);

        // print_r($expireDate);
        // exit();
        $userid = array('nUserIDFK' => $_POST['id']);
        if (isset($expireOn)) 
        {
            $currentDate = date('Y-m-d');
            $expireDate = date('Y-m-d', strtotime($currentDate.' + '.$expireOn.'days'));
            $field = array('bIsAgentFeatured','dtAgentFeaturedExpireOn');

            $value = array($status,$expireDate);
        }
        else
        {
            $field = array('bIsAgentFeatured');

            $value = array($status);
        }


        $dataUpdate = array_combine($field, $value);
        
        $body = '';
        
        $body .= 'Hello '.$userName;
        $body .= '<br><br>';

       $res =  $this->model->update($dataUpdate, 'tblagents',$userid);
       if ($status == 1) 
       {
            $body .= 'We are glad to have you as a Featured Agent on Agenpoint. We wish you lots of business enquiries.';
            
            $body .= '<br><br>';
            
            $body .= 'Thanks,<br>';
            
            $body .= '<a href="'.Urls::$BASE.'">www.agenpoint.com</a>';
           
            $email = $this->sendMail($emailID,"Agent Featured From Agnpoint",$body);
       }
       else if ($status == 0) 
       {
            $body .= 'We are sad to inform you that your Featured Agent service on Agenpoint has expired today. Get in touch with us to reactivate this feature and generate more enquires for your business.';
            
            $body .= '<br><br>';
            
            $body .= 'Thanks,<br>';
            
            $body .= '<a href="'.Urls::$BASE.'">www.agenpoint.com</a>';
           
            $email = $this->sendMail($emailID,"Agent Un-Featured From Agnpoint",$body);
       }
        if($res > 0)
        {
            echo "true";     
        }
        else if($res == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        } 
    }
    public function update_listing_limit()
    {
       
        extract($_POST);
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $oldSubcritionPlan = $this->model->oldSubcritionPlanDetails($userID);
        $oldlistingLimit = $oldSubcritionPlan[0]['nUserListingLimit'];

        $date = str_replace('/', '-', $subscritionExpireOn );

        $expireOnDate = date("Y-m-d h:m:i", strtotime($date));
        
        $date = date('Y-m-d h:m:i');
        // print_r($possessionDate);
        // exit();

        $olddate = $oldSubcritionPlan[0]['dtSubscriptionExpireOn'];

        // $expireOnDate = date_format($subscritionExpireOn,"Y/m/d H:i:s");

        // if ($date < $olddate) 
        // {   
            $listing_limit = $oldlistingLimit + $listing_limit;
        // }
        

        $id = array('nUserIDPK' => $_POST['userID']);
        $userid = array('nUserIDFK' => $_POST['userID']);


        $field = array('nAddListingUpto');

        $value = array($listing_limit);

        $dataUpdate = array_combine($field, $value);

        $this->model->update($dataUpdate, 'tblusers',$id);

            // print_r($listing_limit);
        // exit();

        $field2 = array('bIsActive');

        $value2 = array(0);

        $dataUpdate2 = array_combine($field2, $value2);
        
        $this->model->update($dataUpdate2, 'tblagentsubscription',$userid);


        $field1 = array('nUserIDFK','nSubscriptionIDFK','nSubscriptionDaysCount','nUserListingLimit','dtSubscriptionExpireOn');

        $value1 = array($userID,$plans,$subscritionDaysCount,$listing_limit,$expireOnDate);

        $dataUpdate1 = array_combine($field1, $value1);

        $res = $this->model->insert($dataUpdate1, 'tblagentsubscription');


        if($res > 0)
        {
            echo "true";     
        }
        else if($res == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        }    

        // $this->view->render($name,"Agent Listing", $header, $footer);

    }
    
    public function getSubscritionDetails()
    {
        extract($_POST);
        $getSubscritionDetails = $this->model->getSubscritionDetails($id);
        echo json_encode($getSubscritionDetails);
    }

    public function getAgentSubscritionDetails()
    {
        extract($_POST); 
        $getAgentSubscritionDetails = $this->model->getAgentSubscritionDetails($id);
        echo json_encode($getAgentSubscritionDetails);
    }
    


    public function profile()
    {
        $loggedId = SessionHandling::get("loggedId");
        
        $name = 'modules/agent/view/profile.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getProfileDetail($loggedId);
        $this->view->data = $data;

        $getAgentContact = $this->model->getAgentContact($loggedId);
        $this->view->getAgentContact = $getAgentContact;

        $getAgentLanguages = $this->model->getAgentLanguages($loggedId);
        $this->view->getAgentLanguages = $getAgentLanguages;

        $getAgentAssociations = $this->model->getAgentAssociations($loggedId);
        $this->view->getAgentAssociations= $getAgentAssociations;

        $getSpecializations = $this->model->getAgentSpecializations($loggedId);
        $this->view->getAgentSpecializations= $getSpecializations;

        $getLocality = $this->model->getAgentLocality($loggedId);
        $this->view->getLocality= $getLocality;
 
        $this->view->render($name,"Profile", $header, $footer);
    }
    public function findMobileNumber()
    {
        extract($_POST);
       $findMobileNumber = $this->model->findMobileNumber($number);
       echo json_encode($findMobileNumber);
    }
     public function checkExitsEmail()
    {
        extract($_POST);
       $checkExitsEmail = $this->model->checkExitsEmail($email);
       echo json_encode($checkExitsEmail);
    }
    
    public function checkOthermobileno()
    {
        extract($_POST);
       $checkOthermobileno = $this->model->checkOthermobileno($number);
       echo json_encode($checkOthermobileno);
    }
    public function updateOtherNumber()
    {
        // print_r($_POST);
        extract($_POST);

        $id = array('nAgentContactNumberIDPK' => $id);
        $new_phone_field = array('vContactNumber','bIsMobileNumberVerified');
        $new_Phone_value = array($otherNumber,0);
        $Basic_data = array_combine($new_phone_field, $new_Phone_value);
        // print_r($Basic_data);
        // exit();
        $Basic_data = $this->model->update($Basic_data, 'tblagentcontactnumbers',$id);

       if($Basic_data > 0)
        {
            echo "true" ;     
        }
        else if($Basic_data == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        }   

    }

    public function deleteOtherNumber()
    {
        // print_r($_POST);
        extract($_POST);

        $Basic_data = $this->model->delete($id, 'tblagentcontactnumbers','nAgentContactNumberIDPK');

       if($Basic_data > 0)
        {
            echo "true" ;     
        }
        else if($Basic_data == 0)
        {
            echo "true";         
        }
        else
        {
            echo "false";     
        }   

    }

    public function view_agent()
    {
        // print_r($_POST);
        extract($_POST);
        // exit();
        $loggedId = SessionHandling::get("loggedId");
        
        $name = 'modules/agent/view/view_agent.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        

        $data = $this->model->getProfileDetail($agentID);
        $this->view->data = $data;

        $getAgentContact = $this->model->getAgentContact($agentID);
        $this->view->getAgentContact = $getAgentContact;

        $getAgentLanguages = $this->model->getAgentLanguages($agentID);
        $this->view->getAgentLanguages = $getAgentLanguages;

        $getAgentAssociations = $this->model->getAgentAssociations($agentID);
        $this->view->getAgentAssociations= $getAgentAssociations;

        $getSpecializations = $this->model->getAgentSpecializations($agentID);
        $this->view->getAgentSpecializations= $getSpecializations;

        $getLocality = $this->model->getAgentLocality($agentID);
        $this->view->getLocality= $getLocality;
 
        $this->view->render($name,"Profile", $header, $footer);
    }

    public function getCountry()
    {
        $getCountrys = $this->model->getCountrys();
        echo json_encode($getCountrys);
    }
    public function getSearchCompany()
    {  
     // print_r($_REQUEST);
     //    exit();
        $t1=$_REQUEST['term'];

        $kt=explode(" ",$t1);
        $q = '';
        while(list($key,$val)=each($kt))
        {
            if($val<>" " and strlen($val) > 0)
            {
                $q .= " tCompanyName LIKE '%$val%' OR ";
            }
        }
        $q = substr($q,0,(strlen($q)-3)); 

        $getSearchCompany = $this->model->getSearchCompany($q);
        echo json_encode($getSearchCompany);
    }
    public function getSearchProperty()
    {
        // print_r($_REQUEST);
        // exit();

        $t1=$_REQUEST['term'];

        $kt=explode(" ",$t1);
        $q = '';
        while(list($key,$val)=each($kt))
        {
            if($val<>" " and strlen($val) > 0)
            {
                $q .= " tPropertyName LIKE '%$val%' OR ";
            }
        }
        $q = substr($q,0,(strlen($q)-3)); 

        $getSearchProperty = $this->model->getSearchProperty($q);
        echo json_encode($getSearchProperty);
    }
    
    public function getLanguages()
    {
        $getLanguages = $this->model->getLanguages();
        echo json_encode($getLanguages);
    }
    public function getAssociations()
    {
        $getAssociations = $this->model->getAssociations();
        echo json_encode($getAssociations);
    }
    public function getSpecializations()
    {
        $getSpecializations = $this->model->getSpecializations();
        echo json_encode($getSpecializations);
    }
    public function getSearchDesignation()
    {
        $t1=$_REQUEST['term'];

        $kt=explode(" ",$t1);
        $q = '';
        while(list($key,$val)=each($kt))
        {
            if($val<>" " and strlen($val) > 0)
            {
                $q .= " tDesignationName LIKE '%$val%' OR ";
            }
        }
        $q = substr($q,0,(strlen($q)-3)); 

        $getSearchDesignation = $this->model->getSearchDesignation($q);
        echo json_encode($getSearchDesignation);
    }
    
    public function getLocality()
    {
        $getLocality = $this->model->getLocality();
        echo json_encode($getLocality);
    }

    public function getState()
    {
        $id = $_POST['countryID'];

        $getStates = $this->model->getStates($id);
        echo json_encode($getStates);
    }

    public function getCity()
    {
        $id = $_POST['stateID'];

        $getCity = $this->model->getCity($id);
        echo json_encode($getCity);
    }

    public function getArea()
    {
        $id = $_POST['cityID'];

        $getAreas = $this->model->getAreas($id);
        echo json_encode($getAreas);
    }
    public function Request_Associations()
    {
        $loggedId = SessionHandling::get("loggedId");
        $preImgText = date('ymdhsi');

        $title = $_POST['associationsTitle'];

        $Folder_name = Urls::$ASSOCIATIONS;
                
        if($_FILES['Association-image']['name'] != '')
        {
            $Association_img_folder = $preImgText.$loggedId.'.jpg';

            $targetPath1 = $Folder_name.'/'.$preImgText.$loggedId.'.jpg';
         
            move_uploaded_file($_FILES['Association-image']["tmp_name"], $targetPath1);
        }
        else
        {
            $Association_img_folder = '';
        }

        $field = array('tAssociationName','nAssociationIconPath');

        $value = array($title,$Association_img_folder);

        $Types_data = array_combine($field, $value);

        $Types_data = $this->model->insert($Types_data, 'tblassociations');

        if(isset($Types_data) && $Types_data == 0)
        {
            echo "false";
        }
        else
        {
            echo 'true';
        }  
    }
    public function types()
    {
        $name = 'modules/agent/view/types.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getPropertyType();
        $this->view->data = $data;

        $this->view->render($name,"Property Types", $header, $footer);
    }

    public function subtypes()
    {
        $name = 'modules/agent/view/subtypes.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $getPropertyType = $this->model->getPropertyType();
        $this->view->getPropertyType = $getPropertyType;

        $data = $this->model->getPropertySubType();
        $this->view->data = $data;

        $this->view->render($name,"Property Sub Types", $header, $footer);
    }

    public function edit_type()
    {
        $name = 'modules/agent/view/types.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleTypes = $this->model->getSingleTypes($typeID);
        $this->view->singleTypes = $singleTypes;

        $data = $this->model->getPropertyType();
        $this->view->data = $data;

        $this->view->render($name,"Property Types", $header, $footer);
    }

    public function check_type()
    {
        $type = $_POST['type'];
        

        if ($_POST['type'] == 'submit') 
        {
            $result = $this->model->check_Type_Exist($type);
        }
        else if($_POST['type'] == 'update')
        {

            //print_r($_REQUEST);
            $typeID = $_POST['id'];
            $result = $this->model->check_Type_Exist_update($type,$typeID);
        }

        if($result)
        {
            echo "true";
        }
        else
        {
            echo "false";
        }
    }
    public function ImageCrop()
    {   
        // print_r($_REQUEST);
        // exit();
        

        $img_r = imagecreatefromjpeg($_GET['img']);
        $dst_r = ImageCreateTrueColor( $_GET['w'], $_GET['h'] );
         
        imagecopyresampled($dst_r, $img_r, 0, 0, $_GET['x'], $_GET['y'], $_GET['w'], $_GET['h'], $_GET['w'],$_GET['h']);
          
        header('Content-type: image/jpeg');
        imagejpeg($dst_r);

          exit;
    }
    public function changeImgProfile()
    {
        // print_r($_POST);
        // // print_r($_FILES);
        // exit();
        $loggedId = SessionHandling::get("loggedId");
        $id = array('nUserIDPK' => $loggedId);
        $Folder_name = Urls::$IMG_UPLOADS_USER;

        $targetPath1 = $Folder_name.'user_'.$loggedId.'.jpg';

        $updateProfile = "";
        
        if(isset($_POST["image"]))
        {
            $data = $_POST["image"];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);

            $data = base64_decode($image_array_2[1]);

            // move_uploaded_file($_FILES['Profile_image']["tmp_name"], $targetPath1);

            $imageName = 'user_'.$loggedId.'.jpg';

            file_put_contents($Folder_name.$imageName, $data);

            $field = array('tUserProfileImgPath');

            $value = array($imageName);
            $imagePath = urls::$BASE.$Folder_name.$imageName;

            $userData = array_combine($field, $value);
            // print_r($userData);
            // print_r($targetPath1);
            // exit();
            $updateProfile = $this->model->update($userData, 'tblusers' , $id);

        }

        if($updateProfile > 0)
        {
            echo $imagePath ;     
        }
        else if($updateProfile == 0)
        {
            echo $imagePath;         
        }
        else
        {
            echo "false";     
        }   
    }
    public function createProfile()
    {
        extract($_POST); 
        // print_r($_POST);
        // exit();
                    
        $loggedId = SessionHandling::get("loggedId");  
        $id = array('nUserIDFK' => $loggedId);
        $userID = array('nUserIDPK' => $loggedId);

        if($_POST['submit'] == "basic_Info")
        {   
            $agent = array('nAgentIDFK' => $agentID);
            // print_r($_POST);
            // exit();  
            if (isset($_POST) && !empty($_POST))
            {   
                $old_phone_field = array('eUserGender','vUserMiddleName');
                $old_Phone_value = array($gender,$MiddleName);
                $Basic_data = array_combine($old_phone_field, $old_Phone_value);

                $Basic_data = $this->model->update($Basic_data, 'tblusers',$userID);

                if (isset($OtherPhoneNo)) 
                {   
                    for ($i=0; $i < count($OtherPhoneNo) ; $i++) 
                    {
                        $getUserPhoneNumbers = $this->model->getUserPhoneNumbers($agentID,$OtherPhoneNo[$i]);

                        if($getUserPhoneNumbers <= 0)
                        {
                            $new_phone_field = array('vContactNumber','nAgentIDFK');
                            $new_Phone_value = array($OtherPhoneNo[$i],$agentID);
                            $Basic_data = array_combine($new_phone_field, $new_Phone_value);
                            $Basic_data = $this->model->insert($Basic_data, 'tblagentcontactnumbers');
                        }                        
                    }
                }
                //Update Social Link
                $field = array('tAgentLinkedinLink','tAgentFacebookLink','tAgentInstaLink','tAgentDescription');

                $value = array($linkedinID,$facebookID,$instagramID,$agentBio);

                $Basic_data = array_combine($field, $value);
                $Basic_data = $this->model->update($Basic_data, 'tblagents',$id);
                
                if($Basic_data > 0)
                {
                         echo "true";     
                }
                else if($Basic_data == 0)
                {
                        echo "true";         
                }
                else
                {
                   
                    echo "false";     
                }                   
            }
        }

        else if($_POST['submit'] == "profession")
        {    

            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);           


                if($companyID == '0' && $companyName != 'Individual')
                {
                    $field = array('tCompanyName');
                    $value = array($companyName); 

                    $companiesData = array_combine($field, $value);
                    $companyID = $this->model->insert($companiesData, 'tblcompanies');
                }
                else
                {
                    $companyID = $companyID;
                }

                if ($designationID == '0' && $designationName != 'N/A') 
                {
                    $field = array('tDesignationName');
                    $value = array($designationName); 

                    $designationsData = array_combine($field, $value);
                    
                    $designationID = $this->model->insert($designationsData, 'tbldesignations');
                }
                else
                {
                    $designationID = $designationID;
                }

                if ($reraID == "" && $reraID == null) 
                {
                    $bisReraid = 0;
                }
                else
                {
                    $bisReraid = 1;

                }

                $field = array('nAgentReraID','bIsAgentReraCertified','nDesignationIDFK','nAgentExperience','nCompanyIDFK','tAgentAddress','nCityIDFK','nStateIDFK','nCountryIDFK');
                
                $value = array($reraID,$bisReraid,$designationID,$experience,$companyID,$address1,$cityID,$stateID,$countryID);

                $Types_data = array_combine($field, $value);

                $Types_data = $this->model->update($Types_data, 'tblagents',$id);
                
                $agent = array('nAgentIDFK' => $agentID);

                $Basic_data = $this->model->delete($loggedId,'tblagentlocalitiesexpert','nAgentIDFK');
                if (!empty($locality)) 
                {
                    for ($i=0; $i <count($locality) ; $i++) { 
                       
                        $localityData = array('nAgentIDFK','nLocalityIDFK');

                        $localityValue = array($loggedId,$locality[$i]);

                        $localityData_Value = array_combine($localityData, $localityValue);

                        $localityData_Value = $this->model->insert($localityData_Value, 'tblagentlocalitiesexpert');
                    }
                    # code...
                }
                $Basic_data = $this->model->delete($loggedId,'tblagentlanguagesknown','nAgentIDFK');
                if (!empty($languages)) 
                {

                    for ($j=0; $j <count($languages) ; $j++) { 
                       
                        $languagesData = array('nAgentIDFK','nLanguageIDFK');

                        $languagesValue = array($loggedId,$languages[$j]);

                        $languagesData_Value = array_combine($languagesData, $languagesValue);
                        
                       
                        $languagesData_Value = $this->model->insert($languagesData_Value, 'tblagentlanguagesknown');
                    }
                }
                    $Basic_data = $this->model->delete($loggedId,'tblagentspecializations','nAgentIDFK');
                if (!empty($specializations)) 
                {

                    for ($k=0; $k <count($specializations) ; $k++) { 
                       
                        $specializationsData = array('nAgentIDFK','nSpecializationIDFK');

                        $specializationsValue = array($loggedId,$specializations[$k]);

                        $specializationsData_Value = array_combine($specializationsData, $specializationsValue);
                        
                        $specializationsData_Value = $this->model->insert($specializationsData_Value, 'tblagentspecializations');
                    }
                }
                    $Basic_data = $this->model->delete($loggedId,'tblagentassociations','nAgentIDFK');
                if (!empty($associations)) 
                {

                    for ($v=0; $v<count($associations) ; $v++) { 
                       
                        $associationsData = array('nAgentIDFK','nAssociationIDFK');

                        $associationsValue = array($loggedId,$associations[$v]);

                        $associationsData_Value = array_combine($associationsData, $associationsValue);
                        
                        $associationsData_Value = $this->model->insert($associationsData_Value, 'tblagentassociations');
                    }
                }
                if($Types_data > 0)
                {
                    echo "true";     
                }
                else if($Types_data == 0)
                {
                    echo "true";         
                }
                else
                {
                    echo "false";     
                }               
            }
        }
        else if($_POST['submit'] == "chagePassword")
        {
            // extract($_POST);
            // print_r($_POST);
            // exit();
            $UserID = array('nUserIDPK' => $loggedId);
            

            $field = array('tUserPassword');

            $value = array($newPassword);

            $Types_data = array_combine($field, $value);

            // print_r($Types_data);
            // exit();
            $Types_id = $this->model->update($Types_data,'tblusers',$UserID);
                     
            if($Types_id > 0)
            {
                     echo "true";     
            }
            else if($Types_id == 0)
            {
                    echo "true";         
            }
            else
            {
               
                echo "false";     
            }    
        }
        
    }
    public function createType()
    {
        $loggedId = SessionHandling::get("loggedId");  

        if($_POST['submit'] == "submit")
        {    
            if (isset($_POST) && !empty($_POST))
            {
                extract($_POST);                

                $field = array('tPropertyTypeName');

                $value = array($typeName);

                $Types_data = array_combine($field, $value);
    
                $Types_data = $this->model->insert($Types_data, 'tblpropertytypes');
                
                if(isset($Types_data) && $Types_data == 0)
                {
                    SessionHandling::set("err_msg", "Error while adding Types");
                    header("Location:types");
                }
                else
                {
                    SessionHandling::set("suc_msg", "Types added successully");
                    header("Location:types");
                }               
            }
        }
        else if($_POST['submit'] == "update")
        {
            extract($_POST);    

            $id = array('nPropertyTypeIDPK' => $typeID);

            $field = array('tPropertyTypeName');

            $value = array($typeName);

            $Types_data = array_combine($field, $value);

            $Types_id = $this->model->update($Types_data, 'tblpropertytypes',$id);
                     
            if($Types_id > 0)
            {
                SessionHandling::set("suc_msg", "Types updated successully");
                header("Location:types");              
            }
            else if($Types_id == 0)
            {
                SessionHandling::set("suc_msg", "Types updated successully");
                header("Location:types");              
            }
            else
            {
                SessionHandling::set("err_msg", "Error while updating Types");
                header("Location:types");  
            }    
        }
    }
    public function checkPassword()
    {
        extract($_POST);
        // print_r($_POST);
        // exit();
        $singleTypes = $this->model->checkPassword($oldPassword);
        echo json_encode($singleTypes);
    }
    public function delete_type()
    {
        $Types_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");  

        $id = array('nPropertyTypeIDPK' => $Types_id);

        $currentDate = date('Y-m-d H:i:s');

        $field = array('bIsPropertyTypeRemoved');
        $value = array(1);
        $Types_data = array_combine($field, $value);

        $res = $this->model->update($Types_data, 'tblpropertytypes',$id);
      
        if($res > 0)
        {
            SessionHandling::set("suc_msg", "Types deleted successully");
            echo "true";
        }
        else
        {
            SessionHandling::set("err_msg", "Error while deleting Types");
            echo "false";
        } 
    }

    

  
    public function delete_agent()
    {
        $agent_id = $_POST['id'];

        $loggedId = SessionHandling::get("loggedId");

        $id = array('nAgentIDPK' => $agent_id);

        $field = array('bIsRemoved');
        $value = array(1);

        $Agent_data = array_combine($field, $value);
        
        $res = $this->model->update($Agent_data, 'tblagents',$id);


        $idUser = array('nUserIDPK' => $_POST['userId']);

        $fieldUser = array('bIsUserRemoved');
        $valueUser = array(1);

        $User_data = array_combine($fieldUser, $valueUser);

        $this->model->update($User_data, 'tblusers',$idUser);

        if($res > 0)
        {
            // SessionHandling::set("suc_msg", "agent deleted successully");
            echo "true";
        }
        else
        {
            // SessionHandling::set("err_msg", "Error while deleting agent");
            echo "false";
        } 
    }
  
    public function sendOTPForVerification()
    {
        extract($_POST);
     
        $res = $this->model->sendOTPOverSMS($phone);
        print_r($res);
       
        if($res != "error")
        {           
            $loggedId = SessionHandling::get("loggedId");

            $id = array('nUserIDPK' => $loggedId);

            $field = array('nUserPhoneNumber','tVerificationOTP');
            $value = array($phone,$res);

            $otpData = array_combine($field, $value);

            $res = $this->model->update($otpData, 'tblusers',$id);


            $loggedId = SessionHandling::get("loggedId");  

            $id = array('nUserIDFK' => $loggedId);
            $field = array('tAgentLinkedinLink','tAgentFacebookLink','tAgentInstaLink','tAgentDescription');
            $value = array($linkedinID,$facebookID,$instagramID,$agentBio);
            $Basic_data = array_combine($field, $value);

            $Basic_data = $this->model->update($Basic_data, 'tblagents',$id);

        }
         exit();
        // return $res;
    }

    

    public function verifyOTP()
    {
        extract($_POST);
        // print_r($_POST);
        // exit();
        $combinedOTP = implode('',$otpValue);
        $loggedId = SessionHandling::get("loggedId");
        
        $getProfileData = $this->model->getProfileDetail($loggedId);

        $storedOTP = $getProfileData['tVerificationOTP'];

        if($storedOTP == $combinedOTP)
        {
            $loggedId = SessionHandling::get("loggedId");

            // print_r($combinedOTP);
            // exit();
            if(!empty($otherNumberOTP))
            {
                $id = array('nAgentContactNumberIDPK' => $otherNumberOTP);
            }
            else
            {
                 $id = array('nUserIDPK' => $loggedId);
            }
         

            $field = array('bIsMobileNumberVerified');
            $value = array(1);

            $otpData = array_combine($field, $value);

            // print_r($id);
            // exit();
            if(!empty($otherNumberOTP))
            {
                $res = $this->model->update($otpData, 'tblagentcontactnumbers',$id);
            }
            else
            {
                $res = $this->model->update($otpData, 'tblusers',$id);
            }

            echo "true";
        }
        else
        {
            echo "false";
        }
    }
}