<style type="text/css">
    .paddingLeft
    {
        padding-left: 0px !important;
    }
    .paddinTop
    {
        padding-top: 50px !important;
    }
    td img{
        width: 42px !important;
    }
     tbody>tr>:nth-child(8){
       width: 15%;
    }
    tr td span
    {
        text-align: center !important;
        vertical-align: middle !important;
    }
</style>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
   
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title "><i class="fa fa-home"></i> Agent / Developer Listing</h3>
                            </div>
                            
                        </div>
                    </div>
                    <div class="m-portlet__body">

                        <div class="table-responsive">
                            <!--begin: Datatable -->
                             <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                                <thead align="center">
                                    <tr>
                                        <th>#</th>
                                        <th width="10%">Image</th>
                                        <th>User Type</th>
                                        <th width="15%">Full Name</th>
                                        <th>Email ID</th>
                                        <th>Listing<br>Limit</th>
                                        <th>Verified?</th>
                                        <th width="15%">Featured?</th>
                                        <th>Action</th>
                                        <!-- <th>Listing<br>validity</th> -->
                                    </tr>
                                </thead>
                                <tbody align="center">
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" style="margin-top: 5%;" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <form method="POST" id="subscriptionForm">
        <input type="hidden" name="userID" id="userID"  value="<?php echo $this->data[$i]['nUserIDPK']; ?>">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                     <h4 class="modal-title">Subscription</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" name="PlansID" id="PlansID">
                        <div class="col-lg-4">
                            <label>Select Subscriptions Plan</label>
                            <select name="plans" style="width: 100%;" class="form-control m-input m-select2" id="plans">
                                
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label>Subscriptions Name</label>
                            <input type="text" class="form-control m-input" onkeypress="return isNumberKey(event);" name="Plan_Name" id="Plan_Name" readonly>
                        </div>
                        <div class="col-lg-4">
                            <label>Indian Price</label>
                            <input type="text" class="form-control m-input" name="Indian_Price" id="Indian_Price"  onkeypress="return isNumberKey(event);" readonly min="1">
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label>Valid For[For Day]</label>
                            <input type="text" class="form-control m-input" onkeypress="return isNumberKey(event);" name="subscritionDaysCount" id="subscritionDaysCount" readonly>
                        </div>
                        <div class="col-lg-4">
                            <label>Listing Limit</label>
                            <input type="text" class="form-control m-input" name="listing_limit" id="listing_limit"  onkeypress="return isNumberKey(event);" readonly value="<?php echo $this->data[$i]['nAddListingUpto'] ;?>" min="1">
                        </div>
                        <div class="col-lg-4">
                            <label>Expire On</label>
                            <input type="text" style="cursor: no-drop;"  class="form-control m-input" name="subscritionExpireOn" id="subscritionExpireOn" readonly>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" align="center">
                    <div class="col-lg-2"></div>
                    <button class="btn col-lg-4 btn-success increaseSubscription" type="submit">Submit</button>
                    <button type="button" class="btn btn-secondary col-lg-4" data-dismiss="modal">Close</button>
                    <div class="col-lg-2"></div>
                </div> 
            </div>
        </div>
    </form>
</div>
<div class="modal fade" style="margin-top: 5%;" id="FeatureAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <form method="POST" id="AgentForm">
        <input type="hidden" name="userID" id="userID" value="<?php echo $this->data[$i]['nUserIDPK']; ?>">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                     <h4 class="modal-title">Featured Agent</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label ></label>
                        <div class="col-lg-12">
                            <label id="agentName">Feature Agent Expire On[Days]</label>
                            <input type="text" onkeypress="return isNumberKey(event);" name="FeatureAgentDate" id="FeatureAgentDate" class="form-control m-input">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" align="center">
                    <div class="col-lg-2"></div>
                    <button class="btn col-lg-4 btn-success FeatureAgentDateSubmit" type="button">Submit</button>
                    <button type="button" class="btn btn-secondary col-lg-4 FeatureAgentDateCancel" data-dismiss="modal">Close</button>
                    <div class="col-lg-2"></div>
                </div> 
            </div>
        </div>
    </form>
</div>


<!-- end:: Body -->
<script type="text/javascript">

    $(document).ready(function() 
    {
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        loadDatatable();
        getAllSubscritionPlans();
       
         // $('#subscriptionModal').modal('show');
    });
    $('.datepicker').datepicker({
            autoclose:true,
            startDate : 'today',
            format:'yyyy-mm-dd'
        });
    $(document).on('click',".btn_edit", function(e) {
        alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();

    });  
    $(document).on('click',".btn_view", function(e) {
        //alert();
        e.preventDefault();

        var form = $(this).closest("form");

        //var v = form.find("input[name='RID']").val();
        form.submit();
    });

    $('.increaseSubscription').on('click', function(e) 
    {
       
        e.preventDefault();

        var form_data = $('#subscriptionForm').serialize();
    
            $.ajax({
                url: '<?php echo Urls::$BASE; ?>agent/update_listing_limit',
                type: "POST",
                data: form_data,
                success: function(data)
                {
                    if (data.trim() == "true") 
                    {   
                        loadDatatable();
                        toastr.success("Subscrition Updated Sucessfully", "Subscrition");
                        $('#subscriptionModal').modal('hide');                                       
                    }
                    else
                    {
                        toastr.error("Error While Updateing Subscrition", "Subscrition"); 
                    }
                    // mUtil.scrollTo("subscriptionForm", -200); 
                }
            });
    });
    $('#plans').on('change',function(){

        getSubcriptionPlans();

    });
    function VerifyAgent(id,emailID = null,fName = null,lName = null) 
    {   
        // console.log(emailID);
        var name = fName+' '+lName;
        
        swal({
            title:"Are you sure?",
            text:"You want to Verify this Agent ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {   

                    if($('#'+id).prop("checked") == true)
                    {
                        $('#'+id).val('1');
                        var status = $('#'+id).val();

                        $.ajax({
                            type: "POST",
                            url: "<?php echo Urls::$BASE ?>agent/verifiedFromAdmin",
                            data:{id:id,status:status,emailID:emailID,name:name},
                            success: function(data)
                            {        
                                if (data.trim() == "true") 
                                {   
                                    loadDatatable();
                                    toastr.success("Agent Verified Sucessfully", "Agent");
                                    // $('#subscriptionModal').modal('hide');              
                                }
                                else
                                {
                                    toastr.error("Error While Verifing Agent", "Agent"); 
                                }
                                // mUtil.scrollTo("subscriptionForm", -200);    
                            }
                        });
                    }
                }
                else
                {
                    loadDatatable();
                }  
           });
    }
    function FeaturedAgent(id,status,emailID = null,userName = null) 
    {   
        // alert(status);
        if(status == 1)
        {
            swal({
            title:"Are you sure?",
            text:"You want to Feature this Agent ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {   
                   $('#agentName').html('Feature Agent <b> '+userName+' </b> Exipre on [Days]');
                    $('#FeatureAgent').modal('show');
                    $('.FeatureAgentDateSubmit').on('click',function(e)
                    {
                        var expireOn = $('#FeatureAgentDate').val();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo Urls::$BASE ?>agent/FeaturedAgentUpdate",
                            data:{id:id,status:status,emailID:emailID,userName:userName,expireOn:expireOn},
                            success: function(data)
                            {        
                                if (data.trim() == "true") 
                                {   
                                    $('#FeatureAgent').modal('hide');
                                    loadDatatable();
                                    toastr.success("Featured Agent Added Sucessfully", "Agent");
                                }
                                else
                                {
                                    $('#FeatureAgent').modal('hide');
                                     loadDatatable();
                                    toastr.error("Error While Adding Featured Agent", "Agent"); 
                                }
                                mUtil.scrollTo("subscriptionForm", -50);    
                            }
                        });

                    });
                    $('.FeatureAgentDateCancel').on('click',function(e)
                    {
                        $('#FeatureAgent').modal('hide');
                        loadDatatable();
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
            
        }
        else if (status == 0)
        {
            
            swal({
            title:"Are you sure?",
            text:"You want to Un-Feature this Agent ?",
            type:"warning",
            showCancelButton:!0,
            confirmButtonText:"Yes,Make it!",
            cancelButtonText:"No, cancel!",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {   
                    // $('#agentName').html('Feature Agent <b> '+userName+' </b> Exipre on [Days]');
                    // $('#FeatureAgent').modal('show');
                    $.ajax({
                        type: "POST",
                        url: "<?php echo Urls::$BASE ?>agent/FeaturedAgentUpdate",
                        data:{id:id,status:status,emailID:emailID,userName:userName},
                        success: function(data)
                        {        
                            if (data.trim() == "true") 
                            {   
                                $('#FeatureAgent').modal('hide');
                                loadDatatable();
                                toastr.success("Featured Agent Added Sucessfully", "Agent");
                            }
                            else
                            {
                                $('#FeatureAgent').modal('hide');
                                loadDatatable();
                                toastr.error("Error While Adding Featured Agent", "Agent"); 
                            }
                            mUtil.scrollTo("subscriptionForm", -50); 
                        }
                    });
                }
                else
                {
                    loadDatatable();
                }    
           });
        }
       
        
    }
    function getAllSubscritionPlans() 
    {   
        var agent_id = $('#userID').val();
        var agentSubcrition = "";

        $.ajax({
            type: "POST",
            url: "<?php echo Urls::$BASE ?>agent/getAllSubscritionPlans",
            success: function(data)
            {    
                var plans = JSON.parse(data);

                var plansid = $('#PlansID').val();

                // console.log("plamid"+plansid);
                // getAllSubscritionPlans();
                $('#plans').html('');

                for (var i = 0; i < plans.length; i++) 
                {
                    if (plansid == plans[i].nSubscriptionIDPK) 
                    {
                        $('#plans').append("<option value="+plans[i].nSubscriptionIDPK+" selected>"+plans[i].tSubscriptionName+"</option>");
                    }   
                    else
                    {
                        $('#plans').append("<option value="+plans[i].nSubscriptionIDPK+">"+plans[i].tSubscriptionName+"</option>");
                    }
                }
                $("#plans").trigger("change");

            }

        });

    }

    function getSubcriptionPlans() 
    {   
         var id = $('#plans :selected').val();
            $.ajax({
            type: "POST",
            url: "<?php echo Urls::$BASE ?>agent/getSubscritionDetails",
            data: {id : id},
            success: function(data)
            {    
                var plans = JSON.parse(data);
                // console.log(plans);
                var tSubscriptionName = plans[0].tSubscriptionName
                var fSubscriptionPrice = plans[0].fSubscriptionPrice
                var fOtherCountrySubscriptionPrice = plans[0].fOtherCountrySubscriptionPrice
                var nListingLimit = plans[0].nListingLimit
                var dtSubscriptionExpireOn = plans[0].dtSubscriptionExpireOn
                var dtListingExpireOn = plans[0].dtListingExpireOn
                
                $('#Plan_Name').val(tSubscriptionName);
                $('#Indian_Price').val(fSubscriptionPrice);
                $('#other_Price').val(fOtherCountrySubscriptionPrice);
                $('#subscritionDaysCount').val(dtListingExpireOn);
                $('#listing_limit').val(nListingLimit);
                
                var daycount = dtSubscriptionExpireOn;

                if(daycount>0){
                    var date = new Date(); // Now
                    date.setDate(date.getDate() + parseInt(daycount)); // Set now + 30 days as the new date

                    var dd = date.getDate();

                    var mm = date.getMonth()+1; 
                    var yyyy = date.getFullYear();

                    var fullDate = dd+'-'+mm+'-'+yyyy;
                    // console.log(fullDate);

                    $('#subscritionExpireOn').val(fullDate);

                }
                // $('#subscritionExpireOn').val(dtListingExpireOn);

            }
        });

    }

    function loadDatatable() 
    {
        
        $('#s1').DataTable(
        {
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo Urls::$BASE ?>agent/getAgentDetails",
            "type": "POST",
            "scrollCollapse": true,
            "lengthMenu": [[10, 20, 50, 100, 200], [10, 20, 50, 100, 200]],  
            // start column 3 to asccending
            "columnDefs": 
            [
                {"orderable": false,"targets": [0,0]},
                {"orderable": false,"targets": [0,1]}, 
                {"orderable": false,"targets": [0,8]}                   
            ],
            "order":[[0,"desc"]],
            "destroy": true
        });
    }

    function setId (del_id,userID)
    {  
        swal({
        title:"Are you sure?",
        text:"You want to delete this Agent ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>agent/delete_agent",
                    data:{'id':del_id,userId:userID},
                    success: function(data)
                    {     
                    // toastr.success("Property Deleted Successfully","Property");
                      if(data == "true")
                      {
                        loadDatatable();
                        // location.href="<?php //echo Urls::$BASE ?>agent/";
                      }               
                    }
                });
            }   
       });
    } 

    function addsubscription(agent_id,listing_count)
    {   
        
        var array = JSON.parse("[" + agent_id + "]");
        // alert(array[1]);

        agent_id = array[0];
        listing_count = array[1];

        $('#PlansID').val('');   
        getAllSubscritionPlans();
        $('#userID').val(agent_id);

        $('#subscriptionModal').modal('show');
        $('#plans').select2();
        $.ajax({
            type: "POST",
            url: "<?php echo Urls::$BASE ?>agent/getAgentSubscritionDetails",
            data:{'id':agent_id},
            success: function(data)
            {    
                var plans = JSON.parse(data);
                console.log(plans);  
                $('#PlansID').val(plans[0].nSubscriptionIDFK);   
                getAllSubscritionPlans();
                           
                console.log($('#PlansID').val());
            }
        });
     
    }   
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }
</script>
