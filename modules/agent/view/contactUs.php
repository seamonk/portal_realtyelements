    <div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title "><i class="fa fa-home"></i> Agent</h3>
        </div>
        <div>
          
        </div>
    </div>
</div>


<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              <i style="font-size: 20px;" class="m-menu__link-icon fa fa-info-circle"> &nbsp;</i>
                               Agent Contact Us Listing
                            </h3>
                        </div>
                    </div>
                    
                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Agent Name</th>
                                <th>Customer Name</th>
                                <th>Customer Contact No</th>
                                 <th>Customer Email ID</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            // print_r($this->data);
                            if (!empty($this->data)) {

                                for ($i = 0; $i < count($this->data); $i++) {
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>

                                        <td><?php echo $this->data[$i]['vAgentFirstName']." ".$this->data[$i]['vAgentLastName']; ?></td>
                                        <td><?php echo $this->data[$i]['tCustomerName']; ?></td>
                                        <td><?php echo $this->data[$i]['vCustomerrMobileNo']; ?></td>
                                        <td><?php echo $this->data[$i]['tCustomerEmailId']; ?></td>
                                    </tr>
                                    <?php
                                }
                            }

                            ?>
                        </tbody>
                       </table></div></div></div></div></div></div>

                       <script type="text/javascript">
                               $(document).ready(function () {

        $('#s1').DataTable({
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "columnDefs": [
                {"width": "5%", "targets": 0}
            ]
        });

    });
                       </script>