<style type="text/css">

    .vpb_wrapper {
      max-width:120px;
      border: solid 1px #cbcbcb;
       background-color: #FFF;
       box-shadow: 0 0px 10px #cbcbcb;
      -moz-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-box-shadow: 0 0px 10px #cbcbcb;
      -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px;
      text-align:center;
      padding:10px;
      padding-bottom:3px;
      font-family:Verdana, Geneva, sans-serif;
      font-size:13px;
      line-height:25px;
      float:left;
      margin-right:20px; 
      margin-bottom:20px;
      word-wrap: break-word;
    }
    .vpb_wrapper:hover{ text-decoration:underline; color:#039;}

    .vpb_image_style{ width:100px; height:100px;-webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; border:0px solid;}
    .vpb_image_style img { width:100px; height:100px; border:0px solid;}

    label {
    font-weight: 400 !important;
    }
    /*.select2-selection__choice__remove {
      display: none !important;
    }*/
    /*.select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-selection__choice .select2-selection__choice__remove {
        color: #575962;
         display: none;
    }*/
    /*.select2-selection__choice__remove{
        display: none;
    }*/
    .m-card-profile .m-card-profile__pic img {
        text-align: center;
        max-width: 130px !important;
        margin: 0 auto !important;
        border-radius: 0% !important;
    }

    .bgColor {
        width: 100%;
        height: 150px;
        background-color: #fff4be;
        border-radius: 4px;
        margin-bottom: 30px;
    }

    .inputFile {
        padding: 5px;
        background-color: #FFFFFF;
        border: #F0E8E0 1px solid;
        border-radius: 4px;
    }

    .btnSubmit {
        background-color: #696969;
        padding: 5px 30px;
        border: #696969 1px solid;
        border-radius: 4px;
        color: #FFFFFF;
        margin-top: 10px;
    }

    #uploadFormLayer {
        padding: 20px;
    }

    input#crop {
        padding: 5px 25px 5px 25px;
        background: lightseagreen;
        border: #485c61 1px solid;
        color: #FFF;
        visibility: hidden;
    }

   
    .form-control-feedback{color: red;}

</style>
<link rel="stylesheet" href="<?php echo Urls::$ASSETS?>custom/croppie.css" type="text/css" />

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="m-portlet m-portlet--full-height">
                        <div class="m-portlet__body">
                            <div class="m-card-profile">
                                  
                                <div class="m-card-profile__title m--hide">
                                    Your Profile
                                </div>
                                <div class="m-card-profile__pic">
                                   
                                    <div  class="m-card-profile" id="profile_displa_preview">
                                        <?php
                                            if (empty($this->data['tUserProfileImgPath'])) {
                                                ?>
                                                     <img  id="profileImage" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.'user.jpg'?>">
                                                     <img  id="profile_Image_crop" src="">
                                                <?php
                                            }else{
                                                ?>
                                                    <img  id="profileImage" src="<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$this->data['tUserProfileImgPath'];; ?>">
                                                    <img  id="profile_Image_crop" src="">
                                                <?php
                                            }
                                        ?>
                                        
                                    </div>
                                    
                                    
                                    
                                    <span>
                                        
                                        <div style="margin-top: 5px;">  

                                            <form method="post" id="profile_form" name="profile_form" enctype = "multipart/form-data"> 
                                                <input style="display:none;" type="file"  accept="image/*" name="Profile_image" id="Profile_image" onchange="image_preview(this,'<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.$this->data['tUserProfileImgPath'];?>')" />
                                            </form>
                                            <?php
                                            if (!empty($this->data['tUserProfileImgPath'])) 
                                            {
                                                ?>  
                                             
                                                    <a id="Remove_click" onclick="remove_listing_image();" style="font-weight: 400; cursor: pointer !important;align-content: center;">Remove </a>
                                                 

                                                
                                                <?php
                                            }
                                            ?>
                                        <br>
                                            <i class="la la-camera" onclick="document.getElementById('Profile_image').click();" class="vpb_image_style" class="img-thumbnail" style="font-size: 2.6rem !important; cursor: pointer !important;"></i>
                                        </div><br>
                                    </span>
                                   
                                </div>
                                <button type="button" class="btn-primary" id="crop" value="CROP"  style="display: none;">CROP</button>
                                <div class="m-card-profile__details">
                                    <span class="m-card-profile__name"><?php if (SessionHandling::get('userName')) { echo SessionHandling::get('userName') ." ".SessionHandling::get('lastName');} else { echo "";} ?></span>
                                </div>
                            </div>
                            <div class="m-portlet__body-separator"></div>
                            <div class="m-widget1 m-widget1--paddingless">
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col-4">
                                            <h3 class="m-widget1__title">Views</h3>
                                            <span class="m-widget1__desc">Porfile</span>
                                        </div>
                                        <div class="col m--align-right">
                                           <span class="m-widget1__number m--font-danger"><?php if($this->data['nAgentTotalViews'] < 10){ echo $this->data['nAgentTotalViews']; }else{echo $this->data['nAgentTotalViews'];} ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">Profile</h3>
                                            <span class="m-widget1__desc">Completed</span>
                                        </div>
                                        <div class="col m--align-right">
                                             <span class="m-widget1__number m--font-brand">

                                                <?php
                                                if (SessionHandling::get('userType') == DEVELOPER) 
                                                {
                                                    $profileCOmpleatedcount = 10;
                                                    if (!empty($this->data['tUserProfileImgPath'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                         
                                                    }
                                                    if (!empty($this->data['vUserMiddleName'])) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->data['vUserEmailID'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 10; 
                                                         
                                                    }
                                                    if (!empty($this->data['eUserGender'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 10; 
                                                         
                                                    }
                                                       
                                                    if (!empty($this->data['tAgentDescription']) || !empty($this->data['tAgentLinkedinLink']) && !empty($this->data['tAgentFacebookLink']) && !empty($this->data['tAgentInstaLink']))
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 40; 
                                                         
                                                    }
                                                    if (!empty($this->data['nUserPhoneNumber']) || !empty($this->getAgentContact)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    else{
                                                            //echo $profileCOmpleatedcount.' %';
                                                    }
                                                }
                                                else if (SessionHandling::get('userType') == AGENT) 
                                                {
                                                    $profileCOmpleatedcount = 5;

                                                    if (!empty($this->data['tUserProfileImgPath'])) 
                                                    {
                                                         $profileCOmpleatedcount = 10;
                                                         
                                                    }
                                                    if (!empty($this->data['vUserMiddleName'])) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    }
                                                    if (!empty($this->data['vUserEmailID'])) 
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 15; 
                                                         
                                                    }
                                                       
                                                    if (!empty($this->data['tAgentDescription']) || !empty($this->data['tAgentLinkedinLink']) && !empty($this->data['tAgentFacebookLink']) && !empty($this->data['tAgentInstaLink']))
                                                    {
                                                        $profileCOmpleatedcount = $profileCOmpleatedcount + 5; 
                                                         
                                                    }
                                                    if (!empty($this->data['nAgentReraID'])) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->data['nUserPhoneNumber']) || !empty($this->getAgentContact)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getAgentLanguages)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getAgentAssociations)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getAgentSpecializations)) 
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 10;
                                                        
                                                    }
                                                    if (!empty($this->getLocality))
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    }
                                                    if (!empty($this->data['nCompanyIDFK']) && !empty($this->data['nDesignationIDFK']))
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    }
                                                   
                                                    if (!empty($this->data['nAgentExperience']))
                                                    {
                                                         $profileCOmpleatedcount = $profileCOmpleatedcount + 5;
                                                        
                                                    } 
                                                    else{
                                                            //echo $profileCOmpleatedcount.' %';
                                                    }
                                                }

                                                echo $profileCOmpleatedcount.'%';
                                                ?>

                                            </span>
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="m-portlet__body-separator"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8">
                    <div class="m-portlet m-portlet--full-height m-portlet--tabs">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                    <li class="nav-item m-tabs__item">
                                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                            <i class="flaticon-share m--hide"></i>
                                            Personal Details
                                        </a>
                                    </li>
                                        <?php
                                        $loginID = SessionHandling::get('userType');

                                        if ($loginID == AGENT) 
                                        {
                                            ?>
                                            <li class="nav-item m-tabs__item">
                                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                                    Profession Details
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                                Change Password
                                            </a>
                                        </li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="tab-content">
                            <?php if(!empty($this->data)) { ?>
                                <div class="tab-pane active" id="m_user_profile_tab_1">
                                    <form  class="m-form m-form--label-align-right" id="form_BasicInfo">

                                        <div class="m-portlet__body">
                                            <div class="form-group row">   
                                                <div class="col-lg-12" >
                                                    <a onclick="EditableAllControll('form_BasicInfo');" style="float: right;" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" id="EditControl" ><i class="fa fa-edit" style="float: right;"></i></a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="agentID" value="<?php echo $this->data['nAgentIDPK']; ?>">
                                            <div class="form-group row">

                                                <div class="col-lg-4">
                                                <label>First Name</label>
                                                    <input class="form-control m-input" id="FirstName" type="text" value="<?php echo $this->data['vUserFirstName']; ?>" disabled>
                                                </div>
                                            
                                                <div class="col-lg-4">
                                                    <label>Middle Name</label>
                                                    <input class="form-control m-input" id="MiddleName" name="MiddleName" type="text" value="<?php echo $this->data['vUserMiddleName']; ?>" disabled>
                                                </div>
                                           
                                                <div class="col-lg-4">
                                                    <label>Last Name</label>
                                                    <input class="form-control m-input" id="LastName" type="text" value="<?php echo $this->data['vUserLastName']; ?>" disabled>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                
                                                <div class="col-lg-4">
                                                    <label>Mobile No.</label>
                                                    <div class="m-input-icon m-input-icon--right">
                                                    <?php
                                                        if (!empty($this->data['nUserPhoneNumber'])) 
                                                        {
                                                            ?>  
                                                                <input class="form-control m-input" id="phoneNo" name="phoneNo" onkeypress="return isNumberKey(event);" type="text" oninput="checkmobileno('phoneNo');" value="<?php echo $this->data['nUserPhoneNumber']; ?>">
                                                                <span class="m-input-icon__icon m-input-icon__icon--right">
                                                                    <div class="row" style="padding-top: 10px;">
                                                                        <?php if($this->data['bIsMobileNumberVerified'] == 0) 
                                                                        { 
                                                                            ?>

                                                                           <div onclick="sendOTP('phoneNo')" class="add_Phone SendOTP btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Verify Your Number" style="cursor: pointer; padding: 2px;"><i style="font-size: 20px;" class="fa fa-check-circle"></i></div>

                                                                            <?php 
                                                                        }
                                                                        else
                                                                        {
                                                                            ?>
                                                                            <div onclick="sendOTP('phoneNo')" id="VerifiedNumberAgain" class="add_Phone btn" style="cursor: pointer; padding: 2px;"><i style="font-size: 20px;color: #28a088;" class="fa fa-check-circle"></i></div>

                                                                            <div onclick="AddPhoneNo()"style="cursor: pointer; float: right; padding: 2px;" class="btn m-btn- m-btn--outline-2x add_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Add New Number"><i class="fa fa-plus"></i></div>

                                                                            <?php
                                                                        } ?>
                                                                       
                                                                        
                                                                    </div>


                                                                </span>

                                                            <?php
                                                        }
                                                        else{
                                                            ?>
                                                            <input class="form-control m-input" id="phoneNo" name="phoneNo" onkeypress="return isNumberKey(event);" type="text"  oninput="checkmobileno('phoneNo');" value="<?php echo $this->data['nUserPhoneNumber']; ?>">
                                                            <span class="m-input-icon__icon m-input-icon__icon--right">

                                                            <div class="row" style="padding-top: 10px;">
                                                                <div onclick="sendOTP('phoneNo')"  class="add_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Verify Your Number" style="cursor: pointer; padding: 2px;"><i style="font-size: 18px;" class="fa fa-check-circle"></i></div>
                                                            </div>
                                                            </span>
                                                            <?php
                                                        }
                                                    ?>                                        
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-lg-5">
                                                    <label>Email ID</label>
                                                    <div class="m-input-icon m-input-icon--right">                                            
                                                       <input type="email" class="form-control m-input" name="EmailID" value="<?php echo $this->data['vUserEmailID']; ?>" oninput="checkExitsEmail('EmailID');" id="EmailID" placeholder="Email">
                                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                                            <div class="row" style="padding-top: 10px;">
                                                                <?php if($this->data['bIsEmailIdVerified'] == 0) 
                                                                { 
                                                                    ?>

                                                                   <div class="add_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Not Verified" style="cursor: pointer; float: right; padding: 2px; padding-left: 30px;">
                                                                        <i style="font-size: 20px;" class="fa fa-check-circle"></i></div>
                                                                    <?php 
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <div class="btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Number Verified" style="cursor: pointer;">
                                                                        <i style="font-size: 18px;float: right;padding-left: 10px;color: #28a088;" class="fa fa-check-circle"></i></div>

                                                                    <?php
                                                                } ?>
                                                                
                                                            </div>


                                                        </span>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-lg-3 col-12">
                                                    <label>Gender</label>
                                                    <select class="form-control m-select2" name="gender" id="gender">
                                                        <option disabled>--Select Gender--</option>
                                                        <option value="0" <?php if ($this->data['eUserGender'] == "0") { echo "selected";
                                                    }?> > Female </option>
                                                        <option value="1" <?php if ($this->data['eUserGender'] == "1") { echo "selected";
                                                    }?> > Male </option>
                                                        <option value="2" <?php if ($this->data['eUserGender'] == "2") { echo "selected";
                                                    }?> > Others </option>
                                                        <option value="3" <?php if ($this->data['eUserGender'] == "3") { echo "selected";
                                                    }?> > Rather Not Say </option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="form-group row" >
                                                <div class="col-lg-12" id="AddPhoneNo">

                                                   <?php
                                                        if (!empty($this->getAgentContact)) 
                                                        {
                                                            for ($i=0; $i < count($this->getAgentContact) ; $i++) { 
                                            
                                                            ?>
                                                                <div class="col-lg-4" id="removeDiv_<?php echo $i+1; ?>">
                                                                    <label>Alternative No.</label>
                                                                    <div class="m-input-icon m-input-icon--left">
                                                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                            <span>
                                                                                 <div  onclick="RemovePhoneNo('<?php echo $i+1; ?>','<?php echo $this->getAgentContact[$i]['nAgentContactNumberIDPK']; ?>','addPhone_<?php echo $i;?>')" class="btn m-btn- m-btn--outline-2x remove_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete This <?php echo $this->getAgentContact[$i]['vContactNumber']; ?> Number"><i class="fa fa-times"></i>
                                                                                 </div>
                                                                            </span>
                                                                        </span>               
                                                                        <input onkeypress="return isNumberKey(event);" oninput="checkOthermobileno('addPhone_<?php echo $i;?>');" id="addPhone_<?php echo $i;?>" name="OtherPhoneNo[]" class="form-control m-input" type="text" value="<?php echo $this->getAgentContact[$i]['vContactNumber']; ?>" required>
                                                                        <span class="m-input-icon__icon m-input-icon__icon--right">
                                                                            <div class="row" style="padding-top: 10px;">
                                                                                
                                                                                <?php if($this->getAgentContact[$i]['bIsMobileNumberVerified'] == 0) 
                                                                                { 
                                                                                    ?>
                                                                                    <div  onclick="sendOTP('addPhone_<?php echo $i;?>','<?php echo $this->getAgentContact[$i]['nAgentContactNumberIDPK']; ?>')"  class="add_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Verify Your Number" style="cursor: pointer; padding: 2px;"><i style="font-size: 18px;" class="fa fa-check-circle"></i></div>
                                                                                    <?php 
                                                                                }
                                                                                else
                                                                                {
                                                                                    ?>
                                                                                    <div class="add_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Number Verified" style="cursor: pointer; padding: 2px;">
                                                                                        <i style="font-size: 18px;color: #28a088;"  class="fa fa-check-circle"></i></div>

                                                                                    <?php
                                                                                } ?>
                                                                                   
                                                                                <div  onclick="updateOtherNumber('<?php echo $this->getAgentContact[$i]['nAgentContactNumberIDPK']; ?>','addPhone_<?php echo $i;?>')" class="btn m-btn- m-btn--outline-2x remove_Phone btn" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Update Your Number"  style="cursor: pointer; float: right; padding: 2px;"><i style="font-size: 12px;" class="fa fa-pencil-alt"></i></div>

                                                                            </div>
                                                                        </span>
                                                                    </div>

                                                                    
                                                                </div>
                                                               <?php
                                                            }
                                                        }
                                                   ?>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                <label>Bio</label>
                                                <textarea class="m-input summernote" name="agentBio" id="agentBio"><?php echo $this->data['tAgentDescription']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                            <div class="form-group row">
                                                <div class="col-12 ml-auto">
                                                    <h3 class="m-form__section">Social Media Links:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <label >Linkedin</label>
                                                    <input class="form-control m-input" id="linkedinID" type="text" value="<?php echo $this->data['tAgentLinkedinLink']; ?>" name="linkedinID">
                                                </div>  
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <label >Facebook</label>
                                                    <input class="form-control m-input" id="facebookID" type="text" value="<?php echo $this->data['tAgentFacebookLink']; ?>" name="facebookID">
                                                </div>
                                            </div>                                     
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <label >Instagram</label>
                                                    <input class="form-control m-input" id="instagramID" name="instagramID" type="text" value="<?php echo $this->data['tAgentInstaLink']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-6">
                                                    </div>
                                                    <div class="col-3">
                                                        <button type="submit" name="submit" id="btnSaveBasic" value="basic_Info" class="btn btn-accent m-btn m-btn--custom">Save changes</button>&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="hidden" name="submit" value="basic_Info">
                                                        <button type="reset" onclick="EditableAllControll('form_BasicInfo');" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane " id="m_user_profile_tab_2">
                                   <form  class="m-form m-form--label-align-right" id="form_Profession">
                                        <div class="m-portlet__body"> 
                                            <div class="form-group row">   
                                                <div class="col-lg-12" >
                                                    <a onclick="EditableAllControll('form_Profession');" style="float: right; padding: 5px;" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill" id="EditControl_form2" ><i class="fa fa-edit" style="float: right; padding: 5px;"></i></a>
                                                </div>
                                            </div>                                                                                          
                                            <div class="form-group row">
                                                <input type="hidden" name="agentID" value="<?php echo $this->data['nAgentIDPK']; ?>">
                                                <div class="col-lg-5">
                                                    <label >Company Name</label>
                                                    <?php

                                                    $companyIDData = '0';
                                                    $companyNameData = 'Individual';

                                                    $designationIDData = '0';
                                                    $designationNameData = 'N/A';


                                                    if($this->data['nCompanyIDFK'] != 0 AND $this->data['nCompanyIDFK'] != '')
                                                    {
                                                        $companyIDData = $this->data['nCompanyIDFK'];

                                                        $companyNameData = $this->data['tCompanyName'];
                                                    }

                                                    if($this->data['nDesignationIDFK'] != 0 AND $this->data['nDesignationIDFK'] != '')
                                                    {
                                                        $designationIDData = $this->data['nDesignationIDFK'];

                                                        $designationNameData = $this->data['tDesignationName'];
                                                    }

                                                    ?>

                                                    <input class="form-control m-input"  id="companyID" name="companyID" type="hidden" value="<?php echo $companyIDData; ?>">
                                                    <input class="form-control m-input" id="companyName" name="companyName" type="text" value="<?php echo $companyNameData; ?>">
                                                </div>
                                                <div class="col-4">
                                                    <label>Designation</label>
                                                    <input class="form-control m-input"  id="designationID" name="designationID" type="hidden" value="<?php echo $designationIDData; ?>">
                                                    <input class="form-control m-input" id="designationName" name="designationName" type="text" value="<?php echo $designationNameData; ?>">
                                                </div>
                                                <div class="col-3">
                                                    <label >Experience[Years]</label>
                                                   <input class="form-control m-input" name="experience" type="text" id="experience" placeholder="[Ex. 5 ]" onkeypress="return isNumberKey(event);" value="<?php if(!empty($this->data['nAgentExperience'])){echo $this->data['nAgentExperience']."";} else {echo "Individual Work" ;} ?>">
                                                </div>
                                            
                                            </div>
                                            <div class="form-group  row">
                                                <div class="col-12">
                                                    <label>Rera ID</label>
                                                    <input class="form-control m-input" name="reraID" id="reraID" type="text" value="<?php echo $this->data['nAgentReraID']; ?>">
                                                </div>
                                            </div>
                                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                            <div class="form-group row">
                                                <div class="col-12 ml-auto">
                                                    <h3 class="m-form__section">Office Address:</h3>
                                                </div>
                                            </div>
                                            <div class="form-group  row">
                                                
                                                <div class="col-lg-12">
                                                    <label >Address Line 1</label>
                                                    <input class="form-control" rows="2" name="address1" type="text" value="<?php echo $this->data['tAgentAddress']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group  row">
                                                <div class="col-lg-12">
                                                    <label >Address Line 2</label>
                                                     <input class="form-control" rows="2" name="address2" type="text" value="<?php echo $this->data['tAgentAddress']; ?>">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group  row">
                                                <div class="col-lg-4">
                                                    <label>Country</label>
                                                    <select class="form-control m-select2" name="countryID" id="countryID" style="width: 100%;">
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>State</label>
                                                    <select class="form-control m-select2" name="stateID" id="stateID" style="width: 100%;" >
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label>City</label>
                                                    <select class="form-control m-select2" name="cityID" id="cityID"  style="width: 100%;">
                                                    </select>
                                                   
                                                </div>
                                            </div>     
                                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                              <div class="form-group  row">
                                                <div class="col-lg-12">
                                                <label>The localities you are expert of</label>
                                                    <select class="form-control m-select2" name="locality[]" id="locality"  style="width: 100%;" multiple >
                                                    </select>
                                                </div>
                                            </div>                                
                                            <div class="form-group  row"> 

                                                <div class="col-lg-6">
                                                    <label >Languages you speak</label>
                                                    <select class="form-control m-select2" name="languages[]" id="languages" style="width: 100%;" multiple >
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label >Core Specializations</label>
                                                    <select class="form-control m-select2" name="specializations[]" id="specializations" style="width: 100%;" multiple >
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group  row">
                                                <div class="col-lg-6">
                                                    <label>Associations</label>
                                                    <select class="form-control m-select2" name="associations[]" id="associations" style="width: 100%;" multiple >
                                                    </select>
                                                    <span style="font-size: 12px;"> [ Eg. NAR, RAR, WRA etc. ] </span>
                                                </div>
                                                <div class="col-lg-1" id="addAssociations_hide">
                                                    <label>Add</label>
                                                    <a onclick="AddAssociations()" class="btn btn-sm btn-outline-metal m-btn- m-btn--outline-2x "><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-6">
                                                    </div>
                                                    <div class="col-3">
                                                        <button type="submit" name="submit" id="btnSaveProfession" value="profession" class="btn btn-accent m-btn m-btn--custom">Save changes</button>&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="hidden" name="submit" value="profession">
                                                        <button type="reset" onclick="EditableAllControll('form_Profession');" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane " id="m_user_profile_tab_3">
                                    <form  class="m-form m-form--label-align-right" id="form_ChangePassword">
                                        <div class="m-portlet__body">
                                            <input type="hidden" name="agentID" value="<?php echo $this->data['nAgentIDPK']; ?>">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>Enter Old Password</label>
                                                    <input autofocus type="password" class="form-control m-input" name="oldPassword" value=""  id="oldPassword">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>Enter New Password</label>
                                                    <input type="password" class="form-control m-input" name="newPassword" id="newPassword">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label>Re-Enter Your Password</label>
                                                    <input type="password" class="form-control m-input" name="ReEnterNewPassword" id="ReEnterNewPassword">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-portlet__foot m-portlet__foot--fit">
                                            <div class="m-form__actions">
                                                <div class="row">
                                                    <div class="col-6">
                                                    </div>
                                                    <div class="col-3">
                                                        <button type="submit" name="submit" id="btnChangePassword" value="chagePassword" class="btn btn-accent m-btn m-btn--custom">Save changes</button>&nbsp;&nbsp;
                                                    </div>
                                                    <div class="col-3">
                                                        <input type="hidden" name="submit"  value="chagePassword">
                                                        <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload & Crop Image</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-md-8">
                      <div id="image_demo" style="width:350px;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success crop_image">Crop & Upload Image</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" style="margin-top: 5%;" id="AddAssociations"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <form action="#" id="addAssociationsForm" name="addAssociationsForm" enctype = "multipart/form-data" method="POST">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Associations</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group  row">
                        <div class="col-lg-12">
                            <span class="" onclick="document.getElementById('Association-image').click();">
                                <div style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                    <input style="display:none;" type="file" name="Association-image" id="Association-image" onchange="Association_image_preview(this)" />

                                    
                                    <div align="center" id="Association-display-preview">
                                        <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label for="associationsTitle" class="form-control-label">Associations Title :</label>
                            <input type="text" class="form-control" id="associationsTitle" name="associationsTitle">
                        </div>
                    </div>
                </div>
                <div class="modal-footer" align="center">
                    <button class="btn btn-primary" type="submit" id="associationsSubmit">Send Request</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" style="margin-top: 5%;" id="sendOPTModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <form action="#" id="otpForm" name="otpForm" enctype = "multipart/form-data" method="POST">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enter OTP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">                   
                        <center><label style="text-align: center;color: green;" id="optNum"></label></center>
                        <br>
                        <br>   
                    <div class="form-group row"> 
                        <div class="col-lg-2"> 
                            <input type="hidden" name="otherNumberOTP" id="otherNumberOTP" value="">
                            <input type="hidden" name="MobileNumberOTP" id="MobileNumberOTP" value="">
                        </div>              
                        <div class="col-lg-2">
                            <input type="text" class="form-control" id="otp_1" name="otpValue[]" maxlength="1" onkeypress="return isNumberKey(event);" onkeyup="toUnicode('2')" autofocus>
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" id="otp_2"  name="otpValue[]" maxlength="1" onkeypress="return isNumberKey(event);" onkeyup="toUnicode('3')">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" id="otp_3"  name="otpValue[]" maxlength="1" onkeypress="return isNumberKey(event);" onkeyup="toUnicode('4')">
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" id="otp_4" name="otpValue[]" maxlength="1" onkeypress="return isNumberKey(event);">
                        </div>
                        <div class="col-lg-2"> 
                        </div>                        
                    </div>
                    
                    <div class="form-group row">
                        <br>
                        <div class="col-lg-10" align="center">
                            
                            <label style="text-align: center;color: green;" id="alertOTPMessage"></label>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer" align="center">
                    <button class="btn btn-primary" type="button" id="optSubmit">Verify</button>
                    <button type="button" class="btn btn-secondary" id="otp_resend">Re-send OTP</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- end:: Body -->

<!-- <script src="<?php echo Urls::$ASSETS?>custom/jquery.min.js"></script> -->
<script src="<?php echo Urls::$ASSETS?>custom/croppie.js"></script>

<script type="text/javascript">
    $(document).ready(function() 
    {   
        var image = '';
        $image_crop = $('#image_demo').croppie({
            enableExif: true,
            viewport: {
              width:200,
              height:200,
              type:'circle' //circle
            },
            boundary:{
              width:300,
              height:300
            }
        });

        

        

        // $('#Profile_image').on('change', function(){
            
        // });
        <?php 
            if(SessionHandling::get('redirect')){
            ?>
                
            <?php

            }
            else{
                ?>
                $("#form_BasicInfo *").prop("disabled", true);
                $(".add_Phone").css("display", "none");
                $(".remove_Phone").css("display", "none");
                $("#form_BasicInfo :submit").css("display", "none");
                $("#form_Profession :submit").css("display", "none");
                $(':input[type="reset"]').css('display','none');
                $('#addAssociations_hide').css('display','none');
                $('.summernote').summernote('disable');
                $("#form_Profession *").prop("disabled", true);
                var verify = '<?php echo $this->data['bIsMobileNumberVerified'];?>';
                if (verify == 1) 
                {
                    $('#VerifiedNumberAgain').css('display','block');
                    $('#VerifiedNumberAgain').css('pointer-events','none');
                    $('#VerifiedNumberAgain').prop('title','');
                }
            <?php
            }
        ?>
    
        $('#languages').select2();
        $('#specializations').select2();
        $('#locality').select2();
        $('#cityID').select2();
        $('#stateID').select2();
        $('#countryID').select2();
        $('#associations').select2();
        $('#gender').select2();


        getLanguages();
        getAssociations();
        getSpecializations();
        getLocality();
        getCountryList();
        var size;

        
        toastr.options = {
          "closeButton": true,
          "progressBar": true,
          "positionClass": "toast-top-center"
        };

        $('#agentBio').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });

        $('#countryID').on('change', function(){
            getStateList();
        });

        $('#stateID').on('change', function(){
            getCityList();
        });

        $("#companyName" ).autocomplete(
        {
            source: function (request, response) 
            {
                $("#companyID").val('0');
                $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/getSearchCompany",
                    type: "POST",
                    data: request,
                    dataType: 'json',
                    success: function (data) 
                    {
                        response($.map(data, function (el)
                        {
                            return {
                                label: el.tCompanyName,
                                value: el.nCompanyIDPK
                            };
                        }));
                    }
                });
            },
            select:function (e, ui) 
            {
                e.preventDefault(); // uncomment if you want to display name in place of id
                $("#companyName").val(ui.item.label); 
                $("#companyID").val(ui.item.value);// uncomment if you want to display name in place
            },
            focus: function(event, ui) 
            {
                event.preventDefault();
                $("#companyName").val(ui.item.label);
            }
        });

        $("#designationName" ).autocomplete(
        {
            source: function (request, response) 
            {
                $("#designationID").val('0');

                $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/getSearchDesignation",
                    type: "POST",
                    data: request,
                    dataType: 'json',
                    success: function (data) 
                    {
                        response($.map(data, function (el)
                        {
                            return {
                                label: el.tDesignationName,
                                value: el.nDesignationIDPK
                            };
                        }));
                    }
                });
            },
            select:function (e, ui) 
            {
                e.preventDefault(); // uncomment if you want to display name in place of id
                $("#designationName").val(ui.item.label); 
                $("#designationID").val(ui.item.value);// uncomment if you want to display name in place
            },
            focus: function(event, ui) 
            {
                event.preventDefault();
                $("#designationName").val(ui.item.label);
            }
        });

        $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
            return this.optional(element) || /[a-z]+/.test(value);
        }, "Must have at least one lowercase letter");
         
        /**
         * Custom validator for contains at least one upper-case letter.
         */
        $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
            return this.optional(element) || /[A-Z]+/.test(value);
        }, "Must have at least one uppercase letter");
         
        /**
         * Custom validator for contains at least one number.
         */
        $.validator.addMethod("atLeastOneNumber", function (value, element) {
            return this.optional(element) || /[0-9]+/.test(value);
        }, "Must have at least one number");
         
        /**
         * Custom validator for contains at least one symbol.
         */
        $.validator.addMethod("atLeastOneSymbol", function (value, element) {
            return this.optional(element) || /[!@#$%^&*()]+/.test(value);
        }, "Must have at least one symbol");

        $("#form_ChangePassword").validate({
            rules: {
                oldPassword: {
                    required: true
                },
                newPassword: {
                    required: true,
                    atLeastOneLowercaseLetter: true,
                    atLeastOneUppercaseLetter: true,
                    atLeastOneNumber: true,
                    atLeastOneSymbol: true,
                    minlength: 6,
                    maxlength: 15
                },
                ReEnterNewPassword: {
                    equalTo: "#newPassword",
                    required: true,
                    atLeastOneLowercaseLetter: true,
                    atLeastOneUppercaseLetter: true,
                    atLeastOneNumber: true,
                    atLeastOneSymbol: true,
                    minlength: 6,
                    maxlength: 15
                }
            }
        });
        $("#otpForm").validate({
            rules: {
                otpValue: {
                    required: true
                }
                
            }
        });
        $("#form_BasicInfo").validate({
            rules: {
                phoneNo: {
                    required: true,
                    minlength: 10,
                    maxlength: 15
                },
                EmailID: {
                    required: true,
                    email: true
                },
                OtherPhoneNo: {
                    required:true,
                    minlength: 8,
                    maxlength: 15
                }  
            }
        });
        $("#form_Profession").validate({
            rules: {
                companyName: {
                    required: true
                },
                designationName: {
                    required: true
                },
                experience: {
                    required:true
                },
                countryID: {
                    required:true
                },
                stateID: {
                    required: true
                },
                cityID: {
                    required: true
                },
                locality: {
                    required:true
                },
                languages: {
                    required: true
                },
                specializations: {
                    required: true
                },
                associations: {
                    required:true
                }  
            }
        });

        $("#addAssociationsForm").validate({
            rules: {
                associationsTitle: {
                    required: true
                }
            }
        });
    });
    // var size;
    var image = '';

    var verify = '<?php echo $this->data['bIsMobileNumberVerified'];?>';
    
    function image_preview(vpb_selector_,oldImgPath) 
    {
       
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {  
                   var reader = new FileReader();

                    reader.onload = function (e) 
                    {
                        image = e.target.result;
                        console.log(image);
                        $("#profileImage").attr("src",e.target.result);
                      $image_crop.croppie('bind', {
                        url: e.target.result
                      }).then(function(){
                        console.log('jQuery bind complete');
                      });
                    }
                    reader.readAsDataURL(file);
                    $('#uploadimageModal').modal('show');
               }
            }
            else {  return false; }
        });
        
    }

    $('.crop_image').click(function(e){

            $image_crop.croppie('result', {
              type: 'canvas',
              size: 'viewport'
            }).then(function(response){
                console.log('jQuery complete');
              jQuery.ajax({
                url: "<?php echo Urls::$BASE; ?>agent/changeImgProfile",
                type: "POST",
                data:{"image": response},
                success:function(data)
                {                       
                    $("#HeaderProfilePic").attr("src",data);
                    $("#profileImage").attr("src",data);
                    
                    location.reload();
                   
                    toastr.success("Profile Image Update Sucessfully", "Profile");
                    $('#uploadimageModal').modal('hide');
                }
              });
            })
    });

    $('#btnSaveBasic').on('click', function(e) 
    {
        //$("#btnSaveBasic").removeAttr("disabled");
        e.preventDefault();

        var form_data = $('#form_BasicInfo').serialize();

       
        console.log(verify);
        if (verify == 0) 
        {
            swal({
            title:"Verify Mobile Number",
            text:"Please first verify your mobile number ?",
            type:"error",
            showCancelButton:!0,
            confirmButtonText:"Ok",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    mUtil.scrollTop(-200); 
                }   
            }); 

                // mUtil.scrollTo("#form_BasicInfo", -200); 
        }
        else
        {
            if($('#form_BasicInfo').valid())
            {   

                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>agent/createProfile',
                    type: "POST",
                    data: form_data,
                    success: function(data)
                    {
                        if (data.trim() == "true") 
                        {   

                            toastr.success("Profile Updated Sucessfully", "Profile");    
                            EditableAllControll('form_BasicInfo');  
                            '<?php SessionHandling::unset_key('redirect'); ?>';                   
                            setTimeout(function(){  location.reload();  }, 1200);
                        }
                        else
                        {
                            toastr.error("Error While Updateing Profile", "Profile"); 
                        }
                        mUtil.scrollTo("form_BasicInfo", -200); 
                    }
                });
            }
            else
            {
                // $("#btnSaveBasic").attr("disabled", "disabled");
                mUtil.scrollTo("form_BasicInfo", -200);  
            }
        }

    });
    
    function remove_listing_image()
    {
        // console.log(id);
        swal({
        title:"Are you sure?",
        text:"You want to delete this Image ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE; ?>agent/delete_listing_image",
                    success: function(data)
                    {    
                        if(data == "user.png")
                        {   

                            $("#HeaderProfilePic").attr("src",'<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.'user.jpg'?>');
                            $("#profileImage").attr("src",'<?php echo Urls::$BASE.Urls::$IMG_UPLOADS_USER.'user.jpg'?>');
                            $("#Remove_click").css('display','none');
                            swal({
                                type:"success",
                                title:"Image deleted successully",
                                showConfirmButton:!1,timer:1500
                            });
                            setTimeout(function()
                            {
                                location.reload();  
                            }, 1500);
                        }               
                    }
                }); 
            }   
       }); 
    }
 
    $(document).on('focusout','#oldPassword', function(e)
    {
        var oldPassword = $('#oldPassword').val();
        if(oldPassword != "" && oldPassword != null)
        {
            $.ajax({
                url: '<?php echo Urls::$BASE; ?>agent/checkPassword',
                type: "POST",
                data: {oldPassword : oldPassword},
                success: function(data){
                    console.log(data);
                    if (data.trim() === "true")
                    {

                    }
                    else
                    {
                        $('#oldPassword').val('');
                        $('#oldPassword').focus();
                        toastr.error("Please Enter valid password !", "Profile");
                    }
                }   
            });
        }
    });

    $('#phoneNo').on('focusout',function()
    {
       

        console.log(verify);
        if (verify == 0) 
        {
            swal({
            title:"Verify Mobile Number",
            text:"Please first verify your mobile number ?",
            type:"error",
            showCancelButton:!0,
            confirmButtonText:"Ok",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    mUtil.scrollTop(-200); 
                    $('#VerifiedNumberAgain').css('display','block');
                    $('#optNum').html('OTP send On This '+newPhone+' number')
                    sendOTP('phoneNo');
                }   
            }); 
        }
        else
        {   
           var newPhone =  $('#phoneNo').val();
           var oldPhone =  '<?php echo $this->data['nUserPhoneNumber'];?>';
           if (newPhone != "") 
           {
                if (newPhone == oldPhone) 
                {

                }
                else
                {
                    $('#VerifiedNumberAgain').css('display','block');
                    $('#optNum').html('OTP send On This '+newPhone+' number')
                    sendOTP('phoneNo');
                }
           }
           else
           {
                // toastr.error('Please Enter Mobile number','Mobile Number');
                // $('#phoneNo').focus();
           }
        }

    });

    $('#btnChangePassword').on('click', function(e) 
    {   
        e.preventDefault();
       
        console.log(verify);
        if (verify == 0) 
        {
            swal({
            title:"Verify Mobile Number",
            text:"Please first verify your mobile number ?",
            type:"error",
            showCancelButton:!0,
            confirmButtonText:"Ok",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    mUtil.scrollTop(-200); 
                    setTimeout(function(){  location.reload(); },1200);
                }   
            }); 
        }
        else
        {
            if ($('#form_ChangePassword').valid()) 
            {   
                var form_data = $('#form_ChangePassword').serialize();
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>agent/createProfile',
                    type: "POST",
                    data: form_data,
                    success: function(data)
                    {
                        if (data.trim() == "true") 
                        {   
                            $('#form_ChangePassword')[0].reset();

                            toastr.success("Password Updated Sucessfully", "Profile");
                            '<?php SessionHandling::unset_key('redirect'); ?>';
                        }
                        else
                        {
                            toastr.error("Error While Updateing Password", "Profile");
                        } 
                        mUtil.scrollTo("form_ChangePassword", -200);     
                    }
                });
            } 
            else
            {
                mUtil.scrollTo("form_ChangePassword", -200);  
            }
        }
    });

    $('#btnSaveProfession').on('click', function(e) 
    {
        var form_data = $('#form_Profession').serialize();
        e.preventDefault();
        var verify = '<?php echo $this->data['bIsMobileNumberVerified'];?>';
        console.log(verify);
        if (verify == 0) 
        {
            swal({
            title:"Verify Mobile Number",
            text:"Please first verify your mobile number ?",
            type:"error",
            showCancelButton:!0,
            confirmButtonText:"Ok",
            reverseButtons:!0}).then(function(e)
            {
                if(e.value)
                {
                    mUtil.scrollTop(-200); 
                }   
            }); 
        }
        else
        {
            if ($('#form_Profession').valid()) 
            {
                $.ajax({
                    url: '<?php echo Urls::$BASE; ?>agent/createProfile',
                    type: "POST",
                    data: form_data,
                    success: function(data)
                    {
                        if (data.trim() == "true") 
                        {   
                            toastr.success("Profile Updated Sucessfully", "Profile");
                            EditableAllControll('form_Profession');
                            setTimeout(function(){  location.reload(); },1200);
                            '<?php SessionHandling::unset_key('redirect'); ?>';
                        }
                        else
                        {
                            toastr.error("Error While Updateing Profile", "Profile");
                        }
                        mUtil.scrollTo("form_Profession", -200);  
                    }   
                });
            }
            else
            {
                mUtil.scrollTo("form_Profession", -200);  
            }
        }
    });


    $('#associationsSubmit').on('click', function(e) 
    {
        e.preventDefault();

        var form = $('#addAssociationsForm')[0];

        var filesdata = $('input[type=file]')[0].files;

        var data = new FormData(form);

        // If you want to add an extra field for the FormData
        data.append("file", filesdata);

        if($('#addAssociationsForm').valid())
        {
            $.ajax({
                url: "<?php echo Urls::$BASE; ?>agent/Request_Associations",
                type: "POST",
                data: data,
                contentType: false,
                cache: false,  
                processData:false,
                enctype: 'multipart/form-data',
                success: function(res)
                {   
                    toastr.success("Request sent successully", "Association");
                    $('#addAssociationsForm')[0].reset();
                    $('#AddAssociations').modal('hide');
                    getAssociations();
                }
            });
        }
    });

    function EditableAllControll(id) {

        if ($("#"+id+" *").prop("disabled") == true) 
        {   
            $("#"+id+" *").prop("disabled", false);

            if (id == "form_BasicInfo") 
            {
                $('#EditControl').css('display','none');
                $(".add_Phone").css("display", "block");

                var verify = '<?php echo $this->data['bIsMobileNumberVerified'];?>';
                if (verify == 1) 
                {
                    $('#VerifiedNumberAgain').css('display','block');
                    $('#VerifiedNumberAgain').css('pointer-events','none');
                }
                $(".remove_Phone").css("display", "block");
                $("#FirstName").prop("disabled", true);
                $("#LastName").prop("disabled", true);
                $('.summernote').summernote('enable');
            }
            else{
                $('#EditControl_form2').css('display','none');
                 $('#addAssociations_hide').css('display','block');
            }
            $("#"+id+" :submit").css("display", "block");
            $('#'+id+' :input[type="reset"]').css('display', 'block');
            
            // $('.select2-container--default .select2-selection--multiple .select2-selection__rendered .select2-selection__choice .select2-selection__choice__remove').attr('style', 'display: block');

        }
        else
        {   
            $("#"+id+" *").prop("disabled", true);
            if (id == "form_BasicInfo") 
            {
                $('#EditControl').css('display','block');
                $(".add_Phone").css("display", "none");
                $(".remove_Phone").css("display", "none");
                $("#FirstName").prop("disabled", true);
                $("#MiddleName").prop("disabled", true);
                $("#LastName").prop("disabled", true);
                $('.summernote').summernote('disabled');
                
            }
            else{

                $('#addAssociations_hide').css('display','none');
                $('#EditControl_form2').css('display','block');
            }
            $("#"+id+" :submit").css("display", "none");

            $('#'+id+' :input[type="reset"]').css('display', 'none');
           
            $('.summernote').summernote('disable');

            // $('span .select2-selection__choice__remove').attr('style', 'display: block');
        
        }
   
    }

    
    function getCountryList()   // fetch all country list insert / update
    {
        $.ajax({
            url: '<?php echo Urls::$BASE; ?>location/getCountry',
            type: 'POST',
            success: function (data) {
                $('#countryID').empty();
                $('#countryID').append('<option value="" disabled>-- Select Country --</option>');
                var country = JSON.parse(data);
                for (var i = 0; i < country.length; i++) {
                    var data1 = "<?php if (isset($this->data)) {
                        echo $this->data['nCountryIDFK'];
                    } else {
                        echo NULL;
                    } ?>";
                    if (data1 == country[i].nCountryIDPK)
                    {
                        $('#countryID').append('<option value=' + country[i].nCountryIDPK + ' selected>' + country[i].tCountryName + '</option>');
                    }
                    else
                    {
                        $('#countryID').append('<option value=' + country[i].nCountryIDPK + '>' + country[i].tCountryName + '</option>');
                        
                    }
                }
                $("#countryID").trigger("change");
            }
        }).done(function () {
            getStateList();
        });
    }

    function getStateList() // fetch state list on the basis of the country id
    {
        var countryID = $('#countryID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getState',
            type: 'POST',
            data: {countryID: countryID},
            success: function (data) {
                // console.log(data);
                $('#stateID').empty();
                $('#stateID').append('<option value="" disabled>-- Select State --</option>');
                var state = JSON.parse(data);
                for (var i = 0; i < state.length; i++) {

                    var data2 = "<?php if (isset($this->data)) {
                        echo $this->data['nStateIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == state[i].nStateIDPK)
                    {
                        $('#stateID').append('<option value=' + state[i].nStateIDPK + ' selected>' + state[i].tStateName + '</option>');
                    }
                    else
                    {
                        $('#stateID').append('<option value=' + state[i].nStateIDPK + '>' + state[i].tStateName + '</option>');
                    }
                }
                $("#stateID").trigger("change");
            }

        }).done(function () {
            getCityList();
        });
    }

    function getCityList() // fetch state list on the basis of the country id
    {
        var stateID = $('#stateID').val();

        $.ajax({
            url: '<?php echo Urls::$BASE ?>location/getCity',
            type: 'POST',
            data: {stateID: stateID},
            success: function (data) {
                // console.log(data);
                $('#cityID').empty();
                $('#cityID').append('<option value="" disabled>-- Select City --</option>');
                var city = JSON.parse(data);
                for (var i = 0; i < city.length; i++) {

                    var data2 = "<?php if (isset($this->data)) {
                        echo $this->data['nCityIDFK'];
                    } else {
                        echo NULL;
                    } ?>";

                    if (data2 == city[i].nCityIDPK)
                    {
                        $('#cityID').append('<option value=' + city[i].nCityIDPK + ' selected>' + city[i].tCityName + '</option>');
                    }
                    else
                    {
                        $('#cityID').append('<option value=' + city[i].nCityIDPK + '>' + city[i].tCityName + '</option>');
                    }
                }
                $("#cityID").trigger("change");
            } 
        });
    }

    function getLanguages() 
    {
        $.ajax({
        url: '<?php echo Urls::$BASE; ?>agent/getLanguages',
        type: 'POST',
        success: function(data) 
        {
            $('#languages').empty();
            $('#languages').append('<option value="" disabled>-- Select Languages --</option>');

            var languages = JSON.parse(data);  

            var languagesArray = Array();

            var selectedLanguages = <?php if (!empty($this->getAgentLanguages)) {echo json_encode($this->getAgentLanguages);} else {echo "0";}  ?>;

            for(var z = 0; z < selectedLanguages.length; z++)
            { 
                languagesArray.push(selectedLanguages[z].nLanguageIDFK);
                console.log(selectedLanguages[z].nLanguageIDFK);
            }

            for(var i = 0; i < languages.length; i++)
            { 
                if($.inArray(languages[i].nLanguageIDPK, languagesArray) != -1) {

                    $('#languages').append('<option value='+languages[i].nLanguageIDPK+' selected>'+languages[i].tLanguageName+'</option>');
                } 
                else 
                {
                    $('#languages').append('<option value='+languages[i].nLanguageIDPK+'>'+languages[i].tLanguageName+'</option>');
                }
            }
            
            $("#languages").trigger("change");
        }
        });
    }
  
    function getAssociations() 
    {
            // alert(id);
            $.ajax({
            url: '<?php echo Urls::$BASE; ?>agent/getAssociations',
            type: 'POST',
            success: function(data) 
            {
                $('#associations').empty();
                $('#associations').append('<option value="" disabled>-- Select Associations --</option>');


                var associations = JSON.parse(data);  

                //console.log(country.length);
                var locationArray = Array();
               
                var selectedLocality =  <?php if (!empty($this->getAgentAssociations)) {echo json_encode($this->getAgentAssociations);} else {echo "0";}?>;

                for(var z = 0; z < selectedLocality.length; z++)
                { 
                    locationArray.push(selectedLocality[z].nAssociationIDFK);
                }

                for(var i = 0; i < associations.length; i++)
                { 
                    if($.inArray(associations[i].nAssociationIDPK, locationArray) != -1) {

                     $('#associations').append('<option value='+associations[i].nAssociationIDPK+' selected>'+associations[i].tAssociationName+'</option>');
                    } 
                    else 
                    {
                      $('#associations').append('<option value='+associations[i].nAssociationIDPK+'>'+associations[i].tAssociationName+'</option>');
                    }
                }
                // $("#associations").trigger("change");
            }
        });
    }
    
    function getSpecializations() 
    {
           
            $.ajax({
            url: '<?php echo Urls::$BASE; ?>agent/getSpecializations',
            type: 'POST',
            success: function(data) 
            {
                $('#specializations').empty();
                $('#specializations').append('<option value="" disabled>- Select Specializations -</option>');

                var specializations = JSON.parse(data);

                var specializationsArray = Array();
                var selectedSpecializations = <?php if (!empty($this->getAgentSpecializations)) {echo json_encode($this->getAgentSpecializations);} else {echo "0";}  ?>;

                for(var z = 0; z < selectedSpecializations.length; z++)
                { 
                    specializationsArray.push(selectedSpecializations[z].nSpecializationIDFK);
                    console.log(selectedSpecializations[z].nSpecializationIDFK);
                }
                for(var k = 0; k < specializations.length; k++)
                { 
                    if($.inArray(specializations[k].nSpecializationIDPK, specializationsArray) != -1) {

                      $('#specializations').append('<option value='+specializations[k].nSpecializationIDPK+' selected>'+specializations[k].tSpecializationName+'</option>');
                    } 
                    else 
                    {
                       $('#specializations').append('<option value='+specializations[k].nSpecializationIDPK+'>'+specializations[k].tSpecializationName+'</option>');
                    }
                }
            }
            });
    }

    function getLocality() 
    {
            // alert(id);
            $.ajax({
            url: '<?php echo Urls::$BASE; ?>agent/getLocality',
            type: 'POST',
            success: function(data) 
            {
                $('#locality').empty();
                $('#locality').append('<option value="" disabled>Select Localities</option>');

                var locality = JSON.parse(data); 

                var locationArray = Array();

                var selectedLocality = <?php if (!empty($this->getLocality)) {echo json_encode($this->getLocality);} else {echo "0";}  ?>;
                for(var z = 0; z < selectedLocality.length; z++)
                { 
                    locationArray.push(selectedLocality[z].nLocalityIDFK);
                    
                }
                // console.log(locationArray);

                for(var i = 0; i < locality.length; i++)
                { 
                    if($.inArray(locality[i].nLocalityIDPK,locationArray) != -1) {

                      $('#locality').append('<option value='+locality[i].nLocalityIDPK+' selected>'+locality[i].tLocalityName+'</option>');
                    } 
                    else 
                    {
                      $('#locality').append('<option value='+locality[i].nLocalityIDPK+'>'+locality[i].tLocalityName+'</option>');
                    }
                }
                
            }
            });
    }

    var count = 1;
    count = '<?php if (!empty($this->getAgentContact)) {echo count($this->getAgentContact) + 1 ; } else { echo '1'; }?>';
    
    function AddPhoneNo() 
    {
        count = Number(count);
        if (count <= 3) {

            $('#AddPhoneNo').append('<div class="col-lg-4" id="removeDiv_'+count+'">\
                                        <label>Alternative No.</label>\
                                        <div class="m-input-icon m-input-icon--right">\
                                            <input onkeypress="return isNumberKey(event);" id="addPhone_'+count+'" name="OtherPhoneNo[]" oninput="checkOthermobileno(\'\addPhone_'+count+'\');" class="form-control m-input" type="text" value="" required>\
                                            <span class="m-input-icon__icon m-input-icon__icon--right">\
                                                <span>\
                                                    <a onclick="removeRow('+count+')" class="btn m-btn- m-btn--outline-2x remove_Phone"\
                                                    >\
                                                        <i class="fa fa-times"></i>\
                                                    </a>\
                                                </span>\
                                            </span>\
                                    </div></div>');
            count = Number(count) + 1 ;
        } 
        else
        {
            toastr.error("Max limit reached", "Mobile No.");
        }
    }


    function checkmobileno(id) 
    {
       var MobileNumber = $('#'+id).val();
       if (MobileNumber != "" && MobileNumber != null) 
       {
           $.ajax({
                url: "<?php echo Urls::$BASE; ?>agent/findMobileNumber",
                type: "POST",
                data: {'number':MobileNumber},              
                success: function(res)
                {   
                   if(res.trim() == "true")
                   {
                        $('#'+id).val('');
                        toastr.error('Mobile number already exits','Mobile Number');
                        // $('#'+id).attr('required',true);
                        $('#'+id).focus();
                        // EditableAllControll('form_BasicInfo');
                   }
                }
            });
       }
       else
       {
            toastr.error('Please Enter Mobile number','Mobile Number');
            $('#'+id).focus();
       }

    }
    function checkExitsEmail(id) 
    {
       var EmailID = $('#'+id).val(); 
       $.ajax({
            url: "<?php echo Urls::$BASE; ?>agent/checkExitsEmail",
            type: "POST",
            data: {'email':EmailID},              
            success: function(res)
            {   
               if(res.trim() == "true")
               {
                    $('#'+id).val('');
                    toastr.error('Email ID already exits','Email ID');
                    // $('#'+id).attr('required',true);
                    $('#'+id).focus();
                    // EditableAllControll('form_BasicInfo');
               }
            }
        });

    }
    function checkOthermobileno(id) 
    {
       
       var MobileNumber = $('#'+id).val(); 
       $.ajax({
            url: "<?php echo Urls::$BASE; ?>agent/checkOthermobileno",
            type: "POST",
            data: {'number':MobileNumber},              
            success: function(res)
            {   
               if(res.trim() == "true")
               {
                    $('#'+id).val('');
                    toastr.error('Mobile number already exits','Mobile Number');
                    // $('#'+id).attr('required',true);
                    $('#'+id).focus();
                    // EditableAllControll('form_BasicInfo');
               }
            }
        });

    }

    function removeRow(countDelete)
    {
        $('#removeDiv_'+countDelete).remove();
        count = Number(count) - 1 ; 
    } 

    function RemovePhoneNo(countDelete,id = "",divID = "") 
    {
        //alert(count);
       
        var data = $('#'+divID).val();
        // alert(data);

        swal({
        title:"Are you sure?",
        text:"You want to delete this mobile number "+ data +" ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, Delete it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/deleteOtherNumber",
                    type: "POST",
                    data: {'id':id},              
                    success: function(res)
                    {   
                       if(res.trim() == "true")
                       {
                            $('#removeDiv_'+countDelete).remove();
                            count = Number(count) - 1 ; 

                            toastr.success('Your number has been deleted');
                            EditableAllControll('form_BasicInfo');
                       }
                       else
                       {
                            toastr.error('Error while deleting your number');  
                            EditableAllControll('form_BasicInfo'); 
                       } 
                    }
                });
            }   
       }); 


        //alert(count);
    }
    function updateOtherNumber(id,divID) 
    {   
        var data = $('#'+divID).val();
        // alert(data);

        swal({
        title:"Are you sure?",
        text:"You want to update your mobile number "+ data +" ?",
        type:"warning",
        showCancelButton:!0,
        confirmButtonText:"Yes, Update it!",
        cancelButtonText:"No, cancel!",
        reverseButtons:!0}).then(function(e)
        {
            if(e.value)
            {
                $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/updateOtherNumber",
                    type: "POST",
                    data: {'id':id,'otherNumber':data},              
                    success: function(res)
                    {   
                       if(res.trim() == "true")
                       {
                            toastr.success('Your Mobile Number Updated Successully!');
                            setTimeout(function(){  location.reload();  }, 1500);
                            // EditableAllControll('form_BasicInfo');
                                                      
                       }
                       else
                       {
                            toastr.error('Error while updating your number');  
                            EditableAllControll('form_BasicInfo'); 
                       } 
                    }
                });
            }   
       }); 

     
    }
    

    function Association_image_preview(vpb_selector_)
    {
        var id = 1, last_id = last_cid = '';
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#Association-display-preview').html(''); 
                   
                   var reader = new FileReader();
                   
                   reader.onload = function(e) 
                   {
                       $('#Association-display-preview').append(
                       ' \
                       <img style="" class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br />  \
                       ');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });
    }
    
    function AddAssociations() 
    {
        $('#AddAssociations').css('display' ,'block');
        $('#AddAssociations').modal('show');
    } 

    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return !(charCode > 31 && (charCode < 48 || charCode > 57) && charCode !=46);
    }

    var phone = '';
    var MobileNumber= '';

    function sendOTP(id,otherPhoneNumberId = "") 
    {
       
        phone = $('#'+id).val();
        // alert($('#'+id).val());
        if ($('#'+id).valid())
        {
            $('#otherNumberOTP').val(otherPhoneNumberId);
            $('#optNum').html('OTP send On This '+phone+' number')
            $("#MobileNumberOTP").val(phone);
            $('#sendOPTModal').modal('show');
            var agentBio = $("#agentBio").val();
            var linkedinID = $("#linkedinID").val();
            var facebookID = $("#facebookID").val();
            var instagramID = $("#instagramID").val();
            MobileNumber = phone;   

            $.ajax({
                url: "<?php echo Urls::$BASE; ?>agent/sendOTPForVerification",
                type: "POST",
                data: {'phone':phone,'agentBio':agentBio,'linkedinID':linkedinID,'facebookID':facebookID,'instagramID':instagramID},
                success: function(res)
                {   
                    
                }
            });

        }
        else
        {
            toastr.error('Please Enter valid mobile number first');
        }

    }

    function reSendOTP(MobileNo) 
    {
        $.ajax({
            url: "<?php echo Urls::$BASE; ?>agent/sendOTPForVerification",
            type: "POST",
            data: {'phone':MobileNo},              
            success: function(res)
            {  
                $('#alertOTPMessage').text('OTP Re-send successfully.');
                $('#alertOTPMessage').fadeIn('slow', function()
                {
                    $('#alertOTPMessage').delay(5000).fadeOut(); 
                }); 
            }
        });
    }

    function toUnicode(elmnt)
    {
        $('#otp_'+elmnt).focus();
    }
    $('#otp_resend').on('click',function(e)
    {
            // alert(MobileNumber);
        reSendOTP(MobileNumber);

    });
    $('#optSubmit').on('click', function(e) 
    {
        e.preventDefault();

        var form = $('#otpForm').serialize();

        if($('#otpForm').valid())
        {
            $.ajax({
                    url: "<?php echo Urls::$BASE; ?>agent/verifyOTP",
                    type: "POST",
                    data: form,              
                    success: function(res)
                    {   
                       if(res.trim() == "true")
                       {
                            $('#sendOPTModal').modal('hide');
                            toastr.success('Your mobile number '+ phone +' has been verified successully!');
                            $('#AddPhoneNo').css('display','block');
                            $('#phoneNo').attr('disabled',true);
                            $('.SendOTP').removeAttr("onclick");
                            verify = 1;
                            // setTimeout(function(){  location.reload(); }, 2000);
                            // EditableAllControll('form_BasicInfo');
                       }
                       else{
                            // setTimeout(function(){  location.reload();  }, 1500);
                            toastr.error('Please enter valid OTP!');
                            $('#otpForm')[0].reset();
                            // $('#otp_1')focus();

                       } 
                    }
                });      
       }
       else
        {
            mUtil.scrollTo("otpForm", -200);  
        }
    });
    

</script>
   

