<!-- 01 :  start Define on urls.php -->


/*-----START : SMS Gateway Details---------*/


define ( 'SMS_API', 'uqRDm7M08kO1D75YdT9U9Q' ) ;

define ( 'SMS_API_URL', 'https://www.smsgatewayhub.com/api/mt/SendSms' ) ;

define ( 'SMS_API_USER_NAME', 'vikas.agenpoint' ) ;

define ( 'SMS_API_PASSWORD', '742215' ) ;

define ( 'SMS_API_SENDERID', 'SMSTST' ) ;

define ( 'MESSAGE_FOR_OTP_OVER_SMS', 'is OTP to verify your mobile at AgenPoint. Donot share it with anyone' ) ;

define ( 'MESSAGE_FOR_OTP_OVER_EMAIL', 'is OTP to verify your email address at AgenPoint. Donot share it with anyone' ) ;

define ( 'RESULT_FORMAT_CSV', 'csv' ) ;
define ( 'RESULT_FORMAT_JSON', 'json' ) ;

define ('APPOINTMENT_RESPONSE_MESSAGE',"We are awaiting the confirmation from<BR><b style='color:#01b2e7'>#1</b><BR><BR>You can track the progress of this booking in your Appointment history.<BR><BR>Your appointment ID is<BR><b style='color:#01b2e7'>#2</b>");


/*-----START : SMS Gateway Details---------*/


<!-- 0 :  End -->


<!-- 02 :  call function SendOTP -->

var phone = '';
var MobileNumber= '';

function sendOTP(id,otherPhoneNumberId = "") 
{
    $('#sendOPTModal').modal('show');
   
    phone = $('#'+id).val();

   <!--  $('#otherNumberOTP').val(otherPhoneNumberId);  -->
    $("#MobileNumberOTP").val(phone);  
    MobileNumber = phone; 

        $.ajax({
            url: "<?php echo Urls::$BASE; ?>agent/sendOTPForVerification",
            type: "POST",
            data: {'phone':phone},              
            success: function(res)
            {   
                
            }
        });
}


 <!-- 02 :  End-->

<!-- 03 : create sendOTPForVerification function on particuler module Controller file  -->

    public function sendOTPForVerification()
    {
        extract($_POST);
     
        $res = $this->model->sendOTPOverSMS($phone);
        print_r($res);
       
        if($res != "error")
        {           
            $loggedId = SessionHandling::get("loggedId");

            $id = array('nUserIDPK' => $loggedId);

            $field = array('nUserPhoneNumber','tVerificationOTP');
            $value = array($phone,$res);

            $otpData = array_combine($field, $value);

            $res = $this->model->update($otpData, 'tblusers',$id);            
        }
         exit();
        // return $res;
    }

<!-- 03 : End-->


<!-- 04 : create sendOTPIOverSMS function on particuler module mmodel file -->
function sendOTPOverSMS ( $mobile ) {

    $OTP = rand ( 1000, 9999 ) ;
    $message            = $OTP . " " . MESSAGE_FOR_OTP_OVER_SMS;         
 
    $fields = http_build_query([
        'APIKey'        => SMS_API,
        'senderid'      => 'SMSTST',
        'channel'       => 2,
        'DCS'           => 0,
        'flashsms'      => 0,
        'number'        => $mobile,
        'text'          => $message
    ]);
    
    $url = SMS_API_URL.'?'.$fields;
  
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_USERAGENT => 'Codular Sample cURL Request'
    ]);
    // Send the request & save response to $resp
    $resp = curl_exec($curl);
    // Close request to clear up some resources
    curl_close($curl);

    $resp = json_decode($resp);
   

    if($resp->ErrorCode == "000")
    {
       return $OTP; 
    }
    else
    {
        echo "error";
    }

}

<!-- 04 : End -->


<!-- 05: Resend OTP On Button Click -->

function reSendOTP(MobileNo) 
{
    $.ajax({
        url: "<?php echo Urls::$BASE; ?>agent/sendOTPForVerification",
        type: "POST",
        data: {'phone':MobileNo},              
        success: function(res)
        {  
            $('#alertOTPMessage').text('OTP Re-send successfully.');
            $('#alertOTPMessage').fadeIn('slow', function()
            {
                $('#alertOTPMessage').delay(5000).fadeOut(); 
            }); 
        }
    });
}

    <!-- 05 : End -->
