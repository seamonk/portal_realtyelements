

<style>
    .m-portlet .m-portlet__body {
        padding: 0.7rem 1.7rem !important;
    }
   /* input {
    text-transform: capitalize; ;
    }
    textarea {
    text-transform: capitalize; ;
    }*/
</style>

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->	
    <div class="row">
        <div class="col-lg-12">
            <div class="m-accordion m-accordion--default" id="m_accordion_1" role="tablist">
            <div class="m-accordion__item">
              <div class="m-accordion__item-head collapsed" role="tab" id="m_accordion_1_item_1_head" data-toggle="collapse" href="#m_accordion_1_item_1_body" aria-expanded="false">
                  <span class="m-accordion__item-icon">
                      <i class="fa flaticon-edit-1"></i>
                  </span>
                  <span class="m-accordion__item-title"> 
                    
                          <?php if (!empty($this->singleservices)) {
                              echo "Update Services";
                          } else {
                              echo "Add Services";
                          } ?>
                     
                    </span>
                  <span class="m-accordion__item-mode"></span>
              </div>
                         <?php if (!empty($this->singleservices)) {
                             ?>
                                <div class="m-accordion__item-body collapse show" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php

                          } else {
                              ?>
                                <div class="m-accordion__item-body collapse" id="m_accordion_1_item_1_body" class=" " role="tabpanel" aria-labelledby="m_accordion_1_item_1_head" data-parent="#m_accordion_1">
                             <?php
                          } ?>
              
                  <div class="m-accordion__item-content">
                    <form id="m_form" class="m-form m-form--label-align-right" enctype ="multipart/form-data"
                          action="<?php echo Urls::$BASE; ?>services/createservices"
                          method="POST">

                        <div class="m-portlet__body">
                            <?php if (!empty($this->singleservices)) {
                                ?>
                                <input type="hidden" name="nServiceIDPK" value="<?php echo $this->singleservices[0]['nServiceIDPK']; ?>">
                                <div class="form-group row">
                                  <div class="col-lg-12">
                                      <label>Services Title</label>
                                      <input type="text"
                                             class="form-control m-input"
                                             name="title" id="title" 
                                             value="<?php echo $this->singleservices[0]['tServiceName']?>">
                                  </div>
                                  
                                </div>
                               
                            <?php } else { ?>
                              <div class="form-group row">
                                <div class="col-lg-12">
                                    <label>Services Title</label>
                                    <input type="text"
                                           class="form-control m-input"
                                           name="title" id="title" 
                                           value="">
                                </div>
                               
                              </div>
                            <?php } ?>

                        </div>
                        <?php if (!empty($this->singleservices)) {

                            ?>
                            <div class="s-portlet__foot s-portlet__foot--fit col-lg-12 row">
                              <div class="col-lg-2"></div>
                                <div class="s-form__actions col-lg-4" style="text-align: center;">
                                    <button type="submit" class="btn btn-danger btn-block" value="update" name="submit">Update
                                         Services
                                    </button>
                                </div>
                                <div class="s-form__actions col-lg-4">
                                  <button type="reset" onclick="window.location = '<?php echo Urls::$BASE; ?>services/';" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                    </button>
                                   
                                	
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                        <?php } else { ?>
                            <div class="s-portlet__foot s-portlet__foot--fit row">
                              <div class="col-lg-2"></div>
                                <div class="s-form__actions col-lg-4" style="text-align: center;">
                                    <button type="submit" class="btn btn-danger btn-block" value="submit" name="submit">Add
                                         Services
                                    </button>
                                </div>
                                <div class="s-form__actions col-lg-4">
                                    
                                	 <button type="reset" class="btn btn-secondary btn-block" value="cancel" name="cancel">Cancel
                                    </button>
                                </div>
                                <div class="col-lg-2"></div>

                            </div>
                           
                        <?php }
                        ?>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div></div></div>
     <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              
                                Services Listing
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead align="center">
                            <tr>
                                <th>#</th>
                                <th>Services  Title</th>
                               
                                <th width="19%">Actions</th>
                            </tr>
                            </thead>
                            <tbody  align="center">
                            <?php
                            if (!empty($this->data)) {
                                for ($i = 0; $i < count($this->data); $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        
                                         
                                         <td><?php echo $this->data[$i]['tServiceName']; ?></td>
                                        
                                        <td>
                                            <!-- <a class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash"></i></a> -->
                                            <form method="post" style="display: inline-table;"
                                                  action="<?php echo Urls::$BASE ?>services/editservices">
                                                <input type="hidden" name="serviceID"
                                                       value="<?php echo $this->data[$i]['nServiceIDPK']; ?>">
                                                       <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Edit Services" type="button
                                                       " class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-pencil-alt"></i></button>
                                               
                                            </form>

                                            <button data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Delete Services" type="button" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"  onclick="setId(<?php echo $this->data[$i]['nServiceIDPK']; ?>)"><i class="fa fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

       
        
        $('#s1').DataTable({
            "lengthMenu": [[5,10, 25, 50, 100, -1], [5,10, 25, 50, 100, "All"]],
            "columnDefs": [
                { "width": "5%", "targets": 0 }
              ]
        } );
        $("#m_form").validate({
            rules: {
                title: {
                    required: true
                }
            }
        }); 

    });
   function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this Services ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>services/deleteservices",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>services/";
                        }
                    }
                });
            }
        });
    }
    
</script>