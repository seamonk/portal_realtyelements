<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Services extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/services/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllservices();
        $this->view->data = $data;

        $this->view->render($name, "Services", $header, $footer);
    }

    public function createservices() // insert services
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tServiceName');
                $value = array($title); 

                $servicesData = array_combine($field, $value);
                // print_r($servicesData);
                // exit();
                $servicesID = $this->model->insert($servicesData, 'tblservices');

                if (isset($servicesID) && $servicesID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Services");
                    header("Location:../services/");
                } else {
                    SessionHandling::set("suc_msg", "Services added successfully");
                    header("Location:../services/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update services
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nServiceIDPK' => $nServiceIDPK);

            
                $field = array('tServiceName');
                $value = array($title); 

                $servicesData = array_combine($field, $value);
                // print_r($servicesData);
                // exit();
                $servicesID = $this->model->update($servicesData, 'tblservices',$id);

            if (isset($servicesID) && $servicesID > 0) {
                SessionHandling::set("suc_msg", "Services updated successfully");
                header("Location:../services/listing");
            } else if (isset($servicesID) && $servicesID == 0) {
                SessionHandling::set("suc_msg", "Services updated successfully");
                header("Location:../services/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Services");
                header("Location:../services/listing");
            }
        }
    }

    public function listing()  // services listing
    {
        $name = 'modules/services/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllservices();
        $this->view->data = $data;

        $this->view->render($name, "Services Listing", $header, $footer);

    }

    public function editservices()   // edit services
    {
        $name = 'modules/services/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleservicesData = $this->model->fetchSingleservices($serviceID);
        $this->view->singleservices = $singleservicesData;
         $data = $this->model->getAllservices();
        $this->view->data = $data;

        $this->view->render($name, "Update Services", $header, $footer);
    }
    public function viewservices()   // edit services
    {
         
        extract($_POST);
        $singleservicesData = $this->model->fetchSingleservices($Pid);
        echo json_encode($singleservicesData);

       
    }
    

    public function deleteservices() // delete services
    {
        $servicesID = $_POST['id'];

        $id = array('nServiceIDPK' => $servicesID);

        $field = array('bIsServiceRemoved');
        $value = array(1);

        $servicesData = array_combine($field, $value);

        $res = $this->model->update($servicesData, 'tblservices', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Services deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Services");
            echo "false";
        }
    }

}