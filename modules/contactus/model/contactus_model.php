<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:26 PM
 */

class contactus_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($data, $table)
    {
        $columnString = implode(',', array_keys($data));
        $valueString = ":" . implode(',:', array_keys($data));

        $sql = "INSERT INTO  " . $table . " (" . $columnString . ") VALUES (" . $valueString . ")";

        $query = $this->db->prepare($sql);

        foreach ($data as $key => $val) {

            // $val = strtoupper($val);
            $query->bindValue(':' . $key, $val);
        }

        $insert = $query->execute();
        if ($insert > 0) {
            return $this->db->lastInsertId();         
        }
        return 0;
    }

    public function delete($id,$table,$whereId)
    {
        //$table = 'tblproduct';
        $sql = "DELETE FROM  " . $table . " WHERE ".$whereId." = $id";
        $query = $this->db->prepare($sql);
        $delete = $query->execute();
        if($delete)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getContactUsDetails()
    {
        $sth = $this->db->prepare("SELECT
                                      `nContactUsIDPK`,
                                      `tUserName`,
                                      `vUserMobileNo`,
                                      `tCustomerEmailId`,
                                      `tComment`,
                                      `dtCreatedOn`,
                                      `bIsRemoved`
                                  FROM
                                      `tblcontactus`
                                  WHERE
                                        bIsRemoved = 0
                                    
                                      ");

            // print_r($sth);
            // exit();
        $sth->execute();
        $row = $sth->fetchAll();
    // print_r($row);
    // exit(); 
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function getrefundDetails()
    {

        $sth = $this->db->prepare("SELECT
                                      `nRefundIDPK`,
                                      `tCustomerFirstName`,
                                      `tCustomerLastNname`,
                                      `tProductNname`,
                                      `vCustomerMobileNo`,
                                      `tCustomerEmailId`,
                                      `tCustomerOrderNo`,
                                      `tCustomerReason`,
                                      `tCustomerQuery`,
                                      `dtCreatedOn`,
                                      `bIsRemoved`
                                    FROM
                                      `tblcontactusrefund`
                                  ");

      /*  print_r($sth);
        exit();*/
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }

    public function fetchshipingDetails()
    {
        // SELECT `nCityIDPK`, `tCityName`, `nStateIDFK`, `nCountryIDFK`, `dLatitude`, `dLongitude`, `bIsRemoved` FROM `tblcity` WHERE 1
        $sth = $this->db->prepare("SELECT
                                      `nShippingIDPK`,
                                      `tCustomerFirstName`,
                                      `tCustomerLastNname`,
                                      `vCustomerMobileNo`,
                                      `tCustomerEmailId`,
                                      `tCustomerOrderNo`,
                                      `tCustomerQuery`,
                                      `dtCreatedOn`,
                                      `bIsRemoved`
                                    FROM
                                      `tblcontactusshiping`
                                    ");
        // print_r($sth);
        // exit();
        $sth->execute();
        $row = $sth->fetchAll();
        if (!empty($row)) {
            return $row;
        } else {
            return NULL;
        }
    }



}