<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Contactus extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
//        Session::init();
    }

    public function index()
    {
        $name = 'modules/contactus/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        // $id = $_POST['userID'];
        // print_r($id);
        // exit();
        $data = $this->model->getContactUsDetails();
        // print_r($data);
        // exit();
        $this->view->data = $data;

        $this->view->render($name, "Contactus Listing", $header, $footer);
    }

    public function exchange()
    {
        $name = 'modules/contactus/view/exchange.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';
        // $id = $_POST['userID'];
        // print_r($id);
        // exit();
        $userList = $this->model->getexchangeDetails();
        $this->view->data = $userList;

        $this->view->render($name, " exchange Listing", $header, $footer);
    }


    public function refund()
    {
        $name = 'modules/contactus/view/refund.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        // extract($_POST);

        $data = $this->model->getrefundDetails();

        $this->view->data = $data;

        $this->view->render($name, "refund Listing", $header, $footer);
    }


    public function shiping()
    {
        $name = 'modules/contactus/view/shiping.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        // extract($_POST);

        $data = $this->model->fetchshipingDetails();

        $this->view->data = $data;

        $this->view->render($name, "shiping Listing", $header, $footer);
    }

   

}