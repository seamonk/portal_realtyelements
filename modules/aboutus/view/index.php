<style>
    .m-portlet .m-portlet__body {
        padding: 0.7rem 1.7rem !important;
    }
   /* input {
    text-transform: capitalize; ;
    }
    textarea {
    text-transform: capitalize; ;
    }*/
</style>

<!-- END: Subheader -->
<div class="m-content">

    <!--Begin::Section-->	
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <i class=""></i>
                            <h3 class="m-portlet__head-text">
                                <?php if (!empty($this->singleaboutus)) {
                                    echo "Update About Us";
                                } else {
                                    echo "Add About Us";
                                } ?>
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="<?php echo Urls::$BASE; ?>aboutus/listing"
                                   class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only" data-toggle="m-tooltip"
                                   data-placement="top" data-original-title="About Us Listing">
                                    <i class="fa fa-list"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <!--begin::Form-->
                    <form id="m_form" class="m-form m-form--label-align-right" enctype ="multipart/form-data"
                          action="<?php echo Urls::$BASE; ?>aboutus/createaboutus"
                          method="POST">

                        <div class="m-portlet__body">
                            <?php if (!empty($this->singleaboutus)) {
                                ?>
                                <input type="hidden" name="nAboutUsIDPK" value="<?php echo $this->singleaboutus[0]['nAboutUsIDPK']; ?>">
                                
                                <div class="form-group col-lg-12">


                                                                               
                                    <label><h5>Select About Us Banner Image</h5></label>
                                    
                                    <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                        <span class="" onclick="document.getElementById('banner_image').click();">
                                             <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                        </span>
                                    </div>
                                    <br>
                                    <input style="display:none;" type="file" name="banner_image"  id="banner_image" onchange="vpb_image_preview(this)">

                                    <div style="width:100%;" align="center" id="vpb-display-preview">
                                       
                                       <img id="imgProduct" width="100px;" src="<?php echo Urls::$BASE.Urls::$ABOUTUS.$this->singleaboutus[0]['tAboutUsBannerImage']?>" >

                                    </div>
                                </div>

                                <div class="form-group row">
                                  <div class="col-lg-12">
                                      <label>About Us Title</label>
                                      <input type="text"
                                             class="form-control m-input"
                                             name="title"
                                              value="<?php echo $this->singleaboutus[0]['tAboutUsHeaderTitle']?>" required>
                                  </div>
                                  <div class="col-lg-12">

                                    <label>Description</label>
                                    <textarea name="description" class="form-control m-input" required><?php echo $this->singleaboutus[0]['tAboutUsDescription']?></textarea>

                                </div>
                              </div>
             
                              
                                

                            <?php } else { ?>

                              <div class="col-lg-12">
                                                                               
                                    <label><h5>Select About Us Banner Image</h5></label>
                                    
                                    <div align="center" style="padding: 10px;border: 2px dashed #ebedf2;border-radius: 10px;">
                                        <span class="" onclick="document.getElementById('banner_image').click();">
                                             <i class="fa fa-camera-retro" style="font-size: 11.1rem !important;"></i>
                                        </span>
                                    </div>
                                    <br>
                                    <input type="hidden" name="deleteImg" id="deleteImg">
                                    <input type="hidden" name="deleteImgs" id="deleteImgs">

                                    <input style="display:none;" type="file" name="banner_image"  id="banner_image" onchange="vpb_image_preview(this)">

                                    <div style="width:100%;" align="center" id="vpb-display-preview">
                                       
                                    </div>
                                </div>
                               <div class="form-group row">
                                  <div class="col-lg-10">
                                      <label>About Us Title</label>
                                      <input type="text"
                                             class="form-control m-input"
                                             name="title"
                                             value="" required>
                                  </div>
                                  <div class="col-lg-10">

                                    <label>Description</label>
                                    <textarea name="description" class="form-control m-input" required=""
                                              ></textarea>

                                </div>
                              </div>

                                

                            <?php } ?>

                        </div>
                        <?php if (!empty($this->singleaboutus)) {

                            ?>
                            <div class="s-portlet__foot s-portlet__foot--fit col-lg-12 row">
                              <div class="col-lg-2"></div>
                                <div class="s-form__actions col-lg-4" style="text-align: center;">
                                    <button type="submit" class="btn btn-danger btn-block" value="update" name="submit">Update
                                         About Us
                                    </button>
                                </div>
                                <div class="s-form__actions col-lg-4">
                                    <button type="reset" class="btn btn-secondary btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>aboutus/listing';" value="cancel" name="cancel">Cancel
                                    </button>
                                </div>
                                <div class="col-lg-2"></div>
                            </div>
                        <?php } else { ?>
                            <div class="s-portlet__foot s-portlet__foot--fit row">
                              <div class="col-lg-2"></div>
                                <div class="s-form__actions col-lg-4" style="text-align: center;">
                                    <button type="submit" class="btn btn-danger btn-block" value="submit" name="submit">Add
                                         About Us
                                    </button>
                                </div>
                                <div class="s-form__actions col-lg-4">
                                    <button type="reset" class="btn btn-secondary btn-block" onclick="window.location = '<?php echo Urls::$BASE; ?>aboutus/listing';"  value="cancel" name="cancel">Cancel
                                    </button>
                                	
                                </div>
                                <div class="col-lg-2"></div>

                                </div>
                           
                        <?php }
                        ?>
                    </form>
                    <!--end::Form-->
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#discountType').select2();

        $('#startDate').datepicker({
            autoclose: true,
            startDate: 'today',
            format: 'yyyy-mm-dd'
        });

        $('#endDate').datepicker({
            autoclose: true,
            startDate: 'today',
            format: 'yyyy-mm-dd'
        });
        $("#m_form").validate({
            rules: {
                banner_image: {
                    required: true
                },
                title: {
                    required: true
                },
                description: {
                    required:true
                }  
            }
        });

    });
    function vpb_image_preview(vpb_selector_) 
    {

       
        $.each(vpb_selector_.files, function(vpb_o_, file)
        {
            if (file.name.length>0) 
            {
                if (!file.type.match('image.*')) { return true; } // Do not add files which are not images
                else
                {
                    //Clear previous previewed files and start again
                   $('#vpb-display-preview').html('');
                    
                   var reader = new FileReader();

                   
                   reader.onload = function(e) 
                   {
                    console.log('#vpb-display-preview_0');

                       $('#vpb-display-preview').append(
                       '<div id="selector_'+vpb_o_+'" class="vpb_wrapper"> \
                       <img id="image'+vpb_o_+'" width="100px;" name="image[0][] class="vpb_image_style" class="img-thumbnail" src="' + e.target.result + '" \
                       title="'+ escape(file.name) +'" /><br /> \
                       <a  style="cursor:pointer;padding-top:5px;" title="Click here to remove '+ escape(file.name) +'" \
                       onclick="vpb_remove_selected(\''+vpb_o_+'\',\''+file.name+'\')">Remove</a> \
                       </div>');
                   }
                   reader.readAsDataURL(file);
               }
            }
            else {  return false; }
        });

        
    }
    function vpb_remove_selected(id,name)
    {
        var get_value = $('#deleteImg').val();

        if(get_value == '')
        {
            $('#deleteImg').val(name);
        }
        else
        {
            $('#deleteImg').val(get_value+','+name);
        }

         var ds = Array();
        ds.push(id);
        // delete [ds];
        var deta = Array();
        deta.push($('#deleteImg').val());

        $('#deleteImgs').val(deta);
        $('#selector_'+id).html("");
        $('#banner_image').val('');
        document.getElementById('selector_'+id).style.display = "none";
        console.log(deta);

        console.log(ds);
       
        // $('#v-add-'+id).remove();
       
}

</script>