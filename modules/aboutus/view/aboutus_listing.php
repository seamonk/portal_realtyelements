
<div class="m-content">

    <!--Begin::Section-->

    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Aboutus Listing
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="<?php echo Urls::$BASE; ?>aboutus/"
                                   class="btn btn-secondary m-btn m-btn--icon m-btn--icon-only" data-toggle="m-tooltip"
                                   data-placement="top" data-original-title="Add About Us">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="s1">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>AboutUs Header <br>Title</th>
                                <th>AboutUs Description</th>
                                <th>AboutUs Banner <br>Image</th>
                                <th width="19%">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($this->data)) {
                                for ($i = 0; $i < count($this->data); $i++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i + 1; ?></td>
                                        
                                         <td><?php echo $this->data[$i]['tAboutUsHeaderTitle']; ?></td>
                                        <td><?php echo $this->data[$i]['tAboutUsDescription']; ?></td>
                                        <td>
                                            <?php
                                            if (empty($this->data[$i]['tAboutUsBannerImage'])) 
                                            {
                                                ?><img width="100px" src="<?php echo Urls::$BASE.Urls::$ABOUTUS."no-image.png"; ?>" class="vpb_image_style" class="img-thumbnail">
                                                <?php 
                                            }
                                            else{
                                                ?><img  width="100px;" src="<?php echo Urls::$BASE.Urls::$ABOUTUS.$this->data[$i]['tAboutUsBannerImage']?>"/>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <button onclick="Show_Services(<?php echo $this->data[$i]['nAboutUsIDPK']; ?>)" data-toggle="modal" data-target="#countryAdd"  class="btn_view btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill" title="View"><i class="fa fa-eye"></i></button>

                                            <form method="post" style="display: inline-table;"
                                                  action="<?php echo Urls::$BASE ?>aboutus/editaboutus">
                                                <input type="hidden" name="aboutusID"
                                                       value="<?php echo $this->data[$i]['nAboutUsIDPK']; ?>">
                                                <button  data-toggle="m-tooltip" data-placement="top" type="button"
                                                        data-original-title="Edit aboutus"
                                                        class="btn_edit btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill">
                                                    <i class="fa fa-pencil-alt"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">About Us Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                                
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label>About Us Title</label>
                        <input type="text"
                               class="form-control m-input"
                               name="title" id="title" 
                                disabled>
                    </div>
                    <div class="col-lg-12">

                      <label>Description</label>
                      <textarea name="description" id="description" class="form-control m-input" disabled></textarea>

                  </div>
                </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#s1').DataTable({
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "columnDefs": [
                {"width": "5%", "targets": 0}
            ]
        });

        $(document).on('click', ".btn_edit", function (e) {

            e.preventDefault();

            var form = $(this).closest("form");

            //var v = form.find("input[name='RID']").val();
            // console.log(l);
            //console.log(v);

            form.submit();
        });

        $(document).on('click', ".btn_view", function (e) {

            e.preventDefault();

            var form = $(this).closest("form");

            //var v = form.find("input[name='RID']").val();
            // console.log(l);
            //console.log(v);

            form.submit();
        });

       


    });

    function setId(del_id) {
        swal({
            title: "Are you sure?",
            text: "You want to delete this aboutus ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {
            if (e.value) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Urls::$BASE ?>aboutus/deleteaboutus",
                    data: {'id': del_id},
                    success: function (data) {
                        if (data.trim() == "true") {
                            location.href = "<?php echo Urls::$BASE ?>aboutus/listing";
                        }
                    }
                });
            }
        });
    }
    function Show_Services(id) {
        // alert(id);
        var pId = id
        $.ajax({
         url: '<?php echo Urls::$BASE; ?>aboutus/viewaboutus', //This is the current doc
        type: "POST",
        data: ({Pid: pId}), //variables should be pass like this
        success: function(data)
        {
           // console.log(data);
            var jdata = JSON.parse(data);
            console.log(jdata);

            var tAboutUsHeaderTitle = jdata[0].tAboutUsHeaderTitle;
            var tAboutUsDescription = jdata[0].tAboutUsDescription;
            var tAboutUsTitleOne = jdata[0].tAboutUsTitleOne;
            var tAboutUsTitleOneDescription = jdata[0].tAboutUsTitleOneDescription;
            var tAboutUsTitleTwo = jdata[0].tAboutUsTitleTwo;
            var tAboutUsTitleTwoDescription = jdata[0].tAboutUsTitleTwoDescription;
            var tAboutUsTitleThree = jdata[0].tAboutUsTitleThree;
            var tAboutUsTitleThreeDescription = jdata[0].tAboutUsTitleThreeDescription;
            var tAboutUsBannerImage = jdata[0].tAboutUsBannerImage;
            

            // console.log(projectName);
           

            $('#title').val(tAboutUsHeaderTitle);
            $('#description').val(tAboutUsDescription);
            $('#Nameone').val(tAboutUsTitleOne);
            $('#descriptionone').val(tAboutUsTitleOneDescription);
            $('#Nametwo').val(tAboutUsTitleTwo);
            $('#descriptiontwo').val(tAboutUsTitleTwoDescription);
            $('#Namethree').val(tAboutUsTitleThree);
             $('#descriptionthree').val(tAboutUsTitleThreeDescription);
        }
        }).done(function(){
            
            $('#update').modal('show'); 
      });
        
    }

</script>