<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class Aboutus extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/aboutus/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $this->view->render($name, "aboutus", $header, $footer);
    }

    public function createaboutus() // insert aboutus
    {
        echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                $FolderName =  Urls::$ABOUTUS;

                if (!is_dir($FolderName)) 
                {  
                    mkdir($FolderName, 0777, true);     
                    copy(Urls::$IMG_UPLOADS.'no_image.png',$FolderName.'/no_image.png');
                }   

                $files = $_FILES['banner_image']['name'];

                $preImgText = date('ymdhsi');

                $Folder_name = Urls::$ABOUTUS;

                // $get_deleted_Img = $_POST['deleteFoodImg'];

                // $images = $_FILES['Meetus_logo1']['name'];

                $extension = end(explode(".", $files));
                $imageName = $preImgText.".".$extension;
                $targetPath = $Folder_name.$preImgText.".".$extension;

                move_uploaded_file($_FILES['banner_image']['tmp_name'],$targetPath);
                

                $field = array('tAboutUsHeaderTitle', 'tAboutUsDescription', 'tAboutUsBannerImage');
                $value = array($title, $description, $imageName);

                $aboutusData = array_combine($field, $value);
                // print_r($aboutusData);
                // exit();
                $aboutusID = $this->model->insert($aboutusData, 'tblaboutus');

                if (isset($aboutusID) && $aboutusID == 0) {
                    SessionHandling::set("err_msg", "Error while adding aboutus");
                    header("Location:../aboutus/listing");
                } else {
                    SessionHandling::set("suc_msg", "aboutus added successfully");
                    header("Location:../aboutus/listing");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update aboutus
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();

            $id = array('nAboutUsIDPK' => $nAboutUsIDPK);

            $FolderName =  Urls::$ABOUTUS;

            if (!is_dir($FolderName)) 
            {  
                mkdir($FolderName, 0777, true);     
                copy(Urls::$IMG_UPLOADS.'no_image.png',$FolderName.'/no_image.png');
            }   

            $files = $_FILES['banner_image']['name'];
                    
                $preImgText = date('ymdhsi');

                $Folder_name = Urls::$ABOUTUS;

                // $get_deleted_Img = $_POST['deleteFoodImg'];

                // $images = $_FILES['Meetus_logo1']['name'];

                $extension = end(explode(".", $files));
                $imageName = $preImgText.".".$extension;
                $targetPath = $Folder_name.$preImgText.".".$extension;

                move_uploaded_file($_FILES['banner_image']['tmp_name'],$targetPath);

            if ($files != "") {
                $field = array('tAboutUsHeaderTitle', 'tAboutUsDescription', 'tAboutUsBannerImage');
                $value = array($title, $description, $imageName);
            } else {
                 $field = array('tAboutUsHeaderTitle', 'tAboutUsDescription');
                $value = array($title, $description);
            }

            $aboutusData = array_combine($field, $value);
            // print_r($aboutusData);
            // exit();

            $aboutusID = $this->model->update($aboutusData, 'tblaboutus', $id);

            if (isset($aboutusID) && $aboutusID > 0) {
                SessionHandling::set("suc_msg", "aboutus updated successfully");
                header("Location:../aboutus/listing");
            } else if (isset($aboutusID) && $aboutusID == 0) {
                SessionHandling::set("suc_msg", "aboutus updated successfully");
                header("Location:../aboutus/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating aboutus");
                header("Location:../aboutus/listing");
            }
        }
    }

    public function listing()  // aboutus listing
    {
        $name = 'modules/aboutus/view/aboutus_listing.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllaboutus();
        $this->view->data = $data;

        $this->view->render($name, "aboutus Listing", $header, $footer);

    }

    public function editaboutus()   // edit aboutus
    {
        $name = 'modules/aboutus/view/index.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleaboutusData = $this->model->fetchSingleaboutus($aboutusID);
        $this->view->singleaboutus = $singleaboutusData;

        $this->view->render($name, "Update aboutus", $header, $footer);
    }
    public function viewaboutus()   // edit aboutus
    {
         
        extract($_POST);
        $singleaboutusData = $this->model->fetchSingleaboutus($Pid);
        echo json_encode($singleaboutusData);

       
    }
    

    public function deleteaboutus() // delete aboutus
    {
        $aboutusID = $_POST['id'];

        $id = array('naboutusIDPK' => $aboutusID);

        $field = array('bIsRemoved');
        $value = array(1);

        $aboutusData = array_combine($field, $value);

        $res = $this->model->update($aboutusData, 'tblaboutus', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "aboutus deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting aboutus");
            echo "false";
        }
    }

}