<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 10/3/2018
 * Time: 7:23 PM
 */

class ImageLabels extends Controller
{
    public function __construct()
    {
        parent::__construct();
        SessionHandling::init();
    }

    public function index()
    {
        $name = 'modules/imagelabels/view/imagelabels.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

         $data = $this->model->getAllimagelabels();
        $this->view->data = $data;

        $this->view->render($name, "Image Labels", $header, $footer);
    }

    public function createImageLabels() // insert imagelabels
    {
        // echo "<pre>";
        extract($_FILES);
        extract($_POST);   
        // print_r($_POST);
        // print_r($_FILES);
        //  exit();
        if ($_POST['submit'] == "submit") {
            if (isset($_POST) && !empty($_POST)) {
                extract($_POST);

                
                // $login = SessionHandling::get('loggedId');
               
                $field = array('tImageLabelName');
                $value = array($title);

                $imagelabelsData = array_combine($field, $value);
                // print_r($imagelabelsData);
                // exit();
                $imagelabelsID = $this->model->insert($imagelabelsData, 'tblimagelabels');

                if (isset($imagelabelsID) && $imagelabelsID == 0) {
                    SessionHandling::set("err_msg", "Error while adding Image Labels");
                    header("Location:../imagelabels/");
                } else {
                    SessionHandling::set("suc_msg", "Image Labels added successfully");
                    header("Location:../imagelabels/");
                }
            }
        } else if ($_POST['submit'] == "update") {  // update imagelabels
            extract($_POST);
            extract($_FILES);
            
           // print_r($_POST); 
           // print_r($_FILES);
           // exit();
            // $login = SessionHandling::get('loggedId');

            $id = array('nImageLabelIDPK' => $nimagelabelsIDPK);

            
                $field = array('tImageLabelName');
                $value = array($title);

            $imagelabelsData = array_combine($field, $value);
            // print_r($imagelabelsData);
            // exit();

            $imagelabelsID = $this->model->update($imagelabelsData, 'tblimagelabels', $id);

            if (isset($imagelabelsID) && $imagelabelsID > 0) {
                SessionHandling::set("suc_msg", "Image Labels updated successfully");
                header("Location:../imagelabels/listing");
            } else if (isset($imagelabelsID) && $imagelabelsID == 0) {
                SessionHandling::set("suc_msg", "Image Labels updated successfully");
                header("Location:../imagelabels/listing");
            } else {
                SessionHandling::set("err_msg", "Error while updating Image Labels");
                header("Location:../imagelabels/listing");
            }
        }
    }

    public function listing()  // imagelabels listing
    {
        $name = 'modules/imagelabels/view/imagelabels.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        $data = $this->model->getAllimagelabels();
        $this->view->data = $data;

        $this->view->render($name, "Image Labels Listing", $header, $footer);

    }

    public function editImageLabels()   // edit imagelabels
    {
        $name = 'modules/imagelabels/view/imagelabels.php';
        $header = './commons/header.php';
        $footer = './commons/footer.php';

        extract($_POST);

        $singleimagelabelsData = $this->model->fetchSingleimagelabels($ImageLabelID);
        $this->view->singleimagelabels = $singleimagelabelsData;
        
        $data = $this->model->getAllimagelabels();
        $this->view->data = $data;

        $this->view->render($name, "Update Image Labels", $header, $footer);
    }
    public function viewimagelabels()   // edit imagelabels
    {
         
        extract($_POST);
        $singleimagelabelsData = $this->model->fetchSingleimagelabels($Pid);
        echo json_encode($singleimagelabelsData);

       
    }
    

    public function deleteImageLabels() // delete imagelabels
    {
        $imagelabelsID = $_POST['id'];

        $id = array('nImageLabelIDPK' => $imagelabelsID);

        $field = array('bIsRemoved');
        $value = array(1);

        $imagelabelsData = array_combine($field, $value);

        $res = $this->model->update($imagelabelsData, 'tblimagelabels', $id);

        if ($res > 0) {
            SessionHandling::set("suc_msg", "Image Labels deleted successfully");
            echo "true";
        } else {
            SessionHandling::set("err_msg", "Error while deleting Image Labels");
            echo "false";
        }
    }

}