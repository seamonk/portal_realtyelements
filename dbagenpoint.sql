-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 08, 2019 at 11:00 PM
-- Server version: 5.6.44-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbagenpoint`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblaboutus`
--

CREATE TABLE `tblaboutus` (
  `nAboutUsIDPK` int(5) NOT NULL,
  `tAboutUsHeaderTitle` text NOT NULL,
  `tAboutUsDescription` text NOT NULL,
  `tAboutUsBannerImage` text NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbladminchangelog`
--

CREATE TABLE `tbladminchangelog` (
  `nAdminChangeLogIDPK` int(11) NOT NULL,
  `nLoggedInIDFK` int(11) NOT NULL,
  `tLoggedInIpAddress` text NOT NULL,
  `nAdminChangeLogDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vTriggerSectionMenu` varchar(100) NOT NULL,
  `vTriggerType` varchar(50) NOT NULL,
  `tTriggerSqlQuery` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentassociations`
--

CREATE TABLE `tblagentassociations` (
  `nAgentAssociationIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `nAssociationIDFK` int(11) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentcontactnumbers`
--

CREATE TABLE `tblagentcontactnumbers` (
  `nAgentContactNumberIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `vContactNumber` varchar(20) NOT NULL,
  `bIsMobileNumberVerified` tinyint(4) NOT NULL DEFAULT '0',
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `dtCreatedBy` int(11) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentlanguagesknown`
--

CREATE TABLE `tblagentlanguagesknown` (
  `nAgentLanguageKnownIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `nLanguageIDFK` int(11) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentlocalitiesexpert`
--

CREATE TABLE `tblagentlocalitiesexpert` (
  `nAgentLocalityIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `nLocalityIDFK` int(11) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentreviews`
--

CREATE TABLE `tblagentreviews` (
  `nAgentReviewIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `nUserIDFK` int(11) NOT NULL,
  `tUserReview` text NOT NULL,
  `dtUserReviewDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsAgentReviewsRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagents`
--

CREATE TABLE `tblagents` (
  `nAgentIDPK` int(11) NOT NULL,
  `nUserIDFK` int(11) NOT NULL,
  `nAgentReraID` varchar(50) DEFAULT NULL,
  `nDesignationIDFK` int(11) DEFAULT NULL,
  `nAgentExperience` int(11) DEFAULT NULL,
  `tAgentDescription` text,
  `nCompanyIDFK` int(11) DEFAULT NULL,
  `vAgentMobileNumber2` varchar(20) DEFAULT NULL,
  `vAgentLandlineNumber` varchar(20) DEFAULT NULL,
  `tAgentAddress` text,
  `nAreaIDFK` int(11) DEFAULT NULL,
  `nCityIDFK` int(11) DEFAULT NULL,
  `nStateIDFK` int(11) DEFAULT NULL,
  `nCountryIDFK` int(11) DEFAULT NULL,
  `bIsAgentReraCertified` tinyint(1) DEFAULT NULL,
  `tGoogleMapAddressLink` text,
  `tAgentCompanyWebsiteLink` text,
  `tAgentFacebookLink` text,
  `tAgentLinkedinLink` text,
  `tAgentTwitterLink` text,
  `tAgentYoutubeLink` text,
  `tAgentInstaLink` text,
  `nAgentTotalViews` int(11) NOT NULL DEFAULT '0',
  `bIsAgentVerifiedFromAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `bIsAgentActive` tinyint(1) NOT NULL DEFAULT '1',
  `bIsAgentFeatured` tinyint(1) NOT NULL DEFAULT '0',
  `dtAgentFeaturedExpireOn` date DEFAULT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nRemovedBy` int(11) DEFAULT NULL,
  `dtRemovedOn` datetime DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentspecializations`
--

CREATE TABLE `tblagentspecializations` (
  `nAgentSpecializationIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `nSpecializationIDFK` int(11) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagentsubscription`
--

CREATE TABLE `tblagentsubscription` (
  `nAgentSubscriptionIDPK` int(11) NOT NULL,
  `nUserIDFK` int(11) NOT NULL,
  `nSubscriptionIDFK` int(11) NOT NULL,
  `nSubscriptionDaysCount` int(11) NOT NULL,
  `nUserListingLimit` int(11) NOT NULL,
  `dtSubscriptionExpireOn` datetime NOT NULL,
  `dtUserCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nUserCreatedBy` int(11) NOT NULL,
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `dtRemovedOn` datetime NOT NULL,
  `nRemovedBy` int(11) NOT NULL,
  `nIsDummyData` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblagenttestimonials`
--

CREATE TABLE `tblagenttestimonials` (
  `nAgentTestimonialIDPK` int(11) NOT NULL,
  `nAgentIDFK` int(11) NOT NULL,
  `nTestimonialIDFK` int(11) NOT NULL,
  `bIsAgentTestimonialRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblamenities`
--

CREATE TABLE `tblamenities` (
  `nAmenityIDPK` int(11) NOT NULL,
  `tAmenityName` text NOT NULL,
  `tAmenityImagePath` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblamenities`
--

INSERT INTO `tblamenities` (`nAmenityIDPK`, `tAmenityName`, `tAmenityImagePath`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'Car Parking', '190405020010.png', '2019-09-25 12:49:02', 1, 1, NULL, NULL, 0),
(2, 'Security', '190409120228.png', '2019-09-25 12:49:06', 1, 1, NULL, NULL, 0),
(3, 'Cyclone Proof', '190409122628.png', '2019-09-25 12:48:48', 1, 1, NULL, NULL, 0),
(4, 'Home Theater', '190715060748.png', '2019-10-06 12:38:13', 1, 0, NULL, NULL, 0),
(5, 'Badminton', '190715060349.png', '2019-09-26 10:45:09', 1, 0, NULL, NULL, 0),
(6, 'Concierge Services', '190715061949.png', '2019-10-06 12:39:25', 1, 1, NULL, NULL, 0),
(7, 'Seating', '190715063749.png', '2019-10-06 12:38:40', 1, 0, NULL, NULL, 0),
(8, 'Cafeteria', '190715061550.png', '2019-10-06 12:39:01', 1, 0, NULL, NULL, 0),
(9, 'Concierge', '190715063450.png', '2019-07-15 13:20:34', 1, 0, NULL, NULL, 0),
(10, 'Cricket', '190715064750.png', '2019-07-15 13:20:47', 1, 0, NULL, NULL, 0),
(11, 'Day Care Centre', '190715060351.png', '2019-09-27 08:13:57', 1, 0, NULL, NULL, 0),
(12, 'Earthquake Resistant', '190715062751.png', '2019-09-26 10:46:23', 1, 0, NULL, NULL, 0),
(13, 'Fire Alarm', '190715065851.png', '2019-09-27 07:09:14', 1, 0, NULL, NULL, 0),
(14, 'Fire Extinguisher', '190715061252.png', '2019-09-27 07:20:31', 1, 0, NULL, NULL, 0),
(15, 'Garden', '190715062752.png', '2019-09-27 04:53:24', 1, 0, NULL, NULL, 0),
(16, 'Restaurant ', '190715064252.png', '2019-09-27 08:31:08', 1, 0, NULL, NULL, 0),
(17, 'Gas Connection', '190715065552.png', '2019-09-27 07:20:04', 1, 0, NULL, NULL, 0),
(18, 'Golf Course', '190715060853.png', '2019-09-27 07:51:26', 1, 0, NULL, NULL, 0),
(19, 'Toddlers Room', '190715060854.png', '2019-09-27 07:45:12', 1, 0, NULL, NULL, 0),
(20, 'Sauna Room', '190715062159.png', '2019-09-27 05:25:56', 1, 0, NULL, NULL, 0),
(21, 'Conference Room', '190715065759.png', '2019-09-27 08:34:39', 1, 0, NULL, NULL, 0),
(22, 'Multi-Purpose Hall', '190715071500.png', '2019-09-27 08:36:25', 1, 0, NULL, NULL, 0),
(23, 'Waste Disposal', '190715073300.png', '2019-09-27 08:33:51', 1, 0, NULL, NULL, 0),
(24, 'Meeting Room', '190715074700.png', '2019-09-27 08:35:08', 1, 0, NULL, NULL, 0),
(25, 'Reception', '190715071601.png', '2019-10-06 12:41:09', 1, 0, NULL, NULL, 0),
(26, 'Skating Ring', '190715073601.png', '2019-09-27 08:00:46', 1, 0, NULL, NULL, 0),
(27, 'Steam Bath', '190717121637.png', '2019-09-27 05:26:48', 1, 0, NULL, NULL, 0),
(28, 'Jacuzzi', '190717123637.png', '2019-07-17 07:07:36', 1, 0, NULL, NULL, 0),
(29, 'Left', '190717124937.png', '2019-09-17 13:20:59', 1, 1, NULL, NULL, 0),
(30, 'Library', '190717120738.png', '2019-07-17 07:08:07', 1, 0, NULL, NULL, 0),
(31, 'Swimming Pool', '190717122938.png', '2019-09-27 04:54:55', 1, 0, NULL, NULL, 0),
(32, 'Rainwater Harvest', '190717121639.png', '2019-09-27 07:32:09', 1, 0, NULL, NULL, 0),
(33, 'Squash', '190717123239.png', '2019-07-17 07:09:32', 1, 0, NULL, NULL, 0),
(34, 'Water Softening Plant', '190717125439.png', '2019-09-27 07:29:43', 1, 0, NULL, NULL, 0),
(35, 'Volleyball', '190717121340.png', '2019-09-27 07:49:28', 1, 0, NULL, NULL, 0),
(36, 'woman-stretching-and-flexing-legs-with-arms-up', '190717124140.', '2019-09-17 13:20:40', 1, 1, NULL, NULL, 0),
(37, 'Lift', '190927102817.png', '2019-09-27 04:47:29', 1, 0, NULL, NULL, 0),
(38, 'Club House', '190927101924.png', '2019-09-27 04:54:19', 1, 0, NULL, NULL, 0),
(39, 'Splash Pool', '190927100453.png', '2019-09-27 05:23:04', 1, 0, NULL, NULL, 0),
(40, 'Gymnasium', '190927102854.png', '2019-09-27 05:24:28', 1, 0, NULL, NULL, 0),
(41, 'Security', '190927125131.png', '2019-09-27 07:01:51', 1, 0, NULL, NULL, 0),
(42, 'Facility Staff', '190927124038.png', '2019-09-27 07:08:40', 1, 0, NULL, NULL, 0),
(43, 'CCTV', '190927122849.png', '2019-09-27 07:19:28', 1, 0, NULL, NULL, 0),
(44, 'Power Backup', '190927123354.png', '2019-09-27 07:24:33', 1, 0, NULL, NULL, 0),
(45, 'Water Storage', '190927015400.png', '2019-09-27 07:30:54', 1, 0, NULL, NULL, 0),
(46, 'Intercom Facility', '190927014904.png', '2019-09-27 07:34:49', 1, 0, NULL, NULL, 0),
(47, 'Indoor Games', '190927013712.png', '2019-09-27 07:55:09', 1, 0, NULL, NULL, 0),
(48, 'Tennis', '190927011918.', '2019-09-27 07:48:19', 1, 0, NULL, NULL, 0),
(49, 'Football', '190927014620.png', '2019-09-27 07:50:46', 1, 0, NULL, NULL, 0),
(50, 'Childrenâ€™s Play Area', '190927015529.png', '2019-09-27 07:59:55', 1, 0, NULL, NULL, 0),
(51, 'Yoga', '190927013732.png', '2019-09-27 08:02:37', 1, 0, NULL, NULL, 0),
(52, 'Jogging Track', '190927011139.png', '2019-09-27 08:09:11', 1, 0, NULL, NULL, 0),
(53, 'Senior Citizen Sitting', '190927013243.png', '2019-09-27 08:13:32', 1, 0, NULL, NULL, 0),
(54, 'Drop off Zone', '190927014249.png', '2019-09-27 08:19:42', 1, 0, NULL, NULL, 0),
(55, 'Community Hall', '190927015650.png', '2019-09-27 08:20:56', 1, 0, NULL, NULL, 0),
(56, 'Amphitheatre', '190927010752.png', '2019-09-27 08:22:07', 1, 0, NULL, NULL, 0),
(57, 'Shrine', '190927013654.png', '2019-09-27 08:24:36', 1, 0, NULL, NULL, 0),
(58, 'Service Apartment', '190927021800.png', '2019-09-27 08:30:18', 1, 0, NULL, NULL, 0),
(59, 'Common Washroom', '190927021803.png', '2019-09-27 08:33:18', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblapplypositionrequest`
--

CREATE TABLE `tblapplypositionrequest` (
  `nPositionRequestIDPK` int(11) NOT NULL,
  `tCandidateName` text NOT NULL,
  `tCandidateEmailId` text NOT NULL,
  `nCareerDesignationIDFK` int(11) NOT NULL,
  `tComment` text NOT NULL,
  `tCVFilePath` text NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nRemovedBy` int(11) DEFAULT NULL,
  `dtRemovedOn` datetime DEFAULT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblareas`
--

CREATE TABLE `tblareas` (
  `nAreaIDPK` int(11) NOT NULL,
  `nCityIDFK` int(11) NOT NULL,
  `tAreaName` text NOT NULL,
  `nAreaPincode` int(6) NOT NULL,
  `dAreaLatitude` double(15,10) DEFAULT NULL,
  `dAreaLongitude` double(15,10) DEFAULT NULL,
  `dAreaCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nAreaCreatedBy` int(11) NOT NULL,
  `bIsAreaRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nAreaRemovedBy` int(11) DEFAULT NULL,
  `dtAreaRemovedOn` datetime DEFAULT NULL,
  `bIsAreaDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblareas`
--

INSERT INTO `tblareas` (`nAreaIDPK`, `nCityIDFK`, `tAreaName`, `nAreaPincode`, `dAreaLatitude`, `dAreaLongitude`, `dAreaCreatedOn`, `nAreaCreatedBy`, `bIsAreaRemoved`, `nAreaRemovedBy`, `dtAreaRemovedOn`, `bIsAreaDummyData`) VALUES
(1, 1, 'Prahladnagar', 380015, 33.0000000000, 33.0000000000, '2019-10-06 12:52:12', 1, 1, 1, '2019-10-06 18:22:12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblassociationrequest`
--

CREATE TABLE `tblassociationrequest` (
  `nAssociationRequestIDPK` int(11) NOT NULL,
  `nAssociationIDFK` int(11) NOT NULL,
  `nUserIDFK` int(11) NOT NULL,
  `bIsRemoved` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblassociations`
--

CREATE TABLE `tblassociations` (
  `nAssociationIDPK` int(11) NOT NULL,
  `tAssociationName` text NOT NULL,
  `nAssociationIconPath` text NOT NULL,
  `bIsVeirfied` tinyint(4) NOT NULL DEFAULT '0',
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblassociations`
--

INSERT INTO `tblassociations` (`nAssociationIDPK`, `tAssociationName`, `nAssociationIconPath`, `bIsVeirfied`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'National Association of Realtors', '191006120857.', 0, '2019-10-06 12:57:08', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblbhk`
--

CREATE TABLE `tblbhk` (
  `nBHKIDPK` int(11) NOT NULL,
  `tBHKName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbhk`
--

INSERT INTO `tblbhk` (`nBHKIDPK`, `tBHKName`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, '1BHK', '2019-04-09 06:46:08', 1, 0, NULL, NULL, 0),
(3, '2BHK', '2019-05-18 19:05:10', 1, 0, NULL, NULL, 0),
(4, '3BHK', '2019-05-18 19:05:25', 1, 0, NULL, NULL, 0),
(5, '4BHK', '2019-05-18 19:05:34', 1, 0, NULL, NULL, 0),
(6, '5BHK', '2019-05-18 19:05:43', 1, 0, NULL, NULL, 0),
(7, '6BHK', '2019-05-18 19:05:55', 1, 0, NULL, NULL, 0),
(8, '7BHK', '2019-10-06 12:46:24', 1, 0, NULL, NULL, 0),
(9, '8BHK', '2019-10-06 12:46:38', 1, 0, NULL, NULL, 0),
(10, '9BHK', '2019-10-06 12:46:52', 1, 0, NULL, NULL, 0),
(11, '10BHK', '2019-10-06 12:47:04', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblblogs`
--

CREATE TABLE `tblblogs` (
  `nBlogIDPK` int(11) NOT NULL,
  `dBlogDate` date NOT NULL,
  `tImgPath` text NOT NULL,
  `tBlogHeading` text NOT NULL,
  `tBlogContent` text NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcareerdesignations`
--

CREATE TABLE `tblcareerdesignations` (
  `nCareerDesignationIDPK` int(11) NOT NULL,
  `tDesignationName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcareers`
--

CREATE TABLE `tblcareers` (
  `nCareerIDPK` int(11) NOT NULL,
  `nCareerDesignationIDFK` int(11) NOT NULL,
  `nNumberOfCareerPosition` int(11) NOT NULL,
  `nExeperienceFrom` int(11) NOT NULL,
  `nExeperienceTo` int(11) NOT NULL,
  `nCityIDFK` int(11) NOT NULL,
  `tDescription` text NOT NULL,
  `nCreatedBy` int(11) DEFAULT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nRemovedBy` int(11) DEFAULT NULL,
  `dtRemovedOn` datetime DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcities`
--

CREATE TABLE `tblcities` (
  `nCityIDPK` int(11) NOT NULL,
  `nStateIDFK` int(11) NOT NULL,
  `tCityName` text NOT NULL,
  `tCityImagePath` text NOT NULL,
  `bShowInHeader` tinyint(1) NOT NULL DEFAULT '0',
  `dCityCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCityCreatedBy` int(11) NOT NULL,
  `bIsCityRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nCityRemovedBy` int(11) DEFAULT NULL,
  `dtCityRemovedOn` datetime DEFAULT NULL,
  `bIsCityDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcities`
--

INSERT INTO `tblcities` (`nCityIDPK`, `nStateIDFK`, `tCityName`, `tCityImagePath`, `bShowInHeader`, `dCityCreatedOn`, `nCityCreatedBy`, `bIsCityRemoved`, `nCityRemovedBy`, `dtCityRemovedOn`, `bIsCityDummyData`) VALUES
(1, 1, 'Alipur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(2, 1, 'Andaman Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(3, 1, 'Anderson Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(4, 1, 'Arainj-Laka-Punga', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(5, 1, 'Austinabad', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(6, 1, 'Bamboo Flat', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(7, 1, 'Barren Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(8, 1, 'Beadonabad', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(9, 1, 'Betapur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(10, 1, 'Bindraban', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(11, 1, 'Bonington', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(12, 1, 'Brookesabad', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(13, 1, 'Cadell Point', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(14, 1, 'Calicut', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(15, 1, 'Chetamale', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(16, 1, 'Cinque Islands', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(17, 1, 'Defence Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(18, 1, 'Digilpur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(19, 1, 'Dolyganj', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(20, 1, 'Flat Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(21, 1, 'Geinyale', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(22, 1, 'Great Coco Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(23, 1, 'Haddo', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(24, 1, 'Havelock Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(25, 1, 'Henry Lawrence Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(26, 1, 'Herbertabad', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(27, 1, 'Hobdaypur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(28, 1, 'Ilichar', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(29, 1, 'Ingoie', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(30, 1, 'Inteview Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(31, 1, 'Jangli Ghat', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(32, 1, 'Jhon Lawrence Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(33, 1, 'Karen', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(34, 1, 'Kartara', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(35, 1, 'KYD Islannd', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(36, 1, 'Landfall Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(37, 1, 'Little Andmand', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(38, 1, 'Little Coco Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(39, 1, 'Long Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(40, 1, 'Maimyo', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(41, 1, 'Malappuram', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(42, 1, 'Manglutan', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(43, 1, 'Manpur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(44, 1, 'Mitha Khari', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(45, 1, 'Neill Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(46, 1, 'Nicobar Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(47, 1, 'North Brother Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(48, 1, 'North Passage Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(49, 1, 'North Sentinel Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(50, 1, 'Nothen Reef Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(51, 1, 'Outram Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(52, 1, 'Pahlagaon', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(53, 1, 'Palalankwe', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(54, 1, 'Passage Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(55, 1, 'Phaiapong', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(56, 1, 'Phoenix Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(57, 1, 'Port Blair', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(58, 1, 'Preparis Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(59, 1, 'Protheroepur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(60, 1, 'Rangachang', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(61, 1, 'Rongat', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(62, 1, 'Rutland Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(63, 1, 'Sabari', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(64, 1, 'Saddle Peak', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(65, 1, 'Shadipur', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(66, 1, 'Smith Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(67, 1, 'Sound Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(68, 1, 'South Sentinel Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(69, 1, 'Spike Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(70, 1, 'Tarmugli Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(71, 1, 'Taylerabad', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(72, 1, 'Titaije', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(73, 1, 'Toibalawe', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(74, 1, 'Tusonabad', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(75, 1, 'West Island', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(76, 1, 'Wimberleyganj', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(77, 1, 'Yadita', '', 0, '2019-10-01 08:52:46', 0, 0, NULL, NULL, 0),
(78, 2, 'Adilabad', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(79, 2, 'Anantapur', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(80, 2, 'Chittoor', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(81, 2, 'Cuddapah', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(82, 2, 'East Godavari', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(83, 2, 'Guntur', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(84, 2, 'Hyderabad', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(85, 2, 'Karimnagar', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(86, 2, 'Khammam', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(87, 2, 'Krishna', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(88, 2, 'Kurnool', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(89, 2, 'Mahabubnagar', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(90, 2, 'Medak', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(91, 2, 'Nalgonda', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(92, 2, 'Nellore', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(93, 2, 'Nizamabad', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(94, 2, 'Prakasam', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(95, 2, 'Rangareddy', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(96, 2, 'Srikakulam', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(97, 2, 'Visakhapatnam', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(98, 2, 'Vizianagaram', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(99, 2, 'Warangal', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(100, 2, 'West Godavari', '', 0, '2019-10-01 09:01:27', 0, 0, NULL, NULL, 0),
(101, 3, 'Anjaw', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(102, 3, 'Changlang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(103, 3, 'Dibang Valley', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(104, 3, 'East Kameng', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(105, 3, 'East Siang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(106, 3, 'Itanagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(107, 3, 'Kurung Kumey', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(108, 3, 'Lohit', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(109, 3, 'Lower Dibang Valley', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(110, 3, 'Lower Subansiri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(111, 3, 'Papum Pare', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(112, 3, 'Tawang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(113, 3, 'Tirap', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(114, 3, 'Upper Siang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(115, 3, 'Upper Subansiri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(116, 3, 'West Kameng', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(117, 3, 'West Siang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(118, 4, 'Barpeta', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(119, 4, 'Bongaigaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(120, 4, 'Cachar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(121, 4, 'Darrang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(122, 4, 'Dhemaji', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(123, 4, 'Dhubri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(124, 4, 'Dibrugarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(125, 4, 'Goalpara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(126, 4, 'Golaghat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(127, 4, 'Guwahati', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(128, 4, 'Hailakandi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(129, 4, 'Jorhat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(130, 4, 'Kamrup', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(131, 4, 'Karbi Anglong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(132, 4, 'Karimganj', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(133, 4, 'Kokrajhar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(134, 4, 'Lakhimpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(135, 4, 'Marigaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(136, 4, 'Nagaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(137, 4, 'Nalbari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(138, 4, 'North Cachar Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(139, 4, 'Silchar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(140, 4, 'Sivasagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(141, 4, 'Sonitpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(142, 4, 'Tinsukia', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(143, 4, 'Udalguri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(144, 5, 'Araria', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(145, 5, 'Aurangabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(146, 5, 'Banka', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(147, 5, 'Begusarai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(148, 5, 'Bhagalpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(149, 5, 'Bhojpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(150, 5, 'Buxar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(151, 5, 'Darbhanga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(152, 5, 'East Champaran', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(153, 5, 'Gaya', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(154, 5, 'Gopalganj', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(155, 5, 'Jamshedpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(156, 5, 'Jamui', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(157, 5, 'Jehanabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(158, 5, 'Kaimur (Bhabua)', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(159, 5, 'Katihar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(160, 5, 'Khagaria', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(161, 5, 'Kishanganj', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(162, 5, 'Lakhisarai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(163, 5, 'Madhepura', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(164, 5, 'Madhubani', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(165, 5, 'Munger', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(166, 5, 'Muzaffarpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(167, 5, 'Nalanda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(168, 5, 'Nawada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(169, 5, 'Patna', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(170, 5, 'Purnia', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(171, 5, 'Rohtas', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(172, 5, 'Saharsa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(173, 5, 'Samastipur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(174, 5, 'Saran', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(175, 5, 'Sheikhpura', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(176, 5, 'Sheohar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(177, 5, 'Sitamarhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(178, 5, 'Siwan', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(179, 5, 'Supaul', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(180, 5, 'Vaishali', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(181, 5, 'West Champaran', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(182, 6, 'Chandigarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(183, 6, 'Mani Marja', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(184, 7, 'Bastar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(185, 7, 'Bhilai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(186, 7, 'Bijapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(187, 7, 'Bilaspur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(188, 7, 'Dhamtari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(189, 7, 'Durg', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(190, 7, 'Janjgir-Champa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(191, 7, 'Jashpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(192, 7, 'Kabirdham-Kawardha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(193, 7, 'Korba', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(194, 7, 'Korea', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(195, 7, 'Mahasamund', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(196, 7, 'Narayanpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(197, 7, 'Norh Bastar-Kanker', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(198, 7, 'Raigarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(199, 7, 'Raipur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(200, 7, 'Rajnandgaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(201, 7, 'South Bastar-Dantewada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(202, 7, 'Surguja', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(203, 8, 'Amal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(204, 8, 'Amli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(205, 8, 'Bedpa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(206, 8, 'Chikhli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(207, 8, 'Dadra & Nagar Haveli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(208, 8, 'Dahikhed', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(209, 8, 'Dolara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(210, 8, 'Galonda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(211, 8, 'Kanadi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(212, 8, 'Karchond', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(213, 8, 'Khadoli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(214, 8, 'Kharadpada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(215, 8, 'Kherabari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(216, 8, 'Kherdi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(217, 8, 'Kothar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(218, 8, 'Luari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(219, 8, 'Mashat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(220, 8, 'Rakholi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(221, 8, 'Rudana', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(222, 8, 'Saili', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(223, 8, 'Sili', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(224, 8, 'Silvassa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(225, 8, 'Sindavni', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(226, 8, 'Udva', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(227, 8, 'Umbarkoi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(228, 8, 'Vansda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(229, 8, 'Vasona', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(230, 8, 'Velugam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(231, 9, 'Brancavare', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(232, 9, 'Dagasi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(233, 9, 'Daman', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(234, 9, 'Diu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(235, 9, 'Magarvara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(236, 9, 'Nagwa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(237, 9, 'Pariali', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(238, 9, 'Passo Covo', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(239, 10, 'Central Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(240, 10, 'East Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(241, 10, 'New Delhi', '190715050141.png', 1, '2019-10-03 09:45:34', 0, 0, NULL, NULL, 0),
(242, 10, 'North Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(243, 10, 'North East Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(244, 10, 'North West Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(245, 10, 'Old Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(246, 10, 'South Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(247, 10, 'South West Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(248, 10, 'West Delhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(249, 11, 'Canacona', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(250, 11, 'Candolim', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(251, 11, 'Chinchinim', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(252, 11, 'Cortalim', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(253, 11, 'Goa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(254, 11, 'Jua', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(255, 11, 'Madgaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(256, 11, 'Mahem', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(257, 11, 'Mapuca', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(258, 11, 'Marmagao', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(259, 11, 'Panji', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(260, 11, 'Ponda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(261, 11, 'Sanvordem', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(262, 11, 'Terekhol', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(263, 12, 'Ahmedabad', '190715051134.png', 1, '2019-10-03 09:42:42', 0, 0, NULL, NULL, 0),
(264, 12, 'Amreli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(265, 12, 'Anand', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(266, 12, 'Banaskantha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(267, 12, 'Baroda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(268, 12, 'Bharuch', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(269, 12, 'Bhavnagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(270, 12, 'Dahod', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(271, 12, 'Dang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(272, 12, 'Dwarka', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(273, 12, 'Gandhinagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(274, 12, 'Jamnagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(275, 12, 'Junagadh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(276, 12, 'Kheda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(277, 12, 'Kutch', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(278, 12, 'Mehsana', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(279, 12, 'Nadiad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(280, 12, 'Narmada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(281, 12, 'Navsari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(282, 12, 'Panchmahals', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(283, 12, 'Patan', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(284, 12, 'Porbandar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(285, 12, 'Rajkot', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(286, 12, 'Sabarkantha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(287, 12, 'Surat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(288, 12, 'Surendranagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(289, 12, 'Vadodara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(290, 12, 'Valsad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(291, 12, 'Vapi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(292, 13, 'Ambala', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(293, 13, 'Bhiwani', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(294, 13, 'Faridabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(295, 13, 'Fatehabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(296, 13, 'Gurgaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(297, 13, 'Hisar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(298, 13, 'Jhajjar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(299, 13, 'Jind', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(300, 13, 'Kaithal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(301, 13, 'Karnal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(302, 13, 'Kurukshetra', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(303, 13, 'Mahendragarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(304, 13, 'Mewat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(305, 13, 'Panchkula', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(306, 13, 'Panipat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(307, 13, 'Rewari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(308, 13, 'Rohtak', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(309, 13, 'Sirsa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(310, 13, 'Sonipat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(311, 13, 'Yamunanagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(312, 14, 'Bilaspur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(313, 14, 'Chamba', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(314, 14, 'Dalhousie', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(315, 14, 'Hamirpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(316, 14, 'Kangra', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(317, 14, 'Kinnaur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(318, 14, 'Kullu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(319, 14, 'Lahaul & Spiti', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(320, 14, 'Mandi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(321, 14, 'Shimla', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(322, 14, 'Sirmaur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(323, 14, 'Solan', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(324, 14, 'Una', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(325, 15, 'Anantnag', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(326, 15, 'Baramulla', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(327, 15, 'Budgam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(328, 15, 'Doda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(329, 15, 'Jammu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(330, 15, 'Kargil', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(331, 15, 'Kathua', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(332, 15, 'Kupwara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(333, 15, 'Leh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(334, 15, 'Poonch', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(335, 15, 'Pulwama', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(336, 15, 'Rajauri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(337, 15, 'Srinagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(338, 15, 'Udhampur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(339, 16, 'Bokaro', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(340, 16, 'Chatra', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(341, 16, 'Deoghar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(342, 16, 'Dhanbad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(343, 16, 'Dumka', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(344, 16, 'East Singhbhum', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(345, 16, 'Garhwa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(346, 16, 'Giridih', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(347, 16, 'Godda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(348, 16, 'Gumla', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(349, 16, 'Hazaribag', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(350, 16, 'Jamtara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(351, 16, 'Koderma', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(352, 16, 'Latehar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(353, 16, 'Lohardaga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(354, 16, 'Pakur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(355, 16, 'Palamu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(356, 16, 'Ranchi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(357, 16, 'Sahibganj', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(358, 16, 'Seraikela', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(359, 16, 'Simdega', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(360, 16, 'West Singhbhum', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(361, 17, 'Bagalkot', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(362, 17, 'Bangalore', '190715054934.png', 1, '2019-10-03 09:43:52', 0, 0, NULL, NULL, 0),
(363, 17, 'Bangalore Rural', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(364, 17, 'Belgaum', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(365, 17, 'Bellary', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(366, 17, 'Bhatkal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(367, 17, 'Bidar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(368, 17, 'Bijapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(369, 17, 'Chamrajnagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(370, 17, 'Chickmagalur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(371, 17, 'Chikballapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(372, 17, 'Chitradurga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(373, 17, 'Dakshina Kannada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(374, 17, 'Davanagere', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(375, 17, 'Dharwad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(376, 17, 'Gadag', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(377, 17, 'Gulbarga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(378, 17, 'Hampi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(379, 17, 'Hassan', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(380, 17, 'Haveri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(381, 17, 'Hospet', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(382, 17, 'Karwar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(383, 17, 'Kodagu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(384, 17, 'Kolar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(385, 17, 'Koppal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(386, 17, 'Madikeri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(387, 17, 'Mandya', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(388, 17, 'Mangalore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(389, 17, 'Manipal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(390, 17, 'Mysore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(391, 17, 'Raichur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(392, 17, 'Shimoga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(393, 17, 'Sirsi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(394, 17, 'Sringeri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(395, 17, 'Srirangapatna', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(396, 17, 'Tumkur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(397, 17, 'Udupi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(398, 17, 'Uttara Kannada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(399, 18, 'Alappuzha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(400, 18, 'Alleppey', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(401, 18, 'Alwaye', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(402, 18, 'Ernakulam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(403, 18, 'Idukki', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(404, 18, 'Kannur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(405, 18, 'Kasargod', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(406, 18, 'Kochi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(407, 18, 'Kollam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(408, 18, 'Kottayam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(409, 18, 'Kovalam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(410, 18, 'Kozhikode', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(411, 18, 'Malappuram', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(412, 18, 'Palakkad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(413, 18, 'Pathanamthitta', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(414, 18, 'Perumbavoor', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(415, 18, 'Thiruvananthapuram', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(416, 18, 'Thrissur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(417, 18, 'Trichur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(418, 18, 'Trivandrum', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(419, 18, 'Wayanad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(420, 19, 'Agatti Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(421, 19, 'Bingaram Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(422, 19, 'Bitra Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(423, 19, 'Chetlat Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(424, 19, 'Kadmat Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(425, 19, 'Kalpeni Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(426, 19, 'Kavaratti Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(427, 19, 'Kiltan Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(428, 19, 'Lakshadweep Sea', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(429, 19, 'Minicoy Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(430, 19, 'North Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(431, 19, 'South Island', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(432, 20, 'Anuppur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(433, 20, 'Ashoknagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(434, 20, 'Balaghat', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(435, 20, 'Barwani', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(436, 20, 'Betul', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(437, 20, 'Bhind', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(438, 20, 'Bhopal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(439, 20, 'Burhanpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(440, 20, 'Chhatarpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(441, 20, 'Chhindwara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(442, 20, 'Damoh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(443, 20, 'Datia', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(444, 20, 'Dewas', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(445, 20, 'Dhar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(446, 20, 'Dindori', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(447, 20, 'Guna', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(448, 20, 'Gwalior', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(449, 20, 'Harda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(450, 20, 'Hoshangabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(451, 20, 'Indore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(452, 20, 'Jabalpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(453, 20, 'Jagdalpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(454, 20, 'Jhabua', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(455, 20, 'Katni', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(456, 20, 'Khandwa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(457, 20, 'Khargone', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(458, 20, 'Mandla', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(459, 20, 'Mandsaur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(460, 20, 'Morena', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(461, 20, 'Narsinghpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(462, 20, 'Neemuch', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(463, 20, 'Panna', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(464, 20, 'Raisen', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(465, 20, 'Rajgarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(466, 20, 'Ratlam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(467, 20, 'Rewa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(468, 20, 'Sagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(469, 20, 'Satna', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(470, 20, 'Sehore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(471, 20, 'Seoni', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(472, 20, 'Shahdol', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(473, 20, 'Shajapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(474, 20, 'Sheopur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(475, 20, 'Shivpuri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(476, 20, 'Sidhi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(477, 20, 'Tikamgarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(478, 20, 'Ujjain', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(479, 20, 'Umaria', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(480, 20, 'Vidisha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(481, 21, 'Ahmednagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(482, 21, 'Akola', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(483, 21, 'Amravati', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(484, 21, 'Aurangabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(485, 21, 'Beed', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(486, 21, 'Bhandara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(487, 21, 'Buldhana', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(488, 21, 'Chandrapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(489, 21, 'Dhule', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(490, 21, 'Gadchiroli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(491, 21, 'Gondia', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(492, 21, 'Hingoli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(493, 21, 'Jalgaon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(494, 21, 'Jalna', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(495, 21, 'Kolhapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(496, 21, 'Latur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(497, 21, 'Mahabaleshwar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(498, 21, 'Mumbai', '190715051042.png', 1, '2019-10-03 09:47:17', 0, 0, NULL, NULL, 0),
(499, 21, 'Mumbai City', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(500, 21, 'Mumbai Suburban', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(501, 21, 'Nagpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(502, 21, 'Nanded', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(503, 21, 'Nandurbar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(504, 21, 'Nashik', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(505, 21, 'Osmanabad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(506, 21, 'Parbhani', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(507, 21, 'Pune', '190715052042.png', 1, '2019-10-03 09:44:27', 0, 0, NULL, NULL, 0),
(508, 21, 'Raigad', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(509, 21, 'Ratnagiri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(510, 21, 'Sangli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(511, 21, 'Satara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(512, 21, 'Sholapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(513, 21, 'Sindhudurg', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(514, 21, 'Thane', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(515, 21, 'Wardha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(516, 21, 'Washim', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(517, 21, 'Yavatmal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(518, 22, 'Bishnupur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(519, 22, 'Chandel', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(520, 22, 'Churachandpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(521, 22, 'Imphal East', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(522, 22, 'Imphal West', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(523, 22, 'Senapati', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(524, 22, 'Tamenglong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(525, 22, 'Thoubal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(526, 22, 'Ukhrul', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(527, 23, 'East Garo Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(528, 23, 'East Khasi Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(529, 23, 'Jaintia Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(530, 23, 'Ri Bhoi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(531, 23, 'Shillong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(532, 23, 'South Garo Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(533, 23, 'West Garo Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(534, 23, 'West Khasi Hills', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(535, 24, 'Aizawl', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(536, 24, 'Champhai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(537, 24, 'Kolasib', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(538, 24, 'Lawngtlai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(539, 24, 'Lunglei', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(540, 24, 'Mamit', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(541, 24, 'Saiha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(542, 24, 'Serchhip', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(543, 25, 'Dimapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(544, 25, 'Kohima', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(545, 25, 'Mokokchung', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(546, 25, 'Mon', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(547, 25, 'Phek', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(548, 25, 'Tuensang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(549, 25, 'Wokha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(550, 25, 'Zunheboto', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(551, 26, 'Angul', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(552, 26, 'Balangir', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(553, 26, 'Balasore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(554, 26, 'Baleswar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(555, 26, 'Bargarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(556, 26, 'Berhampur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(557, 26, 'Bhadrak', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(558, 26, 'Bhubaneswar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(559, 26, 'Boudh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(560, 26, 'Cuttack', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(561, 26, 'Deogarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(562, 26, 'Dhenkanal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(563, 26, 'Gajapati', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(564, 26, 'Ganjam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(565, 26, 'Jagatsinghapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(566, 26, 'Jajpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(567, 26, 'Jharsuguda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(568, 26, 'Kalahandi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(569, 26, 'Kandhamal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(570, 26, 'Kendrapara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(571, 26, 'Kendujhar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(572, 26, 'Khordha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(573, 26, 'Koraput', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(574, 26, 'Malkangiri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(575, 26, 'Mayurbhanj', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(576, 26, 'Nabarangapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(577, 26, 'Nayagarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(578, 26, 'Nuapada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(579, 26, 'Puri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(580, 26, 'Rayagada', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(581, 26, 'Rourkela', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(582, 26, 'Sambalpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(583, 26, 'Subarnapur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(584, 26, 'Sundergarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(585, 27, 'Bahur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(586, 27, 'Karaikal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(587, 27, 'Mahe', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(588, 27, 'Pondicherry', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(589, 27, 'Purnankuppam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(590, 27, 'Valudavur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(591, 27, 'Villianur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(592, 27, 'Yanam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(593, 28, 'Amritsar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(594, 28, 'Barnala', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(595, 28, 'Bathinda', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(596, 28, 'Faridkot', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(597, 28, 'Fatehgarh Sahib', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(598, 28, 'Ferozepur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(599, 28, 'Gurdaspur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(600, 28, 'Hoshiarpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(601, 28, 'Jalandhar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(602, 28, 'Kapurthala', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(603, 28, 'Ludhiana', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(604, 28, 'Mansa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(605, 28, 'Moga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(606, 28, 'Muktsar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(607, 28, 'Nawanshahr', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(608, 28, 'Pathankot', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(609, 28, 'Patiala', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(610, 28, 'Rupnagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(611, 28, 'Sangrur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(612, 28, 'SAS Nagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(613, 28, 'Tarn Taran', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(614, 29, 'Ajmer', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(615, 29, 'Alwar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(616, 29, 'Banswara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(617, 29, 'Baran', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(618, 29, 'Barmer', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(619, 29, 'Bharatpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(620, 29, 'Bhilwara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(621, 29, 'Bikaner', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(622, 29, 'Bundi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(623, 29, 'Chittorgarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(624, 29, 'Churu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(625, 29, 'Dausa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(626, 29, 'Dholpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(627, 29, 'Dungarpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(628, 29, 'Hanumangarh', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(629, 29, 'Jaipur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(630, 29, 'Jaisalmer', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(631, 29, 'Jalore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(632, 29, 'Jhalawar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(633, 29, 'Jhunjhunu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(634, 29, 'Jodhpur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(635, 29, 'Karauli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(636, 29, 'Kota', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(637, 29, 'Nagaur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(638, 29, 'Pali', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(639, 29, 'Pilani', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(640, 29, 'Rajsamand', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(641, 29, 'Sawai Madhopur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(642, 29, 'Sikar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(643, 29, 'Sirohi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(644, 29, 'Sri Ganganagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(645, 29, 'Tonk', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(646, 29, 'Udaipur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(647, 30, 'Barmiak', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(648, 30, 'Be', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(649, 30, 'Bhurtuk', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(650, 30, 'Chhubakha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(651, 30, 'Chidam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(652, 30, 'Chubha', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(653, 30, 'Chumikteng', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(654, 30, 'Dentam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(655, 30, 'Dikchu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(656, 30, 'Dzongri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(657, 30, 'Gangtok', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(658, 30, 'Gauzing', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(659, 30, 'Gyalshing', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(660, 30, 'Hema', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(661, 30, 'Kerung', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(662, 30, 'Lachen', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(663, 30, 'Lachung', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(664, 30, 'Lema', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(665, 30, 'Lingtam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(666, 30, 'Lungthu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(667, 30, 'Mangan', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(668, 30, 'Namchi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(669, 30, 'Namthang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(670, 30, 'Nanga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(671, 30, 'Nantang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(672, 30, 'Naya Bazar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(673, 30, 'Padamachen', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(674, 30, 'Pakhyong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(675, 30, 'Pemayangtse', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(676, 30, 'Phensang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(677, 30, 'Rangli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(678, 30, 'Rinchingpong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(679, 30, 'Sakyong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(680, 30, 'Samdong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(681, 30, 'Singtam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(682, 30, 'Siniolchu', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(683, 30, 'Sombari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(684, 30, 'Soreng', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(685, 30, 'Sosing', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(686, 30, 'Tekhug', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(687, 30, 'Temi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(688, 30, 'Tsetang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(689, 30, 'Tsomgo', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(690, 30, 'Tumlong', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(691, 30, 'Yangang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0);
INSERT INTO `tblcities` (`nCityIDPK`, `nStateIDFK`, `tCityName`, `tCityImagePath`, `bShowInHeader`, `dCityCreatedOn`, `nCityCreatedBy`, `bIsCityRemoved`, `nCityRemovedBy`, `dtCityRemovedOn`, `bIsCityDummyData`) VALUES
(692, 30, 'Yumtang', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(693, 31, 'Chennai', '190715055240.png', 1, '2019-10-03 09:47:40', 0, 0, NULL, NULL, 0),
(694, 31, 'Chidambaram', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(695, 31, 'Chingleput', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(696, 31, 'Coimbatore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(697, 31, 'Courtallam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(698, 31, 'Cuddalore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(699, 31, 'Dharmapuri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(700, 31, 'Dindigul', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(701, 31, 'Erode', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(702, 31, 'Hosur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(703, 31, 'Kanchipuram', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(704, 31, 'Kanyakumari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(705, 31, 'Karaikudi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(706, 31, 'Karur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(707, 31, 'Kodaikanal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(708, 31, 'Kovilpatti', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(709, 31, 'Krishnagiri', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(710, 31, 'Kumbakonam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(711, 31, 'Madurai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(712, 31, 'Mayiladuthurai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(713, 31, 'Nagapattinam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(714, 31, 'Nagarcoil', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(715, 31, 'Namakkal', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(716, 31, 'Neyveli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(717, 31, 'Nilgiris', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(718, 31, 'Ooty', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(719, 31, 'Palani', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(720, 31, 'Perambalur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(721, 31, 'Pollachi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(722, 31, 'Pudukkottai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(723, 31, 'Rajapalayam', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(724, 31, 'Ramanathapuram', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(725, 31, 'Salem', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(726, 31, 'Sivaganga', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(727, 31, 'Sivakasi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(728, 31, 'Thanjavur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(729, 31, 'Theni', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(730, 31, 'Thoothukudi', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(731, 31, 'Tiruchirappalli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(732, 31, 'Tirunelveli', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(733, 31, 'Tirupur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(734, 31, 'Tiruvallur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(735, 31, 'Tiruvannamalai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(736, 31, 'Tiruvarur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(737, 31, 'Trichy', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(738, 31, 'Tuticorin', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(739, 31, 'Vellore', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(740, 31, 'Villupuram', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(741, 31, 'Virudhunagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(742, 31, 'Yercaud', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(743, 32, 'Agartala', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(744, 32, 'Ambasa', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(745, 32, 'Bampurbari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(746, 32, 'Belonia', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(747, 32, 'Dhalai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(748, 32, 'Dharam Nagar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(749, 32, 'Kailashahar', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(750, 32, 'Kamal Krishnabari', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(751, 32, 'Khopaiyapara', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(752, 32, 'Khowai', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(753, 32, 'Phuldungsei', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(754, 32, 'Radha Kishore Pur', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(755, 32, 'Tripura', '', 0, '2019-10-01 09:53:51', 0, 0, NULL, NULL, 0),
(756, 33, 'Agra', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(757, 33, 'Aligarh', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(758, 33, 'Allahabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(759, 33, 'Ambedkar Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(760, 33, 'Auraiya', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(761, 33, 'Azamgarh', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(762, 33, 'Bagpat', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(763, 33, 'Bahraich', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(764, 33, 'Ballia', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(765, 33, 'Balrampur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(766, 33, 'Banda', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(767, 33, 'Barabanki', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(768, 33, 'Bareilly', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(769, 33, 'Basti', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(770, 33, 'Bijnor', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(771, 33, 'Budaun', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(772, 33, 'Bulandshahr', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(773, 33, 'Chandauli', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(774, 33, 'Chitrakoot', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(775, 33, 'Deoria', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(776, 33, 'Etah', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(777, 33, 'Etawah', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(778, 33, 'Faizabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(779, 33, 'Farrukhabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(780, 33, 'Fatehpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(781, 33, 'Firozabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(782, 33, 'Gautam Buddha Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(783, 33, 'Ghaziabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(784, 33, 'Ghazipur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(785, 33, 'Gonda', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(786, 33, 'Gorakhpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(787, 33, 'Hamirpur (UP)', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(788, 33, 'Hardoi', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(789, 33, 'Hathras', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(790, 33, 'Jalaun', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(791, 33, 'Jaunpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(792, 33, 'Jhansi', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(793, 33, 'Jyotiba Phule Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(794, 33, 'Kannauj', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(795, 33, 'Kanpur Dehat', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(796, 33, 'Kanpur Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(797, 33, 'Kaushambi', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(798, 33, 'Kheri', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(799, 33, 'Kushinagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(800, 33, 'Lalitpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(801, 33, 'Lucknow', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(802, 33, 'Maharajganj', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(803, 33, 'Mahoba', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(804, 33, 'Mainpuri', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(805, 33, 'Mathura', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(806, 33, 'Mau', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(807, 33, 'Meerut', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(808, 33, 'Mirzapur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(809, 33, 'Moradabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(810, 33, 'Muzaffarnagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(811, 33, 'Pilibhit', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(812, 33, 'Pratapgarh', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(813, 33, 'Raebareli', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(814, 33, 'Rampur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(815, 33, 'Saharanpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(816, 33, 'Sant Kabir Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(817, 33, 'Sant Ravidas Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(818, 33, 'Shahjahanpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(819, 33, 'Shrawasti', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(820, 33, 'Siddharthnagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(821, 33, 'Sitapur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(822, 33, 'Sonbhadra', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(823, 33, 'Sultanpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(824, 33, 'Unnao', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(825, 33, 'Varanasi', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(826, 34, 'Almora', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(827, 34, 'Bageshwar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(828, 34, 'Chamoli', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(829, 34, 'Champawat', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(830, 34, 'Dehradun', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(831, 34, 'Haridwar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(832, 34, 'Nainital', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(833, 34, 'Pauri Garhwal', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(834, 34, 'Pithoragarh', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(835, 34, 'Rudraprayag', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(836, 34, 'Tehri Garhwal', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(837, 34, 'Udham Singh Nagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(838, 34, 'Uttarkashi', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(839, 35, 'Asansol', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(840, 35, 'Bankura', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(841, 35, 'Birbhum', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(842, 35, 'Burdwan', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(843, 35, 'Contai', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(844, 35, 'Cooch Behar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(845, 35, 'Darjiling', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(846, 35, 'Hooghly', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(847, 35, 'Howrah', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(848, 35, 'Jalpaiguri', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(849, 35, 'Kolkata', '190715055641.png', 1, '2019-10-03 09:42:00', 0, 0, NULL, NULL, 0),
(850, 35, 'Malda', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(851, 35, 'Midnapore', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(852, 35, 'Murshidabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(853, 35, 'Nadia', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(854, 35, 'North 24 Parganas', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(855, 35, 'North Dinajpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(856, 35, 'Purulia', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(857, 35, 'South 24 Parganas', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(858, 35, 'South Dinajpur', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(859, 35, 'Tamluk', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(860, 36, 'Adilabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(861, 36, 'Hanamkonda', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(862, 36, 'Hyderabad', '190715050941.png', 1, '2019-10-04 06:06:36', 0, 0, NULL, NULL, 0),
(863, 36, 'Hyderabad South East', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(864, 36, 'Karimnagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(865, 36, 'Khammam (TL)', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(866, 36, 'Mahabubnagar', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(867, 36, 'Medak', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(868, 36, 'Nalgonda', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(869, 36, 'Nizamabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(870, 36, 'Peddapalli', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(871, 36, 'Sangareddy', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(872, 36, 'Secunderabad', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(873, 36, 'Suryapet', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(874, 36, 'Wanaparthy', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0),
(875, 36, 'Warangal', '', 0, '2019-10-03 09:12:09', 0, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblcompanies`
--

CREATE TABLE `tblcompanies` (
  `nCompanyIDPK` int(11) NOT NULL,
  `tCompanyName` text NOT NULL,
  `tCompanyWebsiteLink` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcontactus`
--

CREATE TABLE `tblcontactus` (
  `nContactUsIDPK` int(5) NOT NULL,
  `tUserName` text NOT NULL,
  `vUserMobileNo` varchar(30) NOT NULL,
  `tCustomerEmailId` text NOT NULL,
  `tComment` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcountries`
--

CREATE TABLE `tblcountries` (
  `nCountryIDPK` int(11) NOT NULL,
  `tCountryCode` text NOT NULL,
  `tCountryName` text NOT NULL,
  `dtCountryCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCountryCreatedBy` int(11) NOT NULL,
  `bIsCountryRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nCountryRemovedBy` int(11) DEFAULT NULL,
  `dtCountryRemovedOn` datetime DEFAULT NULL,
  `bIsCountryDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcountries`
--

INSERT INTO `tblcountries` (`nCountryIDPK`, `tCountryCode`, `tCountryName`, `dtCountryCreatedOn`, `nCountryCreatedBy`, `bIsCountryRemoved`, `nCountryRemovedBy`, `dtCountryRemovedOn`, `bIsCountryDummyData`) VALUES
(1, '91', 'India', '2019-10-01 07:58:23', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbldesignations`
--

CREATE TABLE `tbldesignations` (
  `nDesignationIDPK` int(11) NOT NULL,
  `tDesignationName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldesignations`
--

INSERT INTO `tbldesignations` (`nDesignationIDPK`, `tDesignationName`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'CEO', '2019-04-24 10:18:24', 1, 0, NULL, NULL, 0),
(11, 'Sales Manager', '2019-10-01 07:00:33', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblfacings`
--

CREATE TABLE `tblfacings` (
  `nFacingIDPK` int(11) NOT NULL,
  `tFacingName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblfacings`
--

INSERT INTO `tblfacings` (`nFacingIDPK`, `tFacingName`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'North', '2019-04-09 11:58:02', 1, 0, NULL, NULL, 0),
(2, 'South', '2019-04-09 11:58:09', 1, 0, NULL, NULL, 0),
(3, 'East', '2019-04-09 11:58:14', 1, 0, NULL, NULL, 0),
(4, 'West', '2019-04-09 11:58:19', 1, 0, NULL, NULL, 0),
(5, 'South West', '2019-05-18 19:03:01', 1, 0, NULL, NULL, 0),
(6, 'North East', '2019-10-06 12:43:46', 1, 0, NULL, NULL, 0),
(7, 'South East', '2019-10-06 12:44:11', 1, 0, NULL, NULL, 0),
(8, 'North West', '2019-10-06 12:44:30', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblimagelabels`
--

CREATE TABLE `tblimagelabels` (
  `nImageLabelIDPK` int(11) NOT NULL,
  `tImageLabelName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbllanguages`
--

CREATE TABLE `tbllanguages` (
  `nLanguageIDPK` int(11) NOT NULL,
  `tLanguageName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbllanguages`
--

INSERT INTO `tbllanguages` (`nLanguageIDPK`, `tLanguageName`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'English', '2019-04-23 08:24:41', 1, 0, NULL, NULL, 0),
(2, 'Hindi', '2019-05-13 19:39:08', 1, 0, NULL, NULL, 0),
(3, 'Gujarati', '2019-05-13 19:39:58', 1, 0, NULL, NULL, 0),
(4, 'Sanskrit', '2019-09-25 13:03:57', 1, 0, NULL, NULL, 0),
(5, 'Punjabi', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(6, 'Bengali', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(7, 'Assamese', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(8, 'Urdu', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(9, 'Oriya', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(10, 'Marathi', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(11, 'Kannada', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(12, 'Tamil', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(13, 'Telugu', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(14, 'Malayalam', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(15, 'Sindhi', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(16, 'Konkani', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(17, 'Manipuri', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(18, 'Khasi', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0),
(19, 'Mizo', '2019-09-25 13:05:45', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblleads`
--

CREATE TABLE `tblleads` (
  `nLeadIDPK` int(5) NOT NULL,
  `nUserIDFK` int(5) NOT NULL,
  `nPropertyIdFK` int(11) DEFAULT NULL,
  `tCustomerName` text NOT NULL,
  `vCustomerrMobileNo` varchar(30) NOT NULL,
  `tCustomerEmailId` text NOT NULL,
  `tVerificationOTP` int(4) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbllocalities`
--

CREATE TABLE `tbllocalities` (
  `nLocalityIDPK` int(11) NOT NULL,
  `nAreaIDFK` int(11) NOT NULL,
  `nCityIDFK` int(11) NOT NULL,
  `nStateIDFK` int(11) NOT NULL,
  `tLocalityName` text NOT NULL,
  `dLocalityLatitude` double(15,10) DEFAULT NULL,
  `dLocalityLongitude` double(15,10) DEFAULT NULL,
  `dLocalityCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nLocalityCreatedBy` int(11) NOT NULL,
  `bIsLocalityRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nLocalityRemovedBy` int(11) DEFAULT NULL,
  `dtLocalityRemovedOn` datetime DEFAULT NULL,
  `bIsLocalityDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblnewsletters`
--

CREATE TABLE `tblnewsletters` (
  `nNewsLettersIDPK` int(11) NOT NULL,
  `tNewsLetterEmailID` text NOT NULL,
  `dtNewsLetterDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsNewsLetterRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblprojectlayouts`
--

CREATE TABLE `tblprojectlayouts` (
  `nProjectIDPK` int(11) NOT NULL,
  `nPropertyIDFK` int(11) NOT NULL,
  `nProjectSubTypeIDFK` int(11) NOT NULL,
  `nBHKIDFK` int(11) NOT NULL,
  `nFacingIDFK` int(11) DEFAULT NULL,
  `fProjectSuperBuiltArea` int(11) NOT NULL,
  `fProjectBuiltArea` int(11) NOT NULL,
  `fProjectCarpetArea` int(11) NOT NULL,
  `fConstructionSuperBuiltArea` int(11) NOT NULL,
  `fConstructionBuiltArea` int(11) NOT NULL,
  `fConstructionCarpetArea` int(11) NOT NULL,
  `nUnitIDFK` int(11) NOT NULL,
  `bIsPropertyFurnished` tinyint(1) NOT NULL COMMENT ' 0-Unfurnished,1-Furnished,2-Semifurnished',
  `nBedrooms` int(11) NOT NULL,
  `nKitchens` int(11) NOT NULL,
  `nBalconies` int(11) NOT NULL,
  `nBathrooms` int(11) NOT NULL,
  `nWashroom` int(11) DEFAULT NULL,
  `nPentry` int(11) NOT NULL,
  `nCarParkings` int(11) NOT NULL,
  `nTotalFloors` int(11) NOT NULL,
  `nPropertyOnFloor` int(11) NOT NULL,
  `bIsPoojaRoomAvailable` tinyint(1) NOT NULL DEFAULT '0',
  `bIsStoreRoomAvailable` tinyint(1) NOT NULL DEFAULT '0',
  `tLayoutUmages` text NOT NULL,
  `fProjectExpectedPrice` float(10,2) NOT NULL,
  `bIsExpectedPriceNegotiable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No , 1 - Yes',
  `fPricePerUnitArea` float(10,2) NOT NULL,
  `fProjectBookingPrice` float(10,2) NOT NULL,
  `bIsPriceShow` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 show price , 1 dont show price',
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblproperties`
--

CREATE TABLE `tblproperties` (
  `nPropertyIDPK` int(11) NOT NULL,
  `vFolderName` varchar(255) NOT NULL,
  `bIsProperty` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Property,1-Projects',
  `nPropertyTypeIDFK` int(11) NOT NULL,
  `nPropertySubTypeIDFK` int(11) NOT NULL,
  `nPropertyDerivedTypeIDFK` int(11) NOT NULL,
  `nPropertyAddedBy` int(11) NOT NULL DEFAULT '0' COMMENT '0-Agent,1-developer',
  `nPropertyFor` tinyint(1) NOT NULL COMMENT '0-Buy,1-Rent',
  `tPropertyName` text NOT NULL,
  `nCompanyIDFK` int(11) NOT NULL,
  `nLocalityIDFK` int(11) DEFAULT NULL,
  `nPropertyAreaIDFK` int(11) DEFAULT NULL,
  `nPropertyCityIDFK` int(11) NOT NULL,
  `nPropertyStateIDFK` int(11) NOT NULL,
  `nPropertyCountryIDFK` int(11) NOT NULL,
  `dPropertyLatitude` double(15,10) DEFAULT NULL,
  `dPropertyLongitude` double(15,10) DEFAULT NULL,
  `tPropertyNumber` text,
  `tPropertyStreetName` text NOT NULL,
  `tPropertyAddress` text NOT NULL,
  `tPropertyMapLocationLink` text NOT NULL,
  `fPropertySuperBuiltArea` int(11) NOT NULL,
  `fPropertyBuiltArea` int(11) NOT NULL,
  `fPropertyCarpetArea` int(11) NOT NULL,
  `nUnitIDFK` int(11) NOT NULL,
  `bIsPropertyFurnished` tinyint(1) NOT NULL COMMENT ' 	0-Unfurnished,1-Furnished,2-Semifurnished',
  `nTotalFloors` text,
  `nPropertyOnFloor` text,
  `nBedrooms` text,
  `nKitchens` text,
  `nBalconies` text,
  `nBathrooms` text,
  `nWashroom` int(11) DEFAULT NULL,
  `bIsStoreRoomAvailable` tinyint(1) NOT NULL DEFAULT '0',
  `bIsPoojaRoomAvailable` tinyint(1) NOT NULL DEFAULT '0',
  `nBHKIDFK` text NOT NULL,
  `nCarParkings` tinyint(1) NOT NULL DEFAULT '0',
  `bIsPropertyReraCertified` tinyint(1) NOT NULL DEFAULT '0',
  `tPropertyReraID` text,
  `nPropertyAvailability` tinyint(1) DEFAULT '0' COMMENT '0-Ready to move,1-Underconstruction',
  `dtPropertyPossesionDate` date DEFAULT NULL,
  `tPropertyOwnership` text NOT NULL COMMENT 'freehold,leasehold,co-operative society,power of attorney',
  `fPropertyExpectedPrice` float(10,2) NOT NULL,
  `bIsExpectedPriceNegotiable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-No , 1 - Yes',
  `nPropertyBookingPrice` float(10,2) NOT NULL,
  `fPricePerUnitArea` float(10,2) NOT NULL,
  `fPropertyMaintenancePrice` float(10,2) NOT NULL,
  `nPropertyMaintenanceFor` tinyint(1) NOT NULL COMMENT '0-Monthly, 1-Yearly',
  `tPropertyDescription` text NOT NULL,
  `nFacingIDFK` int(11) NOT NULL,
  `fPropertyRoadWidth` float(10,2) DEFAULT NULL,
  `fPropertyPlotSize` float(10,2) DEFAULT NULL,
  `bIsPropertyVerifiedByAdmin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Not Verified, 1-Verified',
  `tPropertyBrochurePath` text,
  `bIsBrokerageAvailable` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 for not , 1 for available',
  `fPropertyBrokerageCharge` float(10,2) DEFAULT NULL,
  `bIsPropertyBrokerageChargeNegotiable` tinyint(1) NOT NULL DEFAULT '0',
  `bIsPropertyBrokerageChargeIn` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-Price,1-Percentage',
  `bIsAvailable` tinyint(1) NOT NULL DEFAULT '1',
  `bIsFeaturedListing` tinyint(1) NOT NULL DEFAULT '0',
  `dtPropertyExpireOn` datetime DEFAULT NULL,
  `bIsPropertyExpire` tinyint(1) NOT NULL DEFAULT '0',
  `nPropertyTotalViews` int(11) NOT NULL DEFAULT '0',
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpropertyamenities`
--

CREATE TABLE `tblpropertyamenities` (
  `nPropertyAmenityIDPK` int(11) NOT NULL,
  `nPropertyIDFK` int(11) NOT NULL,
  `nAmenityIDFK` int(11) NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpropertyderivedtype`
--

CREATE TABLE `tblpropertyderivedtype` (
  `nPropertyDerivedTypeIDPK` int(11) NOT NULL,
  `nPropertySubTypeIDFK` int(11) NOT NULL,
  `tPropertyDerivedTypeName` text NOT NULL,
  `dtPropertyDerivedTypeCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsPropertyDerivedTypeRemoved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpropertyimages`
--

CREATE TABLE `tblpropertyimages` (
  `nPropertyImageIDPK` int(11) NOT NULL,
  `nPropertyIDFK` int(11) NOT NULL,
  `nImageLabelIDFK` int(11) DEFAULT NULL,
  `tImageLabelName` text,
  `tPropertyImageFolderPath` text NOT NULL,
  `bIsFrontImage` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 for Other , 1 Cover Image',
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpropertysubtypes`
--

CREATE TABLE `tblpropertysubtypes` (
  `nPropertySubTypeIDPK` int(11) NOT NULL,
  `nPropertyTypeIDFK` int(11) NOT NULL,
  `tPropertySubTypeName` text NOT NULL,
  `tPropertySubTypeImagePath` text NOT NULL,
  `dtPropertSubTypeCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsPropertySubTypeRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpropertysubtypes`
--

INSERT INTO `tblpropertysubtypes` (`nPropertySubTypeIDPK`, `nPropertyTypeIDFK`, `tPropertySubTypeName`, `tPropertySubTypeImagePath`, `dtPropertSubTypeCreatedOn`, `bIsPropertySubTypeRemoved`) VALUES
(1, 2, 'Row House', '', '2019-04-05 15:14:12', 1),
(2, 1, 'Flats', '', '2019-04-05 15:14:17', 1),
(3, 2, 'comeesub', '190404021314.jpg', '2019-04-05 15:14:07', 1),
(4, 1, 'Flat/Apartment', '190405012346.png', '2019-06-13 12:53:55', 0),
(5, 1, 'Villa/Bungalow', '190405013349.png', '2019-06-13 12:54:39', 0),
(6, 1, 'Independent House', '190405012752.png', '2019-06-13 12:55:07', 0),
(7, 1, 'Plot/Land', '190405012553.png', '2019-06-13 12:55:50', 0),
(8, 2, 'Office Space', '190408075726.png', '2019-06-13 12:58:47', 0),
(9, 2, 'Shop', '190408071527.png', '2019-06-13 12:59:17', 0),
(10, 2, 'Industrial Factory', '190419061415.png', '2019-06-13 13:00:50', 0),
(11, 2, 'Industrial Land', '190419063415.png', '2019-06-13 13:00:39', 0),
(12, 2, 'Warehouse', '190419065915.png', '2019-06-13 13:10:57', 0),
(13, 1, 'Row House', '190613113526.png', '2019-06-13 12:56:35', 0),
(14, 1, 'Penthouse', '190613115226.png', '2019-06-13 12:56:52', 0),
(15, 1, 'Farm House', '190613110727.png', '2019-06-13 12:57:07', 0),
(16, 2, 'Showroom', '190613115029.png', '2019-06-13 13:00:07', 0),
(17, 2, 'Corporate House', '190613112130.png', '2019-06-13 13:00:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblpropertytypes`
--

CREATE TABLE `tblpropertytypes` (
  `nPropertyTypeIDPK` int(11) NOT NULL,
  `tPropertyTypeName` text NOT NULL,
  `bIsPropertyTypeActive` tinyint(1) NOT NULL DEFAULT '1',
  `dPropertyTypeCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsPropertyTypeRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpropertytypes`
--

INSERT INTO `tblpropertytypes` (`nPropertyTypeIDPK`, `tPropertyTypeName`, `bIsPropertyTypeActive`, `dPropertyTypeCreatedOn`, `bIsPropertyTypeRemoved`) VALUES
(1, 'Residential', 1, '2018-10-30 13:00:56', 0),
(2, 'Commercial', 1, '2019-02-27 11:49:32', 0),
(3, 'Residential Cum Commercial', 1, '2019-04-04 09:32:54', 1),
(4, 'aaa', 1, '2018-10-30 13:14:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblservices`
--

CREATE TABLE `tblservices` (
  `nServiceIDPK` int(11) NOT NULL,
  `tServiceName` text NOT NULL,
  `dtServiceCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsServiceRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblspecializations`
--

CREATE TABLE `tblspecializations` (
  `nSpecializationIDPK` int(11) NOT NULL,
  `tSpecializationName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblspecializations`
--

INSERT INTO `tblspecializations` (`nSpecializationIDPK`, `tSpecializationName`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'Residential', '2019-05-14 02:34:46', 1, 0, NULL, NULL, 0),
(2, 'Commercial', '2019-05-14 02:35:03', 1, 0, NULL, NULL, 0),
(3, 'Industrial', '2019-05-14 02:35:25', 1, 0, NULL, NULL, 0),
(4, 'Preleased', '2019-05-14 02:37:20', 1, 0, NULL, NULL, 0),
(5, 'Land Transaction', '2019-05-14 02:38:03', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblstates`
--

CREATE TABLE `tblstates` (
  `nStateIDPK` int(11) NOT NULL,
  `nCountryIDFK` int(11) NOT NULL,
  `tStateName` text NOT NULL,
  `dStateCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nStateCreatedBy` int(11) NOT NULL,
  `bIsStateRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nStateRemovedBy` int(11) DEFAULT NULL,
  `dtStateRemovedOn` datetime DEFAULT NULL,
  `bIsStateDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstates`
--

INSERT INTO `tblstates` (`nStateIDPK`, `nCountryIDFK`, `tStateName`, `dStateCreatedOn`, `nStateCreatedBy`, `bIsStateRemoved`, `nStateRemovedBy`, `dtStateRemovedOn`, `bIsStateDummyData`) VALUES
(1, 1, 'Andaman & Nicobar', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(2, 1, 'Andhra Pradesh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(3, 1, 'Arunachal Pradesh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(4, 1, 'Assam', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(5, 1, 'Bihar', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(6, 1, 'Chandigarh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(7, 1, 'Chhattisgarh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(8, 1, 'Dadra & Nagar Haveli', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(9, 1, 'Daman & Diu', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(10, 1, 'Delhi', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(11, 1, 'Goa', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(12, 1, 'Gujarat', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(13, 1, 'Haryana', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(14, 1, 'Himachal Pradesh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(15, 1, 'Jammu & Kashmir', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(16, 1, 'Jharkhand', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(17, 1, 'Karnataka', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(18, 1, 'Kerala', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(19, 1, 'Lakshadweep', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(20, 1, 'Madhya Pradesh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(21, 1, 'Maharashtra', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(22, 1, 'Manipur', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(23, 1, 'Meghalaya', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(24, 1, 'Mizoram', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(25, 1, 'Nagaland', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(26, 1, 'Orissa', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(27, 1, 'Pondicherry', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(28, 1, 'Punjab', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(29, 1, 'Rajasthan', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(30, 1, 'Sikkim', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(31, 1, 'Tamil Nadu', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(32, 1, 'Tripura', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(33, 1, 'Uttar Pradesh', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(34, 1, 'Uttaranchal', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(35, 1, 'West Bengal', '2019-10-01 09:21:01', 0, 0, NULL, NULL, 0),
(36, 1, 'Telangana', '2019-10-01 09:22:15', 0, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblsubcription`
--

CREATE TABLE `tblsubcription` (
  `nSubscriptionIDPK` int(11) NOT NULL,
  `tSubscriptionName` text NOT NULL,
  `fSubscriptionPrice` float(10,2) NOT NULL,
  `fOtherCountrySubscriptionPrice` float(10,2) NOT NULL,
  `nListingLimit` int(11) NOT NULL,
  `dtSubscriptionExpireOn` int(11) NOT NULL,
  `dtListingExpireOn` int(11) NOT NULL,
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `dCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dtRemovedOn` datetime DEFAULT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblteam`
--

CREATE TABLE `tblteam` (
  `nTeamIDPK` int(11) NOT NULL,
  `tMemberName` varchar(100) NOT NULL,
  `tImgPath1` text NOT NULL,
  `tBio` text NOT NULL,
  `nDesignationIDFK` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblteamdesignation`
--

CREATE TABLE `tblteamdesignation` (
  `nDesignationIDPK` int(11) NOT NULL,
  `tName` text NOT NULL,
  `isRemoved` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbltestimonials`
--

CREATE TABLE `tbltestimonials` (
  `nTestimonialIDPK` int(11) NOT NULL,
  `tTestimonialCLientName` text NOT NULL,
  `tTestimonialClientImgPath` text NOT NULL,
  `tTestimonialDescription` text NOT NULL,
  `dtTestimonialDateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bIsTestimonialRemoved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblunits`
--

CREATE TABLE `tblunits` (
  `nUnitIDPK` int(11) NOT NULL,
  `tUnitName` text NOT NULL,
  `dtCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nCreatedBy` int(11) NOT NULL,
  `bIsRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtRemovedOn` datetime DEFAULT NULL,
  `nRemovedBy` int(11) DEFAULT NULL,
  `bIsDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblunits`
--

INSERT INTO `tblunits` (`nUnitIDPK`, `tUnitName`, `dtCreatedOn`, `nCreatedBy`, `bIsRemoved`, `dtRemovedOn`, `nRemovedBy`, `bIsDummyData`) VALUES
(1, 'Sq.Mt', '2019-10-06 12:44:59', 1, 0, NULL, NULL, 0),
(2, 'Sq.Ft', '2019-10-06 12:45:32', 1, 0, NULL, NULL, 0),
(3, 'Sq.Yd', '2019-10-06 12:45:46', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `nUserIDPK` int(11) NOT NULL,
  `nUserTypeIDFK` int(11) NOT NULL,
  `tUserName` text NOT NULL,
  `vUserEmailID` varchar(50) NOT NULL,
  `tUserPassword` text NOT NULL,
  `vUserFirstName` varchar(100) DEFAULT NULL,
  `vUserMiddleName` varchar(100) DEFAULT NULL,
  `vUserLastName` varchar(100) DEFAULT NULL,
  `nUserPhoneNumber` varchar(20) NOT NULL,
  `eUserGender` enum('0','1','2','3') DEFAULT NULL COMMENT '''0 for female, 1 for male, 2 for others, 3 for rather not say''',
  `tUserProfileImgPath` text,
  `bIsMobileNumberVerified` tinyint(1) NOT NULL DEFAULT '0',
  `bIsEmailIdVerified` tinyint(1) NOT NULL DEFAULT '0',
  `nVerificationOTP` int(11) NOT NULL,
  `tVerificationOTP` text NOT NULL,
  `nAddListingUpto` int(11) NOT NULL DEFAULT '0',
  `bIsActive` tinyint(1) NOT NULL DEFAULT '1',
  `nUserPhoneIMEI` varchar(20) DEFAULT NULL,
  `nCurrentAppVersion` varchar(15) DEFAULT NULL,
  `dtUserCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nUserCreatedBy` int(11) NOT NULL,
  `bIsUserRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `dtUserRemovedOn` datetime DEFAULT NULL,
  `nUserRemovedBy` int(11) DEFAULT NULL,
  `nIsUserDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`nUserIDPK`, `nUserTypeIDFK`, `tUserName`, `vUserEmailID`, `tUserPassword`, `vUserFirstName`, `vUserMiddleName`, `vUserLastName`, `nUserPhoneNumber`, `eUserGender`, `tUserProfileImgPath`, `bIsMobileNumberVerified`, `bIsEmailIdVerified`, `nVerificationOTP`, `tVerificationOTP`, `nAddListingUpto`, `bIsActive`, `nUserPhoneIMEI`, `nCurrentAppVersion`, `dtUserCreatedOn`, `nUserCreatedBy`, `bIsUserRemoved`, `dtUserRemovedOn`, `nUserRemovedBy`, `nIsUserDummyData`) VALUES
(1, 1, 'Superadmin', 'admin@agenpoint.com', 'admin.agenpoint@2019', 'Admin', NULL, NULL, '', NULL, 'user_2.jpg', 1, 1, 1, '0', 0, 1, NULL, NULL, '2019-07-15 18:51:39', 1, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblusertype`
--

CREATE TABLE `tblusertype` (
  `nUserTypeIDPK` int(11) NOT NULL,
  `nUserTypeName` varchar(50) NOT NULL,
  `bIsUserTypeActive` tinyint(1) NOT NULL DEFAULT '1',
  `dtUserTypeCreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dUserTypeCreatedBy` int(11) NOT NULL,
  `bIsUserTypeRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `nUserTypeRemovedBy` int(11) DEFAULT NULL,
  `dtUserTypeRemovedOn` datetime DEFAULT NULL,
  `bIsUserTypeDummyData` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusertype`
--

INSERT INTO `tblusertype` (`nUserTypeIDPK`, `nUserTypeName`, `bIsUserTypeActive`, `dtUserTypeCreatedOn`, `dUserTypeCreatedBy`, `bIsUserTypeRemoved`, `nUserTypeRemovedBy`, `dtUserTypeRemovedOn`, `bIsUserTypeDummyData`) VALUES
(1, 'Superadmin', 1, '2018-10-09 13:31:29', 1, 0, 0, '0000-00-00 00:00:00', 0),
(2, 'Agents', 1, '2018-12-15 10:08:18', 1, 0, 0, '0000-00-00 00:00:00', 0),
(3, 'Developer', 1, '2019-09-07 09:55:19', 1, 0, 0, '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblaboutus`
--
ALTER TABLE `tblaboutus`
  ADD PRIMARY KEY (`nAboutUsIDPK`);

--
-- Indexes for table `tbladminchangelog`
--
ALTER TABLE `tbladminchangelog`
  ADD PRIMARY KEY (`nAdminChangeLogIDPK`),
  ADD KEY `nLoggedInIDFK` (`nLoggedInIDFK`);

--
-- Indexes for table `tblagentassociations`
--
ALTER TABLE `tblagentassociations`
  ADD PRIMARY KEY (`nAgentAssociationIDPK`);

--
-- Indexes for table `tblagentcontactnumbers`
--
ALTER TABLE `tblagentcontactnumbers`
  ADD PRIMARY KEY (`nAgentContactNumberIDPK`);

--
-- Indexes for table `tblagentlanguagesknown`
--
ALTER TABLE `tblagentlanguagesknown`
  ADD PRIMARY KEY (`nAgentLanguageKnownIDPK`);

--
-- Indexes for table `tblagentlocalitiesexpert`
--
ALTER TABLE `tblagentlocalitiesexpert`
  ADD PRIMARY KEY (`nAgentLocalityIDPK`);

--
-- Indexes for table `tblagentreviews`
--
ALTER TABLE `tblagentreviews`
  ADD PRIMARY KEY (`nAgentReviewIDPK`);

--
-- Indexes for table `tblagents`
--
ALTER TABLE `tblagents`
  ADD PRIMARY KEY (`nAgentIDPK`);

--
-- Indexes for table `tblagentspecializations`
--
ALTER TABLE `tblagentspecializations`
  ADD PRIMARY KEY (`nAgentSpecializationIDPK`);

--
-- Indexes for table `tblagentsubscription`
--
ALTER TABLE `tblagentsubscription`
  ADD PRIMARY KEY (`nAgentSubscriptionIDPK`);

--
-- Indexes for table `tblagenttestimonials`
--
ALTER TABLE `tblagenttestimonials`
  ADD PRIMARY KEY (`nAgentTestimonialIDPK`);

--
-- Indexes for table `tblamenities`
--
ALTER TABLE `tblamenities`
  ADD PRIMARY KEY (`nAmenityIDPK`);

--
-- Indexes for table `tblapplypositionrequest`
--
ALTER TABLE `tblapplypositionrequest`
  ADD PRIMARY KEY (`nPositionRequestIDPK`);

--
-- Indexes for table `tblareas`
--
ALTER TABLE `tblareas`
  ADD PRIMARY KEY (`nAreaIDPK`),
  ADD KEY `nCityIDFK` (`nCityIDFK`);

--
-- Indexes for table `tblassociationrequest`
--
ALTER TABLE `tblassociationrequest`
  ADD PRIMARY KEY (`nAssociationRequestIDPK`);

--
-- Indexes for table `tblassociations`
--
ALTER TABLE `tblassociations`
  ADD PRIMARY KEY (`nAssociationIDPK`);

--
-- Indexes for table `tblbhk`
--
ALTER TABLE `tblbhk`
  ADD PRIMARY KEY (`nBHKIDPK`);

--
-- Indexes for table `tblblogs`
--
ALTER TABLE `tblblogs`
  ADD PRIMARY KEY (`nBlogIDPK`);

--
-- Indexes for table `tblcareerdesignations`
--
ALTER TABLE `tblcareerdesignations`
  ADD PRIMARY KEY (`nCareerDesignationIDPK`);

--
-- Indexes for table `tblcareers`
--
ALTER TABLE `tblcareers`
  ADD PRIMARY KEY (`nCareerIDPK`);

--
-- Indexes for table `tblcities`
--
ALTER TABLE `tblcities`
  ADD PRIMARY KEY (`nCityIDPK`),
  ADD KEY `nStateIDFK` (`nStateIDFK`);

--
-- Indexes for table `tblcompanies`
--
ALTER TABLE `tblcompanies`
  ADD PRIMARY KEY (`nCompanyIDPK`);

--
-- Indexes for table `tblcontactus`
--
ALTER TABLE `tblcontactus`
  ADD PRIMARY KEY (`nContactUsIDPK`);

--
-- Indexes for table `tblcountries`
--
ALTER TABLE `tblcountries`
  ADD PRIMARY KEY (`nCountryIDPK`);

--
-- Indexes for table `tbldesignations`
--
ALTER TABLE `tbldesignations`
  ADD PRIMARY KEY (`nDesignationIDPK`);

--
-- Indexes for table `tblfacings`
--
ALTER TABLE `tblfacings`
  ADD PRIMARY KEY (`nFacingIDPK`);

--
-- Indexes for table `tblimagelabels`
--
ALTER TABLE `tblimagelabels`
  ADD PRIMARY KEY (`nImageLabelIDPK`);

--
-- Indexes for table `tbllanguages`
--
ALTER TABLE `tbllanguages`
  ADD PRIMARY KEY (`nLanguageIDPK`);

--
-- Indexes for table `tblleads`
--
ALTER TABLE `tblleads`
  ADD PRIMARY KEY (`nLeadIDPK`);

--
-- Indexes for table `tbllocalities`
--
ALTER TABLE `tbllocalities`
  ADD PRIMARY KEY (`nLocalityIDPK`);

--
-- Indexes for table `tblnewsletters`
--
ALTER TABLE `tblnewsletters`
  ADD PRIMARY KEY (`nNewsLettersIDPK`);

--
-- Indexes for table `tblprojectlayouts`
--
ALTER TABLE `tblprojectlayouts`
  ADD PRIMARY KEY (`nProjectIDPK`);

--
-- Indexes for table `tblproperties`
--
ALTER TABLE `tblproperties`
  ADD PRIMARY KEY (`nPropertyIDPK`),
  ADD KEY `nPlaceAreaIDFK` (`nPropertyAreaIDFK`,`nPropertyCityIDFK`,`nPropertyStateIDFK`,`nPropertyCountryIDFK`),
  ADD KEY `nPlaceCityIDFK` (`nPropertyCityIDFK`),
  ADD KEY `nPlaceAreaIDFK_2` (`nPropertyAreaIDFK`),
  ADD KEY `nPlaceStateIDFK` (`nPropertyStateIDFK`),
  ADD KEY `nPlaceCountryIDFK` (`nPropertyCountryIDFK`);

--
-- Indexes for table `tblpropertyamenities`
--
ALTER TABLE `tblpropertyamenities`
  ADD PRIMARY KEY (`nPropertyAmenityIDPK`);

--
-- Indexes for table `tblpropertyderivedtype`
--
ALTER TABLE `tblpropertyderivedtype`
  ADD PRIMARY KEY (`nPropertyDerivedTypeIDPK`);

--
-- Indexes for table `tblpropertyimages`
--
ALTER TABLE `tblpropertyimages`
  ADD PRIMARY KEY (`nPropertyImageIDPK`);

--
-- Indexes for table `tblpropertysubtypes`
--
ALTER TABLE `tblpropertysubtypes`
  ADD PRIMARY KEY (`nPropertySubTypeIDPK`);

--
-- Indexes for table `tblpropertytypes`
--
ALTER TABLE `tblpropertytypes`
  ADD PRIMARY KEY (`nPropertyTypeIDPK`);

--
-- Indexes for table `tblservices`
--
ALTER TABLE `tblservices`
  ADD PRIMARY KEY (`nServiceIDPK`);

--
-- Indexes for table `tblspecializations`
--
ALTER TABLE `tblspecializations`
  ADD PRIMARY KEY (`nSpecializationIDPK`);

--
-- Indexes for table `tblstates`
--
ALTER TABLE `tblstates`
  ADD PRIMARY KEY (`nStateIDPK`),
  ADD KEY `nCountryIDFK` (`nCountryIDFK`);

--
-- Indexes for table `tblsubcription`
--
ALTER TABLE `tblsubcription`
  ADD PRIMARY KEY (`nSubscriptionIDPK`);

--
-- Indexes for table `tblteam`
--
ALTER TABLE `tblteam`
  ADD PRIMARY KEY (`nTeamIDPK`),
  ADD KEY `nDesignationIDFK` (`nDesignationIDFK`);

--
-- Indexes for table `tblteamdesignation`
--
ALTER TABLE `tblteamdesignation`
  ADD PRIMARY KEY (`nDesignationIDPK`);

--
-- Indexes for table `tbltestimonials`
--
ALTER TABLE `tbltestimonials`
  ADD PRIMARY KEY (`nTestimonialIDPK`);

--
-- Indexes for table `tblunits`
--
ALTER TABLE `tblunits`
  ADD PRIMARY KEY (`nUnitIDPK`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`nUserIDPK`);

--
-- Indexes for table `tblusertype`
--
ALTER TABLE `tblusertype`
  ADD PRIMARY KEY (`nUserTypeIDPK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblaboutus`
--
ALTER TABLE `tblaboutus`
  MODIFY `nAboutUsIDPK` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbladminchangelog`
--
ALTER TABLE `tbladminchangelog`
  MODIFY `nAdminChangeLogIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentassociations`
--
ALTER TABLE `tblagentassociations`
  MODIFY `nAgentAssociationIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentcontactnumbers`
--
ALTER TABLE `tblagentcontactnumbers`
  MODIFY `nAgentContactNumberIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentlanguagesknown`
--
ALTER TABLE `tblagentlanguagesknown`
  MODIFY `nAgentLanguageKnownIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentlocalitiesexpert`
--
ALTER TABLE `tblagentlocalitiesexpert`
  MODIFY `nAgentLocalityIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentreviews`
--
ALTER TABLE `tblagentreviews`
  MODIFY `nAgentReviewIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagents`
--
ALTER TABLE `tblagents`
  MODIFY `nAgentIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentspecializations`
--
ALTER TABLE `tblagentspecializations`
  MODIFY `nAgentSpecializationIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagentsubscription`
--
ALTER TABLE `tblagentsubscription`
  MODIFY `nAgentSubscriptionIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblagenttestimonials`
--
ALTER TABLE `tblagenttestimonials`
  MODIFY `nAgentTestimonialIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblamenities`
--
ALTER TABLE `tblamenities`
  MODIFY `nAmenityIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tblapplypositionrequest`
--
ALTER TABLE `tblapplypositionrequest`
  MODIFY `nPositionRequestIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblareas`
--
ALTER TABLE `tblareas`
  MODIFY `nAreaIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblassociationrequest`
--
ALTER TABLE `tblassociationrequest`
  MODIFY `nAssociationRequestIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblassociations`
--
ALTER TABLE `tblassociations`
  MODIFY `nAssociationIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblbhk`
--
ALTER TABLE `tblbhk`
  MODIFY `nBHKIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tblblogs`
--
ALTER TABLE `tblblogs`
  MODIFY `nBlogIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcareerdesignations`
--
ALTER TABLE `tblcareerdesignations`
  MODIFY `nCareerDesignationIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcareers`
--
ALTER TABLE `tblcareers`
  MODIFY `nCareerIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcities`
--
ALTER TABLE `tblcities`
  MODIFY `nCityIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=876;

--
-- AUTO_INCREMENT for table `tblcompanies`
--
ALTER TABLE `tblcompanies`
  MODIFY `nCompanyIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcontactus`
--
ALTER TABLE `tblcontactus`
  MODIFY `nContactUsIDPK` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblcountries`
--
ALTER TABLE `tblcountries`
  MODIFY `nCountryIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbldesignations`
--
ALTER TABLE `tbldesignations`
  MODIFY `nDesignationIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tblfacings`
--
ALTER TABLE `tblfacings`
  MODIFY `nFacingIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblimagelabels`
--
ALTER TABLE `tblimagelabels`
  MODIFY `nImageLabelIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbllanguages`
--
ALTER TABLE `tbllanguages`
  MODIFY `nLanguageIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tblleads`
--
ALTER TABLE `tblleads`
  MODIFY `nLeadIDPK` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbllocalities`
--
ALTER TABLE `tbllocalities`
  MODIFY `nLocalityIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblnewsletters`
--
ALTER TABLE `tblnewsletters`
  MODIFY `nNewsLettersIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblprojectlayouts`
--
ALTER TABLE `tblprojectlayouts`
  MODIFY `nProjectIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblproperties`
--
ALTER TABLE `tblproperties`
  MODIFY `nPropertyIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpropertyamenities`
--
ALTER TABLE `tblpropertyamenities`
  MODIFY `nPropertyAmenityIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpropertyderivedtype`
--
ALTER TABLE `tblpropertyderivedtype`
  MODIFY `nPropertyDerivedTypeIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpropertyimages`
--
ALTER TABLE `tblpropertyimages`
  MODIFY `nPropertyImageIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpropertysubtypes`
--
ALTER TABLE `tblpropertysubtypes`
  MODIFY `nPropertySubTypeIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tblpropertytypes`
--
ALTER TABLE `tblpropertytypes`
  MODIFY `nPropertyTypeIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblservices`
--
ALTER TABLE `tblservices`
  MODIFY `nServiceIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblspecializations`
--
ALTER TABLE `tblspecializations`
  MODIFY `nSpecializationIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblstates`
--
ALTER TABLE `tblstates`
  MODIFY `nStateIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tblsubcription`
--
ALTER TABLE `tblsubcription`
  MODIFY `nSubscriptionIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblteam`
--
ALTER TABLE `tblteam`
  MODIFY `nTeamIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblteamdesignation`
--
ALTER TABLE `tblteamdesignation`
  MODIFY `nDesignationIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbltestimonials`
--
ALTER TABLE `tbltestimonials`
  MODIFY `nTestimonialIDPK` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblunits`
--
ALTER TABLE `tblunits`
  MODIFY `nUnitIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `nUserIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblusertype`
--
ALTER TABLE `tblusertype`
  MODIFY `nUserTypeIDPK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbladminchangelog`
--
ALTER TABLE `tbladminchangelog`
  ADD CONSTRAINT `fadminchangelogUserIDPK` FOREIGN KEY (`nLoggedInIDFK`) REFERENCES `tblusers` (`nUserIDPK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblareas`
--
ALTER TABLE `tblareas`
  ADD CONSTRAINT `fareaCityIDPK` FOREIGN KEY (`nCityIDFK`) REFERENCES `tblcities` (`nCityIDPK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tblcities`
--
ALTER TABLE `tblcities`
  ADD CONSTRAINT `fcStateIDPK` FOREIGN KEY (`nStateIDFK`) REFERENCES `tblstates` (`nStateIDPK`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
